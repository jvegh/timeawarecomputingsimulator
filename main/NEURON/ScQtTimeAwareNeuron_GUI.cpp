/**
 * @file NEURER_GUI.cpp
 *
 * @brief This is the main file of the graphic version of the SystemC-based basic electronic simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */

#include "systemc.h"
#include <QApplication>
#include "SimulatorWindowNeurer.h"
#include "NeurerConfig.h"

//#include <QtCore/QThread>
bool UNIT_TESTING = false;
scqSimulator* TheSimulator;// This defines if running through a simulator
string ListOfIniFiles;
//extern void
//MyMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int sc_main( int argc, char **argv ) {
    ListOfIniFiles = INI_FILES;
    TheSimulator = new scqSimulator("scqSimulator",argc,argv); // Will install message handler, too
  QApplication a( argc, argv );
  SimulatorWindowNeurer w(argc,argv);

  w.show();
  return a.exec();
}


#if 0
/**
 * @file ScQtSimBasic_CLI.cpp
 *
 * @brief This is the main file of the text version of the SystemC-based basic electronic simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */
#include "systemc.h"
//#include "EMPATypes.h"
#include "Stuff.h"
#include "QStuff.h"
#include "QApplication"
#include <chrono>

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scSimulator.h"

bool UNIT_TESTING = false;
bool OBJECT_FILE_PRINTED;
//extern instr_t instruction_set[];


/**
 * @brief This is the main file of the text version of the EMPA simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */
string simulation_name = PROJECT_NAME " simulation CLI version";

int sc_main(int argc, char* argv[])
{
    auto start = chrono::steady_clock::now();
    auto absolutestart = start;
    QApplication MainApp(argc, argv);
    // Suppress warnings about some deprecated features
    sc_core::sc_report_handler::set_actions( "/IEEE_Std_1666/deprecated",
                                             sc_core::SC_DO_NOTHING );
//    std::cerr << "INFO: Elaborating "<< simulation_name<< endl;
    sc_set_time_resolution(SCTIME_RESOLUTION);
//    std::cerr << "INFO: Preparing EMPA V" << PROJECT_VERSION << " SystemC simulation" << endl;
   //  GUI_MODE = false;    // This is the text version of the application
   // This program is expected to be started with a file name and number of cores
    CheckArgumentList(argc, argv);	//?? There must be 3 args
   OBJECT_FILE_PRINTED = false;
    ostringstream Heading;
    Heading << GetAppName() << " SystemC simulator CLI version has started";
    string LogFileNamePrefix = "Log_"+GetFileNameRoot(string(argv[1]))+"_"+string(argv[2])+"_"; // Prepare log file name

   //      timer stopwatch;
   //      double elapsed = stopwatch.elapsed();

    OpenSystemFiles(string(argv[1]).c_str(), LogFileNamePrefix.c_str(), Heading.str());
    ostringstream StartString,NameString;
    NameString << "scSimulator "
        << GetAppName().c_str();
    StartString << NameString.str() << " has been started as:\n'";
    for(int i = 0; i<3; i++)
    {
      StartString <<argv[i] << " ";
    }
    StartString << "'";
    qInfo() << StartString.str().c_str();
    SetupSystemDirectories(NULL); // Establish system and user directories, with no main window

    char* pEnd;
    int NCores = strtol(argv[2], &pEnd, 10);
 //   if(NCores<0 || NCores>MAX_NUMBER_OF_GRIDPOINTS)
    {
 //     std::cerr << "No of cores was "<< NCores <<", is " << NUMBER_OF_CORES << " must be between 1 and " << NCores;
      NCores = NUMBER_OF_GRIDPOINTS;
    }
    ifstream infile(argv[1], ifstream::in);
/*    if(!infile.good())
    {
      LOG_CRITICAL("File '"<< argv[1] <<"' seems to be in error");
      return -2;
    }
    */
//    std::cerr << "Elapsed time for preparing simulation "
//            << chrono::duration_cast<chrono::milliseconds>(end - start).count()
//            << " msec" << endl;
//   start = chrono::steady_clock::now();
/*   if(!OBJECT_FILE_PRINTED)
     LOG_INFO("Starting to load object data from file '" << argv[1] << "'");*/
//   LOG_INFO(StartString.str().c_str());
    scSimulator* msSim = new scSimulator("scEMPA", argv[1], NCores, true);
//   Simulator* Sim = new Simulator(argv[1], NCores, true);
   //   std::cout << "Preparation time " <<  elapsed*1000 << "ms" << std::endl;
    int returnValue= 0;
       //Run the Simulation for "MAX_CLOCK_CYCLES nanosecnds"
    //    elapsed = stopwatch.elapsed();
//    std::cerr << "INFO: Entering EMPA V" << PROJECT_VERSION << " SystemC simulation" << endl;
        auto end = chrono::steady_clock::now();
    //end = chrono::steady_clock::now();
    std::cerr << "Elapsed time for setting up simulation "
            << chrono::duration_cast<chrono::milliseconds>(end - start).count()
            << " msec" << endl;
    start = chrono::steady_clock::now();
    sc_start(MAX_CLOCK_CYCLES,SC_NS);
    end = chrono::steady_clock::now();
//    std::cerr << "INFO: Exiting EMPA V" << PROJECT_VERSION << " SystemC simulation" << endl;
    if (not sc_end_of_simulation_invoked()) sc_stop(); //< invoke end_of_simulation
    std::cerr << "Elapsed time for performing simulation "
            << chrono::duration_cast<chrono::milliseconds>(end - start).count()
            << " msec" << endl;
    start = chrono::steady_clock::now();
    delete msSim;
    start = chrono::steady_clock::now();
    CloseSystemFiles(string(NameString.str().c_str()) + " simulator CLI version has terminated");
    end = chrono::steady_clock::now();
    std::cerr << "Elapsed time for cleaning up simulation "
            << chrono::duration_cast<chrono::microseconds>(end - start).count()
            << " µs" << endl;
    std::cerr << "Total elapsed time for simulation "
            << chrono::duration_cast<chrono::milliseconds>(end - absolutestart).count()
            << " msec" << endl;
    return returnValue;
}
#endif //0
