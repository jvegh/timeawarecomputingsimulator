/**
 * @file EMPAY86_GUI.cpp
 *
 * @brief This is the main file of the graphic version of the SystemC-based basic electronic simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */

#include "systemc.h"
#include <QApplication>

//#include "SimulatorWindowBase.h"
//#define DEBUG_PRINTS    // Print general debug messages
//#define MAKE_LOG_TRACING
#define MAKE_TIME_BENCHMARKING
// Those defines must be located before 'Macros.h", and are undefined in that file
//#include "Macros.h"

//#include <QtWidgets/QPushButton>
//#include <QtCore/QThread>
bool UNIT_TESTING = false;


/**
 * @file ScQtSimBasic_CLI.cpp
 *
 * @brief This is the main file of the text version of the SystemC-based basic electronic simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */
#include "systemc.h"
#include "Project.h"
#include "BasicConfig.h"
//#include "Stuff.h"
//#include "QStuff.h"
#include "QApplication"
#include <chrono>
#define MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroTimeBenchmarking.h"    // Must be after the define to have its effect

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scSimulator.h"

scqSimulator* TheSimulator;
string ListOfIniFiles;
/**
 * @brief This is the main file of the text version of the scSimulator: a one-bit adder
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */

int sc_main(int argc, char* argv[])
{
    ListOfIniFiles = INI_FILES;
    // The simulator makes self-timing
    TheSimulator = new scSimulator("BASIC/Adder", argc, argv);

    std::cerr << "Total time for setting up " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
    int returnValue= 0;
    sc_start(MAX_CLOCK_CYCLES,SC_NS);

    std::cerr << "Part time for setting running " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationPartTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Total time for running " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Part time for cleaning up " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationPartTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Total elapsed time for simulation " << TheSimulator->SimulationName_Get() << " was "
               << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
     delete TheSimulator;
    return returnValue;
}
