/**
 * @file ScQtTimeAwareMembrane_CLI.cpp.cpp
 *
 * @brief This is the main file of the command-line IF version of the SystemC-based
 * basic electronic simulator of the neuron membrane
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */

#include "systemc.h"
#include <QApplication>

bool UNIT_TESTING = false;
#include "systemc.h"
#include "Project.h"
#include "MembraneConfig.h"
#include "QApplication"
#include <chrono>
#define MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroTimeBenchmarking.h"    // Must be after the define to have its effect

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scMembraneSimulator.h"

scqSimulator* TheSimulator;
string ListOfIniFiles;

int sc_main(int argc, char* argv[])
{
    ListOfIniFiles = INI_FILES;
    // The simulator makes self-timing
    TheSimulator = new scMembraneSimulator("MEMBRANE", argc, argv);

    std::cerr << "Total time for setting up " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
    int returnValue= 0;
    sc_start(MAX_CLOCK_CYCLES,SC_NS);

    std::cerr << "Part time for setting running " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationPartTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Total time for running " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Part time for cleaning up " << TheSimulator->SimulationName_Get() << " was "
              << TheSimulator->SimulationPartTime_Get()/1000/1000 << " msecs" << std::endl;
    std::cerr << "Total elapsed time for simulation " << TheSimulator->SimulationName_Get() << " was "
               << TheSimulator->SimulationSumTime_Get()/1000/1000 << " msecs" << std::endl;
     delete TheSimulator;
    return returnValue;
}
