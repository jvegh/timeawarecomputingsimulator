/**
 * @file ScQtTimeAwareMembrane_GUI.cpp
 *
 * @brief This is the main file of the graphic (Qt) version
 * of the SystemC-based basic electronic simulator
 *
 * @param[in] argc Number of parameters
 * @param[in] argv parameters, #1 is the name of the command file
 * @return int The result of the execution
 */

#include "systemc.h"
#include <QApplication>
#include "SimulatorWindowMembrane.h"
#include "MembraneConfig.h"

bool UNIT_TESTING = false;
scqSimulator* TheSimulator;// This defines if running through a simulator
string ListOfIniFiles;
//extern void
//MyMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);


int sc_main( int argc, char **argv ) {
  ListOfIniFiles = INI_FILES;
  TheSimulator = new scqSimulator("scqSimulator",argc,argv); // Will install message handler, too
QApplication a( argc, argv );
SimulatorWindowMembrane w(argc,argv);

w.show();
return a.exec();
}

