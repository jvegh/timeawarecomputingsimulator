#include <gtest/gtest.h>


//#include "Config.h"
#include "systemc"
#include "AbstractNeurer.h"
#include "AbstractNeurerConnection.h"
#include "ActionPotential.h"
//#include "Stuff.h"
#include "GTestModule.h"
#include "Spike.h"
//#include "NeurerStates.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtSimNeurer_TB* ScQtSimNeurerTB;
extern Spike ClockSpike;
extern ActionPotentialVector ActionPotentialVector1;
/** @class	TestAbstractNeurer
 */


// A new test class  of these is created for each test
class TestAbstractNeurer : public testing::Test
{
public:

   virtual void SetUp()
   {
//      DEBUG_PRINT("TestAbstractNeurer started");
      Proc = ScQtSimNeurerTB->Processor_Get();
      Proc->Reset();
   }

   virtual void TearDown()
   {
//        DEBUG_PRINT("TestAbstractNeurer terminated");
   }
    NeurerProcessor* Proc;

};
 
// Test self-identity of TestAbstractNeurers after creating them
// The test assumes that the system sets up correctly,
// all gridpoints have the correct settings
// Check also if they are properly clusterized

TEST_F(TestAbstractNeurer, Identity)
{
  SC_GRIDPOINT_ID_TYPE CID = 2;
//  AbstractNeurer* VC = Proc->ByID_Get(CID,3);
//  SC_GRIDPOINT_ID_TYPE CID2 = CID+GRID_SIZE_Y;
  //AbstractNeurer* VC2 = Proc->ByID_Get(CID2,2);

  // In AbstractNeurers, the PC and memory handling may only be prepared
  #if USE_MODULE_NAMES
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_02%");
    VC->PC_Set(5);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_020x0005%");
    VC->Status_Set(0x105);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_020x0005[0x0105]%");
  #else
/*    EXPECT_EQ(VC->PrologText_Get(),"@  0.00: {0,4}=(7.N:N)%");
    VC->PC_Set(5);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00: {0,4}=(7.N:N)[0x0005]%");
    VC->Status_Set(0x105);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00: {0,4}=(7.N:N){0x0105}[0x0005]%");
*/  #endif // USE_MODULE_NAMES
 //!!   VC->Status_Set(0x0);
/*  EXPECT_TRUE(VC2->Regime_Get());
  VC2->Regime_Set(false);
  EXPECT_FALSE(VC2->Regime_Get());
*/
          /*!!
 EXPECT_NE((AbstractNeurer*) NULL, VC);
 SC_GRIDPOINT_MASK_TYPE CID_Mask= 1 << CID;
 EXPECT_EQ(CID, VC->ID_Get());
 EXPECT_EQ(CID_Mask, VC->IDMask_Get());
 SC_GRIDPOINT_MASK_TYPE CID_Mask2= 1 << CID2;
 EXPECT_EQ(CID2, VC2->ID_Get());
 EXPECT_EQ(CID_Mask2, VC2->IDMask_Get());
 // The virtual cores are created in unalllocated state, all cores
 EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000);
 EXPECT_EQ(Proc->AllocatedMask_Get(),0x00000000);
 EXPECT_EQ(Proc->PreAllocatedMask_Get(),0x00000000);
 
  // The cluster heads are stored in a special vector
  EXPECT_EQ(8,Proc->NumberOfClusters_Get()); // in 10x6 config, there are 8 Clusters
  VC = Proc->ClusterHead_Get(0);
  EXPECT_EQ(1,VC->X);
  EXPECT_EQ(9,VC->YPosition_Get());
  EXPECT_EQ(4,VC->Priority_Get());
          */
}

TEST_F(TestAbstractNeurer, States)
{
  SC_GRIDPOINT_ID_TYPE CID = 2;
  AbstractNeurer* AN = Proc->ByID_Get(CID,3);
  EXPECT_TRUE(AN->NeurerStateBit_Get(nsb_Resting)); // It is resting after creation
  AN->NeurerStateBit_Set(nsb_Charging,true);
  ActionPotential* AP = new ActionPotential( AN, ActionPotentialVector1);
 // int32_t FT = AP->FiringTime_Get();
//  int32_t RT = AP->RelaxTime_Get();
//  int32_t TT = AP->TotalTime_Get();
  for(int i = 0; i < 85; i++)
  {
  cerr << AP->MomentaryTime_Get()/1000. << "," << AP->MomentaryVoltage_Get()/1000. << '\n';
  AP->ActionPotential_method();
  }
}

/**
 * Tests if the core status bits are properly working
 */
TEST_F(TestAbstractNeurer, UtilityBits)
{
  Proc->Reset();
//  SC_GRIDPOINT_ID_TYPE CID = 5;
  // The virtual cores are created with helper bits clear
  /*

  EXPECT_TRUE(C0->AvailableBit_Get());  // The core is of course available
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_FALSE(C0->WaitBit_Get());
  // Test first is 'Wait' works for conventional commands
  C0->WaitBit_Set(true);
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->MetaBit_Set(true);
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->Reset();                      // Reset should clear the utility bits
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  // Test if 'Meta' works
  C0->MetaBit_Set(true);
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->WaitBit_Set(true);
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->WaitBit_Set(false);
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->MetaBit_Set(false);
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  */
}


/**
 * Tests the initial state of core status settings
 * Checking only bit setting and masks, but no functionality
 */
TEST_F(TestAbstractNeurer, AvailabilityMasks)
{
    Proc->Reset();
 //   SC_GRIDPOINT_ID_TYPE CID = 3;

  // The virtual cores are created in non-denied state
    /*
  EXPECT_FALSE(C0->DeniedBit_Get());
  C0->DeniedBit_Set(true);
  wait(SCTIME_GATE);
  EXPECT_TRUE(C0->DeniedBit_Get());
  EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000 | (1<<CID) );
  EXPECT_FALSE(C0->AvailableBit_Get());
  C0->DeniedBit_Set(false);
  wait(SCTIME_GATE);
  EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000 );
  EXPECT_FALSE(C0->DeniedBit_Get());
  EXPECT_TRUE(C0->AvailableBit_Get());

  EXPECT_EQ(CORE_DEFAULT_ADDRESS,C0->HThread_Get(0)->PC_Get());	// Also the cores are initialized to 0
  EXPECT_EQ(0,C0->PreAllocatedMask_Get());	// Nothing is preallocated for C0
  EXPECT_TRUE(C0->IsAvailable());	// C0 is initially available
//   Mem->Byte_Set(C0,0,0);
   EXPECT_EQ(0,Proc->UnavailableMask_Get()); // Nothing yet is unavailable
  EXPECT_EQ(0,Proc->DeniedMask_Get());	// Nothing disabled
  EXPECT_EQ(0,Proc->AllocatedMask_Get());	// Nothing allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  C0->AllocatedBit_Set();	// Imitate allocating C0
  EXPECT_FALSE(C0->IsAvailable());	// C0 is not any more available
  EXPECT_EQ(CMask,Proc->UnavailableMask_Get()); // Only C0 is unavailable
  EXPECT_EQ(0,Proc->DeniedMask_Get());	// Nothing disabled
  EXPECT_EQ(CMask,Proc->AllocatedMask_Get());	// Nothing allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  AbstractNeurer* C1 = Proc->ByID_Get(1);
  EXPECT_TRUE(C1->IsAvailable());	// C1 is available
  C1->DeniedBit_Set();	//
  C1->AllocatedBit_Set(); // Attempt to allocate a denied core
  EXPECT_FALSE(C1->IsAllocated());
  C1->PreAllocatedBit_Set(); // Attempt to pre-allocate a denied core
  EXPECT_FALSE(C1->IsPreAllocated());
  EXPECT_EQ( (1 << 1 | 1 << CID) ,Proc->UnavailableMask_Get()); // C0 and C1 are unavailable
  EXPECT_EQ(2,Proc->DeniedMask_Get());	// C1 denied
  EXPECT_EQ(1 << CID,Proc->AllocatedMask_Get());	// Only C0 is allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  C0->AllocatedBit_Set(false);	// Imitate DeAllocating C0
  EXPECT_EQ(0,Proc->AllocatedMask_Get());	// Only C0 is allocated
  C0->AllocatedBit_Set(true);	// Imitate Allocating C0
  wait(SCTIME_GATE);


  AbstractNeurer* C2 = Proc->ByID_Get(2);
  EXPECT_TRUE(C2->IsAvailable());	// C1 is available
  // The preallocation is tested locally and globally
  C2->PreAllocatedBit_Set();	// Preallocate C2
  wait(SCTIME_GATE);
  EXPECT_EQ(0x0, C2->PreAllocatedMask_Get()); // C2 has no preallocated cores, but
  EXPECT_EQ(0x4,Proc->PreAllocatedMask_Get());	// C2 preallocated
  EXPECT_EQ(8+4+2,Proc->UnavailableMask_Get()); // C0, C1 and C2 are unavailable
  EXPECT_EQ(2,Proc->DeniedMask_Get());	// C1 disabled
  EXPECT_EQ(1 << CID ,Proc->AllocatedMask_Get());	// C0 is allocated
  EXPECT_FALSE(C1->IsAvailable());	// C1 is not any more available
  EXPECT_FALSE(C2->IsAvailable());	// C2 is not any more available
  EXPECT_FALSE(Proc->ByID_Get(3)->IsAvailable());	// Core3 is still allocated with alias C0
  C2->PreAllocatedBit_Set(false);
  C1->DeniedBit_Set(false);	//
  C0->AllocatedBit_Set(false);
  wait(SCTIME_GATE);
  EXPECT_TRUE(C2->IsAvailable());	// C2 is free again
  EXPECT_TRUE(C0->IsAvailable());	// C1 is free again
  EXPECT_TRUE(C1->IsAvailable());	// C1 is free again
*/
 }


/**
 * Tests the core allocation/deallocation
 */
TEST_F(TestAbstractNeurer, Allocate)
{
    // Just Allocate/Deallocate
    Proc->Reset(); // Make sure everything is clear
    //AbstractNeurer* Core4 = Proc->ByID_Get(4,4);
//   EXPECT_EQ(0,Core4->BackLinkNumber_Get());  // Initially, there are no backlinked items
    //AbstractNeurer* Core3 = Proc->ByIDMask_Get(8,3);
    //AbstractNeurer* Core1 = Proc->ByID_Get(1,2);
    //AbstractNeurer* Core0 = Proc->ByIDMask_Get(1,0);
    /*
    EXPECT_TRUE(Core1->IsAvailable() ); // At the beginning it must be available
    EXPECT_EQ(0,Core4->ChildrenMask_Get());// At the beginning has no children
    Core4->ChildrenMask_Set(6);
    EXPECT_EQ(6,Core4->ChildrenMask_Get());// Check if properly set
    Core4->Reset();
    EXPECT_EQ((AbstractNeurer*)NULL,Core0->Parent_Get());   // After reset, it is 0
    Core0->Parent_Set(Core1);
    EXPECT_EQ(Core1,Core0->Parent_Get());   // The should be equal with the value set
    EXPECT_NE((AbstractNeurer*)NULL,Core1->AllocateFor(Core4)); // Now allocate Core1 for Core 4
    EXPECT_EQ(1 << 1,Core4->ChildrenMask_Get());    // after allocation, it is 2**Core1
    EXPECT_EQ((AbstractNeurer*)NULL,Core1->AllocateFor(Core4)); // Core cannot be allocated twice, for the same
    EXPECT_EQ(1 << 1 , Core4->ChildrenMask_Get()); // Allocated Core 1; no change
   EXPECT_NE((AbstractNeurer*)NULL, Core3->AllocateFor(Core4)); // Another core can be allocated for it
    EXPECT_FALSE(Core1->IsAvailable()); // Not any more available
    EXPECT_FALSE(Core3->IsAvailable()); // Not any more available
    EXPECT_EQ((AbstractNeurer*)NULL, Core4->AllocateFor(Core3)); // Cannot allocate a core with children
    EXPECT_TRUE(Core1->IsAllocated()); // This core is really allocated
    EXPECT_TRUE(Core1->IsAllocated()); // This core is really allocated
    EXPECT_FALSE(Core4->IsAllocated());// This is not allocated
    EXPECT_FALSE(Core0->IsAllocated());// This is not allocated
    EXPECT_EQ(0, Core0->ChildrenMask_Get());//
    EXPECT_EQ(1 << 1 | 1 << 3, Core4->ChildrenMask_Get()); // Allocated Core 1 and Core 3
    EXPECT_EQ(1 << 1 | 1 << 3, Proc->AllocatedMask_Get());
    EXPECT_EQ(1 << 1 | 1 << 3, Proc->UnavailableMask_Get());
    EXPECT_FALSE(Core4->Deallocate());// This is not allocated, but has children
    EXPECT_FALSE(Core0->Deallocate());// This is not allocated, and has no children
    EXPECT_TRUE(Core3->Deallocate()); // Was allocated, for Core 4
    EXPECT_TRUE(Core3->IsAvailable()); // Available again
    EXPECT_EQ(1 << 1 , Core4->ChildrenMask_Get()); // Allocated Core 1
    EXPECT_TRUE(Core1->Deallocate());   // Release Core1 from Core4
    EXPECT_EQ(0, Core4->ChildrenMask_Get()); // No more children
    Proc->Reset();
    // Test preallocation mechanism: a core can be preallocated
    // Shall remain preallocated through allocation/deallocation
    // Now allocate the preallocated core

    AbstractNeurer* Core2 = Proc->ByID_Get(2);
    EXPECT_TRUE(Core1->PreAllocateFor(Core0)); // A preallocated core must not be allocated
    EXPECT_TRUE(Core1->IsPreAllocated());
    EXPECT_TRUE(Core1->IsPreAllocatedFor(Core0));
    EXPECT_FALSE(Core2->IsPreAllocated());
    EXPECT_FALSE(Core2->IsPreAllocatedFor(Core0));

    EXPECT_FALSE(Core1->AllocateFor(Core2)); // NO other core can allocate the preallocated core
    EXPECT_TRUE(Core1->AllocateFor(Core0)); // The preallocated core can be allocated by the allocator core
    EXPECT_FALSE(Core1->AllocateFor(Core0)); // But, only once
    EXPECT_TRUE(Core1->Deallocate());
    EXPECT_TRUE(Core1->IsPreAllocated());
    Core1->PreAllocatedBit_Set(false); // The preallocation shall not be affected
    EXPECT_FALSE(Core1->IsPreAllocated());
    EXPECT_TRUE(Core2->PreAllocateFor(Core1)); // Is already preallocated
    EXPECT_EQ(Core2->IDMask_Get(), Proc->PreAllocatedMask_Get());	// Both C1 and C2 are allocated for C0
    EXPECT_TRUE(Core1->PreAllocateFor(Core0)); // Is already preallocated
    EXPECT_EQ(Core1->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // C1 and C2 are preallocated
    EXPECT_EQ(Core1->IDMask_Get(),Core0->PreAllocatedMask_Get());	// The preallocation is also visible at the parent core
    EXPECT_EQ(0,Proc->AllocatedMask_Get()); // They are not allocated
    EXPECT_EQ((AbstractNeurer*)NULL,Core1->AllocateFor(Core2)); // Preallocated cores cannot be allocated
    EXPECT_EQ(Core1->IDMask_Get()+Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // C1 and C2 are preallocated
    EXPECT_FALSE(Core1->Deallocate());  // The core was not allocated
    Core3->DeniedBit_Set(1);
    wait(SCTIME_GATE);
    EXPECT_FALSE(Core3->PreAllocateFor(Core0));
    EXPECT_FALSE(Core3->AllocateFor(Core0));
    EXPECT_EQ(Core3->IDMask_Get()|Core2->IDMask_Get()|Core1->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core3->DeniedBit_Set(0);

    Proc->Reset(); // Now clear preallocation at SV
    EXPECT_EQ(0,Proc->PreAllocatedMask_Get()); // No cores preallocated
    EXPECT_EQ(0,Proc->AllocatedMask_Get()); // No cores allocated
    EXPECT_EQ(0,Proc->UnavailableMask_Get()); // No cores unavailable
    EXPECT_FALSE(Core2->Deallocate()); // Core2 was not allocated

    // Now try allocation for the processor
    EXPECT_NE((AbstractNeurer*)NULL,Core4->AllocateFor((AbstractNeurer*)NULL));
    EXPECT_EQ(Core4->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    EXPECT_TRUE(Core3->PreAllocateFor((AbstractNeurer*)NULL));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core2->DeniedBit_Set(1);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    EXPECT_FALSE(Core2->PreAllocateFor((AbstractNeurer*)NULL));
    EXPECT_FALSE(Core2->AllocateFor((AbstractNeurer*)NULL));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core2->DeniedBit_Set(0);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are unavailable

    EXPECT_TRUE(Core2->PreAllocateFor((AbstractNeurer*)NULL));
    EXPECT_TRUE(Core2->AllocateFor((AbstractNeurer*)NULL));
    EXPECT_FALSE(Core2->PreAllocateFor((AbstractNeurer*)NULL));
    EXPECT_FALSE(Core4->PreAllocateFor(Core2));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // Three cores are unavailable
    Core2->Deallocate();
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are unavailable
    Core2->PreAllocatedBit_Set(0);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are  is unavailable
    EXPECT_FALSE(Core2->IsPreAllocated());
    EXPECT_TRUE(Core2->IsPreAllocatedFor((AbstractNeurer*)NULL)); // Not any more preallocated
    */
}

/**
* Tests if the cores can be accessed in the right way
*/
TEST_F(TestAbstractNeurer, scLogging)
{
   string CoreT;
   Proc->Reset();
   AbstractNeurer* C2 = Proc->ByID_Get(2,1);
   EXPECT_EQ(C2->StringOfDefaultName_Get(),"2/1");
   EXPECT_EQ(C2->StringOfClusterName_Get(),"{0,4}=(7.N:N)/1");
   EXPECT_EQ(C2->StringOfName_Get(),"2/1");
   EXPECT_EQ(C2->StringOfAliasName_Get(),"");
   EXPECT_TRUE(C2->StringOfAliasName_Set("MyNew")); // It was not set
   EXPECT_EQ(C2->StringOfAliasName_Get(),"MyNew");
   EXPECT_FALSE(C2->StringOfAliasName_Set("MyNew"));    // Set again, to the same; no change but invalid
   EXPECT_EQ(C2->StringOfAliasName_Get(),"MyNew");
   EXPECT_TRUE(C2->StringOfAliasName_Set("OtherNew"));    // Cannot again, to different; no change but invalid
   EXPECT_EQ(C2->StringOfAliasName_Get(),"OtherNew");

   AbstractNeurer* C0 = Proc->ByID_Get(0,0);
   CoreT = C0->StringOfClusterAddress_Get();    // The thread# should be not listed
   EXPECT_EQ(C0->StringOfDefaultName_Get(),"0");
   EXPECT_EQ("{0,0}=(7.H)",CoreT);	// Should describe Core#0
   EXPECT_EQ("{0,0}=(7.H)",C0->StringOfClusterAddress_Get());	// Should describe Core#0
/*   AbstractNeurer* C = Proc->ByID_Get(6);
   // Test QT ID
   string QTID;
   EXPECT_EQ("R6", C->HThread_Get(0)->QTID_Get()); // Empty string at the beginning
   C->HThread_Get(0)->QTID_Set("MyBest");
   EXPECT_EQ("MyBest", C->HThread_Get(0)->QTID_Get());
   // The gridpoint message FIFO must be present and empty
   EXPECT_EQ(0,C->GPMessagefifo->num_available());
   // Shall be tested by TestRouting
   */
}

/*  Given that the connections are "sc" objects, no new connections can be made during testing.
 *  The connections can be created in NeurerProcessor#CreateConnections(),
 *  which routine is called ONLY from the testbench
 */
TEST_F(TestAbstractNeurer, Connections)
{
    Proc->Reset(); // Make sure everything is clear
    EXPECT_EQ(0,Proc->AllocatedMask_Get());  // No physical core is allocated for a task
    AbstractNeurer *N1 = Proc->ByID_Get(2,1);
    AbstractNeurer *N2 = Proc->ByID_Get(3,2);
    AbstractNeurer *N3 = Proc->ByID_Get(4,3);
    // The followings are done in  NeurerProcessor::CreateConnections
//    N1->ConnectTo(N2);  // Connect the axon of N1 to a synaptic point of N2
//    N1->ConnectTo(N3);  // Connect the axon of N1 to a synaptic point of N3
//    N2->ConnectTo(N3);  // Connect the axon of N2 to a synaptic point of N3
    EXPECT_EQ(2,N1->AxonsNo_Get());
    EXPECT_EQ(0,N1->SynapsesNo_Get());
    EXPECT_EQ(2,N3->SynapsesNo_Get());
    scSynapticConnect* i0 = N3->FindSynapseFrom(N3);    // Has no synapse from itself
    scSynapticConnect* i1 = N3->FindSynapseFrom(N1);    // Has from N1
    EXPECT_EQ(nullptr,i0);  // No such synapse
    EXPECT_EQ((uint64_t)i1->Sender_Get(),(uint64_t)N1);
#ifdef _DEBUG
    EXPECT_DEATH(N3->SynapticConnection_Get(5),""); // Wrong index
#endif
    EXPECT_EQ(N3->SynapticConnection_Get(0)->Sender_Get(), N1);
    EXPECT_EQ(N3->SynapticConnection_Get(1)->Sender_Get(), N2);
#ifdef _DEBUG
    EXPECT_DEATH(N1->SynapticConnection_Get(5),""); // Wrong index
#endif
    EXPECT_EQ(N1->AxonalConnection_Get(0)->Receiver_Get(), N2);
    EXPECT_EQ(N1->AxonalConnection_Get(1)->Receiver_Get(), N3);
    sc_core::sc_time TT = sc_time_stamp();
    i1->ProcessSpike(ClockSpike);
     wait(i1->EVENT_SYNAPSIS.Integrate);
    TT = sc_time_stamp()-TT;
    wait(i1->EVENT_SYNAPSIS.Integrate);
    TT = sc_time_stamp()-TT;
    wait(i1->EVENT_SYNAPSIS.Integrate);
    TT = sc_time_stamp()-TT;
    // The integration time is 100 us
    EXPECT_EQ("100000.00",sc_time_to_nsec_Get(TT));
    EXPECT_EQ("   100",sc_time_to_usec_Get(TT));
#ifdef _DEBUG
    EXPECT_DEATH(N1->SynapticConnection_Get(2),""); // No 2nd synapse
    EXPECT_DEATH(N1->AxonalConnection_Get(2),"");
#endif
//        if (N1->AxonalConnection_Get(0))
    // Both have their synaptic and axonial connection lists the partner in the first place
         EXPECT_EQ(N2->ClusterAddress_Get().ToInt(),N1->AxonalConnection_Get(0)->Receiver_Get()->ClusterAddress_Get().ToInt());
//         N2->HThread_Get(2)->QTID_Set("This");
  //  EXPECT_EQ("",N2->StringOfNeurer_Get());
    //EXPECT_EQ("",N3->StringOfNeurer_Get());
//     else
  //      EXPECT_NE(nullptr,N1->AxonalConnection_Get(0));
//     if (N2->SynapticConnection_Get(0))
         EXPECT_EQ(N1->ClusterAddress_Get().ToInt(),N2->SynapticConnection_Get(0)->Sender_Get()->ClusterAddress_Get().ToInt());
//     else
  //      EXPECT_NE(nullptr,N2->SynapticConnection_Get(0));
/*
     EXPECT_EQ(0,N2->FindSynapseCA(N1->ClusterAddress_Get()));
     EXPECT_EQ(-1,N2->FindSynapseCA(N2->ClusterAddress_Get()));
     EXPECT_EQ(0,N2->FindSynapseCA(N1->ClusterAddress_Get()));
*/
}
