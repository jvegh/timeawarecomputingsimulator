#ifndef TESTBENCH_H
#define TESTBENCH_H
//#include "Config.h"
#include "NeurerProcessor.h"
#include "scClusterBus.h"
#include "scClusterBusMemorySlow.h"
//#include "Memory.h"
//#include "BusCtrl.h"
//#include "Trace.h"
extern scClusterBusMemorySlow *MainMemory;
SC_MODULE(ScQtSimNeurer_TB)
{
    // channels
 //   sc_clock C1 {"clk", 100, SC_PS};
  // Constructor
  SC_HAS_PROCESS(ScQtSimNeurer_TB);
    public:
  ScQtSimNeurer_TB( sc_module_name nm) : sc_module(nm)//,C1("C1")
  {
      SC_THREAD(test_thread);
      MainMemory = new scClusterBusMemorySlow("MainMemory", 0x00, FMAX_MEMORY_SIZE-1, 2);
//      MainMemory->clock(C1);    // The far memory uses clock; wait states measured in bus clock periods
      vector<scGridPoint*> Special;
      msProcessor = new NeurerProcessor("NeurerProc", Special,true);
//      msProcessor->clock(C1);
      msProcessor->CreateConnections();
  }

  void test_thread();
  NeurerProcessor* Processor_Get(void){return msProcessor;}
  int TestResult_Get(void){ return mTestResult;}
  protected:
        NeurerProcessor *
    msProcessor;
        scClusterBus* /// The inter-cluster bus
    msClusterBus;
         scClusterBusMemoryFast* 
    mClusterMemoryFast; ///< A register-like memory
        scClusterBusArbiter*
    mClusterBusArbiter; ///< Arbiter of the inter-cluster bus
        int
    mTestResult;  ///< Test result as returned by GTest
//  BusCtrl* Bus;
//  Trace *trace;
};

#endif
