#ifndef INITGTESTNeurer_H
#define INITGTESTNeurer_H

#include <systemc>
#include "testbench.h"

extern ScQtSimNeurer_TB* ScQtSimNeurerTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_Neurer)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_Neurer);
  GTestModule_Neurer(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtSimNeurerTB = new ScQtSimNeurer_TB("ScQtSimNeurer");
  }
protected:
};

#endif //INITGTESTNeurer_H
