#include <gtest/gtest.h>

//#define SUPPRESS_LOGGING    // Uncomment to suppress log messages of form LOG_INFO(x)
#define DEBUGGING       // Uncomment to suppress messages of form DEBUG_PRINT(x)
#include "Macros.h"
#undef DEBUGGING
#undef SUPPRESS_LOGGING

//#include "Config.h"
#include "systemc"
#include "scAbstractPatch.h"
#include "scAbstractMembrane.h"
#include "GTestModule_simple.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareMembrane_simple_TB* ScQtTimeAwareMembrane_simpleTB;

/** @class	GridPointTest
 */


// A new test class  of these is created for each test
class AbstractPatchTest : public testing::Test
{
public:

   virtual void SetUp()
   {
      DEBUG_PRINT("AbstractPatch started");
      Membrane = ScQtTimeAwareMembrane_simpleTB->Processor_Get();
      Membrane->Reset();
   }

   virtual void TearDown()
   {
        DEBUG_PRINT("GridPointTest terminated");
   }
    scAbstractMembrane* Membrane;

};
 
// Test self-identity of GridPoints after creating them
// The test assumes that the system sets up correctly,
// all gridpoints have the correct settings
// Check also if they are properly clusterized
// (for testing is is set explicitly, that one of the threads is active, usually thread 0)


TEST_F(AbstractPatchTest, Identity)
{
    // Recall that for AbstractMembrane and related stuff to clustering exists and used
    // The development is clearly a quick hack of scGridPoint
  SC_GRIDPOINT_ID_TYPE CID = 10;
  scAbstractPatch* AP = Membrane->ByID_Get(CID);
  EXPECT_EQ(AP->X,1);
  EXPECT_EQ(AP->Y,4);
  EXPECT_EQ(AP->State_Get(),mpt_NotInUse);    // It comes up as closed
  AP->State_Set(mpt_Closed);
  EXPECT_EQ(AP->State_Get(),mpt_Closed);    // It comes up as closed
  AP->State_Change();
  EXPECT_EQ(AP->State_Get(),mpt_Opened);    // The first use opens it
  AP->State_Change();
  EXPECT_EQ(AP->State_Get(),mpt_Inactivated); // Then it gets inactivated
  AP->State_Change();
  EXPECT_EQ(AP->State_Get(),mpt_Closed);  // After the refractory time, gets closed
  AP->State_Set(mpt_NotInUse);
}

TEST_F(AbstractPatchTest, Operation)
{
    // Test if timing operates properly, and in-use patches (and only they) receive the events
    SC_GRIDPOINT_ID_TYPE CID = 5;
    scAbstractPatch* AP = Membrane->ByID_Get(CID);
    EXPECT_EQ(AP->X,0);
    EXPECT_EQ(AP->Y,5);
    EXPECT_EQ(AP->State_Get(),mpt_NotInUse);    // It comes up as being not in use
    Membrane->EVENT_MEMBRANE.Timing.notify(SC_ZERO_TIME);   // Start membrane's timer
    wait(1,SC_NS);
    // Now try if in inactivated state AP does not process events
    EXPECT_EQ(AP->State_Get(),mpt_NotInUse);    // Be sure it did not change when not in use
    // Now events Open, Inactivate and Close will be sent out
    wait(Membrane->EVENT_MEMBRANE.Close);
    Membrane->EVENT_MEMBRANE.Timing.cancel();
    AP->EVENT_PATCH.PatchOn.notify();
    wait(SC_ZERO_TIME);
    EXPECT_EQ(AP->State_Get(),mpt_Closed);    // Now it is in use, but closed
    Membrane->EVENT_MEMBRANE.Timing.notify(SC_ZERO_TIME);   // Start membrane's timer
    wait(Membrane->EVENT_MEMBRANE.Open);
    EXPECT_EQ(AP->State_Get(),mpt_Opened);
    wait(Membrane->EVENT_MEMBRANE.Inactivate);
    EXPECT_EQ(AP->State_Get(),mpt_Inactivated);
    wait(Membrane->EVENT_MEMBRANE.Close);
    EXPECT_EQ(AP->State_Get(),mpt_Closed);
    Membrane->EVENT_MEMBRANE.Timing.cancel();

    //
}
