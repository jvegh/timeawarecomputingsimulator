#include <gtest/gtest.h>

//#define SUPPRESS_LOGGING    // Uncomment to suppress log messages of form LOG_INFO(x)
#define DEBUGGING       // Uncomment to suppress messages of form DEBUG_PRINT(x)
#include "Macros.h"
#undef DEBUGGING
#undef SUPPRESS_LOGGING

//#include "Config.h"
#include "systemc"
#include "scAbstractPatch.h"
#include "scAbstractMembrane.h"
#include "GTestModule_simple.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareMembrane_simple_TB* ScQtTimeAwareMembrane_simpleTB;

/** @class	MembraneTest
 */


// A new test class  of these is created for each test
class AbstractMembraneTest : public testing::Test
{
public:

   virtual void SetUp()
   {
      DEBUG_PRINT("AbstractMembrane started");
      Proc = ScQtTimeAwareMembrane_simpleTB->Processor_Get();
      Proc->Reset();
   }

   virtual void TearDown()
   {
        DEBUG_PRINT("AbstractMembrane terminated");
   }
    scAbstractMembrane* Proc;

};
 
// Test self-identity of GridPoints after creating them
// The test assumes that the system sets up correctly,
// all gridpoints have the correct settings
// Check also if they are properly clusterized
// (for testing is is set explicitly, that one of the threads is active, usually thread 0)
// The hillock patches do not have synaptic inputs.
// Rather they switch ion channels on (let external ions in)

int16_t Patch8x8[60] = {
    // Rows go from bottom up
   -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   /* row 5, All non-implemented */
   -1,  0,  0,  0,  0,  0,  0,  3,  3,  3,   /* row 4, with hillock */
   -1,  0, -1,  0,  0,  0,  0,  0,  3, -1,   /* row 3, sealed at the ends */
   -1,  0,  0,  0,  0,  0,  0,  0,  0, -1,   /* row 2, sealed at the ends */
   -1,  0,  0,  0,  0,  0,  0,  0,  0, -1,   /* row 1, sealed at the ends */
   -1, -1, -1, -1, -1, -1, -1, -1, -1, -1    /* row 0, All non-implemented */
};


void PopulateTest(scAbstractMembrane* Proc, int16_t Patch[])
{
    int16_t k = 0;
    // Create communication grid point objects
    for(int i = 0; i < 9; i++) //
    {
        for(int j = 0; j < 5; j++)
        {
            assert(!(i<0 || i>=10));   assert(!(j<0 || j>=6));
            int LinearAddress1 = (7-i)*8 +  j;
            scAbstractPatch* AP= Proc->ByIndex_Get(i,j);
                switch(Patch[k++])
                {
                case -1:
                    AP->State_Set(mpt_Denied);
                break;
                case 0:
                    AP->State_Set(mpt_Closed);
                break;
                case 3:
                break;
                default:
                break;
                }
       }
   }
 }

TEST_F(AbstractMembraneTest, Identity)
{
    // Recall that for AbstractMembrane and related stuff to clustering exists and used
    // The development is clearly a quick hack of scGridPoint
    PopulateTest(Proc, Patch8x8);
  SC_GRIDPOINT_ID_TYPE CID = 10;
  scAbstractPatch* AP = Proc->ByPosition_Get(0,0);
  EXPECT_EQ(AP->State_Get(),mpt_Denied);
  AP = Proc->ByPosition_Get(3,3);
  EXPECT_EQ(AP->State_Get(),mpt_Closed);

}

TEST_F(AbstractMembraneTest, Operation)
{
    PopulateTest(Proc, Patch8x8);
    scAbstractPatch* AP64 = Proc->ByPosition_Get(1,9);

    // This patch may have up to two "good" neighbors, but both of them are closed
    ClusterNeighbor N = cm_North;
    int TotalNeighbors=0;
    while(AP64->NextActiveNeighbor_Get(N))
        TotalNeighbors++;   // Count 'good' neigbors
    EXPECT_EQ(0,TotalNeighbors);
    TotalNeighbors=0;
    N = cm_North;
    scAbstractPatch* AP24 = Proc->ByPosition_Get(2,4);
    // This patch may have up to 5 "good" neighbors, but all of them are closed
    // one of them is not implemented
    while(AP24->NextActiveNeighbor_Get(N))
        TotalNeighbors++;   // Count 'good' neigbors
    EXPECT_EQ(0,TotalNeighbors);
    N = cm_North;
    EXPECT_EQ(mpt_Denied, AP24->State_Get());
    N = cm_NE;
    EXPECT_EQ(mpt_Inactivated, AP24->State_Get());
}
