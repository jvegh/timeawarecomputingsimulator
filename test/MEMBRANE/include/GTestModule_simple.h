#ifndef INITGTESTMembrane_simple_H
#define INITGTESTMembrane_simple_H

#include <systemc>
#include "testbench_simple.h"

extern ScQtTimeAwareMembrane_simple_TB* ScQtTimeAwareMembrane_simpleTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_simple)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_simple);
  GTestModule_simple(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtTimeAwareMembrane_simpleTB = new ScQtTimeAwareMembrane_simple_TB("Membrane_simple");
  }
protected:
};

#endif // INITGTESTMembrane_simple_H
