#ifndef TESTBENCH_H
#define TESTBENCH_H
//#include "Config.h"
//#include "BusCtrl.h"
//#include "Trace.h"
//extern scClusterBusMemorySlow *MainMemory;
#include "scAbstractMembrane.h"

SC_MODULE(ScQtTimeAwareMembrane_simple_TB)
{
    // channels
//    sc_clock C1 {"clk", 100, SC_PS};
  // Constructor
  SC_HAS_PROCESS(ScQtTimeAwareMembrane_simple_TB);
    public:
  ScQtTimeAwareMembrane_simple_TB( sc_module_name nm) : sc_module(nm)
  {
      SC_THREAD(test_thread);
    msProcessor = new scAbstractMembrane("scMembrane", NULL, true);
//      MainMemory = new scClusterBusMemorySlow("MainMemory", 0x00, FMAX_MEMORY_SIZE-1, 2);
//      MainMemory->clock(C1);    // The far memory uses clock; wait states measured in bus clock periods
/*      vector<scGridPoint*> Special;
      msProcessor = new scProcessor("scProc", Special,true);
      msProcessor->clock(C1); */
  }

  void test_thread();
  scAbstractMembrane* Processor_Get(void){return msProcessor;}
  protected:
        scAbstractMembrane *
    msProcessor;
};

#endif
