/** @file ScQtTimeAwareMembrane_simple_gtest.cpp
 *  @brief The main file for the test program for testing
 * SystemC-implemented modules using Google-test
 * This main program assumes nothing specific, just SystemC modules
 * and shows how tose modules can be tested used Google-Test
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
// All systemc modules should include systemc.h header file
#include <systemc.h>
#include "gtest/gtest.h"
#include <chrono>
// Do not log execution in unit testing  mode
//#define SUPPRESS_LOGGING    // In unit testing mode
//#define DEBUGGING       // In debug mode
//#include "Config.h"
//#include "Macros.h"
//#undef SUPPRESS_LOGGING

#define MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroTimeBenchmarking.h"    // Must be after the define to have its effect
#define SC_MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroScTimeBenchmarking.h"    // Must be after the define to have its effect

#include "Project.h"
#include "HWConfig.h"
#include "MembraneConfig.h"
#include "include/GTestModule_simple.h"
unsigned errors = 0;
ScQtTimeAwareMembrane_simple_TB* ScQtTimeAwareMembrane_simpleTB;

bool UNIT_TESTING = true;	// Whether in course of unit testing
//extern bool OBJECT_FILE_PRINTED;

using namespace std; using namespace sc_core;
string simulation_name = "Test ScQtTimeAwareMembrane units";
string ListOfIniFiles;
//#define MAX_CLOCK_CYCLES 1000

int sc_main(int argc, char* argv[]) {
    // turn off warnings about IEEE 1666 deprecated feature
//    OBJECT_FILE_PRINTED = true;
    ListOfIniFiles = INI_FILES;
    // We rely of the default clearing of the value
    chrono::steady_clock::time_point t =chrono::steady_clock::now(),
            absolutestart, start;
    std::chrono::duration< int64_t, nano> end,x,s=(std::chrono::duration< int64_t, nano>)0;
    BENCHMARK_TIME_RESET(&t,&x,&s);
    absolutestart = t; start = absolutestart;
    BENCHMARK_TIME_BEGIN(&t,&x);
    BENCHMARK_TIME_END(&t,&x,&s);
    std::cerr << x.count() <<"," << s.count() << std::endl;
/*    auto start = chrono::steady_clock::now();

    sc_core::sc_time st,sx,ss;
    SC_BENCHMARK_TIME_RESET(&st,&sx,&ss);
    st = sc_time(10,SC_NS);
    ss = sc_time(100,SC_NS);
    SC_BENCHMARK_TIME_BEGIN(&st,&sx);
    sx = sx + sc_time(10,SC_NS);
    SC_BENCHMARK_TIME_END(&st,&sx,&ss);
    std::cerr << st <<","<< sx <<"," << ss << std::endl;
    auto absolutestart = start;
*/    sc_core::sc_report_handler::set_actions( "/IEEE_Std_1666/deprecated",
                                               sc_core::SC_DO_NOTHING );
    std::cerr << "INFO: Preparing " << PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC unit testing" << endl;
    std::cerr << "INFO: Elaborating "<< simulation_name<< endl;
      testing::InitGoogleTest(&argc, argv);

//    sc_set_time_resolution(SCTIME_RESOLUTION);
    //Do whatever setup here you will need for your tests here
    //!! all SC-related object and connections must be established befor calling sc_start

      // Begin testing
//    auto end = chrono::steady_clock::now();
    std::cerr << "INFO: Entering "<< PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC unit testing" << endl;
    BENCHMARK_TIME_BEGIN(&t,&x);
//    auto end=x-t;
    GTestModule_simple InitGTest("gtest_simple"); // Set up SystemC related testing
    int returnValue= 0;
    //Run the Simulation for "MAX_CLOCK_CYCLES  nanosecnds"
//    end = chrono::steady_clock::now();
    BENCHMARK_TIME_END(&t,&s,&s);

    std::cerr  << "Elapsed for setting up testing : "
            << s.count()
            << " msec" << endl;
    BENCHMARK_TIME_BEGIN(&t,&s);
    sc_start(MAX_CLOCK_CYCLES ,SC_US);
//    start = chrono::steady_clock::now();
//    std::cout << "Execution time " <<  elapsed*1000 << "ms" << std::endl;
    // Return here when no more events remained
        if (not sc_end_of_simulation_invoked()) sc_stop(); //< invoke end_of_simulation
    BENCHMARK_TIME_END(&t,&end,&s);
        std::cerr  << "INFO: Exiting " << PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC unit testing took " << end.count()/1000
                   << " msec" << endl;
    return(returnValue);
}

