#include <gtest/gtest.h>


//#define SUPPRESS_LOGGING    // Uncomment to suppress log messages of form LOG_INFO(x)
#define DEBUGGING       // Uncomment to suppress messages of form DEBUG_PRINT(x)
#include "Macros.h"
#undef DEBUGGING
#undef SUPPRESS_LOGGING

//#include "Config.h"
#include "systemc"
#include "scGridPoint.h"
#include "Stuff.h"
//#include "Processor.h"
#include "testbench_bus.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareBase_bus_TB* ScQtTimeAwareBase_busTB;
extern string DebugRoute;
//extern Memory *MainMemory;
/** @class	TestRouting
 */


// A new test class  of these is created for each test
class TestRouting : public testing::Test
{
public:

   virtual void SetUp()
   {
      DEBUG_PRINT("TestRouting started");
      Proc = ScQtTimeAwareBase_busTB->Processor_Get();
      Proc->Reset();
   }

   virtual void TearDown()
   {
        DEBUG_PRINT("GridPointTest terminated");
   }
    scProcessor* Proc;

};
 
// Test routing between scGridPoints via direct messaging

TEST_F(TestRouting, DirectRMessage)
{
    scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
    scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

    // Message to a non-existent point
    scGridPoint *CM1 = Proc->ByPosition_Get(-1,0);
    EXPECT_DEATH(C0->CreateRegisterMessageTo(CM1, 0xb),"");

    // Now test 'R' message to an external member
//    scIGPMessage* Message0 = C0->CreateRegisterMessageTo(C0, 0xa);

    scIGPMessage* Message2 = C0->CreateRegisterMessageTo(C2, 0xa);

    ClusterAddress_t CAD = Message2->DestinationAddress_Get();
    ClusterAddress_t CAS = Message2->ReplyToAddress_Get();
    scGridPoint *Dest =  Proc->ByClusterAddress_Get(CAD);
    scGridPoint *Source =  Proc->ByClusterAddress_Get(CAS);
    EXPECT_EQ(Dest,C2);
    EXPECT_EQ(Source,C0);
    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (0,4), a phantom, attached to the 7th cluster as external
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{0,4}=(7.N:N)");
     // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C2));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C2));


/* //   Message2->DelayTime_Set(10);
 //   C0->RouteMessage(Message2);
    Message0->DelayTime_Set(1);
    C0->RouteMessage(Message0);
    Message0->DelayTime_Set(20);
   C0->RouteMessage(Message0);
 //   Message2->DelayTime_Set(00);
*/    C0->RouteMessage(Message2);
    wait(C2->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//    wait(100,SC_NS);
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT to {0,2}=(7.N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT to {0,4}=(7.N:N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%RCVD DIRECT <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELIVERED 'R' <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELETED <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n");

    // The same message in the reverse direction
    scIGPMessage* Message2R = C2->CreateRegisterMessageTo(C0, 0xa);
    C2->RouteMessage(Message2R);
    wait(C0->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//    wait(100,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%PROXY {0,2}=(7.N) inserted: <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT to {0,2}=(7.N): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT to {0,0}=(7.H): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%RCVD DIRECT <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%DELIVERED 'R' <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%DELETED <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n");

    // Now test 'R' message to an internal member
    scGridPoint *C1 = Proc->ByClusterMember_Get(7,cm_NE);  // internal member of the incomplete cluster
    scIGPMessage* Message1 = C0->CreateRegisterMessageTo(C1, 0xa);
    CAD = Message1->DestinationAddress_Get();
    CAS = Message1->ReplyToAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    C0->RouteMessage(Message1);
//    wait(1000,SC_NS);
    cerr << sc_time_stamp() << "Waiting for Msg_Reg_Received\n";
    wait(C1->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT to {1,1}=(7.NE): <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%RCVD DIRECT <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%DELIVERED 'R' <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%DELETED <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n");

    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (1,1), internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,1}=(7.NE)");
    // This is delivered via direct message
    EXPECT_TRUE(C0->IsNeighborOf(C1));
    EXPECT_FALSE(C0->Is2ndNeighborOf(C1));

    // Now test 'R' message to a neighbor, that is an internal member of another cluster
    scGridPoint *C3 = Proc->ByClusterMember_Get(1,cm_SW);  // internal member of the incomplete cluster
    scIGPMessage* Message3 = C0->CreateRegisterMessageTo(C3, 0xe);
    CAD = Message3->DestinationAddress_Get();
    CAS = Message3->ReplyToAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    // The source is the point {0,0}, the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point {1,3}, internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,3}=(1.SW)");
    // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C3));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C3));
    C0->RouteMessage(Message3);
    wait(C3->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT to {0,2}=(7.N): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT to {1,3}=(1.SW): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%RCVD DIRECT <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%DELIVERED 'R' <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%DELETED <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");

    // Now send message from a phantom member to another phantom member
    scGridPoint *C4 = Proc->ByPosition_Get(0,4);  // internal member of an incomplete cluster
    scGridPoint *C6 = Proc->ByPosition_Get(0,6);  // internal member of another incomplete cluster
    // However, they are neighbors
    scIGPMessage* Message6 = C4->CreateRegisterMessageTo(C6, 0xe);
    C4->RouteMessage(Message6);

    wait(C6->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//    wait(100,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT to {0,6}=(1.NW:NW): <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%RCVD DIRECT <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%DELIVERED 'R' <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%DELETED <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");

/*    MessageFeatures_t F = Message6->FeatureWord_Get();
    F.Time = 10;
    Message6->FeatureWord_Set(F);
    C4->RouteMessage(Message6);
*/
}


TEST_F(TestRouting, ClusterRMessage)
{
   // Now test 'R' message to a head of another cluster
    scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
   scGridPoint *C4 = Proc->ByClusterMember_Get(1,cm_Head);  // internal member of the incomplete cluster
   scIGPMessage* Message4 = C0->CreateRegisterMessageTo(C4, 0x3);
   ClusterAddress_t CAD = Message4->DestinationAddress_Get();
   ClusterAddress_t CAS = Message4->ReplyToAddress_Get();
   scGridPoint *Dest =  Proc->ByClusterAddress_Get(CAD);
   scGridPoint *Source =  Proc->ByClusterAddress_Get(CAS);
   CAD = Message4->DestinationAddress_Get();
   CAS = Message4->ReplyToAddress_Get();
   Dest =  Proc->ByClusterAddress_Get(CAD);
   Source =  Proc->ByClusterAddress_Get(CAS);
   // The source is the point (0,0), the head of the 7th cluster
   EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
   // The destination is point (2,4), internal member the 2nd cluster
   EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{2,4}=(1.H)");
   // This is delivered via direct message
   EXPECT_FALSE(C0->IsNeighborOf(C4));
   EXPECT_FALSE(C0->Is2ndNeighborOf(C4));
   C0->RouteMessage(Message4);
   wait(C4->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//   wait(10,SC_NS);   // Must wait until the message passed by the buffers
   EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{0,0}=(7.H)%ROUTING <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{0,0}=(7.H)%SENT CLUSTER <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{0,0}=(7.H)%SENT CLUSTER <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{2,4}=(1.H)%RECEIVED CLUSTER <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{2,4}=(1.H)%DELIVERED 'R' <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n{2,4}=(1.H)%DELETED <{0,0}=(7.H) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0>\n");

   // Now send a message from a phantom member to another cluster
   scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster
   Message4 = C2->CreateRegisterMessageTo(C4, 0xb);
   C2->RouteMessage(Message4);
   wait(C4->EVENT_GRID.Msg_Reg_Received);  // Now the message was delivered
//   wait(10,SC_NS);   // Must wait until the message passed by the buffers
   EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%PROXY {1,5}=(1.NW) inserted: <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT to {1,5}=(1.NW): <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{1,5}=(1.NW)%RCVD DIRECT <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{1,5}=(1.NW)%ROUTING <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{1,5}=(1.NW)%SENT DIRECT to {2,4}=(1.H): <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{2,4}=(1.H)%RCVD DIRECT <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{2,4}=(1.H)%DELIVERED 'R' <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n{2,4}=(1.H)%DELETED <{0,4}=(7.N:N) => {2,4}=(1.H) Reg%R[0]=0x0,R[1]=0x0,R[3]=0x0>\n");
}

TEST_F(TestRouting, MessageTiming)
{

/*    scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
    scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

    scIGPMessage* Message2   =C2->CreateRegisterMessageTo(C0,0X3);  // Wants to send2 registers

    C2->RouteMessage(Message2);
*/

   scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
//??    scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

    // Message to a non-existent point; must die
    scGridPoint *CM1 = Proc->ByPosition_Get(-1,0);
    EXPECT_DEATH(C0->CreateRegisterMessageTo(CM1, 0xb),"");
         scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

        // Now test 'R' message to an external member

        scIGPMessage* Message2 = C0->CreateRegisterMessageTo(C2, 0xa);

        ClusterAddress_t CAD = Message2->DestinationAddress_Get();
        ClusterAddress_t CAS = Message2->ReplyToAddress_Get();
        scGridPoint *Dest =  Proc->ByClusterAddress_Get(CAD);
        scGridPoint *Source =  Proc->ByClusterAddress_Get(CAS);
        EXPECT_EQ(Dest,C2);
        EXPECT_EQ(Source,C0);
        // The source is the point (0,0), the head of the 7th cluster
        EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");


    // Now test 'R' message to an external member

    EXPECT_EQ(Dest,C2);
    EXPECT_EQ(Source,C0);
    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (0,4), a phantom, attached to the 7th cluster as external
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{0,4}=(7.N:N)");
     // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C2));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C2));
    sc_time T0 = sc_time_stamp();
    Message2->DelayTime_Set(10);
    C0->RouteMessage(Message2);
    cerr << sc_time_to_nsec_Get(sc_time_stamp()-T0) << " " << Message2->Length_Get() << "\n";
    wait(10,SC_NS);  // Must wait until the message passed by the buffers
    EXPECT_EQ("{0,0}=(7.H)%CREATED <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT to {0,2}=(7.N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT to {0,4}=(7.N:N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%RCVD DIRECT <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELAYING by 10 <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%RCVD delayed by 10 <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELIVERED 'R' <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELETED <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n",DebugRoute);
/*
    // The same message in the reverse direction
    scIGPMessage* Message2R = C2->CreateRegisterMessageTo(C0, 0xa);
    C2->RouteMessage(Message2R);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%PROXY {0,2}=(7.N) inserted: <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT message to {0,2}=(7.N): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT message to {0,0}=(7.H): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROCESSING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%DELETED message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n");

    // Now test 'R' message to an internal member
    scGridPoint *C1 = Proc->ByClusterMember_Get(7,cm_NE);  // internal member of the incomplete cluster
    scIGPMessage* Message1 = C0->CreateRegisterMessageTo(C1, 0xa);
    CAD = Message1->DestinationAddress_Get();
    CAS = Message1->ReplyAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    C0->RouteMessage(Message1);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT message to {1,1}=(7.NE): <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%RCVD DIRECT message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%PROCESSING message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%DELETED message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n");

    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (1,1), internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,1}=(7.NE)");
    // This is delivered via direct message
    EXPECT_TRUE(C0->IsNeighborOf(C1));
    EXPECT_FALSE(C0->Is2ndNeighborOf(C1));

    // Now test 'R' message to a neighbor, that is an internal member of another cluster
    scGridPoint *C3 = Proc->ByClusterMember_Get(1,cm_SW);  // internal member of the incomplete cluster
    scIGPMessage* Message3 = C0->CreateRegisterMessageTo(C3, 0xe);
    CAD = Message3->DestinationAddress_Get();
    CAS = Message3->ReplyAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    // The source is the point {0,0}, the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point {1,3}, internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,3}=(1.SW)");
    // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C3));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C3));
    C0->RouteMessage(Message3);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT message to {0,2}=(7.N): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT message to {1,3}=(1.SW): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%RCVD DIRECT message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%PROCESSING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%DELETED message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");

    // Now send message fro a phantom member to another phantom member
    scGridPoint *C4 = Proc->ByPosition_Get(0,4);  // internal member of an incomplete cluster
    scGridPoint *C6 = Proc->ByPosition_Get(0,6);  // internal member of another incomplete cluster
    // However, they are neighbors
    scIGPMessage* Message6 = C4->CreateRegisterMessageTo(C6, 0xe);
    C4->RouteMessage(Message6);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT message to {0,6}=(1.NW:NW): <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%PROCESSING message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%DELETED message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");
*/}

