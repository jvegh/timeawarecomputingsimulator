#include <gtest/gtest.h>

#define SUPPRESS_LOGGING    // Suppress log messages
//#define DEBUGGING       // Uncomment to debug this unit
#include "Macros.h"

#include "scProcessor.h"
#include "scIGPMessage.h"
#include "scIGPCB.h"
#include "testbench_bus.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareBase_bus_TB* ScQtTimeAwareBase_busTB;

// These name arrays are needed for debugging macros
extern NamedEnum_struct ClusterMembers[8];
extern NamedEnum_struct MessageTypes[];

// A new test class  of these is created for each test
class TestTopology : public testing::Test
{
public:

   virtual void SetUp()
   {
//        DEBUG_PRINT("TestTopology started");
        Proc = ScQtTimeAwareBase_busTB->Processor_Get();
        Proc->Reset();
   }

   virtual void TearDown()
   {
//        DEBUG_PRINT("TestTopology terminated");
   }
    scProcessor* Proc;
};
 

/** (for testing is is set explicitly, that one of the threads is active, usially thread 0)
 * */


/**
 * Static tests of the topology
 */

TEST_F(TestTopology, AbstractTopology)
{
    // Verify the default GridPoint creation
    GridPoint GG;
    EXPECT_EQ(GG.ClusterStatus_Get(), cs_NONE);
    EXPECT_EQ(GG.X, 0);
    EXPECT_EQ(GG.X, 0);
    EXPECT_EQ(GG.mClusterAddress.Cluster, (unsigned short int)(MAX_CLUSTERS_LIMIT));

#ifdef _DEBUG
    EXPECT_DEATH(Proc->ByIndex_Get(1,9),"");   // No such point
#endif
    scGridPoint* G = Proc->ByPosition_Get(1,9); // This is the correct form
    EXPECT_EQ(G->ClusterStatus_Get(), cs_Head);
    EXPECT_EQ(G->X, 1);
    EXPECT_EQ(G->Y, (9 - G->X%2)/2);
    EXPECT_EQ(G->mClusterAddress.Cluster, 0);
    EXPECT_EQ(G->ClusterStatus_Get(), cs_Head);
    // The 1st cluster head is created at grid ccordinates(1,9)
    EXPECT_EQ(1,Proc->ClusterHead_Get(0)->X);
//    EXPECT_EQ(9,Proc->YPosition_Get(Proc->ClusterHead_Get(0)));
    EXPECT_EQ(9,Proc->ClusterHead_Get(0)->YPosition_Get());

    // Check if the neighbors are calculated correctly
    GridPoint *GP = Proc->ByClusterMember_Get(1,cm_Head); // This is the second cluster
    EXPECT_EQ(GP->X, 2);
    EXPECT_EQ(GP->Y, 2);
    GridPoint *GPN = Proc->ByClusterMember_Get(1,cm_North); // This is the N neighbor
    EXPECT_EQ(GPN->X, 2);
    EXPECT_EQ(GPN->Y, 3);
    GridPoint *GPS = Proc->ByClusterMember_Get(1,cm_South); // This is the S neighbor
    EXPECT_EQ(GPS->X, 2);
    EXPECT_EQ(GPS->Y, 1);
    GridPoint *GPNE = Proc->ByClusterMember_Get(1,cm_NE); // This is the NE neighbor
    EXPECT_EQ(GPNE->X, 3);
    EXPECT_EQ(GPNE->Y, 2);
    GridPoint *GPSE = Proc->ByClusterMember_Get(1,cm_SE); // This is the SE neighbor
    EXPECT_EQ(GPSE->X, 3);
    EXPECT_EQ(GPSE->Y, 1);
    GridPoint *GPNW = Proc->ByClusterMember_Get(1,cm_NW); // This is the NW neighbor
    EXPECT_EQ(GPNW->X, 1);
    EXPECT_EQ(GPNW->Y, 2);
    GridPoint *GPSW = Proc->ByClusterMember_Get(1,cm_SW); // This is the SW neighbor
    EXPECT_EQ(GPSW->X, 1);
    EXPECT_EQ(GPSW->Y, 1);
    // Now check neighborships
    EXPECT_FALSE(GP->IsNeighborOf(GP));    // I am not my own neigbor
    EXPECT_TRUE(GP->IsTheSameAs(GP));    // But I am identical with me
    EXPECT_TRUE(GP->IsNeighborOf(GPN));
    EXPECT_TRUE(GP->IsNeighborOf(GPS));
    EXPECT_TRUE(GP->IsNeighborOf(GPNE));
    EXPECT_TRUE(GP->IsNeighborOf(GPSE));
    EXPECT_TRUE(GP->IsNeighborOf(GPNW));
    EXPECT_TRUE(GP->IsNeighborOf(GPSW));
    EXPECT_TRUE(GPN->IsNeighborOf(GPNE));
    EXPECT_TRUE(GP->IsNeighborOf(GPNE));
    EXPECT_TRUE(GPNE->IsNeighborOf(GPN));
    EXPECT_FALSE(GPN->IsNeighborOf(GPSE));
    EXPECT_FALSE(GPN->IsNeighborOf(GPSW));
    EXPECT_FALSE(GPNE->IsNeighborOf(Proc->ByClusterMember_Get(0,cm_NE)));


    // Verifies mainly whether typecasting works properly
//    scGridPoint *sGP1 = dynamic_cast<scGridPoint*>(GP); //sGP->GetAPoint(1,1);
    EXPECT_EQ(GRID_SIZE_X*GRID_SIZE_Y, Proc->NumberOfGridPoints_Get());
#ifdef _DEBUG
    EXPECT_DEATH(Proc->Cluster_Create(nullptr),"");
    EXPECT_DEATH(Proc->Cluster_Create(-1,1),"");
    EXPECT_DEATH(Proc->ByIndex_Get(-1,1),"");
    EXPECT_DEATH(Proc->ByClusterMember_Get(-1),"");
    EXPECT_DEATH(Proc->ByClusterMember_Get(1,cm_Broadcast),"");
#endif
    // Check cluster #1
    EXPECT_EQ(2, Proc->ByClusterMember_Get(1,cm_Head)->X);
    EXPECT_EQ(2, Proc->ByClusterMember_Get(1,cm_Head)->Y);
    EXPECT_EQ(1, Proc->ByClusterMember_Get(1,cm_South)->Y);
#ifdef _DEBUG
    EXPECT_DEATH(Proc->ByPosition_Get(2,9),"");
    EXPECT_DEATH(Proc->ClusterAddress_Get(nullptr),"");
#endif
    // Has trouble with the derived class, but works OK
//    EXPECT_DEATH(((AbstractTopology*)Topology)->ClusterHead_Get(-1),"");
    EXPECT_EQ(0,Proc->ClusterHead_Get(0)->ClusterAddress_Get().Cluster);
    EXPECT_EQ(1, Proc->ByClusterMember_Get(0)->X);

    // 8 clusters are pre-created
    EXPECT_EQ(8, Proc->NumberOfClusters_Get());
    // Test if both illegal grid points and legal ones are correctly handled
#ifdef _DEBUG
    EXPECT_DEATH(Proc->ByIndex_Get(GRID_SIZE_X,0),"");
    EXPECT_DEATH(Proc->ByIndex_Get(-1,2),"");
#endif
    EXPECT_NE((scGridPoint*)NULL,Proc->ByIndex_Get(1,2));

    // Test neighborship
#ifdef _DEBUG
    EXPECT_DEATH(Proc->ByIndex_Get(-1,2)->IsNeighborOf(Proc->ByIndex_Get(1,2)),""); // One is outside
#endif
    EXPECT_FALSE(Proc->ByIndex_Get(1,1)->IsNeighborOf(Proc->ByIndex_Get(2,4))); // Both inside, but not neighbors
    EXPECT_TRUE(Proc->ByPosition_Get(0,0)->IsNeighborOf(Proc->ByPosition_Get(1,1))); // Edge + inside

    // Test Cluster State, i.e. that the neighbors are correctly labeled
#ifdef _DEBUG
    EXPECT_DEATH(Proc->ClusterStatus_Get(Proc->ByIndex_Get(-1,2), cm_Head),"");
#endif
    EXPECT_EQ(cs_Error, Proc->ClusterStatus_Get(Proc->ByIndex_Get(0,0), cm_SW));
    EXPECT_EQ(cs_Head, Proc->ClusterStatus_Get(Proc->ByIndex_Get(0,0), cm_Head));

    // Create communication grid point objects; works the same way as Populate()
   scGridPoint* C; int index=0;
   for(int i = 0; i < GRID_SIZE_X; i++) // Twice because of the hexagonal grid
   {
       for(int j = 0; j < GRID_SIZE_Y; j++)
       {
           // Test first the even column
           //int LinearAddress1 = Proc->LinearAddressFromCoordinates_Get(i,j);
           C = Proc->ByIndex_Get(i,j);   // Get a point from the grid
           EXPECT_NE(C, (scGridPoint*) NULL );
           EXPECT_EQ(C->X,i);
           EXPECT_EQ(C->Y,j);
           for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
           {
                EXPECT_EQ(H, C->HThread_Get(H)->ID_Get());
           }

           index++;
       }
   }
   EXPECT_EQ(index,GRID_SIZE_X*GRID_SIZE_Y);

}

TEST_F(TestTopology, Neighborship)
{
    // Test the 1st-order neighbors
    // These are 1st order neighbors
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(1,3)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(1,5)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(2,6)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(3,5)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(3,3)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(2,2)));
    // This is not 1st order neighbor, although they are in the same cluster
    EXPECT_FALSE(Proc->ByPosition_Get(3,5)->IsNeighborOf(Proc->ByPosition_Get(1,3)));
    // The cores are not their own neigbors
    EXPECT_FALSE(Proc->ByPosition_Get(3,5)->IsNeighborOf(Proc->ByPosition_Get(3,5)));
    EXPECT_FALSE(Proc->ByPosition_Get(2,4)->IsNeighborOf(Proc->ByPosition_Get(2,4)));

    // All members are the neighbors of their cluster head
    // For odd numbered clusters
    scGridPoint *GPHO = Proc->ByClusterMember_Get(3,cm_Head);
    for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {  // Check if it is good neighbor
        scGridPoint *GP = Proc->ByClusterMember_Get(3,N);
        if(!GP->IsNeighborOf(GPHO) || !GPHO->IsNeighborOf(GP)
                || (GP->ClusterAddress_Get().Cluster != GPHO->ClusterAddress_Get().Cluster))
        {
            EXPECT_EQ(GPHO->ClusterAddress_Get().Cluster,GP->ClusterAddress_Get().Cluster);
        }
    }
    // For even numbered clusters
    scGridPoint *GPHE = Proc->ByClusterMember_Get(4,cm_Head);
    for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {  // Check if it is good neighbor
        scGridPoint *GP = Proc->ByClusterMember_Get(4,N);
        if(!GP->IsNeighborOf(GPHE) || !GPHE->IsNeighborOf(GP)
                || (GP->ClusterAddress_Get().Cluster != GPHO->ClusterAddress_Get().Cluster))
        {
            EXPECT_EQ(GPHE->ClusterAddress_Get().Cluster,GP->ClusterAddress_Get().Cluster);
        }
    }
    // The same with pointers
    // All members are the neighbors of their cluster head
    // For odd numbered clusters
    GPHO = Proc->ByClusterMember_Get(3,cm_Head);
    for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {  // Check if it is good neighbor
        scGridPoint *GP = Proc->ByClusterPointer_Get(GPHO,N);
        if(!GP->IsNeighborOf(GPHO) || !GPHO->IsNeighborOf(GP))
        {
            EXPECT_EQ(GPHO->ClusterAddress_Get().Cluster,GP->ClusterAddress_Get().Cluster);
        }
    }
    // For even numbered clusters
    GPHE = Proc->ByClusterMember_Get(4,cm_Head);
    for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {  // Check if it is good neighbor
        scGridPoint *GP = Proc->ByClusterPointer_Get(GPHE,N);
        if(!GP->IsNeighborOf(GPHE) || !GPHE->IsNeighborOf(GP))
        {
            EXPECT_EQ(GPHE->ClusterAddress_Get().Cluster,GP->ClusterAddress_Get().Cluster);
        }
    }

   // Test the 2nd-order neighbors
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(2,8)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(3,7)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(4,6)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(4,4)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(4,2)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(3,1)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(2,0)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(1,1)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(0,2)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(0,4)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(0,6)));
    EXPECT_TRUE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(1,7)));
    // A 1st order neighbor is not 2nd order
    EXPECT_FALSE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(2,2)));
    // Neither a farther one
    EXPECT_FALSE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(0,8)));
    // Neither itself
    EXPECT_FALSE(Proc->ByPosition_Get(2,4)->Is2ndNeighborOf(Proc->ByPosition_Get(2,4)));

    // The cluster addressing has the drawback that in the case of phantom heads
    // and alias name (proxied by a real head) must be used: the gridpoint will be
    // an external member of the neighboring cluster (having a real head)
    // However, the system takes care of the situation
    // In all other cases the alias name is the real one

    scGridPoint *Head = Proc->ByPosition_Get(2,4);
    scGridPoint *G1 = Proc->ByPosition_Get(3,5);
    EXPECT_EQ(Proc->NeighborDirectionFind(Head, G1), cm_NE);
    EXPECT_EQ(Proc->NeighborDirectionFind(G1, Head), cm_SW); // The opposite way
    EXPECT_EQ(Proc->NeighborDirectionFind(Head, Head), cm_Broadcast);
    EXPECT_EQ(Proc->NeighborDirectionFind(Head, Proc->ByPosition_Get(3,7)), cm_Broadcast);
}

/**
 * Static tests of the topology (basic signaling)
 */

TEST_F(TestTopology, scProcessorStatic)
{
    SC_HTHREAD_ID_TYPE H = 0;
    // The Initial state of scprocessor
    EXPECT_EQ(0,Proc->msSignalMask[H].Allocated);  // Which GridPoints are allocated by a task
    EXPECT_EQ(0,Proc->msSignalMask[H].Denied);     // Which gridpoints are (from any reason) not usable
//    EXPECT_EQ(0,Proc->msSignalMask.Children); // Which GridPoints are immediate children of Topology
    EXPECT_EQ(0,Proc->msSignalMask[H].PreAllocatedForTopology); // Which cores are preallocated for the Topology itself
    EXPECT_EQ(0,Proc->msSignalMask[H].PreAllocated); // Altogether, including for the topology and for GridPoints
    // The next one is derived; all the 6x10 DridPoints are available
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF); // All 60 gridpoints are available
    EXPECT_EQ(Proc->msSignalMask[H].Denied, 0); // Neither of the cores is denied
    EXPECT_EQ(Proc->msSignalMask[H].Allocated ,0); // None of them is allocated
    EXPECT_EQ(Proc->msSignalMask[H].PreAllocated,0); // None of them is preallocated
//??    EXPECT_EQ(Proc->ChildrenMask_Get(),0); // No direct children yet
    EXPECT_EQ(Proc->msSignalMask[H].PreAllocatedForTopology,0); // No cores are allocated for the topology
    // This was the ideal startup condition
    // However, that may be some issue with as scGridPoint
    scGridPoint *P1 = Proc->ByID_Get(1);
    scGridPoint *P2 = Proc->ByID_Get(2);
    scGridPoint *P3 = Proc->ByID_Get(3);

    // Imitate a denied, etc. bit
    P1->DeniedBit_Set();    //
    EXPECT_EQ(2,Proc->msSignalMask[H].Denied);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF-2); // The denied point is not any more available
    P2->AllocatedBit_Set();    //
    EXPECT_EQ(4,Proc->msSignalMask[H].Allocated);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF-2-4); // The denied point is not any more available
    P3->PreAllocatedBit_Set();    //
    EXPECT_EQ(8,Proc->msSignalMask[H].PreAllocated);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF-2-4-8); // The denied point is not any more available
    P2->AllocatedBit_Set(0);    //
    EXPECT_EQ(0,Proc->msSignalMask[H].Allocated);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF-2-8); // The denied point is not any more available
    P3->PreAllocatedBit_Set(0);    //
    EXPECT_EQ(0,Proc->msSignalMask[H].PreAllocated);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF-2); // The denied point is not any more available
    P1->DeniedBit_Set(0);    //
    EXPECT_EQ(0,Proc->msSignalMask[H].Denied);     // Which gridpoints are (from any reason) not usable
    EXPECT_EQ(Proc->msSignalMask[H].Available,0x0FFFFFFFFFFFFFFFF); // The denied point is not any more available


    // Now test if the IGPCB arrays are properly placed
    P1 = Proc->ByIndex_Get(0,0);
    EXPECT_NE(nullptr,P1);
#ifdef _DEBUG
    EXPECT_DEATH(P1->IGPCB_Get((ClusterNeighbor)-1),""); // wrong index
    EXPECT_DEATH(P1->IGPCB_Get(cm_Broadcast),""); // wrong index
#endif
    EXPECT_EQ((scIGPCB*)nullptr,P1->IGPCB_Get(cm_Head));
    EXPECT_NE((scIGPCB*)nullptr,P1->IGPCB_Get(cm_North));
    scIGPCB *P1N = P1->IGPCB_Get(cm_North);   // The head has no IGPCB to itself
    P2 = Proc->ByIndex_Get(0,1); // The (0,1) is the north neighbor
    EXPECT_EQ(P1N->OtherIGPCB_Get()->GridPoint_Get(),P2); // Get back neighbor address though the IGPCBs
    P1N = P1->IGPCB_Get(cm_NE);   // The head has  IGPCB to itself
    P2 = Proc->ByIndex_Get(1,0); // The (1,0) is the north-east neighbor
    EXPECT_EQ(P1N->OtherIGPCB_Get()->GridPoint_Get(),P2); // Get back neighbor address though the IGPCBs

}

/**
 * Dynamic tests of the topology (communication)
 * Checks only creating the messages!
 */
TEST_F(TestTopology, scProcessorCommunication)
{
    scGridPoint *P1 = Proc->ByID_Get(1);    // A normal member
    scGridPoint *P2 = Proc->ByID_Get(2);    // A phantom member, attached as external member to cluster 7
//    MyMessage->BufferContent_Set(0,1234);
    ClusterAddress_t CA = P1->ClusterAddress_Get();
    // Try addressing withut and with proxy
  #if USE_MODULE_NAMES
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_01",Proc->StringOfClusterAddress_Get(P1)); // Basically, it is a "Head"
    CA.Proxy = cm_NE;
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_01",Proc->StringOfClusterAddress_Get(CA)); // Basically, it is a "Head", now with proxy
    // test how a phantom and can communicate
    // This is a member of a phantom cluster
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_02",Proc->StringOfClusterAddress_Get(P2));
    CA.Proxy = cm_Head;
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_01",Proc->StringOfClusterAddress_Get(CA)); // Basically, it is a "Head" again

    P1->ClusterAddress_Set(CA);
    scIGPMessage* MyMessage = P1->CreateRegisterMessageTo(P2,0x11);
    EXPECT_EQ("Reg<2,gtest.SCQtBase.scProc.GP_01,0x11>",Proc->StringOfMessage_Get(MyMessage));
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_01==>Reg<2,gtest.SCQtBase.scProc.GP_01,0x11>==>gtest.SCQtBase.scProc.GP_02",Proc->StringOfRouting_Get(P1,MyMessage,P2));
    delete MyMessage;

    MyMessage = P1->CreateMemoryMessageTo(P2,  0x100);
    EXPECT_EQ("Mem<1,gtest.SCQtBase.scProc.GP_01,0x100>",Proc->StringOfMessage_Get(MyMessage));
    EXPECT_EQ("gtest.SCQtBase.scProc.GP_01==>Mem<1,gtest.SCQtBase.scProc.GP_01,0x100>==>gtest.SCQtBase.scProc.GP_02",Proc->StringOfRouting_Get(P1,MyMessage,P2));
    delete MyMessage;
  #else
    EXPECT_EQ("{0,2}=(7.N)",Proc->StringOfClusterAddress_Get(P1)); // Basically, it is a "Head"
    // test how a phantom and a normal member can communicate
    // This is a member of a phantom cluster
    EXPECT_EQ("{0,4}=(7.N:N)",Proc->StringOfClusterAddress_Get(Proc->ByClusterAddress_Get(P2->ClusterAddress_Get())));
    CA.Proxy = cm_Head;
    EXPECT_EQ("{0,2}=(7.N)",Proc->StringOfClusterAddress_Get(Proc->ByClusterAddress_Get(CA))); // Basically, it is a "Head" again

    P1->ClusterAddress_Set(CA);
    P1->HThreadExec_Set(P1->HThread_Get(0));
    scIGPMessage* MyMessage = P1->CreateRegisterMessageTo(P2,0x11);
    EXPECT_EQ("<{0,2}=(7.N) => {0,4}=(7.N:N) Reg%R[0]=0x0,R[4]=0x0>",Proc->StringOfMessage_Get(MyMessage));
    delete MyMessage;

    MyMessage = P1->CreateMemoryReadMessage(0x100,4);
    EXPECT_EQ("<{0,2}=(7.N) => Mem2%R0x0100:4>",Proc->StringOfMessage_Get(MyMessage));
    delete MyMessage;
  #endif
}


