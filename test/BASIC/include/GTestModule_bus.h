#ifndef INITGTESTBaseBus_H
#define INITGTESTBaseBus_H

#include <systemc>
#include "testbench_bus.h"

extern ScQtTimeAwareBase_bus_TB* ScQtTimeAwareBase_busTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_bus)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_bus);
  GTestModule_bus(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtTimeAwareBase_busTB = new ScQtTimeAwareBase_bus_TB("ScQtTimeAwareBase_bus");
  }
protected:
};

#endif //INITGTESTBaseBus_H
