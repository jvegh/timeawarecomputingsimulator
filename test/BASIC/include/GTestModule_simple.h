#ifndef INITGTESTBase_simple_H
#define INITGTESTBase_simple_H

#include <systemc>
#include "testbench_simple.h"

extern ScQtTimeAwareBase_simple_TB* ScQtTimeAwareBase_simpleTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_simple)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_simple);
  GTestModule_simple(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtTimeAwareBase_simpleTB = new ScQtTimeAwareBase_simple_TB("ScQtTimeAwareBase_simple");
  }
protected:
};

#endif // INITGTESTBase_simple_H
