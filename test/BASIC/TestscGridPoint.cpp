#include <gtest/gtest.h>

//#define SUPPRESS_LOGGING    // Uncomment to suppress log messages of form LOG_INFO(x)
#define DEBUGGING       // Uncomment to suppress messages of form DEBUG_PRINT(x)
#include "Macros.h"
#undef DEBUGGING
#undef SUPPRESS_LOGGING

//#include "Config.h"
#include "systemc"
#include "scGridPoint.h"
#include "GTestModule_bus.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareBase_bus_TB* ScQtTimeAwareBase_busTB;

/** @class	GridPointTest
 */


// A new test class  of these is created for each test
class scGridPointTest : public testing::Test
{
public:

   virtual void SetUp()
   {
      DEBUG_PRINT("GridPointTest started");
      Proc = ScQtTimeAwareBase_busTB->Processor_Get();
      Proc->Reset();
   }

   virtual void TearDown()
   {
        DEBUG_PRINT("GridPointTest terminated");
   }
    scProcessor* Proc;

};
 
// Test self-identity of GridPoints after creating them
// The test assumes that the system sets up correctly,
// all gridpoints have the correct settings
// Check also if they are properly clusterized
// (for testing is is set explicitly, that one of the threads is active, usually thread 0)


TEST_F(scGridPointTest, Identity)
{
    // Test a cluster header first
    // #10 is the head of the 0th cluster,
  SC_GRIDPOINT_ID_TYPE CID = 10;
  scGridPoint* VC = Proc->ByID_Get(CID);
  EXPECT_EQ(cm_Head,VC->ClusterAddress_Get().Member);
  EXPECT_EQ(0,VC->ClusterAddress_Get().Cluster);
  EXPECT_EQ(cs_Head,VC->ClusterAddress_Get().Status);
  // This is in the next column: belongs to the same cluster, but a member
  SC_GRIDPOINT_ID_TYPE CID2 = CID+GRID_SIZE_Y;
  scGridPoint* VC2 = Proc->ByID_Get(CID2);
  EXPECT_EQ(0,VC->ClusterAddress_Get().Cluster);
  EXPECT_EQ(cm_SE,VC2->ClusterAddress_Get().Member);
  EXPECT_EQ(cs_Member,VC2->ClusterAddress_Get().Status);
  // Now check an orphan (has a phantom head)
  SC_GRIDPOINT_ID_TYPE CID3 = 2;
  scGridPoint* VC3 = Proc->ByID_Get(CID3);
  EXPECT_EQ(cm_North,VC3->ClusterAddress_Get().Member);
  EXPECT_EQ(cm_North,VC3->ClusterAddress_Get().Proxy);
  EXPECT_EQ(7,VC3->ClusterAddress_Get().Cluster);
  EXPECT_EQ(cs_Member,VC3->ClusterAddress_Get().Status);
  SC_GRIDPOINT_ID_TYPE CID4 = 3;
  scGridPoint* VC4 = Proc->ByID_Get(CID4);
  EXPECT_EQ(cm_NW,VC4->ClusterAddress_Get().Member);
  EXPECT_EQ(cm_NW,VC4->ClusterAddress_Get().Proxy);
  EXPECT_EQ(1,VC4->ClusterAddress_Get().Cluster);
  EXPECT_EQ(cs_Member,VC4->ClusterAddress_Get().Status);

    sc_time Now = sc_time_stamp();
  string sctime_string = "@" + Proc->StringOfTime_Get() + ": ";
  VC3->HThreadExec_Set(VC3->HThread_Get(0)); // Need to set only for testing
  // In scGridPoints, the PC and memory handling may only be prepared
  #if USE_MODULE_NAMES
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_02%");
    VC->PC_Set(5);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_020x0005%");
    VC->Status_Set(0x105);
    EXPECT_EQ(VC->PrologText_Get(),"@  0.00gtest.SCQtBase.scProc.GP_020x0005[0x0105]%");
  #else
    // This is the .0-th version of the /0-th thread of the core#2, in R mode
    EXPECT_EQ(VC3->PrologString_Get(), sctime_string + "{0,4}=(7.N:N)<2>%");
    VC3->HThreadExec_Get()->PC_Set(5);
    EXPECT_EQ(VC3->PrologString_Get(), sctime_string + "{0,4}=(7.N:N)<2>[0x0005]%");
    VC3->Status_Set(0x105);
    EXPECT_EQ(VC3->PrologString_Get(), sctime_string + "{0,4}=(7.N:N)<2>{0x0105}[0x0005]%");
  #endif // USE_MODULE_NAMES
    VC3->Status_Set(0x0);
/*  EXPECT_TRUE(VC2->Regime_Get());
  VC2->Regime_Set(false);
  EXPECT_FALSE(VC2->Regime_Get());
*/
 EXPECT_NE((scGridPoint*) NULL, VC);
 SC_GRIDPOINT_MASK_TYPE CID_Mask= 1 << CID;
 EXPECT_EQ(CID, VC->ID_Get());
 EXPECT_EQ(CID_Mask, VC->IDMask_Get());
 SC_GRIDPOINT_MASK_TYPE CID_Mask2= 1 << CID2;
 EXPECT_EQ(CID2, VC2->ID_Get());
 EXPECT_EQ(CID_Mask2, VC2->IDMask_Get());
 // The virtual cores are created in unalllocated state, all cores
 EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000);
 EXPECT_EQ(Proc->AllocatedMask_Get(),0x00000000);
 EXPECT_EQ(Proc->PreAllocatedMask_Get(),0x00000000);
 
  // The cluster heads are stored in a special vector
  EXPECT_EQ(8,Proc->NumberOfClusters_Get()); // in 10x6 config, there are 8 Clusters
  VC = Proc->ClusterHead_Get(0);
  EXPECT_EQ(1,VC->X);
  EXPECT_EQ(9,VC->YPosition_Get());
  EXPECT_EQ(4,VC->BusPriority_Get());
/*??
  ClusterAddress_t CA;
  CA.Cluster = 1;
  CA.Member = cm_NE;
  CA.HThread = 5;
  */
/*  scGridPoint* GP = Proc->ByClusterAddress_Get(CA);
  GP = Proc->HThreadByClusterAddress_Get(CA)->Owner_Get();
  string S = GP->StringOfClusterAddress_Get();*/
//  EXPECT_EQ("{3,5}=(1.NE)/5",Proc->HThreadByClusterAddress_Get(CA)->StringOfClusterAddress_Get());
}

/**
 * Tests if the core status bits are properly working
 */
TEST_F(scGridPointTest, UtilityBits)
{
  Proc->Reset();
  SC_GRIDPOINT_ID_TYPE CID = 5;
//  SC_GRIDPOINT_MASK_TYPE CMask= 1 << CID;
  scGridPoint* C0 = Proc->ByID_Get(CID);
  // The virtual cores are created with helper bits clear
  EXPECT_TRUE(C0->AvailableBit_Get());  // The core is of course available
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_FALSE(C0->WaitBit_Get());
  // Test first is 'Wait' works for conventional commands
  C0->WaitBit_Set(true);
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->MetaBit_Set(true);
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->Reset();                      // Reset should clear the utility bits
  EXPECT_FALSE(C0->MetaBit_Get());
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  // Test if 'Meta' works
  C0->MetaBit_Set(true);
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->WaitBit_Set(true);
  EXPECT_TRUE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->WaitBit_Set(false);
  EXPECT_FALSE(C0->WaitBit_Get());
  EXPECT_TRUE(C0->MetaBit_Get());
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
  C0->MetaBit_Set(false);
  EXPECT_TRUE(C0->IsAvailable());	// C0 remains free
    // Now check the core status bits in the bitset
  C0->Reset();

  EXPECT_EQ(0,C0->OperatingBits_Get());   // Its initial value is off

  EXPECT_FALSE(C0->OperatingBit_Get(gob_General));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_OperationMonitored));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_MorphingMonitored));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_ArchitectureMonitored));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_BuffersMonitored));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_CommunicationMonitored));


 /*
  gob_General,
      gob_ObjectMonitored, //!< Set if the object is monitored at all at this time
      gob_ObjectChanged,  //!< Set if the monitored object changed between monitoring, any of the features below
      gob_ObjectActive, //!< Set if the object on monitored active (allocated of preallocated) at this time
      gob_ObjectWaiting, //!< Set if the object is waiting at this time
  gob_OperationMonitored, //!< If any of the operations monitored
      gob_FetchPending, //!< The core can only make one fetch at a time, for all threads
      gob_ExecPending, //!< The core can only make one execute at a time, for all threads
      gob_RegistersMonitored, //!< Register changes are monitored
      gob_MemoryMonitored, //!< Register changes are monitored
  gob_MorphingMonitored, //!< Metainstructions and consequences monitored
      gob_ResourceMonitored, //!< Resources utilization monitored
      gob_ObjecIsMeta, //!< Set if the object is in Meta stata at this time
  gob_ArchitectureMonitored,
      gob_PortsMonitored, //!< Set if port changes monitored
      gob_SignalsMonitored, //!< Set if signal changes are monitored
  gob_BuffersMonitored,   //!< Set if any of the different buffers are monitored
      gob_BufferChanged,  //!< Set if any of the object-related
  gob_CommunicationMonitored, //!< Set if inter-core and inter-cluster communication monitored
      gob_ICCBChanged,    //!<
  gob_Max, //!< The max number of bits in the set
  */
  /* these must go to thread level
  EXPECT_FALSE(C0->OperatingBit_Get(gob_FetchPending));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_ExecPending));

  C0->OperatingBit_Set(gob_FetchPending,true);
  EXPECT_TRUE(C0->OperatingBit_Get(gob_FetchPending));
  EXPECT_FALSE(C0->OperatingBit_Get(gob_ExecPending));
  C0->OperatingBit_Set(gob_ExecPending,true);
  EXPECT_TRUE(C0->OperatingBit_Get(gob_FetchPending));
  EXPECT_TRUE(C0->OperatingBit_Get(gob_ExecPending));
  */
}


/**
 * Tests the initial state of core status settings
 * Checking only bit setting and masks, but no functionality
 */
TEST_F(scGridPointTest, AvailabilityMasks)
{
    Proc->Reset();
    SC_GRIDPOINT_ID_TYPE CID = 3;
    SC_GRIDPOINT_MASK_TYPE CMask= 1 << CID;
    scGridPoint* C0 = Proc->ByID_Get(CID);
    EXPECT_EQ(Proc->ByID_Get(CID),Proc->ByIDMask_Get(CMask));

  // The virtual cores are created in non-denied state
  EXPECT_FALSE(C0->DeniedBit_Get());
  C0->DeniedBit_Set(true);
  wait(SCTIME_GATE);
  EXPECT_TRUE(C0->DeniedBit_Get());
  EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000 | (1<<CID) );
  EXPECT_FALSE(C0->AvailableBit_Get());
  C0->DeniedBit_Set(false);
  wait(SCTIME_GATE);
  EXPECT_EQ(Proc->DeniedMask_Get(),0x00000000 );
  EXPECT_FALSE(C0->DeniedBit_Get());
  EXPECT_TRUE(C0->AvailableBit_Get());
    C0->HThreadExec_Set(C0->HThread_Get(0)); // Needed only for testing
  EXPECT_EQ(CORE_DEFAULT_ADDRESS,C0->HThreadExec_Get()->PC_Get());	// Also the cores are initialized to 0
  EXPECT_EQ(0,C0->PreAllocatedMask_Get());	// Nothing is preallocated for C0
  EXPECT_TRUE(C0->IsAvailable());	// C0 is initially available
//   Mem->Byte_Set(C0,0,0);
   EXPECT_EQ(0,Proc->UnavailableMask_Get()); // Nothing yet is unavailable
  EXPECT_EQ(0,Proc->DeniedMask_Get());	// Nothing disabled
  EXPECT_EQ(0,Proc->AllocatedMask_Get());	// Nothing allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  C0->AllocatedBit_Set();	// Imitate allocating C0
  EXPECT_FALSE(C0->IsAvailable());	// C0 is not any more available
  EXPECT_EQ(CMask,Proc->UnavailableMask_Get()); // Only C0 is unavailable
  EXPECT_EQ(0,Proc->DeniedMask_Get());	// Nothing disabled
  EXPECT_EQ(CMask,Proc->AllocatedMask_Get());	// Nothing allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  scGridPoint* C1 = Proc->ByID_Get(1);
  EXPECT_TRUE(C1->IsAvailable());	// C1 is available
  C1->DeniedBit_Set();	//
  C1->AllocatedBit_Set(); // Attempt to allocate a denied core
  EXPECT_FALSE(C1->IsAllocated());
  C1->PreAllocatedBit_Set(); // Attempt to pre-allocate a denied core
  EXPECT_FALSE(C1->IsPreAllocated());
  EXPECT_EQ( (1 << 1 | 1 << CID) ,Proc->UnavailableMask_Get()); // C0 and C1 are unavailable
  EXPECT_EQ(2,Proc->DeniedMask_Get());	// C1 denied
  EXPECT_EQ(1 << CID,Proc->AllocatedMask_Get());	// Only C0 is allocated
  EXPECT_EQ(0,Proc->PreAllocatedMask_Get());	// Nothing preallocated
  C0->AllocatedBit_Set(false);	// Imitate DeAllocating C0
  EXPECT_EQ(0,Proc->AllocatedMask_Get());	// Only C0 is allocated
  C0->AllocatedBit_Set(true);	// Imitate Allocating C0
  wait(SCTIME_GATE);


  scGridPoint* C2 = Proc->ByID_Get(2);
  EXPECT_TRUE(C2->IsAvailable());	// C1 is available
  // The preallocation is tested locally and globally
  C2->PreAllocatedBit_Set();	// Preallocate C2
  wait(SCTIME_GATE);
  EXPECT_EQ(0x0, C2->PreAllocatedMask_Get()); // C2 has no preallocated cores, but
  EXPECT_EQ(0x4,Proc->PreAllocatedMask_Get());	// C2 preallocated
  EXPECT_EQ(8+4+2,Proc->UnavailableMask_Get()); // C0, C1 and C2 are unavailable
  EXPECT_EQ(2,Proc->DeniedMask_Get());	// C1 disabled
  EXPECT_EQ(1 << CID ,Proc->AllocatedMask_Get());	// C0 is allocated
  EXPECT_FALSE(C1->IsAvailable());	// C1 is not any more available
  EXPECT_FALSE(C2->IsAvailable());	// C2 is not any more available
  EXPECT_FALSE(Proc->ByID_Get(3)->IsAvailable());	// Core3 is still allocated with alias C0
  C2->PreAllocatedBit_Set(false);
  C1->DeniedBit_Set(false);	//
  C0->AllocatedBit_Set(false);
  wait(SCTIME_GATE);
  EXPECT_TRUE(C2->IsAvailable());	// C2 is free again
  EXPECT_TRUE(C0->IsAvailable());	// C1 is free again
  EXPECT_TRUE(C1->IsAvailable());	// C1 is free again

 }


/**
 * Tests the core allocation/deallocation
 */
TEST_F(scGridPointTest, Allocate)
{
    // Just Allocate/Deallocate
    Proc->Reset(); // Make sure everything is clear
    scGridPoint* Core4 = Proc->ByID_Get(4);
//   EXPECT_EQ(0,Core4->BackLinkNumber_Get());  // Initially, there are no backlinked items
    scGridPoint* Core3 = Proc->ByIDMask_Get(8);
    scGridPoint* Core1 = Proc->ByID_Get(1);
    scGridPoint* Core0 = Proc->ByIDMask_Get(1);
    EXPECT_TRUE(Core1->IsAvailable() ); // At the beginning it must be available
    EXPECT_EQ(0,Core4->ChildrenMask_Get());// At the beginning has no children
    Core4->ChildrenMask_Set(6);
    EXPECT_EQ(6,Core4->ChildrenMask_Get());// Check if properly set
    Core4->Reset();
    EXPECT_EQ((scGridPoint*)NULL,Core0->Parent_Get());   // After reset, it is 0
    Core0->Parent_Set(Core1);
    EXPECT_EQ(Core1,Core0->Parent_Get());   // The should be equal with the value set
    EXPECT_NE((scGridPoint*)NULL,Core1->AllocateFor(Core4)); // Now allocate Core1 for Core 4
    EXPECT_EQ(1 << 1,Core4->ChildrenMask_Get());    // after allocation, it is 2**Core1
    EXPECT_EQ((scGridPoint*)NULL,Core1->AllocateFor(Core4)); // Core cannot be allocated twice, for the same
    EXPECT_EQ(1 << 1 , Core4->ChildrenMask_Get()); // Allocated Core 1; no change
   EXPECT_NE((scGridPoint*)NULL, Core3->AllocateFor(Core4)); // Another core can be allocated for it
    EXPECT_FALSE(Core1->IsAvailable()); // Not any more available
    EXPECT_FALSE(Core3->IsAvailable()); // Not any more available
    EXPECT_EQ((scGridPoint*)NULL, Core4->AllocateFor(Core3)); // Cannot allocate a core with children
    EXPECT_TRUE(Core1->IsAllocated()); // This core is really allocated
    EXPECT_TRUE(Core1->IsAllocated()); // This core is really allocated
    EXPECT_FALSE(Core4->IsAllocated());// This is not allocated
    EXPECT_FALSE(Core0->IsAllocated());// This is not allocated
    EXPECT_EQ(0, Core0->ChildrenMask_Get());//
    EXPECT_EQ(1 << 1 | 1 << 3, Core4->ChildrenMask_Get()); // Allocated Core 1 and Core 3
    EXPECT_EQ(1 << 1 | 1 << 3, Proc->AllocatedMask_Get());
    EXPECT_EQ(1 << 1 | 1 << 3, Proc->UnavailableMask_Get());
    EXPECT_FALSE(Core4->Deallocate());// This is not allocated, but has children
    EXPECT_FALSE(Core0->Deallocate());// This is not allocated, and has no children
    EXPECT_TRUE(Core3->Deallocate()); // Was allocated, for Core 4
    EXPECT_TRUE(Core3->IsAvailable()); // Available again
    EXPECT_EQ(1 << 1 , Core4->ChildrenMask_Get()); // Allocated Core 1
    EXPECT_TRUE(Core1->Deallocate());   // Release Core1 from Core4
    EXPECT_EQ(0, Core4->ChildrenMask_Get()); // No more children
    Proc->Reset();
    // Test preallocation mechanism: a core can be preallocated
    // Shall remain preallocated through allocation/deallocation
    // Now allocate the preallocated core

    scGridPoint* Core2 = Proc->ByID_Get(2);
    EXPECT_TRUE(Core1->PreAllocateFor(Core0)); // A preallocated core must not be allocated
    EXPECT_TRUE(Core1->IsPreAllocated());
    EXPECT_TRUE(Core1->IsPreAllocatedFor(Core0));
    EXPECT_FALSE(Core2->IsPreAllocated());
    EXPECT_FALSE(Core2->IsPreAllocatedFor(Core0));

    EXPECT_FALSE(Core1->AllocateFor(Core2)); // NO other core can allocate the preallocated core
    EXPECT_TRUE(Core1->AllocateFor(Core0)); // The preallocated core can be allocated by the allocator core
    EXPECT_FALSE(Core1->AllocateFor(Core0)); // But, only once
    EXPECT_TRUE(Core1->Deallocate());
    EXPECT_TRUE(Core1->IsPreAllocated());
    Core1->PreAllocatedBit_Set(false); // The preallocation shall not be affected
    EXPECT_FALSE(Core1->IsPreAllocated());
    EXPECT_TRUE(Core2->PreAllocateFor(Core1)); // Is already preallocated
    EXPECT_EQ(Core2->IDMask_Get(), Proc->PreAllocatedMask_Get());	// Both C1 and C2 are allocated for C0
    EXPECT_TRUE(Core1->PreAllocateFor(Core0)); // Is already preallocated
    EXPECT_EQ(Core1->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // C1 and C2 are preallocated
    EXPECT_EQ(Core1->IDMask_Get(),Core0->PreAllocatedMask_Get());	// The preallocation is also visible at the parent core
    EXPECT_EQ(0,Proc->AllocatedMask_Get()); // They are not allocated
    EXPECT_EQ((scGridPoint*)NULL,Core1->AllocateFor(Core2)); // Preallocated cores cannot be allocated
    EXPECT_EQ(Core1->IDMask_Get()+Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // C1 and C2 are preallocated
    EXPECT_FALSE(Core1->Deallocate());  // The core was not allocated
    Core3->DeniedBit_Set(1);
    wait(SCTIME_GATE);
    EXPECT_FALSE(Core3->PreAllocateFor(Core0));
    EXPECT_FALSE(Core3->AllocateFor(Core0));
    EXPECT_EQ(Core3->IDMask_Get()|Core2->IDMask_Get()|Core1->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core3->DeniedBit_Set(0);

    Proc->Reset(); // Now clear preallocation at SV
    EXPECT_EQ(0,Proc->PreAllocatedMask_Get()); // No cores preallocated
    EXPECT_EQ(0,Proc->AllocatedMask_Get()); // No cores allocated
    EXPECT_EQ(0,Proc->UnavailableMask_Get()); // No cores unavailable
    EXPECT_FALSE(Core2->Deallocate()); // Core2 was not allocated

    // Now try allocation for the processor
    EXPECT_NE((scGridPoint*)NULL,Core4->AllocateFor((scGridPoint*)NULL));
    EXPECT_EQ(Core4->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    EXPECT_TRUE(Core3->PreAllocateFor((scGridPoint*)NULL));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core2->DeniedBit_Set(1);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    EXPECT_FALSE(Core2->PreAllocateFor((scGridPoint*)NULL));
    EXPECT_FALSE(Core2->AllocateFor((scGridPoint*)NULL));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // One core is unavailable
    Core2->DeniedBit_Set(0);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are unavailable

    EXPECT_TRUE(Core2->PreAllocateFor((scGridPoint*)NULL));
    EXPECT_TRUE(Core2->AllocateFor((scGridPoint*)NULL));
    EXPECT_FALSE(Core2->PreAllocateFor((scGridPoint*)NULL));
    EXPECT_FALSE(Core4->PreAllocateFor(Core2));
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // Three cores are unavailable
    Core2->Deallocate();
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get()|Core2->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are unavailable
    Core2->PreAllocatedBit_Set(0);
    wait(SCTIME_GATE);
    EXPECT_EQ(Core4->IDMask_Get()|Core3->IDMask_Get(),Proc->UnavailableMask_Get()); // Two cores are  is unavailable
    EXPECT_FALSE(Core2->IsPreAllocated());
    EXPECT_TRUE(Core2->IsPreAllocatedFor((scGridPoint*)NULL)); // Not any more preallocated   
}

/**
* Tests if the cores can be logged in the right way
*/
TEST_F(scGridPointTest, scLogging)
{
    Proc->Reset();
   string CoreT;
   scGridPoint* C0 = Proc->ByID_Get(0);
   CoreT = C0->StringOfClusterAddress_Get();
   EXPECT_EQ("{0,0}=(7.H)",CoreT);	// Should describe Core#0
   EXPECT_EQ("{0,0}=(7.H)",C0->StringOfClusterAddress_Get());	// Should describe Core#0
   scGridPoint* C = Proc->ByID_Get(6);
   // Test QT ID
   string QTID;
   C->HThreadExec_Set(C->HThread_Get(0)); // Needef for testing only
   EXPECT_EQ("6", C->HThreadExec_Get()->StringOfName_Get()); // Default string at the beginning
   C->HThreadExec_Get()->StringOfAliasName_Set("MyBest");
   EXPECT_EQ("MyBest", C->HThreadExec_Get()->StringOfName_Get());
   // The gridpoint message FIFO must be present and empty
   EXPECT_EQ(0,C->GPMessagefifo->num_available());
   // Shall be tested by TestRouting
    // QT_AssembleID overwrites the given name:
   // This is the _0th reincarnation of the 6-th core, /0-th thread, in 'R' mode
   EXPECT_EQ("6", C->HThreadExec_Get()->StringOfDefaultName_Get( ));
   // But another given name overwites it
   C->HThreadExec_Get()->StringOfAliasName_Set("MyBestID");
   EXPECT_EQ("MyBestID", C->HThreadExec_Get()->StringOfAliasName_Get());
   C->HThreadExec_Get()->OperatingBit_Set(tob_Regime,true);  // Now try if 'Q' mode works
   // This is the _0th reincarnation of the 6-th core, /0-th thread, in 'Q' mode
   EXPECT_EQ("6", C->HThreadExec_Get()->StringOfDefaultName_Get( ));
   C->HThreadExec_Set(C->HThread_Get(3));   // Now checking the third thread, no name was given
   // This is the _0th reincarnation of the 6-th core, /3rd thread, in 'R' mode
   EXPECT_EQ("6/3", C->HThreadExec_Get()->StringOfDefaultName_Get());
   C->HThreadExec_Set(C->HThread_Get(3));   // Now checking the third thread, no name was given
   // This is the _0th reincarnation of the 6-th core, /3rd thread, in 'R' mode
   C0->HThreadExec_Set(C0->HThread_Get(5)); // Needef for testing only
 // ??  EXPECT_EQ("6/3-0/5", C0->HThreadExec_Get()->QT_AssembleID(  C));
}

TEST_F(scGridPointTest, EventTiming)
{
    // Test a cluster header first
    // #10 is the head of the 0th cluster,
  SC_GRIDPOINT_ID_TYPE CID = 10;
  scGridPoint* VC = Proc->ByID_Get(CID);
  EXPECT_EQ(cm_Head,VC->ClusterAddress_Get().Member);

  ClusterAddress_t CA = VC->ClusterAddress_Get();
  CA.HThread = 3;   // its number to HThread #3
  scHThread* MyThread = Proc->HThreadByClusterAddress_Get(CA);
  MyThread->EVENT_HTHREAD.NEXT.notify();
//  wait(MyThread->EVENT_HTHREAD.FETCHED);
}
