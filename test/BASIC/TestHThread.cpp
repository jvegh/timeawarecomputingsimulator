#include <gtest/gtest.h>


//#define SUPPRESS_LOGGING    // Uncomment to suppress log messages of form LOG_INFO(x)
#define DEBUGGING       // Uncomment to suppress messages of form DEBUG_PRINT(x)
#include "Macros.h"
#undef DEBUGGING
#undef SUPPRESS_LOGGING

#include "systemc"
#include "scGridPoint.h"
#include "scHThread.h"
#include "Stuff.h"
//#include "Processor.h"
#include "testbench_bus.h"

extern bool UNIT_TESTING;		// Switched off by default
extern ScQtTimeAwareBase_bus_TB* ScQtTimeAwareBase_busTB;
extern string DebugRoute;
/** @class	TestHThread
 *
 * This class ony checks some elementary testing.
 * (for testing is is set explicitly, that one of the threads is active, usially thread 0)
 * Now first the one-thread functionality will be implemented
 */


// A new test class is created for each test
class TestHThread : public testing::Test
{
public:

   virtual void SetUp()
   {
      DEBUG_PRINT("TestHThread started");
      Proc = ScQtTimeAwareBase_busTB->Processor_Get();
      Proc->Reset();
   }

   virtual void TearDown()
   {
        DEBUG_PRINT("TestHThread terminated");
   }
    scProcessor* Proc;

};
 
TEST_F(TestHThread, ThreadIdentity)
{
    Proc->Reset();
    scGridPoint *P3 = Proc->ByID_Get(3);
    SC_HTHREAD_ID_TYPE H0 = 0;
    scHThread* P3H0 = P3->HThread_Get(H0);
    scGridPoint *P5 = Proc->ByID_Get(5);
    SC_HTHREAD_ID_TYPE H3 = 3;
    scHThread* P5H3 = P3->HThread_Get(H3);

/*    EXPECT_EQ(0,P3->HThreadsMask_Get());
    P3->HThreadBit_Set(H0,1);
    EXPECT_EQ(1,P3->HThreadsMask_Get()); // Set a thread mask in P3
    EXPECT_EQ(0,P5->HThreadsMask_Get());
    P5->HThreadBit_Set(H3,1);
    EXPECT_EQ(1 << H3,P5->HThreadsMask_Get()); // Set a thread mask in P5
    EXPECT_EQ(1 << H0,P3->HThreadsMask_Get()); // P3 must not change
    P3->HThreadBit_Set(H0,0);
    EXPECT_EQ(0,P3->HThreadsMask_Get());        // P3 properly reset
    EXPECT_EQ(1 << H3,P5->HThreadsMask_Get()); // P5 must not change
    P5->HThreadBit_Set(H3,0);
    EXPECT_EQ(0,P5->HThreadsMask_Get());        // P5 properly reset
*/
}

// This test  only checks if the basic events work as expected,
// no real functionality behind
TEST_F(TestHThread, Processing)
{
    Proc->Reset();
    scGridPoint *P3 = Proc->ByID_Get(3);
    scHThread* H32 = P3->HThread_Get(2);
    scHThread* H33 = P3->HThread_Get(3);
    scGridPoint *P4 = Proc->ByID_Get(4);
    scHThread* H42 = P4->HThread_Get(2);
    scHThread* H43 = P4->HThread_Get(3);
    // Now we have 4 threads in 2 gridpoints
    // Check if threads are properly handled
#ifdef _DEBUG
    EXPECT_DEATH(H32->PC_Set(FMAX_MEMORY_SIZE),"");
#endif
    H32->PC_Set(100);   // Set one, core, one thread
    EXPECT_EQ(-1,H33->PC_Get()); // Other core, other thread not affected
    EXPECT_EQ(-1,H42->PC_Get());
    // Now check the same with registers
#ifdef _DEBUG
    EXPECT_DEATH(H32->Register_Set(120,0),""); // Illegal register
#endif
    H32->Register_Set(1,0x123);   // Set one, core, one thread
    EXPECT_EQ(0x123,H32->Register_Get(1));  // This register is correctly set
    EXPECT_EQ(0,H33->Register_Get(1));  // Other cores/threads are not affected
    EXPECT_EQ(0,H43->Register_Get(1));
}

/**
* Tests if the cores can be logged in the right way
*/
TEST_F(TestHThread, Logging)
{
    Proc->Reset();
   string CoreT;
   scGridPoint* C0 = Proc->ByID_Get(0);
   CoreT = C0->StringOfClusterAddress_Get();
   EXPECT_EQ("{0,0}=(7.H)",CoreT);	// Should describe Core#0
   EXPECT_EQ("{0,0}=(7.H)",C0->StringOfClusterAddress_Get());	// Should describe Core#0
   scGridPoint* C = Proc->ByID_Get(6);
   // Test QT ID
   string QTID;
   EXPECT_EQ("6/3", C->HThreadExec_Get()->StringOfDefaultName_Get()); // Default string at the beginning
   C->HThreadExec_Get()->StringOfAliasName_Set("MyBest");
   EXPECT_EQ("MyBest", C->HThreadExec_Get()->StringOfAliasName_Get());
   // The gridpoint message FIFO must be present and empty
   EXPECT_EQ(0,C->GPMessagefifo->num_available());
   // Shall be tested by TestRouting
}


// Test HThread timing

TEST_F(TestHThread, Timing)
{
    Proc->Reset();
    // Test if timeout works: set a 20 ns timing and check if the event timed properly
    scGridPoint *P3 = Proc->ByID_Get(3);
    SC_HTHREAD_ID_TYPE H0 = 0;
    scHThread* P3H0 = P3->HThread_Get(H0);
    // Also test if timeout facility works
    sc_time TT = sc_time(20,SC_NS);
    P3H0->Timeout_Set(TT);
    EXPECT_FALSE(P3H0->IsTimedOut());
    wait(10,SC_NS);
    EXPECT_FALSE(P3H0->IsTimedOut());
    wait(11,SC_NS);
    EXPECT_TRUE(P3H0->IsTimedOut());  // Now we are at 21 ns
}

/*    scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
    scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

    scIGPMessage* Message2   =C2->CreateRegisterMessageTo(C0,0X3);  // Wants to send2 registers

    C2->RouteMessage(Message2);
*/

 /*   scGridPoint *C0 = Proc->ByPosition_Get(0,0); // The head of the incomplete cluster
    scGridPoint *C2 = Proc->ByPosition_Get(0,4);  // a phantom member, external member of the incomplete cluster

    // Message to a non-existent point
    scGridPoint *CM1 = Proc->ByPosition_Get(-1,0);
    EXPECT_DEATH(C0->CreateRegisterMessageTo(CM1, 0xb),"");

    // Now test 'R' message to an external member
    scIGPMessage* Message2 = C0->CreateRegisterMessageTo(C2, 0xa);

    ClusterAddress_t CAD = Message2->DestinationAddress_Get();
    ClusterAddress_t CAS = Message2->ReplyAddress_Get();
    scGridPoint *Dest =  Proc->ByClusterAddress_Get(CAD);
    scGridPoint *Source =  Proc->ByClusterAddress_Get(CAS);
    EXPECT_EQ(Dest,C2);
    EXPECT_EQ(Source,C0);
    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (0,4), a phantom, attached to the 7th cluster as external
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{0,4}=(7.N:N)");
     // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C2));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C2));
    C0->RouteMessage(Message2);
    wait(10,SC_NS);  // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT message to {0,2}=(7.N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT message to {0,4}=(7.N:N): <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%RCVD DIRECT message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%PROCESSING message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%DELETED message <{0,0}=(7.H) => {0,4}=(7.N:N) Reg%R[1]=0x0,R[3]=0x0>\n");

    // The same message in the reverse direction
    scIGPMessage* Message2R = C2->CreateRegisterMessageTo(C0, 0xa);
    C2->RouteMessage(Message2R);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%PROXY {0,2}=(7.N) inserted: <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT message to {0,2}=(7.N): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT message to {0,0}=(7.H): <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROCESSING message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%DELETED message <{0,4}=(7.N:N) => {0,0}=(7.H) Reg%R[1]=0x0,R[3]=0x0>\n");

    // Now test 'R' message to an internal member
    scGridPoint *C1 = Proc->ByClusterMember_Get(7,cm_NE);  // internal member of the incomplete cluster
    scIGPMessage* Message1 = C0->CreateRegisterMessageTo(C1, 0xa);
    CAD = Message1->DestinationAddress_Get();
    CAS = Message1->ReplyAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    C0->RouteMessage(Message1);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT message to {1,1}=(7.NE): <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%RCVD DIRECT message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%PROCESSING message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n{1,1}=(7.NE)%DELETED message <{0,0}=(7.H) => {1,1}=(7.NE) Reg%R[1]=0x0,R[3]=0x0>\n");

    // The source is the point (0,0), the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point (1,1), internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,1}=(7.NE)");
    // This is delivered via direct message
    EXPECT_TRUE(C0->IsNeighborOf(C1));
    EXPECT_FALSE(C0->Is2ndNeighborOf(C1));

    // Now test 'R' message to a neighbor, that is an internal member of another cluster
    scGridPoint *C3 = Proc->ByClusterMember_Get(1,cm_SW);  // internal member of the incomplete cluster
    scIGPMessage* Message3 = C0->CreateRegisterMessageTo(C3, 0xe);
    CAD = Message3->DestinationAddress_Get();
    CAS = Message3->ReplyAddress_Get();
    Dest =  Proc->ByClusterAddress_Get(CAD);
    Source =  Proc->ByClusterAddress_Get(CAS);
    // The source is the point {0,0}, the head of the 7th cluster
    EXPECT_EQ(Source->StringOfClusterAddress_Get(), "{0,0}=(7.H)");
    // The destination is point {1,3}, internal member the 7th cluster
    EXPECT_EQ(Dest->StringOfClusterAddress_Get(), "{1,3}=(1.SW)");
    // This is delivered via direct message
    EXPECT_FALSE(C0->IsNeighborOf(C3));
    EXPECT_TRUE(C0->Is2ndNeighborOf(C3));
    C0->RouteMessage(Message3);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,0}=(7.H)%CREATED message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%ROUTING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%PROXY {0,2}=(7.N) inserted: <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,0}=(7.H)%SENT DIRECT message to {0,2}=(7.N): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%RCVD DIRECT message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%ROUTING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,2}=(7.N)%SENT DIRECT message to {1,3}=(1.SW): <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%RCVD DIRECT message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%PROCESSING message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{1,3}=(1.SW)%DELETED message <{0,0}=(7.H) => {1,3}=(1.SW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");

    // Now send message fro a phantom member to another phantom member
    scGridPoint *C4 = Proc->ByPosition_Get(0,4);  // internal member of an incomplete cluster
    scGridPoint *C6 = Proc->ByPosition_Get(0,6);  // internal member of another incomplete cluster
    // However, they are neighbors
    scIGPMessage* Message6 = C4->CreateRegisterMessageTo(C6, 0xe);
    C4->RouteMessage(Message6);
    wait(10,SC_NS);   // Must wait until the message passed by the buffers
    EXPECT_EQ(DebugRoute,"{0,4}=(7.N:N)%CREATED message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%ROUTING message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,4}=(7.N:N)%SENT DIRECT message to {0,6}=(1.NW:NW): <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%RCVD DIRECT message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%PROCESSING message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n{0,6}=(1.NW:NW)%DELETED message <{0,4}=(7.N:N) => {0,6}=(1.NW:NW) Reg%R[1]=0x0,R[2]=0x0,R[3]=0x0>\n");
*/
