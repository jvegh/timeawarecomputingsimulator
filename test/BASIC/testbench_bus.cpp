#include <systemc>
#include "gtest/gtest.h"
using namespace sc_core;
#include "include/testbench_bus.h"
extern bool UNIT_TESTING;		// Switched off by default
using namespace std;

void ScQtTimeAwareBase_bus_TB::test_thread()
{
  mTestResult = RUN_ALL_TESTS();
  sc_stop(); //< invoke end_of_simulation
}//end test_thread

