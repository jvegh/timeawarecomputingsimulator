#ifndef INITGTESTProcBus_H
#define INITGTESTProcBus_H

#include <systemc>
#include "testbench_bus.h"

extern ScQtTimeAwareProc_bus_TB* ScQtTimeAwareProc_busTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_bus)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_bus);
  GTestModule_bus(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtTimeAwareProc_busTB = new ScQtTimeAwareProc_bus_TB("ScQtTimeAwareProc_bus");
  }
protected:
};

#endif //INITGTESTProcBus_H
