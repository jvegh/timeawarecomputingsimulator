#ifndef INITGTESTProc_simple_H
#define INITGTESTProc_simple_H

#include <systemc>
#include "testbench_simple.h"

extern ScQtTimeAwareProc_simple_TB* ScQtTimeAwareProc_simpleTB;
// This module must generate all DUT modules, and connect their signals properly
//
SC_MODULE(GTestModule_simple)
{
  // Constructor
  SC_HAS_PROCESS(GTestModule_simple);
  GTestModule_simple(sc_module_name nm)
  : sc_module(nm)
  {
   ScQtTimeAwareProc_simpleTB = new ScQtTimeAwareProc_simple_TB("ScQtTimeAwareProc_simple");
  }
protected:
};

#endif // INITGTESTProc_simple_H
