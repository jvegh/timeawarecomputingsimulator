#include <systemc>
#include "gtest/gtest.h"
#include "Project.h"
//#include "Config.h"
using namespace sc_core;
using std::cout;
using std::endl;
#include <chrono>
#include "include/testbench_simple.h"
//extern bool UNIT_TESTING;		// Switched off by default
using namespace std;

void ScQtTimeAwareProc_simple_TB::test_thread()
{
//  srand(0);

  std::cerr << "INFO: " << name() << " V" << PROJECT_VERSION << " @ " << sc_time_stamp() << " Starting..."<< endl;
  auto start = chrono::steady_clock::now();
  int result =  RUN_ALL_TESTS();
  auto end = chrono::steady_clock::now();
  std::cerr << "INFO: " << name()  << " V" << PROJECT_VERSION << " @ " << sc_time_stamp() << " Returned with value " << result << endl;
  sc_stop(); //< invoke end_of_simulation
  std::cerr << "Elapsed time for gtest execution: "
          << chrono::duration_cast<chrono::milliseconds>(end - start).count()
          << " msec" << endl;
}//end testbench::test_thread

