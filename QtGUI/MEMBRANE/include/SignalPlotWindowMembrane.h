#ifndef SIGNALPLOTWINDOWMEMBRANE_H
#define SIGNALPLOTWINDOWMEMBRANE_H

//#include "SimulatorWindow.h"
#include "qcustomplot.h"
//class QCustomPlot;
class SimulatorWindow;

class SignalPlotWindowMembrane : public QCustomPlot
{
    Q_OBJECT

public:
    explicit SignalPlotWindowMembrane(QWidget *parent = nullptr);
    ~SignalPlotWindowMembrane();
    void setupColorMapDemo(QCustomPlot *customPlot);// Just for demo

//    void setupSincScatterDemo(QCustomPlot *customPlot);
public slots:
//    void setLupe(const QCPRange &range);

private:
 //   QCustomPlot* navigatorPlot;
};



#endif // SIGNALPLOTWINDOWMEMBRANE.H
