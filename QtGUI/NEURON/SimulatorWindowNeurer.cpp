#include <QVBoxLayout>
#include <QMenuBar>
#include <QMenu>
#include <QListView>
#include <QTreeView>
#include <QStatusBar>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include "SimulatorWindowNeurer.moc"
#include "SimulatorWindowNeurer.h"
#include "scSimulator.h"
#include "uiSimulatorWindowNeurer.h"
#include "SignalPlotWindowNeurer.h"
#include "aboutwidget.h"
#include "Stuff.h"
#include "codeeditor.h"
#include "Macros.h"
//#include "Config.h"
#include <systemc>
#include "scSimulator.h"
#include "scProcessor.h"
#include "scGridPoint.h"

/*
#include <chrono> // for sleep only
#include <thread> // for sleep only
 */
extern bool UNIT_TESTING;
extern scClusterBusMemorySlow *MainMemory;
extern scSimulator* TheSimulator;
// https://stackoverflow.com/questions/22485208/redirect-qdebug-to-qtextedit

SimulatorWindowNeurer::SimulatorWindowNeurer(int argc, char **argv, QWidget *parent) :
    QMainWindow(parent),
     ui(new Ui::SimulatorWindowNeurer)
{
    ui->setupUi(this);  // This creates the basic splitters
    setWindowTitle(QString(GetAppName().c_str())+"/Neurer");
    setWindowIcon(QIcon(":/images/Neurerlogo.png"));
  //   scqSimulator::s_LogWindow = new QTextEdit;
//    HierarchyView = new QTreeView;  // Show hierarchy here
//??    scqSimulator::s_LogWindow->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded);
//??    scqSimulator::s_LogWindow->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded);
    SetupSimulatorWindow(); // Creates sizers and some basic windows


    createMenus();
    createActions();
    createToolBars();
    createSimulator();
    // Now we are ready with the  GUI, deal with the simulation
    // Set up the processor simulator

//    QPushButton *, *StopButton, *StepButton;

 }

SimulatorWindowNeurer::~SimulatorWindowNeurer()
{
 //   delete ui; // Done in anchestor
}
/*!
 * \brief Setup graphic facilities of the simulator window.
 * Calls more virtual functions to set up its splitters and tabs
 */

void SimulatorWindowNeurer::SetupSimulatorWindow()
{
    // The graphic tab contains visual outputs from  the simulator
 //   ui->graphTab->addTab(new CustomPlotWindow, QApplication::translate("SignalWindow","Signals", Q_NULLPTR));
    // The graphic tab contains visual outputs from  the simulator
    ui->graphTab->addTab(new SignalPlotWindowNeurer(this), QApplication::translate("SignalWindow","Signals", Q_NULLPTR));
    ui->graphTab->addTab(new SignalPlotWindowNeurer(this), QApplication::translate("StateWindow","States", Q_NULLPTR));


   //   The text tab contains textual information, such as messages, source cores, settings
    ui->textTab->addTab(scqSimulator::s_LogWindow, QApplication::translate("Text","Messages", Q_NULLPTR));
    ui->textTab->addTab(new CodeEditor, QApplication::translate("Text","Source Code", Q_NULLPTR));

   // The control tab contains control widgets from  the simulator
//        controlTab->addTab(new CustomPlotWindow, QApplication::translate("ControlWindow","Control", Q_NULLPTR));
    ui->controlTab->addTab(new CodeEditor, QApplication::translate("Control","Controls", Q_NULLPTR));

    // The navigation tab contains hierarchy three, etc
    NavigatorWindow = new QTreeView;

    ui->navigatorTab->addTab(NavigatorWindow, QApplication::translate("Navigator","Hierarchy", Q_NULLPTR));
    SetupHierarchyWindow();
//    NavigatorWindow->setModel(completer->model());
}

void SimulatorWindowNeurer::SetupHierarchyWindow()
{
    //completer = new TreeModelCompleter(this);
 /*   const QStringList list {"Parent1\n",
                            "\tChild1\n",
                                "Parent1.Child1.GrandChild1\n",
                                "\t\tGrandChild2\n",
                                "\t\tGrandChild3\n",
                                    "\t\tGrandGrandChild1\n",
                            "\tChild2\n",
                                "\t\tGrandChild1\n",
                                    "\t\t\tGrandGrandChild1\n",
                                "\t\tGrandChild2\n",
                            "\tChild3\n",
                        "\n",
                            "Parent2\n",
                            "\tChild1\n",
                            "\t\tGrandChild1\n",
                            "\tChild2\n",
                            "\tChild3\n",
                            "\t\tGrandChild1\n",
                            "\t\tGrandChild2\n" };*/
    const QStringList list {"Parent1\n",
                            "Parent1.Child1\n",
                            "Parent1.Child1.GrandChild1\n",
                                "Parent1.Child1.GrandChild2\n",
                                "Parent1.Child1.GrandChild3\n",
                                    "Parent1.Child1.GrandChild3.GrandGrandChild1\n",
                            "Parent1.Child2\n",
                                "Parent1.Child2.GrandChild1\n",
                                    "Parent1.Child2.GrandChild1.GrandGrandChild1\n",
                                "Parent1.Child2.GrandChild2\n",
                            "Parent1.Child3\n",
                        "\n",
                            "Parent2\n",
                            "Parent2.Child1\n",
                            "Parent2.Child1.GrandChild1\n",
                            "Parent2.Child2\n",
                            "Parent2.Child3\n",
                            "Parent2.Child3.GrandChild1\n",
                            "Parent2.Child3.GrandChild2\n" };
        //completer->setModel(modelFromStringList(list));
//    QObject::connect(completer, QOverload<const QModelIndex &>::of(&TreeModelCompleter::highlighted),
//                     this, &MainWindow::highlight);

    /*
     * NavigatorWindow->setColumnCount(2);
//    NavigatorWindow->setCheckState(1,Qt::PartiallyChecked);

    QList<QTreeWidgetItem *> items;
    QString name("NameA");
    QString description("Description");
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem(NavigatorWindow);
    treeItem->setCheckState(1,Qt::PartiallyChecked);
    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);
    treeItem->setText(1, description);
//    addTreeChild(treeItem, name + "A", "Child_first");
 //   addTreeChild(treeItem, name + "B", "Child_second");
    QTreeWidgetItem *childItem = new QTreeWidgetItem();

    // QTreeWidgetItem::setText(int column, const QString & text)
     childItem->setText(0, name);
     childItem->setText(1, description);

     // QTreeWidgetItem::addChild(QTreeWidgetItem * child)
     treeItem->addChild(childItem);

    for (int i = 0; i < 10; ++i)
    {
        items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), QStringList(QString("item: %1").arg(i))));
        ((QTreeWidgetItem *)(items.at(i)))->setCheckState(1,Qt::PartiallyChecked);
    }
    NavigatorWindow->insertTopLevelItems(0, items);
*/
}

void SimulatorWindowNeurer::
createSimulator()
{

}


QAbstractItemModel *SimulatorWindowNeurer::modelFromStringList(const QStringList &fileName)
{
#if 0 //
    completer->setSeparator(QLatin1String("."));
    QStandardItemModel *model = new QStandardItemModel(completer);
    QVector<QStandardItem *> parents(10);
    parents[0] = model->invisibleRootItem();

    QRegularExpression re("^\\s+");
 //   while (!file.atEnd()) {
    for ( const auto& i : fileName  )
    {
        const QString line = i.trimmed();
        const QString trimmedLine = line.trimmed();
        if (trimmedLine.isEmpty())
            continue;

        const QRegularExpressionMatch match = re.match(line);
        int nonws = match.capturedStart();
        int level = 0;
        if (nonws == -1) {
            level = 0;
        } else {
            const int capLen = match.capturedLength();
            level = line.startsWith(QLatin1Char('\t')) ? capLen / 4 : capLen;
        }

        if (level + 1 >= parents.size())
            parents.resize(parents.size() * 2);

        QStandardItem *item = new QStandardItem;
        item->setText(trimmedLine);
        parents[level]->appendRow(item);
        parents[level + 1] = item;
    }
    return model;


/*
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
        return new QStringListModel(completer);

#ifndef QT_NO_CURSOR
    QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
#endif

    QStandardItemModel *model = new QStandardItemModel(completer);
    QVector<QStandardItem *> parents(10);
    parents[0] = model->invisibleRootItem();

    QRegularExpression re("^\\s+");
    while (!file.atEnd()) {
        const QString line = QString::fromUtf8(file.readLine()).trimmed();
        const QString trimmedLine = line.trimmed();
        if (trimmedLine.isEmpty())
            continue;

        const QRegularExpressionMatch match = re.match(line);
        int nonws = match.capturedStart();
        int level = 0;
        if (nonws == -1) {
            level = 0;
        } else {
            const int capLen = match.capturedLength();
            level = line.startsWith(QLatin1Char('\t')) ? capLen / 4 : capLen;
        }

        if (level + 1 >= parents.size())
            parents.resize(parents.size() * 2);

        QStandardItem *item = new QStandardItem;
        item->setText(trimmedLine);
        parents[level]->appendRow(item);
        parents[level + 1] = item;
    }

#ifndef QT_NO_CURSOR
    QGuiApplication::restoreOverrideCursor();
#endif
*/
//VJ    return model;
    return model;
#endif //0
    return nullptr;
}

void SimulatorWindowNeurer::
createActions()
{


    startAct = new QAction(QIcon(":/images/start.jpeg"), tr("&Start simulation"), this);
    stopAct->setStatusTip(tr("Start simulation"));
    stepAct = new QAction(QIcon(":/images/step.png"), tr("Make a &Step"), this);
    stepAct->setStatusTip(tr("Start next step"));
    stopAct = new QAction(QIcon(":/images/stop.jpeg"), tr("&Stop simulation"), this);
    stopAct->setStatusTip(tr("Stop simulation"));
/*  saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
  saveAct->setShortcuts(QKeySequence::Save);
  saveAct->setStatusTip(tr("Save the document to disk"));
  connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

  saveAsAct = new QAction(tr("Save &As..."), this);
  saveAsAct->setShortcuts(QKeySequence::SaveAs);
  saveAsAct->setStatusTip(tr("Save the document under a new name"));
  connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));
*/

/*  cutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
  cutAct->setShortcuts(QKeySequence::Cut);
  cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                          "clipboard"));
//    connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

  copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
  copyAct->setShortcuts(QKeySequence::Copy);
  copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                           "clipboard"));
//    connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

  pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
  pasteAct->setShortcuts(QKeySequence::Paste);
  pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                            "selection"));
//    connect(pasteAct, SIGNAL(triggered()), textEdit, SLOT(paste()));

  enableAct = new QAction(QIcon(":/images/edit.png"), tr("&Enable"), this);
//    pasteAct->setShortcuts(Qt::CTRL + Qt::Key_E);
  enableAct->setStatusTip(tr("Enable editing contents"));
  connect(enableAct, SIGNAL(triggered()), this, SLOT(enable()));
*/

/*    guideAct = new QAction(tr("&Guide"), this);
  guideAct->setStatusTip(tr("Show the application's Guide"));
  connect(guideAct, SIGNAL(triggered()), this, SLOT(guide()));


  cutAct->setEnabled(false);
  copyAct->setEnabled(false);
  connect(textEdit, SIGNAL(copyAvailable(bool)),
          cutAct, SLOT(setEnabled(bool)));
  connect(textEdit, SIGNAL(copyAvailable(bool)),
          copyAct, SLOT(setEnabled(bool)));
*/
/*   helpAct = new QAction(tr("Help Contents..."), this);
  helpAct->setShortcut(QKeySequence::HelpContents);
  connect(helpAct, SIGNAL(triggered()),
          helpWindow, SLOT(show()));
*/
}

void SimulatorWindowNeurer::createMenus()
{
    ui->fileMenu = menuBar()->addMenu(tr("&File"));
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    ui->fileMenu->addAction(openAct);
//     fileMenu->addAction(saveAct);
//     fileMenu->addAction(saveAsAct);
    ui->fileMenu->addSeparator();
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    ui->fileMenu->addAction(exitAct);


    menuBar()->addSeparator();

/*    helpMenu->addAction(helpAct);
    helpMenu->addSeparator();
    connect(helpAct, SIGNAL(triggered()), helpWindow, SLOT(show()));
*/
    ui->simulatorMenu = menuBar()->addMenu(tr("&Simulator"));
    startAct = new QAction(QIcon(":/images/start.png"), tr("&Start"), this);
    startAct->setStatusTip(tr("Start simulation"));
    ui->simulatorMenu->addAction(startAct);
    connect(startAct, SIGNAL(triggered()),
            this, SLOT(on_startButton_clicked()));
    stepAct = new QAction(QIcon(":/images/step.png"), tr("&Step"), this);
    stepAct->setStatusTip(tr("Make a simulation step"));
    ui->simulatorMenu->addAction(stepAct);
    connect(stepAct, SIGNAL(triggered()),
            this, SLOT(on_stepButton_clicked()));
    stopAct = new QAction(QIcon(":/images/stop.png"), tr("&Stop"), this);
    stopAct->setStatusTip(tr("Stop simulation"));
    ui->simulatorMenu->addAction(stopAct);
    connect(stopAct, SIGNAL(triggered()),
            this, SLOT(on_stopButton_clicked()));

    ui->helpMenu = menuBar()->addMenu(tr("&Help"));
    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(on_actionAbout_triggered()));
    ui->helpMenu->addAction(aboutAct);
    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
//    startAct = new QAction(tr("&About"), this);

//    connect(startAct, SIGNAL(ui->Ysim->ui->startButton.clicked()),this, SLOT(on_StartButton_clicked()));
}


void SimulatorWindowNeurer::createToolBars()
{
    ui->fileToolBar = addToolBar(tr("File"));
    addToolBar(Qt::RightToolBarArea, ui->fileToolBar);
    ui->fileToolBar->addAction(openAct);
//    ui->fileToolBar->addAction(saveAct);

    ui->simulatorToolBar = addToolBar(tr("Simulation"));
    addToolBar(Qt::RightToolBarArea, ui->simulatorToolBar);
    ui->simulatorToolBar->addAction(startAct);
    ui->simulatorToolBar->addAction(stopAct);
    ui->simulatorToolBar->addAction(stepAct);
//    ui->fileToolBar->addAction(saveAct);

    ui->editToolBar = addToolBar(tr("Edit"));
    addToolBar(Qt::RightToolBarArea, ui->editToolBar);
    /*ui->editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);
    editToolBar->addAction(enableAct);*/
}


void SimulatorWindowNeurer::on_actionAbout_triggered() {
    AboutWidget about;
    about.exec();

/*    mThread->start();
//    std::cerr << "cerr:Thread started up\n";
//    qDebug( "Debug:Thread started up\n");
    // For example you want to stop the thread after 5 seconds
//    QTimer::singleShot(5000, [=]() { mThread->requestInterruption(); });

    std::this_thread::sleep_for(std::chrono::milliseconds(600));
    mThread->next();

    std::this_thread::sleep_for(std::chrono::milliseconds(600));
//    scSimulator* MySim = mThread->Simulator_Get();
    std::this_thread::sleep_for(std::chrono::milliseconds(600));
    */
}


void SimulatorWindowNeurer::closeEvent(QCloseEvent *event)
{
    if (maybeClose()) {
//        writeSettings();    // Write out window-related settings
        event->accept();
/*        if(MyProcessor)
        {
            delete MyProcessor;
            qInfo() << MyProcessor->PrologText_Get().c_str() << "User terminated simulation";
        }
        else
*/            qInfo() << "User terminated simulation";
        qInfo() << "\n-------------------------------\n" << GetAppName().c_str() << " simulator GUI has normally terminated by user";

    } else {
        event->ignore();
    }
}


bool SimulatorWindowNeurer::maybeClose()
{
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                     tr("Closing this window will exit simulation.\n"
                        "Are you sure you want exit?"),
                     QMessageBox::Yes  | QMessageBox::Cancel);
        if (ret == QMessageBox::Yes)
    {
      return true;
    }
        else if (ret == QMessageBox::Cancel)
            return false;
    return false;

}

/**
 * This file reads in the "binary" file to simulate
 * The binary file can be of length 4096 byte
 */

void SimulatorWindowNeurer::open()
{
  if (maybeSave())
  {
/*    textEdit->clear(); // Be sure there is no leftover
    if(MyProcessor)
      delete MyProcessor;
#ifdef EMPA_MODE
    if(MyProcessorWindow) delete MyProcessorWindow;
#else
    if(MyCoreWindow)  delete MyCoreWindow;
#endif
    if(MyMemory)
      delete MyMemory;
*/    QString fileName = QFileDialog::getOpenFileName(this);
  //  DoLoadFile(fileName,8); // Attempt to load object file from 'fileName', and create 8 cores
  }
}

bool SimulatorWindowNeurer::maybeSave()
{
    /*if (textEdit->document()->isModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                     tr("The document has been modified.\n"
                        "Do you want to save your changes?"),
                     QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }*/
    return true;
}

// Start button
void SimulatorWindowNeurer::on_startButton_clicked()
{
//    mThread->next();
}

// Step button
void SimulatorWindowNeurer::on_stepButton_clicked()
{
//    mThread->next();
}

// Stop button
void SimulatorWindowNeurer::on_stopButton_clicked()
{
  //  mThread->Stop = true;
}

