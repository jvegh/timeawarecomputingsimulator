#ifndef SIGNALPLOTWINDOWNEURER_H
#define SIGNALPLOTWINDOWNEURER_H

#include <QMainWindow>
#include "qcustomplot.h"

class SignalPlotWindowNeurer : public QCustomPlot
{
    Q_OBJECT

public:
    explicit SignalPlotWindowNeurer( QWidget *parent);
    ~SignalPlotWindowNeurer();

//    void setupSimpleDemo(QCustomPlot *customPlot);// Just for demo
    void setupRealtimeDataDemo(QCustomPlot *customPlot);
private slots:
  void realtimeDataSlot();
//    void setLupe(const QCPRange &range);

private:
    QTimer dataTimer;
 //   QCustomPlot* navigatorPlot;
};



#endif // SIGNALPLOTWINDOWNEURER.H
