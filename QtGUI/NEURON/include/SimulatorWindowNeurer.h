/** @file SimulatorWindowNeurer.h
 *  @brief The origin of all simulator windows
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#ifndef SimulatorWindowNeurer_H
#define SimulatorWindowNeurer_H

#include <QMainWindow>
#include <QString>
#include <QTextEdit>
#include <QTreeWidget>
//#include <QMouseEvent>
#include <QAbstractItemModel>

//#include "scSimulatorThread.h"
#include "scSimulator.h"
#include "codeeditor.h"
//#include "SimulatorWindow.h"

//#include "treemodelcompleter.h"
class scSimulatorThread;
//class scSimulator;
//class codeeditor;
class treemodelcompleter;

namespace Ui {
class SimulatorWindowNeurer;
}

class SimulatorWindowNeurer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SimulatorWindowNeurer(int argc, char **argv, QWidget *parent = 0);
    virtual ~SimulatorWindowNeurer();
    CodeEditor *codeeditor = new CodeEditor;
     QTreeView* NavigatorWindow;
    scSimulatorThread *mThread;
/*
    Y86SimulatorThread *mThread;
    //https://stackoverflow.com/questions/22485208/redirect-qdebug-to-qtextedit
    EMPARegisterFileWidget *mRegisterFileWidget;
*/
//    QTabWidget* tabWidget, *outTab;
//    TreeModelCompleter* completer;
protected:
    Ui::SimulatorWindowNeurer *ui;

    void SetupSimulatorWindow();
    void SetupHierarchyWindow();
    QAbstractItemModel *modelFromStringList(const QStringList &fileName);

//    bool SetupSimulator(int argc, char **argv);
    void createMenus();
    void createActions();
    void createSimulator();
    QAction *aboutAct, *openAct, *exitAct, *aboutQtAct,
            *startAct, *stopAct, *stepAct;
    void closeEvent(QCloseEvent *event);
    bool maybeClose();
    bool maybeSave();
    void createToolBars();
private slots:

        void open();
    void on_actionAbout_triggered();
    void on_startButton_clicked();
    void on_stepButton_clicked();
    void on_stopButton_clicked();

#if 0
    void on_actionLoadBinaryFile_triggered();
    void on_resetButton_clicked();

    void on_StepWiseBox_clicked();

    void on_actionLoadAssemblyFile_triggered() ;
    #endif
};

#endif // SimulatorWindowNeurer_H


