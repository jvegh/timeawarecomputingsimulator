#ifndef scSIMULATORHREAD_H
#define scSIMULATORHREAD_H

#include "scSimulator.h"
#include <QThread>
//#include "Y86Core.h"


class scSimulatorThread : public QThread
{
    Q_OBJECT
public:
//    explicit Y86SimulatorThread(QObject *parent = 0, bool b = false);
    explicit scSimulatorThread(int argc, char **argv, QObject *parent = 0, bool b = false);
    ~scSimulatorThread();
    void run();
    void next();

    // if Stop = true, the thread will break
    // out of the loop, and will be disposed
    bool Stop;
    void updateSimulatorGUI(scGridPoint* C)
        {  emit updateGUI(C);}
  private:

signals:
    void logMessage(QString);
    void updateGUI(scGridPoint*);
public slots:

};

#endif // scSIMULATORTHREAD_H

