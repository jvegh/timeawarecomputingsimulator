/** @file SignalPlotWindowNeurer.cpp
 *  @brief The origin of all simulator windows
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
/*
#include <QVBoxLayout>
#include <QMenuBar>
#include <QMenu>
#include <QListView>
#include <QTreeView>
#include <QStatusBar>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include "SimulatorWindow.h"
#include "scSimulator.h"
#include "uiSimulatorWindow.h"
//#include "SimulatorThreadBase.h"
#include "aboutwidget.h"
#include "Stuff.h"
#include "codeeditor.h"
#include "Macros.h"
//#include "Config.h"
#include <systemc>
#include "scSimulator.h"
#include "scProcessor.h"
#include "scGridPoint.h"
*/
#include "SignalPlotWindowNeurer.h"

extern bool UNIT_TESTING;
//extern scSimulator* TheSimulator;

SignalPlotWindowNeurer::SignalPlotWindowNeurer(QWidget *parent) :
   QCustomPlot(parent)
{
setupRealtimeDataDemo(this);
//        setupSimpleDemo(this);
 }

SignalPlotWindowNeurer::~SignalPlotWindowNeurer()
{
}

void SignalPlotWindowNeurer::setupRealtimeDataDemo(QCustomPlot *customPlot)
{
// include this section to fully disable antialiasing for higher performance:
/*
customPlot->setNotAntialiasedElements(QCP::aeAll);
QFont font;
font.setStyleStrategy(QFont::NoAntialias);
customPlot->xAxis->setTickLabelFont(font);
customPlot->yAxis->setTickLabelFont(font);
customPlot->legend->setFont(font);
*/
customPlot->addGraph(); // blue line
customPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
customPlot->addGraph(); // red line
customPlot->graph(1)->setPen(QPen(QColor(255, 110, 40)));

QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
timeTicker->setTimeFormat("%h:%m:%s");
customPlot->xAxis->setTicker(timeTicker);
customPlot->axisRect()->setupFullAxesBox();
customPlot->yAxis->setRange(-1.2, 1.2);

// make left and bottom axes transfer their ranges to right and top axes:
connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

// setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
dataTimer.start(0); // Interval 0 means to refresh as fast as possible
}
void SignalPlotWindowNeurer::realtimeDataSlot()
{
  static QTime time(QTime::currentTime());
  // calculate two new data points:
  double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
  static double lastPointKey = 0;
  if (key-lastPointKey > 0.002) // at most add point every 2 ms
  {
    // add data to lines:
    graph(0)->addData(key, qSin(key)+qrand()/(double)RAND_MAX*1*qSin(key/0.3843));
    graph(1)->addData(key, qCos(key)+qrand()/(double)RAND_MAX*0.5*qSin(key/0.4364));
    // rescale value (vertical) axis to fit the current data:
    //ui->customPlot->graph(0)->rescaleValueAxis();
    //ui->customPlot->graph(1)->rescaleValueAxis(true);
    lastPointKey = key;
  }
  // make key axis range scroll with the data (at a constant range size of 8):
  xAxis->setRange(key, 8, Qt::AlignRight);
  replot();

  // calculate frames per second:
/*  static double lastFpsKey;
  static int frameCount;
  ++frameCount;
  if (key-lastFpsKey > 2) // average fps over 2 seconds
  {
    m_parent->statusBar->showMessage(
          QString("%1 FPS, Total Data points: %2")
          .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
          .arg(ui->customPlot->graph(0)->data()->size()+ui->customPlot->graph(1)->data()->size())
          , 0);
    lastFpsKey = key;
    frameCount = 0;

  }
  */
}
