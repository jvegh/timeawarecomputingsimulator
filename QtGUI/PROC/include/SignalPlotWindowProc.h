#ifndef SIGNALPLOTWINDOWPROC_H
#define SIGNALPLOTWINDOWPROC_H

#include <QMainWindow>
#include "qcustomplot.h"

class SignalPlotWindowProc : public QCustomPlot
{
    Q_OBJECT

public:
    explicit SignalPlotWindowProc( QWidget *parent);
    ~SignalPlotWindowProc();

//    void setupSimpleDemo(QCustomPlot *customPlot);// Just for demo
    void setupRealtimeDataDemo(QCustomPlot *customPlot);
private slots:
  void realtimeDataSlot();
//    void setLupe(const QCPRange &range);

private:
    QTimer dataTimer;
 //   QCustomPlot* navigatorPlot;
};



#endif // SIGNALPLOTWINDOWPROC.H
