/** @file SimulatorWindowProc.h
 *  @brief The origin of all simulator windows
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#ifndef SimulatorWindowProc_H
#define SimulatorWindowProc_H

#include <QMainWindow>
#include <QString>
#include <QTextEdit>
#include <QTreeWidget>
//#include <QMouseEvent>
#include <QAbstractItemModel>

#include "scSimulatorThread.h"
#include "scSimulator.h"
#include "codeeditor.h"
//#include "SimulatorWindow.h"

//#include "treemodelcompleter.h"

namespace Ui {
class SimulatorWindowProc;
}

class SimulatorWindowProc : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SimulatorWindowProc(int argc, char **argv, QWidget *parent = 0);
    virtual ~SimulatorWindowProc();
    CodeEditor *codeeditor = new CodeEditor;
     QTreeView* NavigatorWindow;
    scSimulatorThread *mThread;
/*
    Y86SimulatorThread *mThread;
    //https://stackoverflow.com/questions/22485208/redirect-qdebug-to-qtextedit
    EMPARegisterFileWidget *mRegisterFileWidget;
*/
//    QTabWidget* tabWidget, *outTab;
//    TreeModelCompleter* completer;
protected:
    Ui::SimulatorWindowProc *ui;

    void SetupSimulatorWindow();
    void SetupHierarchyWindow();
    QAbstractItemModel *modelFromStringList(const QStringList &fileName);

//    bool SetupSimulator(int argc, char **argv);
    void createMenus();
    void createActions();
    void createSimulator();
    QAction *aboutAct, *openAct, *exitAct, *aboutQtAct,
            *startAct, *stopAct, *stepAct;
//    QMenu *helpMenu, *fileMenu, *simulatorMenu;
    void closeEvent(QCloseEvent *event);
    bool maybeClose();
    bool maybeSave();
/*    void SetHelpMenu(void);
    Y86Processor *mProc;

    void setupExamples();
    void loadBinaryFile(QString filename) ;

    void loadAssemblyFile(QString fileName) ;
*/    void createToolBars();
/*        void
        DisplayProcessorPC(void);
        void
        DisplaySimulatorClock(Y86Core* C);
        void
        DisplaySimulatorFreq(int32_t Val);
        void
        DisplayQTStrings(Y86Core* C);
        */
/*public slots:
    void onValueChanged(int);
*/
/*protected:
        bool mStepWiseMode; // If to wait until user interactively asks for executing the next step
*/
private slots:
#if 0
//    void on_StopButton_clicked();
        void
    updateGUI(Y86Core*);
        void
    on_Parent_QTActivated(int);
        void
    on_Children_QTActivated(int);
/*        void
    on_Parent_QTClicked(QMouseEvent *e);*/
#endif

        void open();
    void on_actionAbout_triggered();
    void on_startButton_clicked();
    void on_stepButton_clicked();
    void on_stopButton_clicked();

#if 0
    void on_actionLoadBinaryFile_triggered();
    void on_resetButton_clicked();

    void on_StepWiseBox_clicked();

    void on_actionLoadAssemblyFile_triggered() ;
    #endif
};

#endif // SimulatorWindowProc_H


