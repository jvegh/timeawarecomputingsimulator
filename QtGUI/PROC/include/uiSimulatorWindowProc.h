/********************************************************************************
** Form generated from reading UI file 'SimulatorWindow.ui'
** but after many unsuccessful attempts, changed to manual edition
** The name is somewhat similar to the QtCreator convention, but is is not rewritten
**
** Created by: Qt User Interface Compiler version 5.9.5, at least the starting point
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIMULATORWINDOW_H
#define UI_SIMULATORWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QTreeView>
#include <QSplitter>
#include <QToolBar>
#include <QTabWidget>
#include <QFileInfo>
#include <QListView> // for testing


#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "SimulatorWindowProc.h"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class CustomPlotWindow: public QCustomPlot
{
    Q_OBJECT

public:
    explicit CustomPlotWindow(QWidget *parent = nullptr){}
};

class GeneralTab : public QWidget
{
    Q_OBJECT

public:
    explicit GeneralTab(const QFileInfo &fileInfo, QWidget *parent = nullptr){}
};

// This is the quasi-thread tab, has nothing to do with Qt
class QTTab : public QWidget
{
    Q_OBJECT

public:
    explicit QTTab(QWidget *parent = nullptr)
    {
    }
};

// This is the simulator tab,
class SimulatorTab : public QWidget
{
    Q_OBJECT

public:
    explicit SimulatorTab(QWidget *parent = nullptr)
    {
    }
};

// This is the memory tab,
class MemoryTab : public QWidget
{
    Q_OBJECT

public:
    explicit MemoryTab(QWidget *parent = nullptr)
    {
    }
};

class UiSimulatorWindowProc
{
protected:
QSplitter *mainsplitter, *leftsplitter, *rightsplitter;
QSplitter *lefttopsplitter, *leftbottomsplitter;
QSplitter *righttopsplitter, *rightbottomsplitter;

public:
    QToolBar *fileToolBar, *simulatorToolBar, *editToolBar;
    QTabWidget *textTab, *graphTab, *controlTab, *navigatorTab;
    QMenuBar *menubar;
    QMenu *fileMenu, *editMenu, *simulatorMenu, *helpMenu;
    QTreeView* mainHierarchyView;
//    QPushButton *StartButton, *StopButton, *StepButton;

public:
// This routine only create UI in general
     void setupUi(QMainWindow *MainWindow)
     {
//         UiSimulatorWindow->setupUi(MainWindow);
         if (MainWindow->objectName().isEmpty())
             MainWindow->setObjectName(QStringLiteral("scqMainProc"));
              MainWindow->resize(1000, 600);
             // On the top level, we have a hprizontal splitter
             mainsplitter = new QSplitter(Qt::Horizontal,MainWindow);
             MainWindow->setCentralWidget(mainsplitter);
             // We have a 3-window splitter
             //| ---------------------- |
             //| Graphic   | Control    |
             //| ---------------------- |
             //| Text      | Navigator  |
             //| ---------------------- |

         // Add  two embedded splitters
             leftsplitter = new QSplitter(mainsplitter);
             leftsplitter->setOrientation(Qt::Vertical);
             rightsplitter = new QSplitter(mainsplitter);
             rightsplitter->setOrientation(Qt::Vertical);
             mainsplitter->addWidget(leftsplitter);
             lefttopsplitter = new QSplitter(leftsplitter);
             lefttopsplitter->setOrientation(Qt::Horizontal);
             leftbottomsplitter = new QSplitter(leftsplitter);
             leftbottomsplitter->setOrientation(Qt::Horizontal);
             leftsplitter->addWidget(lefttopsplitter);
             leftsplitter->addWidget(leftbottomsplitter);

             righttopsplitter = new QSplitter(rightsplitter);
             righttopsplitter->setOrientation(Qt::Horizontal);
             rightbottomsplitter = new QSplitter(rightsplitter);
             rightbottomsplitter->setOrientation(Qt::Horizontal);
             rightsplitter->addWidget(righttopsplitter);
             rightsplitter->addWidget(rightbottomsplitter);
             mainsplitter->addWidget(rightsplitter);

            // Load embedded splitters
             graphTab  = new QTabWidget;
             graphTab->setTabPosition(QTabWidget::North);
             graphTab->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
             lefttopsplitter->addWidget(graphTab);

             //   The text tab contains textual information, such as messages, source cores, settings
             textTab  = new QTabWidget;
             textTab->setTabPosition(QTabWidget::South);
             leftbottomsplitter->addWidget(textTab);

             // The control tab contains control widgets from  the simulator
             controlTab  = new QTabWidget;
             controlTab->setTabPosition(QTabWidget::North);
             righttopsplitter->addWidget(controlTab);

             // The navigation tab contains hierarcyhy three, etc
             navigatorTab  = new QTabWidget;
             navigatorTab->setTabPosition(QTabWidget::South);
             rightbottomsplitter->addWidget(navigatorTab);
            // Create a simple text edit window for logging
             scqSimulator::s_LogWindow = new QTextEdit;
             textTab->addTab(scqSimulator::s_LogWindow,"Log");
             mainHierarchyView = new QTreeView;  // Show hierarchy here
             navigatorTab->addTab(mainHierarchyView,"Main hierarchy");

             //Crate three toolbars
             fileToolBar = new QToolBar;
             editToolBar = new QToolBar;
             simulatorToolBar = new QToolBar;
             //    showMaximized();
             MainWindow->statusBar()->showMessage("Ready to operate");

        // The setup is done by the main window


             // The control splitter
     /*
             QFont font1;
             font1.setPointSize(8);
             dirTreeView = new QTreeView(centralwidget);
             dirTreeView->setObjectName(QStringLiteral("dirTreeView"));
             dirTreeView->resize(500,300);

             */
 }

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
//        StartButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
//        StopButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SimulatorWindowProc: public UiSimulatorWindowProc {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UISIMULATORWINDOW_H
