#include "Project.h"
#include "scSimulatorThread.h"
#include <QDebug>
#include <QMutex>
#include <iostream>
#include <string>
#include "scSimulator.h"
#include "scProcessor.h"
//extern ostream cerr;

// This simulator may be used from scGridPoint up
extern scqSimulator* TheSimulator;
using namespace std;

scSimulatorThread::scSimulatorThread(int argc, char **argv, QObject *parent, bool b ) :
    QThread(parent), Stop(b)
{
    TheSimulator = new scqSimulator("GUI", argc, argv);
//    TheSimulator = new scSimulator(argc, argv[1], 60, "GUI");
    // The GUI sets the flag, i.e. reaching any NEXT_thread in the cores,
    // the simulator auto-suspends itself; must be resumed to continue
//    TheSimulator->StepwiseMode_Set(true);
    run();// Temporary!!
}

scSimulatorThread::~scSimulatorThread()
{
    if(isRunning())
    {
        // Stop our loop
        requestInterruption();

        // Waits until return from run()
        wait();
    }
}

/*!
 * \brief Y86SimulatorThread::run
 *
 * Sends a 'start' event to the processor
 */

void scSimulatorThread::run()
{
//    int it = 0; Y86Core* C;
    int it = 0;
    scGridPoint* C;
    bool Ready = false;

    while(!isInterruptionRequested() && !Ready)
    {

        sc_start(MAX_CLOCK_CYCLES,SC_NS);
 //       end = chrono::steady_clock::now();
        std::cerr << "INFO: Exiting " << PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC simulation" << endl;
        if (not sc_end_of_simulation_invoked()) sc_stop(); //< invoke end_of_simulation
        Ready = true;

        // the line below will enqueue some call to the GUI thread
        // no event loop in the sender thread is needed
        emit logMessage(QString::number(it++));
        //    msSim->Processor_Get()->EVENT_PROCESSOR.START.notify(SCTIME_CLOCKTIME);
//        C = TheSimulator->Processor_Get()->ClusterHead_Get(it);
//        msleep(500);
//        if(it%3)
            updateSimulatorGUI(C);
    }
}

/*!
 * \brief Y86SimulatorThread::next
 *
 * Sends a 'next' signal to the cores that are suspended in 'NEXT_thread()'
 *
 */
void scSimulatorThread::next()
{
    emit logMessage("Command 'next' received");
//    TheSimulator->Processor_Get()->EVENT_PROCESSOR.RESUME.notify(SCTIME_CLOCKTIME);
}

