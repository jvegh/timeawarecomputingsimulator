#include <QVBoxLayout>
#include <QMenuBar>
#include <QMenu>
#include <QListView>
#include <QTreeView>
#include <QStatusBar>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include "SimulatorWindowProc.moc"
#include "SimulatorWindowProc.h"
#include "scSimulator.h"
#include "uiSimulatorWindowProc.h"
#include "SignalPlotWindowProc.h"
//#include "SimulatorThreadProc.h"
#include "aboutwidget.h"
#include "Stuff.h"
#include "codeeditor.h"
#include "Macros.h"
//#include "Config.h"
#include <systemc>
#include "scSimulator.h"
#include "scProcessor.h"
#include "scGridPoint.h"

/*
#include <chrono> // for sleep only
#include <thread> // for sleep only
 */
extern bool UNIT_TESTING;
extern scClusterBusMemorySlow *MainMemory;
extern scSimulator* TheSimulator;
// https://stackoverflow.com/questions/22485208/redirect-qdebug-to-qtextedit

SimulatorWindowProc::SimulatorWindowProc(int argc, char **argv, QWidget *parent) :
    QMainWindow(parent),
     ui(new Ui::SimulatorWindowProc)
{
    ui->setupUi(this);  // This creates the basic splitters
    setWindowTitle(QString(GetAppName().c_str())+"/Proc");
    setWindowIcon(QIcon(":/images/Proclogo.png"));
  //   scqSimulator::s_LogWindow = new QTextEdit;
//    HierarchyView = new QTreeView;  // Show hierarchy here
//??    scqSimulator::s_LogWindow->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded);
//??    scqSimulator::s_LogWindow->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded);
    SetupSimulatorWindow(); // Creates sizers and some basic windows


    createMenus();
    createActions();
    createToolBars();
    createSimulator();
    // Now we are ready with the  GUI, deal with the simulation
    // Set up the processor simulator
#if 0
     mThread = new scSimulatorThread(argc,argv,
             this,true);
     if(!mThread)
     {  // Starting the simulator thread failed
         QMessageBox::critical(this, tr("Application"),
                      tr("Starting the simulator thread failed"),
                      QMessageBox::Ok);
     }
     if(!TheSimulator)
     {  // Starting processor failed
         QMessageBox::critical(this, tr("Application"),
                      tr("Starting the Proc simulator failed"),
                      QMessageBox::Ok);
     }
     mProc = TheSimulator->Processor_Get();
     if(!mProc)
     {  // Starting processor failed
         QMessageBox::critical(this, tr("Application"),
                      tr("Starting the Proc processor failed"),
                      QMessageBox::Ok);
     }
     if((1==argc) || MEMORY_DEFAULT_ADDRESS==TheSimulator->Processor_Get()->PC_Get())
     {   // Could not load the file provided in argv[1]
         if(1==argc)
             QMessageBox::warning(this, "Warning", "No file was provided on command line");
         else
             QMessageBox::warning(this, "Warning", QString(("The file '" + string(argv[1]) + "could not be loaded").c_str()));
         // Maybe offer some example
     }
     else
     {  // Fine, we do have a a loaded binary file
         // Read it again, for the editor
         QFile file(argv[1]);
          if (!file.open(QIODevice::ReadOnly | QFile::Text)) {
              QMessageBox::warning(this, "Warning", "Cannot open file: " + file.errorString());
              return;
          }
          QTextStream in(&file);
          QString text = in.readAll();
         codeeditor->reset();
         codeeditor->setPlainText(text);
         file.close();
         // Now the file is read and displayed
         uint32_t  PC = TheSimulator->Processor_Get()->PC_Get();
         uint32_t LineNo = MainMemory->findLinenoToAddress(PC);
         codeeditor->setCursorToLine(LineNo);
         DisplayProcessorPC();
 //!!        DisplaySimulatorClock(NULL);
 //        DisplaySimulatorFreq(456);
         mThread->start();
         TheSimulator->Processor_Get()->EVENT_PROCESSOR.START.notify(SCTIME_CLOCKTIME);
         std::cerr << "cerr:Thread started up\n";
         qDebug( "Debug:Thread started up\n");
     }

    // Enable the simulator thread to send a log message
    connect(mThread, SIGNAL(logMessage(QString)),
                     s_textEdit, SLOT(append(QString)), Qt::QueuedConnection);
    // Enable the simulator to request GUI update
    connect(mThread, SIGNAL(updateGUI(Y86Core*)),
                     this, SLOT(updateGUI(Y86Core*)), Qt::QueuedConnection);
    // Connect to 'parent' display
    connect(ui->Y86->QTList_Parent_Get(), SIGNAL(activated(int)),
                     this,SLOT(on_Parent_QTActivated(int)));
    // Connect to 'parent' display
    connect(ui->Y86->QTList_Children_Get(), SIGNAL(activated(int)),
                     this,SLOT(on_Children_QTActivated(int)));

    connect(ui->Ysim, SIGNAL(ui->StopButton_Get()->click()),
     this,SLOT(on_stopButton_clicked()));
    connect(ui->Ysim, SIGNAL(ui->Ysim->ui->resetButton_get()->click()),
     this,SLOT(on_resetButton_clicked()));
    connect(ui->Ysim, SIGNAL(ui->Ysim->ui->StepWiseBox->click()),
     this,SLOT( on_StepWiseBox_clicked()));
    setupExamples();    // Set up the example files
#endif

//    QPushButton *, *StopButton, *StepButton;

 }

SimulatorWindowProc::~SimulatorWindowProc()
{
 //   delete ui; // Done in anchestor
}
/*!
 * \brief Setup graphic facilities of the simulator window.
 * Calls more virtual functions to set up its splitters and tabs
 */

void SimulatorWindowProc::SetupSimulatorWindow()
{
    // The graphic tab contains visual outputs from  the simulator
 //   ui->graphTab->addTab(new CustomPlotWindow, QApplication::translate("SignalWindow","Signals", Q_NULLPTR));
    // The graphic tab contains visual outputs from  the simulator
    ui->graphTab->addTab(new SignalPlotWindowProc(this), QApplication::translate("SignalWindow","Signals", Q_NULLPTR));
    ui->graphTab->addTab(new SignalPlotWindowProc(this), QApplication::translate("StateWindow","States", Q_NULLPTR));


   //   The text tab contains textual information, such as messages, source cores, settings
    ui->textTab->addTab(scqSimulator::s_LogWindow, QApplication::translate("Text","Messages", Q_NULLPTR));
    ui->textTab->addTab(new CodeEditor, QApplication::translate("Text","Source Code", Q_NULLPTR));

   // The control tab contains control widgets from  the simulator
//        controlTab->addTab(new CustomPlotWindow, QApplication::translate("ControlWindow","Control", Q_NULLPTR));
    ui->controlTab->addTab(new CodeEditor, QApplication::translate("Control","Controls", Q_NULLPTR));

    // The navigation tab contains hierarchy three, etc
    NavigatorWindow = new QTreeView;

    ui->navigatorTab->addTab(NavigatorWindow, QApplication::translate("Navigator","Hierarchy", Q_NULLPTR));
    SetupHierarchyWindow();
//    NavigatorWindow->setModel(completer->model());
}

void SimulatorWindowProc::SetupHierarchyWindow()
{
    //completer = new TreeModelCompleter(this);
 /*   const QStringList list {"Parent1\n",
                            "\tChild1\n",
                                "Parent1.Child1.GrandChild1\n",
                                "\t\tGrandChild2\n",
                                "\t\tGrandChild3\n",
                                    "\t\tGrandGrandChild1\n",
                            "\tChild2\n",
                                "\t\tGrandChild1\n",
                                    "\t\t\tGrandGrandChild1\n",
                                "\t\tGrandChild2\n",
                            "\tChild3\n",
                        "\n",
                            "Parent2\n",
                            "\tChild1\n",
                            "\t\tGrandChild1\n",
                            "\tChild2\n",
                            "\tChild3\n",
                            "\t\tGrandChild1\n",
                            "\t\tGrandChild2\n" };*/
    const QStringList list {"Parent1\n",
                            "Parent1.Child1\n",
                            "Parent1.Child1.GrandChild1\n",
                                "Parent1.Child1.GrandChild2\n",
                                "Parent1.Child1.GrandChild3\n",
                                    "Parent1.Child1.GrandChild3.GrandGrandChild1\n",
                            "Parent1.Child2\n",
                                "Parent1.Child2.GrandChild1\n",
                                    "Parent1.Child2.GrandChild1.GrandGrandChild1\n",
                                "Parent1.Child2.GrandChild2\n",
                            "Parent1.Child3\n",
                        "\n",
                            "Parent2\n",
                            "Parent2.Child1\n",
                            "Parent2.Child1.GrandChild1\n",
                            "Parent2.Child2\n",
                            "Parent2.Child3\n",
                            "Parent2.Child3.GrandChild1\n",
                            "Parent2.Child3.GrandChild2\n" };
        //completer->setModel(modelFromStringList(list));
//    QObject::connect(completer, QOverload<const QModelIndex &>::of(&TreeModelCompleter::highlighted),
//                     this, &MainWindow::highlight);

    /*
     * NavigatorWindow->setColumnCount(2);
//    NavigatorWindow->setCheckState(1,Qt::PartiallyChecked);

    QList<QTreeWidgetItem *> items;
    QString name("NameA");
    QString description("Description");
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem(NavigatorWindow);
    treeItem->setCheckState(1,Qt::PartiallyChecked);
    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);
    treeItem->setText(1, description);
//    addTreeChild(treeItem, name + "A", "Child_first");
 //   addTreeChild(treeItem, name + "B", "Child_second");
    QTreeWidgetItem *childItem = new QTreeWidgetItem();

    // QTreeWidgetItem::setText(int column, const QString & text)
     childItem->setText(0, name);
     childItem->setText(1, description);

     // QTreeWidgetItem::addChild(QTreeWidgetItem * child)
     treeItem->addChild(childItem);

    for (int i = 0; i < 10; ++i)
    {
        items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), QStringList(QString("item: %1").arg(i))));
        ((QTreeWidgetItem *)(items.at(i)))->setCheckState(1,Qt::PartiallyChecked);
    }
    NavigatorWindow->insertTopLevelItems(0, items);
*/
}

void SimulatorWindowProc::
createSimulator()
{

}


QAbstractItemModel *SimulatorWindowProc::modelFromStringList(const QStringList &fileName)
{
#if 0 //
    completer->setSeparator(QLatin1String("."));
    QStandardItemModel *model = new QStandardItemModel(completer);
    QVector<QStandardItem *> parents(10);
    parents[0] = model->invisibleRootItem();

    QRegularExpression re("^\\s+");
 //   while (!file.atEnd()) {
    for ( const auto& i : fileName  )
    {
        const QString line = i.trimmed();
        const QString trimmedLine = line.trimmed();
        if (trimmedLine.isEmpty())
            continue;

        const QRegularExpressionMatch match = re.match(line);
        int nonws = match.capturedStart();
        int level = 0;
        if (nonws == -1) {
            level = 0;
        } else {
            const int capLen = match.capturedLength();
            level = line.startsWith(QLatin1Char('\t')) ? capLen / 4 : capLen;
        }

        if (level + 1 >= parents.size())
            parents.resize(parents.size() * 2);

        QStandardItem *item = new QStandardItem;
        item->setText(trimmedLine);
        parents[level]->appendRow(item);
        parents[level + 1] = item;
    }
    return model;


/*
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
        return new QStringListModel(completer);

#ifndef QT_NO_CURSOR
    QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
#endif

    QStandardItemModel *model = new QStandardItemModel(completer);
    QVector<QStandardItem *> parents(10);
    parents[0] = model->invisibleRootItem();

    QRegularExpression re("^\\s+");
    while (!file.atEnd()) {
        const QString line = QString::fromUtf8(file.readLine()).trimmed();
        const QString trimmedLine = line.trimmed();
        if (trimmedLine.isEmpty())
            continue;

        const QRegularExpressionMatch match = re.match(line);
        int nonws = match.capturedStart();
        int level = 0;
        if (nonws == -1) {
            level = 0;
        } else {
            const int capLen = match.capturedLength();
            level = line.startsWith(QLatin1Char('\t')) ? capLen / 4 : capLen;
        }

        if (level + 1 >= parents.size())
            parents.resize(parents.size() * 2);

        QStandardItem *item = new QStandardItem;
        item->setText(trimmedLine);
        parents[level]->appendRow(item);
        parents[level + 1] = item;
    }

#ifndef QT_NO_CURSOR
    QGuiApplication::restoreOverrideCursor();
#endif
*/
//VJ    return model;
    return model;
#endif //0
    return nullptr;
}

#if 0
/*!
 * \brief SimulatorWindowY86::updateGUI
 * Update the GUI via reading the simulator
 */
//static int GUI_i=0;
void SimulatorWindowY86::
updateGUI(Y86Core* C)
{
    // Do not update what the user lot looking at
    switch(tabWidget->currentIndex())
    {
        case 0: // The QT tab
        DisplayQTStrings(C);
            break;
    case 1: // The memory tab
        QMessageBox::information(this, tr("Application"),
                       tr("Memory tab"),
                       QMessageBox::Ok);
        break;
    case 2: // Simulator tab
        DisplaySimulatorClock(C);
        break;
    case 3: // Processor tab
        QMessageBox::information(this, tr("Application"),
                       tr("Processor tab"),
                       QMessageBox::Ok);
        break;
     default:
        QMessageBox::information(this, tr("Application"),
                       tr("Wrong tab"),
                       QMessageBox::Ok);
        break;
    }
}
#endif //0
void SimulatorWindowProc::
createActions()
{


    startAct = new QAction(QIcon(":/images/start.jpeg"), tr("&Start simulation"), this);
    stopAct->setStatusTip(tr("Start simulation"));
    stepAct = new QAction(QIcon(":/images/step.png"), tr("Make a &Step"), this);
    stepAct->setStatusTip(tr("Start next step"));
    stopAct = new QAction(QIcon(":/images/stop.jpeg"), tr("&Stop simulation"), this);
    stopAct->setStatusTip(tr("Stop simulation"));
/*  saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
  saveAct->setShortcuts(QKeySequence::Save);
  saveAct->setStatusTip(tr("Save the document to disk"));
  connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

  saveAsAct = new QAction(tr("Save &As..."), this);
  saveAsAct->setShortcuts(QKeySequence::SaveAs);
  saveAsAct->setStatusTip(tr("Save the document under a new name"));
  connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));
*/

/*  cutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
  cutAct->setShortcuts(QKeySequence::Cut);
  cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                          "clipboard"));
//    connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

  copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
  copyAct->setShortcuts(QKeySequence::Copy);
  copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                           "clipboard"));
//    connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

  pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
  pasteAct->setShortcuts(QKeySequence::Paste);
  pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                            "selection"));
//    connect(pasteAct, SIGNAL(triggered()), textEdit, SLOT(paste()));

  enableAct = new QAction(QIcon(":/images/edit.png"), tr("&Enable"), this);
//    pasteAct->setShortcuts(Qt::CTRL + Qt::Key_E);
  enableAct->setStatusTip(tr("Enable editing contents"));
  connect(enableAct, SIGNAL(triggered()), this, SLOT(enable()));
*/

/*    guideAct = new QAction(tr("&Guide"), this);
  guideAct->setStatusTip(tr("Show the application's Guide"));
  connect(guideAct, SIGNAL(triggered()), this, SLOT(guide()));


  cutAct->setEnabled(false);
  copyAct->setEnabled(false);
  connect(textEdit, SIGNAL(copyAvailable(bool)),
          cutAct, SLOT(setEnabled(bool)));
  connect(textEdit, SIGNAL(copyAvailable(bool)),
          copyAct, SLOT(setEnabled(bool)));
*/
/*   helpAct = new QAction(tr("Help Contents..."), this);
  helpAct->setShortcut(QKeySequence::HelpContents);
  connect(helpAct, SIGNAL(triggered()),
          helpWindow, SLOT(show()));
*/
}

void SimulatorWindowProc::createMenus()
{
    ui->fileMenu = menuBar()->addMenu(tr("&File"));
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    ui->fileMenu->addAction(openAct);
//     fileMenu->addAction(saveAct);
//     fileMenu->addAction(saveAsAct);
    ui->fileMenu->addSeparator();
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    ui->fileMenu->addAction(exitAct);


/*    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);
    editMenu->addAction(enableAct);

    preferenceMenu = menuBar()->addMenu(tr("&Preference"));
//    referenceMenu->addAction(procNoAct);
//    editMenu->addAction(copyAct);

*/    menuBar()->addSeparator();

/*    helpMenu->addAction(helpAct);
    helpMenu->addSeparator();
    connect(helpAct, SIGNAL(triggered()), helpWindow, SLOT(show()));
*/
    ui->simulatorMenu = menuBar()->addMenu(tr("&Simulator"));
    startAct = new QAction(QIcon(":/images/start.png"), tr("&Start"), this);
    startAct->setStatusTip(tr("Start simulation"));
    ui->simulatorMenu->addAction(startAct);
    connect(startAct, SIGNAL(triggered()),
            this, SLOT(on_startButton_clicked()));
    stepAct = new QAction(QIcon(":/images/step.png"), tr("&Step"), this);
    stepAct->setStatusTip(tr("Make a simulation step"));
    ui->simulatorMenu->addAction(stepAct);
    connect(stepAct, SIGNAL(triggered()),
            this, SLOT(on_stepButton_clicked()));
    stopAct = new QAction(QIcon(":/images/stop.png"), tr("&Stop"), this);
    stopAct->setStatusTip(tr("Stop simulation"));
    ui->simulatorMenu->addAction(stopAct);
    connect(stopAct, SIGNAL(triggered()),
            this, SLOT(on_stopButton_clicked()));

    ui->helpMenu = menuBar()->addMenu(tr("&Help"));
    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(on_actionAbout_triggered()));
    ui->helpMenu->addAction(aboutAct);
    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
//    startAct = new QAction(tr("&About"), this);

//    connect(startAct, SIGNAL(ui->Ysim->ui->startButton.clicked()),this, SLOT(on_StartButton_clicked()));
}


void SimulatorWindowProc::createToolBars()
{
    ui->fileToolBar = addToolBar(tr("File"));
    addToolBar(Qt::RightToolBarArea, ui->fileToolBar);
    ui->fileToolBar->addAction(openAct);
//    ui->fileToolBar->addAction(saveAct);

    ui->simulatorToolBar = addToolBar(tr("Simulation"));
    addToolBar(Qt::RightToolBarArea, ui->simulatorToolBar);
    ui->simulatorToolBar->addAction(startAct);
    ui->simulatorToolBar->addAction(stopAct);
    ui->simulatorToolBar->addAction(stepAct);
//    ui->fileToolBar->addAction(saveAct);

    ui->editToolBar = addToolBar(tr("Edit"));
    addToolBar(Qt::RightToolBarArea, ui->editToolBar);
    /*ui->editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);
    editToolBar->addAction(enableAct);*/
}


void SimulatorWindowProc::on_actionAbout_triggered() {
    AboutWidget about;
    about.exec();

/*    mThread->start();
//    std::cerr << "cerr:Thread started up\n";
//    qDebug( "Debug:Thread started up\n");
    // For example you want to stop the thread after 5 seconds
//    QTimer::singleShot(5000, [=]() { mThread->requestInterruption(); });

    std::this_thread::sleep_for(std::chrono::milliseconds(600));
    mThread->next();

    std::this_thread::sleep_for(std::chrono::milliseconds(600));
//    scSimulator* MySim = mThread->Simulator_Get();
    std::this_thread::sleep_for(std::chrono::milliseconds(600));
    */
}


void SimulatorWindowProc::closeEvent(QCloseEvent *event)
{
    if (maybeClose()) {
//        writeSettings();    // Write out window-related settings
        event->accept();
/*        if(MyProcessor)
        {
            delete MyProcessor;
            qInfo() << MyProcessor->PrologText_Get().c_str() << "User terminated simulation";
        }
        else
*/            qInfo() << "User terminated simulation";
        qInfo() << "\n-------------------------------\n" << GetAppName().c_str() << " simulator GUI has normally terminated by user";

    } else {
        event->ignore();
    }
}


bool SimulatorWindowProc::maybeClose()
{
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                     tr("Closing this window will exit simulation.\n"
                        "Are you sure you want exit?"),
                     QMessageBox::Yes  | QMessageBox::Cancel);
        if (ret == QMessageBox::Yes)
    {
      return true;
    }
        else if (ret == QMessageBox::Cancel)
            return false;
    return false;

}

/**
 * This file reads in the "binary" file to simulate
 * The binary file can be of length 4096 byte
 */

void SimulatorWindowProc::open()
{
  if (maybeSave())
  {
/*    textEdit->clear(); // Be sure there is no leftover
    if(MyProcessor)
      delete MyProcessor;
#ifdef EMPA_MODE
    if(MyProcessorWindow) delete MyProcessorWindow;
#else
    if(MyCoreWindow)  delete MyCoreWindow;
#endif
    if(MyMemory)
      delete MyMemory;
*/    QString fileName = QFileDialog::getOpenFileName(this);
  //  DoLoadFile(fileName,8); // Attempt to load object file from 'fileName', and create 8 cores
  }
}

bool SimulatorWindowProc::maybeSave()
{
    /*if (textEdit->document()->isModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                     tr("The document has been modified.\n"
                        "Do you want to save your changes?"),
                     QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }*/
    return true;
}

// Start button
void SimulatorWindowProc::on_startButton_clicked()
{
//    mThread->next();
}

// Step button
void SimulatorWindowProc::on_stepButton_clicked()
{
//    mThread->next();
}

// Stop button
void SimulatorWindowProc::on_stopButton_clicked()
{
  //  mThread->Stop = true;
}



#if 0
bool SimulatorWindowY86::SetupSimulator(int argc, char **argv)
{
/*auto start = chrono::steady_clock::now();
 auto absolutestart = start;
 //??QApplication MainApp(argc, argv);
 // Suppress warnings about some deprecated features
 sc_core::sc_report_handler::set_actions( "/IEEE_Std_1666/deprecated",
                                          sc_core::SC_DO_NOTHING );
 std::cerr << "INFO: Elaborating "<< simulation_name<< endl;
 sc_set_time_resolution(SCTIME_RESOLUTION);
 std::cerr << "INFO: Preparing " << PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC simulation" << endl;
//  GUI_MODE = false;    // This is the text version of the application
// This program is expected to be started with a file name and number of cores
 CheckArgumentList(argc, argv);	//?? There must be 3 args
//   OBJECT_FILE_PRINTED = false;
 ostringstream Heading;
 Heading << GetAppName() << " SystemC simulator CLI version has started";
 string LogFileNamePrefix = "Log_"+GetFileNameRoot(string(argv[1]))+"_"+string(argv[2])+"_"; // Prepare log file name


//    OpenSystemFiles(string(argv[1]).c_str(), LogFileNamePrefix.c_str(), Heading.str());
 ostringstream StartString,NameString;
 NameString << "scSimulator "
     << GetAppName().c_str();
 StartString << NameString.str() << " has been started as:\n'";
 for(int i = 0; i<3; i++)
 {
   StartString <<argv[i] << " ";
 }
 StartString << "'";
 qInfo() << StartString.str().c_str();
//    SetupSystemDirectories(NULL,PROJECT_NAME,PROJECT_VERSION); // Establish system and user directories, with no main window
//    SetupSettings(PROJECT_NAME);
 char* pEnd;
 int NCores = strtol(argv[2], &pEnd, 10);
//   if(NCores<0 || NCores>MAX_NUMBER_OF_GRIDPOINTS)
 {
//     std::cerr << "No of cores was "<< NCores <<", is " << NUMBER_OF_CORES << " must be between 1 and " << NCores;
   NCores = NUMBER_OF_GRIDPOINTS;
 }
 ifstream infile(argv[1], ifstream::in);
 if(!infile.good())
 {
   LOG_CRITICAL("File '"<< argv[1] <<"' seems to be in error");
   return -2;
 }
//    std::cerr << "Elapsed time for preparing simulation "
//            << chrono::duration_cast<chrono::milliseconds>(end - start).count()
//            << " msec" << endl;
//   start = chrono::steady_clock::now();
/ *   if(!OBJECT_FILE_PRINTED)
  LOG_INFO("Starting to load object data from file '" << argv[1] << "'");* /
//   LOG_INFO(StartString.str().c_str());
*/
    int NCores = 2;
    TheSimulator = new Y86ProcessorSimulator(argc, argv[1], NCores);
    if(MEMORY_DEFAULT_ADDRESS==msSim->Processor_Get()->PC_Get())
    {   // Could not load the file provided in argv[1]
       return false;
    }
    else
    {   //
        return true;
    }

/*
//   std::cout << "Preparation time " <<  elapsed*1000 << "ms" << std::endl;
 int returnValue= 0;
    //Run the Simulation for "MAX_CLOCK_CYCLES nanosecnds"
 //    elapsed = stopwatch.elapsed();
     auto end = chrono::steady_clock::now();
 //end = chrono::steady_clock::now();
 std::cerr << "Elapsed time for setting up simulation "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " msec" << endl;
 start = chrono::steady_clock::now();
 sc_start(MAX_CLOCK_CYCLES,SC_NS);
 end = chrono::steady_clock::now();
 std::cerr << "INFO: Exiting " << PROJECT_NAME << " V" << PROJECT_VERSION << " SystemC simulation" << endl;
 if (not sc_end_of_simulation_invoked()) sc_stop(); //< invoke end_of_simulation
 std::cerr << "Elapsed time for performing simulation "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count()
         << " msec" << endl;
 start = chrono::steady_clock::now();
 delete msSim;
 start = chrono::steady_clock::now();
 CloseSystemFiles(string(NameString.str().c_str()) + " simulator CLI version has terminated");
 end = chrono::steady_clock::now();
 std::cerr << "Elapsed time for cleaning up simulation "
         << chrono::duration_cast<chrono::microseconds>(end - start).count()
         << " µs" << endl;
 std::cerr << "Total elapsed time for simulation "
         << chrono::duration_cast<chrono::milliseconds>(end - absolutestart).count()
         << " msec" << endl;
         */
}// SetupSystemC

// Start button
void SimulatorWindowY86::on_resetButton_clicked()
{
    // The SC enging must be killed, so the thread is terminated and restarted
    mThread->requestInterruption(); // Kill the thread
    std::this_thread::sleep_for(std::chrono::milliseconds(10)); // Just be sure it happened
    mThread->start();   // Start over
}



void SimulatorWindowY86::SetHelpMenu(void)
{
    aboutAct = new QAction( this);
aboutAct->setText( tr( "&About" ) );
/*fileNewWindowAction->setIcon( QIcon( ":/images/window_new.png") );
fileNewWindowAction->setShortcut(QKeySequence::New);
fileNewWindowAction->setShortcuts(QKeySequence::New);
*/
    aboutAct->setStatusTip(tr("Show the 'about' box"));
connect( aboutAct, SIGNAL( triggered() ), this, SLOT( on_actionAbout_triggered() ) );
}


void SimulatorWindowY86::setupExamples() {
    // This is a system subdirectory, generated by CMake
    string MyData = string(SystemDirectories_Get()->SystemData);
//    string MyData = string(SystemDirectories_Get()->UserData);
    QString bE((MyData+("/examples/Y86/binary/")).c_str());
    QString aE((MyData+("/examples/Y86/assembly/")).c_str());

    auto binaryExamples = QDir(bE).entryList(QDir::Files);
    auto assemblyExamples = QDir(aE).entryList(QDir::Files);
//    auto binaryExamples = QDir(":/examples/binary/").entryList(QDir::Files);
//    auto assemblyExamples = QDir(":/examples/assembly/").entryList(QDir::Files);

    // Load examples
    if (!binaryExamples.isEmpty()) {
        auto* binaryExampleMenu = new QMenu();
        binaryExampleMenu->setTitle("Binary");
        for (const auto& fileName : binaryExamples) {
            binaryExampleMenu->addAction(fileName,
                                         [=] { this->loadBinaryFile(QString(":/examples/binary/") + fileName); });
        }
        // Add binary example menu to example menu
        fileMenu->addMenu(binaryExampleMenu);
 //       connect(aboutAct, SIGNAL(triggered()), this, SLOT(on_actionAbout_triggered()));
//        connect(m_ui->programfiletab, &ProgramfileTab::loadBinaryFile, this,
//                &MainWindow::on_actionLoadBinaryFile_triggered);
    }

    if (!assemblyExamples.isEmpty()) {
        auto* assemblyExampleMenu = new QMenu();
        assemblyExampleMenu->setTitle("Assembly");
        for (const auto& fileName : assemblyExamples) {
            assemblyExampleMenu->addAction(fileName,
                                           [=] { this->loadAssemblyFile(QString(":/examples/assembly/") + fileName); });
        }
        // Add binary example menu to example menu
        fileMenu->addMenu(assemblyExampleMenu);
/*??        connect(ui->programfiletab, &ProgramfileTab::loadAssemblyFile, this,
                &MainWindow::on_actionLoadAssemblyFile_triggered);
                */
    }
}

void SimulatorWindowY86::on_actionLoadBinaryFile_triggered() {
    auto filename = QFileDialog::getOpenFileName(this, "Open binary file", "", "Binary file (*)");
    if (!filename.isNull())
        loadBinaryFile(filename);
}

void SimulatorWindowY86::on_actionLoadAssemblyFile_triggered() {
    auto filename = QFileDialog::getOpenFileName(this, "Open assembly file", "", "Assembly file (*.s *.as *.asm)");
    if (!filename.isNull())
        loadAssemblyFile(filename);
}


void SimulatorWindowY86::loadBinaryFile(QString filename) {
/*    m_ui->programfiletab->setTimerEnabled(false);
    m_ui->programfiletab->setInputMode(false);
    m_ui->processortab->restart();
    Parser::getParser()->loadBinaryFile(filename);
    m_ui->programfiletab->setDisassemblerText();
    emit update();*/
}

void SimulatorWindowY86::loadAssemblyFile(QString fileName) {
    // ... load file
    QFile file(fileName);
/*    m_ui->programfiletab->setInputMode(true);
    m_ui->programfiletab->setTimerEnabled(true);
    Parser::getParser()->clear();
    m_ui->programfiletab->clearOutputArray();
    m_ui->processortab->restart();
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        m_ui->programfiletab->setAssemblyText(file.readAll());
        file.close();
    }
    m_ui->programfiletab->setDisassemblerText();*/
}
void SimulatorWindowY86::DisplayProcessorPC(void)
    {ui->Ysim->WidgetPC->setValue(mProc->PC_Get());}
void SimulatorWindowY86::DisplaySimulatorClock(Y86Core*C)
    {ui->Ysim->WidgetClock->setValue(
                C->ClusterAddress_Get().Cluster*100+C->ClusterAddress_Get().Member
                                     );}
void SimulatorWindowY86::DisplaySimulatorFreq(int32_t Val)
    {ui->Ysim->WidgetFreq->setValue(Val);}

/*!
 * \brief SimulatorWindowY86::on_Children_QTActivated
 * \param index
 * Change QT tab display to the requested child
 */
void SimulatorWindowY86::
on_Children_QTActivated(int index)
{

}

/*!
 * \brief SimulatorWindowY86::on_Parent_QTActivated
 * \param index
 * Change QT tab display to the parent
 */

void SimulatorWindowY86::
on_Parent_QTActivated(int index)
{

}

/*!
 * \brief SimulatorWindowY86::DisplayQTStrings
 * \param C the actual core
 *
 * Display the string of the actual, its parent and children
 */
void SimulatorWindowY86::DisplayQTStrings(Y86Core* C)
{
    ui->Y86->QTList_Get()->clear();
    ui->Y86->QTList_Parent_Get()->clear();
    ui->Y86->QTList_Children_Get()->clear();
    if(mProc->AllocatedMask_Get())
    {   // Some cores are allocated
        ui->Y86->QTList_Get()->addItem(QString(C->StringOfClusterAddress_Get().c_str()));
        Y86Core* P = C->Parent_Get();
        if(P)
            ui->Y86->QTList_Parent_Get()->addItem(QString(C->StringOfClusterAddress_Get().c_str()));
        else
            ui->Y86->QTList_Parent_Get()->addItem("Processor");
        SC_CORE_MASK_TYPE CM = C->ChildrenMask_Get();
        if(CM)
        {   int index = 0;
            while(CM)
            {
                if(CM & 1)
                {   // Display the child
                    Y86Core* Child = mProc->ByID_Get(index);
                    ui->Y86->QTList_Children_Get()->addItem(QString(Child->StringOfQT_Get().c_str()));
                }
                CM /=2; index ++;   // See the next
            }
        }
        else
        {
            ui->Y86->QTList_Children_Get()->addItem("No child");
        }
    }
    else
    {   // No cores are allocated
       ui->Y86->QTList_Get()->addItem("No core");
       ui->Y86->QTList_Parent_Get()->addItem("No parent");
       ui->Y86->QTList_Children_Get()->addItem("No child");
    }
 }//  SimulatorWindowY86::DisplayQTStrings

/*!
 * \brief SimulatorWindowY86::on_StepWiseBox_clicked
 *
 * This method administers user selection whether the simulation should run
 * in stepwise or continuous mode. Notice that the simulator from the
 * simulator thread anyhow will need to run in a suspend/resume mode:
 * the processing is suspended after every single step in order
 * to enable the GUI to update. This stepwise mode selects if the GUI simulator
 * will wait until the user starts the processing of the next step,
 * or the GUI simulator itself will start it with the pre-defined frequency
 */

void SimulatorWindowY86::on_StepWiseBox_clicked()
{
    mStepWiseMode = ui->Ysim->StepWiseBox_Get()->isChecked();
    if(mStepWiseMode)
    {ui->Ysim->startButton_Get()->setText("Next");}
    else
    {ui->Ysim->startButton_Get()->setText("Start");}
}
#endif
