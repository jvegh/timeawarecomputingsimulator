#ifndef SIGNALPLOTWINDOWBASIC_H
#define SIGNALPLOTWINDOWBASIC_H

//#include "SimulatorWindow.h"
#include "qcustomplot.h"
//class QCustomPlot;
class SimulatorWindow;

class SignalPlotWindowBasic : public QCustomPlot
{
    Q_OBJECT

public:
    explicit SignalPlotWindowBasic(QWidget *parent = nullptr);
    ~SignalPlotWindowBasic();
    void setupSimpleDemo(QCustomPlot *customPlot);// Just for demo

//    void setupSincScatterDemo(QCustomPlot *customPlot);
public slots:
//    void setLupe(const QCPRange &range);

private:
 //   QCustomPlot* navigatorPlot;
};



#endif // SIGNALPLOTWINDOWBASIC.H
