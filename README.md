# README #

The today's classic electronics is based on the assumption that
the electronics components of a system interact with each other promply, with no delay.
This handling is resemblant to as interactions are handled in the classic science:
the laws of interactions do not contain an explicit time dependence
(which means infinitely large interaction speed).
Similarly, the mathematics handles its objects in a time-independent way,
assuming only logical dependence between its operands
(in some cases, the time is handled as a separable function).

In modern electronics, it is getting more and more evident, that the instant interaction
is a limited validity approximation, which is used far outside of its range of validity.
Its use leads to a growing number of difficulties:
loosing both power and operational efficiency.

In biological systems (and technological systems mimicking its features or inspired by them)
the non-instant ("spatiotemporal") nature nature of interactions is obvious since the beginning of studying them.
However, the actual description of the neural operation uses
an improper model: the time is handled as a separable function.
Today, both the low expenses and the high avaliability of the ubiquitos computing
make a natural choice to use computing to complement studying biological systems.
However, the biology-oriented computing uses models (improperly) adapted from mathematics
and builds its systems from components fabricated for non-time-aware computing,
using principles of  non-time-aware computing,
by experts trained in non-time-aware computing.


The present (experimental) system attempts to provide an electronic simulator which
targets modern, time-aware computing, that is, includes
facilities mimicking the non-infinite interaction speed,
and in this way enables us to study the
the temporal behavior of such systems. The final goal is to assist developing
time-aware operating methods (primarily: time-aware computing), that can help
analyzing the existing methods and devices, looking for their bottlenecks
(and assisting in this way producing more effective technological solutions).

### What is this repository for? ###

* The repository uses SystemC library and engine as a basic tool to achieve that goal.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
