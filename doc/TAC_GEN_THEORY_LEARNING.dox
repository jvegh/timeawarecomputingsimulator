/**
\page TAC_GEN_THEORY_LEARNING_PAGE Information storage and learning

In information science, the term "information" seems to be established.
Note, however, that it is still based on the "instant interaction".
In the light of discussing first the case of information handling in biology,
we show why the interaction speed is important.


@section TAC_GEN_THEORY_LEARNING_BIOLOGY Learning in the biology

The common experience @cite KochBiophysics:1999, @cite BuzsakiTheBrain:2019
shows that the outputs of biological neurons depend not only on their inputs
(they compute with their inputs) but also on their internal state
(they store information @cite SterlingPrinciples:2017, @cite BuzsakiTheBrain:2019.

<br>
<div class="fallacy">
<b>Fallacy:</b><br>
This feature can easily be misidentified as memristance @cite MissingMemristor:2008,
in the time-unaware model.
In the light of our analysis, some temporal behavior exists;
the question is how much it is related to material or biological features
and how much to the propagation speed.
Five decades ago, even <i>memristance</i> has been introduced
as a fundamental electrical component,
meaning that the memristor's electrical resistance is not constant
but depends on the history of current that had previously flowed through the device @cite MissingMemristor:2008.
Handling the "history" actually means time-aware handling.
There are, however, some serious doubts as to whether a genuine memristor
can actually exist in physical reality @cite RejectingMemristor:2018.
</div>

Furthermore, biological systems can adapt to both short-term and long-term changes
in the external world: they can learn.
We use <i>"the broad definition of learning: use present information to adjust a circuit,
to improve future performance"</i> @cite SterlingPrinciples:2017.
However, the definition needs to explain also,
what is the physical form in which the circuit stored the "present information".
Furthermore, that how the circuit adjusts it during learning.
The time-unaware paradigm can only state that @cite SterlingPrinciples:2017
<i>"we should not seek a special organ for 'information storage' — it is stored,
as it should be, in every circuit."</i>

<br>
<div class="fallacy">
\anchor Fig_NeuronLearning
@image html NeuronLearning1.png "How neurons learn. A) The initial state B) Short time learning, changing synaptic weight by +50\% B) Long time learning, changing conduction velocity by +10\%. Compare this biological computing process with the technological one" width=600px
\latexonly @image latex NeuronLearning1.pdf "How neurons learn. A) The initial state B) Short time learning, changing synaptic weight by +50\% B) Long time learning, changing conduction velocity by +10\%. Compare this biological computing process with the technological one"  width=\textwidth  \endlatexonly
</div>

In Fig. @ref Fig_NeuronLearning
(in the coordinate system shown also in Fig. @ref Fig_Adder1bitPointless),
we assume that the excitation of a neuron at (-1,0,0)  occurs and a neuron fires to an assembly (aligned along coordinate axis <i>y</i>
at <i>x</i> values -0.3, 0.1 and 0.4).
The figure assumes that the excitation (after passing to the branching point) branches towards
three members of the assembly <i>A</i>.
The assembly members <i>A<sub>i</sub></i> send spikes to their common <i>Target</i> neuron
at position (-1.5,0.7).
The corresponding synaptic weights of the target neuron are <i>W<sub>1</sub></i>, <i>W<sub>2</sub></i>, <i>W<sub>3</sub></i> (assumed to be of equal for <i>A)</i>), and three received spikes may cause the target neuron to fire (the sum of the potential contribution of the three spikes is just above the threshold).
Given that the assembly members' position and their firing times slightly differ, so differ their arrival times (see the red arrowheads) of the spikes from the assembly members at the target's position.

The <i>Target</i> is initially in rest.
When the first spike arrives, it increases the membrane's potential,
and so do somewhat later the second and third spikes, too.
With the charge delivered by the third spike, <i>Target</i> reaches its threshold,
and (after some delay: charging the membrane) it fires.
The blue arrow's length denotes the computation's length:
its bottom is at the first spike's arrival time,
its head is at the beginning of the neuron's refractory period.
Given that after reaching
the threshold, some time is needed to charge the membrane to its operating potential,
the length of the arrow includes an extra contribution.

One can easily identify that in terms used in connection with
Fig @ref Fig_ComputingModelTiming, the
spike arriving first to the <i>Target</i> triggers
event "Begin computing" and exceeding the membrane's threshold potential triggers
"End computing".
At that time, the unit computed the result: given
that the synaptic input channels are closed,
the result cannot change anymore.
The "End computing" signal also triggers "Signal delivery":
the neuron opens another ion channels, charges the membrane to its peak potential,
and then it fires.
However, because of its low speed, some (in msec order) time is needed
to reach the neuron's 'output section', its hillock.
After that, the "Signal transfer" (a process external for the neuron) occurs,
and the generated spike typically arrives a few milliseconds later to its destination.
In the meantime, also "Refractory" is active.
During resetting its "internal circuits", also
the neurotransmitters are slightly rearranged.
Given that the concentration of neurotransmitters
(easily identified as synaptic weights <i>W<sub>i</sub></i>) can change against each other,
according to Hebb's observation, the synaptic weight of the synapse
that received the last spike before firing, increases.


@section TAC_GEN_THEORY_LEARNING_TECHNOLOGY Learning in the technology


*/
/*
- The operating unit is idle (no activity)
  Signal: Start delivery
- Process: Delivery to the input section
 -- Nofification type: one operand arrived (single shot)
 -- Notification type: all operands arrived (continuous)
  Signal : Start computing
- Process: the operating unit is making its computations
 -- Mode continuous: Operation restarted when delivery signals
 -- Mode single shot: Do not disturb until computation finished (delivery signal neglected)
 -- Notification: computation is finished
  Signal: start delivery and recovery
- Process: Result delivery
    Result latched and delivery to output section initiated
    If has internal states, delivery signal neglected
  Signal: ready to operate
- The result is delivered to the output section
 -- mode continuous:
 -- Mode single shot: the result latched: no

- Mode: continuous/single shot
- State: the computing unit is ready to perform a new computation (signal)
- State: The unit is ready to accept input (signal)
- State: One operand changed (signal)
- State: Computing has started (signalled)
- State: Computing finished (result in latch)
*/

/*

In this section potential-related issues are interpreted, and some fallacies discussed.
The precise interpretation of the terms is crucial, so please read it carefully.

\section TAC_NEURER_THEORY_POTENTIAL_ASSUCH The potential as a basic quantity

The primary quantity when speaking about electric effects, is the <b><i>charge</i></b>.
The frequently cited saying <a href="https://en.wikipedia.org/wiki/Cherchez_la_femme"><i>Cherchez la femme</i></a>,
used in the sense that "no matter what the problem, a woman is often the root cause".
Our motto is similar: <i>Look for the charge</i>,
meaning "no matter what the problem, some charge is often the root cause".


<a href="https://slideplayer.com/slide/272952/1/images/29/Equipotential+Surfaces+%26+Their+Relation+to+Electric+Field.jpg">The <i>charge</i>  produces
electric field</a>, see @ref Fig_EquipotentialSurfaces.
The electrically charged carriers can be accumulated on a surface, and they form a <i>potential surface</i>,
see also Figure @ref Fig_MembraneAsCondensator.

<div class="fallacy">
<b>Fallacy:</b><br>
In neurobiology, <b><i>the shorthand notation</i></b>  such as that action potential is generated,
travels, arrives, or action potentials travel down the axon by jumping from
one node to the next,
etc.<b><i> always means that the potential is accompanied with transmitting charge</i></b>,
although in many different envelopes, see also section @ref  TAC_NEURER_THEORY_POTENTIAL_SIGNAL.
</div>



\anchor Fig_EquipotentialSurfaces
@image html EquipotentialSurfacesElectric.jpg "Electric charge and electric field" width=600px
\latexonly @image latex EquipotentialSurfacesElectric.jpg "Electric charge and electric field"  width=\textwidth  \endlatexonly


In neurophysiology,
it is a common situation to have different compartments with different solved components
in the compartments, separated by a
selectively permeable membrane, such as in Figure @ref Fig_TwoCompartmentsMembrane.


\anchor Fig_TwoCompartmentsMembrane
@image html two_compartments_separated_by_membrane_w.jpg "Two solution compartments separated by a selectively permeable membrane" width=500px
\latexonly @image latex two_compartments_separated_by_membrane_w.jpg "Two solution compartments separated by a selectively permeable membrane"  width=.8\textwidth  \endlatexonly


Those solved components have dual properties:
on one side they have, as chemical particles, a concentration (and may experience a gradient)
and on the other side an electrical charge (and may experience electrical force). That is, their behavior is under dual control:
both the concentration gradient and the electric force will affect their motion
(and their location in their stationary states),
and in addition the membrane selectively permeable for the different
solved components.
This dual control accompanied with selective permeability on one side needs careful analysis and on the other side enables
the "wonder of life".
See also <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/soldif.html">Energetics of Solute Diffusion</a>.

The <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/biovolt.html">  nice summary </a> helps a lot
in understanding role of voltages in biology.
The case is not simple at all<a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/actran.html">, both the active and passive mechanisms</a>
as well as  <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/allosteric.html"> and the allosteric effects influence
what happens.</a>
If multiple ions and permeabilities are present,
the <a href="https://www.physiologyweb.com/calculators/ghk_equation_calculator.html"> Goldman-Hodgkin-Katz equation</a>
can be used to calculate the resting potential.  Given that the relative membrane permeability of different ions are included,
the potential surely changes if any of the channels closes/opens; that is,
opening a channel alone, may start a rearrangement process, which can be described by a set of rather complex differential equations.
In general, it is wort to visit site <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/allosteric.html"> for physical explanation
of less understood biological statements.</a>


\section TAC_NEURER_THEORY_POTENTIAL_MEMBRANE The membrane potential
The result of the mentioned two forces: charged components appear
on the opposite surfaces of the (insulator) membrane : it will act as a condensator.
\anchor Fig_MembraneAsCondensator
@image html electric-charge-and-electric-field.jpg "Membrane as a condensator" width=728px
\latexonly @image latex electric-charge-and-electric-field.jpg "Membrane as a condensator"  width=.8\textwidth  \endlatexonly



Figure @ref Fig_SemiPermeableMembrane shows that diffusion moves the K+ and Cl– ions in the direction shown,
until the Coulomb force halts further transfer.
This results in a layer of positive charge on the outside, a layer of negative charge on the inside,
and thus a voltage across the cell membrane.
The membrane is normally impermeable to Na+.
The <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/celmem.html"> membrane</a> potential is actually a <i>difference of potentials</i>
measured between intracellular and extracellular sections
of the organism.
Several  <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/mempot.html">mechanisms </a> maintain a small voltage or "potential" across the membrane in its normal or resting state,
 including <a href="http://hyperphysics.phy-astr.gsu.edu/hbase/Biology/nakpump.html"> pumps</a>.

\anchor Fig_SemiPermeableMembrane
@image html Figure_21_07_02a.jpg "The semipermeable membrane of a cell has different concentrations of ions inside and out" width=360px
\latexonly @image latex Figure_21_07_02a.jpg "The semipermeable membrane of a cell has different concentrations of ions inside and out"  width=.5\textwidth  \endlatexonly



\section TAC_NEURER_THEORY_POTENTIAL_ACTION The action potential

One of the most important feature of the membrane's potential that it changes in a very specific way.
Figure @ref Fig_NeuronalActionPotential shows how an action potential runs in time,
with separated phases. As our goal is to discuss <i>only</i> the details the operation
of our simulator relies on (this booklet does not want to be a textbook),
all physically and biologically relevant details are left to discuss in the appropriate cited references.
However, some fallacies are discussed, given that they are not consistently described
in the different sources and may lead to confusion.
First of all, always keep in mind that the <i>charge</i>  produces electric field (and potential)</a>,
including the action potential.

\anchor Fig_NeuronalActionPotential
@image html neuronal_action_potential_phases_w.jpg "Different phases of the neuronal action potential" width=600px
\latexonly @image latex neuronal_action_potential_phases_w.jpg "Different phases of the neuronal action potential"  width=.8\textwidth  \endlatexonly

Initially, the membrane of the neuron is at its resting potential.
A stimulus (delivering change to its input section) from a sensory cell or another neuron(s) (or experimenter's signal)
causes the neuron to depolarize toward the threshold potential ("Triggering" phase);
the neuron starts its "computing". In this phase the "operands" are delivered to the input section
of the neuron (the computing unit).
Triggering can be a slow process,
even it can fail (drops back to resting potential, as leaking leads to losing charge).
If this excitation is successful (depending on the contribution
of the stimulus and the "leaking" of the neuron), the membrane's potential
reaches the threshold potential value (which corresponds to a threshold of the stored change),
the neuron enters its "Charging" phase. Until it enters that phase, it is "computing".
In the Charging phase, another mechanism comes into play: the membrane charges ("depolarizes")
to its peak potential value (the yellow zone): it accumulates ions on its membrane.
<i>Important that the source of charge is different:
no more charge arrives from its input section</i>. Computing is finished,
the result is on its way to the output section, but not yet available.
When that voltage (that is : that charge) arrived, the result will be available
for the external world. Again another
mechanism starts to work: the membrane enters its "Dischanging" phase and discharges ("repolarizes"):
sends out the accumulated charge through its axon.
Due to some reason ("dumping" in the RC circuit model or opening other ion channel(s)
in the chemical description) it can enter its phase "Overrun": the membrane
potential drops below the resting potential ("hyperpolarization")
and finally, it reaches again its initial phase: returns to its resting potential,
and ready to process its cycle again.

The figure also shows some typical values on its axes. Notice that the zero point
of the time scale is at a point when the membrane potential becomes noticeably higher than the resting potential.




\section TAC_NEURER_THEORY_POTENTIAL_SIGNAL Neuronal signalling

For an axion potential to communicate information to another neuron,
it must travel along the axon (i.e. charge must be delivered) to the axon terminal.
An overview of the signalling system is given here based on the <a href="https://courses.lumenlearning.com/physics/chapter/20-7-nerve-conduction-electrocardiograms/"> figures and more detailed discussion</a>.
Figure @ref Fig_NeuronWithDentrites shows that "signals in the form of electric currents
reach the cell body through dendrites and across synapses,
stimulating the neuron to generate its own signal sent down the axon.
The number of interconnections can be far greater than shown here."


\anchor Fig_NeuronWithDentrites
@image html Figure_21_07_01a.jpg "A neuron with its dendrites and long axon." width=300px
\latexonly @image latex Figure_21_07_01a.jpg "A neuron with its dendrites and long axon."  width=.3\textwidth  \endlatexonly


The conceptual (emprirical) action potential shown in Figure @ref Fig_NeuronalActionPotential is repeated
in Figure @ref Fig_NeuronalActionPotential2;
this time together with the rearrangements of ions across the cell membrane as shown.
"Depolarization occurs when a stimulus makes the membrane permeable to Na+ ions.
Repolarization follows as the membrane again becomes impermeable to Na+,
and K+ moves from high to low concentration.
In the long term, active transport slowly maintains the concentration differences,
but the cell may fire hundreds of times in rapid succession without seriously depleting them."
Notice the rightmost inset, showing how the "long-term active transport" occurs.

\anchor Fig_NeuronalActionPotential2
@image html Figure_21_07_03a.jpg "An action potential is the pulse of voltage inside a nerve cell" width=700px
\latexonly @image latex Figure_21_07_03a.jpg "An action potential is the pulse of voltage inside a nerve cell"  width=.8\textwidth  \endlatexonly


That is, we use here the definition that "The action potential is a voltage pulse at one location on a cell membrane,
that varies with time at the given location".
This action potential gets transmitted along the cell membrane, and in particular down an axon,
ain such a way that the changing voltage and electric fields affect the permeability
of the adjacent cell membrane, so that the same process takes place there.
The adjacent membrane depolarizes, affecting the membrane further down, and so on,
as illustrated in <a href="https://courses.lumenlearning.com/physics/chapter/20-7-nerve-conduction-electrocardiograms/">Figure </a> @ref Fig_NerveImpulse.
"Thus the action potential stimulated at one location triggers a nerve impulse that moves slowly (about 1 m/s) along the cell membrane.
A stimulus causes an action potential at one location, which changes the permeability of the adjacent membrane, causing an action potential there.
This in turn affects the membrane further down, so that the action potential moves slowly
(in electrical terms) along the cell membrane.
Although the impulse is due to Na+ and K+ going across the membrane,
<i>it is equivalent to a wave of charge moving along the outside and inside of the membrane</i>.
This is a measurable effect (it is called along the axons the "traveling action potential"),
but recall: it is caused by ion in- and outflux. In other words, the arrival of the action potential
meants the appearance in the receiving neuron. At both ends ions are present inside the axon.
Notice that the end of the axon terminal biology performs electronic to chemical
transformation (initiates neurotransmitter release @cite HowNeuronsCommunicate
for passing a synapsis, in the opposite direction on the pre-synaptic and post-synaptic side.
For a later time, remember that <i>using neurotransmitters
is a very important contribution to the learning ability</i>.

\anchor Fig_NerveImpulse
@image html Figure_21_07_04a.jpg "A nerve impulse is the propagation of an action potential along a cell membrane" width=500px
\latexonly @image latex Figure_21_07_04a.jpg "A nerve impulse is the propagation of an action potential along a cell membrane."  width=.5\textwidth  \endlatexonly


Some axons, like that in Figure @ref Fig_NeuronWithDentrites, are sheathed with myelin,
consisting of fat-containing cells.
This arrangement gives the axon a number of interesting properties.
Since myelin is an insulator, it prevents signals from jumping between adjacent nerves (cross talk).
Additionally, the myelinated regions transmit electrical signals at a very high speed,
much closer to as an ordinary conductor or resistor would.
<i>There is no action potential in the myelinated regions</i>, so that no cell energy is used in them.
The signal is regenerated in the gaps, where the voltage pulse triggers the action potential at full voltage.
So a myelinated axon transmits a nerve impulse faster, with less energy consumption.
Again, for a later time, remember that <i>making nerve impulses faster
is a very important contribution to the learning ability</i>.

\section TAC_NEURER_THEORY_POTENTIAL_POINT From the point of view of the simulator

From our particular point of view, the main point: charge is sent from one membrane of the sender (presynaptic) neuron
and after a very sophisticated process, charge arrives to the membrane of the receiver (postsynaptic) neuron.


*/
