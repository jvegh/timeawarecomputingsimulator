/** @file scIGPMessage_if.cpp
 *  @brief Functionality for the IGPMessage-related processing
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "scProcessor.h" // includes scGridPoint.h
#include "scIGPMessage_if.h"
//#include "scIGPCB.h" //#include "scIGPfifo.h"
//#include "scIGPMessage.h" //#include "Utils.h"
//#include "scLatchFIFO.h"
#include "scSimulator.h"

//#include "scGPMessagefifo.h"
//#include "Memory.h"
class scClusterBus;     // Shall be include
/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h', and are undefined in that file
//#define MAKE_LOG_TRACING    // We want to have trace messages
//#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"

#include "scClusterBusMemorySlow.h"
#include "scHThread.h"
extern scClusterBusMemorySlow* MainMemory;
extern NamedEnum_struct ClusterMembers[8];

//extern string DebugRoute; //!!
/*
 * Basic SystemC functionality of grid modules + grid topologic position handling
 */
extern bool UNIT_TESTING;	// Whether in course of unit testing
/*!
 * \brief Create a new communicating scGridPoint
 * 
 * \param nm the name of the scGridPoint
 * \param[in] Proc the topology we belong to
 */

scIGPMessage_if::scIGPMessage_if(sc_core::sc_module_name nm,scProcessor* Proc):
    sc_core::sc_module( nm)
    ,msProcessor(Proc)
    ,mStatus((uint16_t)-1)
    ,mBusPriority((uint16_t)-1)
    ,m_lock(false)
{
    GPMessagefifo = new scGPMessagefifo("MsgFIFO");  // This stores the messages for this gridpoint
    TimedFIFO = new scTimedIGPMessage_fifo("TimedFIFO");  // This stores the messages for this gridpoint
#if 0
    msRegisterLatch = new RegisterLatch_fifo(string(nm).append("Latch").c_str());
    int ID = Processor->LinearAddressFromCoordinates_Get(GP.X, GP.Y);
    msID = ID;               // The physical ID of the gridpoint, unchangeable
    msMask.ID = (1 << ID);     // A redundant storage, to avoid on the flight calculations
    msMask.Children = 0;    // Initially, we have no children
    msMask.HThreads = 0;    // Initially, we have no threads
    for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
        mHThreads[H] = new scHThread(string("HThread").append(IDtoString(H,2)).c_str(),this, H);
    mHThread = mHThreads[0];             // The actual HThread
    // The followings can only be done once, only in the constructor
    // Establish neigborship facility to the default stage 'no neighbours'
    for(ClusterNeighbor N = cm_Head; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
      { // Initially, there are no FIFOs and no neighbors
        msIGPCB[N] = (scIGPCB*)NULL;
      }
    // Handle HW signals: binding can only be in the constructor
      msPort.Denied(msSignal.Denied);
      msPort.Allocated(msSignal.Allocated);
      msPort.PreAllocated(msSignal.PreAllocated);
      msPort.Available(msSignal.Available);
      msPort.Meta(msSignal.Meta);
      msPort.Wait(msSignal.Wait);
      msPort.Sleeping(msSignal.Sleeping);     // Connected to sleeping, but no mask in Topology
      // by default, no binding to inter-cluster bus
      // Will be set ONLY for cluster head when creating the clusters
      mMaster_if = NULL;

    SC_METHOD(SIGNAL_method);
        sensitive << EVENT_GRID.SIGNAL_StateBit;
    dont_initialize();
    // This will be called when a message arrives to the gridpoint
    SC_THREAD(ReceiveMessage_thread);
        sensitive << GPMessagefifo->scGPMessagefifo_write_event;
    SC_THREAD(ReceiveTimedMessage_thread);
        sensitive << TimedFIFO->TimedIGPMessage_fifo_write_event;
//    int64_t x;
    if(StandAlone)
    {
        //!! Removed temporarily, until NEXT in AbstractCore fully implemented
//!!        SC_METHOD(NEXT_thread);
//!!            dont_initialize();
//!!            sensitive << EVENT_GRID.NEXT;
/*        SC_METHOD(CREATE_method);
            dont_initialize();
            sensitive << EVENT_GRID.CREATE;*/
        Reset(); // Set the signals to the default state
    }
#endif //0
    // This will be called when a message arrives to the gridpoint
    SC_THREAD(ReceiveMessage_thread);
        sensitive << GPMessagefifo->scGPMessagefifo_write_event;
    SC_THREAD(ReceiveTimedMessage_thread);
        sensitive << TimedFIFO->TimedIGPMessage_fifo_write_event;
    Reset();
}// of scIGPMessage_if::scIGPMessage_if



/*!
 * \fn void scGridPoint::Reset(void)
 * 
 * \brief Reset all status bits
 * 
 * */
void scIGPMessage_if::
Reset(void)
{
    TimedFIFO->reset();
    TimedFIFO->TimedIGPMessage_fifo_write_event.cancel();
}

scIGPMessage_if::~scIGPMessage_if()
{
}//scIGPMessage_if::~scIGPMessage_if


void scIGPMessage_if::
bus_action()
{
// const unsigned int mylength = 0x10; // storage capacity/burst length in words
//  int mydata[mylength];
//  scIGPMessage* MyMessage;
//  unsigned int i;
//  ClusterBusStatus status;

while (true)
{
  wait(); // ... for the next rising clock edge
/*     status = bus_port->burst_read(m_unique_priority, mydata,
                m_address, mylength, m_lock);
  if (status == CLUSTER_BUS_ERROR)
sb_fprintf(stdout, "%s %s : blocking-read failed at address %x\n",
       sc_time_stamp().to_string().c_str(), name(), m_address);

  for (i = 0; i < mylength; ++i)
{
  mydata[i] += i;
  wait();
}

  status = bus_port->burst_write(m_unique_priority, mydata,
                 m_address, mylength, m_lock);
  if (status == CLUSTER_BUS_ERROR)
sb_fprintf(stdout, "%s %s : blocking-write failed at address %x\n",
       sc_time_stamp().to_string().c_str(), name(), m_address);
*/
//     wait(m_timeout, SC_NS);
}
}


/*!
* \brief ReceiveMessage_thread, in the bottom layer
*
* This thread is triggered by a GPMessagefifo->scGPMessagefifo_write_event,
* that is generated when the first word of the message is written to the GPMessagefifo.
* That means that the scGridPoint starts to read the message immediately with one clock period delay
* and in parallel with the writing. (a blocking read included that assures synchronization).
* When reading the message is completeted, the message is routed to its destination.
* @see scGridPoint::RouteMessage
*
* At a later phase, it may be replaced with ReceiveTimedMessage_thread,
* but the message sending mechanism may be more complicated
*/
void scIGPMessage_if::
ReceiveMessage_thread(void)
{ // The messages are waiting in GPMessagefifo
    // and they are  processed as soon as a message is written in the FIFO
    //   DEBUG_PRINT_OBJECT("START GPMessagefifo->ReceiveMessage_thread");
    while(true)
    {   // Process until no more events
        DEBUG_EVENT_OBJECT("WAIT scGPMessagefifo_write_event");
        wait(GPMessagefifo->scGPMessagefifo_write_event);
        DEBUG_EVENT_OBJECT("RCVD DIRECT (scGPMessagefifo_write_event)");
        scIGPMessage* MyMessage;
        GPMessagefifo->read(MyMessage);
        DEBUG_CONTRIBUTE_STRING(DebugRoute,"RCVD DIRECT ", StringOfMessage_Get(MyMessage).append("\n"));
        // Now we have the message in our own buffer
        // It cannot be taken for sure that it is for us;
        // Route it again, and ProcessMessage will process if yes
        RouteMessage(MyMessage);
    }
}


   /*!
    *  \brief Receive the message, triggered by TimedIGPMessage_fifo_write_event:
    *
    * Time of message delay is over,
    */
   void scIGPMessage_if::
ReceiveTimedMessage_thread(void)
{ // The messages are waiting in GPMessagefifo
 // and they are processed as soon as a message is written in the FIFO
//   DEBUG_PRINT_OBJECT("START TimedIGPMessage_fifo_write_event");
   while(true)
   {   // Process until no more events
       DEBUG_EVENT_OBJECT("WAIT TimedIGPMessage_fifo_write_event");
       wait(TimedFIFO->TimedIGPMessage_fifo_write_event);
       sc_time MyTime = sc_time_stamp();
       DEBUG_EVENT_OBJECT("RCVD TimedIGPMessage_fifo_write_event");
       // Check if it is jut a write, or if it is really time to act
       sc_time NextTime = TimedFIFO->NextTime_Get() - MyTime;
       if(SC_ZERO_TIME == NextTime)
       {   // Yes, it is time to act: read the content and proceed
           TimedIGPMessage_type MyTimedMessage;   // Will store a pointer from the timed FIFO
           TimedFIFO->read(MyTimedMessage);     // Read the delayed message
           scIGPMessage* MyMessage = MyTimedMessage.Message;  // Save the message
           DEBUG_ONLY(ostringstream oss;\
           oss << "RCVD delayed by " << MyMessage->FeatureWord_Get().Time << " ";\
          DEBUG_CONTRIBUTE_STRING(DebugRoute,oss.str(), StringOfMessage_Get(MyMessage).append("\n")););
           sc_time TimedTime = MyTimedMessage.Time;   // Remember time of message
//                delete MyTimedMessage;  // Now the timed message is not any more needed
           MyMessage->DelayTime_Set(0);    // Be sure no further delay
           if(TimedFIFO->num_available())
           {   // We have more events in the queue
               NextTime = TimedFIFO->NextTime_Get() - TimedTime; // Calculate when to read next
               TimedFIFO->TimedIGPMessage_fifo_write_event.notify(NextTime); // and create notification
            }
            // Now we have the message in our buffer, at the right time
           RouteMessage(MyMessage);    // OK, now can deliver the message
       }
       else
       {   // No it is just a new message in the queue
           // Send a notification for the FIFO, fro a new schedule
           TimedFIFO->TimedIGPMessage_fifo_write_event.notify(NextTime);
       }
   }
}


   // Implement the inter-cluster communication
     // direct Slave Interface; mainly for testing without the bus

     /*!
      * \brief Send out message M to the inter-cluster bus
      *
      * Presently imitated by simply calling the destination's
      * \sa scGridPoint::ReceiveClusterMessage
      */
       ClusterBusStatus scIGPMessage_if::
   SendClusterMessage(scIGPMessage* M)
   {
       // Find out if speking to an scGridpoint  or to the memory
       ClusterAddress_t  CA = M->DestinationAddress_Get()  ;
   //    ClusterAddress_t MyCA = DefaultClusterAddress;
   //    if(CA.ToInt() == MyCA.ToInt())
           if(IsMemory(CA))
       {   // The destination is the memory
           assert(cm_Head == M->ReplyToAddress_Get().Member); // Only head has access to the bus
           assert(msg_Mem == M->Type_Get()); // Be sure to send memory messages only
           DEBUG_CONTRIBUTE_STRING(DebugRoute, "SENT CLUSTER ",
                                   StringOfMessage_Get(M).append("\n"));
           if(IsMemory2(CA))
           {
           MainMemory->ReceiveClusterMessage(M);
           }
           else
           {
               assert(false);  // Other memory handling is not yet implemented
           }
       }
       else
       { // The destination is a gridpoint
           assert(cm_Head == M->DestinationAddress_Get().Member); // Only head has access to the bus
           assert(msg_Mem != M->Type_Get()); // Presently, dealing with registers only
           scGridPoint* OtherGP = Processor_Get()->ByClusterAddress_Get(M->DestinationAddress_Get());
           assert(OtherGP);
           DEBUG_ONLY(scGridPoint* Source = Processor_Get()->ByClusterAddress_Get(M->ReplyToAddress_Get())); // These two lines are needed only for debugging
           DEBUG_ONLY(scGridPoint* Dest =  Processor_Get()->ByClusterAddress_Get(M->DestinationAddress_Get()));
/*           DEBUG_PRINT_OBJECT
               ("SENT CLUSTER " << Source->StringOfClusterAddress_Get() << ','
               << M->StringOfBufferContent_Get(1)  << ','
               << Dest->StringOfClusterAddress_Get() << ',' << Dest->StringOfClusterAddress_Get() );
*/           DEBUG_CONTRIBUTE_STRING(DebugRoute, "SENT CLUSTER ",
                               StringOfMessage_Get(M).append("\n"));
           OtherGP->ReceiveClusterMessage(M);
       }
       return CLUSTER_BUS_OK;
   }

       /*!
        * \brief Receive a message from outside the cluster
        *
        * Presently is called directly by the sender's scGridPoint::SendClusterMessage
        */
       ClusterBusStatus scIGPMessage_if::
   ReceiveClusterMessage(scIGPMessage* M)
   {
  //??     assert(cm_Head == ClusterAddress_Get().Member); // Only head has access to the bus
       // A message from the cluster bus arrived. Surely for us, but maybe to proxy or forward
       DEBUG_ONLY(scGridPoint* Source = Processor_Get()->ByClusterAddress_Get(M->ReplyToAddress_Get())); // These two lines are needed only for debugging
       DEBUG_ONLY(scGridPoint* Dest = Processor_Get()->ByClusterAddress_Get(M->DestinationAddress_Get()));
/*       DEBUG_PRINT_OBJECT
           ("RECEIVED CLUSTER " << Source->StringOfClusterAddress_Get() << ','
           << M->StringOfBufferContent_Get(1)  << ','
           << Dest->StringOfClusterAddress_Get() << ',' << Dest->StringOfClusterAddress_Get() );
*/       DEBUG_CONTRIBUTE_STRING(DebugRoute, "RECEIVED CLUSTER ",
                               StringOfMessage_Get(M).append("\n"));
       RouteMessage(M);
       return CLUSTER_BUS_OK;
   }

#ifdef other
    /*!
     * \brief scGridPoint::Clocked
     * Advances the scGridPoint's own clock counter
     */
    void scGridPoint::
Clocked(void)
{
     if(IsAllocated())
    {   // The scGridPoint is actively used at the moment
            performance->Clocks++;
//        DEBUG_PRINT( name() << " clocked at " <<  sc_time_to_nsec(1));
//        cerr << "Heartbeat @" << sc_time_to_nsec(1) << endl;
    }
//    next_trigger(clock.pos()); //
}
#endif //other
#ifdef CLOCKED
    void scGridPoint::
Clocked(void)
{
    while (true)
    {
        // Now a clock signal received, see what we can do
        if(IsAllocated())
        {   // The scGridPoint is actively used at the moment
            performance->Clocks++;
//        DEBUG_PRINT( name() << " clocked at " <<  sc_time_to_nsec(1));
//        cerr << "Heartbeat @" << sc_time_to_nsec(1) << endl;
        }
        wait();
    }
}
#endif // CLOCKED

