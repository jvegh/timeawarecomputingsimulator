/** @file scClusterBusArbiter.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The header of the bus arbiter
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
*/
 
#ifndef __cluster_bus_arbiter_h
#define __cluster_bus_arbiter_h

//#include <systemc.h>

//#include "ClusterBusTypes.h"
//#include "ClusterBusRequest.h"
#include "scClusterBusArbiter_if.h"

class ClusterBusRequest;
class scClusterBusArbiter
  : public scClusterBusArbiter_if
  , public sc_module
{
public:
  // constructor
  scClusterBusArbiter(sc_module_name name_
                     , bool verbose = false)
    : sc_module(name_)
    , m_verbose(verbose)
  {}

  ClusterBusRequest *arbitrate(const ClusterBusRequest_vec &requests);

private:
  bool m_verbose;

}; // end class cluster_bus_arbiter

#endif // scClusterBusArbiter
