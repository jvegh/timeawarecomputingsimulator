/** @file scIGPfifo.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the scEMPA simulator, inter-gridpoint FIFO
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scIGPfifo_h
#define scIGPfifo_h

#include <systemc>
//#include "BasicConfig.h"
using namespace std;
using namespace sc_core;
using namespace sc_dt;

class scIGPwrite_if : virtual public sc_interface
{
   public:
     virtual void write(int32_t) = 0;
     virtual void reset() = 0;
};

class scIGPread_if : virtual public sc_interface
{
   public:
     virtual void read(int32_t &) = 0;
     virtual int num_available() = 0;
};

/*!
 * \class scIGPfifo
 * 
 * This class is used to transfer messages from core to core
 * 
 * The input port of the FIFO is in the sender scIGPCB, 
 * the output port is in the receiver scIGPCB
 */
class scIGPfifo : public sc_channel, public scIGPwrite_if, public scIGPread_if
{
   public:
     scIGPfifo(sc_module_name name) : sc_channel(name), num_elements(0), first(0) {}

     void write(int32_t c) {
       if (num_elements == max)
         wait(scIGPread_event);

       data[(first + num_elements) % max] = c;
       ++ num_elements;
       scIGPwrite_event.notify();
     }

     void read(int32_t &c){
       if (num_elements == 0)
         wait(scIGPwrite_event);

       c = data[first];
       -- num_elements;
       first = (first + 1) % max;
       scIGPread_event.notify();
     }

     void reset() { num_elements = first = 0; }

     int num_available() { return num_elements;}

   private:
     enum e { max = 10 };
     int32_t data[max];
     uint16_t num_elements, first;
     sc_event scIGPwrite_event, scIGPread_event;
};

#endif //scIGPfifo_h
