/** @file scSimulator.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scSimulator_h
#define scSimulator_h
#include "scqSimulator.h"
class scClusterBus;
class scClusterBusArbiter;
class scProcessor;
//class scGridPoint;
//!! class scqTreeModel;
using namespace sc_core; using namespace std;


//Submodule forward class declarations

/*!
 * @class scSimulator
 * @brief The anchestor of all ScQt-based simulators, using EMPA bus
 * The simulator manipulates scGridPoints, buses, processors, etc.
 */

//SC_MODULE(scSimulator) {
class scSimulator : public scqSimulator {
    //Port declarations
    // channels
  protected:
    sc_clock C1 {"clk", 100, SC_PS};
    //
    //Channel/Submodule* definitions
/*  simple_bus_master_blocking     *master_b;
  simple_bus_master_non_blocking *master_nb;
  simple_bus_master_direct       *master_d;
  simple_bus_slow_mem            *mem_slow;
*/
    scClusterBus                *msClusterBus;
//  simple_bus_fast_mem            *mem_fast;
    scClusterBusArbiter             *msClusterBusArbiter;

  public:
    // Constructor declaration:
    /*! \param[in] nm name of the simulator
     * \param[in] argc number of arguments
     * \param[in] argv the arguments
     * \param[in] StandAlone if stand-alone operation requested
     */
    scSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone=true);
      SC_HAS_PROCESS(scSimulator); // Contructor implemented in .cpp
      virtual
    ~scSimulator();
      string
      PrologText_Get(void);

    void Setup(void);  // Belongs to the constructor
    void writeSettings(void); // Write settings to the project directory
/*        void
    SetupSystemDirectories(QWidget* parent);
        void
    SetupSettings(void);*/
      void
    SInitialize_method(void); ///< This initializes the simulator
      bool
    StepwiseMode_Get(void){ return msStepwiseMode;}
      void
    StepwiseMode_Set(bool b){msStepwiseMode = b;}
      void
    SSUSPEND_thread(void);
      void
    SRESUME_thread(void);
      void
    SSTART_thread(void);
      void
    SSTOP_thread(void);
      void
    SHALT_thread(void);
        struct{
      sc_core::sc_event
    START,
    STOP,
    HALT,
    SUSPEND,
    RESUME;
        }EVENT_SIMULATOR;
        //virtual
        scProcessor*
    Processor_Get(){ return msProcessor;}
   //scqTreeModel* mModuleTree;
   //scqTreeModel* mClusterTree;
//   scqTreeItem *rootItem;

  protected:
        void SetupHierarchies(void);
    /*
       void
    PrintFinalReport(scProcessor *Proc);
    */
        void
    HandleSpecials(void); /// Handle the exceptional points
        vector<scGridPoint*>
    mSpecials;   /// Stores the special points
}; // of scSimulator

#endif // scSimulator_h
