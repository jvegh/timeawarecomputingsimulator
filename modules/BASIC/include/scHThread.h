/** @file scHThread.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Hardware threads (HThreads) for scGridPoints.
 */
/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/

#ifndef scHThread_h
#define scHThread_h
/** @addtogroup TAC_MODULE_BASIC
 *  The neural-related classes and functionality
 *  @{
 */


class scGridPoint;
#include <iomanip>      // std::setfill, std::setw
#include <systemc>
#include <bitset>
//#include "BasicConfig.h"
#include "GridPoint.h"
using namespace sc_core; using namespace std; using namespace sc_dt;

/*! \var typedef  HThreadPreference_t
 * These preference modes can be selected when asking for a new scHThread
 */
typedef enum { hpt_System, hpt_Head, hpt_Same, hpt_Near, hpt_Any} HThreadPreference_t;
/*!
 \struct QTattributes_t
 \brief This structure stores the quasi-thread related attributes
 */
///
typedef struct{
        SC_ADDRESS_TYPE /// Return offset of the code chunk
    Offset;
        size_t /// Return length of the code chunk
    Length;
        string /// The string ID of the QT
    ID;
} QTattributes_t; typedef QTattributes_t *QTattributes_Ptr;

/*! \var typedef ThreadOperatingBits_t
 * \brief the names of the bits in the bitset
 */
typedef enum
{
    tob_Regime, ///< The thread is working in "Modern" mode
    tob_Allocated,  ///< The thread is allocated for another thread
    tob_PreAllocated,  ///< The thread is allocated for another thread
    tob_Blocked,    ///< The HThread is blocked for some reason
    tob_MemoryExpected, //!< Set when asked for data transfer from memory
    tob_DataExpected, //!< The core can wait either for data memory or instruction memory
    // The scGridPoint can make ONE fetch at a time, the threads must share its capacity
    // These two bits describe the status of the scGridPoint
    tob_FetchPending, //!< A fetch for this thread event was started, but not yet finished
    tob_FetchValid, ///< If this thread has a valid prefetched instruction
    // The scThread is executing an instruction on the  scGridPoint
    tob_ExecPending, ///< The EXEC for this thread was issued, but not yet terminated
    tob_MorphFetched,   ///< The fetched instruction is a morhphing instruction
    tob_Morphing,   ///< Executing a morphing instruction
    tob_PCAffected, ///< If PC is affected by the instruction
    tob_Observed,   ///< The HThread is observed (by the simulator)
    tob_Max
} ThreadOperatingBits_t;

/*! \struct CoreStage_t
 * \brief Variables passed between fetch/execute
 */
typedef  struct{
  bool
  okc,okFetch;
        int8_t
    byte1,
    OpCode,     ///< The major operation code
    FunctCode,  ///< Function subcode
    FunctCode7,  ///< Function subcode Funct7
    RSource1,   ///< First source
    RSource2,   ///< Second source
    RDest;      ///< Destination register
        SC_WORD_TYPE
    immediate;
} CoreStage_t; typedef CoreStage_t *CoreStage_Ptr;

/*! \struct CoreFetch_t
 * \brief Instruction-fetching related variables
 */
typedef struct{
    uint32_t PC;    ///< The fetch PC
    uint32_t NewPC; ///< The next PC to fetch
    SC_WORD_TYPE* InstBufferPosition; ///< Pointer to the fetched memory content
    SC_WORD_TYPE* DataBufferPosition; ///< Pointer to the fetched memory content
    CoreStage_t Stage; ///< Fetch related storage
        sc_time
        WaitTimeBegin,  ///< When we started to wait for the FETCH
    ExecutionTime;  ///< The simulated execution time
} CoreFetch_t; typedef CoreFetch_t *CoreFetch_Ptr;

/*! \struct CoreExecute_t
 * \brief  Instruction-execution related variables
 */
typedef struct{
    uint32_t PC;    ///< The fetch PC
//    sc_core::sc_time    Delay;///< Remember the time of fetching the instruction
    CoreStage_t Stage; ///< Fetch related storage
    uint32_t val,dval;
    sc_time ExecutionTime; ///< The simulated execution time
} CoreExecute_t; typedef CoreExecute_t *CoreExecute_Ptr;

/*!
 * \brief The scHThread class
 *
 * The scHThread is the working hose of the system. I rougly corresponds to what a conventioal processor (core) is.
 */

/* \class scHThread The scHThread is the "work horse" of the system
 *
 * In order to avoid using an scHThread for two different goals,
 * it must be activated before directing an instruction stream to it
 * and must be deactivated after the execution of the stream terminated.
 * The state flag whether the scHThread is active, is administered in its owner scGridPoint,
 * with setting the bit corresponding to the scHThread's IDMask.
 *
 * A scHThread can be activated either by another scHThread, or the scProcessor
 * of the scGridpoint owing the scHThread.
 * The first form is only possible using a morphing instruction,
 * the latter is possible without morphing (corresponds to the classic computing).
 * The scProcessor, in this way, can start several scHThread processes,
 * either the conventional or scHThread-initiated ones.

 * The OS has the right to allocate and preallocate some scGridPoint and scHThread resources for a task,
 * and the different tasks do not have the right to activate each other's resources.
 * There are, however, unallocated resources; and (as a "free pool") the
 * tasks can activate those resources.

 */

class scHThread : public sc_core::sc_module
{
    friend class scGridPoint;
    friend class scAbstractCore;
    friend class Y86Core;

  public:
    /*!
     * \brief An scGridPoint owns run several HW threads (scHThread),
     * can run them, with auto-scheduling them.
     *
     * An scHThread "rents" its corresponding owner scGridPoint for the time of processing,
     * i.e., only one of the HThreads can run at the same time, the others are waiting. This can
     * increase utilization of the scGridPoint.
     * Basically, the scHThread can use the HW unit for fetching and executing
     * of one machine instructions. The HTreads have their independent context,
     * such as Program Counter and (two) Program Counters, (one for fetching and one for executing)
     * register set and status word, if any.
     * When executing, the scGridPoint can be in one of two stages:
     * either executing a conventional machine instruction, or morphing,
     * with the assistance of its owner processor.
     *
     * The scGridPoint owns the scHThread (the scHThread can run only on that scGridPoint),
     * but an scHThread can be a parent of another scHThread; in this way they will be
     * in parent-child relationship
     *
     * \param[in] nm Module name
     * \param[in] GP the owner scGridPoint
     * \param[in] ID The unique (whithin the scGridPoint) ID of the thread
     */

    scHThread(sc_core::sc_module_name nm
              , scGridPoint* GP, SC_HTHREAD_ID_TYPE ID) ;
   ~scHThread(void);
    SC_HAS_PROCESS(scHThread);  // We have the constructor in the .cpp file
        struct{
            sc_core::sc_event
            FETCH       ///< New fetch requested
            ,FETCHED    //< Fetch finished
            ,EXEC       ///< New execution requested
            ,EXECUTED   ///< New execution finished
            ,NEXT       ///< Next intruction is requested
            ,TIMEOUT    //< The required timeout is over
            ,MemoryContentArrived //< The physical core informs the the top layer, per HThread
            ;
    }EVENT_HTHREAD; ///< These events are handled at thread level
        bool    ///< The HThread can be time, true if the actin timed out
    IsTimedOut(void) const
        { return mTimedOut;}
        void    ///< Reset the thread for operation
    Reset(void);
        void    ///< Set timout facility for a thread
    Timeout_Set(sc_time &T)
        { mTimedOut = false; EVENT_HTHREAD.TIMEOUT.notify(T);}
        void    ///< Set 'Allocated' status of HThread
    AllocatedBit_Set(bool B);
        bool    ///< Get 'Allocated' status of HThread
    AllocatedBit_Get(bool B);

        void ///< Set 'PreAllocated' status of HThread
    PreAllocatedBit_Set(bool V=true);
       // { OperatingBit_Set(tob_PreAllocated,V);}
        bool ///< Get 'PreAllocated' status of HThreadl
    PreAllocatedBit_Get(void){  return  OperatingBit_Get(tob_PreAllocated);}
        SC_HTHREAD_ID_TYPE
    ID_Get(void){return mID;}
        SC_HTHREAD_MASK_TYPE
    ID_Mask_Get(void) { return 1 << mID;}
        string
    StringOfID_Get(void);
        ClusterAddress_t ///<  Return the cluster address of the gridpoint, stored in the matrix
    ClusterAddress_Get(void);

        string
    PrologString_Get(void);
    // QT handling
/*        bool
    QTRegime_Get(void) { return mQT.Regime;}
        void
    QTRegime_Set(bool R) { mQT.Regime = R;}*/
        SC_ADDRESS_TYPE
    QTOffset_Get(void){ return mQT.Offset;}
        void
    QTOffset_Set(SC_ADDRESS_TYPE O){mQT.Offset = O;}
        size_t
    QTLength_Get(void){ return mQT.Length;}
        void
    QTLength_Set(size_t L){mQT.Length = L;}
/*        string
    QTID_Get(void){ return mQT.ID;}
        void
    QTID_Set(string S){mQT.ID = S;}
        string
    StringOfQT_Get(void); { return mQT.ID;}   */
        string
    StringOfDefaultName_Get(void);
        string
    StringOfClusterName_Get(void);
        string
    StringOfAliasName_Get(void){ return mAliasName;}
        bool
    StringOfAliasName_Set(string A);
//    QT_AssembleID( scHThread* Parent);
        scGridPoint*
    Owner_Get(void){ return mGridPoint;}
    // Allocation-related
        scHThread*
    Parent_Get(void){ return mParent;}
        void
    Parent_Set(scHThread* P){mParent = P;}
        void
    ResetForParent(scHThread* Parent);   
        scHThread* ///< Allocate this scHThread for another one @see PreAllocateFor
    AllocateFor(scHThread* Parent);
        scHThread* ///< PreAllocate this scHThread for another one @see AllocateFor
    PreAllocateFor(scHThread* Parent);


        // State-bits-related functionality
         void
    AllocatedBit_Set(SC_HTHREAD_ID_TYPE H,  bool V=true);
        bool
    AllocatedBit_Get(void)
        {  return OperatingBit_Get(tob_Allocated); }
/*        bool
    IsAllocated(void) { return OperatingBit_Get(tob_Allocated);}*/
        bool
    Deallocate(void);
        SC_WORD_TYPE
    Register_Get(int R)
    {assert((R>=0) && (R<32)); return REGS[R];}
        void
    Register_Set(int R, SC_WORD_TYPE C)
    {assert((R>=0) && (R<32)); REGS[R] = C;}

        SC_ADDRESS_TYPE
    PC_Get()
        { assert(((exec.PC>=0) && (exec.PC<FMAX_MEMORY_SIZE)) || (exec.PC==(unsigned)-1));
            return exec.PC;}
        void
    PC_Set(SC_ADDRESS_TYPE P)
        {  assert(((P>=0) && (P<FMAX_MEMORY_SIZE)) || (P==(unsigned)-1));
            exec.PC = P;}
        void
    fetchNewPC_Set(SC_ADDRESS_TYPE PC)
        { assert((PC>=0) && (PC<FMAX_MEMORY_SIZE));
            fetch.NewPC = PC;}	///< Set the PC of the core for fetching the next instruction
        SC_ADDRESS_TYPE
    fetchPC_Get(void)
        {   return fetch.PC;}	///< Get PC of the core where the instruction is/was fetched
        int8_t
    fetchFunctionCode_Get(void)
            { return fetch.Stage.FunctCode;}	///< Get function code of the fetched instruction
        void
    fetchPC_Set(SC_ADDRESS_TYPE PC){ assert((PC>=0) && (PC<FMAX_MEMORY_SIZE));
            fetch.PC = PC;}	///< Set PC of the core where the instruction will be fetched
        SC_WORD_TYPE
    fetchImmediate_Get(void)
            { return fetch.Stage.immediate; }
        void
    PC_Affected_Set(bool B)  ///< Set if PC is affected after using the instruction
        { OperatingBit_Set(tob_PCAffected,B);}
        bool
    PC_Affected_Get(void)   ///< Get if PC is affected after using the instruction
        { return OperatingBit_Get(tob_PCAffected);}
        string ///
    StringOfPC_Get(void)
    {
        ostringstream oss;
        oss << "0x" << hex << std::setfill('0') << std::setw((FMEMORY_ADDRESS_WIDTH+3)/4) << PC_Get() << dec;
        return oss.str();
    }
        /*!
         * \fn StringOfClusterAddress_Get
         * \return the string form of the complete cluster address of the gridpoint
         */
        string
     StringOfClusterAddress_Get(void);

        bool
    IsMemoryExpected(void) { return mThreadOperatingBits[tob_MemoryExpected];}
        bool
    IsDataMemoryExpected(void) {return mThreadOperatingBits[tob_DataExpected];}
        void
    MemoryExpected_Set(bool B, bool D=true)
        {mThreadOperatingBits[tob_MemoryExpected] = B; mThreadOperatingBits[tob_DataExpected] = D; }
        SC_WORD_TYPE*   /// Return pointer to word containing next instruction
    InstPositionInBuffer(SC_ADDRESS_TYPE A=CORE_DEFAULT_ADDRESS);
        SC_WORD_TYPE*   /// Return pointer to word containing next data
    DataPositionInBuffer(SC_ADDRESS_TYPE A);

        /*!
         * \brief Make sure we have a fetched instruction.
         * If ready, just make nothing.
         * If fetch started, but not finished, just wait until arrives.
         * If not started, initiate fetching
         */
        virtual void
    FinishPrefetching(void);
        void
    PrepareNextInstruction();
        virtual bool
    CatchedAllocationError(scHThread* Parent){return false;}
        void
    OperatingBit_Set(ThreadOperatingBits_t B, bool V);
        bool
    OperatingBit_Get(ThreadOperatingBits_t B){return mThreadOperatingBits[B];};
        void    ///< Put graph data to a vector storage
    ObservedBit_Set(bool B);
        void
    Child_Insert(scHThread* C);
        void
    Child_Remove(scHThread* C);
        int
    ChildCount_Get(void){return mChildren.size();}
        int
    Child_Find(scHThread* C);
        bool
    Y86doFetchInstruction();
        bool
     Y86doExecuteInstruction(void);
        int16_t
    InstanceCount_Get(){  return mInstanceCount % 50;;}
        string
    StringID_Get(void);

        void
        HandleMorphingInstruction();
    void
    HandleConventionalInstruction();
    /*!
     * \brief Return alias name of default name
     * \return alias name, if used
     */
        string
    StringOfName_Get(void);

protected:
        void
    TIMEOUT_method(void);   //
        SC_WORD_TYPE*
    REGS;
        CoreFetch_t
    fetch;  ///< All info relating instruction fetch
        CoreExecute_t
    exec; ///< All information relating to the execution
        SC_WORD_TYPE
    mDataMemoryBuffer[MAX_IGPCBUFFER_SIZE],     // This a per thread cache
    mInstrMemoryBuffer[MAX_IGPCBUFFER_SIZE];    ///< Store received data&instr memory content here
        SC_ADDRESS_TYPE
    mDataMemoryAddressBegin, mDataMemoryAddressEnd, // This a per thread cache
    mInstrMemoryAddressBegin, mInstrMemoryAddressEnd;    ///< The first/last contents in the cache

    /*!
     * \brief Runs when a next instruction is due to process
     * It handles prefetching, suspending operation,
     * handles execution of meta instructions and conventional instructions
     */
//        void
//    NEXT_thread(void);

        /*!
         * \brief FETCH_method Runs when a thread receives an EVENT_HTHREAD.FETCH
         *
         * The thread attemps to 'own' the cores FETCH facilities.
         * If it suceeds, writes its own thread number to the corresponding structure.
         * If not, waits for the corresponding EVENT_HTHREAD.FETCHED signal and retries.
         * The owner scGridPoint will send that signal to all of its HThreads.
         */
        void
    FETCH_method(void);

        /*!
         * \brief EXEC_method Runs when a thread receives an EVENT_HTHREAD.EXEC
         *
         * The thread attemps to 'own' the cores EXEC facilities.
         * If it suceeds, writes its own thread number to the corresponding structure.
         * If not, waits for the corresponding EVENT_HTHREAD.EXECUTED signal and retries.
         * The owner scGridPoint will send that signal to all of its HThreads.
         */
        void
    EXEC_method(void);

       /*!
     * \brief Runs when a next instruction should be taken from memory
     *
     * Works in cooperation with the events/thread of its scGridPoint:
     * it arbitrates scGridPoint utilization (with no priority): the requesting
     * unit makes scGridPoint (NEXT_thread) busy and releases it when the instruction
     * finshes; it must set also the "current thread"
     */
//        void
//    FETCH_thread(void);
    /*!
     * \brief Called when the next instruction in a HThread shall be executed.
     *
     * The method work in cooperation with FETCH_method and EXEC_method
     */
        void
    NEXT_method(void);
        void
    MemoryContentArrived_method(void);
        virtual void
        doGetMemoryContent(void){}
        bool
    mTimedOut;      ///< Is set when EVENT_GRID.TIMEOUT.notify() times out
        short int
    msWaitTimeout;  // The waiting can be time-limited; 0 means no wait
        void
    AllocationBegin_Set(sc_core::sc_time T)
        {   msAllocationBegin = T;}
private:
        bitset<tob_Max>
    mThreadOperatingBits;   ///< The bit of the thread state
        QTattributes_t
    mQT;   ///< Stores the quasi-thread related attributes
        scGridPoint*
    mGridPoint; ///< The owner scGridPoint
        scHThread*
    mParent;    ///< The thread that started us
        vector<scHThread*>
    mChildren;
        SC_ADDRESS_TYPE
    mMemoryRequestAddress;
        const SC_HTHREAD_ID_TYPE
    mID;
        string
    mStringID;  // Store here
        bool
    msPC_Affected;  ///< If PC will be affected by executing this instruction
        short int
    mCSR;   ///< The Control and Status codes
        uint16_t
    mInstanceCount; // How many times this object was instantiated
        sc_core::sc_time
    msAllocationBegin;  // When this scHThread was allocated
        string  //
    mAliasName;
}; // of scHThread
/** @}*/
#endif // scHThread_h
