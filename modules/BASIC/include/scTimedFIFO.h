/** @file scTimedFIFO.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

// Header for the scEMPA simulator, Inter-core block

#ifndef scTimedIGPFIFO_h
#define scTimedIGPFIFO_h
#include <systemc>
//#include "scGridPoint.h"

class scIGPMessage;
using namespace sc_core;
//Submodule forward class declarations
/*! \struct TimedIGPMessage_type
 * \brief The event in the timed event queue
 */

typedef struct{
    sc_core::sc_time Time;
    scIGPMessage* Message;
} TimedIGPMessage_type;

// This is a FIFO for the timed  GridPoint events

class TimedIGPMessage_write_if : virtual public sc_interface
{
   public:
     virtual void write(TimedIGPMessage_type) = 0;
     virtual void reset() = 0;
};

class TimedIGPMessage_read_if : virtual public sc_interface
{
   public:
     virtual void read(TimedIGPMessage_type &) = 0;
     virtual int num_available() = 0;
};

// This is the FIFO for the timed scGridPoint events
// This FIFO is time ordered; the new messages are inserted in the queue
// according to their deadline
/*!
 * \brief The FIFO stores messages ordered by their 'delayed delivery time'
 *
 * When a  new scIGPMessage arrives to scGridPoint, if the delay time is NOT zero,
 * the message is put in this time-ordered queue and and 'write' event is for the first member
 * of the queue is generated. (non-delayed messages are delivered immediately)
 * That is, the scGridPoint will receive the messages at the time that is prescribed by the sender.
 *
 * \param name the name of the FIFO
 */

class scTimedIGPMessage_fifo: public sc_core::sc_channel, TimedIGPMessage_write_if, TimedIGPMessage_read_if
{
   public:
     scTimedIGPMessage_fifo(const sc_module_name& name) ;
     virtual
       ~scTimedIGPMessage_fifo(void){}
     void write(TimedIGPMessage_type T);
     void read(TimedIGPMessage_type &T);
     void dowrite(TimedIGPMessage_type T);
     void doread(TimedIGPMessage_type &T);
     void reset() { num_elements = first = 0;}
     int num_available() { return num_elements;}
     bool NewTimeLower(TimedIGPMessage_type New, TimedIGPMessage_type Old);
     sc_time NextTime_Get(void){if(num_elements) return data[first].Time; else return SC_ZERO_TIME;}
     int Length_Get(void){ return max;}
     sc_event TimedIGPMessage_fifo_write_event, TimedIGPMessage_fifo_read_event;
   private:
     enum e { max = 10 }; // Only for testing!
     TimedIGPMessage_type data[max];
     int num_elements, first;
};


#endif // scTimedIGPFIFO
