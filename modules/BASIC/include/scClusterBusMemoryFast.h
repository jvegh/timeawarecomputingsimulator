/** @file scClusterBusMemoryFast.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The fast ("Register") type memory
 *
 * This can be the basis for the "memory only" processing mode.
 * It uses direct access, i.e. as fast as registers, and can be used
 * in a way similar to that of "registers"; provided that the address
 * is short enough
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
/* 
  The origin of this development file is available as SystemC example
  simple_bus_fast_mem.h : The memory (slave) without wait states.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 
#ifndef __simple_bus_fast_mem_h
#define __simple_bus_fast_mem_h

//#include <systemc.h>


#include "scClusterBusSlave_if.h" //#include "ClusterBusTypes.h"
/*!
 * \class scClusterBusMemoryFast
 * \brief A register-type memory area per .
 * 
 * it provides immdediate response, like registers*/

class scClusterBusMemoryFast
  : public scClusterBusSlave_if
  , public sc_module
{
public:
  // constructor
  scClusterBusMemoryFast(sc_module_name name_
		      , unsigned int start_address
		      , unsigned int end_address)
    : sc_module(name_)
    , m_start_address(start_address)
    , m_end_address(end_address)
  {
    sc_assert(m_start_address <= m_end_address);
    sc_assert((m_end_address-m_start_address+1)%4 == 0);
    unsigned int size = (m_end_address-m_start_address+1)/4;
    MEM = new int [size];
    for (unsigned int i = 0; i < size; ++i)
      MEM[i] = 0;
  }

  // destructor
  ~scClusterBusMemoryFast();

  // direct Slave Interface
  bool direct_read(int *data, unsigned int address);
  bool direct_write(int *data, unsigned int address);
  bool direct_read(scIGPMessage*){return true;};
  bool direct_write(scIGPMessage*){return true;};
  ClusterBusStatus read(scIGPMessage*){ return CLUSTER_BUS_OK;};
  ClusterBusStatus write(scIGPMessage*){return CLUSTER_BUS_OK;};

  // Slave Interface
  ClusterBusStatus read(int *data, unsigned int address);
  ClusterBusStatus write(int *data, unsigned int address);

  unsigned int start_address() const;
  unsigned int end_address() const;

private:
  int * MEM;
  unsigned int m_start_address;
  unsigned int m_end_address;

}; // end class scClusterBusMemoryFast

inline bool scClusterBusMemoryFast::direct_read(int *data, unsigned int address)
{
  return (read(data, address) == CLUSTER_BUS_OK);
}

inline bool scClusterBusMemoryFast::direct_write(int *data, unsigned int address)
{
  return (write(data, address) == CLUSTER_BUS_OK);
}

inline ClusterBusStatus scClusterBusMemoryFast::read(int *data
						   , unsigned int address)
{
  *data = MEM[(address - m_start_address)/4];
  return CLUSTER_BUS_OK;
}

inline ClusterBusStatus scClusterBusMemoryFast::write(int *data
						    , unsigned int address)
{
  MEM[(address - m_start_address)/4] = *data;
  return CLUSTER_BUS_OK;
}

inline  scClusterBusMemoryFast::~scClusterBusMemoryFast()
{
  if (MEM) delete [] MEM;
  MEM = (int *)0;
}

inline unsigned int scClusterBusMemoryFast::start_address() const
{
  return m_start_address;
}

inline unsigned int scClusterBusMemoryFast::end_address() const
{
  return m_end_address;
}

#endif
