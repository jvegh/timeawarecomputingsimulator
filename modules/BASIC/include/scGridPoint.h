/** @file scGridPoint.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the scEMPA simulator, Core.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scGridPoint_h
#define scGridPoint_h

//#include <iomanip>      // std::setfill, std::setw
//#include <systemc>
// "tlm.h"
//#include "tlm_utils/simple_initiator_socket.h"
//#include <cstring>
//#include "scIGPCB.h" //#include "scIGPfifo.h"
#include "scIGPMessage_if.h"
//#include "scLatchFIFO.h"
//#include "scGPMessagefifo.h"
//#include "scHThread.h" // Included by "scIGPMessage_if.h"

// These macros are used for benchmarking
// The work storage variable are defined as member functions, at the end
// Reset is done in constructor; the sum is collected and may be read in destructor
// The work storage also usable in derived classes
#define MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroTimeBenchmarking.h"    // Must be after the define to have its effect
#define SC_MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroScTimeBenchmarking.h"    // Must be after the define to have its effect


#include "scGridEnumTypes.h"    // Single bits related to operation
#include "scIGPMessage.h"

class scProcessor;
class scIGPMessage;
class scClusterBus;
class scIGPCB;
class scSynapticConnect;
extern string sc_time_to_nsec_Get(const int d, const int w, sc_time T);

// Define execution time measuring macros, per module
// Define this anyhow; if false no code is generated

// Functions to find out which type of memory is addressed
bool IsMemory(ClusterAddress_t CA);     //!< If it is any kind of memory
bool IsMemory0(ClusterAddress_t CA);    //!< If it is memory 0 (register)
bool IsMemory1(ClusterAddress_t CA);    //!< If it is memory 1 (cache)
bool IsMemory2(ClusterAddress_t CA);    //!< If it is memory 2 (I/O buffer)
bool IsMemory3(ClusterAddress_t CA);    //!< If it is memory 3 (convential, "far")
/*!
 * \class scGridPoint
 *
 * \brief This class implements the autonomous grid point functionality:
 * it is a communicating GridPoint. The communication interface is implemented
 * in scIGPMessage_if.
 *
 * It has its associated inter-gridpoint communication facilities
 * and communicates autonomously with its immediate neighbors and cluster members
 * via sending and receiving messages of form scIGPMessage.
 *
 * An scGridPoint must implement both slave and master interfaces:
 * the sending scGridPoint is the master, and the receiving scGridPoint
 * is the slave. The messages are always 'write' scIGPMessage messages;
 * the sender sends the message in 'burst' mode, and the slave reads it
 * uninterrupted, and after reading makes the corresponding action.
 * 
 * @verbatim
 *  |GridPoint
 *  |--/scGridPoint
 *  |..../AbstractCore
 *  |....../XXCore
 *  |..../NeurerCore
 * @endverbatim
 *
 * The classes subclassed from scGridPoint comprise two cooperating layers.
 * The top layer is responsible for the computing and morphing, implemented in subclasses
 * (such as a processor core or a neurer, under the supervision of the processor)
 * and this bottom layer is responsible
 * for the communication between modules having interface scIGPMessage_if
 * (and works in essentially unchanged form in the subclassed modules).
 * The bottom layer can receive ready-made
 * messages it can deliver to the destination using its own connections, it
 * can recognize if a message is for its top layer, and it can prepare
 * the message content in a buffer for the top layer and signal the arrival of the message.
 * The two layers share the tasks as follows.
 *
 * This bottom layer may receive packages (for one of its scHThread objects)
 * - i) from its own top layer
 * - ii) from one of its immediate neighbors
 * - iii) through the inter-cluster bus from the external world
 *
 * This bottom layer can send packages (for an scHThread object)
 * - i) to its own top layer (if the own address is recognized)
 * - ii) to one of its immediate neighbors
 * - iii) through the inter-cluster bus to the external world
 *
 * The top layer makes the payload work
 * and can utilize the services provided by this bottom layer.
 *
 * For efficiency,
 * - i) only the cluster heads have access to the inter-cluster bus
 * - ii) the "2nd neighbor" gridpoints can also be accessed directly, using 2 "hops"
 * (independently of whether they belong to the same cluster)
 *
 * The top layer
 * receives a task (a stream of machine instructions) from the memory:
 * it receives an  instruction pointer and initates the execution of the instruction
 * the instuction points to in the memory and for operating them typically
 * needs memory data pointed out by a memory pointer.
 * Accessing instruction/data may take some time, as well as the delivery of the
 * returned information corresponding to instruction and/or data.
 * As the excution of the next instruction is only possible
 * when all previous instructions terminated
 * (this status is or-ing the results status of the finished streams).
 * Then a core picks a new pointer value from the instruction or data pointer unit.
 * If all conditions are meet, that is i.e. the instruction is ready to proceed with execution
 * and both data and instruction are available, the execution of a machine instruction begins
 * and no action interrupts processing of the instruction.
 *
 * @see scIGPMessage
 * @see AbstractCore
 * @see AbstractNeuron
 */

class scGridPoint :  public GridPoint
        ,public scClusterBusSlave_if
        ,public scIGPMessage_if
{
    friend class scHThread; //friend class scAbstractNeurerConnection;
    friend class scSynapticConnect;
public:
    //Port declarations : scGridPoint status bits
        GridPointSignalPort_t
    msPort;   // Port declarations

//        std::unique_ptr< scClusterBusMaster_blocking  >  bus_port;
//  sc_port<scClusterBus_blocking_if*> bus_port;

    //Channel/Submodule* definitions
        // TLM-2 socket, defaults to 32-bits wide, base protocol
        // TLM-2 blocking transport method
        //virtual
//        void ICB_b_transport(const ClusterNeighbor Index, unsigned char* data, const int size );
        
     // Constructor declaration:
    /*!
     * \brief scGridPoint Creates an  GP for Processor scGridPoint at position, with name nm
     * \param[in] nm The SystemC name
     * \param[in] Processor the owner
     * \param[in] GP position
     * \param[in] StandAlone if the gridpoint shall be used as a stand-alone object
     */
    scGridPoint(sc_core::sc_module_name nm, // Just the SystemC name
                  scProcessor* Processor, // The present system is prepared for one processor only, but ...
                  const GridPoint GP
                , bool StandAlone
                );
   ~scGridPoint();//{}
        SC_HAS_PROCESS(scGridPoint);  // We have the constructor in the .cpp file
        // Group dealing with identification
        // The physical ID and the corresponding mask cannot be changed
/*        SC_GRIDPOINT_ID_TYPE
    FETCHID_Get(void){   return msFETCHTHREADID;} ///< Get internal ID (sequence number) of the scGridPoint
        SC_GRIDPOINT_ID_TYPE
    EXECHID_Get(void){   return msEXECTHREADID;} ///< Get internal ID (sequence number) of the scGridPoint
    */
    /**!
     * Add scHthread alias names to the default maps.
     * Both default and cluster address name stringf are added
     * The first schThread  is added in both "x" and "x/0" suffix, to enable using both forms in mappig */
                void
        AddDefaultAliasNames(void) ;
        SC_GRIDPOINT_ID_TYPE    ///< Return the unique ID of the scGridPoint
    ID_Get(){return msID;}
        SC_GRIDPOINT_MASK_TYPE ///< Return the mask form of the ID
    IDMask_Get(void){   return msMask.ID;}
     // Directly HW-related functionality
        string ///< Return the SC name of the module
    Name_Get(void){ return string(name());}

        void    ///< Add a HW thread to the scGridPoint
    AddHThread(unsigned int H, scHThread* P)
        {  assert(P);
           assert(H<MAX_HTHREADS);
            mHThreads[H] = P;}
        scHThread*
    AllocateAHThread(void);
        /*! When creating (subclassed) scGridPoint objects, it creates scHThreads (subclassed object) as configured.
         * scHTreds are fully configured and their names are put in the aliases map*/

        virtual void    ///< Create threads for HThreads sharing this processing unit
    CreateThreads(void);
        void
    Reset(void);


        void
    ResetForParent(scGridPoint* Parent);
        bool
    Deallocate(void);
        void
    RouteMessage(scIGPMessage* Message);
         void
    SendDirectMessageTo(scGridPoint* Target, scIGPMessage* Message);
        scGridPoint*
    Parent_Get(void) { return  msParent;}	///< Return the parent scGridPoint of the scGridPoint
      void
    Parent_Set(scGridPoint *P) { msParent = P;}	///< Set the new parent GridPoint
    // Implement the inter-gridpoint direct and inter-cluster bus  communication
        scIGPCB*    ///< Get address of the communication block
    IGPCB_Get(ClusterNeighbor index)
        {
            assert(!(index<cm_Head || index>=cm_Broadcast));
            return msIGPCB[index];
        }
      scIGPCB*
    msIGPCB[7];  // The  Inter-scGridPoint facilities from (!) Head to NW
      SC_WORD_TYPE
      Register_Get(int R){ return HThreadExec_Get()->Register_Get(R);}
  // direct Slave Interface; mainly for testing without the bus
        bool
    direct_read(int *data, unsigned int address);
        bool
    direct_write(int *data, unsigned int address);
        bool
    direct_read(scIGPMessage* M);
        bool
    direct_write(scIGPMessage* M);

  // Slave Interface, through the bus
        ClusterBusStatus
    read(int *data, unsigned int address){return CLUSTER_BUS_OK;}
        ClusterBusStatus
    write(int *data, unsigned int address){return CLUSTER_BUS_OK;}
        ClusterBusStatus
    read(scIGPMessage* M);
        ClusterBusStatus
    write(scIGPMessage* M);

      // State-bits-related functionality
        void
    AllocatedBit_Set(bool V=true);
        bool
    AllocatedBit_Get(void) {  return msStatus.Allocated;}
        void
    PreAllocatedBit_Set(bool V=true);
        bool // The GridPoints can have own preallocated GridPoints;  must be administered both here and at Topology level
    PreAllocatedBit_Get(void){  return msStatus.PreAllocated;}
        void
    DeniedBit_Set(bool P=true);
        bool
    DeniedBit_Get(void)
        {  return msStatus.Denied;}
        bool
    AvailableBit_Get(void)
        { return msStatus.Available;}
        void
    MetaBit_Set(bool P=true);
        bool
    MetaBit_Get(void){  return msStatus.Meta;}
        void
    WaitBit_Set(bool P=true);
        bool
    WaitBit_Get(void){  return msStatus.Wait;}
        bool
    IsAvailable(void)
        { return AvailableBit_Get();} ///< Return true if the scGridPoint is currently available
        bool
    IsAllocated(void) { return AllocatedBit_Get();}
        bool
    IsPreAllocated(void) { return PreAllocatedBit_Get();}
        bool
    IsDenied(void) { return DeniedBit_Get();}
        bool
    IsWaiting(void) {   return msStatus.Wait;}
        struct{
            sc_core::sc_event
         MakeFetch, FetchReady // These event signals the begin and end of the instruction fetch process
        ,MakeExec, ExecReady   // These event signals the begin and end of the instruction execute process
        ,MakeSend, SendReady   // These event signals the begin and end of the message sending process
        ,TransportedToIGPCB    // No dedicated method, just wait after sending the message
        ,SIGNAL_StateBit
            ,Msg_Reg_Received   // The core received a register-type message
            ,Msg_Qt_Received   // The core received a QT-type message
            ,Msg_Mem_Received   // The core received a memory-type message
            ;
    }EVENT_GRID; //< These events are handled at gridpoint level
        //!! Removed temporarily, until NEXT in AbstractCore fully implemented
//        ,NEXT       // The required action is carrried ot, may take the next one

        string
    StringOfMemoryAddress_Get(void)
        {
            ostringstream oss;
            oss << "0x" << hex << std::setfill('0') << std::setw((FMEMORY_ADDRESS_WIDTH+3)/4) << msMemoryAddress << dec;
            return oss.str();
        }
        // Return a hexa word dscribing the status
        string
    StringOfMessage_Get(scIGPMessage* Message);

        string
    StringOfStatus_Get(void)
        {
            ostringstream oss;
            oss << "0x" << hex << std::setfill('0') << std::setw(4) << mStatus << dec;
            return oss.str();
        }
   /*!
    * \fn scGridPoint::StringOfClusterAddress_Get
    * \return the string form of the complete cluster address of the gridpoint
    */
        string 
    StringOfClusterAddress_Get(void);
        /*!
         * \brief ClusterHead_Get
         * \return the address of the cluster head of the point
         */
        void
    SIGNAL_method(void);
    void
    doWriteSignals(void);
        SC_GRIDPOINT_MASK_TYPE
    PreAllocatedMask_Get(void);
        bool
    PreAllocatedMask_Set(SC_GRIDPOINT_MASK_TYPE Mask);
        virtual void
    ProcessMessage(scIGPMessage* Message);
 
        void
    ChildrenMaskBit_Set(scGridPoint* C);
        void
    ChildrenMaskBit_Clear( scGridPoint* C);
        SC_GRIDPOINT_MASK_TYPE
    ChildrenMask_Get(void) { return msMask.Children;}	///< Get the mask of allocating cores
        void
    ChildrenMask_Set(SC_GRIDPOINT_MASK_TYPE M) { msMask.Children = M;} ///< Set the mask of allocating cores

        SC_HTHREAD_MASK_TYPE
    HThreadsMask_Get(void){SC_HTHREAD_MASK_TYPE MyMask =(int16_t)(msMask.AllocatedHThreads).to_ulong();
            return MyMask;} ///< Return mask of allocated threads

        void
    PreAllocatedMaskBit_Set(scGridPoint* C, bool V=true); ///< Get the mask of allocating cores
        bool 
    IsPreAllocatedFor(scGridPoint* C);
        virtual bool
    IsMorphingInstructionFetched(scHThread* H){return true;}
    // Bits directly handling signals
        uint32_t readDataMem(uint32_t addr, int size);
        void
    HandleSuspending(void);
        /*!
         * \brief HThread_Get Get the thread H of the scGridPoint
         * \param H The ID if the requested thread
         * \return Pointer to the thread
         */
        scHThread*
    HThread_Get(SC_HTHREAD_ID_TYPE H)
        { assert((H>=0) && (H<MAX_HTHREADS));
            return mHThreads[H];}///<  Get  HTread No. H
/*        void
    HThread_Set(SC_HTHREAD_ID_TYPE H){ assert((H>=0) && (H<MAX_HTHREADS));  mHThread = mHThreads[H];}/// Set the actual HThread
*/
        scHThread*  ///< Return the executing thread
    HThreadExec_Get(void) { //assert(mHThreadExec);
                            return mHThreadExec;}
        scHThread*  ///< Return the fetching thread
    HThreadFetch_Get(void) { //assert(mHThreadFetch);
                             return mHThreadFetch;}
        scHThread*  ///< Return the fetching thread
    HThreadSend_Get(void) { //assert(mHThreadFetch);
                             return mHThreadSend;}
        scHThread*  ///< Return the fetching thread
    HThreadReceive_Get(void) { //assert(mHThreadFetch);
                             return mHThreadReceive;}
        void
    HThreadExec_Set(scHThread* H){mHThreadExec = H; }
        void
    HThreadFetch_Set(scHThread* H){mHThreadFetch = H; }
        void
    HThreadSend_Set(scHThread* H){mHThreadSend = H; }
        void
    HThreadReceive_Set(scHThread* H){mHThreadReceive = H; }
        /*!
         * \brief Send a notification to the thread that the requested memory content is available
         */
         core_cooperationmode_t
    CooperationMode_Get(void) { return msCooperationMode;}
        void
    CooperationMode_Set(core_cooperationmode_t M) { msCooperationMode = M;}
       string 
    PrologString_Get(void);
        void
    MemoryAddress_Set(SC_ADDRESS_TYPE MA) { msMemoryAddress = MA;}
        SC_ADDRESS_TYPE
    QTOffset_Get(SC_HTHREAD_ID_TYPE H)
        {   assert((H>=0)&(H<MAX_HTHREADS)); return mHThreads[H]->QTOffset_Get();}
        string
    StringOfName_Get(SC_HTHREAD_ID_TYPE H)
        { assert((H>=0)&(H<MAX_HTHREADS)); return mHThreads[H]->StringOfName_Get();}   ///

        bool
    CatchedAllocationError(scGridPoint* Parent);
        scGridPoint*
    AllocateFor(scGridPoint* Parent); /// Allocate this core for running a QT
        scGridPoint* 
    PreAllocateFor(scGridPoint* Parent);

        scIGPMessage*
    CreateRegisterMessageTo(scGridPoint* To, SC_GRIDPOINT_MASK_TYPE Mask);
        /*!
         * \brief scGridPoint::CreateQtCreateMessageTo
         * It can be needed to pass a QT message for Q_CREATE and Q_KILL
         * \param[in] To the destination gridpoint
         * \param[in] PC the program counter where the fragment will start
         * \param[in] Mask  The mask describing the registers to be passed
         * \param[in] BackMask the mask describing the expected backlinked register contents
         * \return  The prepared message
         */
        scIGPMessage*
    CreateQtCreateMessageTo(scGridPoint* To, SC_ADDRESS_TYPE PC,  SC_GRIDPOINT_MASK_TYPE Mask, SC_GRIDPOINT_MASK_TYPE BackMask);
        scIGPMessage*
    CreateQtKillMessageTo(scGridPoint* To);
        scIGPMessage*
    CreateSpecialMessageTo(scGridPoint* To, int16_t Key, int8_t RegNo,  int32_t R);
        void
    Status_Set(uint16_t S) {mStatus = S;}
        uint16_t
    Status_Get(void) { return mStatus;}
        void 
    ConnectToClusterBus(scClusterBus* ClusterBus);  // Connect this gridpoint to the inter-cluster bus
        scIGPMessage*
    CreateMemoryReadMessage(unsigned int Address, int Length);
        scIGPMessage*
    CreateMemoryWriteMessage(unsigned int Address, int Length);
        int32_t
    FlagWordLength_Get(void) {return 0;}
        int32_t
    ConditionCode_Get(void) {return 0;}
        void
    ConditionCode_Set(int32_t C){}
        SC_GRIDPOINT_MASK_TYPE
    doWaitMask_Get(SC_ADDRESS_TYPE Offset);
        virtual scGridPoint*
    doFindHostToProcess(SC_ADDRESS_TYPE Offs, SC_GRIDPOINT_MASK_TYPE Mask);
        bool
    doCanTerminate( SC_GRIDPOINT_MASK_TYPE Address);
        virtual void
    doCreateQT(scIGPMessage* Message){};
        bool
    doProcessMMessage(scIGPMessage* Message);
        virtual bool
    doProcessQMessage(scIGPMessage* Message);
        bool
    doProcessRMessage(scIGPMessage* Message);
        /*!
        * \brief doSetQTAddresses
        * \param Parent core of the gridpoint
        *
        * Presently has no real fuctionality, the QTs appear only in AbstractCore
        */
        virtual void
    doSetQTAddresses(scGridPoint* Parent)
        { }

        virtual void
    doSkipQTCode(void){}
/*        SC_ADDRESS_TYPE
    PC_Get() { return exec.PC;}
        void
    PC_Set(SC_ADDRESS_TYPE P) {exec.PC = P;}
        void
    fetchNewPC_Set(SC_ADDRESS_TYPE PC){ fetch.NewPC = PC;}	///< Set the PC of the core for fetching the next instruction
        SC_ADDRESS_TYPE
    fetchPC_Get(void) {return fetch.PC;}	///< Get PC of the core where the instruction is/was fetched
        void
    fetchPC_Set(SC_ADDRESS_TYPE PC){ fetch.PC = PC;}	///< Set PC of the core where the instruction will be fetched
*/
        /*!
         * \brief FETCH_thread
         * makes the actual "instruction fetch"
         */
        void
    FETCH_thread();
    /*!
     * \brief doFetchInstruction
     * The actual fetching is done in the subclassed routine
     * Here it imitates the action with a 10 ns wait
     */
        virtual bool
    doFetchInstruction(scHThread* H);
        /*!
         * \brief EXEC_thread
         * makes the actual "instruction exec"
         */
        void
    EXEC_thread();
        /*!
         * \brief SEND_thread
         * Send the actual message
         */
        void
    SEND_thread();
        /*!
         * \brief Send a message
         */
        virtual bool
    doExecInstruction(scHThread* H);
       void NotifyThreadOnMemoryArrival()
    {  mHThreadFetch->EVENT_HTHREAD.MemoryContentArrived.notify();}

    // It is administered in the scGridPoint if a HThread is allocated
        void    ///< Set thread H either active or inactive
    HThreadAllocatedBit_Set(SC_HTHREAD_ID_TYPE H, bool B)
        { assert(H < MAX_HTHREADS); msMask.AllocatedHThreads[H] = B;}
        bool    ///< Return if HThread is active
    HThreadAllocatedBit_Get(SC_HTHREAD_ID_TYPE H)
        { assert(H < MAX_HTHREADS); return msMask.AllocatedHThreads[H];}


        bool
    OperatingBit_Get(GridPointOperatingBit_t B);
        bool
    OperatingGroup_Get(GridPointOperatingBit_t G);
    int OperatingBits_Get(void){return mGridPointOperatingStateBit.to_ulong();}
    bool IsObserved(void) ///< Return true if any of the HThreads is observed
    {return  mObservedHThreads;}
    /*!
         * \brief Set if HThread M is observed
         * \param M Mask type
         * \param B true if observed
         */
        void
    ObservedHThreadBit_Set(SC_HTHREAD_MASK_TYPE M, bool B);
   protected:
        SC_HTHREAD_MASK_TYPE
    mObservedHThreads;                ///< The observed HThreads of this scGridPoint
        int16_t
    InstanceCount_Get(void) {return mInstanceCount % 50;}

/*!
 * 
 *  \brief Create a message and put the addresses into the message
 *  \param[in] To The addressed scGridPoint (the destination of the message)
 *  \param[in] Type The type of the message
 *  \param[in] Length The length of the message (in 32-bit words)
 * \return the prepared message, with only partially filled buffer
 */
        scIGPMessage*
    CreateMessageTo(scGridPoint* To, IGPMessageType Type, int Length);
        void
    AvailableBit_Set(void);
        void
    SleepingBit_Set(void);
        void
    SetupMessageAddressTo(scGridPoint* GP);

        SC_GRIDPOINT_ID_TYPE
    msID;   ///< ID if the scGridPoint
        scHThread
    *mHThreadFetch,  *mHThreadExec, *mHThreadSend, *mHThreadReceive;
        scGridPoint*
    msParent;		///< Parent gridpoint
        GridPointStatusBit_t
    msStatus;
        GridPointSignal_t
    msSignal;   ///< This aggregate comprises all signals shown to the outer world
        GridPointMask_t
    msMask;     ///< This aggregate comprises all masks belonging to the GridPoint
        RegisterLatch_fifo*
    msRegisterLatch;
        SC_ADDRESS_TYPE
    msMemoryAddress;
        SC_ADDRESS_TYPE
    msWaitOffset; // The offset of the QT we are waiting for
        sc_time
    msWaitBegin;  ///< The beginning of the waiting period
        /*! This memory buffer storage is the link between the top layer and the
        * bottom layer: one party places the content here, notifies the other party
        * via EVENT_GRID.MemoryContentArrived[MAX_HTHREADS_LIMIT].notify
        * Only ony of the possible two directions can be used at a time
        * All HTreads have their own buffer;
        */
//    int32_t    mMemoryBuffer[MAX_HTHREADS_LIMIT][MAX_IGPCBUFFER_SIZE];
    // Simple accessor functions
        core_cooperationmode_t
    msCooperationMode;
        uint16_t
    mInstanceCount; // How many times this object was instantiated
        scHThread*
    mHThreads[MAX_HTHREADS];    ///< Stores the address of HThreads
        void OperatingGroup_Set(GridPointOperatingBit_t G, bool B);
        void OperatingBit_Set(GridPointOperatingBit_t B, bool V);
    protected:
        // -- These bits are used in monitoring (by the simulator) gridpoint
        std::bitset<gob_Max>
    mGridPointOperatingStateBit;
        // -- These variables are used to benchmark the running time spent in the objects
        chrono::steady_clock::time_point
    m_running_time; // A work variable, do not touch outside the macros
        std::chrono::duration< int64_t, nano>
    m_part_time, m_sum_time;    // Work variables, contain actual benchmark duration and their sum

}; // of scGridPoint

#endif // scGridPoint_h

// Fast memory; reset
