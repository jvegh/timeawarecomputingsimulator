/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @file Clustering.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Topology information for the electronic modules arranged in a cluster
 */

#ifndef CLUSTERING_H
#define CLUSTERING_H
//#include "Utils.h"
#include "BasicConfig.h"
#include <string>
using namespace std;
typedef struct  {
    string name;
    int id;
} NamedEnum_struct; /**< A structure where the name string can be reached through its index */

extern NamedEnum_struct ClusterMembers[8];
extern int moduloN(int x,int N);

/// The gridpoints can be the head of the cluster, a member of a cluster, or neither (stand-alone)
typedef enum {   
    cs_NONE,    //!< not specified (untouched)
    cs_Member,  //!< is an ordinary member
    cs_Head,    //!< is the head of its cluster
    cs_Error    //!< is in error
} ClusterStatus;

/*! \var typedef ClusterNeighbor
 * \brief The neighborship in the cluster: the members are referred to with their geographic direction
 */
typedef enum
{   cm_Head, //!< The central gridpoint
    cm_North, //!< The North neighbor
    cm_NE, //!< The North-East neighbor
    cm_SE, //!< The South-East neighbor
    cm_South, //!< The South neighbor
    cm_SW,  //!< The South-West neighbor
    cm_NW, //!< The North-West neighbor
    cm_Broadcast //!< All cluster members (broadcast)
} ClusterNeighbor;

/*! \struct ClusterAddress_t
 * \brief A GridPoint can be addressed also by its cluster address of type ClusterAddress_t.
 * Attention: the scHThread number is not stored in the grid (although its place is reserved);
 * its field must be handled per instance.
 *
 * The cluster address comprises address information akin to computer networking.
 * The cluster address is essentially a special uint32_t number, where
 * the different bit fields denote different parts of the address.
 * A cluster address is unique within the system.
 * The GridPoint elements can be sought also by this address
 * The meaning of the bitfields in the cluster addressing
 */

typedef struct {
    uint32_t HThread: HTHREAD_BUS_WIDTH; //!< The 'hardware thread' the module handles
    uint32_t Member: 3;                  //!< @see ClusterNeighbor
    uint32_t Proxy:3;                    //!< Reachable through this neighbor (Relative to the core)
    uint32_t Cluster: CLUSTER_BUS_WIDTH; //!< The sequence number of the cluster
    uint32_t Topology: TOPOLOGY_BUS_WIDTH; //!< The processor the cluster belongs to
    uint32_t Card: CARD_BUS_WIDTH;      //!< The card the topology (like a many-core processor) belongs to
    uint32_t Rack: RACKS_BUS_WIDTH;      //!< The rack the card belongs to
    uint32_t Status: 2;                 //!< @see ClusterStatus, constant
    uint32_t Reserved: (32-HTHREAD_BUS_WIDTH-6-2-CLUSTER_BUS_WIDTH-CARD_BUS_WIDTH-TOPOLOGY_BUS_WIDTH-RACKS_BUS_WIDTH);
      //!< Reserved for later use
    int &ToInt() {
        return *reinterpret_cast<int *>(this);}//!< Just get it as integer
} ClusterAddress_t; //!< A bit-sliced address used in the system

#if (CORE_GRID_SIZE_X > 127)
    #error Adjust GridPoints X position size
#endif
#if (CORE_GRID_SIZE_Y > 127)
    #error Adjust GridPoints Y position size
#endif


#endif // CLUSTERING_H
