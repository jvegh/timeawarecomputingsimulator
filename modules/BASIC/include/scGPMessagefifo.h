/** @file scGPMessagefifo.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief This FIFO stores temporarily the messages from other gridpoints
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scGPMessagefifo_h
#define scGPMessagefifo_h

//#include <systemc>
//#include "BasicConfig.h"
//#include "scIGPfifo.h"
using namespace std;
using namespace sc_core;
using namespace sc_dt;
class scIGPMessage;

class scGPMessagefifowrite_if : virtual public sc_interface
{
   public:
     virtual void write(scIGPMessage*) = 0;
     virtual void reset() = 0;
};

class scGPMessagefiforead_if : virtual public sc_interface
{
   public:
     virtual void read(scIGPMessage*&) = 0;
     virtual int num_available() = 0;
};

/*!
 * \class scGPMessagefifo
 * 
 * The messages are written to this FIFO
 * The messages can be received from either directly from one of the scIGPCBs
 * or through the inter-cluster bus. The receiver objects writes the message
 * into the FIFO and the scGridPoint processes it, after that it receives a message
 * 
 */
class scGPMessagefifo : public sc_channel, public scGPMessagefifowrite_if, public scGPMessagefiforead_if
{
//    friend class scGridPoint;
    friend class scIGPMessage_if;
  public:
     scGPMessagefifo(sc_module_name name) : sc_channel(name), num_elements(0), first(0) {}

     void write(scIGPMessage* c) {
       if (num_elements == max)
         wait(scGPMessagefifo_read_event);

       data[(first + num_elements) % max] = c;
       ++ num_elements;
       scGPMessagefifo_write_event.notify();
     }

     void read(scIGPMessage* &c){
       if (num_elements == 0)
         wait(scGPMessagefifo_write_event);

       c = data[first];
       -- num_elements;
       first = (first + 1) % max;
       scGPMessagefifo_read_event.notify();
     }

     void reset() { num_elements = first = 0; }

     int num_available() { return num_elements;}

   private:
     enum e { max = 10 };
     scIGPMessage* data[max];
     uint16_t num_elements, first;
     sc_event scGPMessagefifo_write_event, scGPMessagefifo_read_event;
};

#endif //scGPMessagefifo_h
