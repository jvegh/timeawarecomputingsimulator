/** @file scIGPMessage_if.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Functionality of scIGPMessage-related stuff
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scIGPMessage_if_h
#define scIGPMessage_if_h

//#include <iomanip>      // std::setfill, std::setw
//#include <systemc>
// "tlm.h"
//#include "tlm_utils/simple_initiator_socket.h"
//#include "scIGPCB.h" //#include "scIGPfifo.h"
//#include "GridPoint.h"
//#include "scIGPMessage.h"
#include "scClusterBusSlave_if.h"
#include "scClusterBusMaster_blocking.h"
#include "scLatchFIFO.h"
#include "scGPMessagefifo.h"
#include "scTimedFIFO.h"
#include "scHThread.h"

// These macros are used for benchmarking
// The work storage variable are defined as member functions, at the end
// Reset is done in constructor; the sum is collected and may be read in destructor
// The work storage also usable in derived classes
/*#define MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroTimeBenchmarking.h"    // Must be after the define to have its effect
#define SC_MAKE_TIME_BENCHMARKING  // uncomment to measure the time with benchmarking macros
#include "MacroScTimeBenchmarking.h"    // Must be after the define to have its effect
*/

using namespace std;
using namespace sc_core;
using namespace sc_dt;

class scProcessor;
class scIGPMessage;
class scClusterBus;
class scIGPCB;
extern string sc_time_to_nsec_Get(const int d, const int w, sc_time T);
extern string DebugRoute;

#define DEBUG_CONTRIBUTE_STRING(D,S,M) \
    DebugRoute.append(StringOfClusterAddress_Get() +"%" +string(S).append(M));

// Define execution time measuring macros, per module
// Define this anyhow; if false no code is generated


using namespace sc_core; using namespace std;

/*!
 * \class scIGPMessage_if
 *
 * \brief This class communication interface for scIGPMessage communication for the autonous communication functionality:
 * for both scGridPoint and scClusterBusMemoryXXX
 * (XX=Slow, Fast, Cache, Buffer).
 *
 * The source of the messages can be scClusterBus or scGridPoint.
 * The actual message receiving mechanism is slightly different, the essence of processing them is the same.
 *
 * In the case of the scGridPoint objects, only cluster heads have access
 * to the scClusterBus, but all cluster members can send/receive direct messages
 * (via their scIGPCB blocks) to each other.
 * Memories form a special "cluster": they formally have cluster address,
 * but as they are physically different entitities: all memories have direct access
 * to scClusterBus bus they have no interface to send direct messages to each other.
 *
 * An scIGPMessage_if must implement both slave and master interfaces:
 * the sending scIGPMessage_if is the master, and the receiving scIGPMessage_if
 * is the slave. The messages are always 'write' scIGPMessage messages;
 * the sender (master) sends the message in 'burst' mode, and the receiver (slave) reads it
 * uninterrupted, and after reading, makes the corresponding action.
 * 
 * This interface is used in all object sending scIGPMessage messages,
 *
 * @see scGridPoint
 * @see scClusterBusMemorySlow
 *
 * This interface object belongs to the bottom layer, and is connected to the
 *
 * actual message processing via virtual function scIGPMessage_if#PSendClusterMessage.
 * The interface is not aware of the content of the message it deals with.
 *
 * Via this bottom layer interface the object may receive packages
 * - i) from its own top layer
 * - ii) from one of its immediate neighbors
 * - iii) through the inter-cluster bus from the external world
 *
 * This bottom layer can send packages
 * - i) to its own top layer (if the own address is recognized)
 * - ii) to one of its immediate neighbors
 * - iii) through the inter-cluster bus to the external world
 *
 * For efficiency,
 * - i) only the cluster heads have access to the inter-cluster bus
 * - ii) the "2nd neighbor" gridpoints can also be accessed directly, using 2 "hops"
 * (independently of whether they belong to the same cluster)
 *
 * @see scIGPMessage
 * @see AbstractCore
 * @see AbstractNeuron
 */

class scIGPMessage_if :    // The IGPMessage related routines
//!!        public scClusterBusSlave_if, // Actually, it is a 'fast' interface
        public sc_core::sc_module
{
public:
/*        sc_in_clk
    clock;
*/        scClusterBusMaster_blocking*
    mMaster_if;

//        std::unique_ptr< scClusterBusMaster_blocking  >  bus_port;
//  sc_port<scClusterBus_blocking_if*> bus_port;

    //Channel/Submodule* definitions
        // TLM-2 socket, defaults to 32-bits wide, base protocol
        // TLM-2 blocking transport method
        //virtual
//        void ICB_b_transport(const ClusterNeighbor Index, unsigned char* data, const int size );
public:
     // Constructor declaration:
    scIGPMessage_if(sc_core::sc_module_name nm // Just the systemC name
                    ,scProcessor* Proc   // The processor we belong to
                );
   ~scIGPMessage_if();//{}
        SC_HAS_PROCESS(scIGPMessage_if);  // We have the constructor in the .cpp file
        // Group dealing with identification
        // The physical ID and the corresponding mask cannot be changed
        uint16_t
    BusPriority_Get(void){return mBusPriority;}
        scProcessor*
    Processor_Get(void) {    return msProcessor;}
        scTimedIGPMessage_fifo*
    TimedFIFO;  ///< This stores the timed message for this gridpoint
        scGPMessagefifo*
    GPMessagefifo;  ///< This stores the messages for this gridpoint
        virtual string
    StringOfClusterAddress_Get(void) = 0;
        virtual string
    StringOfMessage_Get(scIGPMessage* Message) = 0;
        ClusterBusStatus
    SendClusterMessage(scIGPMessage* M);
        ClusterBusStatus
    ReceiveClusterMessage(scIGPMessage* M);
    protected:
        void
    Reset(void);
        // process
       void bus_action(void);
       /*!
        * \brief  This thread receives a message from outside, from any the scGridPoint
        */
       void
   ReceiveMessage_thread(void);
       /*!
        * \brief ReceiveTimedMessage_thread
        * The message can be received form any other scGricPoint
        */
       void
   ReceiveTimedMessage_thread(void);
       virtual void // Must be implemented in scGridPoint and scClusterBusMemorySlow
   RouteMessage(scIGPMessage* Message) = 0; // How to forward a message in the bottom layer
#if 0
        SC_GRIDPOINT_ID_TYPE
    ID_Get(void){   return msID;} ///< Get internal ID (sequence number) of the scGridPoint
        SC_GRIDPOINT_MASK_TYPE
    IDMask_Get(void){   return msMask.ID;}  ///< Get internal identifying mask of the scGridPoint
     // Directly HW-related functionality
        void
    Reset(void);

        void
    ResetForParent(scGridPoint* Parent);
        bool
    Deallocate(void);
        /*!
         * \brief This thread receives a message from outside the scGridPoint
         */
        void
    ReceiveMessage_thread(void);
        void
    ReceiveTimedMessage_thread(void);
        void
    RouteMessage(scIGPMessage* Message);
         void
    SendDirectMessageTo(scGridPoint* Target, scIGPMessage* Message);
        scGridPoint*
    Parent_Get(void) { return  msParent;}	///< Return the parent scGridPoint of the scGridPoint
      void
    Parent_Set(scGridPoint *P) { msParent = P;}	///< Set the new parent GridPoint
    // Implement the inter-gridpoint direct and inter-cluster bus  communication
        scIGPCB*    ///< Get address of the communication block
    IGPCB_Get(ClusterNeighbor index)
        {
            assert(!(index<cm_Head || index>=cm_Broadcast));
            return msIGPCB[index];
        }
      scIGPCB*
    msIGPCB[7];  // The  Inter-scGridPoint facilities from (!) Head to NW
      SC_WORD_TYPE
      Register_Get(int R){ return HThread_Get()->Register_Get(R);}
  // direct Slave Interface; mainly for testing without the bus
        bool
    direct_read(int *data, unsigned int address);
        bool
    direct_write(int *data, unsigned int address);
        bool
    direct_read(scIGPMessage* M);
        bool
    direct_write(scIGPMessage* M);

  // Slave Interface, through the bus
        ClusterBusStatus
    read(int *data, unsigned int address){return CLUSTER_BUS_OK;}
        ClusterBusStatus
    write(int *data, unsigned int address){return CLUSTER_BUS_OK;}
        ClusterBusStatus
    read(scIGPMessage* M);
        ClusterBusStatus
    write(scIGPMessage* M);
   // process
  void bus_action(void);
        bool
        struct{
            sc_core::sc_event
//        TIMEOUT    // The required timeout is over
//        ,
         MakeFetch, FetchReady// These event signal the begin and end of the process
        ,TransportedToIGPCB // No dedicated method, just wait after sending the message
        ,SIGNAL_StateBit
            ,Msg_Reg_Received   // The core received a register-type message
            ;
    }EVENT_GRID; //< These events are handled at gridpoint level
        //!! Removed temporarily, until NEXT in AbstractCore fully implemented
//        ,NEXT       // The required action is carrried ot, may take the next one

        string
    StringOfMemoryAddress_Get(void)
        {
            ostringstream oss;
            oss << "0x" << hex << std::setfill('0') << std::setw((FMEMORY_ADDRESS_WIDTH+3)/4) << msMemoryAddress << dec;
            return oss.str();
        }
        // Return a hexa word dscribing the status
        string
    StringOfMessage_Get(scIGPMessage* Message);

        string
    StringOfStatus_Get(void)
        {
            ostringstream oss;
            oss << "0x" << hex << std::setfill('0') << std::setw(4) << mStatus << dec;
            return oss.str();
        }
   /*!
    * \fn scGridPoint::StringOfClusterAddress_Get
    * \return the string form of the complete cluster address of the gridpoint
    */
        string 
    StringOfClusterAddress_Get(void);
        /*!
         * \brief ClusterHead_Get
         * \return the address of the cluster head of the point
         */
        void
    SIGNAL_method(void);
    void
    doWriteSignals(void);
        SC_GRIDPOINT_MASK_TYPE
    PreAllocatedMask_Get(void);
        bool
    PreAllocatedMask_Set(SC_GRIDPOINT_MASK_TYPE Mask);
        void 
    ProcessMessage(scIGPMessage* Message);
 

        void
    PreAllocatedMaskBit_Set(scGridPoint* C, bool V=true); ///< Get the mask of allocating cores
        bool 
    IsPreAllocatedFor(scGridPoint* C);
    // Bits directly handling signals
        uint32_t readDataMem(uint32_t addr, int size);
//        Performance *
    scProcessor* Processor_Get(void) {    return msProcessor;}
        void
    HandleSuspending(void);

        scHThread*
    HThread_Get(void) { return mHThread;}
        void
    MemoryAddress_Set(SC_ADDRESS_TYPE MA) { msMemoryAddress = MA;}
        SC_ADDRESS_TYPE
    QTOffset_Get(SC_HTHREAD_ID_TYPE H)
        {   assert((H>=0)&(H<MAX_HTHREADS)); return mHThreads[H]->QTOffset_Get();}
        string
    StringOfQT_Get(SC_HTHREAD_ID_TYPE H)
        { assert((H>=0)&(H<MAX_HTHREADS)); return mHThreads[H]->StringOfQT_Get();}   ///

        scIGPMessage*
    CreateRegisterMessageTo(scGridPoint* To, SC_GRIDPOINT_MASK_TYPE Mask);
        /*!
         * \brief scGridPoint::CreateQtCreateMessageTo
         * It can be needed to pass a QT message for Q_CREATE and Q_KILL
         * \param[in] To the destination gridpoint
         * \param[in] PC the program counter where the fragment will start
         * \param[in] Mask  The mask describing the registers to be passed
         * \param[in] BackMask the mask describing the expected backlinked register contents
         * \return  The prepared message
         */
        scIGPMessage*
    CreateQtCreateMessageTo(scGridPoint* To, SC_ADDRESS_TYPE PC,  SC_GRIDPOINT_MASK_TYPE Mask, SC_GRIDPOINT_MASK_TYPE BackMask);
        scIGPMessage*
    CreateQtKillMessageTo(scGridPoint* To);
        scIGPMessage*
    CreateSpecialMessageTo(scGridPoint* To, int16_t Key, int8_t RegNo,  int32_t R);
        void
    Status_Set(uint16_t S) {mStatus = S;}
        uint16_t
    Status_Get(void) { return mStatus;}
        scTimedIGPMessage_fifo*
    TimedFIFO;  ///< This stores the timed message for this gridpoint
        scGPMessagefifo*
    GPMessagefifo;  ///< This stores the messages for this gridpoint
        void 
    ConnectToClusterBus(scClusterBus* ClusterBus);  // Connect this gridpoint to the inter-cluster bus
        ClusterBusStatus
    SendClusterMessage(scIGPMessage* M);
        ClusterBusStatus 
    ReceiveClusterMessage(scIGPMessage* M);
        scIGPMessage*
    CreateMemoryReadMessage(unsigned int Address, int Length);
        scIGPMessage*
    CreateMemoryWriteMessage(unsigned int Address, int Length);
        int32_t
    FlagWordLength_Get(void) {return 0;}
         bool
    doProcessMMessage(scIGPMessage* Message);
        virtual bool
    doProcessQMessage(scIGPMessage* Message);
        bool
    doProcessRMessage(scIGPMessage* Message);
        scIGPMessage*
    CreateMessageTo(scGridPoint* To, IGPMessageType Type, int Length);
    SetupMessageAddressTo(scGridPoint* GP);

        SC_GRIDPOINT_ID_TYPE
    msID;
//!!        RegisterLatch_fifo*
//!!    msRegisterLatch;
        SC_ADDRESS_TYPE
    msMemoryAddress;
#endif //0
        scProcessor*
    msProcessor; ///< Just remember where we were born
        uint16_t
    mStatus;    // This is a status bit of the gridpoint
        uint16_t
    mBusPriority;  ///< The bus priority when the scGridPoint is master
        bool
    m_lock; // It the bus is to be locked
}; // of scIGPMessage_if

#endif // scIGPMessage_if

