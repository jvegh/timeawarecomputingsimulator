/** @file scLatchFIFO.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the EMPA simulator, LatchFIFO.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scLatchFIFO_h
#define scLatchFIFO_h
#include "BasicConfig.h"
//#include <systemc>
//#include "Utils.h"
//#include "scGridPoint.h"

using namespace sc_core;
//Submodule forward class declarations

/*! @struct RegisterLatch_Type
    Register contents as stored in the latch of the receiver
  */

struct RegisterLatch_Type
{
//    ClusterAddress_t SenderCore;
    SC_ADDRESS_TYPE QTOffset;   ///< The offset the content belongs to
    SC_WORD_TYPE RegisterContent;   ///< The content of the register
    int8_t RegisterSelect; ///< The register sequence number
//    bool ProxyMode;
//    InterCoreTransferMode_t TransferMode;
};


class RegisterLatch_write_if : virtual public sc_interface
{
  public:
    virtual void write(RegisterLatch_Type) = 0;
    virtual void reset() = 0;
};

class RegisterLatch_read_if : virtual public sc_interface
{
  public:
    virtual bool read(RegisterLatch_Type &T, SC_ADDRESS_TYPE Offset) = 0;
    virtual int num_available() = 0;
};


// This is the latch FIFO for the register values backlinked by the children
class RegisterLatch_fifo: public sc_core::sc_channel, RegisterLatch_write_if, RegisterLatch_read_if
{
  public:
     RegisterLatch_fifo(const sc_module_name& name) ;

     void write(RegisterLatch_Type T);
     void dowrite(RegisterLatch_Type T);
     bool read(RegisterLatch_Type &T, SC_ADDRESS_TYPE Offset);
     bool doread(RegisterLatch_Type &T, SC_ADDRESS_TYPE Offset);
     void reset() { num_elements = first = 0;}
     int num_available() { return num_elements;}

  private:
     enum e { max = 10 };
     RegisterLatch_Type data[max];
     int num_elements, first;
     sc_event write_event, read_event;
};

#endif // scLatchFIFO
