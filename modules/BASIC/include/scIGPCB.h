/** @file scIGPCB.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The Inter-GridPoint Communication Block
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */


#ifndef scIGPCB_h
#define scIGPCB_h
//#include "scTypes.h"
//#include "tlm.h"
//#include "tlm_utils/simple_target_socket.h"
//#include "Config.h"
#include "scIGPfifo.h"

using namespace sc_core;
//Submodule forward class declarations

class scGridPoint; class scProcessor;
class scIGPCB : public sc_core::sc_module
//SC_MODULE(scIGPCB)
{
    friend class scProcessor;
  public:
    //Port declarations
//    sc_in_clk clock;
      //    sc_in<bool> Clock;  ///< The clock signal, received from scCore
    //Channel/Submodule* definitions
//**    BL_FromSV_fifo* msFromSV_FIFO;
    // TLM-2 socket, defaults to 32-bits wide, base protocol
//    InterCoreBusCtrl* mIntercoreBus;
/*    tlm_utils::simple_target_socket<scIGPCB>* receive_socket;
    tlm_utils::simple_initiator_socket<scIGPCB>* send_socket;
    // TLM-2 blocking transport method
    virtual void b_transport( tlm::tlm_generic_payload& trans, sc_time& delay );
*/      SC_HAS_PROCESS(scIGPCB);
    // This is a more direct way to communicate: the messages is written  to a BL_FromSV_fifo
    // that ends in another scIGPCB. Only one of the two mechanisms is needed  
     sc_port<scIGPwrite_if> scIGPout;
     sc_port<scIGPread_if> scIGPin;
     scIGPfifo *IGPFIFOin,*IGPFIFOout;  // The IGPCB has and input and an output FIFO
      
    scIGPCB(sc_core::sc_module_name nm, scGridPoint* GP, scIGPCB* IGPCB); // This is for testing only
      virtual
   ~scIGPCB();
        void Reset(void);
        scIGPCB*
    OtherIGPCB_Get(void){ return msOtherIGPCB;}
        scGridPoint* /*! Return the address of the scGridPoint this scIGPCB belongs to */
    GridPoint_Get(void){return msGridPoint;}
/*        void
    ConnectSocketsTo(scIGPCB* OtherIGPCB);*/
        void 
    ConnectFIFOsTo(scIGPCB* const OtherIGPCB);
        struct 
        {
        sc_core::sc_event
    MessageReceived;    ///< Notifies when some other IGPCB sent a message
    }EVENT_IGPCB;
        bool
    Sending_Get(void) {return msIsSending;}
        void
    Sending_Set(bool B){msIsSending = B;}
  protected:
    // SystemC-related member variables
    // Operation-related member objects
        scGridPoint*
    msGridPoint;    ///< My own scGridPoint
        scIGPCB*
    msOtherIGPCB;    ///< My partner IGPCB
        void
    OtherIGPCB_SetLate(scIGPCB* OtherIGPCB){msOtherIGPCB = OtherIGPCB; }
  private:
        bool
    msIsSending;

        void /*! Triggered when a message is recaived by the scIGPCB */ 
    MessageReceived_thread(void);
#ifdef other
      void ReceivedClock(void);
    int
  msClocks;  ///< No of clocks the core was active in
 #endif
};
#endif    //scICGPB_h
