/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @file scIGPMessage.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Basic messaging forms between scGridPoints.
 */

#ifndef scIGPMessage_h
#define scIGPMessage_h

#include <cstring>
#include "GridPoint.h"
class scGridPoint;
using namespace sc_core; using namespace std;

/*! \struct IGPMessageType
 * \brief The basic message types of scIGPMessage messages
 */
typedef enum {msg_Reg,  //!< Register-type messages
              msg_Qt,   //!< QT-type messages
              msg_Mem,  //!< Memory-type messages
              msg_Coop,  //!< Cooperation-type messages
              msg_Neur  //!< Neuron-type messages
             } IGPMessageType;

///  The messages may use this feature word in different ways
/*! \struct MessageFeatures_t
 * \brief The basic message types of IGP messages
 */
typedef struct {
        uint32_t Time : (32-4-4-4-4); /*!< The delay time of the message */
        uint32_t SubKey: 4;         /*!<  Only effective in 'C'mode */
        uint32_t ReplyLength : 4;   /*!< Relevant for 'read memory' messages, the requested length of the reply. */
                                    /*!< Relevant in 'S' mode, stores the register ID. */
        uint32_t Length : 4;        /*!< Up to this number of 32-bit words. In a request to read memory, it is 0; the reply uses ReplyLength */
        uint32_t Type: 4;           /*!< We can define 16 different messages */
    int &ToInt() { return *reinterpret_cast<int *>(this);} /*!< Convert to uint32_t */
} MessageFeatures_t;

/*!
 * \brief The scIGPMessage class
 *
 * The addresses must comprise the HThread number correctly!
 * A scIGPMessage breaks down as follows. All messages have a header
 * - [0] message destination of type ClusterAddress_t
 * - [1] message features of type MessageFeatures_t
 * - [2] response address of type ClusterAddress_t
 *
 * Register ('R') messages:
 * - [3] SC_GRIDPOINT_MASK_TYPE mask of registers comprised
 * - [4-] Register contents listed above, see also MessageFeatures_t#Length
 * @see scGridPoint#CreateRegisterMessageTo
 *
 *
 * Cooperationl ('C') messages:
 * - [3] the message contents
 * @see scGridPoint#CreateSpecialMessageTo
 * @see scGridPoint#CreateQtKillMessageTo
 *
 *
 * Quasi-thread ('Qt') messages
 * The contents are as listed at type 'R', plus the
 * - [lengthof('R')+1] Instruction Pointer
 * - [lengthof('R')+2] backlink register SC_GRIDPOINT_MASK_TYPE mask
 * - [lengthof('R')+3] status word (if needed, like by Y86)
 * @see scGridPoint#CreateQtCreateMessageTo
 *
 *
 * Memory ('M') messages
 * The memories are accessed anyhow through the inter-cluster bus,
 * but the mechanism is transparent for the programmer and the cores.
 * - [3] memory address
 * - [4-] memory data, in length as given by MessageFeatures_t#Length
 *
 * The memory read requests do have no extra (empty and obsolete) bytes for the requested contents.
 * Rather, they set the size to 4, and MessageFeatures_t#ReplyLength delivers the reply length
 * information to the memory, and the sent-back message has the corresponding length.
 * This feature make a message CreateReadMessageReply obsolete: if the length is 4, it is a read message.
 * @see scClusterBusMemorySlow#CreateWriteMessage
 * @see scClusterBusMemorySlow#CreateReadMessageReply
 *
 * Neural ('N') messages
 * The messages are sent by scHThread and received by scHThread.
 * More precisely, from AbstractNeurer to  AbstractNeurer.
 * The content depends on the message type
 *  Type 0 (a spike)
 * - [3] memory address of the Spike to be executed
 * - [4-] scale factors, TimeFact<<16 + AmplitudeFact
 *

 * That is, the mimimum length is four 32-bit words.
 * The message is created by the sender scGridPoint, and deleted by the receiver scGridPoint
*/

class scIGPMessage
{
//    friend class scGridPoint;   /*!< The messages are handled directly by scGridPoint */
//    friend class scProcessor;   /*!< and by scProcessor */
//    friend class scClusterBusMemorySlow; /*!< and may go directly to the bus */

  public:
    /*!
     * \brief Implements the messages for the autonomously communicating grid point functionality
     *
     * The messages comprise the destination address and the reply-to address.
     * When assembling the messages, the sending gridpoint fills out the address fields properly,
     * so the bottom layer can forward the message autonomously to its destination.
     * That is, the bottom layer serves as a router, and akin to the computer network routers,
     * has the right to change the addressing mode in function of the actual conditions.
     */

    scIGPMessage(void);
   ~scIGPMessage(void);
        void
        //! \brief Sets the index-th element of the buffer to C
    BufferContent_Set(int index, int32_t C)
    {   assert(!(index<0 || index >= MAX_IGPCBUFFER_SIZE)); // wrong index
        mBuffer[4+index] = C;
    }
        //! \brief Returns the index-th element in the buffer
        int32_t
    BufferContent_Get(int index)
    {   assert(!(index<0 || index >= MAX_IGPCBUFFER_SIZE)); // wrong index
        return mBuffer[4+index];
    }
        /*! \brief  return content of the index-th element in hexadecimal string form */
        string
    StringOfBufferContent_Get(int index)
    {
        ostringstream oss;
        oss << "0x" << hex << BufferContent_Get(index) << dec ;
        return oss.str();
    }
        /*!
         * \brief Returns the features word of the message
         */
        MessageFeatures_t
    FeatureWord_Get(void)
    {
        MessageFeatures_t MsgType;
        static_assert( sizeof(MsgType) <= sizeof(mBuffer[1]), "!");
        memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
        return MsgType;
    }
        /*!
         * \brief Sets the features word to  F
         * \param F the new features word MessageFeatures_t
         */
        void
    FeatureWord_Set(MessageFeatures_t &F)
    {
        static_assert( sizeof(F) <= sizeof(mBuffer[1]), "!");
        memcpy(&mBuffer[1],  &F, sizeof(mBuffer[1]));
    }
    //! \brief Returns MessageFeatures_t#Type
        IGPMessageType
    Type_Get(void)
    {
        MessageFeatures_t MsgType;
        static_assert( sizeof(MsgType) <= sizeof(mBuffer[1]), "!");
        memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
        return (IGPMessageType)MsgType.Type;
    }
        //! \brief Set the type of the message in the buffer to MessageFeatures_t#Type T
        void
    Type_Set(IGPMessageType &T)
    {
        MessageFeatures_t MsgType;
        static_assert( sizeof(T) <= sizeof(mBuffer[1]), "!");
        memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
        MsgType.Type = T;
        memcpy(&mBuffer[1],  &MsgType, sizeof(mBuffer[1]));
    }
        //! \brief Return MessageFeatures_t#Length of the message (Total, including the header)
        int
    Length_Get(void)
    {
            MessageFeatures_t MsgType;
            static_assert( sizeof(MsgType) <= sizeof(mBuffer[1]), "!");
            memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
            return MsgType.Length;
    }
        /*!
         * \brief Get from the 'memory read' message the requested reply length MessageFeatures_t#ReplyLength
         * \return
         */
        int
    ReplyLength_Get(void)
    {
        MessageFeatures_t MsgType;
        static_assert( sizeof(MsgType) <= sizeof(mBuffer[1]), "!");
        memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
        return MsgType.ReplyLength;
    }
        //! Return the ClusterAddress_t address of the destination in the message
        ClusterAddress_t
    DestinationAddress_Get(void)
    {
        ClusterAddress_t CA;
        memcpy(&CA, &mBuffer[0], sizeof(CA));
        return CA;
    }
        //! Set the destination address to ClusterAddress_t address CA
        void
    DestinationAddress_Set(ClusterAddress_t CA)
    {
        memcpy(&mBuffer[0], &CA, sizeof(CA));
        return;
    }
        //! Set the ReplyTo address to ClusterAddress_t address CA
        void
    ReplyToAddress_Set(ClusterAddress_t CA)
    {
        memcpy(&mBuffer[2], &CA, sizeof(CA));
        return;
    }
        //! Return the ClusterAddress_t ReplyTo (or source) address
        ClusterAddress_t
    ReplyToAddress_Get(void)
    {
        ClusterAddress_t CA;
        memcpy(&CA, &mBuffer[2], sizeof(CA));
        return CA;
    }

        //! Set the ReplyTo (or source) address  to  ClusterAddress_t CA
        uint32_t
    Header3Address_Get(void)
    {
        uint32_t A;
        memcpy(&A, &mBuffer[3], sizeof(A));
        return A;
    }
        //! Set the Header3 address to ClusterAddress_t address CA
        void
    Header3Address_Set(uint32_t A)
    {
        memcpy(&mBuffer[3], &A, sizeof(A));
        return;
    }
        //! \brief Set the type of the message in the buffer to MessageFeatures_t#Type T
        void
    DelayTime_Set(int16_t T)
    {
        MessageFeatures_t MsgType;
        static_assert( sizeof(T) <= sizeof(mBuffer[1]), "!");
        memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
        MsgType.Time = T;
        memcpy(&mBuffer[1],  &MsgType, sizeof(mBuffer[1]));
    }
        //! \brief Return MessageFeatures_t#Length of the message (Total, including the header)
        int16_t
    DelayTime_Get(void)
    {
            MessageFeatures_t MsgType;
            static_assert( sizeof(MsgType) <= sizeof(mBuffer[1]), "!");
            memcpy( &MsgType ,&mBuffer[1], sizeof(mBuffer[1]));
            return MsgType.Time;
    }
private:
    int32_t mBuffer[4+MAX_IGPCBUFFER_SIZE];   ///< The buffer for transferring data

}; // of scIGPMessage

#endif // scIGPMessage_h
