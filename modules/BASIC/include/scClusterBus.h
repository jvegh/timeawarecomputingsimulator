/** @file scClusterBus.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The header of the bus arbiter
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  simple_bus.h : The bus.

		 The bus is derived from the following interfaces, and
	         contains the implementation of these: 
		 - blocking : burst_read/burst_write
		 - non-blocking : read/write/get_status
		 - direct : direct_read/direct_write
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 
#ifndef scClusterBus_h
#define scClusterBus_h

//#include <systemc.h>

#include "scGridPoint.h"
//#include "ClusterBusTypes.h"
//#include "ClusterBusRequest.h"
//#include "scClusterBusDirect_if.h"
//#include "scClusterBus_blocking_if.h"
#include "scClusterBus_non_blocking_if.h"
#include "scClusterBusArbiter_if.h"
#include "scClusterBusSlave_if.h"
//#include "scProcessor.h"
//#include "scIGPMessage.h"

class scProcessor; class ClusterBusRequest;
class scClusterBus
  : 
     public scClusterBusDirect_if
   , public scClusterBus_non_blocking_if
   , public scClusterBus_blocking_if
   , public sc_module
{
public:
  // ports
//  sc_in_clk clock;
  sc_port<scClusterBusArbiter_if> arbiter_port;
  sc_port<scClusterBusSlave_if, 0> slave_port;

  SC_HAS_PROCESS(scClusterBus);

  // constructor
  scClusterBus(sc_module_name name_, scProcessor* Proc
             , bool verbose = false)
    : sc_module(name_)
    , m_verbose(verbose)
    , m_current_request(0)
    , mProcessor(Proc)
  {
    // process declaration
/* ???    SC_METHOD(main_action);
    dont_initialize();
    sensitive << clock.neg();
*/
  }
  virtual ~scClusterBus(void){}
  // process
  void main_action();

  // direct BUS interface
/*  bool direct_read(int *data, unsigned int address);
  bool direct_write(int *data, unsigned int address);*/
  bool direct_read(scIGPMessage* M);
  bool direct_write(scIGPMessage* M);
  // non-blocking BUS interface//#include "scGridPoint.h"
  void read(unsigned int unique_priority
/*	    , int *data
        , unsigned int address*/
            , scIGPMessage* M
	    , bool lock = false);
  void write(unsigned int unique_priority
/*	     , int *data
         , unsigned int address*/
             , scIGPMessage* M
         , bool lock = false);
        ClusterBusStatus
    get_status(unsigned int unique_priority);

  // blocking BUS interface
        ClusterBusStatus
    burst_read(unsigned int unique_priority
/*			       , int *data
			       , unsigned int start_address
                   , unsigned int length = 1*/
                   , scIGPMessage* M
                   , bool lock = false);
        ClusterBusStatus
    burst_write(unsigned int unique_priority
            /*	, int *data
				, unsigned int start_address
                , unsigned int length = 1*/
                , scIGPMessage* M
                , bool lock = false);
        scClusterBusSlave_if* get_slave(scIGPMessage*);
        scGridPoint* get_slave_GP(scIGPMessage* M);

private:
  void handle_request();
//  void end_of_elaboration();
  //scClusterBusSlave_if * get_slave(unsigned int address);
  ClusterBusRequest * get_request(unsigned int priority);
  ClusterBusRequest * get_next_request();
  void clear_locks();
    scProcessor* Processor_Get(void){ return mProcessor;}
  bool m_verbose;
  ClusterBusRequest_vec m_requests;
  ClusterBusRequest *m_current_request;
  scProcessor *mProcessor;

}; // end class scClusterBus

#endif // scClusterBus_h
