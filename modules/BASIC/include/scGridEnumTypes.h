/** @file scGridEnumTypes.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef SCGRIDENUMTYPES_H
#define SCGRIDENUMTYPES_H

#include <systemc>
#include <bitset>
#include "GridPoint.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;

/*! \var typedef GridPointOperatingBit_t
 * \brief the names of the bits in the bitset for operating them
 */
typedef enum
{
    gob_General,
        gob_ObjectMonitored, //!< Set if the object is monitored at all at this time
        gob_ObjectChanged,  //!< Set if the monitored object changed between monitoring, any of the features below
        gob_ObjectActive, //!< Set if the object on monitored active (allocated of preallocated) at this time
        gob_ObjectWaiting, //!< Set if the object is waiting at this time
    gob_OperationMonitored, //!< If any of the operations monitored
        gob_FetchPending, //!< The core can only make one fetch at a time, for all threads
        gob_ExecPending, //!< The core can only make one execute at a time, for all threads
        gob_RegistersMonitored, //!< Register changes are monitored
        gob_MemoryMonitored, //!< Register changes are monitored
    gob_MorphingMonitored, //!< Metainstructions and consequences monitored
        gob_ResourceMonitored, //!< Resources utilization monitored
        gob_ObjecIsMeta, //!< Set if the object is in Meta stata at this time
    gob_ArchitectureMonitored,
        gob_PortsMonitored, //!< Set if port changes monitored
        gob_SignalsMonitored, //!< Set if signal changes are monitored
    gob_BuffersMonitored,   //!< Set if any of the different buffers are monitored
        gob_BufferChanged,  //!< Set if any of the object-related
    gob_CommunicationMonitored, //!< Set if inter-core and inter-cluster communication monitored
        gob_IGPBChanged,    //!< Set if the state of the inter-gridpoint communication block changed
        gob_IsSending,      //!< Set if the gridpoint is sending a message
        gob_IsReceiving,    ///!< Set if the gridpoint is receiving a message
    // The IGPCBs communicate independently and parallel!!˘
    gob_Max, //!< The max number of bits in the set
}  GridPointOperatingBit_t;



/*! \struct MetaEvent_Transfer_Type
 *
 * \brief  Instruction-execution related variables
 *
 * The AbstractCore transfers metaevent information between the AbstractProcessor
 * and AbstractCore when a meta-instruction is executed
 */
typedef struct
{
    ClusterAddress_t CA; ///< The cluster address of the physical core (includes HThread!)
    short int Code; ///< The code of the meta-event
    short Param; ///< A link register mask (a byte+1 flag bit)
    SC_ADDRESS_TYPE PC; ///< The program counter the event relates to
    SC_ADDRESS_TYPE Address; ///< The address involved in the operation
    sc_time ExecutionTime; ///< The simulation length of the operation
} MetaEvent_Transfer_Type;

/*!
 \struct GridPointSignalPort_t
 \brief Port declarations: these signals go out to the scProcessor
 */
typedef struct{
        sc_out<bool>
     Denied    ///< If this GridPoint is in denied, from any reason
    ,Allocated ///< Allocated for another GridPoint
    ,PreAllocated ///< Preallocated for another GridPoint
               // Just combined from the above signals
    ,Meta      ///< Executing a metainstruction (surely Allocated)
    ,Wait      ///< Blocked (surely Allocated, maybe Meta)
    ,Available ///< ! (Denied | Allocated | PreAllocated)
        // If a GridPoint has not duty, it sleeps in low power consumption mode
    ,Sleeping  ///< Denied | Wait | !(Allocated| PreAllocated)
     ;
} GridPointSignalPort_t; typedef GridPointSignalPort_t *GridPointSignalPort_Ptr;

/*!
 \struct GridPointSignal_t
 \brief The signals issued by the scGridPoint
 */
typedef struct{
        sc_signal <bool>
     Denied    ///< Whether this scGridPoint is denied, from any reason
               ///< If the scGridPoint is denied, the next two other flags below are reset
    ,Allocated ///< Whether this scGridPoint is allocated by another one
    ,PreAllocated ///< Whether this scGridPoint is preallocated by another one
               ///< The scGridPoint is active:
    ,Meta      ///< Executing a metainstruction (surely Allocated)
    ,Wait      ///< The scGridPoint is blocked (surely Allocated, maybe Meta)
               ///< Just combined from the above signals
    ,Available  ///< Just combined from the above signals
                ///< ! (Denied | Allocated | PreAllocated)
                ///< If an scGridPoint has not duty, it sleeps in low power consumption mode
    ,Sleeping   ///< Denied | Wait | !(Allocated| PreAllocated)
    ;
} GridPointSignal_t; typedef GridPointSignal_t *GridPointSignal_Ptr;

/*!
 \struct GridPointStatusBit_t
 \brief The values are stored here, and set through writing into GridPointStatusBitSignal_t
 */
typedef struct{
        bool
    Denied ///< Whether this scGridPoint is denied, from any reason
           // If the GridPoint is denied, all the other flags below are reset
           // If denied, for all HThreads denied
   ,Allocated ///< Whether this GridPoint is allocated by another one
                // Allocated when all
   ,PreAllocated ///< Whether this scGridPoint is preallocated by another one
           // If neither allocated nor preallocated, it is in the pool
           // The GridPoint is active:
   ,Meta ///< Executing a metainstruction (surely Allocated)
   ,Wait ///< The GridPoint is blocked (surely Allocated, maybe Meta)
        // Just combined from the above signals
   ,Available  ///< !(Denied | Allocated | PreAllocated)
        // If a GridPoint has not duty, it sleeps in low power consumption mode
   ,Sleeping   ///< (Denied | Wait | !(Allocated| PreAllocated))
   ;
} GridPointStatusBit_t; typedef GridPointStatusBit_t *GridPointStatusBit_Ptr;

/*!
 \struct HThreadStatusBit_t
 \brief The values are stored here, and set through writing into HThreadStatusBit_t
 */
typedef struct{
        bool
   Activated ///< Whether this scHThread is activated
                // Allocated when all
   ,PreActivated ///< Whether this scGridPoint is preactivated
           // If neither allocated nor preallocated, it is in the pool
           // The GridPoint is active:
   ,Meta ///< Executing a metainstruction (surely Activated)
   ,Blocked ///< The scHThread is blocked (surely Activated, maybe Meta)
        // Just combined from the above signals
   ,Available  ///< !(Owner->Denied | Allocated | PreAllocated)
        // If a GridPoint has not duty, it sleeps in low power consumption mode
   ,Sleeping   ///< (Owner->Denied | Blocked | !(Activated| PreActivated))
   ;
} HThreadStatusBit_t; typedef HThreadStatusBit_t *HThreadStatusBit_Ptr;

/*!
 \struct GridPointMask_t
 \brief The set of scGridPoint objects can be defined by by a mask word, the bits corresponding to single modules
 */
typedef struct{
        SC_GRIDPOINT_MASK_TYPE
     ID      /// One-hot bitmask corresponding to mID of the scGridPoint
    ,Children       /// (immediate) Children of this scGridPoint
    ,PreAllocated   /// These GridPoints are preallocated for this scGridPoint
    ,Wait           /// Waiting for these scGridPoints
        // Just combined from the above signals
//   ,Available  /// ~(Denied | Allocated | PreAllocated)
        // If a GridPoint has not duty, it sleeps in low power consumption mode
   ,Sleeping   /// Denied | Wait | !(Allocated| PreAllocated)
        //
   ,Backlink;    /// Stores the mask of registers to backlink
    std::bitset<MAX_HTHREADS>
AllocatedHThreads;                ///< The active HThreads

} GridPointMask_t; typedef GridPointMask_t *GridPointMask_Ptr;

/// Codes of mass processing operating modes
typedef enum { ccm_None, ccm_FOR=1, ccm_WHILE=2, ccm_SUMUP=5} core_cooperationmode_t;

#endif // SCGRIDENUMTYPES_H
