/** @file scSimulator.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scqSimulator_h
#define scqSimulator_h
#include <systemc.h>
#include <map> // For the aliases
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>
#include <QDate>
#include <QTime>
#include <QSettings>
#include <QCoreApplication>
#include <QTextEdit>    // Not used in CLI. but must be present
#include <QTreeWidget>

class scProcessor;
class scGridPoint;
class scHThread;
using namespace sc_core; using namespace std;

//Submodule forward class declarations
/*!
 * \brief The SystemDirectories contains names of some system-wide subdirectories
 */
struct SystemDirectories
{
    string Home;    // The nominal home directory of user
    string Config;  // Where the config directory is
    string Work;    // Where the work files go
    string Temp;    // For temporary files
    string Install; // Where the system is installed
    string SystemData;    // for important data
    string SystemDocs;    // for documentation
    string UserData; // User-provided data
    string UserOutput; // User-provided data
    string LogFile; // The log messages
    bool SystemDataFound;   // If system data are available
    bool SystemDocsFound;   // If system docs are available
};

SystemDirectories* SystemDirectories_Get(void);

/*!
 * @class scqSimulator
 * @brief The anchestor of all ScQt-based simulators
 * The simulator manipulates scModules etc.
 * It does NOT use a bus.
 */

class scqSimulator : public sc_core::sc_module {
    //Port declarations
    // channels
   public:
    // Constructor declaration:
      SC_HAS_PROCESS(scqSimulator); // Contructor implemented in .cpp
      static QTextEdit *s_LogWindow;  // This will display log file, if any
      QTreeWidget*
  HierarchyWidget;  // Show hierarchy of modules here
      scqSimulator(sc_core::sc_module_name nm,int argc, char* argv[]);
      virtual
    ~scqSimulator();
/*        string
      PrologText_Get(void);

    void Setup(void);  // Belongs to the constructor
    void writeSettings(void); // Write settings to the project directory
*/

        void
    SetupSystemDirectories(QWidget* parent);
        void
    SetupLogging(QWidget* parent);
        void
    SetupSettings(void);
/*      void
    SInitialize_method(void); ///< This initializes the simulator
      bool
    StepwiseMode_Get(void){ return msStepwiseMode;}
      void
    StepwiseMode_Set(bool b){msStepwiseMode = b;}
      void
     SSTART_thread(void);
      void
    SHALT_thread(void);
        struct{
      sc_core::sc_event
    START,
    STOP;
        }EVENT_SIMULATOR;
        //virtual
    */
        scProcessor*
    Processor_Get(){ return msProcessor;}
        /*!
         * \brief readSettings
         *  The main settings reading routine:
         *   reads settings  from files @verbatim<keywordgroup>.ini @endverbatim at levels Application, User, Project,
         *  in this sequence (i.e. a later setting overwrites the previous one)
         *
         * The list of @verbatim<keywordgroup>.ini @endverbatim files is given as a configuration parameter,
         * as @verbatim #define INI_FILES "MainWindow Others" @endverbatim
         *
         * The settings in the files are given as @verbatim<keywordgroup>/<keyword>=<value>@endverbatim
         * The assumed directory structure:
         *
         * @verbatim
         *  |InstallDir
         *  |--/.config/<packagename>V<packageversion>/<keywordgroup>.ini
         *
         *  |WorkDir
         *  |--/<packagename>V<packageversion>/<keywordgroup>.ini
         *
         *  |ProjectDir
         *  |--/<packagename>V<packageversion>/<keywordgroup>.ini
          * @endverbatim
         *
         * If the expected directory, the @verbatim<keywordgroup>.ini@endverbatim file, or that @verbatim<keyword>@endverbatim does not exist,
         * a warning is issued in the log file. Depending on the actual directory status,
         * duplicated readings are possible.
         *
         * The different packages may have different settings handling method
         */
        virtual void
    readSettings(void);
        /*
         * \brief writeSettings
         * Write the !Changed! settings back at project level
         */
        virtual void
    writeSettings(void);
        string
    SimulationName_Get(void) {return    simulation_name;}
        int64_t
    SimulationPartTime_Get(void);    ///< return actual benchmark duration since last reading
        int64_t
    SimulationSumTime_Get(void);    //6< return actual benchmark duration since last reset
        bool
    IsKeywordInList(const QString Keyword, QStringList KeywordList);
        string
    PrologString_Get(void);

  protected:


    /*
       void
    PrintFinalReport(scProcessor *Proc);
    */
        scProcessor* /// My topology, I am handling
    msProcessor;
/*        void
    HandleSpecials(void); /// Handle the exceptional points
       vector<scGridPoint*>
    mSpecials;   /// Stores the special points
*/      bool
    msStepwiseMode,   ///< If to process stepwise
    msSuspended;      ///< If running simulation is suspended
    static SystemDirectories Directories;
    string mSettingsFileName;   /// The filename of the config files
    string simulation_name;
    QScopedPointer<QFile>   m_logFile;
    chrono::steady_clock::time_point
    m_running_time; // A work variable, do not touch outside the macros
        std::chrono::duration< int64_t, nano>
    m_part_time, m_sum_time;    // Work variables, contain actual benchmark duration and their sum
}; // of scqSimulator

#endif // scqSimulator_h
