/** @file scClusterBusDirect_if.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The inter-cluster bus direct access header
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */


/*****************************************************************************
 
  scClusterBusDirect_if.h : The direct BUS/Slave interface.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/

#ifndef scClusterBus_direct_if_h
#define scClusterBus_direct_if_h

//#include <systemc.h>
class scIGPMessage;
class scClusterBusDirect_if
  : public virtual sc_interface
{
public:
  // direct BUS/Slave interface
/*  virtual bool direct_read(int *data, unsigned int address) = 0;
  virtual bool direct_write(int *data, unsigned int address) = 0;*/
  virtual bool direct_read(scIGPMessage *M) = 0;
  virtual bool direct_write(scIGPMessage *M) = 0;

}; // end class scClusterBusDirect_if

#endif // scClusterBus_direct_if_h
