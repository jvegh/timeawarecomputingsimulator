/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @fileMemoryClustering.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Memory clustering information for the electronic modules arranged in a grid
 */

#ifndef MEMORYCLUSTERING_H
#define MEMORYCLUSTERING_H
#include "Clustering.h"
using namespace std;

typedef enum {    
    memtype_0,  //!< The 'register' type memory
    memtype_1,  //!< The 'cache type' or 'near' memory
    memtype_2,  //!< The 'conventional', major, or 'far' memory
    memtype_3   //!< The 'farthest' memory, memory 'buffers' */
} Memory_t;

//! The memories are modules on the scClusterbus, having a special address
//!
//! They formally have ClusterAddress_t address, but have different functionality
//! The memories have their type code in the address
static const ClusterAddress_t MemoryClusterAddress
{
    MAX_HTHREADS_LIMIT, //HThread
    7, //Member
    7, //Proxy
    MAX_CLUSTERS_LIMIT, //Cluster
    MAX_TOPOLOGIES_LIMIT, //Topology
    MAX_CARDS_LIMIT, //Card
    MAX_RACKS_LIMIT, //Rack
    3, //Status
    3 //Reserved
}; //!< This is used to find out if some memory is addressee


static const ClusterAddress_t Memory0ClusterAddress
{
    MAX_HTHREADS_LIMIT, //HThread
    memtype_0, //Member
    7, //Proxy
    MAX_CLUSTERS_LIMIT, //Cluster
    MAX_TOPOLOGIES_LIMIT, //Topology
    MAX_CARDS_LIMIT, //Card
    MAX_RACKS_LIMIT, //Rack
    3, //Status
    3 //Reserved
}; ///< This is used to find out if default argument is provided

static const ClusterAddress_t Memory1ClusterAddress
{
    MAX_HTHREADS_LIMIT, //HThread
    memtype_1, //Member
    7, //Proxy
    MAX_CLUSTERS_LIMIT, //Cluster
    MAX_TOPOLOGIES_LIMIT, //Topology
    MAX_CARDS_LIMIT, //Card
    MAX_RACKS_LIMIT, //Rack
    3, //Status
    3 //Reserved
}; ///< This is used to find out if default argument is provided

static const ClusterAddress_t Memory2ClusterAddress
{
    MAX_HTHREADS_LIMIT, //HThread
    memtype_2, //Member
    7, //Proxy
    MAX_CLUSTERS_LIMIT, //Cluster
    MAX_TOPOLOGIES_LIMIT, //Topology
    MAX_CARDS_LIMIT, //Card
    MAX_RACKS_LIMIT, //Rack
    3, //Status
    3 //Reserved
}; ///< This is used to find out if default argument is provided

static const ClusterAddress_t Memory3ClusterAddress
{
    MAX_HTHREADS_LIMIT, //HThread
    memtype_3, //Member
    7, //Proxy
    MAX_CLUSTERS_LIMIT, //Cluster
    MAX_TOPOLOGIES_LIMIT, //Topology
    MAX_CARDS_LIMIT, //Card
    MAX_RACKS_LIMIT, //Rack
    3, //Status
    3 //Reserved
}; ///< This is used to find out if default argument is provided


#endif // MEMORYCLUSTERING_H
