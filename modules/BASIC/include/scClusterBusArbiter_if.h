/** @file scClusterBusArbiter_if.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief the interface of the
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
  simple_bus_arbiter_if.h : The arbiter interface.
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
*/
 

#ifndef __cluster_bus_arbiter_if_h
#define __cluster_bus_arbiter_if_h

//#include <systemc.h>

#include "ClusterBusTypes.h"


class scClusterBusArbiter_if
  : public virtual sc_interface
{
public:
  virtual ClusterBusRequest *
    arbitrate(const ClusterBusRequest_vec &requests) = 0;

}; // end class ClusterBusArbiter_if

#endif //__cluster_bus_arbiter_if_h
