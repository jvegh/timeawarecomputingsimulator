/** @file ClusterBusTypes.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The description of the inter-cluster states
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
  simple_bus_types.h : The common types.
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
*/
 

#ifndef ClusterBusTypes_h
#define ClusterBusTypes_h

#include <stdio.h>
#include <systemc.h>

enum ClusterBusStatus { CLUSTER_BUS_OK = 0
			 , CLUSTER_BUS_REQUEST
			 , CLUSTER_BUS_WAIT
			 , CLUSTER_BUS_ERROR };

// needed for more readable debug output
extern char ClusterBusStatus_str[4][20];

struct ClusterBusRequest;
typedef std::vector<ClusterBusRequest *> ClusterBusRequest_vec;

extern int sb_fprintf(FILE *, const char *, ...);

#endif //ClusterBusTypes_h
