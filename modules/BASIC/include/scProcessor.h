/** @file scProcessor.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *  It adds sc facilities
 *  -- gridpoint to gridpoint communication
 *  -- basics of cooperation
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef SCPROCESSOR_H
#define SCPROCESSOR_H
#include "Utils.h"
#include <map>
#include <QString>
#include "AbstractTopology.h"

#include "scClusterBus.h" //#include "scGridPoint.h"
//!! #include "scClusterBusMemorySlow.h"
//#include "scBenchmarkingHead.h" // Contains conditional compiles

//#include "scClusterBusArbiter.h"
 class scClusterBusMemoryFast;
 class scClusterBusMemorySlow;
 class scClusterBusArbiter;
  extern scClusterBusMemorySlow *MainMemory;

typedef struct{
        sc_in< sc_dt::sc_uint<MAX_GRIDPOINTS> >
     Denied
    ,Allocated
    ,PreAllocated
    ,Available
    ;
} TopologySignalPort_t, *TopologySignalPort_Ptr;

typedef struct{
        sc_signal<sc_dt::sc_uint<MAX_GRIDPOINTS>>
     Denied
    ,Allocated
    ,PreAllocated
    ,Available
        ;
} TopologySignal_t, *TopologySignal_Ptr;

// The relation of the gridpoints to the topology
typedef struct{
        SC_GRIDPOINT_MASK_TYPE
     Denied             /// These GridPoints of the processor  denied
    ,Allocated          /// These GridPoints of the processor are allocated by another one
    ,PreAllocated       /// These GridPoints of the processor are preallocated by another one
    ,Available          /// These GridPoints are available for work
        // This one is not a signal, just stored with the signal masks
    ,PreAllocatedForTopology /// These GridPoints are preallocated for the processor
    ;
} TopologyMask_t, *TopologyMask_Ptr;

/*! \var typedef  CorePreference_t
 * These preference modes can be selected when asking for a new core
 */
typedef enum { cpt_Head, cpt_Member, cpt_Neighbor, cpt_AnyCore} CorePreference_t;
// These preference modes can be selected when asking for a new core
/*! \var typedef  ThreadPreference_t
 * These preference modes can be selected when asking for a new thread
 */
typedef enum {
                tpt_Allocated,    //!< In a core allocated for the owner of the HThread
                tpt_Preallocated, //!< In a core preallocated for the  owner of the HThread
                tpt_Core,    //!< In the same core as the owner of the HThread
                tpt_Neighbor, //!< In a direct accessible core
                tpt_Another, ///< Surely in another core
                tpt_AnyThread} ThreadPreference_t;

typedef pair<string,scGridPoint*> GridModuleItem;

using namespace std;
// The modules can be the head of the cluster, a member of a cluster, or neither (stand-alone)
// These are the standard offsets of the neighbors in the order of
// the hexagonal Cluster Grid:  Head, N, NE, SE, S, SW, NW

// Here a two-dimensional topology is assumed
/*!
 * \class scProcessor
 * \brief This is a  processor class comprising communicating scGridPoint classes.
 * Comprises and handles the scGridPoint modules in the grid points.
 * This implements the gridpoint-to-gridpoint communication:
 * The grid comprises cluster heads and members, and enjoys the advantages of the
 * topological proximity.
 *
 * The scProcessor class receives the signals from the scGridPoint
 * modules through its input ports.
 * The module stores the actual values of those signals in the corresponding SC_GRIDPOINT_MASK_TYPE,
 * and receives signals TopologySignal_t through the TopologySignalPort_t array
 *
 *   \brief Comprises scGridPoint modules, but no real calculational functionality
 *   \param nm The SystemC name
 *   \param Specials some special scGridPoints (enables to handle heterogenous topologies)
 *   \param StandAlone true if some part shall be executed here otherwise in the subclass
 *  
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |----/AbstractProcessor
 *  |....../XXProcessor (Y86, RISCV)
 *  |....../NeurerProcessor (Y86, RISCV)
 *  @endverbatim
 */

 /*!
  *
  * \brief Implements a module-name alias facility (i.e., user-provided names.
  * Given that the basic units is scHThread class,
  * the programmer has three ways to refers to a scHThread.
  * (the scHThread is owned by an scGridPoint, so a hthread name
  * is given as [Owner ID]/[HThread ID]
  * The default one is just the sequence number or SystemC module name
  *
  * @see     scHThread#StringOfDefaultName_Get
  * @see     scHThread#StringOfClusterName_Get
  * @see     scHThread#StringOfAliasName_Get
  * @see     scHThread#StringOfName_Get
  *
  * The internal array scProcessor#mAliases stores all possible names,
  * so  scHThread#StringOfName_Get with any form of name will return
  * a pointer to the same scHThread.
  * (it is the user's responsibility to NOT select system-conventional names)
  */

class scProcessor : public AbstractTopology, public sc_core::sc_module
{
    //Port declarations
        TopologySignalPort_t
    msSignalPort[MAX_HTHREADS];
        TopologySignal_t
    msSignal[MAX_HTHREADS];
  public:
      /// The of processor inter-cluster bus-related signals
//        sc_in_clk
//    clock; ///< This clock drives the bus
        scClusterBus*
    mClusterBus;    ///< The inter-cluster bus    
        scClusterBusMemoryFast* 
    mClusterMemoryFast; ///< A register-like memory
        scClusterBusArbiter*
    mClusterBusArbiter; ///< Arbiter of the inter-cluster bus
        scClusterBus*
    ClusterBus_Get(void){   return mClusterBus;}
    scProcessor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone);
    SC_HAS_PROCESS(scProcessor); // Will be defined in separate file
    ~scProcessor(void);
    void RefreshGUI(scGridPoint & G);
        void
    Reset(void);
//        void
  //  CheckIGPCBs();
        scGridPoint*
    ClusterHead_Get(int i)
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ClusterHead_Get(i)); }
     scGridPoint *ByPointer_Get(scGridPoint *GP) ///< A kind of self-check: dynamic_cast will return NULL if wrong
        {return dynamic_cast<scGridPoint *>(GP);}

    scGridPoint* ByIndex_Get(const int X, const int Y) /// Get an scGridPoint by its indices
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ByIndex_Get(X, Y));}
    scGridPoint* ByPosition_Get(const int X, const int Y)
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ByPosition_Get(X, Y));}
    scGridPoint* ByClusterAddress_Get(ClusterAddress_t CA)
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ByClusterAddress_Get(CA));}
    scGridPoint* ClusterHeadOfMember_Get(scGridPoint* GP)
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ClusterHeadOfMember_Get(GP));}
    scGridPoint* ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM= cm_Head)
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ByClusterMember_Get(CN,CM));}
    scGridPoint* ByID_Get(int N) // Get a gridpoint by its ID
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ByID_Get(N));}
     scGridPoint* ByIDMask_Get(SC_GRIDPOINT_MASK_TYPE Mask)
     { return ByID_Get(MaskToID(Mask));}
    scGridPoint* ByClusterPointer_Get(scGridPoint* GP, ClusterNeighbor N)
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ByClusterPointer_Get(GP,N));}
    // Simple accessor functions, PC related stuff
        scHThread*
    ByName_Get(string N);
    /*! Find a HThread by its string name, any kind */

 /*       SC_GRIDPOINT_MASK_TYPE
    ChildrenMask_Get(SC_HTHREAD_ID_TYPE H=0)
    { return msChildrenMask[H];}///< Return which cores are allocated for us
        void
    ChildrenMask_Set(SC_GRIDPOINT_MASK_TYPE M, SC_HTHREAD_ID_TYPE H=0)
    {	msChildrenMask[H] = M;}///< Set mask of our children
        void
    ChildrenMaskBit_Set(scGridPoint* C);  ///< Set the mask bit of the allocated cores
        void
    ChildrenMaskBit_Clear(scGridPoint* C);  ///< Clear the mask bit of the allocated cores
*/    void Populate(vector<scGridPoint*>& Specials);
    void ConnectIGPCBs(void);
    void ConnectClusterHeadsToBus(scClusterBus* Bus);
        SC_GRIDPOINT_MASK_TYPE
    DeniedMask_Get(SC_HTHREAD_ID_TYPE H=0) {  return msSignalMask[H].Denied;}
         void
    DeniedMaskBit_Set(scGridPoint* GP, bool P=true);
         SC_GRIDPOINT_MASK_TYPE
    AllocatedMask_Get(SC_HTHREAD_ID_TYPE H=0) {  return msSignalMask[H].Allocated;}
         void
    AllocatedMask_Set(SC_GRIDPOINT_MASK_TYPE M, SC_HTHREAD_ID_TYPE H=0) {msSignalMask[H].Allocated = M;}
          void
    AllocatedMaskBit_Set(scGridPoint* GP, bool P=true);
          SC_GRIDPOINT_MASK_TYPE
    PreAllocatedMask_Get(SC_HTHREAD_ID_TYPE H=0) {  return msSignalMask[H].PreAllocated;}
         void
    PreAllocatedMask_Set(SC_GRIDPOINT_MASK_TYPE M, SC_HTHREAD_ID_TYPE H=0) { msSignalMask[H].PreAllocated = M;}
           void
    PreAllocatedMaskBit_Set(scGridPoint* GP, bool P=true);
        SC_GRIDPOINT_MASK_TYPE
    AvailableMask_Get(SC_HTHREAD_ID_TYPE H=0)
       {  return ~UnavailableMask_Get(H);}
        SC_GRIDPOINT_MASK_TYPE
    UnavailableMask_Get(SC_HTHREAD_ID_TYPE H=0);
        string
    StringOfMessage_Get(scIGPMessage* M);
         string
    StringOfRegisters_Get(scIGPMessage* M);
         string
    StringOfCooperation_Get(scIGPMessage* M);
         string
    StringOfID_Get(scHThread* H);
         string
    StringOfNeural_Get(scIGPMessage* M);
        string
    StringOfMemory_Get(scIGPMessage* M);
        string
    StringOfSender_Get(scIGPMessage* M);
        string
    StringOfReceiver_Get(scIGPMessage* M);
        string
    StringOfTime_Get(void){ return sc_time_to_nsec_Get();}
        string ///< A generic register name
    RegisterName_Get(int Index);
        virtual scGridPoint*
    ChildAllocateFor(scGridPoint* Parent, CorePreference_t Pref = cpt_AnyCore); ///< Allocate a core with preference
        virtual scHThread*
    HThreadAllocateFor(scHThread* Parent, HThreadPreference_t Pref = hpt_Any);
        scGridPoint*
    ChildPreAllocateFor(scGridPoint* Parent, CorePreference_t Pref = cpt_AnyCore); ///< Allocate a core with preference
        scGridPoint*
    ChildFindFor(scGridPoint* Parent, CorePreference_t CPT); ///< find an scGridPoint for this parent

        void
    Child_Insert(scHThread* C);
        void
    Child_Remove(scHThread* C);
        int
    ChildCount_Get(void){return mChildren.size();}
        int
    Child_Find(scHThread* C);

        /*!
           * \brief scProcessor::HThreadFindFor
           * \param[in] Parent the potential parent
           * \param[in] HPT the preferred HThread (clustered) type
           * \return the requested HThread that can be allocated or NULL if not found
           *
           * The core preferences can be: hpt_Head, hpt_GridPoint, hpt_Cluster, hpt_Any
           * 1./ a thread from a cluster head (the most limited resource)
           * 2./ a thread from the same scGridPoint (ordinary members first)
           * 3./ a thread from the cluster where the scGridPoint is (ordinary members first)
           * 4./ any available HThread
           *
           * In the simplest form, get the next available core,
           * with the lowest sequence number. Can be replaced by considering temperature,
           * utilization, etc
           *
           */
        scHThread*
    HThreadFindFor(scHThread* Parent, HThreadPreference_t HPT); ///< find an scHThread for this parent
        /*!
         * \brief scGridPoint::doQCREATE
         * Create a new QT for TheParent, allocate a core if TheChild is NULL
         *
         * \param[in] TheParent that needs a new child
         * \param[in] TheChild if NULL will be sought, else the scGrigPoint is already allocated
         * \param[in] CloneMask the mask describing the cloned register contents
         * \param[in] BackLinkMask the mask describing the expected backlinked register contents
         * \param[in] CPT Which type of core is preferred
         * \return[in] the core of the created QT, maybe TheChild
         */
        virtual scGridPoint*
    doQCREATE(scGridPoint* TheParent, scGridPoint* TheChild,
                SC_GRIDPOINT_MASK_TYPE CloneMask,
                SC_GRIDPOINT_MASK_TYPE BackLinkMask,
                CorePreference_t CPT);
        virtual scHThread*
    doReboot(SC_ADDRESS_TYPE A){ return (scHThread*)NULL;}

//        scHThread*
//    ByAliasName_Get(string N);
        string
    StringOfClusterAddress_Get(scGridPoint* GP)
    {
        assert(GP);
        return GP->StringOfClusterAddress_Get();
    }
    struct{
            sc_core::sc_event
        START,
        STOP,
        SUSPEND,
        RESUME,
        HALT,
        QTERM,
        MasksChanged;   /// The status of one of the bits in the gridpoints has changed
    }EVENT_PROCESSOR;
        TopologyMask_t
     msSignalMask[MAX_HTHREADS];
        string 
    PrologString_Get(void);
        scProcessor* Processor_Get(void)
    { return this;}
         SC_GRIDPOINT_MASK_TYPE
    PreAllocatedForProcMask_Get(SC_HTHREAD_ID_TYPE H=0){ return msMask[H].PreAllocatedForTopology;}
    // Simple accessor functions, PC related stuff
         void
    PreAllocatedForProcMaskBit_Set(scGridPoint* C, bool V);
        bool
    IsSuspended(void){ return msSuspended;}
        void
    SuspendedBit_Set(bool b){msSuspended = b;}
        bool
    Halted_Get(void) { return msHalted;}
        void
    Halted_Set(bool B){ msHalted = B;}
//        uint32_t // Just for testing
//    DWord_Get(SC_ADDRESS_TYPE Address){ return MainMemory->DWord_Get(Address);}

        void
    Initialize_method(void);
        virtual void
    Reboot(void);
        int16_t
    InstanceCount_Get(void) {return mInstanceCount % 50;}
        string  // Return the string ID of the thread
    StringID_Get(scHThread* H);
        scHThread*
    HThreadByClusterAddress_Get(ClusterAddress_t CA)
        { return ByClusterAddress_Get(CA)->HThread_Get(CA.HThread);}
    /*! Add an alias name A for scHThread HT */
        bool
   Alias_Add(string A, scHThread* HT)
          { mAliases[A] = HT;
          return false;}
    /*! Set a new alias name A for scHThread HT */
        bool
    StringOfAliasName_Set(string A, scHThread *H);
        /*!
        * \brief HandleAliases()
        * Handle the special case when aliases are to be read from a file
        */
           void
       HandleAliases(string FileName);
       /*!
        * \brief ReadTheThreeLevels
        *
        * The group Neurons needs special handling: 'Neurons.ini' contains names of NeuroxX.ini files
        * \param MyGroup the name of the group (.INI file)
        */
           virtual void
       ReadTheThreeLevels(QString MyGroup);
       /*!
        * \brief HandleAliasesPass
        * In the first pass, only adds the alias name to the known names.
        * In the second pass, initializes the aliased objects
        *
        * \param FileName name of file where aliases are listed
        * \param Pass2 if processing the second phase of handling
        */
           void
       HandleAliasesPass(string FileName, bool Pass2);
           void AliasesClear(void)
           {mAliases.clear();}
  protected:
        void
    SignalMaskBit_Set(SC_GRIDPOINT_MASK_TYPE &Signal, SC_GRIDPOINT_MASK_TYPE GPMask,
                      bool State, string S);
        void
    START_thread(void);
        void
    HALT_thread(void);
        void
    SUSPEND_thread(void);
        void
    RESUME_thread(void);
         void
    SetMasks_method(void);
         void
    QTERM_method(void);
         std::map<string,scHThread*>
     mAliases;      /// Stores alias names of the gridpoints
        TopologyMask_t
    msMask[MAX_HTHREADS];
//        SC_GRIDPOINT_MASK_TYPE
//    msChildrenMask[MAX_HTHREADS];
        vector<scHThread*>
    mChildren;  // HThreads are allocated for directly to processor
        bool
    msSuspended;
        bool
    msHalted;  
        uint16_t
    mInstanceCount; // How many time was instantiated immediate QT
};// of class scProcessor

#endif // SCPROCESSOR_H
