// The configured options and settings for TAC/BASIC SystemC developments
// Will be used to configure modules/BASIC/include/BasicConfig.h
/** @file BasicConfig.h
 *  @brief Basic configuration information for the BASIC package
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "HWConfig.h"


// Define settings (.ini) files as a string list
#define INI_FILES "Simulator"

// List keywords legal in 'Simulator.ini'
#undef  INI_SIMULATOR_KEYWORDS
#define INI_SIMULATOR_KEYWORDS "Version"

// Not using clock explicitly makes simulation faster and more simple
//#define MUST_USE_CLOCK_SIGNAL


#define EXECUTION_TIME sc_core::sc_time(10,SC_NS)
// Define memory features
// We may have 'register' memory, type 0
#define RMEMORY_ADDRESS_WIDTH 4
// The maximum size of the simulated memory; must be 2**N
#define RMAX_MEMORY_SIZE (1 << RMEMORY_ADDRESS_WIDTH)

// We may have 'dynamic' memory, type 1
#define DMEMORY_ADDRESS_WIDTH 10
// The maximum size of the simulated memory; must be 2**N
#define DMAX_MEMORY_SIZE (1 << DMEMORY_ADDRESS_WIDTH)
#define DMEMORY_READ_TIME sc_core::sc_time(10,SC_NS)

// We may have 'far' memory, type 2
#define FMEMORY_ADDRESS_WIDTH 16
// The maximum size of the simulated memory; must be 2**N
#define FMAX_MEMORY_SIZE (1 << FMEMORY_ADDRESS_WIDTH)
#define FMEMORY_READ_TIME sc_core::sc_time(60,SC_NS)

// We may have 'buffer' memory, type 3
#define BMEMORY_ADDRESS_WIDTH 10
// The maximum size of the simulated memory; must be 2**N
#define BMAX_MEMORY_SIZE (1 << BMEMORY_ADDRESS_WIDTH)


// Operational characteristics

// Resolution of time  and clock frequency
#define SCTIME_RESOLUTION 10,sc_core::SC_PS
// Gates operating time
#define SCTIME_GATE sc_time(20,sc_core::SC_PS)
// Frequency of the system clock
#define SCTIME_CLOCKTIME sc_time(100,sc_core::SC_PS)


