/** @file scClusterBusMemorySlow.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The fast ("Register") type memory
 *
 * This can be the basis for the "memory only" processing mode.
 * It uses direct access, i.e. as fast as registers, and can be used
 * in a way similar to that of "registers"; provided that the address
 * is short
 *
 * The result of reading wil be avaialable in nr_wait_states later
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
/*!
  The origin of this development file is available as SystemC example
  simple_bus_slow_mem.h : Slave : The memory (slave) with wait states.

  This is the FMemory (code 02): the "far" memory, corresponding to
  the conventional memory, except that it works asynchronously:
  it releases the bus as soon as the request received and
  at the end of the wait period (corresponding to the memory access time)
  becomes a master and sends the reply to the requester.
  (not yet implemented)
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 ****************************************************************/

#ifndef scClusterBusMemorySlow_h
#define scClusterBusMemorySlow_h

//#include <systemc.h>
//#include <string>
#include "Clustering.h"
class scGridPoint; class scProcessor;

#include "scClusterBusSlave_if.h" //#include "ClusterBusTypes.h"
#include "scClusterBusMaster_blocking.h"    // It can also be a master

//#include "scIGPMessage.h"

typedef struct {
    sc_core::sc_time ReplyTime;
    scIGPMessage* ReplyMessage;
} scMemoryMessageFIFO_t;

class scMemoryMessagefifowrite_if : virtual public sc_interface
{
   public:
     virtual void write(scMemoryMessageFIFO_t*) = 0;
     virtual void reset() = 0;
};

class scMemoryMessagefiforead_if : virtual public sc_interface
{
   public:
     virtual void read(scMemoryMessageFIFO_t*&) = 0;
     virtual int num_available() = 0;
};

// This structure keeps track where the line containing the code at Address is
typedef struct {unsigned int Address; int Lineno; std::string Line;} AddressToLineRecord;


/*!
 * \class scMemoryMessageFIFO
 *
 * The messages received by the memory are queued, and processed after
 * the latency time of the memory
 *
 */
class scMemoryMessageFIFO : public sc_channel, public scMemoryMessagefifowrite_if, public scMemoryMessagefiforead_if
{
    friend class scGridPoint;
    friend class scClusterBusMemorySlow;
  public:
     scMemoryMessageFIFO(sc_module_name name) : sc_channel(name), num_elements(0), first(0) {}

     void write(scMemoryMessageFIFO_t* c) {
       if (num_elements == max)
         wait(scMemoryMessageFIFO_read_event);

       data[(first + num_elements) % max] = c;
       ++ num_elements;
//       wait(SCTIME_CLOCKTIME); // Avoid immediate notification
 //!!      scMemoryMessageFIFO_write_event.notify(SCTIME_CLOCKTIME);
     }

     void read(scMemoryMessageFIFO_t* &c){
       if (num_elements == 0)
         wait(scMemoryMessageFIFO_write_event);

       c = data[first];
       -- num_elements;
       first = (first + 1) % max;
//       wait(SCTIME_CLOCKTIME); // Avoid immediate notification
  //!!     scMemoryMessageFIFO_read_event.notify(SCTIME_CLOCKTIME);
     }

     void reset() { num_elements = first = 0; }

     int num_available() { return num_elements;}

   private:
     enum e { max = 8 };
     scMemoryMessageFIFO_t* data[max];
     uint16_t num_elements, first;
     sc_event scMemoryMessageFIFO_write_event, scMemoryMessageFIFO_read_event;
};


/*!
 * \class scClusterBusMemorySlow
 *
 * \brief The 'far' memory of the processor
 *
 * The memory has multiple decoder ports (modeled here as the length of the queue
 * of the input requests
 * The input request is queued (if there is place), and the bur released.
 * When the time of reply arrives, the memory (as a master) sends the requested reply
 * to the requester core.
 */

class scClusterBusMemorySlow
  : public scClusterBusSlave_if
  , public sc_module
{
    friend class scProcessor;
public:
  // ports
/* ?? DO NOT USE CLOCK
 *         sc_in_clk
  clock;
  */
/*        scClusterBusMaster_blocking*
  mMaster_if;
*/
  SC_HAS_PROCESS(scClusterBusMemorySlow);

  // constructor
  scClusterBusMemorySlow(sc_module_name name_
		      , unsigned int start_address
		      , unsigned int end_address
              , unsigned int nr_wait_states);
/*    , m_start_address(start_address)
    , m_end_address(end_address)
    , m_nr_wait_states(nr_wait_states)
    , m_wait_count(-1)
  {
    // process declaration
    SC_METHOD(wait_loop);
    dont_initialize();
    sensitive << clock.pos();

    sc_assert(m_start_address <= m_end_address);
    sc_assert((m_end_address-m_start_address+1)%4 == 0);
    m_size = (m_end_address-m_start_address+1)/4;
    MEM = new int [m_size];
    for (unsigned int i = 0; i < m_size; ++i)
      MEM[i] = 0;
  }*/

  // destructor
  ~scClusterBusMemorySlow();

  // process
  void wait_loop();
  void ProcessFIFOMessages(void);
        ClusterBusStatus
    write(scIGPMessage* M);
  // direct Slave Interface
  bool direct_read(int *data, unsigned int address);
  bool direct_write(int *data, unsigned int address);
  bool direct_read(scIGPMessage*){return true;}
  bool direct_write(scIGPMessage*){return true;}

  // Slave Interface
  ClusterBusStatus read(int *data, unsigned int address);
  ClusterBusStatus write(int *data, unsigned int address);
  ClusterBusStatus read(scIGPMessage*){ return CLUSTER_BUS_ERROR;}
//  ClusterBusStatus write(scIGPMessage*);
        scIGPMessage*
    CreateReadMessageReply(scMemoryMessageFIFO_t* M);
        scIGPMessage*
    CreateWriteMessage(scGridPoint* From, unsigned int Address, int Length);
        std::string
    StringOfClusterAddress_Get(void);

    void
    ReadFIFO(scMemoryMessageFIFO_t *&M){ return MemoryMessagefifo->read(M);}
    void
    WriteFIFO(scMemoryMessageFIFO_t *&M){ return MemoryMessagefifo->write(M);}
        scProcessor* //! Just remember processor for finding the cores
    Processor_Get(void) { return m_processor;}
        void    //! Remember the processor for finding the cores
    Processor_Set(scProcessor* Proc) { m_processor = Proc;}
        string
    PrologText_Get(void);
        void
    ReceiveClusterMessage(scIGPMessage* Message);
        void
    SendClusterMessage(scIGPMessage* Message);
        void
    ProcessMessage(scIGPMessage* M);
        unsigned int
    start_address() const;
        unsigned int
    end_address() const;
        unsigned int
    Size_Get(void){return m_size;}
/*        inline uint32_t
    LoadedBytes_Get(void) { return TotalBytes;}*/
        inline uint8_t // Just for testing
    Byte_Get(uint32_t Address){ return mem[Address];}
        void
    Byte_Set(uint32_t Address, uint8_t B){ mem[Address] = B;}
        uint32_t // Just for testing
    DWord_Get(uint32_t Address);//{ assert(!(Address%4)); return MEM[Address/4];}
        void
    DWord_Set(uint32_t Address, uint32_t C);//{ assert(!(Address%4)); MEM[Address/4] = C;}
        int
    findLinenoToAddress(SC_ADDRESS_TYPE A);
        SC_ADDRESS_TYPE
    findAddressToLineno(int32_t L);
        string
    getSourceLine(int L);
        SC_ADDRESS_TYPE
    StartAddress_Get(void){ return m_start_address; }
        int
    QueueLength_Get(void){ return MemoryMessagefifo->num_available(); }
        void Reset(void);
  private:
    int *MEM;
    unsigned int m_size;
    scProcessor* m_processor;
    unsigned int m_start_address;
    unsigned int m_end_address;
    unsigned int m_nr_wait_states;
    int m_wait_count;
        scMemoryMessageFIFO*
    MemoryMessagefifo;  /// The memory messages are temporarily stored here
        uint8_t *
    mem;
  /**
   * @brief Log classe
   */
//MM  Log *log;
  /**
  * @brief Program counter (PC) read from hex file
  */
/*  uint32_t program_counter;
  uint32_t mInstructionAddress; ///< At which position the instruction found
  uint32_t TotalBytes;      ///< How many bytes were read
  uint32_t TotalLines;      ///< How many line are present in the object file
*/        vector <AddressToLineRecord*> // The real data storage
    AddressToLineMap;
       string
    mFileName;
}; // end class scClusterBusMemorySlow

inline  scClusterBusMemorySlow::~scClusterBusMemorySlow()
{
  if (MEM) delete [] MEM;
  MEM = (int *)0;
}

inline void scClusterBusMemorySlow::wait_loop()
{
  if (m_wait_count >= 0) m_wait_count--;
}

inline bool scClusterBusMemorySlow::direct_read(int *data, unsigned int address)
{
    assert((address - m_start_address)/4 < m_size);
  *data = MEM[(address - m_start_address)/4];
  return true;
}

inline bool scClusterBusMemorySlow::direct_write(int *data, unsigned int address)
{
    assert((address - m_start_address)/4 < m_size);
  MEM[(address - m_start_address)/4] = *data;
  return true;
}

inline ClusterBusStatus scClusterBusMemorySlow::read(int *data
						   , unsigned int address)
{
    assert((address - m_start_address)/4 < m_size);
  // accept a new call if m_wait_count < 0)
  if (m_wait_count < 0)
    {
      m_wait_count = m_nr_wait_states;
      return CLUSTER_BUS_WAIT;
    }
  if (m_wait_count == 0)
    {
      *data = MEM[(address - m_start_address)/4];
      return CLUSTER_BUS_OK;
    }
  return CLUSTER_BUS_WAIT;
}

inline ClusterBusStatus scClusterBusMemorySlow::write(int *data
						    , unsigned int address)
{
    assert((address - m_start_address)/4 < m_size);
  // accept a new call if m_wait_count < 0)
  if (m_wait_count < 0)
    {
      m_wait_count = m_nr_wait_states;
      return CLUSTER_BUS_WAIT;
    }
  if (m_wait_count == 0)
    {
      MEM[(address - m_start_address)/4] = *data;
      return CLUSTER_BUS_OK;
    }
  return CLUSTER_BUS_WAIT;
}

inline unsigned int scClusterBusMemorySlow::start_address() const
{
  return m_start_address;
}

inline unsigned int scClusterBusMemorySlow::
end_address() const
{
  return m_end_address;
}

#endif //  scClusterBusMemorySlow_h
