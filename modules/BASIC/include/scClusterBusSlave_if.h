/** @file scClusterBusSlave_if.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The inter-cluster bus states
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
/*
  The origin of this development file is available as SystemC example
 
  scClusterBusSlave_if.h : The Slave interface.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 
#ifndef scClusterBusSlave_if_h
#define scClusterBusSlave_if_h

//#include <systemc.h>

#include "ClusterBusTypes.h"
//#include "scIGPMessage.h"
#include "scClusterBusDirect_if.h"
class scIGPMessage;


class scClusterBusSlave_if
  : public scClusterBusDirect_if
{
public:
  // Slave interface
  virtual ClusterBusStatus read(int *data, unsigned int address) = 0;
  virtual ClusterBusStatus write(int *data, unsigned int address) = 0;
  virtual ClusterBusStatus read(scIGPMessage* M) = 0;
  virtual ClusterBusStatus write(scIGPMessage* M) = 0;

//  virtual unsigned int start_address() const = 0;
//  virtual unsigned int end_address() const = 0;

}; // end scClusterBusSlave_if

#endif  // scClusterBusSlave_if_h
