/** @file AbstractTopology.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Function prototypes for the topology of electronic module, placed on a die.
 *  It is just math, to physical modules
 */
/*
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
*/


#ifndef ABSTRACTTOPOLOGY_H
#define ABSTRACTTOPOLOGY_H
/** @addtogroup TAC_MODULE_BASIC
 *  This is the first group
 *  @{
 */
//#include <iomanip>      // std::setfill, std::setw
//#include <vector>
//#include <iostream>     // std::cout, std::ios
//#include <sstream>      // std::ostringstream
//#include <bitset>         // std::bitset
//#include "GridPoint.h"
#include "MemoryClustering.h"
class GridPoint;

using namespace std;

const std::vector<char> GridXOffset {0,0,1, 1, 0,-1,-1};
const std::vector<char> GridYOffset {0,2,1,-1,-2,-1, 1};

// Here a  is assumed for the physical scGridPoints
// and the HThreads running on the , sharing its resources
// The core exists only in one instantiation, but data structures
// belonging to independent HThreads are partly stored in the scGridPoint, in arrays

extern int YFromPosition_Get(int x, int y);
/*!
 * AbstractTopology is a simple math class that deals with the topological relations of the modules.
 * The modules are arranged in a 2-d surface, in a square grid, that can be considered also as a hexa grid.
 * Note that 'index' refers to the rectangular view, 'positions' to the hexagonal view
 *
 * A basic idea is that the electronic modules are arranged to form a two-dimensional topology that has physical scGridPoint modules
 * as its nodes. The geometrical properties of the nodes are described by GridPoint. The gridpoints are
 * electronically connected to each other and can communicate autonomously, using a special,
 * hierarchical bus system scClusterBus. Although the bus is fully functional,
 * to speed up simulation its clocking is switched off (to enable faster simulation).
 * However, the simulation imitates the temporal behavior by adjusting the simulated time properly
 * even if the clocking is switched off.
 *
 */
class AbstractTopology
{
  public:
    /*!
     * \brief AbstractTopology::AbstractTopology
     *
     * Creates the topology of GridPoint modules of a time aware electronic processor
     * (serves as a base of core scProcessor, Abstract or Neuerprocessor for brain simulator).
     *  This is purely math, has nothing to do with higher level operations.
     *  Just creates a two-dimensional either rectangular or hexagonal grid.
     *  The rectangular grid comprises individual grid points, without neighbors;
     *  but the points are organized into clusters, the clusters into chips, etc.
     *  The grids comprise cluster heads and members, and enjoy the advantages of the
     *  topological proximity.
     *
     *  The hexagonal grid enables to implement a special storage: the grid points are arranged in a way
     *  that enables to consider the grid points as hexagons, having common boundaries
     *  and enable to communicate with each other through the boundaries.
     *  These 7 cores constitue a cluster. The central core is the Head, and the other 6 Member cores
     *  can only be reached through the Head. The members of the cluster can only be reached
     *  through the cluster head.
     *
     * The access functions return nullptr is the parametrization is wrong.
     *
     * @see  ClusterNeighbor
     */

    AbstractTopology(void);
    virtual ~AbstractTopology(void); // Must be overridden
    int NumberOfGridPoints_Get(void) {return GRID_SIZE_X * GRID_SIZE_Y; }
    int NumberOfClusters_Get(void){return mClusters.size();}
    // Accessor functions: without thread, the physical access (0th HThread)
        GridPoint*
    ByID_Get(const int N); //!< Get a gridpoint by its absolute sequence number and thread
        GridPoint*
    ByIndex_Get(const int X, const int Y);  //!< Get a gridpoint defined by its indices (!NOT position)
        GridPoint*
    ByPosition_Get(int X, int Y);   //!< Get a gridpoint by its topological position (NOT index!)
    // Helper functions for address calculation
        int
    LinearAddressFromCoordinates_Get( int X, int Y)
    {   assert(!(X<0 || X>=GRID_SIZE_X));   assert(!(Y<0 || Y>=GRID_SIZE_Y));
        return  X*GRID_SIZE_Y +  Y; }
        int
    LinearAddressFromPosition_Get(int X, int Y)
    {return LinearAddressFromCoordinates_Get(X,Y/2); }
        int
    LinearAddressFromPosition_Get(GridPoint* GP) ;
 //   {return LinearAddressFromCoordinates_Get(GP->X,GP->Y); }
        /*!
         * \brief Get a GridPoint pointer given by its ClusterAddress_t C, a specialized integer (network-like address)
         * \param[in] C the ClusterAddress_t address of the GridPoint
         * \return pointer to the GridPoint
         */
        GridPoint*
    ByClusterAddress_Get(ClusterAddress_t C);
        /*!
         * Return the pointer to the gridpoint described by cluster member parameters
         * \param[in] CN the sequence number of the cluster
         * \param[in] CM the member requested (if any)
         * \param[in] CP the proxy (if any)
         * \return the pointer to the gridpoint
         */

        GridPoint*
    ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM = cm_Head,  ClusterNeighbor CP = cm_Head);
        /*!
         * \brief Find the direction of neighbor N relative to Head
         * \param N pointer to the neighbor
         * \return direction if found, else cm_Broadcast
         */
        ClusterNeighbor
    NeighborDirectionFind(GridPoint* Head, GridPoint* G);
        /*!
         * \brief Return the pointer corresponding to the in-cluster member CM of cluster
         * \param[in] P The actual GridPoint
         * \param[in] CM The actual member
         * \return
         */
        GridPoint*
    ByClusterPointer_Get(GridPoint* P, ClusterNeighbor CM = cm_Head);
        /*!
         * \brief Create a new cluster at GridPoint P
         * \param[in] P the GridPoint that will be the cluster head
         */
        void
    Cluster_Create(GridPoint* P);
        /*!
         * \brief Create a new cluster at coordinates X,Y
         *
         * It may also be a phantom (the head is outside, some members inside):
         * the members of these phantom clusters are attached to a real cluster as external member.
         * If the cluster head is a phantom, create an external member using an alias
         * the member is attached to a real cluster head, using a proxy
         *
         * \param[in] X position if the cluster head
         * \param[in] Y position if the cluster head
         */
        void
    Cluster_Create(int X, int Y);
        GridPoint* 
    ClusterHeadOfMember_Get(GridPoint* GP); ///< Return the cluster head of GridPoint GP
 //
        /*!
         * \brief Set cluster status upon creation
         * \param P the actual GridPoint
         * \param N member code inside the cluster
         * \param S status to set
         */
        void
    ClusterStatus_Set(GridPoint* P, ClusterNeighbor N, ClusterStatus S); ///< Set status to S of neighbor N relative to GridPoint P
        ClusterStatus
    ClusterStatus_Get(GridPoint* P, ClusterNeighbor N); ///< Return status of neighbor N relative to GridPoint P
        ClusterAddress_t
    ClusterAddress_Get(GridPoint* P); //!< Return the cluster address of the point P
        ClusterAddress_t
    ClusterAddressFromLinear_Get(int N); //!< Return the cluster address of the Nth point
        int 
    NeighborIndexInCluster_Get(GridPoint* Head, GridPoint* Neighbor);

#ifdef DRAW
 Valuable and works, but needed only when changing the number of points or debugging
          void
    DrawPoint(int i,int j, ostringstream &oss);
        void
    Draw(void);
#endif // DRAW
        /*!
           * \brief Create clusters from the elements of the rectangular grid.
           * This routine works perfectly ONLY for a 10*6 sized grid!!
           */
        void
    CreateClusters(void);

        /*!
         * \brief Return pointer to the cluster head of the Nth cluster
         * \param N the sequence number of the cluster
         * \return the cluster head grid point
         */

        GridPoint*
    ClusterHead_Get(unsigned int N)
    {
        assert (!(N<0) || (N>=mClusters.size()));  // Illegal member
        return mClusters[N]; // We always have a 0-th thread
    }
        /*!
             * \brief Return the string describing GridPoint GP in its ClusterAddress_t form
             * \param GP the cluster address of GridPoint
             * \return the string corresponding to GP
             */
        string /// Return the string describing the cluster address of GP
    StringOfClusterAddress_Get(GridPoint* GP);
        SC_ADDRESS_TYPE
    PC_Get() { return mProgramCounter;}
        void
    PC_Set(SC_ADDRESS_TYPE PC) { mProgramCounter = PC;}

  protected:
        // The SAME physical points are stored in different structures
        vector<vector<GridPoint*> >  /// The gridpoints form a 2-dim grid, addressable by their index
    mGrid; 
        vector<GridPoint*>  /// The clustered gridpoints are accessible through their clusters, too
    mClusters;  
        SC_ADDRESS_TYPE /// The architecture itself has a global program counter
    mProgramCounter;
 };// of class AbstractTopology
/** @}*/
#endif // ABSTRACTTOPOLOGY_H
