/** @file scClusterBus_blocking_if.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The inter-cluster blocking IF
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
*/

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
 * 
  scClusterBus_blocking_if.h : The blocking bus interface.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/

#ifndef scClusterBus_blocking_if_h
#define scClusterBus_blocking_if_h

//#include <systemc.h>

#include "ClusterBusTypes.h"
class scIGPMessage;
class scClusterBus_blocking_if
  : public virtual sc_interface
{
public:
  // blocking BUS interface
  virtual ClusterBusStatus burst_read(unsigned int unique_priority
/*				       , int *data
				       , unsigned int start_address
                       , unsigned int length = 1*/
                       , scIGPMessage* M
				       , bool lock = false) = 0;
  virtual ClusterBusStatus burst_write(unsigned int unique_priority
                    /*, int *data
					, unsigned int start_address
                    , unsigned int length = 1*/
                    , scIGPMessage* M
					, bool lock = false) = 0;

}; // end class scClusterBus_blocking_if

#endif
