
/*****************************************************************************
 * @file scClusterBus_blocking.h
 *  @ingroup TAC_MODULE_BASIC
  simple_bus_master_blocking.h : The master using the blocking BUS interface.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/
 

#ifndef __simple_bus_master_blocking_h
#define __simple_bus_master_blocking_h

//#include <systemc.h>

//#include "ClusterBusTypes.h"
#include "scClusterBus_blocking_if.h"

class scClusterBusMaster_blocking : public sc_core::sc_module
//SC_MODULE(scClusterBusMaster_blocking)
{
    friend class scGridPoint;
  // ports
public:
    sc_port<scClusterBus_blocking_if> bus_port;
//  sc_in_clk clock;
  SC_HAS_PROCESS(scClusterBusMaster_blocking);

  // constructor
  scClusterBusMaster_blocking(sc_module_name name_
			     , unsigned int unique_priority
			     , unsigned int address
                             , bool lock
                             , int timeout)
    : sc_module(name_)
    , m_unique_priority(unique_priority)
    , m_address(address)
    , m_lock(lock)
    , m_timeout(timeout)
  {
    // process declaration
//??    SC_THREAD(main_action);
//??    sensitive << clock.pos();
  }
  
  // process
  void main_action();

private:
  unsigned int m_unique_priority;
  unsigned int m_address;
  bool m_lock;
  int m_timeout;

}; // end class scClusterBusMaster_blocking

#endif
