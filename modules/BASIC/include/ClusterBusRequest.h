/** @file ClusterBusRequest.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief The description of the inter-cluster messages
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
  simple_bus_request.h : The bus interface request form.
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
*/
 

#ifndef cluster_bus_request_h
#define cluster_bus_request_h
/** @addtogroup TAC_MODULE_BASIC
 *  This is the first group
 *  @{
 */

#include "ClusterBusTypes.h"
//#include "scIGPMessage.h"
class scIGPMessage;

enum ClusterBus_lock_status { CLUSTER_BUS_LOCK_NO = 0
			      , CLUSTER_BUS_LOCK_SET
			      , CLUSTER_BUS_LOCK_GRANTED
};
/*!
  \struct ClusterBusRequest
* \brief A GridPoint can be addressed also by its cluster address of type ClusterAddress_t
* */

struct ClusterBusRequest
{
  // parameters
  unsigned int priority;

  // request parameters
  bool do_write;
/*  unsigned int address;
  unsigned int end_address;
  int *data;*/
  scIGPMessage* M;
  ClusterBus_lock_status lock;

  // request status
  sc_event transfer_done;
  ClusterBusStatus status;

  // default constructor
  ClusterBusRequest();
};

inline ClusterBusRequest::ClusterBusRequest()
  : priority(0)
  , do_write(false)
  , M((scIGPMessage*)0)
  , lock(CLUSTER_BUS_LOCK_NO)
  , status(CLUSTER_BUS_OK)
{}
/** @}*/
#endif // cluster_bus_request_h
