/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @file GridPoint.h
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Topology information for the electronic modules arranged in a grid
 */

#ifndef GRIDPOINT_H
#define GRIDPOINT_H
//#include <iostream>
#include <sstream>
#include "MemoryClustering.h"
using namespace std;
class scHThread;


/*!
 * \class GridPoint
 *
 * \brief This class handles the topological information for the modules.
 * Provides a lot of math-only utility functions. All the derived classes
 * use those functions. The derived classes are
 * @verbatim
 *  |GridPoint
 *  |--/scGridPoint
 *  |..../AbstractCore
 *  |....../XXCore
 *  |....../AbstractNeurer
 *  |......../Neurer
 * @endverbatim
*/
class GridPoint
{
  public:
    // Arranged in a two-dimensional hexagonal grid; with the H HThread, 3 dimensional
    GridPoint(int16_t xx=0,int16_t yy=0, ClusterStatus CS=cs_NONE):X(xx),Y(yy){
          mClusterAddress.HThread = 0,
          mClusterAddress.Reserved = 0, //reserved
          mClusterAddress.Rack= 0,//Rack
          mClusterAddress.Topology= 0,//Topology (like Processor)
          mClusterAddress.Card= 0,//Card
          mClusterAddress.Cluster = MAX_CLUSTERS_LIMIT,//Cluster
          mClusterAddress.Member=cm_Head,//Member
          mClusterAddress.Proxy =cm_Head;//Proxy
          mClusterAddress.Status = CS;
//          mMyHThreads = 0;  // Initially, no HThreads are
        }
         virtual
    ~GridPoint(){}

        ClusterAddress_t ///<  Return the cluster address of the gridpoint, stored in the matrix
    ClusterAddress_Get(void){ return mClusterAddress;}
        void //!  Set the cluster address of the gridpoint
    ClusterAddress_Set(ClusterAddress_t CA){ mClusterAddress = CA;}
        void //!  Set the cluster address of the gridpoint
    ClusterAddressHThread_Set(int H){ mClusterAddress.HThread = H;}
        ClusterStatus //!  Return the status of the gridpoint
    ClusterStatus_Get(void){ return (ClusterStatus)mClusterAddress.Status;}
        void //!  Set the cstatus of the gridpoint
    ClusterStatus_Set(ClusterStatus CS){ mClusterAddress.Status = (int)CS;}
        int16_t X, ///< The X index of the gridpont
               Y; ///< The Y index of the gridpont
        ClusterAddress_t
    mClusterAddress; ///< The ClusterAddress_t address of the gridpoint
        int
    YPosition_Get(void){ return Y*2 +moduloN(X,2); }
    ///< return Y position from coordinates, NOT identical with accessing GridPoint#Y
    ///< @see GridPoint#X and GridPoint#Y

        /*!
         * \brief Return true if we are an immediate neighbor of GP
         * \param[in] GP the other gridpoint
         * \return true if GP is our neighbor
         */
        bool
    IsNeighborOf(GridPoint* GP)
    {
        if(!(GP)) return false; // The candidate GridPoint is outside
         // Check N and S
        if( (X == GP->X) && (abs(YPosition_Get() - GP->YPosition_Get()) == 2)) return true;
        // Check NE, SE, SW, NW positions
        return( (abs(X - GP->X) == 1) && (abs(YPosition_Get() - GP->YPosition_Get()) == 1) );
    }

       /*!
         * \brief Return true if we are a second neighbor of GP
         * \param[in] GP the other gridpoint
         * \return true if GP is our second neighbor
         */
        bool
    Is2ndNeighborOf(GridPoint* GP)
    {
        if(!(GP)) return false; // The candidate GridPoint is outside
        // Both points are within the grid
        // Check N and S
        if( (abs(X - GP->X)== 0) && (abs(YPosition_Get() - GP->YPosition_Get()) == 4)) return true;
        // Check E and W
        if( (abs(X - GP->X)== 2) && (abs(YPosition_Get() - GP->YPosition_Get()) == 0)) return true;
        // Check NNE, SE, SW, NNW positions
        if( (abs(X - GP->X) == 1) && (abs(YPosition_Get() - GP->YPosition_Get()) == 3) ) return true;
        // Check NNE, SSE, SSW, NNW positions
        if( (abs(X - GP->X) == 2) && (abs(YPosition_Get() - GP->YPosition_Get()) == 2) ) return true;
        return false;
     }

        /*!
         * \brief Return true if GridPoint GP is identical with us
         * \param GP The other gridpoint
         * \return true if this and GP are the same gridpoint
         */
       bool
    IsTheSameAs(GridPoint* GP)
    {
        if(!GP) return false;
        return this == GP;
    }
        /*!
         * \brief Return true if we and GridPoint GP are in the same cluster
         * \param GP The other gridpoint
         * \return true if this and GP belong to the same cluster
         */ 
        bool
    IsInTheSameClusterAs(GridPoint* GP)
    {
        if(!GP) return false;
        return mClusterAddress.Cluster == GP->ClusterAddress_Get().Cluster;
    }

#if 0
       /*!
        * \brief Return the string describing our proxy (if any)
        * \return the string form of only the proxy part of the gridpoint
        */
       string
   StringOfProxy_Get(void
                     )
   {   ostringstream oss;
           ClusterAddress_t CA = ClusterAddress_Get();
        if(CA.Proxy) {oss << ":" << ClusterMembers[CA.Proxy].name;}
       return oss.str();
   }
#endif //0
       /*!
        * \brief Return the string describing the actual thread
        * \return the string form of only the HW thread of the gridpoint
        */
       string
   StringOfThread_Get(
               )
   {   ostringstream oss;
          ClusterAddress_t CA = ClusterAddress_Get();
//       if(CA.HThread)
           oss << CA.HThread;
       return oss.str();
   }
   /*!
    * \brief Return the string form of our cluster address
    *
    * The string form comprises two parts
    * - the arguments between {} provide the position coordinates
    * - the arguments between () provide the clusterized address
    */
      string
    StringOfClusterAddressName_Get(void)
    {
        ostringstream oss;
        oss << '{' << (int16_t)X << ',' << (int16_t)YPosition_Get() << '}' << '=';
        oss << "(" ;
        ClusterAddress_t CA = ClusterAddress_Get();
        oss << CA.Cluster << "." << ClusterMembers[CA.Member].name;
        if(CA.Proxy) {oss << ":" << ClusterMembers[CA.Proxy].name;}
//        oss << StringOfProxy_Get() << ")";
        oss << ")" ;
        if(CA.HThread) oss << '/' << StringOfThread_Get();
        return oss.str();
    }

};

#endif // GRIDPOINT_H
