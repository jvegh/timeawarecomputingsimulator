/** @file scLatchFIFO.cpp
 *  @brief Function prototypes for the EMPA simulator, scFIFO
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scLatchFIFO.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
//#define DEBUG_PENDING_OPERATIONS // Print pending, queued, executing, executed operations
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

using namespace std;


    // This is the FIFO for the backlinked register values
    // received from the scSupervisor
    // Every single scCore contains a FIFO with register contents returned by its children
    // This FIFO locically belongs  scCore, each scCore has its own exemplar
    RegisterLatch_fifo::RegisterLatch_fifo(const sc_module_name& name)
        : sc_core::sc_channel(name), num_elements(0), first(0)
    {
        DEBUG_PRINT(sc_time_stamp() << '|' << name << " Created ");
    }

    // Writing takes place in normal order
      void RegisterLatch_fifo::
    write(RegisterLatch_Type T) {
      if (num_elements == max)
             wait(read_event);
      dowrite(T);
      write_event.notify(SC_ZERO_TIME);
    }

      // Writing takes place in normal order
        void RegisterLatch_fifo::
    dowrite(RegisterLatch_Type T) {
        data[(first + num_elements) % max] = T;
        ++ num_elements;
      }

    // Reading may be selective: if Offset != -1, the item with correct offset found and returned
      bool RegisterLatch_fifo::
    read(RegisterLatch_Type &T, SC_ADDRESS_TYPE Offset){
      if (num_elements == 0)
         wait(write_event);
      if(!doread(T, Offset))
          return false;
      read_event.notify(SC_ZERO_TIME);
      return true;

    }

      // Reading may be selective: if Offset != -1, the item with correct offset found and returned
        bool RegisterLatch_fifo::
    doread(RegisterLatch_Type &T, SC_ADDRESS_TYPE Offset){
            bool found = false;
        if(Offset == (SC_ADDRESS_TYPE)-1)
        { // Any of the item matches
          T = data[first];
          -- num_elements;
          found = true;
        }
        else
        { // We need to find item with a specific Offset
          int index;
          for(int i = 0; i<num_elements; i++)
          {
            index = (first+i) % max;
            RegisterLatch_Type TT = data[index];
            if(TT.QTOffset == Offset)
              { T = data[index]; found = true; break;}
          }
          if(found)
          {
          // Item found and returned, remove it from the queue
          for(int i = index; i<num_elements-1; i++)
            data[(first+i) % max] = data[(first+i+1) % max]; // Shift the queue
          num_elements--;
          }
        }
        if(found)
            first = (first + 1) % max;
        return found;
      }
