/** @file scClusterBusTypes.cpp
 *  @brief The inter-cluster bus states
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  The name changes are because multiple such buses are present in the development
  simple_bus_types.cpp : The common types.
 
  Original Author: Holger Keding, Synopsys, Inc., 2002-01-28
 
 *****************************************************************************/

// only needed for more readable debug output
char ClusterBusStatus_str[4][20] = {"SIMPLE_BUS_OK"
                                      , "SIMPLE_BUS_REQUEST"
                                      , "SIMPLE_BUS_WAIT"
                                      , "SIMPLE_BUS_ERROR"};
