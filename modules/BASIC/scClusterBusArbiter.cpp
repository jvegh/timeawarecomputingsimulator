/** @file scClusterBusArbiter.cpp
 *  @brief The arbiter module of the inter-cluster bus
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  Changing the names is a must, because multiple buses are present in the development
  The operation of the arbiter is essentialy not changed, but uses different modules and ways
  simple_bus_arbiter.cpp : The arbitration unit.
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
*/

#include "scClusterBusArbiter.h" //#include "ClusterBusTypes.h"
#include "ClusterBusRequest.h"
//#include "ClusterBusTypes.h"
//#include "Utils.h"

ClusterBusRequest *
scClusterBusArbiter::arbitrate(const ClusterBusRequest_vec &requests)
{
  unsigned int i;
  // at least one request is here
  ClusterBusRequest *best_request = requests[0];

  if (m_verbose) 
    { // shows the list of pending requests
      sb_fprintf(stdout, "%s %s :", sc_time_stamp().to_string().c_str(), name());
      for (i = 0; i < requests.size(); ++i)
	{
//	  ClusterBusRequest *request = requests[i];
          // simple_bus_lock_status encoding
//          const char lock_chars[] = { '-', '=', '+' };
          // simple_bus_status encoding
 /*          sb_fprintf(stdout, "\n    R[%d](%c%s@%x)",
                     request->priority,
                     lock_chars[request->lock],
                     ClusterBusStatus_str[request->status],
                     request->address);*/
	}
    }

  // highest priority: status==SIMPLE_BUS_WAIT and lock is set: 
  // locked burst-action
  for (i = 0; i < requests.size(); ++i)
    {
      ClusterBusRequest *request = requests[i];
      if ((request->status == CLUSTER_BUS_WAIT) &&
	  (request->lock == CLUSTER_BUS_LOCK_SET))
	{
	  // cannot break-in a locked burst
	  if (m_verbose)
            sb_fprintf(stdout, " -> R[%d] (rule 1)\n", request->priority);
	  return request;
	}
    }

  // second priority: lock is set at previous call, 
  // i.e. SIMPLE_BUS_LOCK_GRANTED
  for (i = 0; i < requests.size(); ++i)
    if (requests[i]->lock == CLUSTER_BUS_LOCK_GRANTED)
      {
	if (m_verbose)
	  sb_fprintf(stdout, " -> R[%d] (rule 2)\n", requests[i]->priority);
	return requests[i];
      }

  // third priority: priority
  for (i = 1; i < requests.size(); ++i)
    {
      sc_assert(requests[i]->priority != best_request->priority);
      if (requests[i]->priority < best_request->priority)
	best_request = requests[i];
    }

  if (best_request->lock != CLUSTER_BUS_LOCK_NO)
    best_request->lock = CLUSTER_BUS_LOCK_GRANTED;
	
  if (m_verbose) 
    sb_fprintf(stdout, " -> R[%d] (rule 3)\n", best_request->priority);

  return best_request;
}
