/** @file scGate2Input.cpp
 *  @brief Function prototypes for the scGrid simulator,
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "scGate2Input.h"
#include "scGatesProcessor.h"
#include "scSimulator.h"

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h', and are undefined in that file
//#define MAKE_LOG_TRACING    // We want to have trace messages
//#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"


extern bool UNIT_TESTING;	// Whether in course of unit testing

scGate2Input::scGate2Input(sc_core::sc_module_name nm
                        ,scGatesProcessor* Processor
                        ,const GridPoint GP, bool StandAlone):
    scGridPoint(nm, Processor,GP,StandAlone)
{
    /*
    msRegisterLatch = new RegisterLatch_fifo(string(nm).append("Latch").c_str());
    msID =Processor->LinearAddressFromCoordinates_Get(GP.X, GP.Y);               // The physical ID of the gridpoint, unchangeable
    msMask.ID = (1 << msID);     // A redundant storage, to avoid on the flight calculations
    msMask.Children = 0;    // Initially, we have no children
    msMask.AllocatedHThreads = 0;    // Initially, we have no threads
    // The followings can only be done once, only in the constructor
    // Establish neigborship facility to the default stage 'no neighbours'
    for(ClusterNeighbor N = cm_Head; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
      { // Initially, there are no FIFOs and no neighbors
        msIGPCB[N] = (scIGPCB*)NULL;
      }
    CreateThreads();
    // Handle HW signals: binding can only be in the constructor
      msPort.Denied(msSignal.Denied);
      msPort.Allocated(msSignal.Allocated);
      msPort.PreAllocated(msSignal.PreAllocated);
      msPort.Available(msSignal.Available);
      msPort.Meta(msSignal.Meta);
      msPort.Wait(msSignal.Wait);
      msPort.Sleeping(msSignal.Sleeping);     // Connected to sleeping, but no mask in Topology
      // by default, no binding to inter-cluster bus
      // Will be set ONLY for cluster head when creating the clusters
      mMaster_if = NULL;

    SC_METHOD(SIGNAL_method);
        sensitive << EVENT_GRID.SIGNAL_StateBit;
    dont_initialize();
    SC_THREAD(FETCH_thread);
        sensitive << EVENT_GRID.MakeFetch;
    SC_THREAD(EXEC_thread);
        sensitive << EVENT_GRID.MakeExec;
     if(StandAlone)
    {
        //!! Removed temporarily, until NEXT in AbstractCore fully implemented
//!!        SC_METHOD(NEXT_thread);
//!!            dont_initialize();
//!!            sensitive << EVENT_GRID.NEXT;
        SC_METHOD(CREATE_method);
            dont_initialize();
            sensitive << EVENT_GRID.CREATE;
        Reset(); // Set the signals to theit default state
    }
*/
    // The rest can be called also from outside the constructor
}// of scGate2Input::scGate2Input


/*!
 * \fn void scGridPoint::Reset(void)
 * 
 * \brief Reset all status bits
 * 
 * */
void scGate2Input::
Reset(void)
{
     BENCHMARK_TIME_RESET(&m_running_time,&m_part_time,&m_sum_time); // Just clear all work storage, for any case
}

scGate2Input::~scGate2Input()
{
}//scGridPoint::~scGridPoint


