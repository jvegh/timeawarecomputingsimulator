/** @file scIGPMessage.cpp
 *  @brief Function prototypes for the EMPA simulator, core
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

//#include "scTypes.h"
//#include "scGridPoint.h"
#include "scIGPMessage.h"
//#include "Memory.h"
class scGridPoint;

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"

NamedEnum_struct MessageTypes[] =
{
    {"Reg",   msg_Reg},
    {"Qt",   msg_Qt},
    {"Mem",   msg_Mem},
    {"Coop",  msg_Coop},
    {"Neur",   msg_Neur}
};

extern bool UNIT_TESTING;	// Whether in course of unit testing

// Creates a message for inter-GridPoint communication
scIGPMessage::scIGPMessage()
{
}// of scIGPMessage::scIGPMessage

scIGPMessage::~scIGPMessage()
{
}// of scIGPMessage::~scIGPMessage
// Message handling
