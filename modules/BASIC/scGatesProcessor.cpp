/** @file scGatesProcessor.cpp
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include <algorithm>    // for sorting
#include "scGatesProcessor.h"
//#include "scIGPMessage.h"
/*#include "scClusterBusMemoryFast.h"
#include "scClusterBusMemorySlow.h"
#include "scClusterBusArbiter.h"
#include "scIGPCB.h"
*/
//#include "Utils.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing
//extern NamedEnum_struct MessageTypes[];
//extern scClusterBusMemorySlow* MainMemory;
/*!
 * \brief The processor working with scGate2Input electronix gates
 *   \param nm The SystemC name
 *   \param Specials some special scGridPoints (enables to handle heterogenous topologies)
 *   \param StandAlone true if some part shall be executed here otherwise in the subclass
 */
scGatesProcessor::
scGatesProcessor(sc_core::sc_module_name nm,
            vector<scGridPoint*> Specials, bool StandAlone)
         : scProcessor(nm, Specials,false)
{
    for(int h = 0; h < MAX_HTHREADS; h++)
    {
    // Set mask values for the signals
    msSignalMask[h].Denied = 0; // Neither of the GridPoints is denied
    msSignalMask[h].Allocated = 0; // None of them is allocated
    msSignalMask[h].PreAllocated = 0; // None of them is preallocated
    msSignalMask[h].Available = (int64_t) -1;    // All are available
//    msSignalMask.Children = 0; // No direct children yet
    msSignalMask[h].PreAllocatedForTopology = 0; // No scGridPoints are allocated DIRECTLY for the topology
    
    // Connect signals to ports
    msSignalPort[h].Denied(msSignal[h].Denied);  // Which scGridPoints are denied
    msSignalPort[h].Allocated(msSignal[h].Allocated);  // Which scGridPoints are Allocated
    msSignalPort[h].PreAllocated(msSignal[h].PreAllocated);  // Which scGridPoints are PreAllocated
    msSignalPort[h].Available(msSignal[h].Available);  // Which scGridPoints are available
    }
    // Availability status bits are handled here
    SC_METHOD(SetMasks_method); // Any of the states relating masking changed
       sensitive << EVENT_PROCESSOR.MasksChanged;
       dont_initialize(); //
//    MainMemory->Processor_Set(this);
    // Implement the Cluster-Related modules
 /*   mClusterBus = new scClusterBus("ClusterBus",this);
//    mClusterBus->clock(clock);

    mClusterBusArbiter = new scClusterBusArbiter("ClusterArbiter");
    // No arbiter clock
    mClusterBus->arbiter_port(*mClusterBusArbiter);

    mClusterMemoryFast = new scClusterBusMemoryFast("Register", 0x00, 32*4-1);
    //   mClusterMemoryFast->clock(clock);    // The direct memory has no clock   
    mClusterBus->slave_port(*mClusterMemoryFast);

    // The main memory is independent, must be created and clocked in the top module
    // It can be connected to the cluster bus here
    mClusterBus->slave_port(*MainMemory);

//    MetaEvents = new  MetaEvent_fifo("SV_MetaFIFO"); 
*/
   // These threads are simple flag-handlers, so they are NOT re-implemented in the subclasses

    if(StandAlone)
    {   // scGridPoints are not subclassed, will be populated
        SC_METHOD(Initialize_method);
        //SC_THREAD(START_thread);
        //    sensitive << EVENT_PROCESSOR.START;
         // Triggered by writing into the MetaEvent_fifo       
/*         SC_THREAD(Execute_thread);
           sensitive << MetaEvents->write_event;  */
 //!!        SC_THREAD(BackLink_thread);       
 //!!          sensitive << msBLfifo->write_event;       
        // The specials can contain even analog units with scGridPoint behavior!
            Populate(Specials);    // Populate with scGridPoints
            CreateClusters();      // Clusterize the scGridPoints
            //ConnectIGPCBs();       // Connect direct scGridPoints to scGridPoints communication
//            CheckIGPCBs();
 //           ConnectClusterHeadsToBus(mClusterBus);  // Connect ONLY cluster heads to inter-cluster bus
            Reset();
    }
}

    scGatesProcessor::
~scGatesProcessor(void)
{
}
    // Comparator function to sort pairs
    // according to first value
    bool cmp(pair<string,scGridPoint*>& a,
             pair<string,scGridPoint*>& b)
    {
        return a.first < b.first;
    }
/*!
 *  This method starts up when all initializations finished
 */
void scGatesProcessor::Initialize_method(void)
{
//    wait(SCTIME_SV); // For setup
    DEBUG_PRINT("Initialize_method started");
 //    mscoreStatus = STAT_AOK; // Assume everything all-right
    // We surely have no processing unit and also the first instruction must be fetched

 //   EVENT_PROCESSOR.START.notify();
}

/*!
 * \fn void scProcessor::ConnectClusterHeadsToBus(scClusterBus* Bus)
 * \brief Connect cluster head modules to the inter-cluster Buses
 * 
 * The module assumes that the clusters of the scProcessor has already been set up
 * (i.e. must be called after  AbstractTopology::CreateClusters() ).
 * The clusters (and the memories) change messages to each other through the scClusterBus.
 * All modules connected to the scClusterBus have both slave and master interfaces.
 * All but phantom cluster heads are connected; the same topology is assumed
 * as in  AbstractTopology::CreateClusters().
 */ 


    void scGatesProcessor::
Reset(void)
{


}

  /*!
   * \brief scProcessor::SetMasks_thread
   * This method is called when any of the masks relating operation changes
   * scRegistered method, sensitive to EVENT_SCPROCESSOR.MasksChanged
   */
    void scGatesProcessor::
SetMasks_method(void)
{
        for(SC_HTHREAD_ID_TYPE h=0; h<MAX_HTHREADS; h++)
        {
    ostringstream oss;
    /*oss << hex << "Unavailable 0x" << UnavailableMask_Get(h)
        << "; Allocated 0x" << msSignalMask[h].Allocated
        << "; PreAllocated 0x" << msSignalMask[h].PreAllocated
        << "; Denied 0x" <<  msSignalMask[h].Denied
        << dec <<"\n";
    DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.MasksChanged: " << oss.str());
*/    msSignal[h].Denied.write(msSignalMask[h].Denied);
    msSignal[h].Allocated.write(msSignalMask[h].Allocated);
    msSignal[h].PreAllocated.write(msSignalMask[h].PreAllocated);
    }
}



#if 0
/*!
 * \brief scProcessor::START_thread
 * This thread is waiting for a START_event and processes it
 */
    void scGatesProcessor::
START_thread(void)
{
  DEBUG_PRINT_OBJECT("EVENT_PROCESSOR.START_thread started@scProcessor");
   while(true)
    {
    DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.START");



    wait(EVENT_PROCESSOR.START);
//    wait(15,SC_NS);
    DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.START");
    msHalted = false;
    wait(SCTIME_CLOCKTIME);
    Reboot();
    }
}
#endif // 0


    /*!
     * \brief scProcessor::Populate
     * \param Specials pre-created, subclassed from scGridPoint, points
     *
     *
     */
 void scGatesProcessor::
Populate(vector<scGridPoint*>& Specials)
{

     // Define the logic gates here
     // In this example, only the needed five gates are populated, see scProcessor for populating all gridpoints

     // Create communication grid point objects
/*    scGridPoint* C;
    for(int i = 0; i < GRID_SIZE_X; i++, i++) // Twice because of the hexagonal grid
    {
        for(int j = 0; j < GRID_SIZE_Y; j++)
        {
            int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
                if(!mGrid.at(i).at(j))
                {// This is pre-populated by a special
                    C = new scGridPoint(sc_core::sc_module_name(string("GP").append(IDtoString(LinearAddress1,GRID_BUS_WIDTH)).c_str()),
                        this,// LinearAddress1,
                        GridPoint(i,j), true//StandAlone
                    );
                    mGrid.at(i).at(j)= C; // Store the pointer in the grid
                }
                int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
                if(!mGrid.at(i+1).at(j))
                {// This is pre-populated by a special
                    C = new scGridPoint(sc_core::sc_module_name(string("GP").append(IDtoString(LinearAddress3,GRID_BUS_WIDTH)).c_str()),
                        this, //LinearAddress3,
                        GridPoint(i+1,j), true//StandAlone
                    );
                    mGrid.at(i+1).at(j) = C; // Store the pointer in the shadow-column
                }
        }
    }
    */
 }


 //  bool HThreadCompare (scHThread* i,scHThread* j) { return ((uint64_t)i<(uint64_t)j); }

   void scGatesProcessor::
Child_Insert(scGridPoint* C)
{
   assert(C);   // Cannot be inserted
   mChildren.push_back(C);
   sort(mChildren.begin(), mChildren.end());   // Keep the children sorted
}

   void scGatesProcessor::
Child_Remove(scGridPoint* C)
{
  assert(C);  // Cannot be removed
  uint MySize = mChildren.size()-1;
  std::remove (mChildren.begin(), mChildren.end(), C);
  mChildren.resize(MySize);
  mChildren.shrink_to_fit();   // Keep the list compact
}
