/** @file scTimedFIFO.cpp
 *  @brief Function prototypes for the EMPA simulator, scFIFO
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scTimedFIFO.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
//#define DEBUG_PENDING_OPERATIONS // Print pending, queued, executing, executed operations
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

// This file defines the FIFOs used in EMPA data communication

extern bool UNIT_TESTING;
using namespace std;

  scTimedIGPMessage_fifo::scTimedIGPMessage_fifo(const sc_module_name& name)
      : sc_core::sc_channel(name), num_elements(0), first(0)
{
   DEBUG_PRINT(sc_time_stamp() << '|' << name << " Created ");
   for(int i = 0; i<num_elements; i++) data[i].Time = SC_ZERO_TIME;
}

  /// write a new inter-gridpoint message to the FIFO
    void scTimedIGPMessage_fifo::
  write(TimedIGPMessage_type T) {
    if (num_elements == max)
             wait(TimedIGPMessage_fifo_read_event);
    dowrite(T);
    TimedIGPMessage_fifo_write_event.notify(SC_ZERO_TIME);
  }

    /// write a morphing command to the FIFO
    void scTimedIGPMessage_fifo::
  dowrite(TimedIGPMessage_type T) {
    int i=0; int index;
    while(i<num_elements)
    { // Check all elements
        index = (first+i) % max;  // This is the next position in the FIFO
        if(NewTimeLower(T,data[index]))
        break;// Insert here
         i++;
    }
    index = (first+i) % max;  // This is the position of inserting in the FIFO
    // Anyhow, we must insert new item here
//    DEBUG_PRINT("Inserting event " << meta_table[T.Code].name << " @" << index);
  // Maybe we must shift the queue elements
    if(i<num_elements)
    {// We ned to shift the elements above this position
      int j = num_elements;
      while( j>0)
        {
          data[(first + j) % max] = data[(first + j-1) % max];
          DEBUG_PRINT("Shifted (" << ((first + j) % max) << ") <= (" << ((first + j-1) % max) << ')');
          j--;
        }
    }
    data[index] = T; // Anyhow, insert the new item here
    ++ num_elements;
/*    DEBUG_PRINT_SC(" Metainstruction " << T.Code << " (" <<  meta_table[T.Code].name << ") written @" << (index)  << " from core#" << T.CoreID);
    DEBUG_PRINT_SC(" WQueue is: [" << first << " of " << num_elements << "] "
                   << data[0].Code << ',' << data[1].Code << ',' << data[2].Code << ',' << data[3].Code << ','<< data[4].Code << ',' << data[5].Code << ','
                                      << data[6].Code << ',' << data[7].Code << ',' << data[8].Code << ','<< data[9].Code );
*/    }

    /*!
     * This function defines in which order metainstructions are executed
     * \param New the instruction to be inserted
     * \param Old the old instruction to compare its priority to
     * \return true if New is to be inserted before Old
     */
    bool scTimedIGPMessage_fifo::
  NewTimeLower(TimedIGPMessage_type New, TimedIGPMessage_type Old)
  {
        return New.Time < Old.Time;
  }
/// Read a metainstruction from the FIFO (blocking)
    void scTimedIGPMessage_fifo::
  read(TimedIGPMessage_type &T){
    if (num_elements == 0)
       wait(TimedIGPMessage_fifo_write_event);
    doread(T);
    TimedIGPMessage_fifo_read_event.notify(SC_ZERO_TIME);
  }

    void scTimedIGPMessage_fifo::
  doread(TimedIGPMessage_type &T){
    T = data[first];
    -- num_elements;
/*    DEBUG_PRINT_SC(" Metainstruction " << T.Code << " (" <<  meta_table[T.Code].name << ") read @" << first  << " from core#" << T.CoreID);
    DEBUG_PRINT_SC(" RQueue is: [" << first << " of " << num_elements << "] "
                   << data[0].Code << ',' << data[1].Code << ',' << data[2].Code << ',' << data[3].Code << ','<< data[4].Code << ',' << data[5].Code << ','
                                      << data[6].Code << ',' << data[7].Code << ',' << data[8].Code << ','<< data[9].Code );
*/    first = (first + 1) % max;
  }
