/** @file scClusterBusMemorySlow.cpp
 *  @brief Function prototypes for the EMPA simulator, core
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "scGridPoint.h"
#include "scClusterBusMemorySlow.h"
//#include "scClusterBus.h"
#include <locale>         // std::locale, std::tolower
#include "scProcessor.h"
/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
//#define MAKE_LOG_TRACING    // We want to have trace messages
//#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing

extern bool IsMemory(ClusterAddress_t CA);

/*!
 * \brief scClusterBusMemorySlow::scClusterBusMemorySlow
 * \param name_ SystemC module name
 * \param start_address First valid address
 * \param end_address Last valid address
 * \param nr_wait_states
 */
scClusterBusMemorySlow::scClusterBusMemorySlow(sc_module_name name_
            , unsigned int start_address
            , unsigned int end_address
            , unsigned int nr_wait_states)
  : sc_module(name_)
  , m_start_address(start_address)
  , m_end_address(end_address)
  , m_nr_wait_states(nr_wait_states)
  , m_wait_count(-1)
{
  // process declaration
/* ?? DO NOT USE CLOCK
 *   SC_METHOD(wait_loop);
  dont_initialize();

  sensitive << clock.pos();
  */
  sc_assert(m_start_address <= m_end_address);
  sc_assert((m_end_address-m_start_address+1)%4 == 0);
  m_size = (m_end_address-m_start_address+1)/4;
  Reset();
/*  mMaster_if = new scClusterBusMaster_blocking(string("CHM2").c_str(),
                                               2, //mPriority,
                                               0, //Address, // This will probably be not used
                                              true,    // Need to lock the bus for Burst_Write
                                              1);   // Add one more cycle for timeout
  mMaster_if->clock(clock);
  mMaster_if->bus_port(*scClusterBus); // Connect to bus as master
*/
  MemoryMessagefifo = new scMemoryMessageFIFO("F_FIFO");
  SC_THREAD(ProcessFIFOMessages);
  sensitive << MemoryMessagefifo->scMemoryMessageFIFO_write_event;
}


    void scClusterBusMemorySlow::
Reset(void)
{//   program_counter = MEMORY_DEFAULT_ADDRESS;
    MEM = new int [m_size];
    for (unsigned int i = 0; i < m_size; ++i)
        MEM[i] = 0;
    mem = (uint8_t*) MEM;    // Just a quick hack to enable Y86 files to load
}

inline ClusterBusStatus scClusterBusMemorySlow::
write(scIGPMessage* M)
{
//    scClusterBusSlave_if* IF = Processor_Get()->ClusterBus_Get()->get_slave(M);
//    IF->write(M);
    // Now imitate messaging via the bus
    scGridPoint* GP = Processor_Get()->ClusterBus_Get()->get_slave_GP(M);
    GP->ReceiveClusterMessage(M);
    return CLUSTER_BUS_WAIT;
}



// Message handling
// As the FIFO is blocking, no signaling needed
    void scClusterBusMemorySlow::
ProcessFIFOMessages(void)
{
    while(true)
    {   // Read next message from the FIFO
        scMemoryMessageFIFO_t *F;
        MemoryMessagefifo->read(F);
        scIGPMessage* Message = F->ReplyMessage;
/*        ClusterAddress_t CA = Message->DestinationAddress_Get();
        scGridPoint* GP;
        if(IsMemory(CA))
        {   // The memory is the destination
            GP = Processor_Get()->ByClusterAddress_Get(Message->ReplyAddress_Get());
        }
        else
        {   // The memory is the source
            GP = Processor_Get()->ByClusterAddress_Get(CA);
        }

//        DEBUG_CONTRIBUTE_STRING(DebugRoute,"deQUEUED message ",GP->StringOfMessage_Get(Message).append("\n"));
*/
//!!         MyTime = F->ReplyTime - sc_core::sc_time_stamp();
        sc_core::sc_time MyTime = FMEMORY_READ_TIME;
        wait(MyTime);
        ProcessMessage(Message);  // Now the message will be executed in the right time
    }
}

    /*!
     * \fn scClusterBusMemorySlow::CreateReadMessageReply
     * \brief The reply of memory to the former 'CreateReadMessage' request from an scGridPoint
     * \param M The message will be sent to this scGridPoint
     * \return The filled-out message form
     */
    scIGPMessage* scClusterBusMemorySlow::
CreateReadMessageReply(scMemoryMessageFIFO_t *M)
{
        // We have a read-request in M, re-use it; do not create
    scIGPMessage *MyMessage = M->ReplyMessage;
        // Handle the address of the target core
    ClusterAddress_t CA = MyMessage->ReplyToAddress_Get();
    MyMessage->ReplyToAddress_Set(MemoryClusterAddress);
    MyMessage->DestinationAddress_Set(CA);
//    static_assert(sizeof(MyMessage->DestinationAddress_Get() ) <= sizeof(CA), "!");
    // Handle message features
    MessageFeatures_t MsgType = MyMessage->FeatureWord_Get();
//    memcpy(&MsgType, &MyMessage->mBuffer[1], sizeof(MsgType)); // Create the type
    MsgType.Length = MsgType.ReplyLength+4;
    MsgType.Type = msg_Mem;
//    static_assert(sizeof(MyMessage->mBuffer[1]) <= sizeof(MsgType), "!");
    MyMessage->FeatureWord_Set(MsgType);
//    memcpy(&MyMessage->mBuffer[1], &MsgType, sizeof(MsgType)); // Create the type
    // Handle reply contents, the memory contents
    int32_t Address = MyMessage->Header3Address_Get();
//    memcpy(&Address, &MyMessage->mBuffer[3], sizeof(Address)); // Create the type
    // Now the heading is filled out properly
    for(int i = 4; i<MsgType.Length; i++)
    {
        int32_t Data;
        direct_read(&Data, Address++);
        MyMessage->BufferContent_Set(i-4,Data);
//        memcpy(&MyMessage->mBuffer[i], &Data, sizeof(Data));   // Store memory content(s)
    }
    // Now the message is ready
    return MyMessage;
}

    /*!
     * \fn scClusterBusMemorySlow::CreateWriteMessage
     * \brief The message 'CreateWriteMessage' request from an scGridPoint to write the memory
     * \param From The message received from this scGridPoint
     * \param Address The beginning of the memory block (4-aligned value)
     * \param Length The length of the memory block
     * \return The filled-out message form
     */
    scIGPMessage* scClusterBusMemorySlow::
CreateWriteMessage(scGridPoint* From, unsigned int Address, int Length)
{
    assert(From);   // Must be real scGridPoint
    scIGPMessage *MyMessage = new scIGPMessage;
    // Handle the address of the target core
    ClusterAddress_t CA = From->ClusterAddress_Get();
//    static_assert(sizeof(MyMessage->mBuffer[2]) <= sizeof(CA), "!");
    MyMessage->ReplyToAddress_Set(CA);
//    memcpy(&MyMessage->mBuffer[2], &CA, sizeof(CA));
    // Handle message features
    MessageFeatures_t MsgType;
    MsgType.Length = Length+4;
    MsgType.Type = msg_Mem;
    MsgType.Time = 0;
//    static_assert(sizeof(MyMessage->mBuffer[1]) <= sizeof(MsgType), "!");
//    memcpy(&MyMessage->mBuffer[1], &MsgType, sizeof(MsgType)); // Create the type
    MyMessage->FeatureWord_Set(MsgType);
    // Handle destination address
//    ClusterAddress_t MyCA = MemoryClusterAddress;
//    static_assert(sizeof(MyMessage->mBuffer[2]) <= sizeof(MyCA), "!");
    MyMessage->DestinationAddress_Set(CA);
//    memcpy(&MyMessage->mBuffer[0], &MyCA, sizeof(CA));   // Store reply address
//    MyMessage->Header3Address_Get(&Address);
//    memcpy(&MyMessage->mBuffer[3], &Address, sizeof(Address));   // Store data address
    for(int i = 4; i<Length; i++)
    {
        int32_t Data;
        direct_read(&Data, Address++);
        MyMessage->BufferContent_Set(i-4,Data);
 //       memcpy(&MyMessage->mBuffer[i], &Data, sizeof(Data));   // Store memory content(s)
    }
    // Now the message is ready
    return MyMessage;
}

    /*!
     * \brief scClusterBusMemorySlow::ReceiveClusterMessage
     * \param Message the message received
     *
     * Receiving the message through the bus is presently imitated by
     * directly calling this routine by the sender
     */
    void scClusterBusMemorySlow::
ReceiveClusterMessage(scIGPMessage* Message)
{
    ClusterAddress_t CA = Message->ReplyToAddress_Get();
    scGridPoint* Source = Processor_Get()->ByClusterAddress_Get(CA);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"RECEIVED ",Source->StringOfMessage_Get(Message).append("\n"));
    scMemoryMessageFIFO_t* F = new scMemoryMessageFIFO_t;
    F->ReplyMessage = Message;
    F->ReplyTime = sc_core::sc_time_stamp() + FMEMORY_READ_TIME;
    MemoryMessagefifo->write(F);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"QUEUED ",Source->StringOfMessage_Get(Message).append("\n"));
    // Now the message is in the memory FIFO and will appear at the memory access time later
 }

    /*!
     * \brief scClusterBusMemorySlow::SendClusterMessage
     * \param Message
     *
     * Sending the message is recently imitated by directly
     * calling the receiver
     */
    void scClusterBusMemorySlow::
SendClusterMessage(scIGPMessage* Message)
{
    ClusterAddress_t CA = Message->DestinationAddress_Get();
    scGridPoint* Dest = Processor_Get()->ByClusterAddress_Get(CA);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"SENT CLUSTER ",Dest->StringOfMessage_Get(Message).append("\n"));
//    write(Message); // Imitatate writing through the bus
    scGridPoint* GP = Processor_Get()->ClusterBus_Get()->get_slave_GP(Message);
    GP->ReceiveClusterMessage(Message);
}

    /*!
    * \fn scClusterBusMemorySlow::ProcessMessage
    * \param M The message to process
    *
    * \brief     void scClusterBusMemorySlow::ProcessMessage
    */
   void scClusterBusMemorySlow::
ProcessMessage(scIGPMessage* M)
{
    assert(M);  // Message is valid?
    // The message is self-contained, get addresses
    ClusterAddress_t DestinationCA = M->DestinationAddress_Get();
    sc_core::sc_time ReceiveTime = sc_core::sc_time_stamp(); // Remember when the message was received
    // Check is the destination is the "far" memory
    if(IsMemory(DestinationCA))
    {   // The destination is the "far" memory, either for reading or writing
        //    DEBUG_PRINT_OBJECT_MESSAGE("DELIVERED 'M'",M);
        scGridPoint* Source = Processor_Get()->ByClusterAddress_Get(M->ReplyToAddress_Get());
        DEBUG_CONTRIBUTE_STRING(DebugRoute,"DELIVERED 'M' ",Source->StringOfMessage_Get(M).append("\n"));
        unsigned int Address = M->Header3Address_Get();
//        memcpy(&Address, &M->mBuffer[3], sizeof(Address));   // Get memory address
        unsigned int Length = M->Length_Get();
        if(Length==4)
        {   // It is a 'read request', no useful data to write into memory
            // Just remember the source (this will be the destination of the reply)
            // The time of the reply
            // and the length of memory region
            scIGPMessage *MM = new scIGPMessage;
            MM->DestinationAddress_Set(M->ReplyToAddress_Get());  // We are sending back to the requestor
            MM->ReplyToAddress_Set(M->DestinationAddress_Get());    // Add our signature
            MessageFeatures_t MsgFeatures = M->FeatureWord_Get();
            MsgFeatures.Length = MsgFeatures.ReplyLength + MsgFeatures.Length;   // How many words to read
            MM->FeatureWord_Set(MsgFeatures);
            MM->Header3Address_Set(Address);
//            memcpy(&MM->mBuffer[3], &M->mBuffer[3], sizeof(M->mBuffer[3]));   // Store memory data address
            ReceiveTime = ReceiveTime + FMEMORY_READ_TIME;
            // The actual reading will take place when this request is read from the queue
            scMemoryMessageFIFO_t *F = new scMemoryMessageFIFO_t;
            F->ReplyTime = ReceiveTime;
            F->ReplyMessage = MM;
            MemoryMessagefifo->write(F);
            delete M;   // Message processed; a new message will be sent by memory
        }
        else
        {   // the data in the message are to be written in the memory, starting at 'Address'
            for(unsigned int i = 4; i<Length; i++)
            {
                int32_t Data = M->BufferContent_Get(i-4);
//                memcpy(&Data, &M->mBuffer[i], sizeof(Data));   // Write data into memory
                direct_write(&Data, Address++);
            }
         }
    }
    else
    { // This is the reply from the memory, to a former 'read' request
        // i.e. the destination is one of the cluster heads

        scGridPoint* GP = Processor_Get()->ClusterBus_Get()->get_slave_GP(M);
        unsigned int Address = M->Header3Address_Get();
//        memcpy(&Address, &M->mBuffer[3], sizeof(Address));   // Get memory address
        unsigned int Length = M->Length_Get();
        for(unsigned int i = 4; i<Length; i++)
        {
            int32_t Data;
            direct_read(&Data, Address++);
            M->BufferContent_Set(i-4,Data);
//            memcpy(&M->mBuffer[i] ,&Data, sizeof(Data));   // Write data into memory
        }

        DEBUG_CONTRIBUTE_STRING(DebugRoute,"SENT CLUSTER ",GP->StringOfMessage_Get(M).append("\n"));
        GP->ReceiveClusterMessage(M);
       // Send out to the inter-cluster bus. Someone will receive and process it
    }
}

   uint32_t scClusterBusMemorySlow::// Just for testing
DWord_Get(uint32_t Address){
       assert(!(Address%4));
       LOG_HEXA("Reading from " , Address);
       LOG_HEXA(" the value ", MEM[Address/4]);

       return MEM[Address/4];}
   void scClusterBusMemorySlow::
DWord_Set(uint32_t Address, uint32_t C){

       assert(!(Address%4));
       LOG_HEXA("Writing to " , Address);
       LOG_HEXA(" the value ", C);
       MEM[Address/4] = C;
   }


   string scClusterBusMemorySlow::
PrologText_Get(void)
{
   ostringstream oss;
   string SG = "Mem2";
   oss  << '@' << StringOfTime_Get()  <<": " << SG.c_str();
    oss << '%';
//    while(oss.str().size()<24) oss << ' ';
   return oss.str();
}

   /*!
    * \fn scGridPoint::StringOfClusterAddress_Get
    * \return the string form of the complete cluster address of the gridpoint
    */
   string scClusterBusMemorySlow::
StringOfClusterAddress_Get(void)
{   ostringstream oss;
   #if USE_MODULE_NAMES
       oss << name();
   #else
       oss << "Mem2";
   #endif // USE_MODULE_NAMES
   return oss.str();
}

     // Return line number corresponding to A
     //http://www.cplusplus.com/reference/algorithm/lower_bound/
     //http://stackoverflow.com/questions/40642386/how-to-prevent-an-iterator-to-go-out-of-range/40644696#40644696
     /**
      * @todo rewrite with binary_search
      */
       int scClusterBusMemorySlow::
findLinenoToAddress(SC_ADDRESS_TYPE A)
     {
         if (AddressToLineMap.empty())
           return 0; // We have no source file
         int B;
       int L = AddressToLineMap.size()-1; // This is the total number of source lines
       while((unsigned int)(B = AddressToLineMap[--L]->Address) == (unsigned int) -1 );
       // Now L contains the last line where content is
       if(A>AddressToLineMap[L]->Address) return L+1;
       // The normal case: look for an address in the intermediate region
       std::vector<AddressToLineRecord*>::const_iterator it;
       for(it = AddressToLineMap.begin(); it != AddressToLineMap.end(); it+=1)
         {
           if((*it)->Address == (unsigned int) -1) continue;
           if((*it)->Address >= A) break;
         }
       return (A==(*it)->Address) ? (*it)->Lineno : (*it)->Lineno-1;
     }

       /*!
        * \brief findAddressToLineno Return address corresponding to lineno
        * \param L the line number of the address searched
        * \return the found memory addressof
        *
        * If L is too large, the address of the last line is returned
        * @todo rewrite with binary_search
        */
    SC_ADDRESS_TYPE scClusterBusMemorySlow::
findAddressToLineno(int32_t L)
{
       if((unsigned int)L>=AddressToLineMap.size())
       { // Looking for after the end of the source code
         int index = 0; unsigned int Last;
         do{
            Last = AddressToLineMap[AddressToLineMap.size()- ++index]->Address;
         }while(Last == (unsigned int) -1);
         return Last;
       }
       // We are looking for a legal address
       std::vector<AddressToLineRecord*>::const_iterator it;
       for(it = AddressToLineMap.begin(); it != AddressToLineMap.end(); it++)
       {
         if((*it)->Address == (unsigned int) -1) continue;
         if((*it)->Lineno >= L) break;
       }
       return (*it)->Address;
}


    string scClusterBusMemorySlow::
getSourceLine(int L){
           string MyString;
           if(!L) return MyString;
            int MyAddress; --L;
           do
           {
             MyAddress = AddressToLineMap[L++]->Address;
           }
           while((MyAddress==-1) && ((unsigned int)L < AddressToLineMap.size()));
           MyString = AddressToLineMap[L-1]->Line;
           return MyString;

         }
