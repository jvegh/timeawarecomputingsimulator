/** @file scClusterBusMaster_blocking.cpp
 *  @brief The blocking mode master of the inter-cluster bus
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

/* 
  The origin of this development file is available as SystemC example
  Changing the names is a must, because multiple buses are present in the development
 
  simple_bus_master_blocking.cpp : The master using the blocking BUS interface.
 
  Original Author: Ric Hilderink, Synopsys, Inc., 2001-10-11
 
 *****************************************************************************/

#include "scClusterBusMaster_blocking.h"
#include "scIGPMessage.h"

void scClusterBusMaster_blocking::main_action()
{
/*  const unsigned int mylength = 0x10; // storage capacity/burst length in words
  int mydata[mylength];*/
  scIGPMessage MyM;
//  unsigned int i;
//!!  ClusterBusStatus status; // Will be used in the real bus

  while (true)
    {
      wait(); // ... for the next rising clock edge
#if 0
      status = bus_port->burst_read(m_unique_priority, /*mydata,
                    m_address, mylength,*/&MyM,  m_lock);
      if (status == CLUSTER_BUS_ERROR)
	sb_fprintf(stdout, "%s %s : blocking-read failed at address %x\n",
		   sc_time_stamp().to_string().c_str(), name(), m_address);

/*      for (i = 0; i < mylength; ++i)
	{
	  mydata[i] += i;
	  wait();
	}
*/
      status = bus_port->burst_write(m_unique_priority, /*mydata,
                     m_address, mylength,*/ &MyM, m_lock);
      if (status == CLUSTER_BUS_ERROR)
/*	sb_fprintf(stdout, "%s %s : blocking-write failed at address %x\n",
           sc_time_stamp().to_string().c_str(), name(), m_address);*/
#endif
      wait(m_timeout, SC_NS);
    }
}
