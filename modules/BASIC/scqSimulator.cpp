/** @file scSimulator.cpp
 *  @brief Function prototypes for the  scProcessor simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scProcessor.h"

#include "Stuff.h"
#include "Project.h"
//#include "QStuff.h"
#include "QApplication"
#include <QMessageBox>
#include <string>
#include "scGridPoint.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING
#define MAKE_TIME_BENCHMARKING
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scqSimulator.h"

//QTextStream* s_LogStream;  // The text strean of log
class scClusterBusMemorySlow;
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern std::string ListOfIniFiles;
scClusterBusMemorySlow *MainMemory;
//EMPAPlotter *Plot;
//extern reg_struct reg_table[R_ERR+1];
QDir dir;
// This is the logging facility provided by Qt.
//
// http://www.qtcentre.org/threads/19534-redirect-qDebug()-to-file
//
// qDebug is disabled in release mode
// http://beyondrelational.com/modules/2/blogs/79/Posts/19245/how-to-put-logging-in-c-application-using-qt50.aspx
SystemDirectories SDirectories;
SystemDirectories* SystemDirectories_Get(void){return &SDirectories;}

QTextEdit * scqSimulator::s_LogWindow = 0;
 void
MyMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
     string LogType;
     QFile outfile(SystemDirectories_Get()->LogFile.c_str());
     QString SourceFile = context.file; int spos = SourceFile.lastIndexOf('/')+1;
     SourceFile.remove(0,spos);
     // Prepare message prolog
     switch (type) {
        case QtDebugMsg:
            LogType += "D> ";
            LogType += SourceFile.toStdString() + ':' + QString::number(context.line).toStdString() + "> " + msg.toStdString();
            break;
        case QtFatalMsg:
            LogType += "Fatal: " + SourceFile.toStdString() + ':' + QString::number(context.line).toStdString() + "> ";
        case QtCriticalMsg:
            LogType += "**CRITICAL> ";
        case QtWarningMsg:
            LogType += "W>  ";
        case QtInfoMsg:
//        LogType += "I> ";
         LogType += msg.toStdString();
        break;
    }
    LogType += " @" + QTime::currentTime().toString("hh:mm:ss\n").toStdString();
    if(scqSimulator::s_LogWindow)
    {  // We have a redirection to the message window
       scqSimulator::s_LogWindow->append((LogType + msg.toStdString()).c_str());
    }
    else
    {
        // Else, write to std::cerr
        std::cerr << LogType.c_str(); // Anyhow: write to std::cerr

    }
    // Anyhow, write to the log file
    if(outfile.open(QIODevice::WriteOnly | QIODevice::Append))
    {   // We have an open log file
        QTextStream ts(&outfile);
        ts << QString(LogType.c_str());
        ts.flush();
    }
}

/*!
 * @brief	Creates  @ref scqSimulator, with scModules
 *
 * @param[in] nm The name of the simulator
 * @param[in] argc As in main()
 * @param[in] argv As in main()
 * 
 *  @verbatim
 *  |scqSimulator
 *  |../scSimulator
 *  |../ProcessorSimulator
 *  |..../XXProcessorSimulator
 *  |..../NeurerSimulator
 * @endverbatim
 */
 // https://stackoverflow.com/questions/22485208/redirect-qdebug-to-qtextedit

scqSimulator::scqSimulator(sc_core::sc_module_name nm,int argc, char* argv[])
    : sc_core::sc_module( nm )
    ,msStepwiseMode(false)  // Initially, do not step
    ,msSuspended(false) // Initially not suspended
{
    s_LogWindow = 0; // Signals that no GUI message handler
    simulation_name =  string(PROJECT_NAME) + "_" + string(nm) + " V" + string(PROJECT_VERSION) + " CLI";
    // The benchmark is reset, and timing started
    BENCHMARK_TIME_RESET(&m_running_time,&m_part_time,&m_sum_time);
    BENCHMARK_TIME_BEGIN(&m_running_time,&m_part_time);
    // We must install message handler before QApplication
    qInstallMessageHandler(MyMessageHandler);// Take care, log file name is not yet set
    QApplication MainApp(argc, argv);
    SetupSystemDirectories(NULL); // Establish system and user directories, with no main window
    sc_set_time_resolution(SCTIME_RESOLUTION);
    // The setup part finished
    DEBUG_PRINT(simulation_name << " scqSimulator inited in "
              << SimulationPartTime_Get()/1000/1000 << " msecs");

    //Do whatever setup here you will need for your tests here
    //!! all SC-related object and connections must be established befor calling sc_start
/*
    if(StandAlone)
    {
        // Now specials can be loaded with special points; will be considered by the topology
        readSettings();     /// Read the setting files at different levels

        HandleSpecials();   /// Populate mSpecials with the found special scNeurers (from mSpecial)
 //!!  handle optional input file     MainMemory = new scMemory("Main_Memory", filename);
        msProcessor = new scProcessor("Proc", mSpecials, true);
        msProcessor->clock(C1);
//        MainMemory->Y86loadHexFile(filename);
        SC_THREAD(SInitialize_method);
    }
    // As long as nothing more than sending out event in the derived simulator class
    SC_THREAD(SSUSPEND_thread);
        sensitive << EVENT_SIMULATOR.SUSPEND;
    SC_THREAD(SRESUME_thread);
        sensitive << EVENT_SIMULATOR.RESUME;
    SC_THREAD(SSTART_thread);
        sensitive << EVENT_SIMULATOR.START;
    SC_THREAD(SSTOP_thread);
        sensitive << EVENT_SIMULATOR.STOP;
    SC_THREAD(SHALT_thread);
        sensitive << EVENT_SIMULATOR.HALT;
    SC_METHOD(SInitialize_method);
            sensitive << EVENT_SIMULATOR.HALT;
*/
//    Plot = new EMPAPlotter(msProcessor, FileNameRoot.c_str());
 }
scqSimulator::~scqSimulator()
{
    writeSettings();
//    BENCHMARK_TIME_END(&m_running_time,&m_part_time,&m_sum_time);
    // Return here when no more events remained
    if (not sc_end_of_simulation_invoked()) sc_stop(); //< invoke end_of_simulation
    DEBUG_PRINT(simulation_name << SimulationSumTime_Get()/1000/1000 << " msecs");

//    PrintFinalReport(msProcessor);

//    if(Plot){ delete Plot; Plot = 0;}
}
// Return operating time since the last reading
    int64_t scqSimulator::
SimulationPartTime_Get(void){
    BENCHMARK_TIME_END(&m_running_time,&m_part_time,&m_sum_time);
    return m_part_time.count();
}    ///< return actual benchmark duration

// Return operating time since the last reset
    int64_t scqSimulator::
SimulationSumTime_Get(void){
    BENCHMARK_TIME_END(&m_running_time,&m_part_time,&m_sum_time);
    return m_sum_time.count();
}    ///< return actual benchmark duration

    string scqSimulator::
PrologString_Get(void)
{
    ostringstream oss;
    string SG = " Sim";
    oss  << '@' << StringOfTime_Get()  <<": " << SG.c_str();
     oss << '%';
//    while(oss.str().size()<24) oss << ' ';
    return oss.str();
}


#if 0
/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
void scqSimulator::
SInitialize_method(void)
{
  DEBUG_PRINT_OBJECT("Initialize_method started" );
  Processor_Get()->EVENT_PROCESSOR.START.notify();
}

 
 /*               vector <int32_t> in,out;
                GP->Inspect(in,out);
                GP->InspectSwitch(true);
                GP->Inspect(in,out);*/


/*!
 * \brief scSimulator::SSTART_thread
 * This thread is waiting for a START_event and processes it
 */
    void scSimulator::
SSTART_thread(void)
{
    DEBUG_PRINT_OBJECT("SSTOP_thread started");
    while(true)
    {
        DEBUG_EVENT("WAIT EVENT_SIMULATOR.START");
        wait(EVENT_SIMULATOR.START);
        DEBUG_EVENT("EVENT_SIMULATOR.START");
        if(!Processor_Get()->Halted_Get())
        {
         LOG_INFO("Attempted to start a running processor");
        }
        else
        {
            Processor_Get()->EVENT_PROCESSOR.START.notify();
        }
    }

}

    /*!
     * \fn scSimulator::SSTOP_thread
     * This thread is waiting for a STOP_event and processes it
     */
        void scSimulator::
    SSTOP_thread(void)
    {
 //      DEBUG_PRINT_OBJECT("SSTOP_thread started");
        while(true)
        {
            wait(EVENT_SIMULATOR.STOP);
            DEBUG_EVENT("RCVD EVENT_SIMULATOR.STOP");
            if(Processor_Get()->Halted_Get())
            {
              LOG_INFO("Attempted to stop at a running processor");
            }
            else
            {
              Processor_Get()->EVENT_PROCESSOR.STOP.notify();
            }
        }

    }
/*!
 * \brief scSimulator::HALT_thread
 * This thread is waiting for a HALT_event and processes it
 */
    void scSimulator::
SHALT_thread(void)
{
   DEBUG_PRINT("SHALT_thread started");
    while(true)
    {
        wait(EVENT_SIMULATOR.HALT);
        DEBUG_EVENT("RCVD EVENT_SIMULATOR.HALT");
        if(Processor_Get()->Halted_Get())
        {
            LOG_INFO( "Attempted to halt a halted processor");
        }
        else
        {
            if(!Processor_Get()->Halted_Get())
            {
                Processor_Get()->EVENT_PROCESSOR.HALT.notify();
                LOG_INFO( "Processor halted by the simulator");
            }
            LOG_INFO( "Simulation halted");
        }
    }
}

/*!
 * \brief scSimulator::HandleSpecials
 *
 * Different special-purpose scGrudPoints may have to be added to the grid,
 * they are handled here
 */
    void scSimulator::
HandleSpecials(void) // Belongs to the constructor
{
// Handle scGridPoints in mSpecial that need special handling
    size_t size = mSpecials.size();
    if(!size)   return; // No special points
}

/*

    Plot = new EMPAPlotter(msProcessor, FileNameRoot.c_str());
    */

    void scSimulator::
Setup(void)  // Belongs to the constructor
{
}
#endif //0

/*    string scqSimulator::
PrologText_Get(void)
{
    ostringstream oss;
    string SG = "Simulator";
    oss  << '@' << StringOfTime_Get()  <<": " << SG.c_str();
    oss << '%';
//    while(oss.str().size()<24) oss << ' ';
    return oss.str();
}
*/
    void scqSimulator::
readSettings()
{
    // Now read the list of .ini files that should be present
    QStringList SettingsList = QString(ListOfIniFiles.c_str()).split(QString(" "));
    for (int i = 0; i < SettingsList.size(); ++i)
    {
        QString MyGroup = SettingsList.at(i);
        DEBUG_PRINT(  "looking for files '" << MyGroup.toStdString().c_str() << ".ini'");
//        ReadTheThreeLevels(MyGroup);    // OK, attempt to read those ini files, at three levels
    }
//    BENCHMARK_TIME_END(&m_running_time,&m_part_time,&m_sum_time);
    DEBUG_PRINT(simulation_name << " settings read in "
              << SimulationPartTime_Get()/1000/1000 << " msecs");
}

    void scqSimulator::
writeSettings(){}

#if 0
/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
    /**
     * @brief Print a summary report at the end of processing
     *
     * Print a summary table, which contains statistics of operation
     * @return void
     */
    void scSimulator::PrintFinalReport(scProcessor *Proc)
    {
      if(!UNIT_TESTING)
      {
    //  Processor* Proc = Processor_Get();
    //  unsigned int tmemorycycles = 0;
      qInfo() << "\nSummary table";
      qInfo().noquote().nospace() << "Core(no)    " << "Instr " << "MemR  "  << "MemW  "  << " Tmemo " << "  Tftch " << "  Texec " << "  Twait  "  << " TTime " << "Payl% " << "Util%" ;

      // These will be used for summing
      uint64_t  tinstructions=0, // Executed instructions
              tmemoryreads, // Memory accesses
              tmemoryrites, // Memory accesses
              tdeepwaittime, // Waiting at low power
              tpayloadtime=0, // Payload % (tfecthtime+texextime)/ttotaltime)
              tutilizationtime=0; // utilization % (ttotaltime/simulationtime);
    sc_time tfetchtime = SC_ZERO_TIME; // Intruction fetch time
    sc_time texectime = SC_ZERO_TIME; // The total time to calculate
    sc_time twaittime = SC_ZERO_TIME; // Just waiting
    sc_time tmemtime = SC_ZERO_TIME; // Waiting for memory
    sc_time ttotaltime = SC_ZERO_TIME; // Total time being used

      int NoOfCores = Proc->NoOfCores_Get();
      int SimTime = sc_core::sc_time_stamp().value(); if(!SimTime) ++SimTime;
      int CoreUtilization[NUMBER_OF_CORES]; int counter = 0;
      for(int i = 0; i < NoOfCores; i++)
      {
        scCore* C = Proc->CoreByID_Get(i);
        if(C->InstructionsExecuted_Get() )//|| C->MetaIntructionsExecuted_Get())
//        if(C->FetchTime_Get()+C->ExecTime_Get())
        { // The core was active during this run
          counter++;
          tinstructions += C->InstructionsExecuted_Get();
          tmemoryreads += C->DataMemoryRead_Get(); // Memory accesses
          tmemoryrites += C->DataMemoryWrite_Get(); // Memory accesses
          tmemtime += C->MemoryTime_Get();
          tfetchtime += C->FetchTime_Get();
          texectime += C->ExecTime_Get();
          twaittime += C->WaitTime_Get();
          sc_time TotalTime = C->MemoryTime_Get() + C->FetchTime_Get() + C->ExecTime_Get() + C->WaitTime_Get();
 /*         tmemoryaccesses += C->MemoryCycles_Get();
          tdeepwaittime += C->DeepWaitTime_Get();
          int TotalTime = C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get();
          ttotaltime += TotalTime;
          int PayloadUtilization = (C->FetchTime_Get()+C->ExecTime_Get())*100/TotalTime + 0.49;
          tpayloadtime+= (C->FetchTime_Get()+C->ExecTime_Get());
     //     int LowEnergyUtilization = (C->DeepWaitTime_Get())*100/TotalTime + 0.49;
          int ThisUtilization = (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get())*100/(SimTime/1000) + 0.49;
          tutilizationtime += (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get());
          CoreUtilization[i] = ThisUtilization;
          */
          ostringstream LogInfo;
            LogInfo << "Core(" << std::setw(2) << std::setfill('0') << i << ")  "
          << std::setw(6) << setfill(' ')
          << std::setw(6) << C->InstructionsExecuted_Get() // No of instruction executed by this core
          << std::setw(6) << C->DataMemoryRead_Get() // No of times the memory accessed
          << std::setw(6) << C->DataMemoryWrite_Get() // No of times the memory accessed
          << std::setw(6) << sc_time_to_nsec(C->MemoryTime_Get(),2) // Time of memory access
          << std::setw(6) << sc_time_to_nsec(C->FetchTime_Get(),2) // Time of fetching
          << std::setw(6) << sc_time_to_nsec(C->ExecTime_Get(),2) // Time of executing
          << std::setw(6) << sc_time_to_nsec(C->WaitTime_Get(),2) // Time of waiting
          << std::setw(6) << sc_time_to_nsec(TotalTime,2) // Time of waiting
/*          << std::setw(6) << C->FetchTime_Get()
          << std::setw(6) << C->ExecTime_Get()
          << std::setw(6) << C->ControlTime_Get()
          << std::setw(6) << C->WaitTime_Get()
          << std::setw(6) << C->DeepWaitTime_Get()
          << std::setw(6) << TotalTime // Total time being used by this core
          << std::setw(6)  << PayloadUtilization // Utilization% (tfecthtime+texextime+tcontroltime)/tutilizationtime
          << std::setw(6)  << ThisUtilization // Payload % (tfecthtime+texextime)/ttotaltime)
*/              ;
          qInfo() << LogInfo.str().c_str();
    //      tutilizationtime += ThisUtilization;
    //      tpayloadtime += PayloadUtilization;
         }
      }
/*
 *       ostringstream LogInfo;
      if(counter)
      {
        LogInfo << "Total     "  << setfill(' ')
        << std::setw(6) << tinstructions
        << std::setw(6) << tmemoryaccesses
        << std::setw(6) << tfetchtime
        << std::setw(6) << texectime
        << std::setw(6) << tcontroltime
        << std::setw(6) << twaittime
        << std::setw(6) << tdeepwaittime
        << std::setw(6) << ttotaltime
        << std::setw(6) << (int)(tpayloadtime*100/ttotaltime+ 0.49)
        << std::setw(6) << (int)(tutilizationtime*100/(SimTime/1000)/counter + 0.49);
           ;

      LogInfo  << "\nSimTime   " << std::setw(6) << //QString("%1").arg
                                     (sc_core::sc_time_stamp().value()/1000);
      qInfo() << LogInfo.str().c_str() ;
      int Max = 0; int MyVal; double Avg = 0; double Weights = 0;
      for(int i=0; i<counter; i++)
      {
          MyVal = CoreUtilization[i];
          if(MyVal>Max) Max = MyVal;
          Avg += i*MyVal;
          Weights += MyVal;
      }
      Avg = Avg/Weights;
      int AvgInt = Avg * 100;
      double Norm = 50./Max; QString a("%1");
      qInfo().noquote() << "\nSpeed gain =" << QString::number(double(texectime+tfetchtime)/(SimTime/1000),'f',2);
      qInfo().noquote() << QString("Average core number = %1").arg(AvgInt/100.,5);
      qInfo().noquote() << "Core utilization frequency distribution\n";
      for(int i=0; i< counter; i++)
      {
          int j = CoreUtilization[i]*Norm;
          qInfo().noquote() << i << a.arg(CoreUtilization[i],j+1,10,QChar('>'));
      }
      qInfo().noquote() << "\n";

      plotfile << "$};\n";
      plotfile << "\\node[right,above] at ( 0.9 cm,.5cm) {";
      plotfile << "$U=" << tutilizationtime/counter << "\\%,~P=" << tpayloadtime/counter;
      plotfile << "\\% $};\n";
    }
    //  cerr << LogInfo.str();
    */
      } // UNIT_TESTING
    }
 #endif //other


    /*!
     * \brief SetupSystemDirectories
     * \param parent the parent widget (usually QMainWindow)
     *
     * It verifies whether the system directory structure exists
     * and sets it up if not
     *
     * @verbatim
     *  |InstallDir
     *  |--/bin
     *  |--/data
     *  |--/docs
     * @endverbatim
     *
     * If this directory does not exist, it is usually an installation error
     *
     * It is assumed to use a project-like directory structure. I.e., the user has a
     * per session subdirectory for the simulation sessions. It comprises 'data' subdirectory
     * for persistent data (furnished by the user) and 'output' subdirectory for the generated files
     * @verbatim
     *  |WorkDir
     *  |../data
     *  |../output
     * @endverbatim
     *
     * The work directory is normally the directory where the tool is invoked from.
     * If that directory is either the 'InstallDir/bin' or 'UserHome', (in order to avoid
     * polluting those directories) the tool creates a new subdirectory 'UserHome/PackageName'
     *
     * The tool needs also a temporary directory. It will be created in
     *  @verbatim
     *  |TempDir
     *  |--/PackageName
     *  @endverbatim
     * if the temporary directory is available for the user; otherwise in
     *  @verbatim
     *  |UserHome
     *  |--/PACKAGE_NAME
     *  |----/temp
     *  @endverbatim
     *
     * In addition, the application maintains a 'last state' information in
     * @verbatim
     *  |UserHome
     *  |--/.config
     *  |----/.PACKAGE_NAME
     *  |------/PROJECT_NAME-PROJECT_VERSION@.ini
     *  @endverbatim

     * Finally, the a similar 'project settings' is available in
     * @verbatim
     *  |WorkDirectory
     *  |---PROJECT_NAME-PROJECT_VERSION@.ini
     *  @endverbatim

     */
    void scqSimulator::
    SetupSystemDirectories(QWidget* parent)
    {
        bool TempDirCreated = false;
        SystemDirectories_Get()->Temp = dir.tempPath().toStdString();
        bool SystemTempFound = QDir(QString(SystemDirectories_Get()->Temp.c_str())).exists();
        string DirTempRoot = "/" + string(PROJECT_NAME)+"-"+string(PROJECT_VERSION);
        if(QDir(QString(SystemDirectories_Get()->Temp.c_str())).exists())
        { // Yes, system-wide temporary directory exists
            SystemDirectories_Get()->Temp.append( DirTempRoot);
            size_t found = DirTempRoot.find(" "); // Replace " " with "-" in file name
            if(found != (size_t)-1) DirTempRoot.replace(found,1,"-");

            // Now the .log file goes to the right place
            TempDirCreated = true;
            SystemDirectories_Get()->LogFile = SystemDirectories_Get()->Temp + QDate::currentDate().toString("_yyyy-MM-dd").toStdString() + QTime::currentTime().toString("_hh:mm.log").toStdString();
        }
        // Now handle user work directory
        bool WorkDirRelocated = false;

        SystemDirectories_Get()->Install = QCoreApplication::applicationDirPath().toStdString();

        bool WorkDirCreated = false;
        SystemDirectories_Get()->Home = dir.homePath().toStdString();

        // Work directory found or not needed
        SystemDirectories_Get()->Work = dir.currentPath().toStdString(); // Presuppose current directory as work
        string Work = SystemDirectories_Get()->Work;
        string Install = SystemDirectories_Get()->Install;
        string Home = SystemDirectories_Get()->Home ;
        if(!SystemDirectories_Get()->Home.compare(SystemDirectories_Get()->Work) || !SystemDirectories_Get()->Install.compare(SystemDirectories_Get()->Work))
        {
            SystemDirectories_Get()->Work = SystemDirectories_Get()->Home + DirTempRoot;
            WorkDirRelocated = true;
            if(!QDir(SystemDirectories_Get()->Work.c_str()).exists())
             { // No such directory, create it
                WorkDirCreated = true;
                if(!QDir(QString(SystemDirectories_Get()->Home.c_str())).mkdir(QString(SystemDirectories_Get()->Work.c_str())) )
                { // Could not create work directory
                           qFatal("Could not create work directory; exiting");
                }
            }
        }

        // Now we surely have a work directory
        if(!TempDirCreated)
        {   // Create emergeny work directory is user's work directory
            TempDirCreated = true;
            SystemDirectories_Get()->LogFile = SystemDirectories_Get()->Work + "/temp/" + QDate::currentDate().toString("_yyyy-MM-dd").toStdString() + QTime::currentTime().toString("_hh:mm.log").toStdString();

        }

        // OK, print logo
        qInfo() << "Simulator " << PROJECT_NAME  << " "  << PROJECT_VERSION << " started up";

        // Now the logging file and directory is set up, can start logging
        if(WorkDirCreated)
            qInfo()  << "Created new work directory " << SystemDirectories_Get()->Work.c_str();
        else
            qInfo()  << "Using existing work directory " << SystemDirectories_Get()->Work.c_str();
        if(WorkDirRelocated)
            qInfo()  << "  Work directory relocated to " << SystemDirectories_Get()->Work.c_str() ;

        SystemDirectories_Get()->SystemData = SystemDirectories_Get()->Install + "/../" + "data";
         SystemDirectories_Get()->SystemDataFound = true;
        // Handle system data directory
        if(!QDir(QString(SystemDirectories_Get()->SystemData.c_str())).exists())
        {
            std::cerr << "  Not found system data directory " << SystemDirectories_Get()->SystemData.c_str()<< std::endl;
            SystemDirectories_Get()->SystemDataFound = false;
    /*    ret = QMessageBox::warning(parent, "Application",
                         "System data directory not found\n"
                            "Shall I continue?",
    //                                   int ret = QMessageBox::warning(parent, tr("Application"),
      //                                                  tr("System data directory not found\n"
         //                                                  "Shall I continue?"),
                         QMessageBox::Yes  | QMessageBox::Cancel);
            if (ret != QMessageBox::Yes)
         {
                qFatal("Missing system data directory; exiting");
         }*/
        }
        else
        {
    ////       ProcessDataFiles(Directories.SystemData);    // Some data may be provides at system level

           std::cerr << "  Found system data directory " << SystemDirectories_Get()->SystemData.c_str() << std::endl;
        }

        // Handle system docs directory
        SystemDirectories_Get()->SystemDocs = SystemDirectories_Get()->Install + "/../" + "docs";
        SystemDirectories_Get()->SystemDocsFound = true;
        // Handle system docs directory
        if(!QDir(QString(SystemDirectories_Get()->SystemDocs.c_str())).exists())
            {
            std::cerr << "  Not found system docs directory " << SystemDirectories_Get()->SystemDocs.c_str() << std::endl;
            SystemDirectories_Get()->SystemDocsFound = false;
            if (parent)
            {
                //                             "System docs directory not found in "+(string(Directories.SystemDocs.c_str()+string("\n"))) +

                int ret = QMessageBox::warning(parent, "Application",
                             "System docs directory not found\n"
                                "Shall I continue?",
                             QMessageBox::Yes  | QMessageBox::Cancel);
                if (ret != QMessageBox::Yes)
            {
                    qCritical() << "System docs directory '" << SystemDirectories_Get()->SystemDocs.c_str() << "' missing; exiting";
            }
            }
            }
        else
        {
            std::cerr << "  Found system docs directory '" << SystemDirectories_Get()->SystemDocs.c_str() << std::endl;
        }
 //       bool SystemTempFound = QDir(QString(SystemDirectories_Get()->Temp.c_str())).exists();
 //       string DirTempRoot = SystemDirectories_Get()->Temp;

        if(!SystemTempFound)
            SystemDirectories_Get()->Temp = SystemDirectories_Get()->Work +
                    //'/' + PROJECT_NAME +
                    "/temp";
        SystemDirectories_Get()->Config =  SystemDirectories_Get()->Home  +"/.config" + DirTempRoot;
        // Handle user data directory
        bool UDataDirFound = false;
//        bool UDataDirCreated = false;
        SystemDirectories_Get()->UserData = SystemDirectories_Get()->Work + '/' + "data";
        QDir datapath(SystemDirectories_Get()->UserData.c_str());
        if (!datapath.exists()){
            if(datapath.mkdir(SystemDirectories_Get()->UserData.c_str()))
            {
//            UDataDirCreated = true;
            }
            else
            {
                qFatal("User data directory cannot be created");
            }
          }
        else UDataDirFound = true;

        if(UDataDirFound)
            std::cerr << "  Using old user data directory " << SystemDirectories_Get()->UserData.c_str() << std::endl;
        else
            std::cerr << "  Created new user data directory " << SystemDirectories_Get()->UserData.c_str() << std::endl;

        // Handle user output directory
        bool UOutputDirFound = false;
 //       bool UOutputDirCreated = false;
        SystemDirectories_Get()->UserOutput = SystemDirectories_Get()->Work + '/' + "output";
        QDir outpath(SystemDirectories_Get()->UserOutput.c_str());
        if (!outpath.exists()){
            if(datapath.mkdir(SystemDirectories_Get()->UserOutput.c_str()))
            {
//            UOutputDirCreated = true;
            }
            else
            {
                qFatal("User output directory cannot be created");
            }
          }
        else UOutputDirFound = true;

        if(UOutputDirFound)
            std::cerr << "  Using old user output directory " << SystemDirectories_Get()->UserOutput.c_str() << std::endl;
        else
            std::cerr << "  Created new user output directory" << SystemDirectories_Get()->UserOutput.c_str() << std::endl;

        std::cerr << "Directories of the tool are set as follows" << std::endl;
        std::cerr << "  Install:    " << SystemDirectories_Get()->Install.c_str() << std::endl;
        if(SystemDirectories_Get()->SystemDataFound)
            std::cerr << "    System data " << SystemDirectories_Get()->Install.c_str() << "../data" << std::endl;
        else
            LOG_WARNING("Data-dependent services are not available");
        if(   SystemDirectories_Get()->SystemDocsFound)
            std::cerr << "    System docs " << SystemDirectories_Get()->Install.c_str() << "../docs" << std::endl;
        else
            LOG_WARNING("System documentation files are not available");
        std::cerr << "  User config:" << SystemDirectories_Get()->Config.c_str() << std::endl;
        std::cerr << "  Work:       " << SystemDirectories_Get()->Work.c_str() << std::endl;
        std::cerr << "    Output:     " << SystemDirectories_Get()->UserOutput.c_str() << std::endl;
        std::cerr << "    Data:       " << SystemDirectories_Get()->UserData.c_str() << std::endl;
        std::cerr << "  Temporary:  " << SystemDirectories_Get()->Temp.c_str() << std::endl;
        std::cerr << "    Log file: " << SystemDirectories_Get()->LogFile.c_str() << std::endl;
        // Suppress warnings about some deprecated features
        sc_core::sc_report_handler::set_actions( "/IEEE_Std_1666/deprecated",
                                                 sc_core::SC_DO_NOTHING );
        mSettingsFileName =  "." PACKAGE_NAME "V" PROJECT_VERSION; // The sought filename
        SetupSettings();    // Set up settings handling of the simulator
    }

    // After this setup, QSettings can be used from different places
    // using the default constructor
    void scqSimulator::
SetupSettings()
    {
        QCoreApplication::setOrganizationName(MY_COMPANY);
        QCoreApplication::setOrganizationDomain(MY_DOMAIN);
        QCoreApplication::setApplicationName(QString(PROJECT_NAME));
    }

    /*        QSettings Ssettings(QSettings::IniFormat , QSettings::SystemScope, mSettingsFileName.c_str());
   // First read the application-wide settings
   readSettings(&Ssettings,"System");
*/     /*  string UserFileName = SystemDirectories_Get()->Config;
// First read application-wide settings
//        QSettings Usettings(QSettings::IniFormat , QSettings::UserScope, QString( UserFileName));
   QSettings AMsettings(QSettings::IniFormat , QSettings::UserScope, (mSettingsFileName + "/" + "Main").c_str());
   // Second read the user-wide settings
   readSettings(&AMsettings,"Application", "MainWindow");
   QSettings AOsettings(QSettings::IniFormat , QSettings::UserScope, (mSettingsFileName + "/" + "Others").c_str());
   readSettings(&AOsettings,"Application", "Others");

   // Second read user-wide settings
   string S0 =  SystemDirectories_Get()->Home + "/" + mSettingsFileName + "/" + string("Main.ini");
   QSettings UMsettings( QString(S0.c_str() ), QSettings::IniFormat);
   readSettings(&UMsettings,"User", "MainWindow");
   S0 =  SystemDirectories_Get()->Home + "/" + mSettingsFileName + "/" + string("Others.ini");
   QSettings UOsettings( QString(S0.c_str() ), QSettings::IniFormat);
   readSettings(&UOsettings,"User", "Others" );
   // Thirs read project-wide settings
       string S =  SystemDirectories_Get()->Work + "/" + mSettingsFileName + "/" +string("Main.ini");
   QSettings PMsettings( QString(S.c_str() ), QSettings::IniFormat);
   readSettings(&PMsettings,"Project", "MainWindow");
   S =  SystemDirectories_Get()->Work + "/" + mSettingsFileName + "/" +string("Others.ini");
   QSettings POsettings( QString(S.c_str() ), QSettings::IniFormat);
   readSettings(&POsettings,"Project", "Others");
   */
