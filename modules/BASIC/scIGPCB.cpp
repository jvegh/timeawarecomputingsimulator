/** @file scIGPCB.cpp
 *  @brief Function prototypes for the scIGPCB
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "scGridPoint.h"
#include "scIGPCB.h" //#include "scIGPMessage.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scProcessor.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing
extern string DebugRoute;  // This will go to Macros.h


/*!
   * @class scIGPCB
   * @brief The  electronically implemented Inter-GridPoint Communication Block implements
   * a helper modules of direct inter-gridpoint communication
   * towards the immediate (and second intermediate) neighbors in the hexagonal module grid.
   * This helper module only makes the transport but makes nothing with the data,
   * only sends a notification signal to its owner gridpoint.
   * The messages are written to the message queue of the owner pridpoint.
   * 
   * The scIGPCB modules are created in pairs, when \see scProcessor populates
   * the grid and notices that an \see scGridPoint has an immediate neigbor.
   *
   * This unit is responsible for the inter-core communication
   */
    scIGPCB::scIGPCB(const sc_core::sc_module_name nm, scGridPoint* GP, scIGPCB* IGPCB)
: sc_core::sc_module(nm)
  ,msGridPoint(GP)
  ,msOtherIGPCB(IGPCB)
  ,msIsSending(false)
//  msClocks(0)
{
    // Creates a FIFO pair
    // Will be connected when creating clusters
    IGPFIFOin = new scIGPfifo("FIFOin");
    IGPFIFOout = new scIGPfifo("FIFOout");
    
    Reset();
    SC_THREAD(MessageReceived_thread);
        sensitive << EVENT_IGPCB.MessageReceived;
}


    scIGPCB::
~scIGPCB()
{
    delete IGPFIFOin;
    delete IGPFIFOout;
}

/*!
 * \fn void scIGPCB::ConnectFIFOsTo(scIGPCB* const OtherIGPCB)
 *
 * \brief Connect the two FIFOs between the partner scIGPCB modules
 * \param OtherIGPCB the address of the partner IGPCB the FIFOs are connected to
 */
    void scIGPCB::
ConnectFIFOsTo(scIGPCB* const OtherIGPCB)
{
    scIGPout(*OtherIGPCB->IGPFIFOin);
    scIGPin(*OtherIGPCB->IGPFIFOout);
    OtherIGPCB->scIGPout(*IGPFIFOin);
    OtherIGPCB->scIGPin(*IGPFIFOout);
}


    /*!  @brief This routine picks up from the FIFO the message 
     * 
     */
    /*!
     * \brief scIGPCB::MessageReceived_thread
     *
     * This method is activated when this IGPCB receives a direct message
     * All the processing happens in the bottom layer
     */
    void scIGPCB::
MessageReceived_thread(void)
{
//    DEBUG_PRINT("RCVD EVENT_IGPCB.MessageReceived thread started"); 
        // This is triggered when the other party starts with the message
    while(true)
    {
        // This first phase happens in the bottom layer

        wait(EVENT_IGPCB.MessageReceived);
        DEBUG_ONLY(scGridPoint* This = GridPoint_Get(););
        DEBUG_EVENT(This->StringOfClusterAddress_Get() << "% WAIT EVENT_IGPCB.MessageReceived");
        // As it arrives through direct wiring, give it a uniform packing,
        // so the scGridPoint cannot find out which way the message arrived
        scIGPMessage* MyMessage = new scIGPMessage;
        // Make a copy of the header; we have at least 4 words
        // These words are uint32_t, typing is lost
        int32_t Word;
        IGPFIFOin->read(Word);  // Read from the buffer; destination address
        ClusterAddress_t CA;
        memcpy(&CA, &Word, sizeof(Word));
        MyMessage->DestinationAddress_Set(CA);
        MessageFeatures_t F;
        IGPFIFOin->read(Word);  // Read from the buffer; feature word
        memcpy(&F, &Word, sizeof(Word));
        MyMessage->FeatureWord_Set(F);
        IGPFIFOin->read(Word);  // Read from the buffer; reply-to address
        memcpy(&CA, &Word, sizeof(Word));
        MyMessage->ReplyToAddress_Set(CA);
        IGPFIFOin->read(Word);  // Read from the buffer; the last word of the header
        MyMessage->Header3Address_Set((uint32_t) Word);

        // OK, now copy the buffer content
   
        int16_t Length = MyMessage->Length_Get();
        assert((Length>=0) && (Length<=MAX_IGPCBUFFER_SIZE));
        // We may have more words in the message, read them
        for(int i = 4; i<Length; i++)
        {
            IGPFIFOin->read(Word);  // Read from the buffer; blocking read
            MyMessage->BufferContent_Set(i-4, Word);   // and make our own copy
        }
        DEBUG_ONLY(scGridPoint* Other = OtherIGPCB_Get()->GridPoint_Get(););
        DEBUG_EVENT(GridPoint_Get()->StringOfClusterAddress_Get()
            << "% RCVD EVENT_IGPCB.MessageReceived from "
            << Other->StringOfClusterAddress_Get() );
        // Now the message is completed: from the FIFO buffer,
        // it is converted to the universal message format

        // Now pass the received message to the lower layer of the gridpoint
        // The received message goes into a queue
        // and it notifies the bottom layer of the corresponding core
        GridPoint_Get()->GPMessagefifo->write(MyMessage);
        DEBUG_EVENT(This->StringOfClusterAddress_Get() <<
                    "% SENT GPMessagefifo_write to " << GridPoint_Get()->StringOfClusterAddress_Get());
    // Our mission is now over:
    //    the scGridPoint will finish processing: either forwards it
    // or processes it
    }
}// end of MessageReceived_thread


    void scIGPCB::
Reset(void)
{
//    msBufferPointer = 0;  // Write to the buffer at the beginning
    IGPFIFOout->reset();    // Reset the fifo
}

#ifdef other

    /* !
     * \brief scIGPCB::b_transport
     *
     * This routine reads/write data between the given address and its own buffer
     * 1. The scGridPoint assembles the message using
     *    scGridPoint::ICB_b_transport writes into scIGPCB[index]
     *    and sends EVENT_GridDataSent to itself
     * 2. The addressed scIGPCB::b_transport receives the message in its buffer[]
     *    and sends EVENT_GridDataReceived to its scGridPoint
     * 3. The (other) scGridPoint::ICB_b_transport reads from its buffer[]
     *    to the real target in scGridPoint (maybe latch or register)
     *    and sends EVENT_GridDataReceived to itself
     * \param trans the transfer command
     * \param delay the transfer delay
     */
/*    void scIGPCB::
b_transport(tlm::tlm_generic_payload& trans, sc_time& delay )
{
  tlm::tlm_command cmd = trans.get_command();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  unsigned char*   byt = trans.get_byte_enable_ptr();
  unsigned int     wid = trans.get_streaming_width();

  if (byt != 0) {
    trans.set_response_status( tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE );
    return;
  }
  if (len > 4 || wid < len) {
    trans.set_response_status( tlm::TLM_BURST_ERROR_RESPONSE );
    return;
  }

  // Obliged to implement read and write commands
  if ( cmd == tlm::TLM_READ_COMMAND )
  {
    memcpy(ptr, &Buffer[msBufferPointer], len);
    msOtherIGPCB->GridPoint_Get()->EVENT_Grid.TransportedToIGPCB.notify(); // Unblock the sending gridpoint
//    EVENT_GridDataArrived.notify();   // Let the other object know it must handle the data
  }
  else if ( cmd == tlm::TLM_WRITE_COMMAND )
  {
    memcpy(&Buffer[msBufferPointer], ptr, len);
    msBufferPointer += len;
//    msOtherIGPCB->EVENT_GridDataArrived.notify();   // Let the object know it must handle the data
  }

  // Illustrates that b_transport may block
//  wait(delay);

  // Reset timing annotation after waiting
  delay = SC_ZERO_TIME;

  // Obliged to set response status to indicate successful completion
  trans.set_response_status( tlm::TLM_OK_RESPONSE );
  msOtherIGPCB->GridPoint_Get()->EVENT_Grid.TransportedToIGPCB.notify(delay);
}
*/

/*    send_socket = new tlm_utils::simple_initiator_socket<scIGPCB>("send_socket") ;
    receive_socket = new tlm_utils::simple_target_socket<scIGPCB>("receive_socket");
    receive_socket->register_b_transport(       this, &scIGPCB::b_transport);
    */
    // mIntercoreBus = new InterCoreBusCtrl(string(name()).append("Bus").c_str());
    /*    msIntercore_fifo = new InterCore_fifo(sc_core::sc_module_name(string("ICB_")
                                                                      //.append(IDtoString(LinearAddress1,CORE_BUS_WIDTH))
                                                                      .c_str()));
    */


//scCore::Process {implementations }
//scCore::Helper {implementations }

/*void scICB::ReceivedClock(void)
{
    while (true){
        mClocks++;
      std::cout << "Time at " << name() << " =" <<  sc_core::sc_time_stamp()  <<'\n';
//      JustPrint();
      wait();

    }
}*/

//  inline int scICB::
//BackLinkNumber_Get(void) { return msFromSV_FIFO->num_available();}
#endif

