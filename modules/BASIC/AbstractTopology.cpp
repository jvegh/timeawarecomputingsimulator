/** @file AbstractTopology.cpp
 *  @ingroup TAC_MODULE_BASIC
 *  @brief Source file for electronic simulators, for handling points with electronic functionality  (modules)
 *  arranged logically as a rectangular grid, physically as a hexagonal grid
 * The physical proximity provides options for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in scProcessor.cpp
 * and Buses.cpp
 */
/*
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
*/
// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    ///< Print event debug messages  for this module
//#define DEBUG_PRINTS    ///< Print general debug messages for this module
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

#include "AbstractTopology.h"
#include "GridPoint.h"
//#include <iomanip>      // std::setfill, std::setw


extern bool UNIT_TESTING;	// Whether in course of unit testing

 // The 8 fields enable to address the 7 cores plus a broadcast
NamedEnum_struct ClusterMembers[8] =
{
    {"H",    cm_Head},
    {"N",    cm_North},
    {"NE",   cm_NE},
    {"SE",   cm_SE},
    {"S",    cm_South},
    {"SW",   cm_SW},
    {"NW",   cm_NW},
    {"B",    cm_Broadcast}
};



// Logically, the point are created in a simple rectangular grid
// However, they 'know' their topological location
// Creates the topology of GridPoint modules
/* The modules are organized into clusters.
* A module can be the head of the cluster, a member of a cluster, or neither (stand-alone).
* for the codes, see * @see  ClusterNeighbor.
* The relative offsets in positions ar given by GridXOffset and GridYOffset.
   * The hexagonal gridpoints can form a 'cluster': they are arranged as
   *
   * @code
   *              \ N(X,Y+2) /
   *               ---------
   *  NW(X-1,Y+1) /         \ NE(X+1,Y+1)
   *     --------<   (X,Y)   >--------
   *  SW(X-1,Y-1) \         / SE(X+1,Y-1)
   *               ---------
   *              / N(X,Y-2)\
   * @endcode

*
* \brief
* @verbatim
*  |AbstractTopology
*  |--/scProcessor
*  |..../AbstractProcessor
*  |....../XXProcessor (Y86, RISCV)
*  |....../NeurerProcessor
* @endverbatim
*/
    AbstractTopology::
AbstractTopology(void) :
    mProgramCounter(CORE_DEFAULT_ADDRESS)
{
        // The clustering adjusted manually; only works for 10x6 geometry
    assert (!((GRID_SIZE_X < 0) || (GRID_SIZE_Y < 0)));
    assert (!((GRID_SIZE_X > 10) || (GRID_SIZE_Y > 6)));
    mGrid.resize(GRID_SIZE_X);
    for(int i = 0; i < GRID_SIZE_X; i++, i++) // This needed because of the hexagonal grid
    {
        mGrid.at(i).resize(GRID_SIZE_Y);
        mGrid.at(i+1).resize(GRID_SIZE_Y);
    }
    // First, populate with NULLs; Cluster_Create will assert otherwise
    // Create both the vector and the matrix storage
    for(int i = 0; i < GRID_SIZE_X; i++) // This needed because of the hexagonal grid
        for(int j= 0; j < GRID_SIZE_Y; j++)
                mGrid[i][j] = (GridPoint*) nullptr; // No points are present
}

    AbstractTopology::
~AbstractTopology(void)
{
}

  // Create clusters from the elements of the rectangular grid.
    void AbstractTopology::
CreateClusters(void)
{
    // Not needed if passed creation; but recall also here
   assert (!((GRID_SIZE_X < 0) || (GRID_SIZE_Y < 0)));  // Protect that size
   assert (!((GRID_SIZE_X > 10) || (GRID_SIZE_Y > 6)));
   Cluster_Create(1,9);
   Cluster_Create(2,4);
   Cluster_Create(4,8);
   Cluster_Create(5,3);
   Cluster_Create(7,7);
   Cluster_Create(8,2);
   // These next two clusters are incomplete, contain 1+2 members
   Cluster_Create(9,11);
   Cluster_Create(0,0);
   // These are phantom clusters: the head is outside,
   // but some orphan members are inside
   Cluster_Create(-1,5);
   Cluster_Create(3,-1);
   Cluster_Create(6,-2);
   Cluster_Create(10,6);
   Cluster_Create(6,12);
   Cluster_Create(3,13);
}


  /*!
      * \brief Create a new cluster at point P
     */
    void AbstractTopology::
Cluster_Create(GridPoint* P)
{
    assert(P);  // Creating a cluster in nothing
    assert(mGrid[P->X][P->Y]);  // Has no object
    assert(cs_NONE == ClusterStatus_Get(P,cm_Head)); // The GridPoint has status other than csNONE
    for(ClusterNeighbor N = cm_Head; N <= cm_NW; N = (ClusterNeighbor) (N+1))
    assert(!(cs_Head == ClusterStatus_Get(P,N)) ||
            (cs_Member == ClusterStatus_Get(P,N))); // One of the cluster members already belongs to a cluster
    // Now the cluster can surely be created  
    mClusters.push_back(P); // Among others connect it to the inter-cluster buses
    ClusterStatus_Set(P, cm_Head, cs_Head); // Set that it is a head
    for(ClusterNeighbor N = cm_North; N <= cm_NW; N = (ClusterNeighbor) (N+1))
        { // Now set up the members
            GridPoint* PM = ByClusterPointer_Get(P,N);
            if(PM)
            {  // The member exists, set its status to cluster member
                ClusterAddress_t CA = PM->ClusterAddress_Get();
                CA.Cluster = P->ClusterAddress_Get().Cluster;
                CA.Member = N;
                CA.Proxy = cm_Head;
                CA.HThread = 0;
                PM->ClusterAddress_Set(CA);
                PM->ClusterStatus_Set(cs_Member);
            }
        }
}
    /*!
     * \brief Create a new cluster at position X,Y, in the 0th HThread
     */
    void  AbstractTopology::
Cluster_Create(int X, int Y)
{
    GridPoint* P = ByPosition_Get(X,Y);
    if(P)
        Cluster_Create(P);
    else
    {   // We must create a cluster with phantom head: the head is outside, but some members inside
        GridPoint* PhantomHead = new GridPoint(X,YFromPosition_Get(X,Y), cs_Error);  // Create the phantom head
        for(ClusterNeighbor N = cm_Head; N <= cm_NW; N = (ClusterNeighbor) (N+1))
        { // Verify if the real members of the phantom head are already assigned
            assert(!(cs_Head == ClusterStatus_Get(PhantomHead,N)) ||
                    (cs_Member == ClusterStatus_Get(PhantomHead,N)));
        }

        //The source is a phantom: it does not have cluster head,
        // so do NOT create a new cluster 
        // Some of the cluster heads must be a second neighbor,
        // it can be a replacement head
        //
        // We must look for replacement heads for the real points
        // belonging to this phantom cluster head

        GridPoint* PhantomMember; ClusterNeighbor NPH = cm_North;
        for(; NPH <= cm_NW; NPH = ClusterNeighbor((int)NPH + 1))
        {  // Check if it is good proper candidate neighbor
            PhantomMember = ByClusterPointer_Get(PhantomHead,NPH);
            if(PhantomMember)
            {// A real member of the phantom cluster found
                GridPoint* ReplacementHead = (GridPoint*)nullptr; // The last clusters may have smaller number of members
                for (int i = NumberOfClusters_Get()-1; i >=0; i--)
                {
                    ReplacementHead = ClusterHead_Get(i);
                     if(ReplacementHead && ReplacementHead->Is2ndNeighborOf(PhantomMember)) break;
                }
                // Now we have the replacement cluster head,
                // find out which of its members we can use as proxy
                GridPoint* Proxy; ClusterNeighbor NP = cm_North;
                for(; NP <= cm_NW; NP = ClusterNeighbor((int)NP + 1))
                {  // Check if it is good proper candidate neighbor
                    Proxy = ByClusterPointer_Get(ReplacementHead,NP);
                    if(Proxy && Proxy->IsNeighborOf(PhantomMember)) break;
                }
                // Now a proper proxy found;
                // find out the direction to reach the external number
                GridPoint* ProxyP; ClusterNeighbor NQ = cm_North;
                for(; NQ <= cm_NW; NQ = ClusterNeighbor((int)NQ + 1))
                {  // Check if it is good neighbor
                    ProxyP = ByClusterPointer_Get(Proxy,NQ);
                    if(ProxyP && ProxyP->IsTheSameAs(PhantomMember)) break;
                }

                // Now we can give alias name to the original address of the requestor
                    GridPoint* PM = ByClusterPointer_Get(Proxy,NQ);
                    if(PM)  // The member exists, set its status to cluster member
                    {
                        ClusterAddress_t CA = PhantomMember->ClusterAddress_Get();
                        CA.Cluster = ReplacementHead->ClusterAddress_Get().Cluster;
                        CA.Proxy = NQ; // Proxy requested
                        CA.Member = NP; // from the proxy
                        CA.HThread = 0;
                        PhantomMember->ClusterAddress_Set(CA);
                        PhantomMember->ClusterStatus_Set(cs_Member);
                    }
                DEBUG_PRINT("PHANTOM aliased to " << PhantomMember->StringOfClusterAddress_Get() << ")");
             }
        }
        // All real members aliases, we are done
        delete PhantomHead;
    }
}

  /*!
     * \brief AbstractTopology::ByIndex_Get
     * Return the grid point at the given coordinates
     * \param X coordinate
     * \param Y coordinate
     * \return NULL if outside the grid
     */
    GridPoint* AbstractTopology::
ByIndex_Get(const int X, const int Y)
{
    assert (!((GRID_SIZE_X < 0) || (GRID_SIZE_Y < 0)));
    assert (!((GRID_SIZE_X > 10) || (GRID_SIZE_Y > 6)));
    assert (!((X<0) || (X >= GRID_SIZE_X)));
    assert (!((Y<0) || (Y >= GRID_SIZE_Y)));
   return mGrid[X][Y];
}
    /*!
     * \brief ByID_Get(int N) Get the gridpoint defined by its sequence number
     * \param[in] N The sequence number of the GridPoint
     * \return
     */
    GridPoint* AbstractTopology::
ByID_Get(int N)
{
    assert (( N >= 0) && (N<GRID_SIZE_X * GRID_SIZE_Y));
    int MyX = N/GRID_SIZE_Y; int MyY = N%GRID_SIZE_Y;
    return mGrid[MyX][MyY];
}

        int  AbstractTopology::
    LinearAddressFromPosition_Get(GridPoint* GP)
    {return LinearAddressFromCoordinates_Get(GP->X,GP->Y); }


    /*    GridPoint* AbstractTopology::
    ByID_Get(int N)
    {
        assert (!(( N < 0) || (N>=GRID_SIZE_X * GRID_SIZE_Y)));
        int MyX = N/GRID_SIZE_Y; int MyY = N%GRID_SIZE_Y;
        return mGrid[MyX][MyY];
    }
    */

        // Return the pointer corresponding to cluster parameters
    GridPoint* AbstractTopology::
ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM, ClusterNeighbor CP)
{
    assert (!((CN < 0) || ((CN >= mClusters.size()) && (CN != MAX_CLUSTERS_LIMIT))));
    assert (!((CM<cm_Head) || (CM > cm_NW)));
    GridPoint* P = mClusters.at(CN);
    // Get the member first, and prepare to return it
    if(CM) P =  ByClusterPointer_Get( P, CM);
        //DEBUG_PRINT("Address is "
            //<< StringOfClusterAddress_Get(P));
    // If the proxy is not the head, return that member instead
    if(CP) P = ByClusterPointer_Get(P,CP);
        //DEBUG_PRINT("Address is "
            //<< StringOfClusterAddress_Get(P));
    return P;
}

    // This returns the GridPoint with the variable fields given in CA
    GridPoint* AbstractTopology::
ByClusterAddress_Get(ClusterAddress_t CA)
{
    return ByClusterMember_Get(CA.Cluster, (ClusterNeighbor)CA.Member, (ClusterNeighbor)CA.Proxy);
}

    GridPoint* AbstractTopology::
ByPosition_Get(int X, int Y)
{
        // Must not assert: used internally to detect if point legal
   if(X<0 || X >= GRID_SIZE_X) return (GridPoint*) NULL;
   if(Y<0 || Y >= GRID_SIZE_Y*2) return (GridPoint*) NULL;
   assert( X%2 == Y%2); // Both must be even or both odd
   return ByIndex_Get(X,(Y - moduloN(X,2))/2);
}

    ClusterStatus AbstractTopology::
ClusterStatus_Get(GridPoint* P, ClusterNeighbor N)
{
    assert(P); // Illegal point
    assert (!(N<cm_Head) || (N > cm_NW));  // Illegal member
//        if((N<cm_Head) || (N > cm_NW) || !P) return cs_Error;
    GridPoint* PP =  ByClusterPointer_Get(P, N);
    if(!PP)return cs_Error; // Must NOT assert
    return PP->ClusterStatus_Get();
}

    ClusterAddress_t AbstractTopology::
ClusterAddress_Get(GridPoint* P)
{
        assert(P);
    return P->mClusterAddress;
}

    GridPoint* AbstractTopology::
ByClusterPointer_Get(GridPoint* P, ClusterNeighbor N)
{
    if((N<cm_Head) || (N > cm_NW) || !P) return (GridPoint*) NULL;
    int Xpos = P->X +GridXOffset[N];
    int Ypos = P->YPosition_Get() +GridYOffset[N];
    if(Xpos<0 || Xpos>= GRID_SIZE_X) return (GridPoint*) NULL;
    if(Ypos<0 || Ypos>= GRID_SIZE_Y*2) return (GridPoint*) NULL;
    return ByPosition_Get(Xpos, Ypos);
}
    // Set cluster status of member N in cluster *P to S
    void AbstractTopology::
ClusterStatus_Set(GridPoint* P, ClusterNeighbor N, ClusterStatus S)
{
    GridPoint* PP = ByClusterPointer_Get(P,N);
        if(!PP)  return;    // No such member, something wrong
    // We have a phantom head, with real members.
    // Create aliases for this member using a helper head
    // PP is real, but has no real address
    //We find a proxy

    PP->mClusterAddress.Cluster = mClusters.size()-1;
    PP->mClusterAddress.Member = N;
    PP->mClusterAddress.Proxy = cm_Head;
    PP->mClusterAddress.HThread = 0;
    PP->ClusterStatus_Set(S);
}


    /*!
     * \brief AbstractTopology::ClusterHeadOfMember_Get
     * \param GP the actual GridPoint
     * \return the head of the cluster GP belongs to
     */
    GridPoint* AbstractTopology::
ClusterHeadOfMember_Get(GridPoint* GP)
{
    assert (GP);
    return mClusters[ GP->ClusterAddress_Get().Cluster];
}


int AbstractTopology::
NeighborIndexInCluster_Get(GridPoint* Head, GridPoint* Neighbor)
{
    if  (!Head || !Neighbor) return -1;
    ClusterNeighbor N = cm_North;
    for(; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {
        GridPoint* NN = ByClusterPointer_Get(Head,N);
        if(Neighbor->IsTheSameAs(NN)) return N;
    }
    return -1;
}

ClusterNeighbor AbstractTopology::
NeighborDirectionFind(GridPoint* Head, GridPoint* G)
{
    if(!Head->IsNeighborOf(G)) return cm_Broadcast;
    ClusterNeighbor N = cm_North;
    for(; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {
        GridPoint* NN = ByClusterPointer_Get(Head,N);
        if(G->IsTheSameAs(NN)) return N;
    }
    return cm_Broadcast;
}

// Return the string describing the point in scClusterAddress_t form
    string AbstractTopology::
StringOfClusterAddress_Get(GridPoint* GP)
{   //
    assert(GP);
    ostringstream oss;
    oss << "(" ;
    if(!ClusterHeadOfMember_Get(GP)) {oss << "!";}  // Maybe its is a phantom
    oss << GP->ClusterAddress_Get().Cluster << "." << ClusterMembers[GP->ClusterAddress_Get().Member].name;
//    oss << StringOfProxy_Get(GP) << ")";
    if(GP->ClusterAddress_Get().Proxy) {oss << ":" << ClusterMembers[GP->ClusterAddress_Get().Proxy].name;}
    return oss.str();
}
/*
    string AbstractTopology::/// Return the string corresponding to the proxy part, if any, of the cluster address of GP
StringOfProxy_Get(GridPoint* GP)
{   //
    ostringstream oss;
    if(GP->ClusterAddress_Get().Proxy) {oss << ":" << ClusterMembers[GP->ClusterAddress_Get().Proxy].name;}
    return oss.str();
}*/

#ifdef DRAW
    // Helper functions to draw the grid, mainly for debugging
    /*!
     * \fn AbstractTopology::DrawPoint
     * \brief Draw basic characteristics of the gridpoint
     * \param i X coord
     * \param j Y coord
     * \param oss
     */
    void AbstractTopology::
DrawPoint(int i, int j, ostringstream &oss)
{
    GridPoint* P = mGrid[i][j];
    oss << '(' << setw(2) << P->X << ',' << setw(2) << P->Y << ")[";
    switch(P->ClusterStatus_Get())
    {
      case cs_NONE:   oss << ' '; break;
      case cs_Member: oss << 'M'; break;
      case cs_Head:   oss << 'H'; break;
      case cs_Error:  oss << '?'; break;
      default:  oss << 'd'; break;
    }
    oss << ']';
}

    /*!
     * \fn AbstractTopology::Draw
     * \brief Draw basic characteristics of the rectangular matrix
     */
    void AbstractTopology::
Draw()
{
    for(int j = GRID_SIZE_Y-1; j>=0; j--)
    {
        ostringstream oss; string S ="/----------\\";
        for(int i = 0; i*2+1 < GRID_SIZE_X; i++)
          { oss << S.c_str();  DrawPoint(i*2+1,j,oss);}
        oss << '\n';
        for(int i = 0; i*2 < GRID_SIZE_X; i++)
          {  DrawPoint(i*2,j,oss);  oss << S.c_str();}
        std::cerr << oss.str().c_str() << '\n';
    }
}
#endif //DRAW

