/** @file scProcessor.cpp
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include <algorithm>    // for sorting
#include <QFile>
#include "scProcessor.h"
//#include "scIGPMessage.h"
#include "scClusterBusMemoryFast.h"
#include "scClusterBusMemorySlow.h"
#include "scClusterBusArbiter.h"
#include "scIGPCB.h"
//#include "Utils.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
extern scClusterBusMemorySlow* MainMemory;
/*!
 * \brief The processor working with scGridPoints
 *   \param nm The SystemC name
 *   \param Specials some special scGridPoints (enables to handle heterogenous topologies)
 *   \param StandAlone true if some part shall be executed here otherwise in the subclass
 */
scProcessor::
scProcessor(sc_core::sc_module_name nm,
            vector<scGridPoint*> Specials, bool StandAlone)
         : AbstractTopology(),sc_core::sc_module( nm)
    , msSuspended(false)
    , msHalted(true)
    ,mInstanceCount(0)
{
    for(int h = 0; h < MAX_HTHREADS; h++)
    {
    // Set mask values for the signals
    msSignalMask[h].Denied = 0; // Neither of the GridPoints is denied
    msSignalMask[h].Allocated = 0; // None of them is allocated
    msSignalMask[h].PreAllocated = 0; // None of them is preallocated
    msSignalMask[h].Available = (int64_t) -1;    // All are available
//    msSignalMask.Children = 0; // No direct children yet
    msSignalMask[h].PreAllocatedForTopology = 0; // No scGridPoints are allocated DIRECTLY for the topology
    
    // Connect signals to ports
    msSignalPort[h].Denied(msSignal[h].Denied);  // Which scGridPoints are denied
    msSignalPort[h].Allocated(msSignal[h].Allocated);  // Which scGridPoints are Allocated
    msSignalPort[h].PreAllocated(msSignal[h].PreAllocated);  // Which scGridPoints are PreAllocated
    msSignalPort[h].Available(msSignal[h].Available);  // Which scGridPoints are available
    }
    // Availability status bits are handled here
    SC_METHOD(SetMasks_method); // Any of the states relating masking changed
       sensitive << EVENT_PROCESSOR.MasksChanged;
       dont_initialize(); //
    SC_METHOD(QTERM_method);
        sensitive << EVENT_PROCESSOR.QTERM;
        dont_initialize();
        if(MainMemory)
        {   // We defined and will use memory, bus, etc
    MainMemory->Processor_Set(this);
    // Implement the Cluster-Related modules
    mClusterBus = new scClusterBus("ClusterBus",this);
//    mClusterBus->clock(clock);

    mClusterBusArbiter = new scClusterBusArbiter("ClusterArbiter");
    // No arbiter clock
    mClusterBus->arbiter_port(*mClusterBusArbiter);

    mClusterMemoryFast = new scClusterBusMemoryFast("Register", 0x00, 32*4-1);
    //   mClusterMemoryFast->clock(clock);    // The direct memory has no clock   
    mClusterBus->slave_port(*mClusterMemoryFast);

    // The main memory is independent, must be created and clocked in the top module
    // It can be connected to the cluster bus here
    mClusterBus->slave_port(*MainMemory);
        }

//    MetaEvents = new  MetaEvent_fifo("SV_MetaFIFO"); 
   // These threads are simple flag-handlers, so they are NOT re-implemented in the subclasses
    SC_THREAD(HALT_thread);
        sensitive << EVENT_PROCESSOR.HALT;
    SC_THREAD(SUSPEND_thread);
        sensitive << EVENT_PROCESSOR.SUSPEND;
    SC_THREAD(RESUME_thread);
        sensitive << EVENT_PROCESSOR.RESUME;

    if(StandAlone)
    {   // scGridPoints are not subclassed, will be populated
        SC_METHOD(Initialize_method);
        SC_THREAD(START_thread);
            sensitive << EVENT_PROCESSOR.START;
         // Triggered by writing into the MetaEvent_fifo       
/*         SC_THREAD(Execute_thread);
           sensitive << MetaEvents->write_event;  */
 //!!        SC_THREAD(BackLink_thread);       
 //!!          sensitive << msBLfifo->write_event;       
        // The specials can contain even analog units with scGridPoint behavior!
            Populate(Specials);    // Populate with scGridPoints
            CreateClusters();      // Clusterize the scGridPoints
            ConnectIGPCBs();       // Connect direct scGridPoints to scGridPoints communication
//            CheckIGPCBs();
            ConnectClusterHeadsToBus(mClusterBus);  // Connect ONLY cluster heads to inter-cluster bus
            Reset();
    }
}

    scProcessor::
~scProcessor(void)
{
}
/* Just for a debug
    void scProcessor::
    CheckIGPCBs()
    {
        for(int i = 0; i < GRID_SIZE_X; i++)
          for(int j = 0; j < GRID_SIZE_Y; j++)
        {
           // Take the points, one by one
            scGridPoint* GP = ByIndex_Get(i,j);
            scGridPoint* CHead = ByIndex_Get(i,j);
            // This CHead is not necessarily a real cluster head; it is just a starting point
            for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
            {  // Check if it is good neighbor
                if(!CHead->msIGPCB[N])
                { // This ICB is NOT connected
                    if(ByClusterPointer_Get(CHead,N)) // It should be a good neighbor
                     cerr << "Missing connection at(" << i << "," << j*2 + i%2  << "," << N << ")\n" ;
                }
                else
                { // The two IGPCBs are connected, check if they are connected properly (they must point to each other)
 //                   if(CHead->msIGPCB[N]->OtherIGPCB_Get() == )
                }
            }
        }

    }
*/
     /*!
 * \brief scProcessor::RefreshGUI
 * \param G The gridpoint sending the message
 *
 * This message is sent
 */
void scProcessor::RefreshGUI(scGridPoint & G)
{

}

/*!
 *  This method starts up when all initializations finished
 */
void scProcessor::Initialize_method(void)
{
//    wait(SCTIME_SV); // For setup
    DEBUG_PRINT("Initialize_method started");
 //    mscoreStatus = STAT_AOK; // Assume everything all-right
    // We surely have no processing unit and also the first instruction must be fetched

 //   EVENT_PROCESSOR.START.notify();
}

/*!
 * \fn void scProcessor::ConnectIGPCBs(void)
 * \brief Connects the scIGPCB modules one the two side of the common boundaries ressespondingly
 * 
 * The routine assumes that the grid with the clusters is set up properly.
 * Finds the scGridPoint modules having common boundary, creates
 * scIGPCB modules on both sides, and connects their FIFOs correspondingly
 */

    void scProcessor::
ConnectIGPCBs(void)
{
    // Establish connection between them: look for the neighbors,
     // remember them and create an IGPCB  directed towards them
    for(int i = 0; i < GRID_SIZE_X; i++)
      for(int j = 0; j < GRID_SIZE_Y; j++)
    {
       // Take the points, one by one
        scGridPoint* GP = ByIndex_Get(i,j);
//!!        GP->clock(clock);
         //int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
        scGridPoint* CHead = ByIndex_Get(i,j);
        // This CHead is not necessarily a real cluster head; it is just a starting point
        for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
        {  // Check if it is good neighbor
            if(!CHead->msIGPCB[N])
            { // This ICB is NOT yet created by a previous pair production
                scGridPoint* GPN = ByClusterPointer_Get(GP,N);
                if(GPN)
                { // It is a good neighbor, make a FIFO we can write into
                    scGridPoint* CMember = GPN;
                    int M = NeighborIndexInCluster_Get(CMember,CHead); // Find  out the reverse relationship
//!!WAS BAD                    scGridPoint* CMM = ByClusterPointer_Get(CHead,(ClusterNeighbor) M);
                    scGridPoint* CMM = ByClusterPointer_Get(CMember,(ClusterNeighbor) M);
                    if(CMM)
                    {
                        // Create inter-scGridPoints communication block in both of the neighboring scGridPoints,
                        // in the right neighbor vector
                        string IGPCB_N = '>' + IDtoString(GP->X,2)+IDtoString(GP->YPosition_Get(),2)+ClusterMembers[M].name;
                        string IGPCB_M = '<' + IDtoString(CMM->X,2)+IDtoString(CMM->YPosition_Get(),2)+ClusterMembers[N].name;
                        CHead->msIGPCB[N] = new scIGPCB(sc_core::sc_module_name(//string("ICBH_").append(IDtoString(ICBN1,GRID_BUS_WIDTH)).c_str()),
                                                   IGPCB_N.c_str()),
                                                   CHead, CMember->msIGPCB[M]);
  //                      CHead->msIGPCB[N]->clock(clock);
                        CMember->msIGPCB[M] = new scIGPCB(sc_core::sc_module_name(//string("ICBM_").append(IDtoString(ICBN1,GRID_BUS_WIDTH)).c_str()),
                                                     IGPCB_M.c_str()),
                                                   CMember, CHead->msIGPCB[N]);
//                        CMember->msIGPCB[M]->clock(clock);
                        CHead->msIGPCB[N]->OtherIGPCB_SetLate(CMember->msIGPCB[M]);  // This was not known when was created
                        // After creating, bind them properly
                        CHead->msIGPCB[N]->ConnectFIFOsTo(CMember->msIGPCB[M]);
                    }
                }
            }
        }
    }
}


/*!
 * \fn void scProcessor::ConnectClusterHeadsToBus(scClusterBus* Bus)
 * \brief Connect cluster head modules to the inter-cluster Buses
 * 
 * The module assumes that the clusters of the scProcessor has already been set up
 * (i.e. must be called after  AbstractTopology::CreateClusters() ).
 * The clusters (and the memories) change messages to each other through the scClusterBus.
 * All modules connected to the scClusterBus have both slave and master interfaces.
 * All but phantom cluster heads are connected; the same topology is assumed
 * as in  AbstractTopology::CreateClusters().
 */ 

    void scProcessor::
ConnectClusterHeadsToBus(scClusterBus* Bus)
{
    // These are complete clusters
    ByPosition_Get(1,9)->ConnectToClusterBus(Bus);
    ByPosition_Get(2,4)->ConnectToClusterBus(Bus);
    ByPosition_Get(4,8)->ConnectToClusterBus(Bus);
    ByPosition_Get(5,3)->ConnectToClusterBus(Bus);
    ByPosition_Get(7,7)->ConnectToClusterBus(Bus);
    ByPosition_Get(8,2)->ConnectToClusterBus(Bus);
    // These next two clusters are incomplete, contain 1+2 members
    ByPosition_Get(9,11)->ConnectToClusterBus(Bus);
    ByPosition_Get(0,0)->ConnectToClusterBus(Bus);
    // The phantom clusters can NOT be connected
}

    void scProcessor::
Reset(void)
{
    // It is assumed that the memory is already loaded with the hexa file
    mProgramCounter = CORE_DEFAULT_ADDRESS;
            //MainMemory->ProgramCounter_Get();
    msHalted = false;
    for(SC_HTHREAD_ID_TYPE h = 0; h < MAX_HTHREADS; h++)
    {
    //!!    msChildrenMask[h] = 0;
        msSignalMask[h].PreAllocated = 0;
        msSignalMask[h].PreAllocatedForTopology = 0;
    }
    for(int i = 0; i < GRID_SIZE_X; i++)
      for(int j = 0; j < GRID_SIZE_Y; j++)
          ByIndex_Get(i,j)->Reset();
    // As part of resetting, populate aliases map with names of the scHThreads
    mAliases.clear();
    for(int i = 0; i < GRID_SIZE_X; i++)
      for(int j = 0; j < GRID_SIZE_Y; j++)
      {
          scGridPoint* G = ByIndex_Get(i,j);
          for(int h = 0; h<MAX_HTHREADS; h++)
          {
              scHThread* H = G->HThread_Get(h);
              Alias_Add(H->StringOfDefaultName_Get(),H);
              Alias_Add(H->StringOfClusterName_Get(),H);
          }
      }


}

    /*!
   * \brief scProcessor::SetMasks_thread
   * This method is called when any of the masks relating operation changes
   * scRegistered method, sensitive to EVENT_SCPROCESSOR.MasksChanged
   */
    void scProcessor::
SetMasks_method(void)
{
        for(SC_HTHREAD_ID_TYPE h=0; h<MAX_HTHREADS; h++)
        {
    ostringstream oss;
    oss << hex << "Unavailable 0x" << UnavailableMask_Get(h)
        << "; Allocated 0x" << msSignalMask[h].Allocated
        << "; PreAllocated 0x" << msSignalMask[h].PreAllocated
        << "; Denied 0x" <<  msSignalMask[h].Denied
        << dec <<"\n";
    DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.MasksChanged: " << oss.str());
    msSignal[h].Denied.write(msSignalMask[h].Denied);
    msSignal[h].Allocated.write(msSignalMask[h].Allocated);
    msSignal[h].PreAllocated.write(msSignalMask[h].PreAllocated);
    }
}


    /*!
     * \brief scProcessor::QTERM_method
     * The scCore informs the scSupervisor that resource situation has changed
     * The purpose of this routine is only monitoring, the interested cores
     * receive the signals directly
     */
    void scProcessor::
QTERM_method(void)
{
        DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.QTERM" );
}


      /*!
       * \brief SignalBit_Change sets a bit in an sc_signal mask
       * The bits are primarily changed in the scGridPoints,
       * and mirrored in scProcessor
       *
       * The scGridPoints sets the corresponding bits, that are connected 
       * to the mask work in the scProcessor
       * \param Signal The corresponding SC_GRIDPOINT_MASK_TYPE word
       * \param GPMask The one-hot bit mask position mask
       * \param State The new logical state of that bit
       * \param S     The string for the debug print
       */
   void scProcessor::
SignalMaskBit_Set(SC_GRIDPOINT_MASK_TYPE &Signal, SC_GRIDPOINT_MASK_TYPE GPMask,
                  bool State, string S)
{
       for(SC_HTHREAD_ID_TYPE h=0; h<MAX_HTHREADS; h++)
       {
    DEBUG_ONLY(SC_GRIDPOINT_MASK_TYPE OldSignal = Signal);
    Signal &= ~GPMask;
    if(State) Signal |= GPMask;
    msSignalMask[h].Available = AvailableMask_Get(h);
    DEBUG_PRINT_IF_DIFFERENT(S.c_str() << " mask changed  to 0x" << hex <<  Signal  << dec, OldSignal, Signal);
    DEBUG_PRINT_IF_DIFFERENT("Available mask changed  to 0x" << hex << msSignalMask[h].Available  << dec, OldSignal, Signal);
       }
}

    SC_GRIDPOINT_MASK_TYPE scProcessor::
UnavailableMask_Get(SC_HTHREAD_ID_TYPE h)
{ return msSignalMask[h].PreAllocated
       | msSignalMask[h].Allocated
       | msSignalMask[h].Denied;
}




    /*!
       * \brief scProcessor::ChildFindFor
       * \param Parent the potential parent
       * \param CPT the preferred core (clustered) type
       * \return the requested core that can be allocated or NULL if not found
       *
       * The core preferences can be: cpt_Head, cpt_Member, cpt_Neighbor, cpt_AnyCore
       * 1./ cluster head (the most limited resource)
       * 2./ cluster member (a core from the same cluster, ordinary members first)
       * 3./ immediate neighbor (in the same cluster first)
       * 4./ any available core
       *
       * In the simplest form, get the next available core,
       * with the lowest sequence number. Can be replaced by considering temperature,
       * utilization, etc
       *
       */
      scGridPoint* scProcessor::
ChildFindFor(scGridPoint* Parent, CorePreference_t CPT=cpt_AnyCore)
{
       scGridPoint *GP;
       // For the processor,  and always take a cluster head if cpt_AnyCore asked
       if(!Parent && CPT==cpt_AnyCore) CPT = cpt_Head;
       // Try first if we have available cluster head
       //!! Consider threads here
       if(cpt_Head == CPT )
       {
            for(unsigned int i = 0; i<mClusters.size(); i++)
            {
                GP = ClusterHead_Get(i);
                if(GP->IsAvailable()) return GP;   // OK, a free cluster head found
            }

            if(Parent)
                CPT = cpt_Member;   // No available head found, attempt a member
            else
                CPT = cpt_AnyCore;  // // No available head found; for the processor, take any core
       }

       if(cpt_Member == CPT)
       { // Find a member helper for 'Parent'
           ClusterAddress_t CA = Parent->ClusterAddress_Get();
           for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
           {
                scGridPoint* GP = ByClusterMember_Get(CA.Cluster, N);
                if(GP->IsAvailable()) return GP;   // OK, a free cluster member found
           }
           // No "ordinary" member found, attempt a "corresponding" member
           for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
           {
                scGridPoint* OCore = ByClusterMember_Get(CA.Cluster, N); // This is an ordinary member
                ClusterAddress_t CB = OCore->ClusterAddress_Get();
                for(ClusterNeighbor NN = cm_North; NN <= cm_NW; N = ClusterNeighbor((int)NN + 1))
                {
                    scGridPoint* GP = ByClusterMember_Get(CB.Cluster, NN);
                    if(GP->IsAvailable() // OK, a free corresponding member found
                       && ( cm_Head  != GP->ClusterAddress_Get().Member)  // And, it not a free cluster head
                            ) return GP;
                }
                // Also, no " corresponding member" found
           }
           CPT = cpt_AnyCore;  // // The last chance: take any core
       }
       // Any core would do
       SC_GRIDPOINT_MASK_TYPE MyMask = 1;
       for (int i=0; i<MAX_GRIDPOINTS; i++)
       {
          scGridPoint *GP = ByIDMask_Get(MyMask);
          if(GP->IsAvailable()) return GP;
          MyMask += MyMask;   // Get next core
       }
      return (scGridPoint*)NULL ; // No available core found
}

//      typedef enum { hpt_Head, hpt_Member, hpt_Neighbor, hpt_Any} HThreadPreference_t;

    scHThread* scProcessor::
HThreadFindFor(scHThread* Parent, HThreadPreference_t HPT) ///< find an scHThread for this parent
{
    scHThread* H = nullptr;
    // Try first if we have available cluster head
    // Presently, only 'Any core will do' is elaborated
    if(hpt_Any == HPT )
    {
        scGridPoint* GP;
        for(unsigned int i = 0; i<MAX_GRIDPOINTS; i++)
        {
            GP = ByID_Get(i);   // Get a
            if(GP->IsAvailable())
            {   // OK, a free cluster head found, look for an scHThread
                H = GP->AllocateAHThread();
                if(H) return H;
                // Else, look further
            }
         }
    }

#ifdef later
    if(hpt_Head == HPT )
    {
        scGridPoint* GP;
        for(unsigned int i = 0; i<mClusters.size(); i++)
        {
            GP = ClusterHead_Get(i);
            if(GP->IsAvailable())
            {   // OK, a free cluster head found, look for an scHThread
                H = GP->AllocateAHThread();
                if(H) return H;
            }

         }
    }
        if(hpt_Member == HPT)
        { // Find a member helper for 'Parent'
/*            ClusterAddress_t CA = Parent->ClusterAddress_Get();
            for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
            {
                 scGridPoint* GP = ByClusterMember_Get(CA.Cluster, N);
                 if(GP->IsAvailable()) return GP;   // OK, a free cluster member found
            }
            // No "ordinary" member found, attempt a "corresponding" member
            for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
            {
                 scGridPoint* OCore = ByClusterMember_Get(CA.Cluster, N); // This is an ordinary member
                 ClusterAddress_t CB = OCore->ClusterAddress_Get();
                 for(ClusterNeighbor NN = cm_North; NN <= cm_NW; N = ClusterNeighbor((int)NN + 1))
                 {
                     scGridPoint* GP = ByClusterMember_Get(CB.Cluster, NN);
                     if(GP->IsAvailable() // OK, a free corresponding member found
                        && ( cm_Head  != GP->ClusterAddress_Get().Member)  // And, it not a free cluster head
                             ) return GP;
                 }
                 // Also, no " corresponding member" found
            }
            CPT = cpt_AnyCore;  // // The last chance: take any core
            */
        }
//        , hpt_Any
        if(hpt_Neighbor == HPT)
        {

        }
        // Any core would do
 /*       SC_GRIDPOINT_MASK_TYPE MyMask = 1;
        for (int i=0; i<MAX_GRIDPOINTS; i++)
        {
           scGridPoint *GP = ByIDMask_Get(MyMask);
           if(GP->IsAvailable()) return GP;
           MyMask += MyMask;   // Get next core
        }
        */
#endif //later
     return nullptr;    // No available thread found
}

    /**
    * 	@brief		This procedure allocates a child core for the Parent
    *  The Parent can be either a physical core or NULL, representing the owner scProcessor
    *
    * 	@param[in] Parent	The root core the allocated child core belongs to
    * \param CPT the preferred core (clustered) type
    * 	@return		The allocated scCore*, NULL if no success
    * 	@sa		scProcessor::Core_Deallocate(), scCore::Allocate(), scCore::Deallocate()
    */

        // Allocate a child core for the Parent core
        scGridPoint* scProcessor::
ChildAllocateFor(scGridPoint* Parent, CorePreference_t CPT)
{
    scGridPoint *GP =  ChildFindFor(Parent, CPT); ///< find a core for this parent
    if(GP)
        GP = GP->AllocateFor(Parent);
    return GP;
}

/*!
 * \brief Return the string-form ID of an scHThread of the processor
 * \return the string as
 */
    string scProcessor::
StringID_Get(scHThread* H)
{
           string ID="P";
/*           ID = ID.append(to_string(ID_Get()));   // Add the scGridpoint number
           if(Owner_Get()->InstanceCount_Get()-1) ID = ID.append("_").append(to_string(Owner_Get()->InstanceCount_Get()-1));
           ID = ID.append(".").append(to_string(ID_Get()));
           if(InstanceCount_Get()) ID = ID.append("_").append(to_string(InstanceCount_Get()));
*/           return ID;
}

    scHThread* scProcessor::
HThreadAllocateFor(scHThread* Parent, HThreadPreference_t HPT)
{
    scHThread* H = HThreadFindFor(Parent, HPT);
    if(H)
        H = H->AllocateFor(Parent);
    return H;
}

        // PreAllocate a child core for the Parent scGridPoint
        // param CPT the preferred core (clustered) type

        scGridPoint* scProcessor::
ChildPreAllocateFor(scGridPoint* Parent, CorePreference_t CPT)
{
    scGridPoint *GP =  ChildFindFor(Parent, CPT); ///< find a core for this parent
    if(GP)
          GP = GP->PreAllocateFor(Parent);
    return GP;
}



/*!
 * \brief scProcessor::START_thread
 * This thread is waiting for a START_event and processes it
 */
    void scProcessor::
START_thread(void)
{
  DEBUG_PRINT_OBJECT("EVENT_PROCESSOR.START_thread started@scProcessor");
   while(true)
    {
    DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.START");



    wait(EVENT_PROCESSOR.START);
//    wait(15,SC_NS);
    DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.START");
    msHalted = false;
    wait(SCTIME_CLOCKTIME);
    Reboot();
    }
}

     // Create a new QT for TheParent, allocate a core if TheChild is NULL
    scGridPoint* scProcessor::
  doQCREATE(scGridPoint* TheParent, scGridPoint* TheChild,
            SC_GRIDPOINT_MASK_TYPE CloneMask,
            SC_GRIDPOINT_MASK_TYPE BackLinkMask,
            CorePreference_t CPT)
  {
      scGridPoint* TheCore;
      if(TheChild)
          TheCore = TheChild; // The core is pre-allocated
      else
          TheCore = ChildAllocateFor(TheParent, CPT);
      if(!TheCore) return (scGridPoint*)NULL; // No available cores found, help yourself with NULL
      // Everything OK, do the work
  //!!    SetCloningMasks(TheCore);
  //!!    TheCore->CloneRegisterFileFrom(TheParent, CLONE_REGISTER_MASK, true);
      return TheCore;
  }


    void
scProcessor::Reboot()
{
    scHThread* RebootThread = doReboot(-1);
        /*          scCore* RebootCore = doReboot(-1);    // Find first an available core
                  DEBUG_EVENT("SENT  NEXT_event");
                  RebootCore->NEXT_event.notify(SC_ZERO_TIME);
              */

}

    /*      int32_t a = 12; int32_t b = 0;
        mClusterMemoryFast->write(&a,12);
        mClusterMemoryFast->read(&b,12);
          int32_t c = 34; int32_t d = 0;
        mClusterMemoryFast->write(&c,12);
        mClusterMemoryFast->read(&d,12);*/

/*!
 * \brief scProcessor::HALT_thread
 * This thread is waiting for a HALT_event and processes it
 */
void scProcessor::HALT_thread(void)
{
//    DEBUG_PRINT_OBJECT("HALT_thread started");
    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.HALT");
        wait(EVENT_PROCESSOR.HALT);
        DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.HALT");
        msHalted = true;
        LOG_TRACE(string("HALTed"));
    }
}

/*!
 * \brief scProcessor::SUSPEND_thread
 * Since the simulator is the outmost object of the system, it must be suspended for the time of the work
 *
 */
void scProcessor::
SUSPEND_thread(void)
  {
//    DEBUG_PRINT_OBJECT("SUSPEND_thread started");
    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.SUSPEND");
        wait(EVENT_PROCESSOR.SUSPEND);
        DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.SUSPEND");
        msSuspended = true;
    }
  }

void scProcessor::
RESUME_thread(void)
  {
//    DEBUG_PRINT_OBJECT("RESUME_thread started");
    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.RESUME");
        wait(EVENT_PROCESSOR.RESUME);
        DEBUG_EVENT_OBJECT("RCVD EVENT_PROCESSOR.RESUME");
//        LOG_INFO_SC("RCVD RESUME_event@scProcessor");
//        SC_CORE_MASK_TYPE SuspendMask= ChildrenMask_Get();
        msSuspended = false;
    }
  }


    void scProcessor::
DeniedMaskBit_Set(scGridPoint* C, bool P)
{
    SignalMaskBit_Set(msSignalMask[C->ClusterAddress_Get().HThread].Denied, C->IDMask_Get(), P, "Denied");
//    DEBUG_PRINT("Denied mask changed  to " << hex <<  msSignalMask.Denied  << dec);
}

    void scProcessor::
AllocatedMaskBit_Set(scGridPoint* C, bool P)
{
     SignalMaskBit_Set(msSignalMask[C->ClusterAddress_Get().HThread].Allocated, C->IDMask_Get(), P, "Allocated");
//     DEBUG_PRINT("Allocated mask changed  to " << hex << msSignalMask.Allocated  << dec);
//             DEBUG_PRINT("Available mask changed  to " << hex << msMask.Available << dec);
}

    void scProcessor::
PreAllocatedMaskBit_Set(scGridPoint* C, bool P)
{
    SignalMaskBit_Set(msSignalMask[C->ClusterAddress_Get().HThread].PreAllocated, C->IDMask_Get(), P, "PreAllocated");
    DEBUG_PRINT("PreAllocated mask changed to " << hex << msSignalMask[C->ClusterAddress_Get().HThread].PreAllocated  << dec);
    DEBUG_PRINT("Available mask changed  to " << hex << msSignalMask[C->ClusterAddress_Get().HThread].Available  << dec);
}

    void scProcessor::
PreAllocatedForProcMaskBit_Set(scGridPoint* C, bool V)
    {   msMask[C->ClusterAddress_Get().HThread].PreAllocatedForTopology &= ~C->IDMask_Get();
        if(V) msMask[C->ClusterAddress_Get().HThread].PreAllocatedForTopology |= C->IDMask_Get();
    }


    /*!
     * \brief scProcessor::Populate
     * \param Specials pre-created, subclassed from scGridPoint, points
     */
    void scProcessor::
Populate(vector<scGridPoint*>& Specials)
{
     // Create communication grid point objects
    scGridPoint* C;
    for(int i = 0; i < GRID_SIZE_X; i++, i++) // Twice because of the hexagonal grid
    {
        for(int j = 0; j < GRID_SIZE_Y; j++)
        {
            int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
                if(!mGrid.at(i).at(j))
                {// This is pre-populated by a special
                    C = new scGridPoint(sc_core::sc_module_name(string("GP").append(IDtoString(LinearAddress1,GRID_BUS_WIDTH)).c_str()),
                        this,// LinearAddress1,
                        GridPoint(i,j), true//StandAlone
                    );
                    mGrid.at(i).at(j)= C; // Store the pointer in the grid
                }
                int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
                if(!mGrid.at(i+1).at(j))
                {// This is pre-populated by a special
                    C = new scGridPoint(sc_core::sc_module_name(string("GP").append(IDtoString(LinearAddress3,GRID_BUS_WIDTH)).c_str()),
                        this, //LinearAddress3,
                        GridPoint(i+1,j), true//StandAlone
                    );
                    mGrid.at(i+1).at(j) = C; // Store the pointer in the shadow-column
                }
        }
    }
 }
 /*!
  * \brief StringOfMessage_Get
  *
  * \param Message the message to be decoded
  * \return the memory parameters from the Message
  *
  */
 string scProcessor::
    StringOfMessage_Get(scIGPMessage* Message)
 {
    ostringstream oss;
    assert(Message);
    oss << '<' << StringOfSender_Get(Message) << " => ";
    oss << StringOfReceiver_Get(Message);
    if(msg_Mem == Message->Type_Get())
        {} // oss << StringOfMemory_Get(Message);
    else if(msg_Qt == Message->Type_Get())
    {
        oss << " Qt%" << StringOfRegisters_Get(Message);
    }
    else if(msg_Reg == Message->Type_Get())
        oss << " Reg%" << StringOfRegisters_Get(Message);
    else if(msg_Coop == Message->Type_Get())
        oss << " Coo%" << StringOfCooperation_Get(Message);
    else if(msg_Coop == Message->Type_Get())
        oss << " Neu%" << StringOfNeural_Get(Message);
    oss << ">";
    return oss.str();
 }


    string scProcessor::
StringOfMemory_Get(scIGPMessage* M)
{
    ostringstream oss;
    oss << hex  << "0x" << std::setw(FMEMORY_ADDRESS_WIDTH/4) << std::setfill('0') << M->Header3Address_Get() << ":";
    for(int i = 4; i<M->Length_Get(); i++)
    {
        oss << "0x" << hex << M->BufferContent_Get(i-4);
        if(i < M->Length_Get()-1) oss << ",";
    }
    oss << dec;
    if( 4 == M->Length_Get()) oss << M->ReplyLength_Get();
    return oss.str();
}
    string scProcessor::
StringOfRegisters_Get(scIGPMessage* M)
{
    ostringstream oss;
    IGPMessageType T = M->Type_Get();
    uint32_t Mask = M->Header3Address_Get();
    int32_t i=0; int32_t index=0; int32_t C;
    while(Mask)
    {
       if(Mask & 1)
       {    // Write the corresponding register
           C = M->BufferContent_Get(index++);
           oss << RegisterName_Get(i) << "=0x" << hex << C << dec;
//           if(C) oss << '(' << C << ')';
           if(Mask/2) oss << ",";
       }
       Mask /=2;
       ++i;
    }
    if(msg_Qt == T)
    {   // It must contain also the PC and state flags
        oss << "!0x" << hex << M->BufferContent_Get(index++);
        C = M->BufferContent_Get(index++);
        if(C) oss << ":0x" << hex << C;
    }
//    oss << dec;
    return oss.str();
}

    string scProcessor::
StringOfCooperation_Get(scIGPMessage* Message)
{
    ostringstream oss;
    assert(Message);
    MessageFeatures_t F = Message->FeatureWord_Get();
    switch(F.SubKey)
    {case 0:
    default: assert(0); break;
    }
    return oss.str();
}

    string scProcessor::
StringOfNeural_Get(scIGPMessage* Message)
{
    ostringstream oss;
    assert(Message);
    MessageFeatures_t F = Message->FeatureWord_Get();
    switch(F.SubKey)
    {case 0:
    default: assert(0); break;
    }
    return oss.str();
}

    string scProcessor::
RegisterName_Get(int Index)
{
    ostringstream oss;
    oss << "R[" << Index << "]";
    return oss.str();
}

    string scProcessor:: ///< Return the string describing the message
StringOfSender_Get(scIGPMessage* M)
{
        string MemString[] = {"Mem0%", "Mem1%", "Mem2%", "Mem3%"};
    ClusterAddress_t CA = M->ReplyToAddress_Get();
    if(IsMemory(CA))
    {
        // The sender is the "far" memory
        return MemString[CA.Member]+StringOfMemory_Get(M);
    }
    else
    {   // The sender is an scGridPoint
        scGridPoint* GP = ByClusterAddress_Get(CA);
        return GP->StringOfClusterAddress_Get();
    }
}

    string scProcessor::
StringOfReceiver_Get(scIGPMessage* M)
{
    string MemString[] = {"Mem0%", "Mem1%", "Mem2%", "Mem3%"};
    ClusterAddress_t CA = M->DestinationAddress_Get();
    if(IsMemory(CA))
    {   // The destination is the "far" memory
        if(4==M->Length_Get())
        return MemString[CA.Member]+"R" + StringOfMemory_Get(M);
        else return MemString[CA.Member] + StringOfMemory_Get(M);
    }
    else
    {   // The sender is an scGridPoint
        scGridPoint* GP = ByClusterAddress_Get(CA);
        return GP->StringOfClusterAddress_Get();
    }
}

/*    void scProcessor::
ChildrenMaskBit_Set(scGridPoint* C)
  { msChildrenMask[C->ClusterAddress_Get().HThread] |= C->IDMask_Get();}	///< Set the mask bit of the allocated cores
    void scProcessor::
ChildrenMaskBit_Clear(scGridPoint* C)
  { msChildrenMask[C->ClusterAddress_Get().HThread] &= ~C->IDMask_Get();}  ///< Clear the mask bit of the allocated cores
*/

    string scProcessor::
PrologString_Get(void)
{
    ostringstream oss;
    string SG = "Proc";
//??    oss  << '@' << Time_Get()  <<": " << SG.c_str();
     oss << '%';
//    while(oss.str().size()<24) oss << ' ';
    return oss.str();
}


    /*!
        * \brief Return the string-form ID of the thread
        * \return the string as
        * The string ID comprises "P" and the index of the child in the list of children
        * The string comprises (string forms of) scGrid ID + scHThread ID
        */
   string scProcessor::
StringOfID_Get(scHThread* H)
{
    string ID = "P";
    int MyH = Child_Find(H);
    if(-1 == MyH)
    {    // We did not find the child
        return ID.append("?").append(to_string(mChildren.size()));
    }
    else
    {    // we did find the child
        return ID.append(to_string(MyH));
    }
}

   /*!
    * \brief Find child C in the list of children, immediately allocated for the processor
    * \param C the address of the child
    * \return the index if found, else -1
    */
   int scProcessor::
Child_Find(scHThread* C)
{
   int32_t right = mChildren.size(); // one position passed the right end
   if(!right) return -1;   // Not found if no children
   int32_t mid = 0, left = 0 ;
   uint64_t key = (uint64_t) nullptr;
   while (left < right) {
       if((uint64_t)mChildren[mid] == (uint64_t)C)
            return mid; // Accidentally, we found it
       mid = left + (right - left)/2;
       if (key > (uint64_t) mChildren[mid]){
                left = mid+1;
            }
          else if (key < (uint64_t) mChildren[mid]){
            right = mid;
          }
       }
    return -1;
}

 //  bool HThreadCompare (scHThread* i,scHThread* j) { return ((uint64_t)i<(uint64_t)j); }
//   scHThread* scProcessor::
//ByAliasName_Get(string N){return Aliases->ByName_Get(N);}

   void scProcessor::
Child_Insert(scHThread* C)
{
   assert(C);   // Cannot be inserted
   mChildren.push_back(C);
   sort(mChildren.begin(), mChildren.end());   // Keep the children sorted
}

   void scProcessor::
Child_Remove(scHThread* C)
{
  assert(C);  // Cannot be removed
  uint MySize = mChildren.size()-1;
  std::remove (mChildren.begin(), mChildren.end(), C);
  mChildren.resize(MySize);
  mChildren.shrink_to_fit();   // Keep the list compact
}


   scHThread* scProcessor::
ByName_Get(string N)
{
   std::map<string,scHThread*>::iterator it;
   it = mAliases.find(N);
   if (it != mAliases.end())
       return it->second;
   else
       return (scHThread*)nullptr;
}



   /*!
    * \brief Set an alias name for the thread
    * \param A the new alias name
    * \return true if the name used for the first time
    */
   bool scProcessor::
StringOfAliasName_Set(string A, scHThread *H)
{   assert(H);  // Do not handle null pointers
   assert(A.length()); // Do not map empty strings
   if(ByName_Get(A))
       return false;   // The name was set already, do nothing
   Alias_Add(A,H);
   H->StringOfAliasName_Set( A);
   return true;
}


   void scProcessor::
ReadTheThreeLevels(QString MyGroup)
{
       // See in NeurerSimulator.cpp
}

   void scProcessor::
HandleAliasesPass(string FileName, bool Pass2)
{
   if(!QFile(FileName.c_str()).exists())
   {
       qWarning() << "File '" << FileName.c_str() << "does not exist";
       return;
   }
   QFile MyFile(QString(FileName.c_str()));
   if (!MyFile.open(QIODevice::ReadOnly | QIODevice::Text))
   {
       qWarning() << "Could not open file '" << FileName.c_str();
       return;
   }
   // The file found, try to read it
   QTextStream in(&MyFile);
   DEBUG_PRINT("Pass " << (int) Pass2 << ": Mapping neuron names from file '" << FileName.c_str()<< "'");
   int LineCount = 0;
   while (!in.atEnd())
   {
       QString line = in.readLine(); LineCount++;
       if(!line.isEmpty())
       {// OK, we have something, hopefully an alias name
        // It is expected to be something like
        // NewAlias = OldAlias
           QStringList ThisKey = line.split("=");
           if (ThisKey.size()>2)
               qInfo() << "Too many args in line '" << line.toStdString().c_str() << "'";
           QString NewKey = ThisKey.at(0).trimmed(); QString OldKey = ThisKey.at(1).trimmed();
           scHThread* HT = ByName_Get(NewKey.toStdString());
           if(!Pass2)
           {
               if(HT)
               {
                   qCritical() << "New key " << NewKey << " is in use already"<< "in file " << FileName.c_str() << "@line " << LineCount << "'";
                   break;
               }

           if(OldKey.size())
           {// The name of an old key is listed, find that key in the list map
               HT = ByName_Get(OldKey.toStdString());
               if(HT)
               {   // The old key (and the scHThread) found
  //                 qInfo() << "Duplum " << OldKey << " found in file " << FileName.c_str() << "@line " << LineCount << "'";
/*                    if(HT->IDMask_Get() & Processor_Get()->AllocatedMask_Get())
                   {   // The gridpoint was already allocated
                       qWarning().nospace() << "GridPont " << GP->Name_Get().c_str() << " was allocated already as " << OldKey.toStdString().c_str();
                   }
                   else
                   { // It is OK, allocate if for the processor
                       HT->AllocateFor(nullptr);
                   }*/
               }
               else
               {   // No, we do not have such and old key
                   qWarning() << "Not found 'Old key'" << OldKey << "in file " << FileName.c_str() << "@line " << LineCount;
//                    HT = Processor_Get()->ChildAllocateFor(nullptr);
//??                   HT = Processor_Get()->HThreadAllocateFor(nullptr);
               }
           }
           else
           {   // No, the old key is empty, meaning give me a new key
//               HT = Processor_Get()->ChildAllocateFor(nullptr);
      //??         HT = Processor_Get()->HThreadAllocateFor(nullptr);
   //??            qWarning() << "A new module " << HT->name() << " for alias " << NewKey << " taken in file " << FileName.c_str() << "@line " << LineCount;
               // the new alias is taken already
           }

           assert(HT); // Anyhow, we have a gridpoint for our mapping
           Alias_Add(NewKey.toStdString(),HT);
           DEBUG_PRINT("Added alias name to the list: '" << NewKey.toStdString() << "' for '" << HT->name() <<"'");
           }
           else {
               // We are in the second pass, read init parameters
               ReadTheThreeLevels(NewKey);
           }
           // Given that the alias is listed, attempt to read its settings
       }// of reading a line
   }// of reading the file
}// HandleAliasesPass

   void scProcessor::
HandleAliases(string FileName)
{
   HandleAliasesPass(FileName,false);// Add the names
   HandleAliasesPass(FileName,true);
}// HandleAliases


