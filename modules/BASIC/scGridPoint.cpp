/** @file scGridPoint.cpp
 *  @brief Function prototypes for the scGrid simulator,
 */
/*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

// #include "scGridPoint.h"
#include "scProcessor.h" // includes scGridPoint.h
#include "scIGPCB.h" //#include "scIGPfifo.h"
//#include "scIGPMessage.h" //#include "Utils.h"
//#include "scLatchFIFO.h"
#include "scSimulator.h"

//#include "scGPMessagefifo.h"
//#include "Memory.h"
class scClusterBus;     // Shall be include
/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h', and are undefined in that file
//#define MAKE_LOG_TRACING    // We want to have trace messages
//#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"

string DebugRoute;  // This will go to Macros.h
#include "scClusterBusMemorySlow.h"
#include "scHThread.h"
extern scClusterBusMemorySlow* MainMemory;
extern NamedEnum_struct ClusterMembers[8];

//scSimulator* TheSimulator = NULL;

bool IsMemory(ClusterAddress_t CA)
{   CA.Member = 7; ClusterAddress_t DCA = MemoryClusterAddress; return DCA.ToInt() == CA.ToInt();}

bool IsMemory0(ClusterAddress_t CA)
{   ClusterAddress_t DCA = Memory0ClusterAddress; return DCA.ToInt() == CA.ToInt();}
bool IsMemory1(ClusterAddress_t CA)
{   ClusterAddress_t DCA = Memory1ClusterAddress; return DCA.ToInt() == CA.ToInt();}
bool IsMemory2(ClusterAddress_t CA)
{   ClusterAddress_t DCA = Memory2ClusterAddress; return DCA.ToInt() == CA.ToInt();}
bool IsMemory3(ClusterAddress_t CA)
{   ClusterAddress_t DCA = Memory3ClusterAddress; return DCA.ToInt() == CA.ToInt();}

//extern string DebugRoute; //!!
/*
 * Basic SystemC functionality of grid modules + grid topologic position handling
 */
extern bool UNIT_TESTING;	// Whether in course of unit testing

scGridPoint::scGridPoint(sc_core::sc_module_name nm
                        ,scProcessor* Processor
                        ,const GridPoint GP, bool StandAlone):
    GridPoint(GP), scIGPMessage_if(nm,Processor)
    //sc_core::sc_module( nm)
    ,mHThreadFetch(nullptr)            // The actual number of the handled HThread fetch
    ,mHThreadExec(nullptr)            // The actual number of the handled HThread execute
    ,msParent(nullptr)
    ,msMemoryAddress(MEMORY_DEFAULT_ADDRESS)
    ,mInstanceCount(0)
    // ,mCoreStateBit(0) // Done in Reset()
{
    msRegisterLatch = new RegisterLatch_fifo(string(nm).append("Latch").c_str());
    msID =Processor->LinearAddressFromCoordinates_Get(GP.X, GP.Y);               // The physical ID of the gridpoint, unchangeable
    msMask.ID = (1 << msID);     // A redundant storage, to avoid on the flight calculations
    msMask.Children = 0;    // Initially, we have no children
    msMask.AllocatedHThreads = 0;    // Initially, we have no threads
    // The followings can only be done once, only in the constructor
    // Establish neigborship facility to the default stage 'no neighbours'
    for(ClusterNeighbor N = cm_Head; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
      { // Initially, there are no FIFOs and no neighbors
        msIGPCB[N] = (scIGPCB*)NULL;
      }
    // This function is virtual: creates the scHThreads for the scGridoint
    CreateThreads();
    // Handle HW signals: binding can only be in the constructor
      msPort.Denied(msSignal.Denied);
      msPort.Allocated(msSignal.Allocated);
      msPort.PreAllocated(msSignal.PreAllocated);
      msPort.Available(msSignal.Available);
      msPort.Meta(msSignal.Meta);
      msPort.Wait(msSignal.Wait);
      msPort.Sleeping(msSignal.Sleeping);     // Connected to sleeping, but no mask in Topology
      // by default, no binding to inter-cluster bus
      // Will be set ONLY for cluster head when creating the clusters
      mMaster_if = NULL;

    SC_METHOD(SIGNAL_method);
        sensitive << EVENT_GRID.SIGNAL_StateBit;
    dont_initialize();
    SC_THREAD(FETCH_thread);
        sensitive << EVENT_GRID.MakeFetch;
    SC_THREAD(EXEC_thread);
        sensitive << EVENT_GRID.MakeExec;
    SC_THREAD(SEND_thread);
        sensitive << EVENT_GRID.MakeSend;
     if(StandAlone)
    {
        //!! Removed temporarily, until NEXT in AbstractCore fully implemented
//!!        SC_METHOD(NEXT_thread);
//!!            dont_initialize();
//!!            sensitive << EVENT_GRID.NEXT;
/*        SC_METHOD(CREATE_method);
            dont_initialize();
            sensitive << EVENT_GRID.CREATE;*/
        Reset(); // Set the signals to theit default state
    }
    // The rest can be called also from outside the constructor
}// of scGridPoint::scGridPoint

/* Create the HThreads that share this processing resource */
        void scGridPoint::
CreateThreads(void)
{
    for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
    {
        scHThread *N = new scHThread(string("H").append(IDtoString(H,HTHREAD_BUS_WIDTH)).c_str(),this, H);
        AddHThread(H, N);
    }
}

// Add scHthread alias names to the default maps.
// The first schThread  is added in both "x" and "x/0" form, to enable using both forms
        void scGridPoint::
AddDefaultAliasNames(void)
{
    for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
    {   // Here the name does not contain "/0" for brevity
        scHThread *N = HThread_Get(H);
        Processor_Get()->Alias_Add(N->StringOfDefaultName_Get(),N);
        Processor_Get()->Alias_Add(N->StringOfClusterAddress_Get(),N);
    }
    scHThread *N = HThread_Get(0);
    Processor_Get()->Alias_Add(N->StringOfDefaultName_Get().append("/0"),N);
    Processor_Get()->Alias_Add(N->StringOfClusterAddress_Get().append("/0"),N);
}


//!!EVENT_GRID.FetchReady must be issued when fetch finished!!
//! After that, one must wait for SC_ZERO_TIME
//! Similarly for Exec
//!
//!
//!! Removed temporarily, until NEXT in AbstractCore fully implemented
#if 0
void scGridPoint::
NEXT_thread(void)
{
while(true)
{
    DEBUG_EVENT_OBJECT("WAIT  EVENT_GRID.NEXT");
    wait(EVENT_GRID.NEXT);
    DEBUG_EVENT_OBJECT("RECVD EVENT_GRID.NEXT");
/*
    // The currect instruction  may be fetched or non fetched
    FinishPrefetching();
    //        PrepareNextInstruction(); included above

    // The previous instruction is finished, the current one not yet started
    HandleSuspending(); // Suspend if the simulator asks so

  // Now we surely have a fetched instruction: execute it

  // Instruction performance is handle separatedly for conventional and meta instructions
    sc_time ExecDelay = sc_time_stamp();
    if(exec.Stage.CoreStatus == STAT_META)
        HandleMetaInstruction();
    else
        HandleConventionalInstruction();
    TerminateExecution();
    performance->ExecTimeAdd(sc_time_stamp()-ExecDelay);
    */
}
}//scGridPoint::   NEXT_thread
#endif //0

#if 0
/*!
 * \brief scCore::HandleSuspending
 * Suspends execution of processing when formerly a the simulator issued a 'SUSPEND' instruction
 */
void scGridPoint::
HandleSuspending()
{
    if( TheSimulator)    // IF a simulator is also used
        if(TheSimulator->StepwiseMode_Get())  // and it is set to 'StepWise' mode
            Processor_Get()->SuspendedBit_Set(true); // set the bit, so a 'resume' can continue
/*    if(Processor_Get()->IsSuspended())
    { // The operation is suspended, wait until resumed
        DEBUG_EVENT("WAIT  RESUME_event");
        wait(Processor_Get()->EVENT_PROCESSOR.RESUME);
        DEBUG_EVENT("RECVD   RESUME_event");
    }*/
}

#endif //0

/*!
 * \fn void scGridPoint::Reset(void)
 * 
 * \brief Reset all status bits
 * 
 * */
void scGridPoint::
Reset(void)
{
    msStatus.Denied =false;  // Whether this module is denied from any reason
    msStatus.Allocated = false; // Whether this module is allocated by another one
    msStatus.PreAllocated = false; // Whether this module isMyCore->doFetchInstruction( preallocated by another one
    msStatus.Available  = true; // A derived signal, see below
    msStatus.Meta = false;  // Whether this module is executing meta-instruction
    msStatus.Wait = false;  // Whether this module is blocked by some condition
    DeniedBit_Set(false);
    AllocatedBit_Set(false);
    PreAllocatedBit_Set(false);
    MetaBit_Set(false);
    WaitBit_Set(false);
    msMask.Children = 0;
    msCooperationMode = ccm_None;
    msRegisterLatch->reset();
    for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
        mHThreads[H]->Reset();
    msMask.AllocatedHThreads = (SC_HTHREAD_MASK_TYPE) 0;
    mObservedHThreads = (SC_HTHREAD_MASK_TYPE) 0;  ///
    mGridPointOperatingStateBit = 0;    // Clear operating bits, including groups
//    msICB->Reset();
//    msLinkRegisters = R_NONE;
    BENCHMARK_TIME_RESET(&m_running_time,&m_part_time,&m_sum_time); // Just clear all work storage, for any case
}

scGridPoint::~scGridPoint()
{
}//scGridPoint::~scGridPoint


/*! \fn void scGridPoint::ConnectToClusterBus(scClusterBus* Bus)
 *  \brief Connnect this scGridPoint to the inter-cluster bus
 * 
 * All scGridPoint can be connected, but only the cluster heads are connected
 * The cluster heads can be both slave and master
 *  
 */
       void scGridPoint::
ConnectToClusterBus(scClusterBus* ClusterBus)  // Connect this gridpoint to the inter-cluster
{
    assert(cm_Head == ClusterAddress_Get().Member); // Connect only the cluster heads
    int MaxHeads = Processor_Get()->NumberOfClusters_Get();
    int index;
    for(index = 0; index<MaxHeads; index++)
    {
        if(Processor_Get()->ClusterHead_Get(index)->IsTheSameAs(this)) break; 
    }
    assert(index<MaxHeads);
    assert(!((X<0)||(X>=GRID_SIZE_X)));
    assert(!((Y<0)||(Y>=GRID_SIZE_Y)));
    mBusPriority = 4 + index;  // the first 4 numbers are reserved for memories
//    DEBUG_PRINT_OBJECT("Connected to bus as slave #" << mPriority);
    ClusterBus->slave_port(*this);
    int32_t Address = 0;
//    bus_port = new std::make_unique<bus_port>("bus_slave")

    mMaster_if = new scClusterBusMaster_blocking(string("CH").append(IDtoString(index,1)).c_str(),
                                                 mBusPriority, Address, // This will probably be not used
                                                true,    // Need to lock the bus for Burst_Write
                                                1);   // Add one more cycle for timeout
//???    mMaster_if->clock(clock);
    mMaster_if->bus_port(*ClusterBus); // Connect to bus as master

}

/*!
 * \brief Write the prepared signal values to the signals
 * 
 * Triggered by event 'GRID_EVENT.SIGNAL_StateBit', i.e. when any state
 * of the gridpoint is changed.
 * Triggers event 'EVENT_SCPROCESSOR.MasksChanged' for the processor
 */
void scGridPoint::
SIGNAL_method(void)
{
//    DEBUG_EVENT_OBJECT("RCVD 'GRID_EVENT.SIGNAL_StateBit'");
    doWriteSignals();    // Do the actual writing
//    DEBUG_EVENT_OBJECT("SENT 'TOPOLOGY.MasksChanged'");
    msProcessor->EVENT_PROCESSOR.MasksChanged.notify(SCTIME_GATE); // Let the topology know that something has changed
}



/*!
 *  \fn void scGridPoint::doWriteSignals(void)
 * \brief do the actual writing of signals
 */
void scGridPoint::
doWriteSignals(void)
{
    msSignal.Denied.write(msStatus.Denied);
    msSignal.Meta.write(msStatus.Meta);
    msSignal.Wait.write(msStatus.Wait);
    msSignal.Allocated.write(msStatus.Allocated);
    msSignal.PreAllocated.write(msStatus.PreAllocated);
    msStatus.Available = !(msStatus.Denied || msStatus.Allocated || msStatus.PreAllocated);
    msSignal.Available.write(msStatus.Available);
    msPort.Denied.write(msSignal.Denied);
    msPort.Meta.write(msSignal.Meta);
    msPort.Wait.write(msSignal.Wait);
    msPort.Allocated.write(msSignal.Allocated);
    msPort.PreAllocated.write(msSignal.PreAllocated);
    msPort.Available.write(msSignal.Available);
}

    void scGridPoint::
ObservedHThreadBit_Set(SC_HTHREAD_MASK_TYPE M, bool B)
    {
        mObservedHThreads  &= ~M;
        if(B) mObservedHThreads |= M;
    }

/*! \fn void scGridPoint::DeniedBit_Set(bool P)
 *
 * \brief Sets the 'Denied' bit of the gridpoint to value of P
 *
 * The denied scGridPoint is not usable, independeply from its reason (permanent or temporary).
 * If the deniedbit is in use, it predicts its state but cannot be
 * allocated or preallocated.
 *
 * \param[in] P The new value of bit 'Denied'
 *
 * @see DeniedBit_get
 * @see AllocateFor
 * @see PreAllocateFor
 */
    void scGridPoint::
DeniedBit_Set(bool P)
{
    msStatus.Denied = P;
    LOG_TRACE_SIGNAL("DeniedBitSet", P);
    Processor_Get()->DeniedMaskBit_Set(this, P);
    AvailableBit_Set(); SleepingBit_Set();
}

    /*! \fn void scGridPoint::AllocatedBit_Set(bool P)
     *
     * \brief Sets the 'Allocated' bit of the gridpoint to value of P
     *
     * An scGridPoint shall be dynamically allocated before using it.
     * Those core will be seem to unavailable for another cores.
     * This bit shows if it is already is allocated
     * \param[in] P The new value of bit 'Allocated'
     *
     * @see AllocatedBit_Get
     * @see PreAllocatedBit_Get
     * @see DeniedBitSet
     * @see AvailableBit_Get
     */

    void scGridPoint::
AllocatedBit_Set(bool P)
{
    if(IsDenied()) return;
    msStatus.Allocated = P;
    LOG_TRACE_SIGNAL("AllocatedBitSet", P);
    Processor_Get()->AllocatedMaskBit_Set(this, P);
    AvailableBit_Set(); SleepingBit_Set();
}

    /*! \fn void scGridPoint::PreAllocatedBit_Set(bool P)
     *
     * \brief Sets the 'PreAllocated' bit of the scGridPoint to value of P
     *
     * In certain situations some scGridPoint is allocated much before actualy
     * using it. Those  pew-allocated points can lated be alloacated, deallocated and
     * reallocated by the requestor ore, while they seem unavailable for the uther cores.1
     *
     * \param[in] P The new value of bit 'PreAllocated'
     */

    void scGridPoint::
PreAllocatedBit_Set(bool P)
{
    if(IsDenied()) return;
    msStatus.PreAllocated = P;
    LOG_TRACE_SIGNAL("PreAllocatedBitSet", P);
    Processor_Get()->PreAllocatedMaskBit_Set(this, P);
    AvailableBit_Set(); SleepingBit_Set();
}
    /*!
     * \brief AvailableBit_Set
     * Set whether the gridpoint is available to work with
     *
     */
    void scGridPoint::
AvailableBit_Set(void)
{ msStatus.Available = ! (msStatus.Allocated
                       || msStatus.PreAllocated
                       || msStatus.Denied);
  LOG_TRACE_SIGNAL("AvailableBit set to ",msStatus.Available);
  EVENT_GRID.SIGNAL_StateBit.notify(SC_ZERO_TIME);
}

    /*!
     * \brief SleepingBit_Set
     * Set whether the module at the gridpoint is sleeping
     * (Denied | Wait | !(Allocated| PreAllocated))
     */
    void scGridPoint::
SleepingBit_Set(void)
{ msStatus.Sleeping =  (msStatus.Denied
                        || msStatus.Wait
                        || ! ( msStatus.PreAllocated
                             || msStatus.Allocated));
  LOG_TRACE_SIGNAL("SleepingBit set to ",msStatus.Sleeping);
  EVENT_GRID.SIGNAL_StateBit.notify(SC_ZERO_TIME);
}

    /*!
     * \brief scGridPoint::MetaBit_Set
     * \param P
     */
    void scGridPoint::
MetaBit_Set(bool P)
{
    msStatus.Meta = P;
    LOG_TRACE_SIGNAL("MetaBitSet", P);
//!!    EVENT_GRID.SIGNAL_StateBit.notify(SC_ZERO_TIME);
}

    /*!
     * \brief scGridPoint::WaitBit_Set
     * \param P
     */
    void scGridPoint::
WaitBit_Set(bool P)
{
    msStatus.Wait = P;
    LOG_TRACE_SIGNAL("WaitBitSet", P);
    EVENT_GRID.SIGNAL_StateBit.notify(SC_ZERO_TIME);
}

    /*!
     * \brief Process the received message
     *
     * The message is at its destination, process is
     * \param Message the message received
     */
    void scGridPoint::
ProcessMessage(scIGPMessage* Message)
{
    ClusterAddress_t CAD = Message->DestinationAddress_Get();
    DEBUG_PRINT_MESSAGE("PROCESSING message ",Message);

    // The message arrived to the final destination.
    if(cm_Broadcast == CAD.Member)
    { //  However: it is a message to everybody, just do it
        CAD.Member = cm_Head;
        CAD.Proxy = cm_Head;
              // Now send it for all members
        DEBUG_PRINT_OBJECT("Broadcasting message should be implemented");
    }
    // Proxy already handled, so just execute
    // First check if it is a delayed message
    int MessageTime = Message->FeatureWord_Get().Time;
    if(MessageTime)
    {   // OK, the message is for us, but we are expected to execute it at a later time
        ostringstream oss;
        oss << "DELAYING by " << MessageTime << " ";
        DEBUG_CONTRIBUTE_STRING(DebugRoute,oss.str(),StringOfMessage_Get(Message).append("\n"));
        TimedIGPMessage_type* DM = new TimedIGPMessage_type();
        DM->Time = sc_time_stamp()+MessageTime*SCTIME_CLOCKTIME;
        DM->Message = Message;
        TimedFIFO->write(*DM);
        // At this point the message is postponed to a later time,
        // But, will come back later, do not delete its content; just delay it
        return;
    }

    // Now way out, we must execute it: no one must take it it is promply for usver,

    IGPMessageType M = Message->Type_Get();
    DEBUG_ONLY(bool ProcessedOK = false;)
    switch(M)
    {
        case msg_Reg: // It is a register message, store it in the FIFO
            DEBUG_PRINT_MESSAGE("RECEIVED 'R' ",Message);
            DEBUG_CONTRIBUTE_STRING(DebugRoute,"DELIVERED 'R' ",StringOfMessage_Get(Message).append("\n"));
            DEBUG_ONLY(ProcessedOK =) doProcessRMessage(Message);
            DEBUG_PRINT_MESSAGE(" DELIVERED 'R' ",StringOfMessage_Get(Message).append("\n"));
            cerr << sc_time_stamp() << "before sending for Msg_Reg_Received by " << StringOfClusterAddress_Get()<<"\n";
            wait(3*SCTIME_CLOCKTIME);
            EVENT_GRID.Msg_Reg_Received.notify(SCTIME_CLOCKTIME);
            cerr << sc_time_stamp() << "After sending for Msg_Reg_Received by " << StringOfClusterAddress_Get()<<"\n";
//            DEBUG_PRINT_OBJECT("RECEIVED 'R' message with mask " << Message->StringOfBufferContent_Get(3));
            break;
        case msg_Qt:  // Create a QT in the already prepared core (made by other core)
            DEBUG_PRINT_MESSAGE("RECEIVED 'Q' message ",Message);
            DEBUG_CONTRIBUTE_STRING(DebugRoute,"DELIVERED 'Q' ",StringOfMessage_Get(Message).append("\n"));
            DEBUG_ONLY(ProcessedOK =) doProcessQMessage(Message);
            DEBUG_PRINT_MESSAGE(" DELIVERED 'Q' ",StringOfMessage_Get(Message).append("\n"));
            wait(SC_ZERO_TIME);
            EVENT_GRID.Msg_Qt_Received.notify(SCTIME_CLOCKTIME);
        break;
        case msg_Mem:
            DEBUG_PRINT_MESSAGE("RECEIVED 'M' message ",Message);
            DEBUG_CONTRIBUTE_STRING(DebugRoute,"DELIVERED 'M' ",StringOfMessage_Get(Message).append("\n"));
            DEBUG_ONLY(ProcessedOK =) doProcessMMessage(Message);
            DEBUG_PRINT_MESSAGE(" DELIVERED 'M' ",StringOfMessage_Get(Message).append("\n"));
            wait(3*SCTIME_CLOCKTIME);
            EVENT_GRID.Msg_Mem_Received.notify(SCTIME_CLOCKTIME);
//            DEBUG_PRINT_OBJECT("RECEIVED 'M' message with address " << Message->StringOfBufferContent_Get(3));
            break;
        default:{}    
    }
    DEBUG_ONLY(assert(ProcessedOK);)
     // Anyhow, the received message must be destroyed
    DEBUG_PRINT_MESSAGE("DELETED ",Message);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"DELETED ",StringOfMessage_Get(Message).append("\n"));
    delete Message;
    return;
}

/*!
 * \brief scGridPoint::doProcessQMessage
 * \param Message The 'Q' message to be processed
 * \return true if message recognized
 *
 * This routine can be called when some other object has an action with the scGridPoint
 * Such cases are
 * QCREATE  - a QT is formally created by another core or the processor
 * Q_KILL   - a QT if forced to exit out of order
 */
    bool scGridPoint::
doProcessQMessage(scIGPMessage* Message)
{
    DEBUG_PRINT_OBJECT("PROCESSING message "+StringOfMessage_Get(Message));
    {
        doCreateQT(Message);
        DEBUG_PRINT_OBJECT("RECEIVED 'Q' message with mask " << Message->StringOfBufferContent_Get(3));
    }

    assert(msg_Qt == Message->Type_Get());
    return true;
}

    /*!
     * \brief Process the received register content
     * \param Message The 'R' message to be processed
     * \return true if message recognized
     *
     * This routine can be called when the scGridPoint received new 'register' contents
     * Such cases are
     * QTERM  - a QT terminated and returned the backlinked register contents
     * QSpec  - a QT finished a special operation and sent the message
     *
     * The scGridPoint can be in state waiting for the content, enabling direct copying
     * or, the contents can be written into a latch FIFO, to be processed later
     *
     */
        bool scGridPoint::
    doProcessRMessage(scIGPMessage* Message)
    {
        DEBUG_ONLY(IGPMessageType T = Message->Type_Get();)
        DEBUG_PRINT_OBJECT("PROCESSING 'R' message "+StringOfMessage_Get(Message));
        DEBUG_ONLY(assert(msg_Reg == T);)
//!!        uint16_t N = Message->Length_Get();
        SC_GRIDPOINT_MASK_TYPE Mask = Message->Header3Address_Get();
        uint16_t R = 0; // Index to register file
//!!        uint16_t index = 0; // Index to message buffer
        while(Mask)
        {   // pack the contents into the backlink FIFO
            if(Mask & 1)
            {   // This register content is present
  //              RegisterLatch_Type* NewLatchedEntry = new RegisterLatch_Type;
//                int32_t I = Message->BufferContent_Get(2);
//                ClusterAddress_t* CA = (ClusterAddress_t*)&I;
                //??scGridPoint* Sender = Processor_Get()->ByClusterAddress_Get(Message->ReplyToAddress_Get());
                        //Processor_Get()->ByClusterAddress_Get(*CA);
                //??SC_HTHREAD_ID_TYPE H = Message->ReplyToAddress_Get().HThread;
                //SC_ADDRESS_TYPE Offset = Sender->QTOffset_Get(H);
                {
                //    ClusterAddress_t SenderCore;
 //                   SC_ADDRESS_TYPE QTOffset;   ///< The offset the content belongs to
   //                 SC_WORD_TYPE RegisterContent;   ///< The content of the register
     //               int8_t RegisterSelect; ///< The register sequence number

                //msRegisterLatch->write()
                }
            }
            Mask /= 2; R++;
         }
        return true;
    }

        /*!
         * \brief scGridPoint::doProcessMMessage
         * \param Message The received message
         * \return true on success
         */
        bool scGridPoint::
    doProcessMMessage(scIGPMessage* Message)
    {
        DEBUG_ONLY(uint16_t M = Message->Type_Get();)
        DEBUG_PRINT_OBJECT("PROCESSING message "+StringOfMessage_Get(Message));
        DEBUG_ONLY(assert(msg_Mem == M);)
        // Fine, we received a memory message: copy the memory content to a buffer
        // and let the top layer to know about
        // Find out which thread received the message
//        ClusterAddress_t CA = Message->DestinationAddress_Get();
//        int ThreadNo = CA.HThread;
        // Copy the received memory content to per-thread temporary storage
        MessageFeatures_t F = Message->FeatureWord_Get();
        int L =F.Length-4;
        assert(L <= MAX_IGPCBUFFER_SIZE );  // Prevent buffer overflow
        scHThread* H = HThreadExec_Get();
        assert(H->IsMemoryExpected());  // Assert on unexpected memory message
        // OK, the thread expects a memory message
        SC_WORD_TYPE* Ptr; int index = 0;
        if(H->IsDataMemoryExpected())
        {   // Copy it to data memory
            Ptr = H->mDataMemoryBuffer;
            H->mDataMemoryAddressBegin = Message->Header3Address_Get();
            H->mDataMemoryAddressEnd = H->mDataMemoryAddressBegin +  L * sizeof(H->mInstrMemoryAddressEnd);;   ///< The first/last contents in the cache
        }
        else
        {   // Copy it to instruction memory
            Ptr = H->mInstrMemoryBuffer;
            H->mInstrMemoryAddressBegin = Message->Header3Address_Get();
            H->mInstrMemoryAddressEnd = H->mInstrMemoryAddressBegin + L * sizeof(H->mInstrMemoryAddressEnd);   ///< The first/last contents in the cache
        }
        // Now make actual copying
        while(L--)
        {
            *Ptr++ = Message->BufferContent_Get(index++);
        }
        // We set also the limiting addresses
        H->MemoryExpected_Set(false);   // Enable next memory transfer
//        H->EVENT_HTHREAD.MemoryContentArrived.notify();
        return true;
    }
   /*!
    * \fn scGridPoint::StringOfClusterAddress_Get
    * \return the string form of the complete cluster address of the gridpoint
    */
   string scGridPoint::
StringOfClusterAddress_Get(void)
{   ostringstream oss;
   #if USE_MODULE_NAMES
       oss << name();
   #else
       oss << GridPoint::StringOfClusterAddressName_Get();
   #endif // USE_MODULE_NAMES
   return oss.str();
}

   /*!
    * \brief scGridPoint::StringOfMessage_Get
    * \param Message the message to be stringified
    * \return the string corresponding to the Message
    */
   string scGridPoint::
StringOfMessage_Get(scIGPMessage* Message)
{   ostringstream oss;
    assert(Message);
    oss << Processor_Get()->StringOfMessage_Get(Message);
/*    if(msg_Qt == Message->Type_Get())
    {
        oss << ";PC=0x" << hex << PC_Get();
        if(ConditionCode_Get()) oss << ";0x" << ConditionCode_Get();
        oss << dec;
    }*/
    return oss.str();
}

    /*!
     * \brief scGridPoint::RouteMessage ; called by the sender scGridPoint or by the
     * receiver thread
     *
     * The target scGridPoint, maybe in other cluster or even processor or rack;
     * the delivery is transparent for both the sender and the receiver
     *
     * \param[in] Message the assembled message, contains addresses
     * \return
     *
     * The situation is: the scGridPoint has a message; either its top layer created one,
     * or the bottom layer received one. The bottom layer can receive messages either
     * directly from the neighbors or through the inter-cluster bus.
     * That is, this scGridPoint can be both the source or the destination of a message,
     * or a proxy between them (to forward the message to the real address provided that they are 2nd neighbors).
     * If the message is outside of this reach, it is forwarded to the head of the cluster,
     * where it will be forwarded through the inter-cluster bus to its destination.
     * Only the message is considered, the method is self-contained (except that 
     * the message will be either processed, proxied or routed).
     * 
     * Routing principle:
     * The cluster addresses of the source and the destination are provided in the message.
     * The local messages (if the distance of the gridpoints are 0, 1 or 2) delivered locally 
     * using direct connections, if needed using a proxy.
     * If the source and destination scGridPoint modules are located in different clusters
     * and have no common boundary, the message is routed to the cluster head of the Source
     * and will be delivered via sending it through the inter-cluster bus (the next level of hierarchy)
     * to the cluster head of the Destination scGridPoint that forwards to its real destination.
     *
     * Only the cluster heads have access to the inter-cluster bus, the members of the cluster
     * shall use their cluster head to reach other clusters or the memories.
     *
     * Surely the 'cm_Head' goes to the 'member' field, and the real member is put
     * in the 'proxy' field, otherwise the 'proxy' is 'cm_Head'.
     * Upon receiving a message where the 'proxy' is not 'cm_Head'
     * (it is only possible in the cluster head, the members have no direct access to messaging),
     * the 'proxy' number is moved to the 'member' field, and the 'proxy' field is set to 'cm_Head'.
     * After retargeting the message in this way, the message is re-routed:
     * the cluster head sends a new message (now within the cluster)to the real member.
     * 
     * For message routing, scGridPoint is a self-contained unit. The message comprises the  source
     * and the destination address, and the topology comprises how to route the message.
     *
     * @see scGridPoint::ReceiveMessage_thread
     */
    void  scGridPoint::
RouteMessage(scIGPMessage* Message)
{   
    ClusterAddress_t CAD = Message->DestinationAddress_Get();
    ClusterAddress_t CAS = Message->ReplyToAddress_Get();
    ClusterAddress_t MyCA = Memory2ClusterAddress;  // This is set if the address if wrong
    // Take care: the memory does not map to any scGridPoint
    scGridPoint *Dest = (MyCA.ToInt() == CAD.ToInt()) ? (scGridPoint*) NULL
            : Processor_Get()->ByClusterAddress_Get(CAD);
    
    // First check if we are the destination; if yes, process the message
    // "We" means the scGridPoint; all HW threads, with or without proxies
    if(IsTheSameAs(Dest))
    {
        // It is definitely for us, but maybe broadcast
        if(cm_Broadcast == Dest->ClusterAddress_Get().Member)
        {   // It is a message for all members, organize broadcasting to the members
            // We must create a temporary copy
            assert(false); //Not yet implemented
/*            ClusterAddress_t NewCA = ClusterAddress_Get();
            NewCA.Member = NewCA.Proxy;
            NewCA.Proxy = cm_Head;
            Message->DestinationAddress_Set(NewCA);
            RouteMessage(Message); return;
            */
            return;
        }
        // No way out, we need to work, let the upper layer to work
        ProcessMessage(Message); return;
    }

    // Not our message, we are just routing it in the bottom layer
    // Anyhow, announce source and dest
    DEBUG_PRINT_MESSAGE("ROUTING ",Message);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"ROUTING ",StringOfMessage_Get(Message).append("\n"));

    scGridPoint *Source = (MyCA.ToInt() == CAS.ToInt()) ? (scGridPoint*) NULL
            : Processor_Get()->ByClusterAddress_Get(CAS);
    scGridPoint* Proxy;     // Maybe we will need a proxy

    // The memory messages need special handling
    if(MyCA.ToInt() == CAD.ToInt())    // The destination is the 'far' memory
    {   // The destination is the memory
        // Anyhow, we must reach a cluster head

        // It may happen that a phantom member wants to access the memory
        if(cm_Head != ClusterAddress_Get().Proxy)
        {   // The source pre-defines its proxy
            if(cm_Head == ClusterAddress_Get().Member)
            {   // We are the proxy, we must send the message to the inter-cluster bus
                DEBUG_CONTRIBUTE_STRING(DebugRoute, "SENT CLUSTER ",
                                        StringOfMessage_Get(Message).append("\n"));
                MainMemory->ReceiveClusterMessage(Message); return;
            }
            else
            {   // Just forward the message to its predefined proxy
            CAS.Member = CAS.Proxy;
            CAS.Proxy = cm_Head;
            Proxy = Processor_Get()->ByClusterAddress_Get(CAS);
            DEBUG_PRINT_OBJECT("ROUTED PROXY message from "
                << Source->StringOfClusterAddress_Get()
                << " through " << Proxy->StringOfClusterAddress_Get()
                << " to " << Dest->StringOfClusterAddress_Get() );
            DEBUG_CONTRIBUTE_STRING(DebugRoute,"PROXY " + Proxy->StringOfClusterAddress_Get() + " predefined: ",StringOfMessage_Get(Message).append("\n"));
            SendDirectMessageTo(Proxy, Message); return; // send the message to the predefined proxy
            }
        }
        else
        {   // We need to use our own cluster head
            if(cm_Head == ClusterAddress_Get().Member)
            {   // We are the cluster head
                MainMemory->ReceiveClusterMessage(Message); return;
            }
            else
            {
            CAS.Proxy = cm_Head; //
            CAS.Member = cm_Head; // from the proxy
            Proxy = Processor_Get()->ByClusterAddress_Get(CAS);    // The cluster head will proxy
            DEBUG_PRINT_OBJECT("PROXY " << Proxy->StringOfClusterAddress_Get() << " inserted");
            DEBUG_CONTRIBUTE_STRING(DebugRoute,"PROXY " + Proxy->StringOfClusterAddress_Get() + " inserted: ",StringOfMessage_Get(Message).append("\n"));
            SendDirectMessageTo(Proxy, Message); return; // send the  message to the cluster head
            }
        }

    }
    // else: route a message for an scGridPoint
    assert(Dest);   // Just be sure we have a corect message :)

    // We only forward the message, check how it should be done
    // Check if it goes to an immediate neighbor; if so can be sent directly   
    if(IsNeighborOf(Dest))
    {
        SendDirectMessageTo(Dest, Message); return;  // This is for the neighbor, send it directly
    }
    // OK, maybe the message is for our second neighbor; if so, find a proxy scGridPoint
    if(Is2ndNeighborOf(Dest))
    {   // find the common neighbor
        scGridPoint* GP = Source;
        if(Source)
        {   // The source is another gridpoint
            ClusterNeighbor N = cm_North;
            for(; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
            {  // Check if it is good neighbor
                Proxy = Processor_Get()->ByClusterPointer_Get(GP,N);
                if(Proxy && Proxy->IsNeighborOf(Dest)) break;
            }
        }
        else
        {   // The source is the memory
            ClusterAddress_t MyCA = Dest->ClusterAddress_Get();
            MyCA.Proxy = 0; // The proxy is prefefined
            Proxy = Processor_Get()->ByClusterAddress_Get(MyCA);
        }
        // Now the proxy found: it is a common neighbor
        DEBUG_PRINT_OBJECT("ROUTED PROXY message from "
            << Source->StringOfClusterAddress_Get()
            << " through " << Proxy->StringOfClusterAddress_Get()
            << " to " << Dest->StringOfClusterAddress_Get() );
        DEBUG_CONTRIBUTE_STRING(DebugRoute,"PROXY " + Proxy->StringOfClusterAddress_Get() + " inserted: ",StringOfMessage_Get(Message).append("\n"));
        SendDirectMessageTo(Proxy, Message); return;
    }
/*
    // If the present and the destination scGridPoints are in the same cluster,
    // we can use the cluster head as proxy
    if(IsInTheSameClusterAs(Dest))
    {   // We either have a proxy already or can use the cluster head as proxy
        assert(0); // maybe this branch is never taken?
        ClusterAddress_t CAP = CAD;
        // We select the proxy core, but do not touch the message
        { CAP.Member = CAS.Proxy; CAP.Proxy = cm_Head; }

        Dest =  Processor_Get()->ByClusterAddress_Get(CAP);
        DEBUG_PRINT_MESSAGE("PROXY " + Dest->StringOfClusterAddress_Get() + " inserted: ",Message);
        DEBUG_CONTRIBUTE_STRING(DebugRoute,"PROXY " + Dest->StringOfClusterAddress_Get() + " inserted: ",StringOfMessage_Get(Message).append("\n"));
        SendDirectMessageTo(Dest, Message); return; // send the message to the cluster head
    }
*/

    // It is not a neighbor, we anyhow must find a proxy
    // The phantom scGridPoint defines its default proxy
    if(cm_Head != ClusterAddress_Get().Proxy)
    {   // The source pre-defines its proxy
        CAS.Member = CAS.Proxy;
        CAS.Proxy = cm_Head;
        Proxy = Processor_Get()->ByClusterAddress_Get(CAS);
        DEBUG_PRINT_OBJECT("ROUTED PROXY message from "
            << Source->StringOfClusterAddress_Get()
            << " through " << Proxy->StringOfClusterAddress_Get()
            << " to " << Dest->StringOfClusterAddress_Get() );
        DEBUG_CONTRIBUTE_STRING(DebugRoute,"PROXY " + Proxy->StringOfClusterAddress_Get() + " predefined: ",StringOfMessage_Get(Message).append("\n"));
        SendDirectMessageTo(Proxy, Message); return; // send the message to the predefined proxy
    }

    // These were all the cases when immediate sending was possible
    
    // We attempted to process the message immediately,
    // to send directly and to send using proxy
    // If no success, we must send it to the cluster head, asking for its help
    // or to send it to the inter-cluster bus, if we are the cluster head.
    if(cm_Head == ClusterAddress_Get().Member)
    { // We are the cluster head, must send out to the bus
        DEBUG_CONTRIBUTE_STRING(DebugRoute, "SENT CLUSTER ",
                                StringOfMessage_Get(Message).append("\n"));
//??        DEBUG_MESSAGE(ClusterBusStatus CSt = SendClusterMessage(Message);)
        DEBUG_ONLY(assert(CLUSTER_BUS_ERROR != CSt);)
        return;
    }
    else
    {   // We send the message to the cluster head, get its address
            CAS.Proxy = CAS.Member; // Proxy requested in the reply
            CAS.Member = cm_Head; // from the proxy
            scGridPoint* Proxy = Processor_Get()->ByClusterAddress_Get(CAS);    // The cluster head will proxy
            DEBUG_PRINT_OBJECT("Routed through proxy " << Proxy->StringOfClusterAddress_Get() );
            DEBUG_CONTRIBUTE_STRING(DebugRoute, "SEND DIRECT PROXY " + Proxy->StringOfClusterAddress_Get()
                                   , StringOfMessage_Get(Message).append("\n"));
            SendDirectMessageTo(Proxy, Message); return; // send the (modified) message to the proxy
    }
}


    /*!
     * \fn scGridPoint::CreateMemoryReadMessage
     * \brief The message 'CreateReadMemoryMessage' request from an scGridPoint to read the memory
     * \param Address The beginning of the memory block (4-aligned value)
     * \param Length The length of the memory block, 4-byte (DWord) value
     * \return The filled-out message form
     */
    scIGPMessage* scGridPoint::
CreateMemoryReadMessage(unsigned int Address, int Length)
{
    scIGPMessage *MyMessage = new scIGPMessage;
    // Handle the address of the target core
    ClusterAddress_t CA = ClusterAddress_Get();
    MyMessage->ReplyToAddress_Set(CA);
    // Handle message features
    MessageFeatures_t MsgType;
    MsgType.ReplyLength = Length;
    MsgType.Length = 4;
    MsgType.Type = msg_Mem;
    MsgType.Time = 0;
    DebugRoute = "";
    MyMessage->FeatureWord_Set(MsgType);
    // Handle reply-to address
//        ClusterAddress_t MyCA = DefaultClusterAddress;
        MyMessage->DestinationAddress_Set(Memory2ClusterAddress);
        MyMessage->Header3Address_Set(Address);
//    memcpy(&MyMessage->mBuffer[3], &Address, sizeof(Address));   // Store reply address
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"CREATED ",StringOfMessage_Get(MyMessage).append("\n"));
    return MyMessage;
}

    /*!
     * \fn scGridPoint::CreateMemoryWriteMessage
     * \brief The message 'CreateWriteMemoryMessage' request from an scGridPoint to write the memory
     * \param Address The beginning of the memory block (4-aligned value)
     * \param Length The length of the memory block, 4-byte (DWord) value
     * \return The filled-out message form
     */
    scIGPMessage* scGridPoint::
CreateMemoryWriteMessage(unsigned int Address, int Length)
{
    scIGPMessage *MyMessage = new scIGPMessage;
    // Handle the address of the destination core
    ClusterAddress_t CA = ClusterAddress_Get();
    MyMessage->ReplyToAddress_Set(CA);  // This was the sender, just to know
    // Handle message features
    MessageFeatures_t MsgType;
    MsgType.ReplyLength = Length;
    MsgType.Length = 4+Length;  // This also means it is a 'write' message
    MsgType.Type = msg_Mem;
    MsgType.Time = 0;
    DebugRoute = "";
    MyMessage->FeatureWord_Set(MsgType);
    // Handle reply-to address
//        ClusterAddress_t MyCA = DefaultClusterAddress;
    MyMessage->DestinationAddress_Set(Memory2ClusterAddress);
    MyMessage->Header3Address_Set(Address);
//    memcpy(&MyMessage->mBuffer[3], &Address, sizeof(Address));   // Store reply address
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"CREATED ",StringOfMessage_Get(MyMessage).append("\n"));
    return MyMessage;
}

/*!
 * \fn  bool scGridPoint::direct_write(scIGPMessage* M)
 * \brief Write the message directly to the scGridpoint (and process it)
 */
    bool scGridPoint::
direct_write(scIGPMessage* M)
{
        return true;
}
    bool scGridPoint::
direct_read(scIGPMessage* M)
{
        return true;
}
  
        bool scGridPoint::
    direct_read(int *data, unsigned int address)
    {
        return true;
    }
        bool scGridPoint::
    direct_write(int *data, unsigned int address)
    {
        return true;
    }

  // Slave Interface, through the bus
        ClusterBusStatus scGridPoint::
    read(scIGPMessage* M)
    {
        return CLUSTER_BUS_OK;
    }
        ClusterBusStatus scGridPoint::
    write(scIGPMessage* M)
    {
        ReceiveClusterMessage(M);
        return CLUSTER_BUS_OK;
    }

    

/* !
 * \fn ClusterBusStatus scClusterBus::IGPMessage_Send(scIGPMessage *M)
 * \brief Send message M to the partner
 * 
 * The message is self-contained, i.e. length, type, partner ID are all contained.
 * The function is a replacement of scClusterBus::burst_write
 */
/*    ClusterBusStatus scClusterBus::
IGPClusterMessage_Send(scIGPMessage *M)
{
      ClusterBusRequest *request = get_request(unique_priority);

  request->do_write           = true; // we are writing
  request->M = M;
}*/

    /*!
     * \brief SendDirectMessageTo Delived 'Message' to the 'Target'
     * \param[in] Target the target gridpoint
     * \param[in] Message the message to be delivered
     *
     * This routine is called when a pre-assembled message is to be sent
     */
    void scGridPoint::
SendDirectMessageTo(scGridPoint* Target, scIGPMessage* Message)
{
    // First, find out if the target and message are legal
    assert( Target );   // Be sure the tartget is valid
//    ClusterAddress_t CAS = Message->ReplyAddress_Get();
//    ClusterAddress_t CAD = Message->DestinationAddress_Get();
//    scGridPoint* Source = Processor_Get()->ByClusterAddress_Get(CAS);
     // OK, we need an IGPCB index, find it
//    ClusterAddress_t CAT = Target->ClusterAddress_Get();
    string DString = Target->StringOfClusterAddress_Get();
     //if(Message->ForcedDirect_Get()) FString = "FORCED ";
    string FString = "SENT DIRECT to " + Target->StringOfClusterAddress_Get();
    DEBUG_PRINT_MESSAGE(FString +": ",Message);
    DEBUG_CONTRIBUTE_STRING(DebugRoute,FString +": ",StringOfMessage_Get(Message).append("\n"));
    ClusterNeighbor N = cm_North; ClusterNeighbor Index= cm_Broadcast;    // assume not found
    for(; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
    {
        if(msIGPCB[N])
        {   // We do have a neighbor at all
            if(msIGPCB[N]->OtherIGPCB_Get()->GridPoint_Get() == Target)
            {   //That's it!
                Index = N;
                break;
            }
        }
    }
    // Now we know on which side we have the requested target
    if(cm_Broadcast != Index)
    {
        // OK, we have found it:
        // msIGPCB[Index] is the sender, msIGPCB[N]->OtherIGPCB_Get() is the receiver
        msIGPCB[Index]->Sending_Set(true);
//The transport method        ICB_b_transport(Index, (unsigned char*)Message->Buffer_Get(), 4 );
        int16_t Length = Message->Length_Get();
        assert((Length>=0)&(Length<=MAX_IGPCBUFFER_SIZE));
        // Now let the other party know that a message follows
        msIGPCB[Index]->OtherIGPCB_Get()->EVENT_IGPCB.MessageReceived.notify(SCTIME_CLOCKTIME);
   //     DEBUG_PRINT_MESSAGE("BUFFERING message ",Message);

       DEBUG_PRINT_MESSAGE
            ("NOTIFIED "
            + msIGPCB[Index]->OtherIGPCB_Get()->GridPoint_Get()->StringOfClusterAddress_Get() + " about message ",Message);
//        DEBUG_CONTRIBUTE_STRING(DebugRoute,"NOTIFIED "
  //                              + msIGPCB[Index]->OtherIGPCB_Get()->GridPoint_Get()->StringOfClusterAddress_Get() + " about message ",StringOfMessage_Get(Message).append("\n"));
            // This early notification enables writing and reading FIFO in parallel
            // the blocking read at the other end enables to read the message as soon as it arrives
       // In the meantime the other party can read the previous contents
       // This party writes into the FIFO of the other party
        // Write the heading first; take care of losing typing
       int32_t Word;
       Word = Message->DestinationAddress_Get().ToInt();
       msIGPCB[Index]->scIGPout->write(Word);
       wait(SCTIME_CLOCKTIME);
       Word = Message->FeatureWord_Get().ToInt();
       msIGPCB[Index]->scIGPout->write(Word);
       wait(SCTIME_CLOCKTIME);
       Word = Message->ReplyToAddress_Get().ToInt();
       msIGPCB[Index]->scIGPout->write(Word);
       wait(SCTIME_CLOCKTIME);
       Word = Message->Header3Address_Get();
       msIGPCB[Index]->scIGPout->write(Word);
       wait(SCTIME_CLOCKTIME);
       // OK, now the heading cloned; now clone the data

        int32_t i = 0; Length -=4;
        while(Length--)
        {
            msIGPCB[Index]->scIGPout->write(Message->BufferContent_Get(i++));
           wait(SCTIME_CLOCKTIME);
        }
        msIGPCB[Index]->Sending_Set(false);  // The sending phase, in this IPCB is over
        // OK, the message is over
    }
    else
    {   // No direct neighbor 
        assert(false);//"No direct neighbor found");
    }
    wait(SCTIME_CLOCKTIME); // for executing the instruction itself
 }

    void scGridPoint::
FETCH_thread()
{
    while(true)
    {
        wait(EVENT_GRID.MakeFetch);
        DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.FETCH" <<  " began " ) ;
        doFetchInstruction(HThreadFetch_Get());
        DEBUG_EVENT_OBJECT("Instruction fetched" ) ;
        EVENT_GRID.FetchReady.notify();
    }
}

    bool scGridPoint::
doFetchInstruction(scHThread* H)
{
        DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.MakeFetch") ;
        wait(100,SC_PS);
        DEBUG_EVENT_OBJECT("SENTD EVENT_GRID.FetchReady") ;
        return true;
}

    void scGridPoint::
EXEC_thread()
{
    while(true)
    {
        wait(EVENT_GRID.MakeExec);
        DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.EXEC" <<  " began " ) ;
        doExecInstruction(HThreadExec_Get());
        DEBUG_EVENT_OBJECT("Instruction executed" ) ;
    }
}


    void scGridPoint::
SEND_thread()
{
    while(true)
    {
        wait(EVENT_GRID.MakeSend);
        DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.SEND" <<  " began " ) ;
        // HThreadSend_Get() returns which thread requests sending
//        doExecInstruction(HThreadExec_Get());
        DEBUG_EVENT_OBJECT("Instruction executed" ) ;
    }
}


    bool scGridPoint::
doExecInstruction(scHThread* H)
{
        DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.MakeExec") ;
        wait(10,SC_NS);
        DEBUG_EVENT_OBJECT("SENTD EVENT_GRID.ExecReady") ;
        EVENT_GRID.ExecReady.notify();
        return true;
}

    /*!
     * \brief scGridPoint::SetupMessageAddressTo
     *
     * The points are addressed
     * \param GP the address of the grid point
     * \return false if self-addressing
     *
     * The address is composed as (from right)
     * HTHREAD_BUS_WIDTH    bits The addressed HThread (4)
     * 3                    bits The member number (3)
     * 3                    bits The proxy member (3)
     * CLUSTER_BUS_WIDTH    bits The cluster number (4)
     */
    void scGridPoint::
SetupMessageAddressTo(scGridPoint* GP)
{
//    assert(!(GP->HThread_Get()<0 || GP->HThread_Get()>=HTHREADS_LIMIT));
    assert(GP);
    DEBUG_ONLY(ClusterAddress_t CA = GP->ClusterAddress_Get();
    int32_t A = CA.Cluster;
    assert(!(A<0 || A>= CLUSTERS_LIMIT));)  // Wrong cluster number
    if(IsTheSameAs(GP))
       return;
    if(IsNeighborOf(GP))
    { // The addressed GridPoint in an immediate neighbor, just do it
            return;
    }

        if(this->ClusterAddress_Get().Cluster == GP->ClusterAddress_Get().Cluster)
    {   // The message is for another member, but not direct neighbor, set the head as proxy
            return;
    }
}
        /*!
         * \brief ClusterHeadOfMember_Get
         * \return the address of the cluster head of the point
         */
/*    scGridPoint* scGridPoint::
ClusterHead_Get(ClusterAddress_t CA)
{
    ClusterAddress_t MyCA = DefaultClusterAddress;  // This is set if the address if wrong
    if(MyCA.ToInt() == CA.ToInt())
        CA = ClusterAddress_Get();
    CA.Member = cm_Head;        // Be sure to take the head
    return Processor_Get()->ByClusterAddress_Get(CA);
}
*/
   /**
    * @fn scGridPoint::PrologString_Get
    * Assemble a heading text for 'gridpoint/core/neurer' messages
    *
    * @return string containing time, memory address, QT state, core state
    */
   string scGridPoint::
PrologString_Get(void) // Get a unified prolog text for logging
{
    ostringstream oss;
    string SG = StringOfClusterAddress_Get();
    oss  << '@' << StringOfTime_Get()  << ": " << SG.c_str();
    SC_HTHREAD_ID_TYPE H = HThreadExec_Get()->ID_Get();
    if(H)
    {
        string ST = StringOfThread_Get();
        if(ST.size()) oss << '/' <<  ST.c_str() ;
    }
    string SQ = mHThreads[H]->StringOfName_Get();
    if(SQ.size()) oss << '<' << SQ.c_str() << '>';  // Append QT name if it is in use
    if((uint16_t)-1 != mStatus)
        oss << '{' << StringOfStatus_Get().c_str() << '}';
    if((SC_ADDRESS_TYPE)CORE_DEFAULT_ADDRESS != HThreadExec_Get()->PC_Get())
        oss << '[' << HThreadExec_Get()->StringOfPC_Get().c_str() << ']';   // Append PC if the PC is in use
    oss << '%';
//    while(oss.str().size()<24) oss << ' ';
    return oss.str();
}


     /*!
      * \brief scGridPoint::PreallocatedMask_Get
      * \return mask describing preallocated scGridPoint
      */
    SC_GRIDPOINT_MASK_TYPE scGridPoint::
PreAllocatedMask_Get(void){ return msMask.PreAllocated; }

     /*!
      * \brief scGridPoint::PreallocatedMask_Set
      * Preallocate scGridPoints given by Mask
      * \param Mask describrs cores to be preallocated
      * \return true if all cores could be preallocated, otherwise no core gets preallocated
      */
     bool scGridPoint::
PreAllocatedMask_Set(SC_GRIDPOINT_MASK_TYPE Mask)
{
    // Check if any of the gridpoints is alrady allocated
  if(Mask & Processor_Get()->UnavailableMask_Get()) return false;
  msMask.PreAllocated = Mask; // Allocate the gridpoints for us
  Processor_Get()->PreAllocatedMask_Set(Processor_Get()->PreAllocatedMask_Get() | Mask); //and globally
  return true;
}
 
 
    void scGridPoint::
ChildrenMaskBit_Set(scGridPoint* C)
    { msMask.Children |= C->IDMask_Get();}	/// Get the mask of allocating cores
void
scGridPoint::ChildrenMaskBit_Clear( scGridPoint* C) { msMask.Children &= ~C->IDMask_Get();}  ///Clear the mask of deallocated core
   /*!
    * \brief scGridPoint::Deallocate
    *
    * \brief Put back a freed core to the pool
    *
    * The deallocation takes place in one atomic step,
    * admistering properly the bits both in this (the child) and its parent

    * \return true if succeeded, false if was not deallocated
    *
    * @see scGridPoint::AllocateFor
    */
    bool
scGridPoint::Deallocate(void)
{
  if(!IsAllocated())
    return false; // The core is not allocated
  if(ChildrenMask_Get() //& ~IDMask_Get()
          )
    return false;	// The core has children, must not be deallocated
  // OK, the core can be deallocated
  scGridPoint* Parent = Parent_Get();  // Yes, we do have a parent
  if(Parent)
    {// We do have a real parent core
       DEBUG_PRINT(hex << "Deallocating core#" << ID_Get() << " from core #" <<  Parent_Get()->ID_Get());
       Parent->ChildrenMaskBit_Clear(this);
    }
  else
    {// We were created by the root (processor)
       DEBUG_PRINT(hex << "Deallocating core#" << ID_Get()  << " from Processor");
//!!       Processor_Get()->ChildrenMaskBit_Clear(this);
    }
  AllocatedBit_Set(false);
  Processor_Get()->AllocatedMask_Set(Processor_Get()->AllocatedMask_Get() & ~IDMask_Get());	// Administer in the topology mask
  Parent_Set((scGridPoint*)NULL);   // Make sure we have no parent
  return true;
}

     /*!
      * \fn      scGridPoint* scGridPoint::PreAllocateFor(scGridPoint* Parent)
      *
      * \brief Allocate this core for Parent if possible
      * \param[in] Parent the parent core
      * \return allocated core if successful or NULL
      */
     scGridPoint* scGridPoint::
PreAllocateFor(scGridPoint* Parent)
{
   if(CatchedAllocationError(Parent) //|| !CatchedFramingError()
        )
     return (scGridPoint*) NULL; // For some reason, the core is not available
   // Yes, it can be preallocated
   PreAllocatedBit_Set(true); Parent_Set(Parent);
   if(Parent)
     Parent->PreAllocatedMaskBit_Set(this,true);
   else // The processor is the parent
     Processor_Get()->PreAllocatedForProcMaskBit_Set(this,true);
   return this;
}

     /*!
      * \fn bool scGridPoint::CatchedAllocationError(scGridPoint* Parent)
      * \brief Catches possible allocation errors
      * \return true if the core cannot be allocated
      *
      * A core must NOT be allocated if
      * - it already has children
      * - if is not available from any reason other than being preallocated for the requestor
      */
     bool scGridPoint::
CatchedAllocationError(scGridPoint* Parent)
{
         if(IsDenied())
         {
             // Maybe here some emergency action needed:
             return true; // the core may fail even while is allocated
         }
  return !( // No error if
            IsAvailable()  // The core is available
           || (!IsAllocated() && IsPreAllocatedFor(Parent)) // or is preallocated for the requestor
            )
           || ChildrenMask_Get(); // or already has children
}


     /*!
      * scGridPoint* scGridPoint::AllocateFor(scGridPoint* Parent)
      *
      * \brief Allocate this core for Parent if possible
      *
      * An scGridPoint must be allocated before using it.
      * The allocation takes place in one atomic step,
      * administering properly the bits both in Parent and this (the new child)
      * \param Parent the parent core
      * \return allocated core or NULL
      *
      * @see scGridPoint::Deallocate
      */
     scGridPoint* scGridPoint::
AllocateFor(scGridPoint* Parent)
{
   if(CatchedAllocationError(Parent) //|| !CatchedFramingError()
           )
       return (scGridPoint*) NULL; // For some reason, the core is not available
       
   // Yes, it can be allocated
//   AllocationBegin_Set(sc_time_stamp()); // Remember when the core was allocated
   mInstanceCount++;  // Count how many times was instantiated
   AllocatedBit_Set(true);  Parent_Set(Parent);
   ResetForParent(Parent);  // Do not touch preallocation
   return this;
}

     /*!
     * \brief Allocate an scHThread from this scGridPoint
     * \return the allocated scHThread if succeeded, else NULL
     */
    scHThread* scGridPoint::
AllocateAHThread(void)
{
    for(SC_HTHREAD_ID_TYPE H=0; H<MAX_HTHREADS; H++)
    {
        if(!HThreadAllocatedBit_Get(H))
        {   // OK, we found an available HThread
           HThreadAllocatedBit_Set(H,true); // Mark it as active
           return HThread_Get(H);   // and use it
        }
    }
    return (scHThread*)nullptr; // No available svHThread, in this scGridPoint
}


     /*!
     * \brief scGridPoint::ResetForParent
     * \param Parent the parent of the QT, maybe NULL
     *
     * Prepares the core to work as a child of the Parent
     * Calls the core-specific virtual method doSetQTAddresses
     */
    void scGridPoint::
ResetForParent(scGridPoint* Parent)
{
     WaitBit_Set(false);     // Not yet seen 'wait'
     MetaBit_Set(false);     // The meta-instruction is executed by the Parent, the core starts in conventional mode
     msMask.Children = 0;    // We have no children
 //          msWaitCount = 0; // Not waiting when allocated

/*!!     SC_HTHREAD_ID_TYPE H = Parent->ClusterAddress_Get().HThread;
     mHThreads[H]->QTRegime_Set(Parent); // 'true' if we have a real parent
     mHThreads[H]->QTID_Set(QT_AssembleID(Parent));
*/     doSetQTAddresses(Parent); // derived classes may set QT addresses differently
     if(Parent)
     {	// We have another core as parent
         Parent->ChildrenMaskBit_Set(this);
         LOG_TRACE_SOURCE("Allocated for '" + Parent->PrologText_Get() + "'");
     }
     else
     {	// The processor is the parent
         //!!Processor_Get()->ChildrenMaskBit_Set(this);
         LOG_TRACE_SOURCE("Allocated for '" + Processor_Get()->PrologText_Get());
     }
      CooperationMode_Set(ccm_None);	// Not in mass operating mode
}// scGridPoint::ResetForParent(scGridPoint* Parent)

         /*!
          * \fn  bool scGridPoint::IsPreAllocatedFor(scGridPoint* C)
          * \param C the candidate parent
          * \return  true if the core is preallocated for C
          */
   bool scGridPoint::
IsPreAllocatedFor(scGridPoint* C)
{
  if(C)
    return C->PreAllocatedMask_Get() & msMask.ID;
  else // It may be reserved for the processor
    return  Processor_Get()->PreAllocatedForProcMask_Get()  & msMask.ID;
}

        void scGridPoint::
PreAllocatedMaskBit_Set(scGridPoint* C, bool V) /// Get the mask of allocating cores
{   msMask.PreAllocated &= ~C->IDMask_Get();
    if(V) msMask.PreAllocated |= C->IDMask_Get();
}

    // Creates a message header, with filleing out the mandatoty fields
    scIGPMessage* scGridPoint::
CreateMessageTo(scGridPoint* To, IGPMessageType Type, int Length)
{
    assert(To);   // Must be real scGridPoint
    DebugRoute = "";
    scIGPMessage *MyMessage = new scIGPMessage;
    // Handle the address of the target core
    ClusterAddress_t CA = To->ClusterAddress_Get();
//    static_assert(sizeof(MyMessage->mBuffer[0]) <= sizeof(CA), "!");
//    memcpy(&MyMessage->mBuffer[0], &CA, sizeof(CA));
    MyMessage->DestinationAddress_Set(CA);
    // Handle message features
    MessageFeatures_t MsgType;
    MsgType.Length = Length;
    MsgType.Type = Type;
    MsgType.Time = 0;
//    static_assert(sizeof(MyMessage->mBuffer[1]) <= sizeof(MsgType), "!");
//    memcpy(&MyMessage->mBuffer[1], &MsgType, sizeof(MsgType)); // Create the type
    MyMessage->FeatureWord_Set(MsgType);
    // Handle reply address
    CA = ClusterAddress_Get();
//    static_assert(sizeof(MyMessage->mBuffer[2]) <= sizeof(CA), "!");
//    memcpy(&MyMessage->mBuffer[2], &CA, sizeof(CA));   // Store reply address
    MyMessage->ReplyToAddress_Set(CA);
    return MyMessage;
}
    /*!
     * \brief scGridPoint::CreateRegisterMessageTo
     * \param To the destination of the message
     * \param Mask the bits decribing the register contents in the message
     * \return The prepared message
     *
     * A register-type message is prepared.
     * The message structure is created (will be destroyed at the destination)
     * and the register contents defined by the mask loaded into the buffer
     */

    scIGPMessage* scGridPoint::
CreateRegisterMessageTo(scGridPoint* To, SC_GRIDPOINT_MASK_TYPE Mask)
{
        // Fills out surce/dest fields, pus length
    scIGPMessage *MyMessage = CreateMessageTo(To, msg_Reg, OnesInMask_Get(Mask)+4 );
    MyMessage->Header3Address_Set((uint32_t) Mask);
    int index = 0; int Reg = 0;
    while(Mask)
    {
        if(Mask & 1)
        {
            MyMessage->BufferContent_Set(index++, Register_Get(Reg));
        }
        Mask /=2; Reg++;
    }
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"CREATED ",StringOfMessage_Get(MyMessage).append("\n"));
    return MyMessage;
}

    // Create a 'create QT' message
    scIGPMessage* scGridPoint::
CreateQtCreateMessageTo(scGridPoint* To, SC_ADDRESS_TYPE PC, SC_GRIDPOINT_MASK_TYPE Mask, SC_GRIDPOINT_MASK_TYPE BackMask)
{
    scIGPMessage *MyMessage = CreateMessageTo(To, msg_Qt,
                                              OnesInMask_Get(Mask)  // The number of registers to pass
                                              +5    // The header length + PC
                                              +FlagWordLength_Get() // 1 for Y86, 0 for others
                                              );

  //  MyMessage->mBuffer[3] = Mask;
    MyMessage->Header3Address_Set(Mask);
    MessageFeatures_t T = MyMessage->FeatureWord_Get();
//    T.SubKey = QType;
    MyMessage->FeatureWord_Set(T);
    // Will implement register contents to mBuffer[3]+
//    switch(QType)
    {
    int index = 0; int Reg = 0;
    while(Mask)
    {
        if(Mask & 1)
        {
            MyMessage->BufferContent_Set(index++,Register_Get(Reg));
//            MyMessage->mBuffer[index++] = REGS[Reg];
        }
        Mask /=2; Reg++;
    }
    MyMessage->BufferContent_Set(index++,HThreadExec_Get()->PC_Get());
//    MyMessage->mBuffer[4+index++] = PC_Get();

//    if(FlagWordLength_Get()) MyMessage->mBuffer[4+index++] = ConditionCode_Get();
    if(FlagWordLength_Get()) MyMessage->BufferContent_Set(index++,ConditionCode_Get());
    }
    DEBUG_CONTRIBUTE_STRING(DebugRoute,"CREATED ",StringOfMessage_Get(MyMessage).append("\n"));
    return MyMessage;
}

    scIGPMessage* scGridPoint::
CreateQtKillMessageTo(scGridPoint* To)
{   assert(0);
    return nullptr;
}

    /*!
     * \brief scGridPoint::CreateSpecialMessageTo
     * \param To    the scGridPoint where the message is to be transmitted
     * \param Key   the key of the special function
     * \param RegNo The address of the register
     * \param R
     * \return the register content; the real body
     */

    scIGPMessage* scGridPoint::
CreateSpecialMessageTo(scGridPoint* To, int16_t Key, int8_t RegNo, int32_t R)
{
        scIGPMessage *MyMessage = CreateMessageTo(To, msg_Coop,
                                                  4 // Always 4 words
                                                   );
        MessageFeatures_t MsgType;
        MsgType.Length = 4; // We have exactly 4 bytes
//        MsgType.Type = Type;
        MsgType.SubKey = Key;
        MsgType.ReplyLength = RegNo;
        MsgType.Time = 0;
        MyMessage->Header3Address_Set(R);
//        MyMessage->mBuffer[3] = R;  // The real message
        MyMessage->FeatureWord_Set(MsgType);
        return MyMessage;
}

    /*!
     * \brief scGridPoint::doCanTerminate
     * \param Address the offset of the QT we need to wait for or a 'wild card'
     * \return
     */
    bool scGridPoint::
doCanTerminate( SC_GRIDPOINT_MASK_TYPE Address)
{
    // The two procedures return a mask of the cores we are waiting for
    return !doWaitMask_Get(Address);
}

    /*!
     * \brief scGridPoint::doWaitMask_Get
     * Report which scGridPoint(s) we must wait for
     * \param Offset the scGridPoints we must wait for (excluding non-children and myself)
     * \return The mask describing still cores we are must wait
     */

    SC_GRIDPOINT_MASK_TYPE scGridPoint::
doWaitMask_Get(SC_ADDRESS_TYPE Offset)
{
    SC_GRIDPOINT_MASK_TYPE SearchMask = msProcessor->AllocatedMask_Get();
    // Now eliminate false wait bits from the mask
    scGridPoint* Anchestor = this;
    while(Anchestor)
    {
        SearchMask &= ~Anchestor->msMask.ID; // Except myself, and my anchestors
        Anchestor = Anchestor->Parent_Get();
    }

    // Now we have the candidate cores
    if(!SearchMask) return 0;  // Will not wait: no candidate cores
    if(MEMORY_DEFAULT_ADDRESS==Offset)
    { // We are using "Wild card", waiting for "all"
        return SearchMask;// contains word with the "one-hot" bitmasks
    }
    else
    { // We are waiting for a well-defined offset
        scGridPoint* C = doFindHostToProcess(Offset,SearchMask);
        if(C)
            return C->IDMask_Get();
        else
          return 0;
    }
}


    /*!
     * \brief scGridPoint::doFindHostToProcess
     * @param[in] Offs The offset of the QT we are looking for
     * @param[in] Mask Contains mask for candidate hosts
     * @return pointer to the core the QT runs on
     * Find the core the QT with offset Addr is running
     *
     * This routine maps the code chunk given by Addr to a physical core.
     * Given that one code fragment can only belong to at most one physical core,
     * the correspondance is unique.
     */
   scGridPoint* scGridPoint::
doFindHostToProcess(SC_ADDRESS_TYPE Offs, SC_GRIDPOINT_MASK_TYPE Mask)
{
    SC_GRIDPOINT_MASK_TYPE CoreMask = 1;
    while(Mask)
    {
        if(Mask & 1)
        {
            scGridPoint* ThisCore = msProcessor->ByIDMask_Get(CoreMask);
            scHThread* ThisHThread = ThisCore->HThreadExec_Get(); // Get the actual thread
            if(ThisHThread->QTOffset_Get() == Offs) return ThisCore;  // The core found
        }
        CoreMask += CoreMask;
        Mask /=2;
    }
    return (scGridPoint*) NULL; // The core not found, probably already terminated
} // of scGridPoint::doFindHostToProcess

// This group of bits enables to monitor changes in the simulator object effectively
/*   gob_General,
       gob_ObjectMonitored, //!< Set if the object is monitored at all at this time
       gob_ObjectChanged,  //!< Set if the monitored object changed between monitoring, any of the features below
       gob_ObjectActive, //!< Set if the object on monitored active (allocated of preallocated) at this time
       gob_ObjectWaiting, //!< Set if the object is waiting at this time
   gob_OperationMonitored, //!< If any of the operations monitored
       gob_FetchPending, //!< The core can only make one fetch at a time, for all threads
       gob_ExecPending, //!< The core can only make one execute at a time, for all threads
       gob_RegistersMonitored, //!< Register changes are monitored
       gob_MemoryMonitored, //!< Register changes are monitored
   gob_MorphingMonitored, //!< Metainstructions and consequences monitored
       gob_ResourceMonitored, //!< Resources utilization monitored
       gob_ObjecIsMeta, //!< Set if the object is in Meta stata at this time
   gob_ArchitectureMonitored,
       gob_PortsMonitored, //!< Set if port changes monitored
       gob_SignalsMonitored, //!< Set if signal changes are monitored
   gob_BuffersMonitored,   //!< Set if any of the different buffers are monitored
       gob_BufferChanged,  //!< Set if any of the object-related
   gob_CommunicationMonitored, //!< Set if inter-core and inter-cluster communication monitored
       gob_ICCBChanged,    //!<
*/
   bool scGridPoint::
   OperatingBit_Get(GridPointOperatingBit_t B)
   {
       assert(B < gob_Max);
       return mGridPointOperatingStateBit[B];
   }
   bool scGridPoint::
   OperatingGroup_Get(GridPointOperatingBit_t G)
   {
       assert(G < gob_Max);
       assert( (gob_General == G) || (gob_OperationMonitored == G) || (gob_MorphingMonitored == G) ||
               (gob_ArchitectureMonitored == G) || (gob_BuffersMonitored == G) || (gob_CommunicationMonitored == G) );
       return mGridPointOperatingStateBit[G];
   }

   void scGridPoint::
   OperatingGroup_Set(GridPointOperatingBit_t G, bool B)
   {
       assert(G < gob_Max);
       assert( (gob_General == G) || (gob_OperationMonitored == G) || (gob_MorphingMonitored == G) ||
               (gob_ArchitectureMonitored == G) || (gob_BuffersMonitored == G) || (gob_CommunicationMonitored == G) );
       mGridPointOperatingStateBit[G] = B;
   }

   void scGridPoint::
   OperatingBit_Set(GridPointOperatingBit_t B, bool V)
   {
       assert(B < gob_Max);
       mGridPointOperatingStateBit[B] = V;
       if(V)
       {    // We are setting a value, be sure the group is also set
       if( (gob_ObjectMonitored == B) || (gob_ObjectChanged == B) || (gob_ObjectActive == B) || (gob_ObjectWaiting == B))
           OperatingGroup_Set(gob_General, true);
//           Operating
        }
       else
       {// We are clearing, clear group if no member remained
       }
   }



#ifdef other
    /*!
     * \brief scGridPoint::Clocked
     * Advances the scGridPoint's own clock counter
     */
    void scGridPoint::
Clocked(void)
{
     if(IsAllocated())
    {   // The scGridPoint is actively used at the moment
            performance->Clocks++;
//        DEBUG_PRINT( name() << " clocked at " <<  sc_time_to_nsec(1));
//        cerr << "Heartbeat @" << sc_time_to_nsec(1) << endl;
    }
//    next_trigger(clock.pos()); //
}
#endif //other
#ifdef CLOCKED
    void scGridPoint::
Clocked(void)
{
    while (true)
    {
        // Now a clock signal received, see what we can do
        if(IsAllocated())
        {   // The scGridPoint is actively used at the moment
            performance->Clocks++;
//        DEBUG_PRINT( name() << " clocked at " <<  sc_time_to_nsec(1));
//        cerr << "Heartbeat @" << sc_time_to_nsec(1) << endl;
        }
        wait();
    }
}
#endif // CLOCKED


#ifdef other

    /*!
     * Allocation/PreAllocation mechanism
     * The scGridPoints are able to reserve (to PreAllocate) some scGridPoint for their future use
     * The PreAllocated scGridPoints can only be allocated by the scGridPointe that PreAllocated them
     * The PreAllocated scGridPoint are returned to the pool when the QT on the PreAllocating scGridPoint terminates
     * The PreAllocation also means activation: the scGridPoint in the pool are sleeping,
     * PreAllocating them means that they will be ready (in two clock cycles) for operation
     * They can go back to "snooze" if not used for a longer time
     */


    // Allocation-related functionality
    void scGridPoint::
PreAllocatedMask_Clear(void)
{
    SC_GRIDPOINT_MASK_TYPE M = PreAllocatedMask_Get();    // Our own mask
    SC_GRIDPOINT_ID_TYPE Index = 0;
    while(M)
    {
        if(M&1)
        {   // Clear the corresponding bit in the scGridPoint
            // (and at the same time, in the topology's preallocated mask)
            Processor_Get()->GridPointByID_Get(Index)->PreAllocatedBit_Set(false);
         }
        M /= 2; Index++;
    }
}

        /*!
         * \brief scGridPoint::PreallocatedMask_Get
         * \return mask describing preallocated scGridPoint
         */
    SC_GRIDPOINTE_MASK_TYPE scGridPoint::
PreAllocatedMask_Get(void){ return msMask.PreAllocated; }

        /*!
         * \brief scGridPoint::PreallocatedMask_Set
         * Preallocate scGridPoint given by Mask
         * \param Mask describes scGridPoints to be preallocated
         * \return true if all scGridPoints could be preallocated, otherwise no scGridPoint gets preallocated
         */
    bool scGridPoint::
PreAllocatedMask_Set(SC_CORE_MASK_TYPE M)
   {
     if(M & Processor_Get()->CoresUnavailableMask_Get()) return false;
     // Now surely all requested cores can be preallocated
     SC_CORE_ID_TYPE Index = 0;
     while(M)
     {
         if(M&1)
         {   // Set the corresponding bit in the cores
             // (and at the same time, in the processor's preallocated
             Processor_Get()->CoreByID_Get(Index)->PreAllocatedBit_Set(true);
          }
         M /= 2; Index++;
     }
     return true;
   }
#endif //other
#ifdef other

    void scGridPoint::
ChildrenMaskBit_Set(scGridPoint* C, bool V) ///< Set the children mask during DeAllocation
    {   msMask.Children &= ~C->IDMask_Get();
        if(V) msMask.Children |= C->IDMask_Get();
    }
    void scGridPoint::
PreAllocatedMaskBit_Set(scGridPoint* C, bool V) ///< Set the children mask during PreAllocation
    {   msMask.PreAllocated &= ~C->IDMask_Get();
        if(V) msMask.PreAllocated |= C->IDMask_Get();
    }
#if 0

/*!
 * @brief	Creates an EMPA-aware scCore in a @ref sc Processor Proc
 *
 * @param[in] Proc The actual processor, owner of the cores
 * @param[in] ID The sequence number of the core
 */

scCore::scCore(sc_core::sc_module_name nm, sc Processor* Proc, SC_CORE_ID_TYPE ID, bool StandAlone)
    : scVirtualCore(nm, Proc, ID)

//
    ,msProcessor(Proc)
    ,msID(ID)
    ,msIDMask(1 << ID)
    ,msPrefetchValid(false)
    ,msPrefetchPending(false)
    ,msVectorMode(cvm_None)
{
  msBLfifo = new BL_FromSV_fifo("FromSV"); ///<  The backlinked register contents are latched here

  if(StandAlone)
    {
      DEBUG_PRINT_SC("StandAlone threading!!");
      SC_THREAD(FETCH_thread);
        sensitive << FETCH_event;
      SC_THREAD(NEXT_thread);
        sensitive << NEXT_event;
      SC_THREAD(QALLOC_thread)
        sensitive << QALLOC_event;
      SC_THREAD(QCALL_thread)
        sensitive << QCALL_event;
      SC_THREAD(QCREATE_thread)
        sensitive << QCREATE_event;
      SC_THREAD(QCREATEMETA_thread)
        sensitive << QCREATEMETA_event;
      SC_THREAD(QHALT_thread);
        sensitive << QHALT_event;
      SC_THREAD(QMUTEX_thread)
          sensitive << QMUTEX_event;
      SC_THREAD(QTERM_thread);
        sensitive << QTERM_event;
      SC_THREAD(QWAIT_thread);
        sensitive << QWAIT_event;
      SC_THREAD(QWAITMETA_thread);
        sensitive << QWAITMETA_event;
   }
  msSupervisor = Processor_Get()->Supervisor_Get();
  msMemory = Processor_Get()->Memory_Get();
  Reset((scCore*)NULL);
  PreallocatedMask_Clear();   // Preallocated bits are only cleared at creation!
}


    void scCore::
Reset(void)
  {
    msWaitMask = 0;
    msIsWaiting = false;
    msLinkRegisters[msNestingLevel] = R_NONE;
    msNewCoreState->pc = 0;
    msNewCoreState->cc = DEFAULT_CC;
    clear_mem(msNewCoreState->r);
    msICB->Reset();
  }

  /*!
   * \brief scCore::MUTEX_thread
   */
  void scCore::
QMUTEX_thread(void)
{
  DEBUG_PRINT("scCore::QMUTEX_thread started");
  while(true)
    {
      DEBUG_EVENT_SC("WAIT QMUTEX_event @" << PrologText_Get().c_str());
      wait(QMUTEX_event);
      DEBUG_EVENT_SC("RCVD QMUTEX_event @" << PrologText_Get().c_str());
      int AllocationMode = ExecuteStage.argA;
      register_t Allocationregister = ExecuteStage.argB;
     }
}


  /*!
   * \brief scCore::doQWait
   * \param Address of the QT we are waiting for, or -1 if wildcard
   * \param ForParent true if waiting in the parent rather than the core
   * \param WaitLatentChildren true if to wait also preallocated cores
   */

    void scCore::
QT_TerminateCooperation(void)
  {
     scCore* Parent = Parent_Get();
     if(Parent && Parent->VectorMode_Get())
     {// This core was in cooperation, terminate cooperation
         //    Parent->LatchedFromChild_Set(C->LatchedForParent_Get());
        switch(Parent->VectorMode_Get())
        {
         case cvm_FOR:
         case cvm_WHILE:
             // In modes ccm_FOR and ccm_WHILE, if the latched out register is 0, no more repetitions
             if(!Parent->ICB_Get()->msPseudoRegisters->FromChild) Parent->PreallocatedBits_Clear(IDMask_Get());
             if(!UNIT_TESTING)
//                  qInfo() << C->PrologText_Get().c_str() << "Latched for parent %esv" << Parent->msICB->msFromChild;
//              LOG_INFO_SC(C->PrologText_Get() << "Latched for parent %esv" << Parent->mICB->mFromChild);
           break;
         case cvm_SUMUP:
             break;
         default:;
        }
     }
  }


/*
  // Wait until the QT defined by its offset address terminates
  SC_CORE_MASK_TYPE scCore::
doWaitProcess(unsigned int W)
{
 scCore* CoreToWaitFor = doFindCoreToProcess(W);
 if(CoreToWaitFor)
     return CoreToWaitFor->IDMask_Get();
 else
     return (mask_t) NULL;
}
*/
/*!
 * \brief scCore::Byte_Get
 *
 * Get a byte at address PC from the memory
 * \param PC
 * \return byte value from the memory
 */
    byte_t scCore::
Byte_Get(SC_ADDRESS_TYPE PC)   {  return msMemory->Byte_Get(this,PC);  }///< Get the code at address PC

      /*!
       * \brief scCore::Word_Get
       *
       * Get a word at address PC from the memory
       * \param PC
       * \return word value from the memory
       */
    SC_WORD_TYPE scCore::
Word_Get(SC_ADDRESS_TYPE PC)   {  return msMemory->Word_Get(this,PC);  }///< Get the 32-bit value at address PC

     /*!
      * \brief scCore::PreallocatedMask_Get
      * \return mask describing preallocated cores
      */
    SC_CORE_MASK_TYPE scCore::
PreallocatedMask_Get(void){ return msICB->PreallocatedMask_Get(); }

     /*!
      * \brief scCore::PreallocatedMask_Set
      * Preallocate cores given by Mask
      * \param Mask describes cores to be preallocated
      * \return true if all cores could be preallocated, otherwise no core gets preallocated
      */
     bool scCore::
PreallocatedMask_Set(SC_CORE_MASK_TYPE Mask)
{
  if(Mask & msSupervisor->CoresUnavailableMask_Get()) return false;
  msICB->PreallocatedMask_Set(Mask); // Allocated for us
  msSupervisor->CoresPreallocatedMask_Set(msSupervisor->CoresPreallocatedMask_Get() | Mask); //and globally
  return true;
}

#endif
     // These functions check some electronic error in the implementation
     // They should be utilized in 'Core.cpp' in the same form, in order to provide the
     // true electronic functionality for the higher level

     /*!
      * \brief scVirtualCore::CatchedAllocationError
      * \return true if the core cannot be allocated
      *
      * A core must NOT be allocated if
      * - it already has children
      * - if is not available from any reason other than being preallocated for the requestot
      */
     bool scGridPoint::
CatchedAllocationError(scGridPoint* Parent)
{
  return !( // No error if
            IsAvailable()  // The core is available
           || (!IsAllocated() && IsPreAllocatedFor(Parent)) // or is preallocated for the requestor
            )
           || ChildrenMask_Get(); // or already has children
}

#if 0
     // Return true if the QT created legally:
     // We must be at a "create"-type instruction, and at its parameter show point to a "terminate"
/*    bool scCore::
CatchedFramingError()
{
  byte_t B = Code_Get(msNewCoreState->pc);
  B = Processor_Get()->Supervisor_Get()->MetaEvent_Get().Address;
  if(HPACK(I_QT, Q_CREATE) != B && HPACK(I_QT, Q_CREATET) != B && HPACK(I_QT, Q_CREATEF) != B && HPACK(I_QT, Q_CREATEM) != B)
  {
    msCoreStatus = STAT_FRM; // We are trying to create a QT using different instruction
    if ( (HPACK(I_QT, Q_TERM) == Code_Get(Word_Get(msNewCoreState->pc+(QCreate_Size-4)))) &&
      (HPACK(I_QT, Q_TERM) == Code_Get(Processor_Get()->Supervisor_Get()->MetaEvent_Get().Address
                  +Processor_Get()->Supervisor_Get()->MetaEvent_Get().Param )));
    return true;
  }
  else
  {
    msCoreStatus = STAT_FRM;
  }
}
*/

     /*!
      * \brief scCore::CheckedForFramingError
      * \return true if error catched
      *
      * It can be called both for 'Q' mode and 'R' mode
      */
     bool scCore::
CatchedFramingError(void)
{
  if(ExecuteStage.hi0 ==  (itype_t) I_QT)
    {// It looks like a quasi-thread
      qt_t QT_Subtype = MetaSubtype_Get();
      if( (QT_Subtype!=Q_CREATE) && (QT_Subtype!=Q_CREATET) && (QT_Subtype!=Q_CREATEF) && (QT_Subtype!=Q_CREATER)
              )
      { // It is not called in a CreateX metainstruction
        ExecuteStage.CoreStatus = STAT_FRM;
        return true;
      }
       // It is surely a "create"-type metainstruction, check if properly terminated
        byte_t B = Code_Get(PC_Get());
        B = Processor_Get()->Supervisor_Get()->MetaEvent_Get().Address;
        if((HPACK(I_QT, Q_CREATE) != B) && (HPACK(I_QT, Q_CREATET) != B) && (HPACK(I_QT, Q_CREATEF) != B)
                && (HPACK(I_QT, Q_CREATER) != B)
                )
             return false;
           return (HPACK(I_QT, Q_TERM) == Code_Get(Word_Get(msNewCoreState->pc+(QCreate_Size-4))));
    }
    else
    {// It can be a regular thread
      if(Parent_Get())
      {
        ExecuteStage.CoreStatus = STAT_QOP;
        return true;
      }
    }
    return false;
}

/*     if(IsMeta())
     {// It looks like a quasi-thread
       qt_t QT_Subtype = MetaSubtype_Get();
       if( (QT_Subtype!=Q_CREATE) && (QT_Subtype!=Q_CREATET) && (QT_Subtype!=Q_CREATEF) && (QT_Subtype!=Q_CREATEM))
         return (Core*)NULL;
       // It is surely a "create"-type metainstruction, check if properly terminated
       if(!Create_OK())
         if(!UNIT_TESTING)
         {
           LOG_CRITICAL( PrologText_Get().c_str() << "No matching QTerm");
           return (Core*)NULL;
         }
     }
     else
     {// It can be a regular thread
       if(!Parent_Get())
         {	// We have a real parent, illegal to use regular code
           if(!UNIT_TESTING)
             LOG_CRITICAL( PrologText_Get().c_str() << "Illegal use of regular code");
           return (Core*)NULL;
         }
     }
*/

#endif


    /*!
     * \brief scGridPoint::ReleasePreAllocatedCores
     * Upon terminating a QT, release pre-allocated cores, if any
     */
    void scGridPoint::
ReleasePreAllocatedCores(void)
{
    SC_CORE_MASK_TYPE M = PreAllocatedMask_Get();
    if(M)
    { // The core wants to be terminated, but some others are preallocated for it
        int i = 0;
        while(M)
        {
            if(M & 1)
            {
                scCore *C = Processor_Get()->CoreByID_Get(i);
                C->PreAllocatedBit_Set(false);
            }
            ++i; M = M >> 1;
        }
    }
}


    void scVirtualCore::
doCodeCloneMessage(char* Buffer)
    {

    }

    void scVirtualCore::
doCodeMemoryMessage(char* Buffer)
    {

    }

    /**
     * Access data memory to get data for LOAD OPs
     * @param  addr address to access to
     * @param size size of the data to read in bytes
     * @return data value read
     */
    SC_WORD_TYPE scVirtualCore::
readInstrMem(uint32_t addr, int size)
       { sc_time MemDelay = sc_time_stamp();
         performance->codeMemoryRead();
         SC_WORD_TYPE W = Processor_Get()->readInstrMem(addr, size);
          performance->MemoryTimeAdd(sc_time_stamp()-MemDelay);
        return  W;
    }

    /**
     * Access data memory to get data for LOAD OPs
     * @param  addr address to access to
     * @param size size of the data to read in bytes
     * @return data value read
     */
    uint32_t scVirtualCore::
readDataMem(uint32_t addr, int size)
       {
        sc_time MemDelay = sc_time_stamp();
        performance->dataMemoryRead();
        SC_WORD_TYPE W = Processor_Get()->readDataMem(addr, size);
        performance->MemoryTimeAdd(sc_time_stamp()-MemDelay);
        return  W;
    }
/*      uint32_t data;
      tlm::tlm_generic_payload trans;
      sc_time delay = SC_ZERO_TIME;

      trans.set_command( tlm::TLM_READ_COMMAND );
      trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
      trans.set_data_length( size );
      trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
      trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
      trans.set_dmi_allowed( false ); // Mandatory initial value
      trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
      trans.set_address( addr );

      Processor_Get()->data_bus->b_transport( trans, delay);

      if ( trans.is_response_error() ) {
        SC_REPORT_ERROR("Memory", "Read memory");
      }
      return data;
    }
    */
    /*!
     * \brief scVirtualCore::readData8
     * \param addr the address of the data byte
     * \return the value of the requested memory cell
     */
    unsigned char scVirtualCore::
readData8(uint32_t addr)
    {  return Processor_Get()->readData8(addr); }
/*       unsigned char data;
       tlm::tlm_generic_payload trans;
       sc_time delay = SCTIME_DMEM;
       trans.set_command( tlm::TLM_READ_COMMAND );
       trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
       trans.set_data_length( 1 ); // For size
       trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
       trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
       trans.set_dmi_allowed( false ); // Mandatory initial value
       trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
       trans.set_address( addr );
       Processor_Get()->data_bus->b_transport( trans, delay);
       if ( trans.is_response_error() ) {
         SC_REPORT_ERROR("Memory", "Read memory");
       }
       return data;
    }
    */
    /*!
     * \brief scVirtualCore::writeData8
     * \param addr the address of the data byte
     * \param data to be written
     */
    void scVirtualCore::
writeData8(uint32_t addr, unsigned char data)
    { Processor_Get()->writeData8(addr, data); }
/*       tlm::tlm_generic_payload trans;
       sc_time delay = SCTIME_DMEM;
       trans.set_command( tlm::TLM_WRITE_COMMAND );
       trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
       trans.set_data_length( 1 ); // For size
       trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
       trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
       trans.set_dmi_allowed( false ); // Mandatory initial value
       trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
       trans.set_address( addr );
       Processor_Get()->data_bus->b_transport( trans, delay);
       if ( trans.is_response_error() ) {
         SC_REPORT_ERROR("Memory", "Write memory");
       }
    }
*/
    /*!
     * \brief scVirtualCore::readData16
     * \param addr the address of the data byte
     * \return the value of the requested memory cell
     */
    uint16_t scVirtualCore::
readData16(uint32_t addr)
    { return Processor_Get()->readData16(addr); }
/*       uint16_t data;
       tlm::tlm_generic_payload trans;
       sc_time delay = SCTIME_DMEM;
       trans.set_command( tlm::TLM_READ_COMMAND );
       trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
       trans.set_data_length( 2 ); // For size
       trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
       trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
       trans.set_dmi_allowed( false ); // Mandatory initial value
       trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
       trans.set_address( addr );
       Processor_Get()->data_bus->b_transport( trans, delay);
       if ( trans.is_response_error() ) {
         SC_REPORT_ERROR("Memory", "Read memory");
       }
       return data;
    }
    */
    /*!
     * \brief scVirtualCore::writeData16
     * \param addr the address of the data byte
     * \param data to be written
     */
    void scVirtualCore::
writeData16(uint32_t addr, uint16_t data)
    { Processor_Get()->writeData16(addr, data);}
/*       tlm::tlm_generic_payload trans;
       sc_time delay = SCTIME_DMEM;
       trans.set_command( tlm::TLM_WRITE_COMMAND );
       trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
       trans.set_data_length( 2 ); // For size
       trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
       trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
       trans.set_dmi_allowed( false ); // Mandatory initial value
       trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
       trans.set_address( addr );
       Processor_Get()->data_bus->b_transport( trans, delay);
       if ( trans.is_response_error() ) {
         SC_REPORT_ERROR("Memory", "Write memory");
       }
    }
*/
    /*!
     * \brief scVirtualCore::readData32
     * \param addr the address of the data byte
     * \return the value of the requested memory cell
     */
    uint32_t scVirtualCore::
readData32(uint32_t addr)
    { return Processor_Get()->readData32(addr);}
/*       uint32_t data;
       tlm::tlm_generic_payload trans;
       sc_time delay = SCTIME_DMEM;
       trans.set_command( tlm::TLM_READ_COMMAND );
       trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
       trans.set_data_length( 4 ); // For size
       trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
       trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
       trans.set_dmi_allowed( false ); // Mandatory initial value
       trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
       trans.set_address( addr );
       Processor_Get()->data_bus->b_transport( trans, delay);
       if ( trans.is_response_error() ) {
         SC_REPORT_ERROR("Memory", "Read memory");
       }
       return data;
    }
    */
    /*!
     * \brief scVirtualCore::writeData32
     * \param addr the address of the data byte
     * \param data to be written
     */
    void scVirtualCore::
writeData32(uint32_t addr, uint32_t data)
    { Processor_Get()->writeData32(addr, data); }
    /*       tlm::tlm_generic_payload trans;
           sc_time delay = SCTIME_DMEM;
           trans.set_command( tlm::TLM_WRITE_COMMAND );
           trans.set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
           trans.set_data_length( 4 ); // For size
           trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
           trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
           trans.set_dmi_allowed( false ); // Mandatory initial value
           trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
           trans.set_address( addr );
           Processor_Get()->data_bus->b_transport( trans, delay);
           if ( trans.is_response_error() ) {
             SC_REPORT_ERROR("Memory", "Write memory");
           }
    */





         /*!
          * \brief scVirtualCore::IsPreallocatedFor
          * \param C the candidate parent
          * \return  true if the core is preallocated for C
          */
   bool scVirtualCore::
IsPreAllocatedFor(scVirtualCore* C)
{
  if(C)
    return C->PreAllocatedMask_Get() & msMask.ID;
  else // It may be reserved for the processor
    return  Processor_Get()->PreAllocatedForProcMask_Get()  & msMask.ID;
}

#if 0


    void scCore::
doRegisterCloneBack_write(void)
  {
    SC_CORE_MASK_TYPE M = BacklinkRegisterMask_Get();
    if(((M & 0x100) && !(M & 0xFF))  // Register mask is clear
        || (!(M & 0x100) && ((M & 0xFF)== R_NONE)) // or %eno explicitly asked
            )
        return;
//    if((reg_id_t) M == R_NONE) return;	// Requested not to clone
//    if(!M) return;	// Requested not to clone
    BL_FromCore_Transfer_Type MyBL;
    MyBL.CoreSelect = msParent[msNestingLevel]->ID_Get();
    MyBL.QTOffset = msICB->Offset_Get();
    reg_id_t R = R_EAX;
    if(M & 0x100)
      {// We have a register mask in M
        M = M & 0xFF;
        while(M)
          {
            if(M & 1)
            {
              MyBL.RegisterSelect = R;
              MyBL.RegisterContent = RegisterValue_Get(R, true);
              msSupervisor->BacklinkFIFO_Write(MyBL);
            }
            R = (reg_id_t)(R + 1); M = M >> 1;
          }
      }
    else
      {
        R = (reg_id_t) M;
        MyBL.RegisterSelect = R;
        if(R<=R_EDI)
          MyBL.RegisterContent = RegisterValue_Get(R, true);
        else if(R==R_ECC)
                MyBL.RegisterContent = RegCC_Get();
             else if(R==R_ESV)
                    MyBL.RegisterContent = msICB->msPseudoRegisters->ForParent;
        OldVal = MyBL.RegisterContent;
        ExecuteStage.OldReg = MyBL.RegisterSelect;
//        DEBUG_PRINT("EVENT SENT Core#" << ID_Get() << " R[" << reg_name(R) << hex << "]=" << MyBL.RegisterContent << " for core#" << MyBL.CoreSelect << " backlinked");
        Supervisor_Get()->BacklinkFIFO_Write(MyBL);
      }
    }

    bool scCore::
doRegisterCloneBack_read(SC_ADDRESS_TYPE Address, BL_FromSV_Transfer_Type &T)
{
  if(!msBLfifo->num_available())
    return false; // No reading if FIFO empty
  while(msBLfifo->num_available() && msBLfifo->doread(T,(SC_ADDRESS_TYPE)Address))
  { // Read the value from offset 'Address'
      RegisterValue_Set((reg_id_t) T.RegisterSelect, T.RegisterContent);
  }
  return true;
}

/*    void scCore::
RegisterCloneBack_write()
  {
     doRegisterCloneBack_write();
     msSupervisor->sc_BackLinkWrite_Event.notify();
  }*/
/*    void scCore::
doBacklinkedRegisters_Get(SC_ADDRESS_TYPE Address)
  { // This functionality is repeated in Core::doBacklinkedRegisters_Get
        while(msBLfifo->num_available())
        { // Read all backlinked contents
          BL_FromSV_Transfer_Type T;
          msBLfifo->doread(T,(SC_ADDRESS_TYPE)Address);
 //         int OldValue = RegisterValue_Get((reg_id_t) T.RegisterSelect);
          RegisterValue_Set((reg_id_t) T.RegisterSelect, T.RegisterContent);
//          DEBUG_PENDING(PrologText_Get().c_str() << "Backlinking R[" << reg_name((reg_id_t) T.RegisterSelect) << "]=" << hex << T.RegisterContent
  //                 << " from QT@" << T.QTOffset << " from old value " << OldValue << dec);
        }
  }
*/
#endif


#ifdef other

    // Mode set how to handle pseudo-register %esv
      CoreMode_t scCore::
    ESVMode_Get(void)
    {
          if(mICB->msParentMode)
              { // We are a child for mass processing
                return cm_Child;
              }
              else if(mICB->msPreallocatedMask)
              { // We are the parent in mass processing, PREprocessing
                return cm_ParentPRE;
              }
              else if(msVectorMode)
              { // Mass processing mode, POSTprocessing
                return cm_ParentPOST;
              }
              else
              { // Not special at all
                  return  cm_Normal;
              }
    }

#endif
#endif // other

#if 0
      void scGridPoint::
InititializeFunctionPointers(void)
{
    //http://stackoverflow.com/questions/30228452/function-pointer-within-class
    scGridPoint::Ptr_Inspect = &scGridPoint::InspectNothing;   // Anyhow, be sure that inpect pointer has a value
//    InspectSwitch(false); ?? Somehow the pointer is not set??
      }
      
    void scGridPoint::
InspectNothing(vector<int32_t>& in, vector<int32_t>& out){
    std::cerr << "Doing nothing, received " << in.size();
}
    
    void scGridPoint::
InspectDo(vector<int32_t>& in, vector<int32_t>& out){
    doInspect(in, out);
}

    /*!
     * \brief scGridPoint::doInspect
     *
     * Make the actual inspection; overwritten in subclasses
     * \param in    input data vector
     * \param out   output data vector
     */
    void scGridPoint::
doInspect(vector<int32_t>& in, vector<int32_t>& out)
{
        std::cerr << "scGridPoint Inspecting, received " << in.size();
}

    /*!
     * \brief scGridPoint::Inspect
     * \param[in] in describing the subject of the inspection
     * \param[out] out results of the inspection
     */
    void scGridPoint::
Inspect(vector<int32_t>& in, vector<int32_t>& out)
{
        return (this->*Ptr_Inspect)(in, out);
}
    /* !
     * \brief scGridPoint::ICB_b_transport
     * This can work in parallel with transport to other IGPCBs.
     * Technically, this method transfers the
     * \param Index of the sending member the
     * \param data to be sent
     * \param size of the data, bytes
     */
/*    void scGridPoint::
ICB_b_transport(const ClusterNeighbor Index, unsigned char* data, const int size )
{
    assert (!((GRID_SIZE_X < 0) || (GRID_SIZE_Y < 0)));// No such ICB
    assert(!(Index<cm_Head || Index>=cm_NW));
    assert (msIGPCB[Index]); // No neighbor there

    // Well, everything looks OK
    tlm::tlm_generic_payload trans;
        sc_time delay = SC_ZERO_TIME;
   trans.set_command( tlm::TLM_WRITE_COMMAND );
//        trans.set_data_ptr( reinterpret_cast<unsigned char*>(data) );
   trans.set_data_ptr( data );
   trans.set_data_length( size );
   trans.set_streaming_width( 4 ); // = data_length to indicate no streaming
   trans.set_byte_enable_ptr( 0 ); // 0 indicates unused
   trans.set_dmi_allowed( false ); // Mandatory initial value
   trans.set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
 //  trans.set_address( addr );
   // This activates the transfer; aour own IGPCB sends it, and the other ICB receives it
    (*IGPCB_Get(Index)->send_socket)->b_transport(trans, delay);
}
*/
    // Now the data appeared in the other IGPCB, tell the module it is there

/*#define LOG_MESSAGE_TERMINATED(TX,MYCLUSTER) \
    if(!UNIT_TESTING){std::cerr << "   >>> Message'" << string(TX).c_str() << "' terminated in (" \
        << (int)X << ',' << (int)Y << ")@" << MYCLUSTER.Cluster << ':' << ClusterMembers[MYCLUSTER.Member].name.c_str() << '\n';}*/
/*    // Define a macro to print a cluster address
#define PRINT_PROXY(C) \
    if(C->ClusterAddress.Proxy) {std::cerr << ":" << ClusterMembers[C->ClusterAddress.Proxy].name;}
#define PRINT_MESSAGE(M) \
    std::cerr << MessageTypes[M->Type_Get()]
#define PRINT_CLUSTER(C) \
    std::cerr << "(" << C->ClusterAddress.Cluster << "." << ClusterMembers[C->ClusterAddress.Member].name; \
    PRINT_PROXY(C); std::cerr << ")";
*/

#endif //0

