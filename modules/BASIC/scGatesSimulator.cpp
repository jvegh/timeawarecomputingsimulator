/** @file scSimulator.cpp
 *  @brief Function prototypes for the  scProcessor simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scGatesProcessor.h"

#include "Stuff.h"
#include "Project.h"
#include "BasicConfig.h"
#include "scGridPoint.h"
//#include "QStuff.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING
//#define MAKE_TIME_BENCHMARKING
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scGatesSimulator.h"
#include "scClusterBusMemorySlow.h"
// !!#include "scqTreeModel.h"

//#include "scGridPoint.h"    // Debug
//#include "scIGPMessage.h"   //  only


extern bool UNIT_TESTING;	// Whether in course of unit testing
// !! scClusterBusMemorySlow *MainMemory;
//EMPAPlotter *Plot;
//extern reg_struct reg_table[R_ERR+1];
//qQDir dir;
// This is the logging facility provided by Qt.
//
// http://www.qtcentre.org/threads/19534-redirect-qDebug()-to-file
//
// qDebug is disabled in release mode
// http://beyondrelational.com/modules/2/blogs/79/Posts/19245/how-to-put-logging-in-c-application-using-qt50.aspx

/*!
 * @class scSimulator
 * @brief The equivalent of the electronically implemented simulator
 * The simulator manipulates \see scProcessor, scCore, etc
 */

/*!
 * @brief	Creates an EMPA-aware @ref scSimulator, with scProcessor
 *
 * @param[in] nm The name of the simulator
 * @param[in] argc Number of arguments
 * @param[in] argv the argument list
 * @param[in] StandAlone if stand-alone operation requested
 * 
 *  * @verbatim
 *  |scSimulator
 *  |../ProcessorSimulator
 *  |..../XXProcessorSimulator
 * @endverbatim

 */

scGatesSimulator::scGatesSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone)
    : scqSimulator( nm, argc, argv )
//    ,msStepwiseMode(false)  // Initially, do not step
//    ,msSuspended(false) // Initially not suspended
{
     // This file will be sought in the system, user and project directories, in this order

    if(StandAlone)
    {
        // Create the main memory, belonging to the systems (presently only one processor)
 //       MainMemory = new scClusterBusMemorySlow("MainMemory", 0x00, FMAX_MEMORY_SIZE-1, 2);
// !!       MainMemory->clock(C1);    // The far memory uses clock; wait states measured in bus clock periods

        // Now specials can be loaded with special points; will be considered by the topology
 //       HandleSpecials();   /// Populate mSpecials with the found special scNeurers (from mSpecial)
 //!!  handle optional input file     MainMemory = new scMemory("Main_Memory", filename);
        msProcessor = new scGatesProcessor("Proc", mSpecials, true);
//        msProcessor->clock(C1);
        /// Read the setting files at different levels; most not when we have a processor
//        readSettings();
//        SetupHierarchies();

//        MainMemory->Y86loadHexFile(filename);
        SC_THREAD(SInitialize_method);
    }
    // As long as nothing more than sending out event in the derived simulator class
//    SC_THREAD(SSTART_thread);
//        sensitive << EVENT_SIMULATOR.HALT;
 /*   SC_METHOD(clock_method);
        sensitive << C1;*/

//    Plot = new EMPAPlotter(msProcessor, FileNameRoot.c_str());
 }
/*
void scSimulator::
SetupHierarchies(void)
{
    //mModuleTree = new scqTreeModel(msProcessor);
//    for(int i = 0; i < GRID_SIZE_X; i++)
//       for(int j = 0; i < GRID_SIZE_X; i++)
//            if()

    SC_GRIDPOINT_MASK_TYPE ActivePoints = Processor_Get()->AllocatedMask_Get();
    unsigned int index = 0;
    while(ActivePoints)
    {
     if(ActivePoints&1)
     {
         scGridPoint* GP = Processor_Get()->ByID_Get(index);
         assert(GP);
         DEBUG_PRINT("Active " << GP->Name_Get() << "==" << Processor_Get()->StringOfClusterAddress_Get(GP));
     }
     ActivePoints /= 2; index++;
    }

//    scqTreeModel* mClusterTree;

}
 */

/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
void scGatesSimulator::
SInitialize_method(void)
{
  DEBUG_PRINT_OBJECT("Initialize_method started" );

  // Now everything is ready, but nothing started
//  Processor_Get()->EVENT_PROCESSOR.START.notify();
/*  scGridPoint *GP = Processor_Get()->ByID_Get(10);
  scHThread *H1 = GP->HThread_Get(1);
  scHThread *H5 = GP->HThread_Get(5);
  scGridPoint *GP1 = Processor_Get()->ByID_Get(11);
  scHThread *H11 = GP1->HThread_Get(1);
  // These event are notified at the same time
  // The H1 and H5 are concurrent, H11 can run parallel
  H1->EVENT_HTHREAD.FETCH.notify(); //
  H5->EVENT_HTHREAD.FETCH.notify();
  H11->EVENT_HTHREAD.FETCH.notify();
 */
}

#if 0
/*!
 * \brief scSimulator::SSTART_thread
 * This thread is waiting for a START_event and processes it
 */
    void scGatesSimulator::
SSTART_thread(void)
{
    DEBUG_PRINT_OBJECT("SSTOP_thread started");
    while(true)
    {
        DEBUG_EVENT("WAIT EVENT_SIMULATOR.START");
        wait(EVENT_SIMULATOR.START);
        DEBUG_EVENT("EVENT_SIMULATOR.START");
        if(!Processor_Get()->Halted_Get())
        {
         LOG_INFO("Attempted to start a running processor");
        }
        else
        {
            Processor_Get()->EVENT_PROCESSOR.START.notify();
        }
    }

}
#endif //0

    void scGatesSimulator::
Setup(void)  // Belongs to the constructor
{
}

scGatesSimulator::~scGatesSimulator()
{
//    PrintFinalReport(msProcessor);

//    if(Plot){ delete Plot; Plot = 0;}
    DEBUG_PRINT(simulation_name << " scSimulator total time "
              << m_sum_time.count()/1000/1000 << " msecs");

}
