/** @file scHThread.cpp
 *  @brief Function prototypes for the EMPA simulator, H
 *
 */
/*
 *  *  @author János Végh (jvegh)
 *  @bug No known bugs.
*/
#include <algorithm>    // for sorting
#include "scHThread.h"
#include "scGridPoint.h"
#include "scProcessor.h"
#include  <systemc>

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
class scClusterBusMemorySlow;
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern scClusterBusMemorySlow* MainMemory;

// Handles a HW thread for scGridPoint
scHThread::scHThread(sc_core::sc_module_name nm
                       , scGridPoint* GP, SC_HTHREAD_ID_TYPE ID):
    sc_core::sc_module( nm)
  ,mTimedOut(false)
  ,msWaitTimeout(0)
  ,mGridPoint(GP)
  ,mParent(nullptr)
  ,mID(ID)
  ,mInstanceCount(0)
{
    SC_METHOD(TIMEOUT_method);
        dont_initialize();
        sensitive << EVENT_HTHREAD.TIMEOUT;
    SC_METHOD(MemoryContentArrived_method);
        sensitive << EVENT_HTHREAD.MemoryContentArrived;
        dont_initialize();
//    SC_THREAD(NEXT_thread);
 //       sensitive << EVENT_HTHREAD.NEXT;
    SC_METHOD(NEXT_method);
        sensitive << EVENT_HTHREAD.NEXT;
        dont_initialize();
    SC_METHOD(FETCH_method);
        sensitive << EVENT_HTHREAD.FETCH;
        dont_initialize();
    SC_METHOD(EXEC_method);
        sensitive << EVENT_HTHREAD.EXEC;
        dont_initialize();
    REGS = new SC_WORD_TYPE [32];
}// of scHThread::scHThread

scHThread::~scHThread()
{
    delete REGS;
}// of scHThread::~scHThread
// Message handling

    string scHThread::
StringOfClusterAddress_Get(void)
{   ostringstream oss;
#if USE_MODULE_NAMES
    oss << Owner_Get()->name();
#else
    oss << Owner_Get()->StringOfClusterAddress_Get();
#endif // USE_MODULE_NAMES
    SC_HTHREAD_ID_TYPE H =  ID_Get();
    if(H)
        oss << "/" << hex << (int16_t) H  << dec;
    return oss.str();
}

    string scHThread::
StringOfName_Get(void)
{
     ostringstream oss;
     if(mAliasName.length())
         return mAliasName;
     else
#if USE_MODULE_NAMES
     return name();
#else
     oss << StringOfDefaultName_Get();
     return oss.str();
#endif // USE_MODULE_NAMES
    }   ///

    string scHThread::
StringOfDefaultName_Get(void)
    {
      ostringstream oss;
      oss << Owner_Get()->ID_Get();
      SC_HTHREAD_ID_TYPE H =  ID_Get();
      if (H)
          oss <<   "/" << hex  << (int16_t) H << dec;
      return oss.str();
    }

    string scHThread::
StringOfClusterName_Get(void)
{
        ostringstream oss;
        SC_HTHREAD_ID_TYPE H =  ID_Get();
        oss << Owner_Get()->StringOfClusterAddress_Get();
        if(H)
            oss <<  hex << "/" << (uint16_t)  ID_Get() << dec;
        return oss.str();
}

    /*!
     * \brief Set an alias name for the thread
     * \param A the new alias name
     * \return true if the name used for the first time
     */
    bool scHThread::
StringOfAliasName_Set(string A)
{    if(Owner_Get()->Processor_Get()->ByName_Get(A))
        return false;   // The name was set already
    Owner_Get()->Processor_Get()->Alias_Add(A,this);
    mAliasName = A;
    return true;
}

//    bool HThreadCompare (scHThread* i,scHThread* j) { return ((uint64_t)i<(uint64_t)j); }

    void scHThread::
Child_Insert(scHThread* C)
{
    assert(C);   // Cannot be inserted
    int ChildIndex = Child_Find(C);
    assert(ChildIndex<0);  // Must not insert the same child again
    mChildren.push_back(C);
    sort(mChildren.begin(), mChildren.end());   // Keep the children sorted
}

    void scHThread::
Child_Remove(scHThread* C)
{
   assert(C);  // Cannot be removed
   uint MySize = mChildren.size()-1;
   std::remove (mChildren.begin(), mChildren.end(), C);
   mChildren.resize(MySize);
   mChildren.shrink_to_fit();   // Keep the list compact
}

    /*!
     * \brief Find child C in the list of children
     * \param C the address of the child
     * \return the index if found, else -1
     */
    int scHThread::
Child_Find(scHThread* C)
{
    int32_t right = mChildren.size(); // one position passed the right end
    if(!right) return -1;   // Not found if no children
    int32_t mid = 0, left = 0 ;
    uint64_t key = (uint64_t) nullptr;
    while (left < right) {
        if((uint64_t)mChildren[mid] == (uint64_t)C)
             return mid; // Accidentally, we found it
        mid = left + (right - left)/2;
        if (key > (uint64_t) mChildren[mid]){
                 left = mid+1;
             }
           else if (key < (uint64_t) mChildren[mid]){
             right = mid;
           }
        }
          // We arrived at the end, check if we found it
         if((uint64_t)mChildren[left] == (uint64_t)C)
             return left;
         else
             return -1;
}

    void scHThread::
Reset(void)
{
    PC_Set(Owner_Get()->Processor_Get()->PC_Get());
    for (unsigned int i = 0; i < 32; i++)
        REGS[i] = 0;
    mQT.Length = 0;
    mQT.Offset = CORE_DEFAULT_ADDRESS;
    OperatingBit_Set(tob_Regime,false);
    // Set the memory buffer empty
    mDataMemoryAddressBegin = mDataMemoryAddressEnd =
    mInstrMemoryAddressBegin = mInstrMemoryAddressEnd = MEMORY_DEFAULT_ADDRESS;
    mThreadOperatingBits = 0;
    mChildren.empty();  // No children
}


// This method is called when EVENT_THREAD.TIMEOUT.notify() times out
// Just a flag is set and the requestor can ask whether the
// function succeeded or timed out
void scHThread::
TIMEOUT_method(void)
{  mTimedOut = true;}

/*!
 * \brief Allocate a thread both in the owner scGridPoint and mark it as allocated
 * \param B
 */
void scHThread::
AllocatedBit_Set(bool B)
{   assert(Owner_Get()->HThreadAllocatedBit_Get(mID)); // If it was allocated already
    Owner_Get()->HThreadAllocatedBit_Set(mID,B);   // at scGridPoint
    OperatingBit_Set(tob_Allocated,true);  // at the scHThread
}
/*!
 * \brief Allocate a thread both in the owner scGridPoint and mark it as allocated
 * \param B
 */
void scHThread::
PreAllocatedBit_Set(bool B)
{   assert(Owner_Get()->HThreadAllocatedBit_Get(mID)); // This thread is in use
    Owner_Get()->HThreadAllocatedBit_Set(mID,B);   // at scGridPoint
    OperatingBit_Set(tob_PreAllocated,true);
}

/* The idea of HW thread handling (not yet implemented)
* The cores have a 'one-hot' mask where the bits identify the HW threads.
* When a QT gets allocated, the corresponding in HWthread.InUse is set,
* HWthread.ReadyToFetch is set and HWthread.ReadyToExecute is cleared.
* When the FETCH_thread or NEXT_thread starts, first they find out which
* of the threads can be operated: they AND the HWthread.InUse with
* HWthread.ReadyToFetch and HWthread.ReadyToExecute, respectively.
* The set bits mean ready to operate HW threads, the simple circular scheduler
* selects one of them and makes the operation. A limiting resource is the
* processing capacity, but the memory is much slower, i.e. multiple utilization
* is possible.
* The other limiting resource is how much threads can be served by the memory.
*/

/**
 * @fn scGridPoint::PrologString_Get
 * Assemble a heading text for 'gridpoint/core/neurer' messages
 *
 * @return string containing time, memory address, QT state, core state
 */
string scHThread::
PrologString_Get(void) // Get a unified prolog text for logging
{
    scHThread* T =Owner_Get()->HThreadExec_Get();
    Owner_Get()->HThreadExec_Set(this);
    string S = Owner_Get()->PrologString_Get();
    Owner_Get()->HThreadExec_Set(T);
    return S;
}

/*!
 * \brief  Put back a freed scHThread to the pool
 *
 * The deallocation takes place in one atomic step,
 * admistering properly the bits both in this scHThread (the child) and its parent scHThread

 * \return true if succeeded, false if it was not deallocated
 *
 * @see scHThread::AllocateFor
 */
 bool
scHThread::Deallocate(void)
{
    assert(OperatingBit_Get(tob_Allocated)); // Abort if the scHThread is not allocated
    /*if(ChildrenMask_Get() //& ~IDMask_Get()
       )
 return false;	// The core has children, must not be deallocated
 */
    assert(0 == ChildCount_Get());
    // OK, the HThread can be deallocated
    scHThread* Parent = Parent_Get();  // Yes, we do have a parent
    if(Parent)
    {// We do have a real parent scHThread
        DEBUG_PRINT_OBJECT( "Deallocating HThread from HThread" <<  Parent_Get()->StringOfClusterAddress_Get());
        Parent->Child_Remove(this);
    }
    else
    {// We were created by the root (processor)
        DEBUG_PRINT_OBJECT(hex << "Deallocating HThread from Processor");
        Owner_Get()->Processor_Get()->Child_Remove(this);
    }
    // Marks the thread as deallocated
    OperatingBit_Set(tob_Allocated,false);    // Set HThread's Allocated
    Parent_Set((scHThread*)NULL);   // Make sure we have no parent
    return true;
}

 /*!
  *
  * \brief Pre-Allocate this scHThread for Parent if possible
  * \param[in] Parent the parent scHThread
  * \return allocated scHTread if successful or NULL
  */
  scHThread* scHThread::
PreAllocateFor(scHThread* Parent)
{
    assert(!OperatingBit_Get(tob_PreAllocated));
    OperatingBit_Set(tob_PreAllocated,true);    // Set HThread's PreAllocated
    if(CatchedAllocationError(Parent) //|| !CatchedFramingError()
    )
 return (scHThread*) NULL; // For some reason, the core is not available
// Yes, it can be preallocated
    PreAllocatedBit_Set(true);
        /*
         Parent_Set(Parent);
if(Parent)
 Parent->PreAllocatedMaskBit_Set(this,true);
else // The processor is the parent
 Processor_Get()->PreAllocatedForProcMaskBit_Set(this,true);
 */
return this;
}
/*    void  scHThread::
  AllocatedBit_Set(bool V)
    { OperatingBit_Set(tob_Allocated,V);
      Owner_Get()->msMask.ActiveHThreads[H] = V;
    }
*/
  /*!
   *
   * \brief Allocate this scHThread as a child for Parent if possible
   *
   * An scHThread must be allocated before using it.
   * The allocation takes place in one atomic step,
   * administering properly both the vector in Parent and
   * the allocated bit in this thread
   * \param Parent the parent scHThread
   * \return allocated scHThread or NULL
   *
   * @see scHThread#Deallocate
   * @see scHThread#mChildren
   * @see scProcessor#mChildren
   */
    scHThread* scHThread::
AllocateFor(scHThread* Parent)
{
    assert(!OperatingBit_Get(tob_Allocated));   // It was allocated already
    if(CatchedAllocationError(Parent) //|| !CatchedFramingError()
        )
    return (scHThread*) NULL; // For some reason, the core is not available

// Yes, it can be allocated
    OperatingBit_Set(tob_Allocated,true);    // Set HThread as Allocated
    if(Parent)
    {
        Parent->Child_Insert(this);
    }
    else
    {
        Owner_Get()->Processor_Get()->Child_Insert(this);
    }
    Parent_Set(Parent);
    mInstanceCount++;  // Count how many times was instantiated
    AllocationBegin_Set(sc_time_stamp()); // Remember when the core was allocated
    return this;
}

    /*!
     * \brief Set up this scHThread to continue the job for Parent
     * \param Parent the parent thread; maybe NULL for the processor
     * 
     * Calls the core-specific cirtual method doSetQTAddresses
     *
     * @see scHThread#Deallocate
     * @see scHThread#AllocateFor
     */    
    void scHThread::
ResetForParent(scHThread* Parent)
{
     bool Pre = OperatingBit_Get(tob_PreAllocated);
     mThreadOperatingBits = 0;      // Clear all status bits
     OperatingBit_Set(tob_PreAllocated,Pre);    // Preserve preallocation
     Parent_Set(Parent); // Set its parent
     mChildren.empty(); // Remove all children, if any
//     mStringID = StringID_Set(Parent);
//     QTID_Set(QT_AssembleID(Parent));
#ifdef thread
    //          msWaitCount = 0; // Not waiting when allocated
   
   /*!!     SC_HTHREAD_ID_TYPE H = Parent->ClusterAddress_Get().HThread;
        mHThreads[H]->QTRegime_Set(Parent); // 'true' if we have a real parent
        mHThreads[H]->QTID_Set(QT_AssembleID(Parent));
   */     Owner_Get()->doSetQTAddresses(Parent); // derived classes may set QT addresses differently
        if(Parent)
        {	// We have another core as parent
            Parent->ChildrenMaskBit_Set(this);
            LOG_TRACE_SOURCE("Allocated for '" + Parent->PrologText_Get() + "'");
        }
        else
        {	// The processor is the parent
            Processor_Get()->ChildrenMaskBit_Set(this);
            LOG_TRACE_SOURCE("Allocated for '" + Processor_Get()->PrologText_Get());
        }
         CooperationMode_Set(ccm_None);	// Not in mass operating mode
#endif //
}
    
    ClusterAddress_t scHThread:: ///<  Return the cluster address of the scHThread
ClusterAddress_Get(void){ assert(Owner_Get()); ClusterAddress_t  CA =Owner_Get()->ClusterAddress_Get(); CA.HThread = ID_Get(); return CA;}

// The routine attempts to 'own' the fetch facilities of the scGridpoint and fetches the next instruction
// The method is basically sensitive to EVENT_HTHREAD.FETCH
// If some other thread uses the fetching facility of the core,
// it is made temporarily sensitive to EVENT_GRID.FetchReady of the owner scGridPoint
    void scHThread::
FETCH_method(void)
{
    // A request to fetch the next instruction is received: check availability of our core's FETCH
    scGridPoint* GP = Owner_Get();
    assert(GP);  // Be sure that we have an scGridPoint
    OperatingBit_Set(tob_FetchPending,true);    // Set HThread's FetchPending
    if(GP->OperatingBit_Get(gob_FetchPending))  // Ask if Gridpoint's Fetch is available
    { // The fetch is going on, check if we see out own ownership
         if(GP->HThreadFetch_Get()->ID_Get() == ID_Get())
        {   // it was me who locked the GP
            DEBUG_EVENT_OBJECT("EVENT_GRID.FetchReady received, fetch finished") ;
            GP->OperatingBit_Set(gob_FetchPending, false); // Now the scGridPoint is free
            OperatingBit_Set(tob_FetchValid,true);
            OperatingBit_Set(tob_FetchPending,false);
            next_trigger(EVENT_HTHREAD.FETCH);  // This fetch gone, restore trigger
            EVENT_HTHREAD.FETCHED.notify();    // Now the fetched instruction is available
        }
        else
        {
            // Sorry, we must wait: some other thread is fetching
            // Temporarily change trigger to GP->FetchReady
            DEBUG_EVENT_OBJECT("SENT EVENT_GRID.MakeFetch, again") ;
            GP->EVENT_GRID.MakeFetch.notify(SC_ZERO_TIME);  // Repeat request
            next_trigger(GP->EVENT_GRID.FetchReady);    // Wait until owner scGridPoint is free again
            DEBUG_EVENT_OBJECT("WAIT >EVENT_GRID.FetchReady") ;
        }
    }
    else
    {   // OK, the FETCH unit is available
        GP->OperatingBit_Set(gob_FetchPending, true); //  make our scGridPoint busy
        GP->HThreadFetch_Set(this);  // Tell who keeps GP busy
        // Just to survive debugging!!!
        GP->HThreadExec_Set(this);  // Tell who keeps GP busy
        DEBUG_EVENT_OBJECT("EVENT_GRID.FETCH started " ) ;
        GP->EVENT_GRID.MakeFetch.notify(SC_ZERO_TIME);
        next_trigger(GP->EVENT_GRID.FetchReady);    // Wait until owner scGridPoint is free again
    }
}

    // The routine attempts to 'own' the exec facilities of the scGridpoint and executes the next instruction
    // The method is basically sensitive to EVENT_HTHREAD.EXEC
    void scHThread::
EXEC_method(void)
{
    // A request to execute the next instruction is received: check availability of our core's EXEC
    scGridPoint* GP = Owner_Get();
    assert(GP);  // Be sure that we have an scGridPoint
    if(GP->OperatingBit_Get(gob_ExecPending))  // Ask if Gridpoint's Exec is available
    { // The execution is going on, check if we see out own ownership
         if(GP->HThreadExec_Get()->ID_Get() == ID_Get())
        {   // it was me who locked the GP
            DEBUG_EVENT_OBJECT("EVENT_GRID.ExecReady received, exec finished") ;
            GP->OperatingBit_Set(gob_ExecPending, false); // Now the scGridPoint is free
            OperatingBit_Set(tob_ExecPending,false);
            next_trigger(EVENT_HTHREAD.EXEC);  // This exec gone, restore trigger
            EVENT_HTHREAD.EXECUTED.notify();
        }
        else
        {
            // Sorry, we must wait: some other thread is executing
            // Temporarily change trigger to GP->ExecReady
            DEBUG_EVENT_OBJECT("SENT EVENT_GRID.MakeExec, again") ;
            GP->EVENT_GRID.MakeExec.notify(SC_ZERO_TIME);  // Repeat request
            next_trigger(GP->EVENT_GRID.ExecReady);    // Wait until owner scGridPoint is free again
            DEBUG_EVENT_OBJECT("WAIT >EVENT_GRID.ExecReady") ;
        }
    }
    else
    {   // OK, the EXEC unit is available
        GP->OperatingBit_Set(gob_ExecPending, true); //  make our scGridPoint busy
        GP->HThreadExec_Set(this);  // Tell who keeps GP busy
        // Tell the scGridPoint it can execute
        GP->EVENT_GRID.MakeExec.notify();//SC_ZERO_TIME);
        // Wait until the gridpoints do the job
        next_trigger(GP->EVENT_GRID.ExecReady);    // Wait until owner scGridPoint is free again
        return;
    }
    next_trigger(EVENT_HTHREAD.EXEC);   // Will wait for an EXEC again
}

    /*!
     * \brief NEXT_method
     *
     * Sensitive to EVENT_HTHREAD.NEXT
     *
     * If the fetch is not yet finished, sensitivity temporary set to
     * EVENT_HTHREAD.FETCHED
     *
     * An scHThread performs a sequential processing of a code fragment;
     * i.e., it takes one instruction and executes it. The intruction can be pseudo or conventional instruction.
     */
    void scHThread::
NEXT_method()
{
    DEBUG_EVENT_OBJECT("RCVD EVENT_HTHREAD.NEXT");
    // The currect instruction  may be fetched or non fetched
    FinishPrefetching(); // Make sure fetching finished
    if(!OperatingBit_Get(tob_FetchValid))
       { // Not yet, but the fetch initiated, wait until ready
        next_trigger(EVENT_HTHREAD.FETCHED); return;}
    // We surely have a fetched instruction
//    bool IsMorphingInstruction = Owner_Get()->IsMorphingInstructionFetched(this);

        // The previous instruction is finished, the current one not yet started
//        HandleSuspending(); // Suspend if the simulator asks so; solved at scGridPoint level

      // Now we surely have a fetched instruction: execute it

      // Instruction performance is handled separatedly for conventional and meta instructions
    //!!    sc_time ExecDelay = sc_time_stamp();

        if(OperatingBit_Get(tob_MorphFetched))
            HandleMorphingInstruction();
        else
            HandleConventionalInstruction();
//        TerminateExecution();

        //    performance->ExecTimeAdd(sc_time_stamp()-ExecDelay);

/*   if(OperatingBit_Get(tob_FetchValid))
    { // we have a prefetched value
        sc_time FetchTime = sc_time_stamp()-fetch.ExecutionTime;    // Compute when we started fetching
        PrepareNextInstruction();   // Prepare the counters to advance
        DEBUG_ONLY(sc_time WaitTime = scTimeStamp() - fetch.WaitTimeBegin; \
        if(WaitTime>SC_ZERO_TIME) \
            DEBUG_EVENT_OBJECT("Proceeding after fetching delay " << sc_time_to_nsec_Get(2,8,FetchWaitTime) << " ns");)
    //               PERFORMANCE_ADDFETCHTIME(FetchTime);
    //!!               PERFORMANCE_ADDWAITTIME(FetchWaitTime);
     }
    else
    {   // it is an emergency case: for some reason, the FETCH was not executed

    }

    DEBUG_EVENT_OBJECT("RCVD EVENT_CORE.NEXT");
    // First finish prefetching, maybe it is running
    FinishPrefetching(); // Make sure fetching finished

    // We are at the end of 'NEXT', restore original sensitivitiy
    next_trigger(EVENT_HTHREAD.NEXT);
*/
}

    void scHThread::
    HandleMorphingInstruction()
    {}
void scHThread::
HandleConventionalInstruction()
{}

#ifdef older
    /*!
       * \brief AbstractCore::NEXT_thread
       * runs when a next instruction is due to process
       * It handles prefetching, suspending operation,
       * handles execution of meta instructions and conventional instructions
       *
       * The idea of HW thread handling (not yet implemented)
       * The cores have a 'one-hot' mask where the bits identify the HW threads.
       * When a QT gets allocated, the corresponding in HWthread.InUse is set,
       * HWthread.ReadyToFetch is set and HWthread.ReadyToExecute is cleared.
       * When the FETCH_thread or NEXT_thread starts, first they find out which
       * of the threads can be operated: they AND the HWthread.InUse with
       * HWthread.ReadyToFetch and HWthread.ReadyToExecute, respectively.
       * The set bits mean ready to operate HW threads, the simple circular scheduler
       * selects one of them and makes the operation. A limiting resource is the
       * processing capacity, but the memory is much slower, i.e. multiple utilization
       * is possible.
       * The other limiting resource is how much thread can be served by the memory.
       */
    void AbstractCore::
    NEXT_thread(void)
    {
        while(true)
        {
            DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.NEXT");
            wait(EVENT_CORE.NEXT);
            DEBUG_EVENT_OBJECT("RCVD EVENT_CORE.NEXT");

        // The currect instruction  may be fetched or non fetched
        FinishPrefetching(); // Make sure fetching finished
    //    bool IsMetaInstruction = IsMetaInstructionFetched();

        // The previous instruction is finished, the current one not yet started
        HandleSuspending(); // Suspend if the simulator asks so; solved at scGridPoint level

      // Now we surely have a fetched instruction: execute it

      // Instruction performance is handled separatedly for conventional and meta instructions
    //!!    sc_time ExecDelay = sc_time_stamp();

        if(IsMetaInstructionFetched())
            HandleMetaInstruction();
        else
            HandleConventionalInstruction();
        TerminateExecution();

        //    performance->ExecTimeAdd(sc_time_stamp()-ExecDelay);
        }
    }//AbstractCore::   NEXT_thread
#endif
    /*!
 * \brief Set the 'Observed' bit for this scHThread
 * \param B true if this thread is observed
 */
    void scHThread::
ObservedBit_Set(bool B)
{
    OperatingBit_Set(tob_Observed,B);
    Owner_Get()->ObservedHThreadBit_Set(ID_Mask_Get(),B);
}



        /** @brief The basic ISA functionality is taken from Y86
         *
         * Added state store/restore
         * Error handling: strings are returned, rather than direct printing
         *
         */
#ifdef kellezide
         bool
         scHThread::
     Y86doExecuteInstruction(void)
     {
         SC_WORD_TYPE //!!OldMem, OldVal,
                 argA, argB;
         uint32_t val//, dval
                 ;
         LOG_TRACE_SOURCE("Executing instruction");
         //performance->instructionsInc();
             exec.ExecutionTime = EXECUTION_TIME;  // This is a base time
#ifdef newdef
             PC_Affected_Set(false);  // Set if the PC is affected by the current instruction
             msCoreStatus = STAT_AOK;   // Assume success
             ostringstream oss;  // For the instruction in the switch
             // Now branch on instruction
         switch (exec.Stage.OpCode) {
           case I_NOP: break;
              PC_Set( exec.PC );
          case I_RRMOVL:  // Both unconditional and conditional moves
    //         if (!exec.Stage.ok1)
      //       { exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!reg_valid((reg_id_t)exec.Stage.RSource1))
             { msCoreStatus = STAT_REG; break;}
             if (!reg_valid((reg_id_t)exec.Stage.RDest))
             { msCoreStatus = STAT_REG; break;}
             // exec.Stage.
             val = RegisterValue_Get((reg_id_t)exec.Stage.RSource1);
     //         OldMem = RegisterValue_Get(ExecuteStage.lo1);
     //        if (cond_holds(RegCC_Get(), (cond_t)ExecuteStage.lo0))
                      RegisterValue_Set((reg_id_t)exec.Stage.RDest, //exec.Stage.
                                        val);
                 LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=", val );
    //!!             PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
      //           LOG_TRACE_VALUE("Reg[" +string(reg_table[exec.Stage.lo1].name) + "]<=", //exec.Stage.
      //                   val );
              break;
           case I_IRMOVL:
    //          if (!exec.Stage.ok1)
      //        {exec.Stage.CoreStatus = STAT_ADR; break;}
              if (!exec.Stage.okc)
              { msCoreStatus = STAT_INS; break;}
              if (!reg_valid((reg_id_t)exec.Stage.RDest))
              { msCoreStatus = STAT_REG; break;}
              //exec.Stage.
    //!!                  OldReg = RegisterValue_Get(exec.Stage.lo1, false);
              RegisterValue_Set((reg_id_t)exec.Stage.RDest, exec.Stage.immediate);
              LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.Stage.immediate );
    //!!          PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
     //       //    mLastAddr = immediate; // Do no scroll to this address
              break;
           case I_RMMOVL:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
             if (!reg_valid((reg_id_t)exec.Stage.RSource1))
             { msCoreStatus = STAT_REG; break;}
              if (reg_valid((reg_id_t)exec.Stage.RDest) //|| (exec.Stage.hi1==R_ESV)
                      )
                exec.Stage.immediate += RegisterValue_Get((reg_id_t)exec.Stage.RDest,false);
     //         get_word_val(msCoreState->m, ExecuteStage.immediate, &OldMem);
     //!!         OldMem = readData32(exec.Stage.immediate);
     //         OldMem = MainMemory->DirectGet(exec.Stage.immediate);
                exec.val = RegisterValue_Get((reg_id_t)exec.Stage.RSource1,false);
     //         if (!set_word_val(msCoreState->m, ExecuteStage.immediate, ExecuteStage.val))
     //           return STAT_ADR;
                writeInstrMem(exec.Stage.immediate, exec.val);
     //!!         writeData32(exec.Stage.immediate, exec.val);
              oss << "0x" << hex << exec.Stage.immediate << dec;
              LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", exec.val );

     //!!        PLOT_QT_MEMORY_DEBUG(this,exec.Stage.immediate,exec.val,true);
             //         PC_Set( ExecuteStage.PC);
              break;
           case I_MRMOVL:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
              if (!reg_valid((reg_id_t)exec.Stage.RSource1))
              { msCoreStatus = STAT_REG; break;}
               //!!
    //!!          OldVal = RegisterValue_Get(exec.Stage.hi1,false);
              if (reg_valid((reg_id_t)exec.Stage.RDest) //|| (exec.Stage.lo1==R_ESV)
                      )
                exec.Stage.immediate += RegisterValue_Get((reg_id_t)exec.Stage.RDest,false);
              val = readInstrMem(exec.Stage.immediate);
    //!!                  val = readData32(exec.Stage.immediate);
    //!!                  PLOT_QT_MEMORY_DEBUG(this,exec.Stage.immediate,val,false);
     //         if (!get_word_val(msCoreState->m, exec.Stage.immediate, &exec.Stage.val))
     //!!            return STAT_ADR;
               LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RSource1].name) + "]<=",val );
              RegisterValue_Set((reg_id_t)exec.Stage.RSource1, val,false);
    //!!          PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.hi1, true);
              break;
           case I_ALU:
    //          if (!exec.Stage.ok1)
      //        { exec.Stage.CoreStatus = STAT_ADR; break;}

              argA = RegisterValue_Get( (reg_id_t)exec.Stage.RSource1);
      /*        if((exec.Stage.lo1 == R_ESV) && (ParentMode_Get()==cvm_SUMUP) && (ExecuteStage.lo0==A_ADD))
                { // It is an exception: just latch the value for the parent and trigger addition
                  OldMem = Parent_Get()->msICB->msPseudoRegisters->FromChild;
                  Parent_Get()->msICB->msPseudoRegisters->FromChild = OldMem + ExecuteStage.argA;
                }
              else // It is just a normal operation
     */            if (reg_valid((reg_id_t)exec.Stage.RDest) //|| (exec.Stage.lo1==R_ESV)
                       )
                   {
                      argB = RegisterValue_Get((reg_id_t)exec.Stage.RDest,false);
                      exec.val = compute_alu((alu_t)exec.Stage.FunctCode, argA, argB);
                      //!!OldVal = RegisterValue_Get(exec.Stage.lo1);
                      RegisterValue_Set((reg_id_t)exec.Stage.RDest, exec.val,false);
                      LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.val );
                            //                 OldCC = RegCC_Get();
    //                  PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
                      RegCC_Set( compute_cc((alu_t)exec.Stage.FunctCode, argA, argB));
                   }
               break;
           case I_JMP:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
             // The fetch must recognize that no prefetch should happen
             assert(!fetch.Pending);
             assert(!fetch.Valid);
             PC_Affected_Set(true);
             if (cond_holds(RegCC_Get(), (cond_t)exec.Stage.FunctCode))
             {
      //           PLOT_EXEC_BALL(sc_time_stamp(), exec.ExecutionTime);
                 exec.PC = exec.Stage.immediate; // Will continue there
                 fetch.PC = exec.PC; // but pick it up first
     //            fetch.NewPC =  exec.Stage.immediate;
     //            exec.PC = fetch.PC;
             }
               break;
           case I_CALL:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
             // The fetch must recognize that no prefetch should happen
             assert(!fetch.Pending);
             assert(!fetch.Valid);

                 PC_Affected_Set(true);
                 exec.val = RegisterValue_Get(R_ESP) -4;   // The memory for the return address
    //!!             OldMem = MainMemory->DirectGet(exec.val);
                 RegisterValue_Set(R_ESP,  exec.val);
                 LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.val );
                 oss << "0x" << hex << exec.val << dec;
                 LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", fetch.PC );
    //             PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
    //             writeData32(exec.val, fetch.PC);
      //           PLOT_QT_MEMORY_DEBUG(this,exec.val,fetch.PC,true);
     //            MainMemory->DirectPut(exec.val, fetch.PC);
                 writeInstrMem(exec.val, fetch.PC);
                 exec.PC = exec.Stage.immediate; // Will continue there
                 fetch.PC = exec.PC; // but pick it up first
                break;
           case I_RET:
               // Return Instruction.  Pop address from stack
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
             // The fetch must recognize that no prefetch should happen
             assert(!fetch.Pending);
             assert(!fetch.Valid);
               PC_Affected_Set(true);
               exec.dval = RegisterValue_Get(R_ESP);
               exec.val = readInstrMem(exec.dval);
    //!!           exec.val = readData32(exec.dval);
                exec.dval += 4;
               RegisterValue_Set(R_ESP, exec.dval);

               LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval );
        //       PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
          //     PLOT_QT_MEMORY_DEBUG(this,exec.dval,exec.val,false);
               exec.PC = exec.val; // Will continue there
               fetch.PC = exec.PC; // but pick it up first
             break;
            case I_PUSHL:
    //            if (!exec.Stage.ok1)
      //              {exec.Stage.CoreStatus = STAT_ADR; break;}
                if (!reg_valid((reg_id_t)exec.Stage.RSource1))
                    { msCoreStatus = STAT_REG; break;}
                exec.val = RegisterValue_Get((reg_id_t)exec.Stage.RSource1);   // The register content to push
                exec.dval = RegisterValue_Get(R_ESP) - 4;
    //!!            OldMem = readInstrMem(exec.dval);
    //!!           OldMem = MainMemory->DirectGet(exec.dval);
                RegisterValue_Set(R_ESP,exec.dval);
                LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval );
                oss << "0x" << hex << exec.dval << dec;
                LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", exec.val );
    //!!           PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
    //!!           PLOT_QT_MEMORY_DEBUG(this,exec.dval,exec.val,true);
    //!!           writeData32(exec.dval, exec.val );
                writeInstrMem(exec.dval, exec.val);
                break;
         case I_POPL:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!reg_valid((reg_id_t)exec.Stage.RSource1))
             { msCoreStatus = STAT_REG; break;}
    //!!            OldMem = RegisterValue_Get(exec.Stage.hi1);  // The old register content
                exec.dval = RegisterValue_Get(R_ESP) ;        // Where ESP points to
                exec.val = readInstrMem(exec.dval);
                exec.dval += 4;
                RegisterValue_Set(R_ESP, exec.dval);       // Update ESP
                LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval);
    //!!            exec.val = readData32(exec.dval);
                RegisterValue_Set((reg_id_t)exec.Stage.RSource1, exec.val);
                LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RSource1].name) + "]<=",exec.val );
    //!!            PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
             break;
     /*      case I_LEAVE:
             if (!exec.Stage.okc)
             {exec.Stage.CoreStatus = STAT_INS; break;}
               // The fetch must recognize that no prefetch should happen
             assert(!fetch.Pending);
             assert(!fetch.Valid);
               ExecuteStage.dval = get_reg_val(Registers_Get(), R_EBP);
               set_reg_val(Registers_Get(), R_ESP, ExecuteStage.dval+4);
                if (!get_word_val(msNewCoreState->m, ExecuteStage.dval, &ExecuteStage.val))
                  return STAT_ADR;
               set_reg_val(Registers_Get(), R_EBP, ExecuteStage.val);
               PC_Set( ExecuteStage.PC );
               break;
     */      case I_IADDL:
    //         if (!exec.Stage.ok1)
      //       {exec.Stage.CoreStatus = STAT_ADR; break;}
             if (!exec.Stage.okc)
             { msCoreStatus = STAT_INS; break;}
             if (!reg_valid((reg_id_t)exec.Stage.RDest))
             { msCoreStatus = STAT_REG; break;}
               argB = RegisterValue_Get((reg_id_t)exec.Stage.RDest);
               exec.val = argB + exec.Stage.immediate;
               RegisterValue_Set((reg_id_t)exec.Stage.RDest, exec.val);
               LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.val );
    //           PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
               RegCC_Set( compute_cc(A_ADD, exec.Stage.immediate, argB));
               break;
           default: // Some unrecognized intruction received
               msCoreStatus = STAT_INS;
           }
#endif // newdef
         return true;
//         return Y86STAT_AOK == Owner_Get()->CoreStatus_Get();
    } // of scHThread::Y86doExecuteInstruction
#endif //kellezide
#if 0
void scHThread::NEXT_thread(void)
{
    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.NEXT");
        wait(EVENT_HTHREAD.NEXT);
        DEBUG_EVENT_OBJECT("RCVD EVENT_CORE.NEXT");
/*
    // The currect instruction  may be fetched or non fetched
    FinishPrefetching(); // Make sure fetching finished
//    bool IsMetaInstruction = IsMetaInstructionFetched();

    // The previous instruction is finished, the current one not yet started
    HandleSuspending(); // Suspend if the simulator asks so; solved at scGridPoint level

  // Now we surely have a fetched instruction: execute it

  // Instruction performance is handled separatedly for conventional and meta instructions
//!!    sc_time ExecDelay = sc_time_stamp();

    if(IsMetaInstructionFetched())
        HandleMetaInstruction();
    else
        HandleConventionalInstruction();
    TerminateExecution();

    //    performance->ExecTimeAdd(sc_time_stamp()-ExecDelay);
    */
    }
}
#endif //0
/*!
 * \brief scHThread::FinishPrefetching
 * At the beginning of executing NEXT_method, when this function called,
 * the next instruction the prefetch operation may be
 * 1) already finished,
 * 2) still pending
 * 3) not yet started
 * When this subroutine returns, the prefetch operation is surely finished
 * and the core is ready to execute the finished operation
 */


/*!!       if(mTimedOut)
       {// During execution a timeout occurred: must kill the execution of the QT
           doKillQT();
           mTimedOut = false;
       }*/


/*
 * This utility routine make sure that the next instruction is fetched; if needed, makes fetching
 */
    void scHThread::
FinishPrefetching(void)
{
    if(OperatingBit_Get(tob_FetchValid))
    {   // The prefetch was started in the FETCH_thread, it is already finished
        DEBUG_ONLY(sc_time FetchTime = sc_time_stamp()-fetch.ExecutionTime; )   // Compute when we started
        DEBUG_ONLY(sc_time WaitTime = scTimeStamp() - fetch.WaitTimeBegin; \
        if(WaitTime>SC_ZERO_TIME) \
            DEBUG_EVENT_OBJECT("Proceeding after fetching delay " << sc_time_to_nsec_Get(2,8,FetchWaitTime) << " ns");)
//               PERFORMANCE_ADDFETCHTIME(FetchTime);
//!!               PERFORMANCE_ADDWAITTIME(FetchWaitTime);
        PrepareNextInstruction();   // Prepare the counters to advance
        // Will be called when next 'FETCH' arrives
        next_trigger(EVENT_HTHREAD.FETCH);    // Wait until finished
        EVENT_HTHREAD.EXEC.notify(SC_ZERO_TIME);
    }
    else
    { //This core has NO valid prefetched instruction
        if(!OperatingBit_Get(tob_FetchPending))
        { // Prefetching even not started, initiate it, then
            OperatingBit_Set(tob_FetchPending,true); // Prefetching started
            fetch.WaitTimeBegin = sc_time_stamp();    // Mark the beginning of waiting for a fetch
            fetch.PC = fetch.NewPC;
//            exec.PC = fetch.PC;
            EVENT_HTHREAD.FETCH.notify();
            DEBUG_EVENT_OBJECT("SENT EVENT_HTHREAD.FETCH (emergency)");
        }
        // Now anyhow, we do have a pending fetch request
        DEBUG_EVENT_OBJECT("WAIT EVENT_HTHREAD.FETCHed");
        next_trigger(EVENT_HTHREAD.FETCHED);    // Wait until finished
//          PLOT_FETCH_WAIT_BALL(sc_time_stamp()-FetchWaitTime,FetchWaitTime);
//!!          if(WT!= FetchStage.ExecutionTime.value())
//            Plotter_Get()->PlotQTfetchwait(ID_Get(),mFetchDelayTime,PC_Get(),WT);
        // Anyhow, we need to see you again when fetch finished
    }
       //PLOT_FETCH_BALL(fetch.ExecutionTime, FetchTime//-FetchWaitTime
                       //);
}


    void scHThread::
OperatingBit_Set(ThreadOperatingBits_t B, bool V)
{
    assert(B < tob_Max);
    mThreadOperatingBits[B] = V;
}

   void scHThread::
PrepareNextInstruction()
{
    // Now ready with the conventional instruction fecthing
    OperatingBit_Set(tob_FetchValid,false);
    OperatingBit_Set(tob_FetchPending,false);
           // See if a meta-instruction is fetched
    if(mGridPoint->IsMorphingInstructionFetched(this))
           {
               // It is a morphing instruction for the core, just call it with the subtype
               // This condition is noticed in the prefetch cycle
       //        fetch.Stage.CoreStatus = STAT_META;
/*               msMeta.CA = ClusterAddress_Get();
               msMeta.PC = fetchPC_Get();
               msMeta.Code = fetch.Stage.FunctCode;
               msMeta.Address = fetch.Stage.immediate;
               msMeta.Param = fetch.Stage.byte1;
               msMeta.ExecutionTime = fetch.ExecutionTime;
*/           }
           exec.Stage = fetch.Stage; // Take over the fetched stage
           exec.PC = fetch.PC;     // The last fetched intruction will be executed
           fetch.PC = fetch.NewPC; // The next instruction will be fetched
}

#if 0
void scHThread::FETCH_thread(void)
{
    while(true)
    {
        DEBUG_FETCH_EVENT("WAIT EVENT_HTHREAD.FETCH");
        wait(EVENT_HTHREAD.FETCH);
        DEBUG_FETCH_EVENT("RCVD EVENT_HTHREAD.FETCH");
        // As we have a memory buffer per thread, look first if we have the content already.
//        fetch.InstrBufferPosition = InstructionPositionInBuffer(fetch.PC);
        if(InstPositionInBuffer(fetch.PC))
        {   // Yes, we do have the content in our cache; the content is already @fetch.InstrBufferPosition

        }
        else
        {   // No luck, must fetch it
            // See first, if the HW unit is busy in fetching
            if(scGridPoint_Get()->OperatingBit_Get(gob_FetchPending))
            {   // Hm, some other thread uses the shared HW, must wait.
                wait(scGridPoint_Get()->EVENT_GRID.FetchReady);
                // It looks like our scGridPoint is free; retry
                EVENT_HTHREAD.FETCH.notify();
            }
            else
            {   // Great, we have a scGridpoint:
                scGridPoint_Get()->OperatingBit_Set(gob_FetchPending,true); // OK, we are using it
                scGridPoint_Get()->HThreadFetch_Set(this);   // Now it is our core
            // Take care: we need a separated HTread ID for fetching!!!
//            scGridPoint_Get()->Use the fetch thread, maybe no event needed?
            // Wait scGridPoint event
                scGridPoint_Get()->OperatingBit_Set(gob_FetchPending,false);    // The fetch is over, release the core
            }

        }
         // Anyhow, now we have the requested content
        EVENT_HTHREAD.FETCHED.notify();
    } // while
} // FETCH_thread
#endif //0

/*!
     * \brief Return pointer to the buffer containing next instruction
     * \param A address of next instruction, defaults to fetch.PC
     * \return pointer or NULL if not in the buffer
     */
    SC_WORD_TYPE* scHThread::
InstPositionInBuffer(SC_ADDRESS_TYPE A)
{
    if(CORE_DEFAULT_ADDRESS == A) A = fetch.PC;
    assert(!(A&3)); // Must be double-word aligned
    if(
            (CORE_DEFAULT_ADDRESS == mInstrMemoryAddressBegin)  // The buffer is empty
          || (A >= mInstrMemoryAddressEnd)
      )
        fetch.InstBufferPosition = (SC_WORD_TYPE*) NULL;
    else
        fetch.InstBufferPosition = &(mDataMemoryBuffer[A/4]);   // return the requested address
     return fetch.InstBufferPosition;
}

    /*!
         * \brief Return pointer to the buffer containing next data
         * \param A address of next data
         * \return pointer or NULL if not in the buffer
         */
    SC_WORD_TYPE* scHThread::
DataPositionInBuffer(SC_ADDRESS_TYPE A)
{
    assert(!(A&3)); // Must be double-word aligned
    SC_WORD_TYPE* Data;
    if(
            (CORE_DEFAULT_ADDRESS == mDataMemoryAddressBegin)  // The buffer is empty
         ||  (A >= mDataMemoryAddressEnd)
      )
        Data = (SC_WORD_TYPE*) NULL;
    else
        Data = &(mDataMemoryBuffer[A/4]);   // return the requested address
    return Data;
}
//??        DEBUG_PRINT_WITH_SOURCE("Begin fetching line ");
/*        fetch.ExecutionTime = sc_time_stamp();  // Remember the beginning of prefetching
        fetch.Pending = true; // Prefetching will be started
        fetch.Valid = false;  // No valid prefetched instruction
        LOG_TRACE_FETCH_SOURCE("Fetching instruction");
    //?? The instruction is fetched to avoid timing in the doXXX routine
        if(!doFetchInstruction())
        {
            LOG_CRITICAL("Instruction fetch failed; wrong code");
            break; // Illegal code fetched; fatal
        }
        wait(SCTIME_CLOCKTIME);    // Imitate decoding
        PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);
    // The fetch time is included in readInstrMem
    // Flags are cleared in PrepareNextInstruction
        fetch.Pending = false; // Prefetching finished
        fetch.Valid = true;  // This core has valid prefetched instruction
//!!        Plotter_Get()->PlotQTfetch(ID_Get(),mFetchDelayTime,FetchPC, FetchStage.ExecutionTime.value()); // Plot fetch mark
        EVENT_CORE.FETCHEXECUTED.notify(SC_ZERO_TIME);
        DEBUG_FETCH_EVENT("SENT FETCHEXECUTED_event");
        */


/*!
 * \brief The memory content arrived to the owner scGridPoint
 *
 * This method is called when EVENT_THREAD.MemoryContentArrived.notify()
 * signals that the requested content is available in the scGridPoint
 * The request must store the memory
 *
 * @see scHThread#MemoryRequestAddress
 */
void scHThread::
MemoryContentArrived_method(void)
{
}

 /*!
     * \brief Return the string-form ID of the thread
     * \return the string as
     * The string ID of the thread is appended to string of its parent
     * The string comprises (string forms of) scGrid ID + "." + scHThread ID (+ "_" + Instance count, if any)
     */
    string scHThread::
StringOfID_Get(void)
{
    string ID; scHThread* MyParent = Parent_Get(); scGridPoint* MyOwner = Owner_Get();
    if(MyParent)
    {   // We have a parent thread
        if(MyParent->Parent_Get())
            ID = MyParent->StringOfID_Get();
        else
        {   // Check if we are at the processor
            ID = ID.append(to_string(MyParent->ID_Get()));
            int MyH = MyParent->Child_Find(this);
            if(-1 == MyH)
            {    // We did not find the child
                ID = ID.append("?").append(to_string(mChildren.size()));
            }
            else
            {    // we did find the child
                ID =  ID.append(".").append(to_string(MyH));
            }
        }
    }
    else
    {   // The processor is our parent
        ID = MyOwner->Processor_Get()->StringOfID_Get(this);
    }
    // Now the prolog is ready, add our own part
    ID = ID.append(":").append(to_string(MyOwner->ID_Get()));   // Add the scGridpoint number
    // Neglect uninitialized and single-child parents
    if(MyOwner->InstanceCount_Get()>1) ID = ID.append("_").append(to_string(MyOwner->InstanceCount_Get()-1));
    ID = ID.append(".").append(to_string(ID_Get()));
    if(InstanceCount_Get()>1) ID = ID.append("_").append(to_string(InstanceCount_Get()-1));
    return ID;
}
