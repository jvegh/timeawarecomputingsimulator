/** @file Stuff.h
 *  @brief Function prototypes for the EMPA simulator, processor.
 *
 *  This contains the prototypes for the EMPA simulator, processor.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef EMPA_STUFF_H
#define EMPA_STUFF_H
//#include "Config.h"
//#include "Processor.h"
//#include <glog/logging.h>
#include <string>
extern bool GUI_MODE;	// This tells whether routine run in GUI mode
using namespace std;


/*!
   * \brief CheckArgumentList
   * Check the parameters of the starting command line
   * \param[in] S String at the beginning
   * \param argc Number of command line parameters as in main()
   * \param argv list of char* of the parameters as in main()
   * \returns the resulting message
   */
string
CheckArgumentList(string S, int argc, char** argv);


void
OpenSystemFiles(const char* FileName, const char* LogFileName, string Heading);

void CloseSystemFiles(string Trailing);

/**
 *  @brief  Assemble the root part of the filename used for different goals
 *
 *  @param[in]  DataFileName   Name of the simulator command file
*/
  string
GetFileNameRoot(string DataFileName="");

/**
 *  @brief  Return application name and version
 */
  std::string
GetAppName(void);

/*
 *  @brief  Return application name and version
 * /
  std::string
GetAppVersion(void);
*/

bool FinishProgram(void);

#endif //  EMPA_STUFF_H
