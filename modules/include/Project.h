// The configured project name

// Create file 'Project.h' file in the BASIC subdirectory
#define PROJECT_VERSION_MAJOR 0
#define PROJECT_VERSION_MINOR 0
#define PROJECT_VERSION_PATCH 4
#define PROJECT_VERSION "0.0.4"
#undef  PROJECT_NAME
#define PROJECT_NAME "ScQtTimeAware"
#define PACKAGE_NAME "scqtTimeAware"
#define ScQtTimeAware_VERSION_MAJOR 0
#define ScQtTimeAware_VERSION_MINOR 0
#define ScQtTimeAware_VERSION_PATCH 4
#define ScQtTimeAware_VERSION "0.0.4"
