/** @file Neurer.cpp
 *  @brief Function prototypes for the SystemC based neuron simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

#include "Neurer.h"
#include "NeurerProcessor.h"

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
//#include "scGridPoint.h"
//extern Memory *MainMemory;
//extern NamedEnum_struct ClusterMembers[8];

extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
/*!
 * \brief Neurer::Neurer
 *
 * This class implements the general neurer functionality, closer to the electronics
 * \param[in] nm The SystemC name of the Neurer
 * \param[in] Topology the topology we belong to (the NeurerProcessor)
 * \param[in] GP the topological location of the new neurer point
 * \param[in] ID The unique ID of the neurer
 * \param[in] StandAlone if the object is to be created as stand-alone
 *
 */
// \param StandAlone standalone event handling shall be implemented

Neurer::Neurer(sc_core::sc_module_name nm
               , NeurerProcessor* Topology
               , scGridPoint* GP // The topographic position
             , SC_HTHREAD_ID_TYPE ID
              , bool StandAlone)
       :AbstractNeurer(nm, //Topology,
                       GP, ID, false)
{
}// of scGridPoint::scGridPoint

void Neurer::
Reset(void)
{
    AbstractNeurer::Reset();
}
Neurer::~Neurer()
{
}//Neurer::~Neurer

NeurerProcessor* Neurer::
Processor_Get(void) { return  dynamic_cast<NeurerProcessor*>(AbstractNeurer::Processor_Get());}	///< Return the parent scGridPoint of the scGridPoint

