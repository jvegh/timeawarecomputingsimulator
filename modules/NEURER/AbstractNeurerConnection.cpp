/** @file AbstractNeurerConnection.cpp
 *  @brief Function prototypes for the SystemC based neuron simulator
 */
 /*  Provides connection for AbstractNeurer
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "AbstractNeurerConnection.h"
#include "AbstractNeurer.h"
#include "Spike.h"
/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"

    scSynapticConnect::
scSynapticConnect(sc_core::sc_module_name nm, AbstractNeurer* Sender, AbstractNeurer* Receiver
                  ):
         sc_core::sc_module(nm),
     mSender(Sender)
    ,mWeight(THOUSAND)
    ,mReceiver(Receiver)
    ,mSpike(nullptr)
    {
        SC_METHOD(Integrate_method);
            sensitive << EVENT_SYNAPSIS.Integrate;
        dont_initialize();  // Do not integrate at the beginning
       SC_METHOD(Fire);
       sensitive << EVENT_SYNAPSIS.Fire;
       dont_initialize();   // Do not fire at the beginning
    }
    scSynapticConnect::
~scSynapticConnect(){}

    // This method is called when the next change integration period follows
    void scSynapticConnect::
Integrate_method(void)
{
    int16_t T = (mSpike->Time_Get(mSpikeIndex)*mTimeFactor)/THOUSAND; // usec
    // In units of 1e-6 sec // usec
    int16_t ThisTime = T - mLastTime; // Our current time slot, usec
    int16_t A = (mSpike->Amplitude_Get(mSpikeIndex++)*mAplitudeFactor)/THOUSAND; //The new synaptic current, pA
    // In units of NEURON_CURRENT_UNIT 1e-12 A // pA
    int16_t ThisCurrent = (A+mLastCurrent)/2; ///< The average current for the time slot, pA
    if(ThisCurrent > SYNAPTIC_CURRENT_CRITICAL)
    {
        DEBUG_EVENT_OBJECT("Saltatoric Fire");
        Receiver_Get()->Fire();   // A saltatoric discharge
        return;
    }
    // Else, we are continuing current integration
    // in units of us*pA = 1e-18 Coulomb
    int64_t ThisCharge = (ThisTime * ThisCurrent * mWeight)/THOUSAND; ///< The new charge contribution
    Sender_Get()->AddChargeContribution(ThisCharge);
    if(T)
         EVENT_SYNAPSIS.Integrate.notify(ThisTime*sc_time(1,SC_US));
    mLastTime = T;
    mLastCurrent = A;
    DEBUG_PRINT_OBJECT("Collecting at (" << mSpikeTime << "," << mSpikeAmplitude);
}
    AbstractNeurer* scSynapticConnect::
Receiver_Get(void){ return mReceiver;}
    AbstractNeurer* scSynapticConnect::
Sender_Get(void){ return mSender;}
    int16_t scSynapticConnect::
Weight_Get(void){ return mWeight;}
    // The unit uf factors is THOUSAND
    void scSynapticConnect::
ProcessSpike( Spike& MySpike, uint16_t MyTimeFactor, uint16_t MyAplitudeFactor)
{
    mSpike = &MySpike;
    mSpikeIndex = 0;    // Go to the beginning of the spike
    mTimeFactor = MyTimeFactor;
    mAplitudeFactor = MyAplitudeFactor;
    // We are calculating with usec and nA units
    mLastTime = 0;
    // Assume we are starting with the first current at zero time
    mLastCurrent = (mSpike->Amplitude_Get(mSpikeIndex)*mAplitudeFactor)/THOUSAND; //The new synaptic current, pA
    uint16_t ThisTime = (mSpike->Time_Get(mSpikeIndex)*mTimeFactor)/THOUSAND;
    EVENT_SYNAPSIS.Integrate.notify(ThisTime*sc_time(1,SC_US)); // Take the first time
}

    /*!
     * \brief scSynapticConnect::Fire
     *
     * Called from the owner's (gridpoint) 'Fire' method.
     * As it sends through its scGridPoint (the owner of the owner scHThread),
     * if must receive the ownership of the scGridPoint for the time of sending
     */
    void scSynapticConnect::
Fire(void)
{
    scGridPoint* GP = Sender_Get()->Owner_Get();
    assert(GP);  // Be sure that we have an scGridPoint
    AbstractNeurer* R = Receiver_Get();
    // A request to send the next spike is received: check availability of our scridPoint
    if(GP->OperatingBit_Get(gob_IsSending))  // Ask if Gridpoint's Send is available
    {// No, it is sending
        if((scSynapticConnect*)GP->HThreadSend_Get() == this)
       {   // it was me who locked the GP
           DEBUG_EVENT_OBJECT("EVENT_GRID.SendReady received, sending finished") ;
           GP->OperatingBit_Set(gob_IsSending, false); // Now the scGridPoint is free
           next_trigger(EVENT_SYNAPSIS.Fire);  // This fetch gone, restore trigger
           GP->EVENT_GRID.SendReady.notify();
       }
        else
        {
            // Sorry, we must wait: some other thread is sending
            // Temporarily change trigger to GP->FetchReady
            DEBUG_EVENT_OBJECT("SENT EVENT_GRID.MakeFetch, again") ;
            GP->EVENT_GRID.MakeSend.notify(SC_ZERO_TIME);  // Repeat request
            next_trigger(GP->EVENT_GRID.SendReady);    // Wait until owner scGridPoint is free again
            DEBUG_EVENT_OBJECT("WAIT >EVENT_GRID.SendReady") ;
        }
    }
    else
    {   // OK, the SEND unit is available
        GP->OperatingBit_Set(gob_IsSending, true); //  make our scGridPoint busy
        GP->HThreadSend_Set((scHThread*)this);  // Tell who keeps GP busy
        // Just to survive debugging!!!
        DEBUG_ONLY(GP->HThreadExec_Set(this->Sender_Get());)  // Tell who keeps GP busy
        DEBUG_EVENT_OBJECT("EVENT_GRID.FETCH started " ) ;
        GP->EVENT_GRID.MakeSend.notify(SC_ZERO_TIME);
        next_trigger(GP->EVENT_GRID.SendReady);    // Wait until owner scGridPoint is free again
    }
/*    if(GP->OperatingBit_Get(gob_FetchPending))  // Ask if Gridpoint's Fetch is available
    { // The fetch is going on, check if we see out own ownership
         if(GP->HThreadFetch_Get()->ID_Get() == ID_Get())
        {   // it was me who locked the GP
            DEBUG_EVENT_OBJECT("EVENT_GRID.FetchReady received, fetch finished") ;
            GP->OperatingBit_Set(gob_FetchPending, false); // Now the scGridPoint is free
            OperatingBit_Set(tob_FetchValid,true);
            OperatingBit_Set(tob_FetchPending,false);
            next_trigger(EVENT_HTHREAD.FETCH);  // This fetch gone, restore trigger
            EVENT_HTHREAD.FETCHED.notify();    // Now the fetched instruction is avaialable
        }
        else
        {
            // Sorry, we must wait: some other thread is fetching
            // Temporarily change trigger to GP->FetchReady
            DEBUG_EVENT_OBJECT("SENT EVENT_GRID.MakeFetch, again") ;
            GP->EVENT_GRID.MakeFetch.notify(SC_ZERO_TIME);  // Repeat request
            next_trigger(GP->EVENT_GRID.FetchReady);    // Wait until owner scGridPoint is free again
            DEBUG_EVENT_OBJECT("WAIT >EVENT_GRID.FetchReady") ;
        }
    }
    else
    {   // OK, the FETCH unit is available
        GP->OperatingBit_Set(gob_FetchPending, true); //  make our scGridPoint busy
        GP->HThreadFetch_Set(this);  // Tell who keeps GP busy
        // Just to survive debugging!!!
        GP->HThreadExec_Set(this);  // Tell who keeps GP busy
        DEBUG_EVENT_OBJECT("EVENT_GRID.FETCH started " ) ;
        GP->EVENT_GRID.MakeFetch.notify(SC_ZERO_TIME);
        next_trigger(GP->EVENT_GRID.FetchReady);    // Wait until owner scGridPoint is free again
    }
*/
    }

    string scSynapticConnect::
PrologString_Get(void)
{
   return "MySynapsis";
}

    scAxonalConnect::
scAxonalConnect(AbstractNeurer* Receiver ):
                mReceiver(Receiver)
{}

            scAxonalConnect::
~scAxonalConnect()
{}
    AbstractNeurer* scAxonalConnect::
Receiver_Get() {return mReceiver;}

    string scAxonalConnect::
PrologString_Get(void)
{
       return "MyAxon";
}
