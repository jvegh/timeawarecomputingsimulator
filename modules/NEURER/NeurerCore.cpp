/** @file NeurerCore.cpp
 *  @brief Function prototypes for the SystemC based neuron simulator
 *  Provides base for cores for neurers
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

#include "NeurerCore.h"
#include "NeurerProcessor.h"
#include "AbstractNeurer.h"

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
//#include <vector>
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];

NeurerCore::NeurerCore(sc_core::sc_module_name nm,
                   NeurerProcessor* Proc
                   , const GridPoint GP
                   , bool StandAlone
                   ):    AbstractCore(nm, Proc, GP, false)
{
/*
    SC_THREAD(EXECUTEMETA_thread)
        sensitive << EVENT_CORE.EXECUTEMETA;
    SC_THREAD(QHALT_thread);
       sensitive << EVENT_CORE.QHALT;
    SC_THREAD(QCREATE_thread);
       sensitive << EVENT_CORE.QCREATE;
    SC_THREAD(QTERM_thread);
        sensitive << EVENT_CORE.QTERM;
    SC_THREAD(QWAIT_thread);
        sensitive << EVENT_CORE.QWAIT;
    SC_THREAD(QWAITMETA_thread);
        sensitive << EVENT_CORE.QWAITMETA;
    SC_THREAD(QCREATEMETA_thread);
        sensitive << EVENT_CORE.QCREATEMETA;
    SC_THREAD(NEXT_thread);
        sensitive << EVENT_CORE.NEXT;
    SC_THREAD(FETCH_thread);
        sensitive << EVENT_CORE.FETCH;
    SC_THREAD(QCALL_thread);
        sensitive << EVENT_CORE.QCALL;
    SC_THREAD(QMUTEX_thread);
        sensitive << EVENT_CORE.QMUTEX;
    SC_THREAD(QALLOC_thread);
        sensitive << EVENT_CORE.QALLOC;
*/    if(StandAlone)
    {
        Reset();    // All core attributes, maybe per thread
        // Now re-create its scHThreads
    }
  //The neurers  have their own subclasses scHThreads, so they must be re-created
  for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
  {
      assert(mHThreads[H]); // It was not null: wrong
      AbstractNeurer *N = new AbstractNeurer(string("N").append(IDtoString(H,HTHREAD_BUS_WIDTH)).c_str(),this, H);
      assert(N); // Unsuccessful generation
      AddHThread(H, N); // Now strore the new, derived scHThread here
      Processor_Get()->Alias_Add(N->StringOfDefaultName_Get().append("/0"),N);
      Processor_Get()->Alias_Add(N->StringOfClusterAddress_Get().append("/0"),N);
  }
//  AddDefaultAliasNames(); // and recreate with new ones
  // The alias map is full with old data

}// of NeurerCore::NeurerCore

    void NeurerCore::
Reset(void)
{
    AbstractCore::Reset();
/*#ifdef MEASURE_PERFORMANCE
    mInstructionCount = 0;
    mMetaInstructionCount= 0;
    mInstructionTime = sc_time(0,SC_NS);
    mMetaInstructionTime = sc_time(0,SC_NS);
    mMemoryTime = sc_time(0,SC_NS);
    mFetchTime = sc_time(0,SC_NS);
    mWaitTime = sc_time(0,SC_NS);
#endif //MEASURE_PERFORMANCE
*/
}
    NeurerCore::
~NeurerCore()
{
}//AbstractCore::~AbstractCore

    NeurerProcessor* NeurerCore::
Processor_Get(void)
{        return dynamic_cast<NeurerProcessor*>(AbstractCore::Processor_Get());}
