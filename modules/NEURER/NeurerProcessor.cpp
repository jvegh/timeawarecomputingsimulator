/** @file NeurerProcessor.cpp
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "NeurerProcessor.h"
//#include "Neurer.h"
//#include "scIGPCB.h"
//#include "scIGPMessage.h"
//#include "Utils.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "AbstractNeurerConnection.h"
#include "NeurerCore.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
extern Spike ClockSpike;

/*!
 * \brief NeurerProcessor::NeurerProcessor
 * \param nm SystemC name
 * \param Specials the list of special neurons
 * \param StandAlone true if some part shall be executed here rather than in the subclass

 @verbatim
*  |AbstractTopology
*  |--/scProcessor
*  |..../AbstractProcessor
*  |....../XXProcessor
*  |....../NeurerProcessor
* @endverbatim
*/
     NeurerProcessor::
NeurerProcessor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone)
             : AbstractProcessor(nm, Specials, false)
{
    if(StandAlone)
    {
        SC_METHOD(Initialize_method);
        SC_THREAD(START_thread);
             sensitive << EVENT_PROCESSOR.START;
        SC_METHOD(FrequencyBasic_method);
        SC_METHOD(FrequencyAlpha_method);
        Populate(Specials); // Populate with abstract cores
        CreateClusters();      // Clusterize cores as gridpoints
        ConnectIGPCBs();       // Connect their communication lines
        ConnectClusterHeadsToBus(mClusterBus);  // Connect ONLY cluster heads to inter-cluster bus
        Reset();
        AliasesClear();   // Clear it
    }
}
     /*     MetaEvents = new  MetaEvent_fifo("SV_MetaFIFO");
          // Triggered by writing into the MetaEvent_fifo
          SC_THREAD(Execute_thread);
            sensitive << MetaEvents->write_event;
     */

 /*
     // OK, now can experiment with
         Neurer* N1 = Neurer_Get(2,4);

         Neurer* N2 = Neurer_Get(4,4);
         Neurer* N3 = Neurer_Get(3,4);

        N1->InsertConnectionFrom(GridPoint(2,4).ClusterAddress_Get());
        N2->InsertConnectionFrom(N3);
        N2->InsertConnectionFrom(N2);
        N3->InsertConnectionFrom(N1);
        int i = 0;
  //       for(int i=0; i<3; i++)
         {
             int32_t MyInt = N1->SynapticConnect_Get(i);
             std::cerr << hex << MyInt <<  " " << endl;
             MyInt = N2->SynapticConnect_Get(i);
             std::cerr << hex << MyInt <<  " " << endl;
             MyInt = N3->SynapticConnect_Get(i);
             std::cerr << hex << MyInt <<  dec << endl;
         }
 */

   NeurerProcessor::
~NeurerProcessor(void)
{
}
    void NeurerProcessor::
Reset(void)
  {
      AbstractProcessor::Reset();
  }

    /*!
     * This method starts up when everything is initialized
     */
    void NeurerProcessor::
Initialize_method(void)
    {
    //    wait(SCTIME_SV); // For setup
        DEBUG_PRINT_OBJECT("Initialize_method started" );
    //    mscoreStatus = STAT_AOK; // Assume everything all-right
        // We surely have no processing unit and also the first instruction must be fetched
        Processor_Get()->EVENT_PROCESSOR.START.notify(SC_ZERO_TIME);
    }
    /*!
     * \brief This thread is waiting for a START_event and processes it
     * It should be reimplemented in the derived classes
     */
    void NeurerProcessor::
START_thread(void)
    {
        while(true)
        {
             DEBUG_EVENT_OBJECT("WAIT  EVENT_PROCESSOR.START");
             wait(EVENT_PROCESSOR.START);
             DEBUG_EVENT_OBJECT("RCVD  EVENT_PROCESSOR.START");
             msHalted = false;
             AbstractNeurer *N1 = ByID_Get(2,1);
             AbstractNeurer *N2 = ByID_Get(3,2);
             AbstractNeurer *N3 = ByID_Get(4,3);
             // The followings are done in  NeurerProcessor::CreateConnections
         //    N1->ConnectTo(N2);  // Connect the axon of N1 to a synaptic point of N2
         //    N1->ConnectTo(N3);  // Connect the axon of N1 to a synaptic point of N3
         //    N2->ConnectTo(N3);  // Connect the axon of N2 to a synaptic point of N3
             DEBUG_PRINT_OBJECT(N1->AxonsNo_Get()<< " axons");
             DEBUG_PRINT_OBJECT(N1->SynapsesNo_Get() << " Synapses");
             DEBUG_PRINT_OBJECT(N3->SynapsesNo_Get() << " Synapses");
             scSynapticConnect* i1 = N3->FindSynapseFrom(N1);    // Has from N1
/*             EXPECT_EQ((uint64_t)i1->Sender_Get(),(uint64_t)N1);
             EXPECT_DEATH(N3->SynapticConnection_Get(5),""); // Wrong index
             EXPECT_EQ(N3->SynapticConnection_Get(0)->Sender_Get(), N1);
             EXPECT_EQ(N3->SynapticConnection_Get(1)->Sender_Get(), N2);
             EXPECT_DEATH(N1->SynapticConnection_Get(5),""); // Wrong index
             EXPECT_EQ(N1->AxonalConnection_Get(0)->Receiver_Get(), N2);
             EXPECT_EQ(N1->AxonalConnection_Get(1)->Receiver_Get(), N3);
*/             sc_core::sc_time TT = sc_time_stamp();
             i1->ProcessSpike(ClockSpike);
              wait(i1->EVENT_SYNAPSIS.Integrate);
              DEBUG_PRINT_OBJECT(N3->RunningCharge_Get());
             TT = sc_time_stamp()-TT;
             wait(i1->EVENT_SYNAPSIS.Integrate);
             DEBUG_PRINT_OBJECT(N3->RunningCharge_Get());
            TT = sc_time_stamp()-TT;
            wait(i1->EVENT_SYNAPSIS.Integrate);
            DEBUG_PRINT_OBJECT(N3->RunningCharge_Get());
           TT = sc_time_stamp()-TT;
/*             //    wait(1,SC_NS);
             // Temporary, used to test FETCH events
             scProcessor *Proc = Processor_Get();
             scGridPoint *GP10 = Proc->ByID_Get(10);
             scGridPoint *GP11 = Proc->ByID_Get(11);
             scHThread *HT103 = GP10->HThread_Get(3);
             scHThread *HT105 = GP10->HThread_Get(5);
             scHThread *HT115 = GP11->HThread_Get(5);
             //        wait(1,SC_NS);  // Enable initialization happen
             // These HThreads ask for a gridpoint at the same time.
             // Only one of HT103 or HT105 will get through
             HT103->EVENT_HTHREAD.FETCH.notify();
             wait(10,SC_PS);
             HT105->EVENT_HTHREAD.FETCH.notify();
             HT115->EVENT_HTHREAD.FETCH.notify();
             wait(10,SC_PS);
             bool HT103Busy = HT103->OperatingBit_Get(tob_FetchPending);
             bool HT105Busy = HT105->OperatingBit_Get(tob_FetchPending);
             bool HT115Busy = HT115->OperatingBit_Get(tob_FetchPending);
*/        }
    }

  void NeurerProcessor::
Populate(vector<scGridPoint*>& Specials)
{
    // Create communication grid point objects
    NeurerCore* C;
    for(int i = 0; i < GRID_SIZE_X; i++, i++) // This needed because of the hexagonal grid
    {
        for(int j = 0; j < GRID_SIZE_Y; j++)
        {
            int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
            C = new NeurerCore(sc_core::sc_module_name(string("N").append(IDtoString(LinearAddress1,GRID_BUS_WIDTH)).c_str()),
                    this, 
                           //LinearAddress1, 
                    GridPoint(i,j), //0,
                                 false
            );
            // No HThreads are created, now attach AbstractNeuron as HThread
//            CreateNeurons(C);

            mGrid.at(i).at(j) = C; // Store the pointer in the grid
            int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
            C = new NeurerCore(sc_core::sc_module_name(string("N").append(IDtoString(LinearAddress3,GRID_BUS_WIDTH)).c_str()),
                    this,
                    //LinearAddress3, 
                    GridPoint(i+1,j), //0,
                                 false
            );
//           CreateNeurons(C);
           mGrid.at(i+1).at(j) = C; // Store the pointer in the shadow-column
        }
    }
}

    void NeurerProcessor::
CreateConnections(void)
{
        AbstractNeurer *N1 = ByID_Get(2,1);
        AbstractNeurer *N2 = ByID_Get(3,2);
        AbstractNeurer *N3 = ByID_Get(4,3);
        N1->ConnectTo(N2);  // Connect the axon of N1 to a synaptic point of N2
        N1->ConnectTo(N3);  // Connect the axon of N1 to a synaptic point of N3
        N2->ConnectTo(N3);  // Connect the axon of N2 to a synaptic point of N3
}


// Provide base frequencies for the neural operation
    void NeurerProcessor::
FrequencyBasic_method(void){    next_trigger(100,SC_MS); EVENT_NEURERPROCESSOR.BASIC.notify();}
  void NeurerProcessor::
FrequencyAlpha_method(void){    next_trigger(33333,SC_US);  EVENT_NEURERPROCESSOR.ALPHA.notify();}
