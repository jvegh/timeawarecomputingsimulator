/** @file AbstractNeurer.h
 *  @ingroup TAC_MODULE_NEURER
*/
/**  @brief Function prototypes for the neural simulator, and abstract neurer.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef AbstractNeurer_h
#define AbstractNeurer_h
/** @addtogroup TAC_MODULE_NEURER
 *  The neural-related classes and functionality
 *  @{
 */


#include "Project.h"
#include "scGridPoint.h"
#include "NeurerConfig.h"

using namespace sc_core; using namespace std;
//#include "AbstractNeurerConnection.h"
class scSynapticConnect; class scAxonalConnect; class ActionPotential;

typedef vector <scSynapticConnect*>  scSynapticConnects_t;
typedef vector <scAxonalConnect*>  scAxonalConnects_t;

class NeurerProcessor;
class Spike;
class QSettings;

/*! \var typedef NeurerOperatingBit_t
 * \brief the names of the bits in the bitset for operating the neurer
 */
typedef enum
{
    nsb_Resting,    //!< Set if neurer is resting
    nsb_Triggering, //!< Set if neurer is attempting to reach membrane's threshold
    nsb_Charging,   //!< Set if neurer is charging up its membrane to prepare firing
    nsb_Discharging, //! Set if neurer' membrane is in course of discharging
    nsb_Firing, //! Set if neurer' membrane is in course of firing
    nsb_Max, //!< The max number of bits in the set
}  NeurerOperatingBit_t;


/*!
 * The base class is the scHThread, i.e. HW thread functionalityit has its associated inter-gridpoint communication facilities
 * and communicates autonomously with its immediate neighbors and cluster members

 * The neurers are kentaurs:
 *  half-part cores, with non-specialized core-functionality
 *  and half-part neuron-specific functionality.
 *
 * This class provides the base for Neurer, the artificial neuron
 * \param [nm] the name of the abstract neurer
 * \param[in] Processor the NeurerProcessor we belong to
 * \param[in] GP the owner scGridPoint the scHThread belongs to
 * \param[in] ID The unique (within the scGridPoint) ID of the thread
 * \param[in] if stand-alone working required
 *
 * @verbatim
 *  |scHThread
 *  |--/AbstractNeurer
 *  |..../Neurer
 * @endverbatim
 */

class AbstractNeurer :  public scHThread
{
    friend class scSynapticConnect;
//    friend class AbstractNeurerState;
  public:
    // Constructor declaration:
    AbstractNeurer(const sc_core::sc_module_name nm, // Just the systemC name
//                    NeurerProcessor* Processor, // The present system is prepared for one topology only, but ...
                   scGridPoint* GP, SC_HTHREAD_ID_TYPE ID,
              bool StandAlone = true
            );
    ~AbstractNeurer(void);
        SC_HAS_PROCESS(AbstractNeurer);  // We have the constructor in the .cpp file
      // Directly HW-related functionality
        void
    Initialize_method(void);
 /*       void
     CreateNeurons(void);*/
        void
    Reset(void);
        bool
    IsMetaInstructionFetched(void)
    {assert(0);//return fetch.Stage.hi0 ==  (itype_t) I_QT;
            return false;
        }
        bool
    doFetchInstruction(void//SC_WORD_TYPE Instr
                         ) // The actual fetching; does not generate an event
{assert(0); return false;
    }
        bool
    doExecuteInstruction(void){assert(0); return false;}
        void
    doExecuteMetaInstruction()
        {assert(0);
            }
        NeurerProcessor*
    Processor_Get(void);

        void
    doQTERM(void)
       {assert(0);  }
       bool
   IsMetaInstructionReceived(void)
       {assert(0);  return false;}
        string
    StringOfName_Get(void){   return Owner_Get()->StringOfName_Get(ID_Get());}
        string
    PrologString_Get(void);
        scGridPoint*
    Parent_Get(void) { return  dynamic_cast<scGridPoint*>(scHThread::Parent_Get());}	//!< Return the parent scGridPoint of the scHThread
        scAxonalConnect*
    AxonalConnection_Get(unsigned int i)
    {   assert(i<m_Axons.size());
        return m_Axons[i];
    }
        scSynapticConnect*
    SynapticConnection_Get(unsigned int i)
    {   assert(i<m_Synapses.size());
        return m_Synapses[i];
    }
        void
    ConnectTo(AbstractNeurer* A);
        void
    ConnectFrom(scSynapticConnect* A);
        scSynapticConnect*
    FindSynapseFrom(AbstractNeurer* A);  //!< Return synaptic connect corresponding from A, or NULL
        scAxonalConnect*
    FindAxonTo(AbstractNeurer* A); // Return axonal connect to A, or NULL

       struct{
        sc_core::sc_event
            ActionPotential,
            FireBegin,           /// Received when the peak voltage of membrane received
            RefractoryEnd,      /// Received when the membrane potential drops below its triggering threshold
            Reset       /// Puts the neurer in its initial state
            ,Trigger /// Puts the neurer in synaptic-aware more
            ,Charge  /// Accumulates charge on the membrane, synaptic-unaware
            ,Discharge /// Sends the spike to the axon
            , Leaking        /// The neuron permanently loses its charge, periodically decreasing the charge
            ,Spiking        /// The neuron takes the next current value and processes it
            ,Recalculate    /// In the case of prospective change of behavior, the new potential is calculated
         ;
        }EVENT_NEURER;  //!< These events are handled at abstract processor level
        void
    Integrate_method(void);
        void    //!< Set the length of the period after which the potential is decreased by one
    LeakingPeriod_Set(short unsigned int P);
        short unsigned int
    LeakingPeriod_Get(void){ return mLeakingPeriod;}
        void
    SynchronFrequency_Set(sc_core::sc_event* F){ mSynchronFrequency = F;
                                                 mSynchronFrequency->notify();
                                               }
        /*!
         * \brief Reset neurons to their initial state
         */
        void
    Reset_method(void);       //Puts the neurer in its initial state
//        void    //!< handles action potential
//    ActionPotential_method(void);
        /*!
         * This method is activated when an ActionPotential sends EVENT_NEURER.FireBegin.
         * The neuron starts its firing
         */
        void
    FireBegin_method(void);
        /*!
         * This method is activated when an ActionPotential sends EVENT_NEURER.RefractoryEnd.
         * The neuron finishes its refractory period
         */
        void
    RefractoryEnd_method(void);
        void
    Trigger_method(void);  // Puts the neurer in synaptic-aware more
        void
    Charge_method(void);  // Accumulates charge on the membrane, synaptic-unaware
        void
    Discharge_method(void); // Sends the spike to the axon
        /*!
         * \brief IntegratedCurrentInPeriod
         * A simple trapezoidal area (for short period, where the function is nearly linear)
         * \param FromTime  The beginning of the time period, in usec
         * \param ToTime    The end of the time period, in usec
         * \param FromValue The value of current at the beginning of the period, in nA
         * \param ToValue   The value of current at the end of the period, in nA
         * \return the total charge, in nA*usec == 10^{-15}C (i.e. A*sec), usable with IncreaseActionPotentialWithCharge
         */
        int
    IntegratedCurrentInPeriod(int FromTime, int ToTime,
                                  int FromValue, int ToValue)
        {   assert(FromTime<ToTime);
            int Average = (FromValue + ToValue)/2;
            float TimeDiff = ToTime - FromTime;
            return TimeDiff*Average;
        }
        /*!
         * \brief NeurerCondensatorCapacity_Get
         * \return the assumed capacity, in pF
         *
         * It is assumed that the capacity of a neuronal condensator is 1uF/cm^2 == 10^{-2}pF/u^2
         * and the area is proportional with the number of synapses, and one synapsis if 10 u^2
         */
        int
        NeurerCondensatorCapacity_Get(void)
        {
            return mNoOfSynapticPoints// The number of synaptic points
                    * 10 // Synaptic area, u^2
                    * .01 // The capacity of a single synapsis, a neuronal condensator
                    * THOUSAND; //The result is in  pF
        }
        /*!
          * \brief IncreaseActionPotentialWithCharge
          * \param C the charge as calculated by IntegratedCurrentInPeriod 10^{-12}C
          *
          */
         void
    IncreaseActionPotentialWithCharge(int C)
        {
            mRunningCharge += C
  //                   /NeurerCondensatorCapacity_Get()
                    ;    // Add the actual contribution
             // it is in uV*usec == 10^{-12}F == 10^{-3}pC
        }

         void AddVoltageContribution(int32_t dV){}
         int64_t
    RunningCharge_Get(){ return mRunningCharge;}
        void
    AddChargeContribution(int32_t C);
        /*!
         * \brief Take the next point from the current table
         */
        void Spiking_method(void);
        int32_t AxonsNo_Get(void)
    {return m_Axons.size();} //!< List of neurons where our axons lead to
        int32_t SynapsesNo_Get(void)
    { return m_Synapses.size();} //!< List of neurons where our synapses receive input
        void
    ReadSettings(string Group, QSettings* settings);
//        void setState(AbstractNeurer AN, AbstractNeurerState S);
//#if BIOLOGICAL_NEURON
        int32_t
        MembraneRestingPotential_Get()
        {
            return mMembraneRestingPotential;
        }
        void
        MembraneRestingPotential_Set(int32_t NewPot)
        {
            mMembraneRestingPotential = NewPot;
        }
        int32_t
        MembranePotentialOffset_Get()
        {
            return 0;
        }
        /*!

         * \brief Add a contribution to membrane's potential; changes states and initiates timed operations.
         * For details see section @ref TAC_NEURER_TECHNICAL_OPERATION_PAGE.
         * \param[in] NewContrib the voltage contribution to add to present potential
         */
        void
        MembranePotentialContribution_Add(int32_t NewContrib);
        /*!
         * \brief Add a contribution from a synapse; called from a synapse handler; neglects input when 'Charging'
         * \param[in] NewContrib the new contribution from a synapse
         * \return true if the contribution accepted
         */
        bool
        MembraneSynapticContribution_Add(int32_t NewContrib);
//#endif //BIOLOGICAL_NEURON
        /*!
         * \brief Set a bit in the state word AbstractNeurer#mOperatingState
         * \param[in] B which bit in the state word to set
         * \param[in] V the new value of the bit
         *
         */
        void
    NeurerStateBit_Set(NeurerOperatingBit_t B, bool V)
    {
        assert(B < gob_Max);
        if(B==nsb_Resting) mOperatingState = 0; // Clear all other bits if resting
        mOperatingState[B] = V;
    }
        /*!
         * \brief Get value of a bit in the state word AbstractNeurer#mOperatingState
         * \param[in] B which bit in the state word to read
         * \return the value of bit
         */
        bool
    NeurerStateBit_Get(NeurerOperatingBit_t B)
        {   assert(B < gob_Max);
            return mOperatingState[B];
        }

protected:

        std::bitset<nsb_Max>
    mOperatingState; //!< The word of operating bits
//        AbstractNeurerState *mState;   //!< Stores the actual working state
    void
        State_Set();
/*        void    /// Make the actual inspection; overwritten in subclasses
    doInspect(vector<int32_t>& in, vector<int32_t>& out);*/
        void Leaking_method(void);
        void Recalculate_method(void);

        scAxonalConnects_t
    m_Axons; //!< List of neurons where our axons lead to
//        vector <SynapticConnect_t>
        scSynapticConnects_t
    m_Synapses; //!< List of neurons where our synapses receive input
       void
    AxonalConnectionAdd_To(AbstractNeurer* AC);
       void
    Fire(void);
        short unsigned int
    mLeakingPeriod;
        void
    Resynchronize_method(void);
        sc_core::sc_time
    LocalTime_Get(void);
        sc_core::sc_event*
    mSynchronFrequency; //!< Contains which event resets the phase timing
        sc_core::sc_time
    mSynchronOffset;    //!< Contains the time of the last synchronization event
        int
    mNoOfSynapticPoints; //!< Calculated runtime, actually as m_Synapses.size()
        int64_t     // Where the calculation of charge contributions is summed
    mRunningCharge;
        // Floating points have a default -1 … 1 range, but can take any integral limits
//        typedef saturating::type<float, -10, 200> ActionPotential_t;
 //       saturating::type<float, -10, 200>
        int32_t     //!< The actual, measurable potential
    mMembranePotential;
        int32_t     //!< The actual, measurable potential offset, applied externally
    mMembranePotentialOffset;
        ActionPotential*
    mActionPotential;
        Spike*
    mSpike;
        int
    mSpikeTime; //!< Our shadow time about spiking
        int
    mSpikeAmplitude; //!< Our shadow amplitude about spiking
        int
    mSpikeIndex;    //!< The running index of the broken line
        uint16_t
    mTimeFactor, mAmplitudeFactor; //!< Scale factors for the spike table
        // Fix beginning of current action potential to current simuated time
        sc_core::sc_time
    ActionTime_Get();  // Return current time relative to zje beginnin gof the action potential
        void
    ActionPotentialTimeBegin_Set(void);
        sc_core::sc_time
    mActionPotentialStartAtMembrane1, mActionPotentialStartAtMembrane2;
#if BIOLOGICAL_NEURON
 //   #if !USE_INDIVIDUAL_RESTING_POTENTIAL
 //      static int
   // #endif // USE_INDIVIDUAL_RESTING_POTENTIAL
        int32_t   //!< The value of the resting potential, for this neuron only, in uV
        mMembraneRestingPotential;
#endif //
        //Just working variables, cross-action potential phases
    bool    // Remember if the last time, the membrane was within tolerance
        mBelowThresholdPreviously,
        mNearRestingPreviously;
        sc_core::sc_time    //!< Remember the beginning of the actual action potential
    mActionPotentialBegin;
}; // of AbstractNeurer
/** @}*/
#endif // AbstractNeurer_h

