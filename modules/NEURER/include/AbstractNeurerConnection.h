/** @file AbstractNeurerConnection.h
 *  @brief Function prototypes for the ScQtNeurer simulator, and abstract neurer connections.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef AbstractNeurerConnection_h
#define AbstractNeurerConnection_h

#include <vector>
//#include "types.hpp"
#include "NeurerConfig.h"
#include "Spike.h"

using namespace sc_core; using namespace std;

class scAxonalConnect;
class AbstractNeurer;
//class Spike;

/*!
 * \brief Handles input from an axon of a remote neuron, firing to this synapsis.
 * Handles the input broken-line Spike
 *
 * All synapses of a neuron are represented by an scSynapticConnect.
 * The AbstractNeurer stores its synapses in an ordered list in AbstractNeurer#m_Synapses.
 * An scSynapticConnect lives independently: it receives its one single Spike from the host AbstractNeurer,
 * and integrates its input independently, storing its partial sum in the corresponding storage of
 * the host AbstractNeurer. Also, it may trigger, independently, firing of the host AbstractNeurer.
 *
 * Technically, a scSynapticConnect receives the information from its host that it should process a Spike.
 * It will integrate the current described in the spike, adding the charge to the running sum in the AbstractNeurer.
 * If the synaptic current (the average current divided by the time span of the step)
 * exceeds a threshold value, the synapsis causes the host neurer to fire, idependently from its action potential.
 */
class scSynapticConnect : public sc_core::sc_module
{
public:
    scSynapticConnect(sc_core::sc_module_name nm, AbstractNeurer* Sender, AbstractNeurer* Receiver);
    SC_HAS_PROCESS(scSynapticConnect);
    ~scSynapticConnect();
        AbstractNeurer*
    Receiver_Get(void);//{ return mParent;}
        AbstractNeurer*
    Sender_Get(void);//{ return mSender;}
        int16_t
    Weight_Get(void);//{ return mWeight;}
        void
        /*!
         * \brief The "saltatoric" fire: calls the parent's "Fire" process
         */
    Fire(void);
        /*!
         * \brief Process a spike by this synapsis
         * \param[in] MySpike   pointer to the table of the Spike
         * \param[in] MyTimeFactor the time values from the table shall be multiplied by this/THOUSAND
         * \param[in] MyAplitudeFactor the amplitude values from the table shall be multiplied by this/THOUSAND
         */
        void
    ProcessSpike(Spike& MySpike, uint16_t MyTimeFactor=THOUSAND, uint16_t MyAplitudeFactor=THOUSAND);
        struct{
         sc_core::sc_event
             Integrate    ///< A new portion of a synaptic event appeared
            ,Fire    ///< The synapsis wanna send a message to the postsynaptic neuron
         ;
         }EVENT_SYNAPSIS;  //!< These events are handled at abstract processor level
         void
    Integrate_method(void);
         string
    PrologString_Get(void);
 protected:
    AbstractNeurer* mSender; ///< the AbstractNeurer (i.e., a scHThread) we work for
    AbstractNeurer* mReceiver;   ///< The other end of the connection
    Spike* mSpike;  ///< Pointer to the spike table
    uint16_t mTimeFactor, mAplitudeFactor; ///< Scale factors for the spike table
    uint16_t mWeight; ///< Its weight; in units of THOUSAND
    int16_t mSpikeIndex; ///<Running index to the spike table
    uint16_t mLastTime;  ///< Time since the beginning of the spike, in usec; Limits to 65 msec
    int16_t mLastCurrent;   ///< Last synaptic current, in pA; Limits to +/- 32 nA
};

/*!
 * \brief Handles the axonal output to a synapsis of a remote neuron.
 * Handles the output broken-line spike
 */

class scAxonalConnect
{
public:
    scAxonalConnect(AbstractNeurer* Receiver
                    );
    ~scAxonalConnect();
    AbstractNeurer* Receiver_Get(void );
        string
    PrologString_Get(void);
protected:
    AbstractNeurer* mReceiver; ///< The other end of the connection
};


#endif // AbstractNeurerConnection_h

