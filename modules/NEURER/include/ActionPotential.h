/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @file ActionPotential.h
 *  @brief Handling action potential for neuron
 */

#ifndef ACTIONPOTENTIAL_H
#define ACTIONPOTENTIAL_H

#include "NeurerConfig.h"
#include "Utils.h"
using namespace std;
class AbstractNeurer;

/*!
 * \struct SpikePoint
 *
 * \brief The form of an action potential is stored in 2-item vectors, a simple X-Y value pair
 * The time is given at an abstract scale (as digitized from an image),
 * The amplitude is signed, in units uV; the time is in usec
 * The output is set to 'Amplitude', and the next change happens at 'Time'
 * If 'Time'==0, the spike is terminated.
 *
 */
struct ActionPotentialPoint{unsigned int Time; signed int Amplitude;};
typedef vector<ActionPotentialPoint>  ActionPotentialVector;


/*!
 * \class ActionPotential
 *
 * \brief Handles action potential tables for the neurons.
 *
 * For efficiency, only one copy of the action potential forms is used
 * The action potentialis given using microsec (1e-6 sec) as X-scale (time) and microVolt (1e-6 V) as Y-scale (voltage) units.
 * You can define a scale factor (the coded value is multiplied by that value)
 *
 * At the beginning of a action potential, value (0,0) is tacitly assumed
*/

/*
 * Performing floating calculations is a serious limiting factor of computing simulation.
 * This class uses a special method that the factors are in units of THOUSAND
 * The integer values to be scaled are multiplied by that factor, then divided by THOUSAND
 * If THOUSAND=1024 was selected in the config file, the division is replaced by shifting
 * This leads to 2.4% deviation from the 'true' value, and much quicker operation.
 *
 * Spikes may have two forms, depending on a configurable variable.
 * If SPIKE_DETAILED is true:
 * a real current pulse is delivered, which is integrated in the synapses,
 * (a broken line, with adjustable time and aplitude factors)
 * otherwise only the major information is delived
 *
 * In both cases the arrival time is delivered, per synapses to the spikes
 * (this is the logical time: the message
 * A spike is sent by AbstractNeurer#Fire to all (postsynaptic)  neurons, the synapses of which are listed
 * in AbstractNeurer#m_Axons. The spike message is delivered immediately, but it is put
 * by the destination neuron in a time-ordered queue, where it will seemingly arrive
 * exactly at the wanted time, and will be delivered to the correct synapsys, where
 * it will be processed as described at scSynapticConnect.
 *
 * excitation period 0-2.3; -70 mV--50 mV
 * 0 ms 324, 5 ms 610
 * 0 V 221, -100 mV 504
 * X position: (x-324)*(610-324)/5 == 57.2

 * Pont (500,232) ::: (3.077,-3.887)
 * Pont (531,120) ::: (3.618, 35.689)

 */

/*! \class ActionPotential
 * When the membrane's threshold potential is reached, an action potential is generated.
 * Technically, a new object "ActionPotential" object is generated, which autonomously sends
 * its contributions until the signal ends. More than one independent action potentials may be active
 * at he same time.
 * When an ActionPotential is started, a new object is created and it adds independently
 * its contribution to the AbstractNeurer it belongs to.
 * When its potential value contribution falls below the resting potential value,
 * it sends an "NEURER.RefractoryEnd" event to its neurer and continues adjusting its potential
 * (in the refractory period, ONLY the ActionPotential can change membrane's potential.
 * Outside the refractory period, also synaptic inputs can contribute.
 * This method enables to handle superposition of action potentials:
 * a new ActionPotential can begin immediately after the previous potential
 * (i.e., within the relative refractory period)
 * is out of the absolute refractory period.
 *
 * An ActionPotential comprises three crucial timing events:
 * - When the AbstractNeurer generates a new ActionPotential
 * - When the ActionPotential generates a
 *
 */
class ActionPotential
{
  public:
    ActionPotential(AbstractNeurer* Neurer, const ActionPotentialVector& ActionPotentialForm );

    // * For simplicity (and lazyness) the time (X) and voltage (Y) values are calculated as
    // * X position: (x-324)/57.2
/*    sc_core::sc_time Time_Get(int n)
    {assert(n<mActionPotentialLength);
        sc_core::sc_time ThisTime = sc_core::sc_time((mActionPotentialTable[n].Time-324)/0.572,sc_core::SC_US);   // Return time value to the potential increase
        sc_core::sc_time TDif = ThisTime-mLastTime; mLastTime = ThisTime;
        return TDif;}   // Returm the difference relative to the current time, in usec : the next firing time
    // * Y position: (y-221)*(504-221)/100 = -2.83
    uint32_t Voltage_Get(int n){assert(n<mActionPotentialLength);
         return (-(mActionPotentialTable[n].Amplitude-221)/0.00283 );}
        // The potential contruibution compared to the resting potential
    bool AddVoltageContribution(int32_t dV);
*/          virtual
    ~ActionPotential(){}
        void        //!< Called to add the actual contribution to neuron's membrane potential
    ActionPotential_method(void);
        int32_t
    FiringTime_Get(){   return ActionTime_Get(mActionPotentialLength-2) - TimeBegin;}
        int32_t
    RelaxTime_Get(){   return ActionTime_Get(mActionPotentialLength-1) -TimeBegin;}
        int32_t
    TotalTime_Get(){   return ActionTime_Get(mActionPotentialLength-4) - TimeBegin;}
        int32_t
    MomentaryTime_Get(){   return ActionTime_Get(mActionPotentialIndex) - TimeBegin;}
        int32_t
    MomentaryVoltage_Get(){   return ActionVoltage_Get(mActionPotentialIndex);}
        int32_t
    ActionTime_Get(int Index);
//        { return (mActionPotentialTable[Index].Time-324)*17.38;}
        int32_t
    ActionVoltage_Get(int Index);
//        {return -(mActionPotentialTable[0].Amplitude-221)*353.3;}

protected:
    AbstractNeurer* MyNeurer;   // Which neurons membrane we are contriibuting to
    const ActionPotentialVector& mActionPotentialTable;   // Points to a spike table
    int32_t mLastTime;  // Remember the last time, in usec
    int32_t mLastVoltage; // Last voltage, in uV
    int mActionPotentialIndex;  // Just a work variable, maybe not needed
    int mActionPotentialLength;   // Just to know  size
    int32_t TimeBegin; // When the action potential became active
    bool mInRefractoryPeriod;   //
 };

#endif // ACTIONPOTENTIAL_H
