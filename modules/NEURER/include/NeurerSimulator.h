/** @file NeurerSimulator.h
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef NeurerSimulator_h
#define NeurerSimulator_h
#include <systemc>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>
#include <QDate>
#include <QTime>
#include <QSettings>
#include <QCoreApplication>
//#include "scGridPoint.h"
//#include "scBenchmarkingHead.h" // Contains conditional compiles
#include "scSimulator.h"
#include "NeurerProcessor.h"
class scClusterBus;
class scClusterBusArbiter;
class NeurerProcessor;
class scGridPoint;
class scqTreeModel;
using namespace sc_core; using namespace std;


//Submodule forward class declarations
//SC_MODULE(scSimulator) {
class NeurerSimulator : public scSimulator {
    //Port declarations
    // channels
  protected:
 //   sc_clock C1 {"clk", 100, SC_PS};
    //
    //Channel/Submodule* definitions
/*  simple_bus_master_blocking     *master_b;
  simple_bus_master_non_blocking *master_nb;
  simple_bus_master_direct       *master_d;
  simple_bus_slow_mem            *mem_slow;
*/
     scClusterBus                *msClusterBus;
//  simple_bus_fast_mem            *mem_fast;
  scClusterBusArbiter             *msClusterBusArbiter;
  public:
    // Constructor declaration:
    NeurerSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone=true);
      SC_HAS_PROCESS(NeurerSimulator); // Contructor implemented in .cpp
      virtual
    ~NeurerSimulator();

        void
    readSettings();
        virtual void
    writeSettings(void);
 /*       void
    ReadNeuronSettings(QSettings *settings, string Group);
*/
      void Setup(void);  // Belongs to the constructor
      void
    SInitialize_method(void); ///< This initializes the simulator
 /*     string
      PrologText_Get(void);
    void writeSettings(void); // Write settings to the project directory
      bool
    StepwiseMode_Get(void){ return msStepwiseMode;}
      void
    StepwiseMode_Set(bool b){msStepwiseMode = b;}
      void
    SSUSPEND_thread(void);
      void
    SRESUME_thread(void);
      void
    SSTART_thread(void);
      void
    SSTOP_thread(void);
      void
    SHALT_thread(void);
        struct{
      sc_core::sc_event
    START,
    STOP,
    HALT,
    SUSPEND,
    RESUME;
        }EVENT_SIMULATOR;
        //virtual
        void ClockFlag_Set(bool B){m_clockFlag = B;}
        bool ClockFlag_Get(void){return m_clockFlag;}

   scqTreeModel* mModuleTree;
   scqTreeModel* mClusterTree;
//   scqTreeItem *rootItem;
 */
      NeurerProcessor*
    Processor_Get();
//    { return dynamic_cast<NeurerProcessor*>(scSimulator::Processor_Get());}
  protected:
      /*!
       * \brief scqSimulator::readSettings
       * makes the real reading, application, user or project-wide settings
       *
       * \param settings the QSettings to be used
       * \param Level a string, either "Application", "User" or "Project"
       * \param Group a string, the name of the keyword group
       */
          void
      readSettings(QSettings *settings, string Level, string Group); ///< makes the actual reading
          void
      ReadTheThreeLevels(QString MyGroup);
  #if 0
        void clock_method(void)
        { ClockFlag_Set(true);}
        void SetupHierarchies(void);
    /*
       void
    PrintFinalReport(scProcessor *Proc);
    */
#endif //0
        void
    HandleSpecials(void); /// Handle the exceptional points
        vector<scGridPoint*>
    mSpecials;   /// Stores the special points
#if 0
      bool
    msStepwiseMode,   ///< If to process stepwise
    msSuspended;      ///< If running simulation is suspended
      bool
    m_clockFlag;
//    static SystemDirectories Directories;
    string mSettingsFileName;   /// The filename of the config files
    QScopedPointer<QFile>   m_logFile;
#endif //0

}; // of scSimulator

#endif // NeurerSimulator_h
