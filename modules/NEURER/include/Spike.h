/*  @author János Végh (jvegh)
*  @bug No known bugs.
*/
/** @file Spike.h
 *  @brief Handling the spikes of neuronal communication
 */

#ifndef SPIKE_H
#define SPIKE_H
//#include <iostream>     // for bit fields
//#include <sstream>
//#include <bitset>         // std::bitset

#include "NeurerConfig.h"
#include "Utils.h"
using namespace std;

/*!
 * \struct SpikePoint
 *
 * \brief The form of spikes is stored in 2-item vectors, a simple X-Y value pair
 * The time is given at an abstract scale, typically in multiples of usec (up to 565 msec spike length)
 * The amplitude is signed, typically in multiples of uA; (from -32 mA to +32 mA)
 * The output is set to 'Amplitude', and the next change happens at 'Time'
 * If 'Time'==0, the spike is terminated.
 *
 */
struct SpikePoint{unsigned int Time; signed int Amplitude;};
typedef vector<SpikePoint>  SpikeVector;


/*!
 * \class Spike
 *
 * \brief Handles spike tables for the neurons.
 *
 * For efficiency, only one copy of the spike forms is used
 * The spike is given using microsec (1e-6 sec) as X-scale (time) and picoA (1e-12 Amper) as Y-scale (current) units.
 * You can define a scale factor (the coded value is multiplied by that value)
 *
 * At the beginning of a spike, value (0,0) is tacitly assumed
*/

/*
 * Performing floating calculations is a serious limiting factor of computing simulation.
 * This class uses a special method that the factors are in units of THOUSAND
 * The integer values to be scaled are multiplied by that factor, then divided by THOUSAND
 * If THOUSAND=1024 was selected in the config file, the division is replaced by shifting
 * This leads to 2.4% deviation from the 'true' value, and much quicker operation.
 *
 * Spikes may have two forms, depending on a configurable variable.
 * If SPIKE_DETAILED is true:
 * a real current pulse is delivered, which is integrated in the synapses,
 * (a broken line, with adjustable time and aplitude factors)
 * otherwise only the major information is delived
 *
 * In both cases the arrival time is delivered, per synapses to the spikes
 * (this is the logical time: the message
 * A spike is sent by AbstractNeurer#Fire to all (postsynaptic)  neurons, the synapses of which are listed
 * in AbstractNeurer#m_Axons. The spike message is delivered immediately, but it is put
 * by the destination neuron in a time-ordered queue, where it will seemingly arrive
 * exactly at the wanted time, and will be delivered to the correct synapsys, where
 * it will be processed as described at scSynapticConnect.
 */

class Spike
{
  public:
    Spike(const SpikeVector& SpikeForm, uint16_t AmplitudeFactor=THOUSAND, uint16_t TimeFactor=THOUSAND ):
    mSpikeTable(SpikeForm)
    ,mAmplitudeFactor(AmplitudeFactor)
    ,mTimeFactor(TimeFactor)
    ,mSpikeLength(SpikeForm.size())
    {
    }
    uint16_t AmplitudeFactor_Get(void) {return mAmplitudeFactor;}
    uint16_t TimeFactor_Get(void) {return mTimeFactor;}
    uint16_t Time_Get(int n){assert(n<mSpikeLength); return (mSpikeTable[n].Time * mTimeFactor)/THOUSAND;}
    uint16_t Amplitude_Get(int n){assert(n<mSpikeLength); return (mSpikeTable[n].Amplitude * mAmplitudeFactor)/THOUSAND;}
//    void NextSpikePoint_Get(SpikePoint& P);
          virtual
    ~Spike(){}

protected:
    const SpikeVector& mSpikeTable;   // Points to a spike table
    uint16_t mAmplitudeFactor; ///< Amplitude multiplier of the current spike
    uint16_t mTimeFactor;  ///< Time multiplier of the current spike
    int mSpikeIndex;
    int mSpikeLength;   // Just to know  size
 };

#endif // SPIKE_H
