/** @file Neurer.h
 *  @brief Function prototypes for the Neurer simulator.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef Neurer_h
#define Neurer_h

#include "AbstractNeurer.h"

using namespace sc_core; using namespace std;
class NeurerProcessor;
/*!
 * \brief The Neurer class.
 * The class implements the neurer functionality, closer to the neurons.
 * The base class is the AbstractNeurer, i.e. a communicating gridpoint.
 *
 * @verbatim
 *  |GridPoint
 *  |--/scGridPoint
 *  |----/AbstractCore
 *  |....../XXCore
 *  |------/AbstractNeurer
 *  |--------/Neurer
 * @endverbatim
*/

class Neurer :  public AbstractNeurer
{
  public:
    Neurer(sc_core::sc_module_name nm, // Just the SystemC name
             NeurerProcessor* Processor, // The present system is prepared for one topology only, but ...
             scGridPoint* GP // The topographic position
           , SC_HTHREAD_ID_TYPE ID
             ,bool StandAlone
            );
    ~Neurer(void);
        SC_HAS_PROCESS(Neurer);  // We have the constructor in the .cpp file
      // Directly HW-related functionality
        void
    Reset(void);

        Neurer*
    Parent_Get(void) { return  dynamic_cast<Neurer*>(AbstractNeurer::Parent_Get());}	///< Return the parent scGridPoint of the scGridPoint
        NeurerProcessor*
    Processor_Get(void);
protected:
}; // of Neurer

#endif // Neurer_h

