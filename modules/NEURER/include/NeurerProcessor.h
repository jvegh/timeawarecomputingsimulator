/** @file NeurerProcessor.h
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *  It assumes 'Nerver' objects in the gridpoints
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef NEURERPROCESSOR_H
#define NEURERPROCESSOR_H
#include "AbstractProcessor.h"
#include "Neurer.h"

/*! \brief The NeurerProcessor class assumes that in the AbstractProcessor gridpoints
 * neurer objects are sitting
  */
using namespace std;
// The modules can be the head of the cluster, a member of a cluster, or neither (stand-alone)
// These are the standard offsets of the neighbors in the order of
// the hexagonal Cluster Grid:  Head, N, NE, SE, S, SW, NW

// Here a two-dimensional topology is assumed, and a multi-layer one implied
/*!
 * \brief The NeurerProcessor class
 *
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |----/AbstractProcessor
 *  |------/NeurerProcessor
 * @endverbatim
 */
class NeurerProcessor : public AbstractProcessor
{
  public:
    NeurerProcessor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone);
    SC_HAS_PROCESS(NeurerProcessor); // Will be defined in separate file
    ~NeurerProcessor(void);
        void
    Reset(void);
  //      AbstractNeurer*
//        doReboot(SC_ADDRESS_TYPE A){assert(0);};
        void Initialize_method(void);
        void
    CreateNeurons(AbstractCore* C);
        void
    CreateConnections(void);
        void
    Populate(vector<scGridPoint*>& Specials);
/*        AbstractNeurer*
    Neurer_Get(int i, int j, SC_HTHREAD_ID_TYPE H)
        {assert(H<MAX_HTHREADS);return dynamic_cast<AbstractNeurer *>(AbstractProcessor::GridPoint_Get(i,j)->HThread_Get(H));}
*/        AbstractNeurer*
    ByIndex_Get(const int X, const int Y, SC_HTHREAD_ID_TYPE H)
        {   assert(H<MAX_HTHREADS);
        return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByIndex_Get(X, Y)->HThread_Get(H));}
/*    AbstractNeurer* ByPosition_Get(const int X, const int Y, SC_HTHREAD_ID_TYPE H)
        { return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByPosition_Get(X, Y)->HThread_Get(H));}
    AbstractNeurer* ByClusterAddress_Get(ClusterAddress_t CA, SC_HTHREAD_ID_TYPE H)
        { return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByClusterAddress_Get(CA)->HThread_Get(H));}
    AbstractNeurer* ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM, SC_HTHREAD_ID_TYPE H)
        { return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByClusterMember_Get(CN,CM)->HThread_Get(H));}
    AbstractNeurer* ByName_Get(string N){ return  dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByName_Get(N));}
*/        void
    START_thread(void);
/*        Neurer*
    ClusterHead_Get(int i)
        { return dynamic_cast<Neurer*>(AbstractProcessor::ClusterHead_Get(i)); }
    scGridPoint *GridPoint_Get(const int i, const int j)
        {return dynamic_cast<scGridPoint *>(mGrid.at(i).at(j));}
    scGridPoint *GridPoint_Get(scGridPoint *GP) // A kind of self-check: dynamic_cast will return NULL if wrong
        {return dynamic_cast<scGridPoint *>(GP);}
*/
        struct{
         sc_core::sc_event  ///< Provide basic frequencies for neurons, at fixed rate
               BASIC        ///< 10 Hz
             ,ALPHA    ///< 30 Hz
         ;
         }EVENT_NEURERPROCESSOR;  //!< These events are handled at abstract processor level

    AbstractNeurer* ByID_Get(unsigned int N, unsigned int H)
    {   assert(H<MAX_HTHREADS);
        assert(N<MAX_GRIDPOINTS);
          return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByID_Get(N)->HThread_Get(H));
    }
    AbstractNeurer* ByIDMask_Get(SC_CORE_MASK_TYPE Mask, unsigned int H) /// Return a pointer to gridpoint of the core given by its mask
        {     assert(H<MAX_HTHREADS);
              AbstractCore* A= AbstractProcessor::ByIDMask_Get(Mask);
                AbstractNeurer* T = dynamic_cast<AbstractNeurer*>(A->HThread_Get(H));
                      return T;
//        return dynamic_cast<AbstractNeurer*>(AbstractProcessor::ByIDMask_Get(Mask));
        }

/*    void Populate(void);
    void ConnectIGPCBs(void);
        bool
    RouteMessageTo(ClusterAddress_t To);
        SC_GRIDPOINT_MASK_TYPE
    DeniedMask_Get(void) {  return msSignalMask.Denied;}
         void
    DeniedMaskBit_Set(scGridPoint* GP, bool P=true);
         SC_GRIDPOINT_MASK_TYPE
    AllocatedMask_Get(void) {  return msSignalMask.Allocated;}
          void
    AllocatedMaskBit_Set(scGridPoint* GP, bool P=true);
          SC_GRIDPOINT_MASK_TYPE
    PreAllocatedMask_Get(void) {  return msSignalMask.PreAllocated;}
           void
    PreAllocatedMaskBit_Set(scGridPoint* GP, bool P=true);
         SC_GRIDPOINT_MASK_TYPE
    AvailableMask_Get(void)
         {  return ~UnavailableMask_Get();}
         SC_GRIDPOINT_MASK_TYPE
    UnavailableMask_Get(void);
*/
protected:
    /*!
     * \brief Provides a signal EVENT_NEURERPROCESSOR.BASIC with frequency 10 Hz
     */
    void FrequencyBasic_method(void);
    /*!
     * \brief Provides a signal EVENT_NEURERPROCESSOR.BASIC with frequency 30 Hz
     */
    void FrequencyAlpha_method(void);
 };// of class NeurerProcessor

#endif // NEURERPROCESSOR_H
