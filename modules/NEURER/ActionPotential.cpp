/** @file ActionPotential.cpp
 *  @brief Functionality for handling the action potential
 */
 /*
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
 using namespace std;
#include "ActionPotential.h"
#include "AbstractNeurer.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"


/*
 * from https://www.dummies.com/education/science/biology/action-potential-of-neurons/
 * begin 2.3,-50
excitation period 0-2.3; -70 mV--50 mV
0 ms 324, 5 ms 610
0 V 221, -100 mV 504
X position (in usec): (x-324)*5000/(610-324) == (x-324)*17.38
Y position(in uV): (y-221)*100*1000/(504-221) == (221-y)*353.3

Pont (500,232) ::: (3.059,-3.887)
Pont (531,120) ::: (3.684, 35.689)
Pont (486,342) ::: (2.884, -42.75)
Pont (591,420) ::: (4.752, -70.7)

462,504  talppont
462,376  starting
470,380
474,372
481,362
486,342
488,334
489,330
490,320
491,310
492,300
493,295
494,290
495,288
496,271
497,263
498,247
499,239
500,232
501,224
502,214
503,199
504,188
505,182
506,178
508,165
510,150
511,143
512,138
514,133
516,130
517,127
519,123
521,122
525,122
527,121
528,125
530,128
533,135
535,141
536,145
538,151
539,160
540,166
541,175
542,178
543,185
544,192
545,200
546,205
547,211
548,223
549,227
550,232
551,240
552,251
554,263
556,278
557,289
561,303
562,317
564,327
566,334
567,344
568,350
570,360
573,373
575,383
578,391
577,386
580,394
583,403
585,411
588,413
591,420
595,426
601,432
604,434
611,434
627,433
644,440
657,424
660,426
662,425
667,423
673,421
682,420
687,420
692,420
*/

/*
 * These numeric tables contain how how to set the action potential according to the numeric table:
 * (what amplitude, how long time)
 * A zero time terminates the list
 */

//static A
ActionPotentialVector ActionPotentialVector1 {
    {470,380}, {474,372}, {481,362}, {486,342}, {488,334}, {489,330}, {490,320}, {491,310}, {492,300}, {493,295},
    {494,290}, {495,288}, {496,271}, {497,263}, {498,247}, {499,239}, {500,232}, {501,224}, {502,214}, {503,199},
    {504,188}, {505,182}, {506,178}, {508,165}, {510,150}, {511,143}, {512,138}, {514,133}, {516,130}, {517,127},
    {519,123}, {521,122}, {525,122}, {527,121}, {528,125}, {530,128}, {533,135}, {535,141}, {536,145}, {538,151},
    {539,160}, {540,166}, {541,175}, {542,178}, {543,185}, {544,192}, {545,200}, {546,205}, {547,211}, {548,223},
    {549,227}, {550,232}, {551,240}, {552,251}, {554,263}, {556,278}, {557,289}, {561,303}, {562,317}, {564,327},
    {566,334}, {567,344}, {568,350}, {570,360}, {573,373}, {575,383}, {578,391}, {577,386}, {580,394}, {583,403},
    {585,411}, {588,413}, {591,420}, {595,426}, {601,432}, {604,434}, {611,434}, {627,433}, {644,440}, {657,424},
    {660,426}, {662,425}, {667,423}, {673,421}, {682,420},
    {0,0}, // Terminate the list
    {525,122}, // The peak of action potential
    {591,420} // The end of the absolute refractory period
};

// static ActionPotential MyActionPotential(nullptr,ActionPotentialVector1);

extern bool UNIT_TESTING;	// Whether in course of unit testing

ActionPotential::ActionPotential( AbstractNeurer* Neurer, const ActionPotentialVector& ActionPotentialForm ):
MyNeurer(Neurer)
,mActionPotentialTable(ActionPotentialForm)  // Sets up the potential form
,mActionPotentialLength(ActionPotentialForm.size()) // Just to know
,mInRefractoryPeriod(false)
{
    mLastTime = ActionTime_Get(0) - MEMBRANE_CHARGE_DELAY;
    mLastVoltage = ActionVoltage_Get(0);
    TimeBegin = mLastTime;
    mActionPotentialIndex = 1;  // The real play starts at point 1
/*    SC_METHOD(ActionPotential_method);
        sensitive << EVENT_POTENTIAL.Next;
        dont_initialize();  // No spike at the beginning
        */
}

int32_t ActionPotential::
ActionTime_Get(int Index)
{ return (mActionPotentialTable[Index].Time-324)*17.38;}
int32_t ActionPotential::
ActionVoltage_Get(int Index)
{return -(mActionPotentialTable[Index].Amplitude-221)*353.3;}

/*! Gets the next time difference (interpreted in microsecs)
 * and increases the membrane potential (interpreted in microvolts) in its neuron's membrane, directly
 * then schedules the next potential increase step. The 0th step is initiated by the constructor.
 * When the potential drops below the resting potential, finishes the absolute refractory period
 * (sends a notification to NEURER.RefractoryEnd to its neuron), then continues producing
 * its potential contributions until the relative refractory period ends.
 */
    void ActionPotential::
ActionPotential_method(void)
{
    int32_t T = mActionPotentialTable[mActionPotentialIndex].Time; // Check coding
    if(T)
    {
        // it is a non-zero value, continue giving contributions
        // X position (in usec): (x-324)*5000/(610-324) == (x-324)*17.38

        int32_t ThisTime = ActionTime_Get(mActionPotentialIndex);
        int32_t  dT = ThisTime - mLastTime;  // This is the time difference to the next event
        //Y position(in uV): (y-221)*100*1000/(504-221) == (221-y)*353.3
        int32_t mThisVoltage = ActionVoltage_Get(mActionPotentialIndex++);
        int32_t dV = mThisVoltage - mLastVoltage;
/*        MyNeurer->AddVoltageContribution(dV);
        // How we have when and mow many: add the conribution to the membrane
        if(!mInRefractoryPeriod & (mThisVoltage < 0))
        {
            MyNeurer->EVENT_NEURER.RefractoryEnd.notify(dT); mInRefractoryPeriod = true;
        }
*/
    }
    else
        // it is the end of the
        delete this;
}
/*            int32_t T = (mActionPotential->Time_Get(mActionPotential->mActionPotentialIndex));
        mActionPotential->mActionPotentialTime += T;
        mActionPotential->mActionPotentialAmplitude  = (mActionPotential->mActionPotential->Amplitude_Get(mActionPotential->mActionPotentialIndex++));
        if(T)
             EVENT_NEURER.ActionPotential.notify(T*sc_time(1,SC_US));
        else {mActionPotential->index = 1;
        }  // Reset for a new action potential

        }
       DEBUG_PRINT_OBJECT("ActionPotential at (" << mActionPotentialTime << "," << mActionPotentialAmplitude);
*/     //   }


