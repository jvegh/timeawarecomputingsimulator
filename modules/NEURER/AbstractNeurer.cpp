/** @file AbstractNeurer.cpp
 *  @brief Function prototypes for the SystemC based neuron simulator
 */
 /*  Provides base for the scNeurer
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

//#include "scTypes.h"
#include "Spike.h"
#include "ActionPotential.h"
//#include "AbstractNeurer.h"
#include "NeurerProcessor.h"
//#include "Memory.h"
#include "AbstractNeurerConnection.h"
//#include "NeurerStates.h"
#include <QSettings>    // For reading neurer settings
#include "scqSimulator.h"
//extern scqSimulator* TheSimulator;// This defines if running through a simulator

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
//#include <vector>


/*
 * These numeric tables contain how to set the output according to the numeric table:
 * (what amplitude, how long time)
 * A zero time terminates the list
 */
static SpikeVector SquareSpikeVector {
    {10,100}, {0,0}
};

static SpikeVector ClockSpikeVector {
    {100,100}, {100,-100}, {200,-100}, {200,0}, {0,0}
};



//static
Spike ClockSpike(ClockSpikeVector);
Spike SquareSpike(SquareSpikeVector);


/*
 * from https://www.dummies.com/education/science/biology/action-potential-of-neurons/
 * begin 2.3,-50
excitation period 0-2.3; -70 mV--50 mV
0 ms 324, 5 ms 610
0 V 221, -100 mV 504
X position: (x-324)*(610-324)/5 == 57.2
X position: (x-324)/57.2
Y position: (y-221)*(504-221)/100 = -2.83

Pont (500,232) ::: (3.077,-3.887)
Pont (531,120) ::: (3.618, 35.689)


462,504  talppont
462,376  starting
470,380
474,372
481,362
486,342
488,334
489,330
490,320
491,310
492,300
493,295
494,290
495,288
496,271
497,263
498,247
499,239
500,232
501,224
502,214
503,199
504,188
505,182
506,178
508,165
510,150
511,143
512,138
514,133
516,130
517,127
519,123
521,122
525,122
527,121
528,125
530,128
533,135
535,141
536,145
538,151
539,160
540,166
541,175
542,178
543,185
544,192
545,200
546,205
547,211
548,223
549,227
550,232
551,240
552,251
554,263
556,278
557,289
561,303
562,317
564,327
566,334
567,344
568,350
570,360
573,373
575,383
578,391
577,386
580,394
583,403
585,411
588,413
591,420
595,426
601,432
604,434
611,434
627,433
644,440
657,424
660,426
662,425
667,423
673,421
682,420
687,420
692,420
*/

/*
 * These numeric tables contain how how to set the action potential according to the numeric table:
 * (what amplitude, how long time)
 * A zero time terminates the list
 */

class NeurerProcessor;
/*
 * Basic SystemC functionality of grid modules + grid topologic position handling
 */
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
/*!
 * \brief AbstractNeurer::AbstractCore
 *
 */
//* \param StandAlone standalone event handling shall be implemented

    AbstractNeurer::
AbstractNeurer(sc_core::sc_module_name nm,
//               NeurerProcessor* Processor, // The present system is prepared for one topology only, but ...
               scGridPoint* GP, SC_HTHREAD_ID_TYPE ID,
               const bool StandAlone
                   ):    scHThread(nm, GP,ID)
      ,mOperatingState(0)  // Initially, the neurers are in rest
   ,mLeakingPeriod(1000)
   ,mNoOfSynapticPoints(1)
   ,mRunningCharge(0)
   ,mSpike(new Spike(ClockSpikeVector, 2*THOUSAND, 10*THOUSAND))
{
    Reset();
#if BIOLOGICAL_NEURON
    mMembraneRestingPotential = RESTING_POTENTIAL;
#endif // BIOLOGICAL_NEURON
    SC_THREAD(Initialize_method);
    SC_METHOD(Leaking_method);
        sensitive << EVENT_NEURER.Leaking;
    SC_METHOD(Spiking_method);
        sensitive << EVENT_NEURER.Spiking;
        dont_initialize();  // No spike at the beginning

    SC_METHOD(FireBegin_method);
        sensitive << EVENT_NEURER.FireBegin;           /// Received when the peak voltage of membrane received
        dont_initialize();  // No firing at the beginning
    SC_METHOD(RefractoryEnd_method);
        sensitive << EVENT_NEURER.RefractoryEnd;      /// Received when the membrane potential drops below its triggering threshold
        dont_initialize();  // No refractory end at the beginning

 /*   SC_METHOD(ActionPotential_method);
        sensitive << EVENT_NEURER.ActionPotential;
        dont_initialize();  // No spike at the beginning
  */  SC_METHOD(Recalculate_method);
        sensitive << EVENT_NEURER.Recalculate;
        // Set the basic requency as default
        mSynchronFrequency = &Processor_Get()->EVENT_NEURERPROCESSOR.BASIC;
    SC_METHOD(Resynchronize_method);    // Must be AFTER the reset
        sensitive << *mSynchronFrequency;

    SC_METHOD(Reset_method);
        sensitive << EVENT_NEURER.Reset;       // Puts the neurer in its initial state

    SC_METHOD(Trigger_method);
        sensitive << EVENT_NEURER.Trigger;  ///< Puts the neurer in synaptic-aware more

    SC_METHOD(Charge_method);
        sensitive << EVENT_NEURER.Charge;  ///< Accumulates charge on the membrane, synaptic-unaware

    SC_METHOD(Discharge_method);
        sensitive << EVENT_NEURER.Discharge; ///< Sends the spike to the axon

/*        int MyCharge =
 IntegratedCurrentInPeriod(1000, 2*1000,
                               100, 200);
*/
//    IncreaseActionPotentialWithCharge(MyCharge);

    mSpike = new Spike(ClockSpikeVector, 2, 10);
//    mActionPotential = new ActionPotential(ActionPotentialVector1);
/*    int index = 0; float T = 0;
    do
    {
        T = MySpike->Time_Get(index);
        float A = MySpike->Amplitude_Get(index++);
        cerr << "Fired at (" << T << "," << A << ")\n";
    }
    while(T);
*/
/*    vector <int32_t> in,out;
    Inspect(in,out);
    InspectSwitch(true);
    Inspect(in,out);*/
}// of AbstractNeurer::AbstractNeurer

    /*!
     * \brief Sets the neurer to its basic state.
     * May include applied external potential and external current, both time-dependent
     * Sensitive to the event  EVENT_NEURER.Reset
     */
    void AbstractNeurer::
Reset_method(void)
    {
        mOperatingState = 0;    // Clear all state bits
        mBelowThresholdPreviously = true; // The potential is close to threshold
        NeurerStateBit_Set(nsb_Resting, true);
        ActionPotentialTimeBegin_Set();  // This is the beginning of as action time; resets at failing
#if BIOLOGICAL_NEURON
        mMembranePotential = MembraneRestingPotential_Get() + MembranePotentialOffset_Get();
#endif // BIOLOGICAL_NEURON
    }

    sc_core::sc_time AbstractNeurer::
ActionTime_Get()
    {   return sc_core::sc_time_stamp()-mActionPotentialBegin;
    }// This is the beginning of as action time; resets at failing

    void AbstractNeurer::
MembranePotentialContribution_Add(int32_t NewPot)
{
    // The first case that need action: the potential is near to the resting one
    // Check if the potential is close to the resting potential, before and after contribution
    bool NearRestingBefore = abs(mMembranePotential-MembraneRestingPotential_Get()) < MEMBRANE_POTENTIAL_TOLERANCE;
    bool BelowThresholdBefore = mMembranePotential<MEMBRANE_THRESHOLD;
    mMembranePotential += NewPot;   // OK, now we have a new membrane level
    bool NearRestingAfter = abs(mMembranePotential-MembraneRestingPotential_Get()) < MEMBRANE_POTENTIAL_TOLERANCE;
    bool AboveThresholdAfter = mMembranePotential>MEMBRANE_THRESHOLD;

    if( NearRestingBefore and NearRestingAfter and mNearRestingPreviously)
    {   // we noticed some uncertainty, but three points show the voltage is at the the resting potential
        Reset_method();     // Be on the safe side: reset all bits
    }

    if((NeurerStateBit_Get(nsb_Triggering) or NeurerStateBit_Get(nsb_Discharging))
            and BelowThresholdBefore and AboveThresholdAfter and mBelowThresholdPreviously)
    {   // we were definitely below the threshold, and now we are definitely above it
        // so we switch to "charge" mode
//        SetCharge_Method();
    }

    mNearRestingPreviously = NearRestingBefore; // Remember the last before this, for the next time
    mBelowThresholdPreviously = BelowThresholdBefore; // Remember if pre-last time it was below the threshold

    // The second critical case is: the potential exceeded the critical value
}

    // The action potential for statistics
    void AbstractNeurer::
ActionPotentialTimeBegin_Set() // This is the beginning of as action time; resets at failing
{
        mActionPotentialBegin = sc_core::sc_time_stamp();
}


/*
 *
 */
    bool AbstractNeurer::
MembraneSynapticContribution_Add(int32_t NewContrib)
{
    if(NeurerStateBit_Get(nsb_Charging))
        {   // We are in the charging period, no synaptic input is accepted
            return false;
        }
    // OK, we are either resting, or discharging; simply contribute
    MembranePotentialContribution_Add(NewContrib);
    return true;
}

    /*
     * This method is activated when an ActionPotential sends EVENT_NEURER.FireBegin.
     * The neuron starts its firing
     */

    void AbstractNeurer::
FireBegin_method(void)
    {

    }
    /*
     * This method is activated when an ActionPotential sends EVENT_NEURER.RefractoryEnd.
     * The neuron finishes its refractory period
     */
    void AbstractNeurer::
RefractoryEnd_method(void)
    {

    }

    void AbstractNeurer::
Trigger_method(void)
    {

    }

    void AbstractNeurer::
Charge_method(void)
    {

    }

    void AbstractNeurer::
Discharge_method(void)
    {

    }
/*
    int32_t  AbstractNeurer::
    MembraneRestingPotential_Get()
    {

    }
    int32_t  AbstractNeurer::
    MembranePotentialOffset_Get()
    {

    }*/
    /*
    Reset       ///< Puts the neurer in its initial state
    ,Trigger ///< Puts the neurer in synaptic-aware more
    ,Charge  ///< Accumulates charge on the membrane, synaptic-unaware
    ,Discharge ///< Sends the spike to the axon
*/

/*!
 * This method starts up when everything is initialized
 */
    void AbstractNeurer::
Initialize_method(void)
{
    DEBUG_PRINT_OBJECT("Initialize_method started" );
    SynchronFrequency_Set(&Processor_Get()->EVENT_NEURERPROCESSOR.BASIC);

    // Just for debugging
    if(this == Processor_Get()->ByIndex_Get(0,0,0))
        Fire();
}
/*
    void AbstractNeurer::
 CreateNeurons(void)
 {
   for(SC_HTHREAD_ID_TYPE H = 0; H < MAX_HTHREADS; H++)
       AddHThread(H, new AbstractNeurer(string("HThread").append(IDtoString(H,HTHREAD_BUS_WIDTH)).c_str(),this, H,true));
 //      C->mHThreads[H] = new AbstractNeuron(string("HThread").append(IDtoString(H,2)).c_str(),C, H);
 }
    */

    /*
     * Most often, the threshold potential is a membrane potential value between –50 and –55 mV, but can vary based upon several factors. A neuron's resting membrane potential (–70 mV) can be altered to either increase or decrease likelihood of reaching threshold via sodium and potassium ions.

-70 mV
The resting membrane potential of a neuron is about -70 mV (mV=millivolt) - this means that the inside of the neuron is 70 mV less than the outside. At rest, there are relatively more sodium ions outside the neuron and more potassium ions inside that neuron.

The course of the action potential can be divided into five parts: the rising phase, the peak phase, the falling phase, the undershoot phase, and the refractory period. During the rising phase the membrane potential depolarizes (becomes more positive). The point at which depolarization stops is called the peak phase.

An action potential has several phases; hypopolarization, depolarization, overshoot, repolarization and hyperpolarization.
*/
    
    void AbstractNeurer::
Reset(void)
{
        scHThread::Reset();
}

    /*!
     * \brief The neuron permanently loses its charge, periodically decreasing the charge.
     * The larger the period, the slower the dischange. A zero value means "no leaking".
     */
    void AbstractNeurer::
Leaking_method(void)
{
    if(mLeakingPeriod)
    {// We really have leaking
        mLeakingPeriod -= 1;
        EVENT_NEURER.Leaking.notify(mLeakingPeriod*sc_time(SCTIME_RESOLUTION));
    }
}

    /*!
     * \brief Add a new contribution to the running collected charge
     * \param C the charge contribution (may be negative)
     */
    void    AbstractNeurer::
AddChargeContribution(int32_t C)
{
    mRunningCharge += C;
}

       // EVENT_NEURER.Spiking received
        void AbstractNeurer::Spiking_method(void)
        {
            int T = (mSpike->Time_Get(mSpikeIndex)*mTimeFactor)/THOUSAND;
            mSpikeTime += T;
            mSpikeAmplitude  = (mSpike->Amplitude_Get(mSpikeIndex++)*mAmplitudeFactor)/THOUSAND;
            if(T)
                 EVENT_NEURER.Spiking.notify(T*sc_time(1,SC_US));
           DEBUG_PRINT_OBJECT("Fired at (" << mSpikeTime << "," << mSpikeAmplitude);
        }


     void AbstractNeurer::
Fire(void)
{
         mSpikeIndex = 0; mSpikeTime = 0;
         EVENT_NEURER.Spiking.notify(); // Notify spiking method
}

     /*!
     * \brief This method is sensitive to EVENT_NEURERPROCESSOR#BASIC or equivalent.
     */
    void AbstractNeurer::
Resynchronize_method(void)
{
        mSynchronOffset = sc_core::sc_time_stamp();
}

    /*!
     * \brief LocalTime_Get
     * \return the time since last synchron signal
     */
    sc_core::sc_time AbstractNeurer::
LocalTime_Get(void){    return  sc_core::sc_time_stamp()-mSynchronOffset;}
    void AbstractNeurer::   ///< Set the lenght of the period after which the potential decreasd by one
LeakingPeriod_Set(short unsigned int P)
{
    mLeakingPeriod = P;
    EVENT_NEURER.Leaking.notify();  // Force an immediate setting
}

    /*!
     * \brief  In the case of prospective change of behavior, the new potential is calculated
     */
    void AbstractNeurer::
Recalculate_method(void)
{

}

AbstractNeurer::~AbstractNeurer()
{
}//AbstractNeurer::~AbstractNeurer

NeurerProcessor* AbstractNeurer::
Processor_Get(void)
{ return  dynamic_cast<NeurerProcessor*>(scHThread::Owner_Get()->Processor_Get());}	/// Return the parent scGridPoint of the scGridPoint

// Both lists are ordered by absolute values, just to be able to use binary search
bool Synapticcmp(scSynapticConnect* a, scSynapticConnect* b)
{   return (uint64_t) a->Sender_Get() < (uint64_t) b->Sender_Get(); }

bool Axonalcmp(scAxonalConnect* a, scAxonalConnect* b)
{   return (uint64_t) a->Receiver_Get() < (uint64_t) b->Receiver_Get(); }

void AbstractNeurer::
ConnectTo(AbstractNeurer* A)
{
    assert(A); // Verify it pointer is real
    string ConnectName = string("Syn_").append(IDtoString(Owner_Get()->ID_Get(),3)).append("_").append(IDtoString(ID_Get(),3)).append("_").
            append(IDtoString(A->Owner_Get()->ID_Get(),3)).append("_").append(IDtoString(A->ID_Get(),3));
    scSynapticConnect* YourSynapse = new scSynapticConnect(sc_core::sc_module_name(ConnectName.c_str())
                                                           ,this, A);
    scAxonalConnect* MyAxon = new scAxonalConnect(A//, this
                                                  );

    scAxonalConnects_t::iterator it = std::lower_bound( m_Axons.begin(),m_Axons.end(), MyAxon, Axonalcmp); // find proper position in descending order
    m_Axons.insert( it, MyAxon ); // insert before iterator it
    A->ConnectFrom(YourSynapse);
}


void AbstractNeurer::
ConnectFrom(scSynapticConnect* A)
{
    std::cerr << "Connected " << A->Sender_Get()->StringOfName_Get() << "->" << StringOfName_Get() << "\n";
    DEBUG_PRINT_OBJECT("Connected to " << Parent_Get()->StringOfClusterAddress_Get());
    DEBUG_PRINT_OBJECT("Connecting from " << A->Sender_Get()->Parent_Get()->StringOfClusterAddress_Get());
    std::cerr << "Connected to " << StringOfClusterAddress_Get() << "\n";
    std::cerr << "Connecting from " << A->Sender_Get()->StringOfClusterAddress_Get() << "\n";
    scSynapticConnects_t::iterator it = std::lower_bound( m_Synapses.begin(),m_Synapses.end(), A, Synapticcmp); // find proper position in descending order
    m_Synapses.insert( it, A ); // insert before iterator 'it'
}

/*!
 * \brief Find the synapsis which is connected to AnbstractNeurer A
 * \param A the address of neurer to find
 * \return pointer to the scSynapticConnect if found, NULL else
 */
    scSynapticConnect* AbstractNeurer::
FindSynapseFrom(AbstractNeurer* A) {
    int32_t right = m_Synapses.size(); // one position passed the right end
    if(!right) return (scSynapticConnect*) nullptr;
    int32_t mid = 0, left = 0 ;
    uint64_t key = (uint64_t) nullptr;
    while (left < right) {
        if((uint64_t)m_Synapses[mid]->Sender_Get() == (uint64_t)A)
             return m_Synapses[mid]; // Accidentally, we found it
        mid = left + (right - left)/2;
        if (key > (uint64_t) m_Synapses[mid]->Sender_Get()){
            left = mid+1;
        }
      else if (key < (uint64_t) m_Synapses[mid]->Sender_Get()){
        right = mid;
      }
   }
     // We arrived at the end, check if we found it
    if((uint64_t)m_Synapses[left]->Sender_Get() == (uint64_t)A)
        return m_Synapses[left];
    else
        return (scSynapticConnect*)nullptr;
}

/*!
 * \brief Find the axon which is connected to AbstractNeurer A
 * \param A the address of the neurer to find
 * \return pointer to the scAxonalConnect if found, NULL else
 */
    scAxonalConnect* AbstractNeurer::
FindAxonTo(AbstractNeurer* A) {
    int32_t right = m_Axons.size(); // one position passed the right end
    assert(right);
    uint64_t key = (uint64_t) nullptr;
    int32_t mid = 0, left = 0 ;
    while (left < right) {
        if((uint64_t)m_Axons[mid]->Receiver_Get() == (uint64_t)A)
             return m_Axons[mid]; // Accidentally, we found it
      mid = left + (right - left)/2;
      if (key > (uint64_t) m_Axons[mid]->Receiver_Get()){
          left = mid+1;
      }
      else if (key < (uint64_t) m_Axons[mid]->Receiver_Get()){
        right = mid;
      }
   }
     // We arrived at the end, check if we found it
    if((uint64_t)m_Axons[left]->Receiver_Get() == (uint64_t)A)
        return m_Axons[left];
    else
        return (scAxonalConnect*) nullptr;
}

    string AbstractNeurer::
PrologString_Get(void)
{
    return "MyNeuron";
}

    void AbstractNeurer::
ReadSettings(string Group, QSettings* settings)
{
        /// Temporary hack to
     foreach(const QString &MyChild, settings->childKeys())
    {
        if(QStringList(INI_NEURON_KEYWORDS).indexOf(MyChild) < 0)
                qWarning() << "Unexpected keyword " << MyChild << " for " << Group.c_str() << " will be neglected";
            else
            {   // This is a legal keyword
                if(!MyChild.compare("Version"))
                {   QString IniVersion = settings->value("Version","").toString();
                    if(IniVersion.compare(QString(PROJECT_VERSION)))
                        qWarning() << "Expected Version=" << QString(PROJECT_VERSION) << ", received " << IniVersion;
                }
                else
                { //

                }
            }
        }
        // Now we have all the connected neurons in childGroups, read connection parameters
        foreach(const QString &MyGroup, settings->childGroups())
        {
            AbstractNeurer* Dest = (AbstractNeurer*)Processor_Get()->ByName_Get(MyGroup.toStdString());

            settings->beginGroup(MyGroup);
            if(Dest)
            {
                ConnectTo(Dest);
                DEBUG_PRINT( "Reading connection for '" << Group << "=>" << MyGroup.toStdString() );

                // Now we have the destination: read its parameters
                foreach(const QString &MyKey, settings->childKeys())
                {
                    DEBUG_PRINT( "Reading key '" << MyKey.toStdString() << "' for '" << Group << "=>" << MyGroup.toStdString() << "'");
                }
            }
            else
            {   // It is something else, such as 'Group'
                DEBUG_PRINT( "Unimplemented group '" <<MyGroup.toStdString() << "' used");
            }
            settings->endGroup();
        }
}


 #ifdef other
   /**
    * @brief Assemble a heading text for 'core' messages
    *
    * @return string containing time, memory address, QT state, core state
    */
   string AbstractNeurer::
PrologText_Get(SC_ADDRESS_TYPE PC) // Get a unified prolog text for logging
 {
   ostringstream oss;
   string S =  QTText_Get(PC);
   oss << Processor_Get()->TimeProlog_Get()  << S;
   int QTLength = strlen(S.c_str());
   for(int i = QTLength; i<12 ; i++) oss << " ";
   return oss.str();
 }

//Assemble QT info for the heading text for 'core' messages
  string scVirtualCore::
QTText_Get(unsigned int PC)
{
  ostringstream oss;
  oss << "[" << hex << setw((MEMORY_ADDRESS_WIDTH+3)/4) << setfill('0') << (unsigned int) PC << "]" << CoreText_Get() << "<";
  string IDstring = QTID_Get()+'>';
  if(0==IDstring.length()) IDstring = "Proc";
  oss << IDstring;
  return oss.str();
}

  /**
   * @brief Assemble core info for the heading text for 'core' messages
   *
   * @return string, describing core internals
   */
      // Assemble core info for the heading text for 'core' messages
    string scVirtualCore::
  CoreText_Get(void)
      {
        ostringstream oss;
        oss << hex << setw(2) << setfill('0') << dec << ID_Get();// << "C";
//        if(msNestingLevel) oss << "/" << msNestingLevel;
  //      if(mEmbeddedLevel) oss << "/" << (char) ('a' + mEmbeddedLevel -1);
        return oss.str();
      }

  // Assemble the name for a new QT, started by Parent
    string scVirtualCore::
  QT_AssembleID( scVirtualCore* Parent)
  {
  //  Processor* Proc = Processor_Get();
    string QTID;
    // Prepare an ID in advance
    char Plus; char QTType;
    if(Parent)
    { // we do have a real core as parent, continue its naming
//        char
//      QTType = Parent->Regime_Get() ? 'Q' : 'R';
      QTID = QTID.append(Parent->QTID_Get()); // We have a core parent, get its QTID
      Plus = AssembleQTIDSubcode(Parent->ChildrenMask_Get() & ~IDMask_Get());
//      Plus = AssembleQTIDSubcode(Parent->ChildrenCount_Get());
    }
    else
    {
      QTType = Regime_Get() ? 'Q' : 'R';;
      QTID = QTID.append(1,QTType);
      Plus = AssembleQTIDSubcode(Processor_Get()->ChildrenMask_Get() & ~IDMask_Get());  // All children but me :)
//      Plus = AssembleQTIDSubcode(Processor_Get()->ChildrenCount_Get());  // All children but me :)
    }
    // Anyhow, now the QTID base is assembled, append subcode
    QTID.append(1,Plus);
    return QTID;
  }
#endif //other

    /* !
     * \brief AbstractNeurer::doInspect
     *
     * Make the actual inspection; overwrite in scGridPoint
     * \param in    input data vector
     * \param out   output data vector
     */
/*    void
doInspect(vector<int32_t>& in, vector<int32_t>& out)
{
        std::cerr << "AbstractNeurer Inspecting, received " << in.size();
}*/
