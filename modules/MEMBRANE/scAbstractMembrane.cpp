/** @file scAbstractMembrane.cpp
 *  @brief Function prototypes for the topology of processor cores, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "Utils.h"
#include "scAbstractMembrane.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

bool OBJECT_FILE_PRINTED;
extern bool UNIT_TESTING;	// Whether in course of unit testing
/*!
 * \brief scAbstractMembrane::scAbstractMembrane
 * \param nm SystemC name
 * \param Specials the list of special neurons
 * \param StandAlone true if some part shall be executed here rather than in the subclass
 * 
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |..../scAbstractMembrane
 * @endverbatim
 * 
 */
     scAbstractMembrane::
scAbstractMembrane(sc_core::sc_module_name nm, nullptr_t N, bool StandAlone)
             : AbstractTopology(),sc_core::sc_module( nm)
             ,mMPS(mpt_Closed)
{
         assert (!((PATCH_GRID_SIZE_X < 0) || (PATCH_GRID_SIZE_Y < 0)));
         mGrid.resize(PATCH_GRID_SIZE_X);
         for(int i = 0; i < PATCH_GRID_SIZE_X; i++, i++) // This needed because of the hexagonal grid
         {
             mGrid.at(i).resize(PATCH_GRID_SIZE_Y);
             mGrid.at(i+1).resize(PATCH_GRID_SIZE_Y);
         }
         // Populate first with NULLs
         for(int i = 0; i < PATCH_GRID_SIZE_X; i++) // This needed because of the hexagonal grid
             for(int j= 0; j < PATCH_GRID_SIZE_Y; j++)
                     mGrid[i][j] = (scAbstractPatch*) nullptr; // No points are present
         PopulateMembrane() ;
         CreateClusters();      // Just for the 10x5 case!!

         // The computation can be started with an event EVENT_MEMBRANE.Timing
         SC_THREAD(TIMING_thread);
             sensitive << EVENT_MEMBRANE.Timing;
//             dont_initialize();
//             EVENT_MEMBRANE.Timing.notify(sc_time(0,SC_MS));
  //           wait(sc_time(10,SC_MS));
    if(StandAlone)
    {
 /*       SC_THREAD(Initialize_method);
        SC_THREAD(TIMING_thread);
             sensitive << EVENT_PROCESSOR.START;
        Populate(N); // Populate with abstract patches*/
  ///!!! ConnectIGPCBs();       // Connect their communication lines
 ///!!! ConnectClusterHeadsToBus(mClusterBus);  // No clusters
        Reset();

    }
}

   scAbstractMembrane::
~scAbstractMembrane(void)
{
}

   /*!
      * \brief scAbstractMembrane::ByIndex_Get
      * Return the patch point at the given coordinates
      * \param X coordinate
      * \param Y coordinate
      * \return NULL if outside the grid
      */
     scAbstractPatch* scAbstractMembrane::
 ByIndex_Get(const int X, const int Y)
 {
     assert (!((PATCH_GRID_SIZE_X < 0) || (PATCH_GRID_SIZE_Y < 0)));
     assert (!((X<0) || (X >= PATCH_GRID_SIZE_X)));
     assert (!((Y<0) || (Y >= PATCH_GRID_SIZE_Y)));
    return dynamic_cast<scAbstractPatch*>
            (AbstractTopology::ByIndex_Get(X,Y));
 }
     /*!
      * \brief ByID_Get(int N) Get the membrane patch defined by its sequence number
      * \param[in] N The sequence number of the patch point
      * \return
      */
     scAbstractPatch* scAbstractMembrane::
 ByID_Get(int N)
 {
     assert (( N >= 0) && (N<PATCH_GRID_SIZE_X * PATCH_GRID_SIZE_Y));
     int MyX = N/PATCH_GRID_SIZE_Y; int MyY = N%PATCH_GRID_SIZE_Y;
     return dynamic_cast<scAbstractPatch*>
             (AbstractTopology::ByIndex_Get(MyX,MyY));
 }

     scAbstractPatch* scAbstractMembrane::
 ByPosition_Get(int X, int Y)
 {
         // Must not assert: used internally to detect if point legal
    if(X<0 || X >= PATCH_GRID_SIZE_X) return (scAbstractPatch*) NULL;
    if(Y<0 || Y >= PATCH_GRID_SIZE_Y*2) return (scAbstractPatch*) NULL;
    assert( X%2 == Y%2); // Both must be even or both odd
    return ByIndex_Get(X,(Y - moduloN(X,2))/2);
 }



     /*!
      * \brief Populate the scAbstractMembrane with scAbstractPatch modules
      */
  void scAbstractMembrane::
 PopulateMembrane(void)
 {
      // Create communication grid point objects
     scAbstractPatch* A;
     for(int i = 0; i < PATCH_GRID_SIZE_X; i++, i++) // Twice because of the hexagonal grid
     {
         for(int j = 0; j < PATCH_GRID_SIZE_Y; j++)
         {
             int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
                 if(!mGrid.at(i).at(j))
                 {// This is pre-populated by a special
                     A = new scAbstractPatch(sc_core::sc_module_name(string("AP").append(IDtoString(LinearAddress1,PATCHGRID_BUS_WIDTH)).c_str()),
                         this,// LinearAddress1,
                         GridPoint(i,j), true//StandAlone
                     );
                     mGrid.at(i).at(j)= A; // Store the pointer in the grid
                 }
                 int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
                 if(!mGrid.at(i+1).at(j))
                 {// This is pre-populated by a special
                     A = new scAbstractPatch(sc_core::sc_module_name(string("AP").append(IDtoString(LinearAddress3,PATCHGRID_BUS_WIDTH)).c_str()),
                         this, //LinearAddress3,
                         GridPoint(i+1,j), true//StandAlone
                     );
                     mGrid.at(i+1).at(j) = A; // Store the pointer in the shadow-column
                 }
         }
     }
  }

/*!
 * This method starts up when everything is initialized
 */
void scAbstractMembrane::Initialize_method(void)
{
  EVENT_MEMBRANE.Timing.notify(sc_time(3,SC_MS));

//    wait(SCTIME_SV); // For setup
//    DEBUG_PRINT_OBJECT("Initialize_method started" );
//    mscoreStatus = STAT_AOK; // Assume everything all-right
    // We surely have no processing unit and also the first instruction must be fetched
//    Processor_Get()->EVENT_PROCESSOR.START.notify(SC_ZERO_TIME);
}

/*!
     * \brief The membran given cyclic timing signals for its patches.
     * Technically, it can be directed to the really-in-use patches.
     * Recently, the patch receives the events but neglects them in not-in-use state
     *
     */
    void scAbstractMembrane::
TIMING_thread(void)
{
    while(1)
    {
        switch(mMPS)
        {
            case mpt_Closed:  // Was Closed, gets opened
                    mMPS = mpt_Opened;  // Now we will be opened for some time
                    EVENT_MEMBRANE.Open.notify(SC_ZERO_TIME);
                    cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Timing.notify() in Closed" << endl;
                    cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Open.notify() in Closed" << endl;
                    wait(10, SC_NS);
                    EVENT_MEMBRANE.Timing.notify(SC_ZERO_TIME);
            break;
            case mpt_Opened:  // Was opened, gets inactivated
                  mMPS = mpt_Inactivated; // Now we will be closed for some time
                  EVENT_MEMBRANE.Inactivate.notify(SC_ZERO_TIME);
                  cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Timing.notify()" << endl;
                  cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Inactivate.notify() in Opened" << endl;
                  wait(sc_time(2,SC_NS));
                  EVENT_MEMBRANE.Timing.notify(SC_ZERO_TIME);
            break;
            case mpt_Inactivated: // Was inactivated, gets closed
                  mMPS = mpt_Closed;     // Now will take refractory time
                  EVENT_MEMBRANE.Close.notify(SC_ZERO_TIME);
                  cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Timing.notify()" << endl;
                  cout << "[" << sc_time_stamp() << "] called EVENT_MEMBRANE.Close.notify() in Closed" << endl;
                  wait(sc_time(2,SC_NS));
                  EVENT_MEMBRANE.Timing.notify(SC_ZERO_TIME);
            break;
        }
    }
}

/*!
 * \brief A timing period passed, see which one and send the corresponding event
 */
    void scAbstractMembrane::
Reset(void)
{
 /*     scProcessor::Reset();
      for(int i = 0; i < GRID_SIZE_X; i++)
        for(int j = 0; j < GRID_SIZE_Y; j++)
            ByIndex_Get(i,j)->Reset();
            */
}

    // Create clusters from the elements of the rectangular grid.
      void scAbstractMembrane::
  CreateClusters(void)
  {
      // Not needed if passed creation; but recall also here
     assert (!((GRID_SIZE_X < 0) || (GRID_SIZE_Y < 0)));  // Protect that size
     assert (!((GRID_SIZE_X > 10) || (GRID_SIZE_Y > 6)));
     Cluster_Create(1,9);
     Cluster_Create(2,4);
     Cluster_Create(4,8);
     Cluster_Create(5,3);
     Cluster_Create(7,7);
     Cluster_Create(8,2);
     // These next two clusters are incomplete, contain 1+2 members
     Cluster_Create(9,11);
     Cluster_Create(0,0);
     // These are phantom clusters: the head is outside,
     // but some orphan members are inside
     Cluster_Create(-1,5);
     Cluster_Create(3,-1);
     Cluster_Create(6,-2);
     Cluster_Create(10,6);
     Cluster_Create(6,12);
     Cluster_Create(3,13);
  }


/*!
 * \brief scAbstractMembrane::START_thread
 * This thread is waiting for a START_event and processes it
 * It should be reimplemented in the derived classes
 */
#if 0
    void
AbstractMembrane::START_thread(void)
{
    while(true)
    {
         DEBUG_EVENT_OBJECT("WAIT  EVENT_PROCESSOR.START");
         wait(EVENT_PROCESSOR.START);
         DEBUG_EVENT_OBJECT("RCVD  EVENT_PROCESSOR.START");
         msHalted = false;
         //    wait(1,SC_NS);
         // Temporary, used to test FETCH events
/*         scProcessor *Proc = Processor_Get();
         scGridPoint *GP10 = Proc->ByID_Get(10);
         scGridPoint *GP11 = Proc->ByID_Get(11);
         scHThread *HT103 = GP10->HThread_Get(3);
         scHThread *HT105 = GP10->HThread_Get(5);
         scHThread *HT115 = GP11->HThread_Get(5);
         //        wait(1,SC_NS);  // Enable initialization happen
         // These HThreads ask for a gridpoint at the same time.
         // Only one of HT103 or HT105 will get through
         HT103->EVENT_HTHREAD.FETCH.notify();
         wait(10,SC_PS);
         HT105->EVENT_HTHREAD.FETCH.notify();
         HT115->EVENT_HTHREAD.FETCH.notify();
         wait(10,SC_PS);
         bool HT103Busy = HT103->OperatingBit_Get(tob_FetchPending);
         bool HT105Busy = HT105->OperatingBit_Get(tob_FetchPending);
         bool HT115Busy = HT115->OperatingBit_Get(tob_FetchPending);
         */
    }
}
#endif


