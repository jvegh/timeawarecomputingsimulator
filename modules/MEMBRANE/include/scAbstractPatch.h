/** @file scAbstractPatch.h
 *  @brief Function prototypes for the EMPA simulator, Core.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scAbstractPatch_h
#define scAbstractPatch_h
/** @addtogroup TAC_MODULE_MEMBRANE
 *  The membrane patch related classes and functionality
 *  @{
 */


class scAbstractMembrane;
#include "GridPoint.h"
//#include "scClusterBusMemorySlow.h"
using namespace sc_core;
using namespace sc_dt;

//typedef SC_PATCH_ID_TYPE SC_CORE_ID_TYPE;
//typedef SC_PATCH_MASK_TYPE  SC_CORE_MASK_TYPE;

#ifdef MEASURE_PERFORMANCE
    #define PERFORMANCE_COUNTINSTRUCTION CountInstruction();
    #define PERFORMANCE_COUNTMETAINSTRUCTION CountMetaInstruction();
    #define PERFORMANCE_ADDINSTRUCTIONTIME(t) mInstructionTime +=t;
    #define PERFORMANCE_ADDMETAINSTRUCTIONTIME(t) mMetaInstructionTime +=t;
    #define PERFORMANCE_ADDMEMORYTIME(t) mMemoryTime +=t;
    #define PERFORMANCE_ADDFETCHTIME(t) mFetchTime +=t;
    #define PERFORMANCE_ADDWAITTIME(t) mWaitTime +=t;
#else
    #define PERFORMANCE_COUNTINSTRUCTION
    #define PERFORMANCE_COUNTMETAINSTRUCTION
    #define PERFORMANCE_ADDINSTRUCTIONTIME(t)
    #define PERFORMANCE_ADDMETAINSTRUCTIONTIME(t)
    #define PERFORMANCE_ADDMEMORYTIME(t)
    #define PERFORMANCE_ADDFETCHTIME(t)
    #define PERFORMANCE_ADDWAITTIME(t)
#endif // MEASURE_PERFORMANCE



#define  USE_DEBUG_DATA_TYPES

#ifdef USE_DEBUG_DATA_TYPES
typedef int32_t SC_REGISTER_TYPE;
typedef int SC_COOPERATION_MODE_TYPE;
typedef int32_t SC_REGISTER_MASK_TYPE;
#else
typedef sc_dt::sc_uint<CORE_REGISTER_BITS> SC_REGISTER_TYPE;
typedef sc_dt::sc_uint<COOPERATION_MODE_WIDTH> SC_COOPERATION_MODE_TYPE;
typedef sc_dt::sc_uint<MAX_NUMBER_OF_REGFteregicISTERS> SC_REGISTER_MASK_TYPE;
#endif //USE_DEBUG_DATA_TYPES


// Type for the one-hot bitmasks
//typedef int SC_CORE_MASK_TYPE;
typedef SC_GRIDPOINT_MASK_TYPE SC_CORE_MASK_TYPE;
typedef int SC_REGISTER_TYPE;

using namespace sc_core; using namespace std;

class MembraneProcessor;
/*!
 * \class scAbstractPatch
 * \brief Neuron's membrane on its 'input side' (the presynaptic part,
 * where its input comes from),
 * has several synapses, while on its 'output side'
 * (the post-synaptic part)
 * comprises more-or-less well defined corresponding 'patches'
 * (as part of the post-synaptic membrane),
 * where the potential change due to the charge received through its synapses,
 * has an immediate effect on patch's potential that later propagates from patch to patch.
 * Its is assumed that the patch can be described by one synaptic input
 * and one ion channel state. Membrane's Action Potential
 * propagates with a well-defined speed ("conduction velocity") from patch to patch
 * within the membrane, as dictated by the states of the ion channel characteristics.
 *
 * The scAbstractMembrane is covered by scAbstractPatch modules.
 * Their input is on the presynaptic side (a synapse), the output is connected to neighboring patches.
 * The system operates through timed messages.
 * It is assumed that the local potential of the scAbstractPatch can be changed
 * during a synaptic input event (with some synaptic delay, as "non-instant interaction')
 * and the scAbstractPatch element interacts with its neighbors. Their interaction
 * is modelled using the behavior of ion channels. The potential change in a patch
 * propagates toward its neighbours, as the actual states of the ion channels enable.
 *
 * The ion-channel like behavior means that the ions have three (plus one technical) states,
 * see MembranePatchState.
 * By default, the channel is closed (mpt_Closed). As a potential-controlled channel,
 * it gets opened (mpt_Opened) when the neighbor's potential changes,
 * and after forwarding
 * its potential to all neighbors, it gets inactivated (mpt_Inactivated, unable to receive
 * any charge for some time). The technical state 'denied' ((mpt_Denied)
 * is needed
 * because most real neuron membranes have not strictly rectangular form.
 * The scAbstractPatch is actually built on top of GridPoint
 * (i.e., not on top of scGridPoint: the communication functionality is not used.),
 * but the topology information (without clustering) is used.
 * The SystemC functionality here is used to force temporal behavior.
 *
 * @verbatim
 *  |GridPoint
 *  |--/scAbstractPatch
 * @endverbatim
 */

/*! \struct MembranePatchState
 * \brief The possible states of an abstract patch
 */
typedef enum {mpt_Denied,  //!< The patch is not biologically implemented
              mpt_Opened,  //!< The patch is working
              mpt_Inactivated,  //!< The patch is in its refractory period
              mpt_Closed,   //!< The patch is ready to work
              mpt_NotInUse, //!< Presently the patch is not used; waiting for explicit activation
              mpt_Max       // Just and administrative limit watch
             } MembranePatchState;

class scAbstractPatch:  public GridPoint, public sc_core::sc_module
{
    friend MembraneProcessor;
  public:
    /*! Construct an abstract patch. Implements Reset() and event handling.
     * \param[in] nm    The SystemC module name
     * \param[in] Proc  The membrane the patch belongs to
     * \param[in] GP the topological location of the module
     * \param[in] StandAlone If the core implements some stand-alone functionalities
     *
     * The module implements all general-purpose functionality of an neural membrane's patch.
     */

    scAbstractPatch(sc_core::sc_module_name nm
             ,scAbstractMembrane* Proc
             ,const GridPoint GP
             ,bool StandAlone
            );

    ~scAbstractPatch(void);
        SC_HAS_PROCESS(scAbstractPatch);  // We have the constructor in the .cpp file
        void
    Reset(void);

        struct
        {    sc_core::sc_event
          PatchOn  // Include this patch in the game
          ;
         }EVENT_PATCH; //< These events are handled at gridpoint level
        void
    ACTION_method(void);
        /*!
         * \brief Opens the patch: is able to receive charge
         */
        void
    OPEN_method(void);
        /*!
         * \brief Inactivates the patch: it forwarded charge, needs refractory
         */
        void
    INACTIVATE_method(void);
        /*!
         * \brief Sets the default state 'Closed', i.e. able to work again
         */
        void
    CLOSE_method(void);
        /*!
         * \brief Sets patch's state to mps
         * \param[in] mps The state to set
         */
        void
    State_Set(MembranePatchState mps)
    {
        assert(mps < mpt_Max);
        mMPS = mps;
    }

        /*!
         * \brief HaveTransversalCurrent, if any of the neighbors has
         * a voltage different from our own
         * \return true if transversal current is present for this patch
         */
        bool
    HaveTransversalCurrent(void);

        MembranePatchState
    State_Get(void){ return mMPS;}
        /*!
         * \brief Change patch's state to the next one (cycling)
         *
         * Closed-> Opened -> Inactivated
         */
        void
    State_Change(void);
        int32_t
    Potential_Get(void){ return mPotential;}
        void
        Potential_Set(int32_t P){ mPotential = P;}
        int HandleInputs(int32_t Function);
    scAbstractPatch* NextActiveNeighbor_Get(ClusterNeighbor &NN);
    /*!
     * \brief ChargeContribution_Add
     * \param Donator   the patch which donated the charge
     * \param ChargeContribution the amount of charge
     */
    void ChargeContribution_Add(scAbstractPatch* Donator, int ChargeContribution);
protected:
        int32_t
    mPotential, // The present potential value
    mLastPotential; // The previous potential value
        int32_t
    mCurrentCharge;
        bool
    mWasActive; // If the patch received charge, so it must be inactivated
 #ifdef MAKE_PERFORMANCE_DIAGRAM
        sc_time
    msAllocationBegin;    ///< The beginning of the allocation
        sc_time
    msMetaBegin;    ///< The beginning of a meta instruction

        sc_time
    AllocationBegin_Get(void) { return msAllocationBegin;} // Just to record the beginning of being allocated
         void
    AllocationBegin_Set(sc_time T) {msAllocationBegin = T;} // Just to record the beginning of being allocated
#endif //MAKE_PERFORMANCE_DIAGRAM
         scAbstractMembrane*  //!< We belong to this membrane
    mMembrane;
       MembranePatchState   //!< The charge forwarding state for the patch
    mMPS;
       int8_t
    mNeighborContributions;
       int32_t
    mFutureCharge,
    mTotalCharge;
       int32_t
       mThisPotential;
#ifdef MEASURE_PERFORMANCE

#endif //MEASURE_PERFORMANCE
}; // of AbstractCore
/** @}*/

#endif // scAbstractPatch_h

