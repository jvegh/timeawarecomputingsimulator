// The configured options and settings for SystemC developments
// Will be used to configure Main_File/include/Config.h in all subdirs
/** @file
 *  @brief Topology information for the Neurer simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

// The user-configurable part
// Define if want to simulate biological neurons

#define BIOLOGICAL_NEURON true

#if BIOLOGICAL_NEURON
    #define USE_INDIVIDUAL_RESTING_POTENTIAL false   // If to use individual resting potential neurons
    #define RESTING_POTENTIAL -70*THOUSAND    // The value of resting potential, in uV
    #define PATCH_CAPACITY THOUSAND // The capacity of one single membrane patch
    #define MEMBRANE_POTENTIAL_TOLERANCE 1000 // The value is within tolerance level to resting potential
    #define MEMBRANE_THRESHOLD  RESTING_POTENTIAL + 20000;  //Threshold is at -50 mV
    #define MEMBRANE_CHARGE_DELAY 150   // The time the Na+ ions need to reach the membrane
    #define MEMBRANE_SPIKE_DELAY 150   // The time the spike needs to reach the axon hillock
#endif // of BIOLOGICAL_NEURON
// !!
// Do not edit below this line until you know what you are doing
// !!

#include "HWConfig.h"
// Take over some constants
// Clustering works only for 10x6 !!!
#define PATCH_GRID_SIZE_X 10
#define PATCH_GRID_SIZE_Y 6
#define PATCHGRID_BUS_WIDTH 8
#define  SC_PATCH_ID_TYPE SC_CORE_ID_TYPE
#define MAX_NUMBER_OF_PATCHES PATCH_GRID_SIZE_X*PATCH_GRID_SIZE_Y

#define INI_FILES "Simulator Neurons"


// List keywords legal in 'Simulator.ini'
#undef  INI_SIMULATOR_KEYWORDS
#define INI_SIMULATOR_KEYWORDS "Version"

// List
#define INI_NEURON_KEYWORDS "Version"

// For the biology, redefine time resolution and clock rate
// Resolution of time  and clock frequency
#define SCTIME_RESOLUTION 10,sc_core::SC_US
#define NEURER_LEAKING_TIME 10
// Gates operating time
//#define SCTIME_GATE sc_time(20,sc_core::SC_PS)
// Frequency of the system clock
#define SCTIME_CLOCKTIME sc_time(100,sc_core::SC_US)

// In many operations, THOUSAND is used to calculate
// making multiply/divide, it is much faster to use 2^n value; the difference is only 2%
// If it matters, change it to 1000
#define THOUSAND 1024

// Cellular constants assumed
// http://www.columbia.edu/cu/biology/courses/w3004/Lecture5.pdf
//#define SPECIFIC_RESISTANCE_TIMES_CM2 2000 // Ohm/cm^2
//#define SPECIFIC_RESISTANCE_TIMES_u2 2e11  // Ohm/u^2
//#define SPECIFIC_CAPACITANCE_PRO_CM2 1e-6 // F/cm^2
//#define SPECIFIC_CAPACITANCE_PRO_u2 1e-14  // F/u^2
//#define NEURON_AREA 25 // u^2
//#define RC_TIME SPECIFIC_RESISTANCE_TIMES_u2*SPECIFIC_CAPACITANCE_PRO_u2 = 2ms
// So, the actual values used here are
#define SPECIFIC_RESISTANCE_TIMES_u2 5e11  // Ohm/u^2
#define SPECIFIC_CAPACITANCE_PRO_u2 4e-14  // F/u^2
#define NEURON_CURRENT_UNIT 1e-12 // pA
#define NEURON_VOLTAGE_UNIT 1e-6 // uV
#define NEURON_TIME_UNIT 1e-6 // us
#define NEURON_RC_CONSTANT SPECIFIC_RESISTANCE_TIMES_u2*SPECIFIC_CAPACITANCE_PRO_u2 // 20 msec

#define NEURON_RESISTANCE_MOHM NEURON_AREA*SPECIFIC_RESISTANCE_PRO_u2*1e-6 //MOhm
#define SPECIFIC_CAPACITANCE_PRO_CM2 1e-6 // F/cm^2
#define SYNAPTIC_CURRENT_CRITICAL 30000 // in 1e-12A = pA
#define NEURON_CURRENT_INTEGRATION_STEP_MIN 500 // in usec
//#define SPECIFIC_CAPACITANCE_PRO_u2 1e-14  // F/u^2
//#define RC_TIME SPECIFIC_RESISTANCE_TIMES_u2*SPECIFIC_CAPACITANCE_PRO_u2 //

// With these constants, the oscillation time is cca 2 ms
//#define NEURON_CAPACITANCE NEURON_AREA*SPECIFIC_CAPACITANCE_PRO_u2
//#define NEURON_RESISTANCE SPECIFIC_RESISTANCE_TIMES_u2/NEURON_AREA
//#define RC NEURON_RESISTANCE*NEURON_CAPACITANCE




// Operational characteristics
