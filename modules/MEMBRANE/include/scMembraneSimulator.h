/** @file scMembraneSimulator.h
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef scMembraneSimulator_h
#define scMembraneSimulator_h
/** @addtogroup TAC_MODULE_MEMBRANE
 *  The membrane-related classes and functionality
 *  @{
 */
//#include <systemc>
#include <map> // For the aliases
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>
#include <QDate>
#include <QTime>
#include <QSettings>
#include <QCoreApplication>
//#include "scGridPoint.h"
#include "scSimulator.h"
#include "scAbstractMembrane.h"

class scMembraneProcessor;
class scAbstractPatch;
using namespace sc_core; using namespace std;


//Submodule forward class declarations
class scMembraneSimulator : public scqSimulator {
    //Port declarations
    // channels
  public:
    // Constructor declaration:
  /*!
     * \brief The generic MembraneSimulator
     * \param nm SystemC name
     * \param argc Number of arguments
     * \param argv  The argument strings
     * \param StandAlone If standalone operation requested
     */
    scMembraneSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone=true);
      SC_HAS_PROCESS(scMembraneSimulator); // Contructor implemented in .cpp
      virtual
    ~scMembraneSimulator();
      string
      PrologText_Get(void);

    void Setup(void);  // Belongs to the constructor
/*    void writeSettings(void); // Write settings to the project directory
      void
    SInitialize_method(void); ///< This initializes the simulator
      bool
    StepwiseMode_Get(void){ return msStepwiseMode;}
      void
    StepwiseMode_Set(bool b){msStepwiseMode = b;}
      void
    SSUSPEND_thread(void);
      void
    SRESUME_thread(void);
*/      void
    SSTART_thread(void);
/*      void
    SSTOP_thread(void);
      void
    SHALT_thread(void);
    */
    void
        writeSettings(void);
    struct{
      sc_core::sc_event
    START;
        }EVENT_SIMULATOR;
        //virtual
       scAbstractMembrane*
    Processor_Get();

  protected:
    string mSettingsFileName;   /// The filename of the config files
    QScopedPointer<QFile>   m_logFile;
        scAbstractMembrane*
    msMembrane;
}; // of scMembraneSimulator
/** @}*/

#endif // MembraneSimulator_h
