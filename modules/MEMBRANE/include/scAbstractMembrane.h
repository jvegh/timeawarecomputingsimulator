/** @file scAbstractMembrane.h
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *  It assumes 'AbstractCore' objects in the gridpoints
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef SCABSTRACTMEMBRANE_H
#define SCABSTRACTMEMBRANE_H
/** @addtogroup TAC_MODULE_MEMBRANE
 *  The membrane-related classes and functionality
 *  @{
 */
#include "AbstractTopology.h"
#include "scAbstractPatch.h"
#include "MembraneConfig.h"

/*! \brief The AbstractProcessor class assumes that in the AbstractProcessor gridpoints
 * AbstractCore objects are sitting
  */
using namespace std;
// The modules can be the head of the cluster, a member of a cluster, or neither (stand-alone)
// These are the standard offsets of the neighbors in the order of
// the hexagonal Cluster Grid:  Head, N, NE, SE, S, SW, NW

// Here a two-dimensional topology is assumed, and a multi-layer one implied

/*!
 * \brief The scAbstractMembrane class
 *
 * @verbatim
 *  |AbstractTopology
 *  |----/scAbstractMembrane
 * @endverbatim
 */

class scAbstractMembrane : public AbstractTopology, public sc_core::sc_module
{
  public:
    scAbstractMembrane(sc_core::sc_module_name nm, nullptr_t N, bool StandAlone);
    SC_HAS_PROCESS(scAbstractMembrane); // Will be defined in separate file
    ~scAbstractMembrane(void);
        void
    Reset(void);
    // Directly HW-related functionality
    void Initialize_method(void);
    // Accessor functions: without thread, the physical access (0th HThread)
        scAbstractPatch*
    ByID_Get(const int N); //!< Get a gridpoint by its absolute sequence number and thread
        scAbstractPatch*
    ByIndex_Get(const int X, const int Y);  //!< Get a gridpoint defined by its indices (!NOT position)
        scAbstractPatch*
    ByPosition_Get(int X, int Y);   //!< Get a gridpoint by its topological position (NOT index!)
        int
    LinearAddressFromCoordinates_Get( int X, int Y)
    {   assert(!(X<0 || X>=PATCH_GRID_SIZE_X));   assert(!(Y<0 || Y>=PATCH_GRID_SIZE_Y));
        return  X*PATCH_GRID_SIZE_Y +  Y; }
        void
    PopulateMembrane(void);
        void
    CreateClusters(void);
//        void
//    ClockMethod(void);
/*        void
    START_thread(void);
        void
    NEXT_thread(void);
        void
    OPEN_thread(void);
        void
    CLOSE_thread(void);
        void
    REFRACTORY_thread(void);*/
        /*!
         * \brief Called when a timing period is over;
         * sends out Open, Close, Inactivate events
         */
        void
    TIMING_thread(void);
   struct
   {    sc_core::sc_event
     Open,  // Begin computing
     Close, // End computing
     Inactivate, // inactivate computing unit
     Timing // Give membrane-wide timing signals
     ;
    }EVENT_MEMBRANE; //< These events are handled at gridpoint level

   scAbstractPatch* ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM= cm_Head)
   { return dynamic_cast<scAbstractPatch*>(AbstractTopology::ByClusterMember_Get(CN,CM));}
   scAbstractPatch* ByClusterPointer_Get(scAbstractPatch* P, ClusterNeighbor CM = cm_Head)
   { return dynamic_cast<scAbstractPatch*>(AbstractTopology::ByClusterPointer_Get(P,CM));}
//   void State_Set(int16_t i, int16_t j, MembranePatchState S)
//    {   mPatchGrid[i][j]->State_Set(S); }
   void State_Set(MembranePatchState S){ assert(S<mpt_Max); mMPS = S;}
protected:
      // The SAME physical points are stored in different structures
//      vector<vector<scAbstractPatch*> >  /// The gridpoints form a 2-dim grid, addressable by their index
//  mPatchGrid;
      MembranePatchState
      mMPS; // A phantom state

};// of class scAbstractMembrane
/** @}*/

#endif // SCABSTRACTMEMBRANE_H
