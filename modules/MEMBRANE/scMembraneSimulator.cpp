/** @file scMembraneSimulator.cpp
 *  @brief Function prototypes for the  scProcessor simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "Stuff.h"
#include "Project.h"
#include "HWConfig.h"
#include "scAbstractPatch.h"
#include "scMembraneSimulator.h"
#include "scAbstractMembrane.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING
//#define MAKE_TIME_BENCHMARKING
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scqSimulator.h"
#include "scAbstractMembrane.h"


extern bool UNIT_TESTING;	// Whether in course of unit testing
//qQDir dir;
// This is the logging facility provided by Qt.
//
// http://www.qtcentre.org/threads/19534-redirect-qDebug()-to-file
//
// qDebug is disabled in release mode
// http://beyondrelational.com/modules/2/blogs/79/Posts/19245/how-to-put-logging-in-c-application-using-qt50.aspx

/*!
 * @class scMembraneSimulator
 * @brief The equivalent of the electronically implemented simulator
 * The simulator manipulates \see a neuron membrane
 */

/*!
 * @brief	Creates an scMembraneProcessor
 * 
 *  * @verbatim
 *  |scSimulator
 *  |../scMembraneSimulator
 * @endverbatim
 */

scMembraneSimulator::scMembraneSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone)
    : scqSimulator( nm, argc, argv)
{
    msMembrane = new scAbstractMembrane("Membrane", NULL, true);
#ifdef newdef
     // This file will be sought in the system, user and project directories, in this order

    if(StandAlone)
    {
        msProcessor = new MembraneProcessor("Proc", mSpecials, true);
        /// Read the setting files at different levels; most not when we have a processor
//        readSettings();
  //      SetupHierarchies();
    }
    // As long as nothing more than sending out event in the derived simulator class
     SC_THREAD(SSTART_thread);
        sensitive << EVENT_SIMULATOR.START;
#endif //newdef
 }


scMembraneSimulator::~scMembraneSimulator()
{
    std::cerr << simulation_name << " MembraneSimulator total time "
              << m_sum_time.count()/1000/1000 << " msecs" << std::endl;

}

    void scMembraneSimulator::
Setup(void)  // Belongs to the constructor
{
}

string scMembraneSimulator::
PrologText_Get(void)
{
ostringstream oss;
string SG = "Simulator";
//oss  << '@' << StringOfTime_Get()  <<": " << SG.c_str();
 oss << '%';
//    while(oss.str().size()<24) oss << ' ';
return oss.str();
}


void scMembraneSimulator::
    writeSettings(void)
    {}
