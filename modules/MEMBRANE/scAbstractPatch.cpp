/** @file scAbstractPatch.cpp
 *  @brief Function prototypes for the SystemC based EMPA simulator
 *  Provides base for the more concrete cores
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

#include "scAbstractPatch.h"
#include "scAbstractMembrane.h"
class MembraneProcessor;

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
#include "GridPoint.h"
extern bool OBJECT_FILE_PRINTED;		// If the object file already printed
extern bool UNIT_TESTING;	// Whether in course of unit testing
//extern NamedEnum_struct MessageTypes[];

scAbstractPatch::scAbstractPatch(sc_core::sc_module_name nm,
                   scAbstractMembrane* Proc
                   , const GridPoint GP
                   , bool StandAlone
                   ):    GridPoint( GP), sc_core::sc_module( nm)
    ,mMembrane(Proc) // the 'membrane processor'
    ,mMPS(mpt_NotInUse)   // By default, the patch is closed
    ,mPotential(RESTING_POTENTIAL)// The present potential value
   ,mLastPotential(RESTING_POTENTIAL) // The previous potential value
   ,mNeighborContributions(0)   // Which neighbor contributed with charge
   ,mFutureCharge(0)    // Charge collected for the next cycle
   ,mTotalCharge(0) //
{
    Reset();    // All core attributes, maybe per thread
    SC_METHOD(ACTION_method);   // All patch actions are handled
        sensitive //<< mMembrane->EVENT_MEMBRANE.Open
                  //<< mMembrane->EVENT_MEMBRANE.Inactivate
                  //<< mMembrane->EVENT_MEMBRANE.Close
                  << EVENT_PATCH.PatchOn;
    dont_initialize();

}// of AbstractCore::AbstractCore

    void scAbstractPatch::
Reset(void)
{
    //GridPoint::Reset();
#ifdef MEASURE_PERFORMANCE
    mInstructionCount = 0;
    mMetaInstructionCount= 0;
    mInstructionTime = sc_time(0,SC_NS);
    mMetaInstructionTime = sc_time(0,SC_NS);
    mMemoryTime = sc_time(0,SC_NS);
    mFetchTime = sc_time(0,SC_NS);
    mWaitTime = sc_time(0,SC_NS);
#endif //MEASURE_PERFORMANCE

}
    scAbstractPatch::
~scAbstractPatch()
{
}//scAbstractPatch::~scAbstractPatch

    /*!
     * \brief scAbstractPatch::ACTION_method
     *
     * The only method where action happens.
     * Initially, the patch is closed.
     * Initially, a patch is sensitive to the mMembrane->EVENT_MEMBRANE.Open
     * After being opened, it sets its sensitivity to mMembrane->EVENT_MEMBRANE.Inactivate
     * After being inactivated, it sets its sensitivity to mMembrane->EVENT_MEMBRANE.Close
     * After refractory, it sets its sensitivity to the mMembrane->EVENT_MEMBRANE.Open
     * (between the above actions, a membrane-dictated time passes,
     * see scAbstractMembrane#TIMING_thread)
     *
     *
     *
     * If a charge from the immediate neighbors' transversal current
     * (or longitudinal current from a synaptic or a hyllock-ial input)
     * arrives, the patch gets to state MembranePatchState#mps_Opened
     * (the 'Open' operation can be repeated by the different partners
     * within the time slot; only the 1st one has its effect).
     *  MembranePatchState#mpt_NotInUse

     * If scAbstractPatch#mMPS is open,
     */
    void scAbstractPatch::
ACTION_method(void)
{
    assert( mpt_Denied != mMPS); // A denied patch must not be used
//    if (mpt_NotInUse == mMPS) return; // Neglect the event if not in use
    scAbstractPatch* AP;
    ClusterNeighbor N = cm_North;
    switch(mMPS)
    {
        // The patch is ready to work (was in use)
        case mpt_Closed:
            mMPS = mpt_Opened;
            next_trigger(mMembrane->EVENT_MEMBRANE.Inactivate);    // Wait until finished
            cout << "[" << sc_time_stamp() << "]: " << StringOfClusterAddressName_Get() << " was closed, is opened" << endl;
            HandleInputs(0);
        break;
        // The patch was open; now the refractory period follows
        case mpt_Opened:
            mMPS = mpt_Inactivated; // The next state
            // Now close all neighboring patches which were open
            while(AP = NextActiveNeighbor_Get(N))
            {
                if(mpt_Opened==AP->mMPS)
                {   // It was open, now inactivate
                    AP->State_Set(mpt_Inactivated);
                }
            }
        // Set trigger to EVENT_MEMBRANE.Close
            next_trigger(mMembrane->EVENT_MEMBRANE.Close);    // Wait until finished
            cout << "[" << sc_time_stamp() << "]: " << StringOfClusterAddressName_Get() << " was open, now refractory follows" << endl;
        break;

            // The patch was taken out from the work
        case mpt_NotInUse:
        // The patch was inactivated, now make it ready to work
        case mpt_Inactivated:
            mMPS = mpt_Closed;  // The refractory period is over
            // Define here which event follows
            // If there was no input in this period, wait EVENT_PATCH.PatchOn
            // mMPS = mpt_NotInUse:
            // next_trigger(EVENT_PATCH.PatchOn);
            // and set state mpt_NotInUse,
            // otherwise wait for EVENT_MEMBRANE.Open
            next_trigger(mMembrane->EVENT_MEMBRANE.Open);    // Wait until finished
            cout << "[" << sc_time_stamp() << "]: " << StringOfClusterAddressName_Get() << " refractory finished, gets closed" << endl;
        break;
    default:
        break;
    }
}

// Find the next neighbor which can participate
    /*!
 * \brief scAbstractPatch::NextActiveNeighbor_Find
 * \param N index of actual cluster neighbor
 * \return pointer to the patch, or zero if no more
 */
scAbstractPatch* scAbstractPatch::NextActiveNeighbor_Get(ClusterNeighbor &NN)
{
    ClusterAddress_t CA = ClusterAddress_Get();
    if(NN>cm_NW) return (scAbstractPatch*) nullptr; // The neighbors are over
    scAbstractPatch* AP = (scAbstractPatch*) nullptr;
    for(ClusterNeighbor N = NN; (NN <= cm_NW); N = ClusterNeighbor((int)N + 1))
    {
        NN =(ClusterNeighbor) ((int) NN + 1);
        AP = mMembrane->ByClusterPointer_Get(this, N);
        if(AP) // The point exists, we may continue
        switch(AP->mMPS)
        {
            case mpt_Denied:
                 // Needs special care, maybe: we surely cannot forward in this direction

 //                break;
            case mpt_Inactivated:
                 // That patch is relaxing, cannot forward that way
//                 break;
            case mpt_NotInUse:
//                break;
            case mpt_Closed:
                AP =  (scAbstractPatch*) nullptr;
                break;
            case mpt_Opened:
                break;
        }
        if(AP) return AP;   // The patch is active
    }
    return (scAbstractPatch*) nullptr;
}
    // Calculate the number of neighbors who receive the distributed potential
int scAbstractPatch::HandleInputs(int32_t Function)
{
    int TotalNeighbors = 0;
    int ChargeContribution; int TotalCharge = 1000;
    scAbstractPatch* AP;
    ClusterNeighbor N = cm_North;
    while(NextActiveNeighbor_Get(N))
        TotalNeighbors++;
    if(!TotalNeighbors)
        return TotalNeighbors; // We have no available neighbors
   // Now we know how many neighbors we have
    ChargeContribution = TotalCharge/TotalNeighbors;    // TODO: take care of rounding
    // Now distribute the charge between them
    N = cm_North;
    while((AP=NextActiveNeighbor_Get(N)))
    {
        AP->ChargeContribution_Add(this,ChargeContribution);
    }
 }

/*
 * The procedure is called by Donator.
 * The receiver simply stores ChargeContribution as 'future charge'
 * and remembers Donator
 */
void scAbstractPatch::ChargeContribution_Add(scAbstractPatch* Donator, int ChargeContribution)
{
    ClusterNeighbor N = mMembrane->NeighborDirectionFind(this, Donator);
    if(cm_Broadcast == N)
    { // No proper neighbor
        // TODO: handle all ways closed case
        return;
    }
    int8_t MaskBit = 1 << (int8_t)N;
    assert(!(mNeighborContributions & MaskBit)); // A neigbor can contribute only once
    mNeighborContributions |= MaskBit;
    mFutureCharge += ChargeContribution;
}


    void scAbstractPatch::
State_Change(void)
{
    switch(mMPS)
    {
        case mpt_Denied: // The patch is not biologically implemented
        assert(false);
        break;
        case mpt_Closed:  // The patch is ready to work
        mMPS = mpt_Opened;  // Now will be opened
        break;
        case mpt_Opened:  // The patch needs relaxing
        mMPS = mpt_Inactivated;
        break;
        case mpt_Inactivated:  // The patch relaxed, goes to work
        mMPS = mpt_Closed;
        break;
    }
}


    /*!
     * \brief OPEN_thread
     * This method is sensitive to mMembrane->EVENT_MEMBRANE.Open
     * Actually, it opens calculation: calculates the new membrane potential,
     * and stores it internally
     */
    void scAbstractPatch::
OPEN_method(void)
    {
    // The patch state is set to 'open' (changing the potential enabled)
        // The patch looks around and check if
        // 1/ there is a synaptic current contribution
        // 2/ the neighbours have different potential
        // 3/ calculates the new potential (and stores it internally)
        if( mpt_Denied == mMPS) return; // A denied patch must not be activated
        // To simplify calculations, the channel is not opened until some input is present
        if(mpt_Opened == mMPS)
        { // Some neighbor opened it, nothing to do
            mWasActive = false; // Assume no activity
            mCurrentCharge = 0; // No charge collected yet
        }
    }

    /*!
     * \brief check if any neighbor has a different potential
     * \return true if there is transversal current between this and any neighbor
     */
    bool scAbstractPatch::
HaveTransversalCurrent(void)
{
    ClusterAddress_t CA = ClusterAddress_Get();
    bool answer = false;
    for(ClusterNeighbor N = cm_North; (N <= cm_NW) && answer; N = ClusterNeighbor((int)N + 1))
    {
         scAbstractPatch* AP = mMembrane->ByClusterMember_Get(CA.Cluster, N);
         if(!AP) break;
         // We do have a neighbor at least at this position
         switch(AP->mMPS)
         {
            case mpt_NotInUse:
                break;
             case mpt_Denied:
                 // Needs special care, maybe: we surely cannot forward in this direction

                 break;
             case mpt_Inactivated:
                 // That patch is relaxing, cannot forward that way
                 break;
             case mpt_Closed:
                 // We are the first attempting to change it
//                 AP->mMPS = mpt_Opened;
                 // From this point, no matter who opened
             case mpt_Opened:
                 answer = AP->Potential_Get() != Potential_Get();
                 // Some other input already opened
//                 AP->mNewPotential = RESTING_POTENTIAL;
                 break;
         }
     }

}

    /*!
     * \brief CLOSE_method
     * This method is sensitive to mMembrane->EVENT_MEMBRANE.Close
     * No changes any more, the calculated new potential value replaces
     * the old potential value
     */
    void scAbstractPatch::
CLOSE_method(void)
    {
        // The patch closes its gates (no more input is effective)
        // and
        if(mpt_Closed == mMPS) return; // There was no change
        assert ( mpt_Opened != mMPS);   //

        // OK, we are closing an open channel
        if(!mNeighborContributions)
        {
            // This patch was inactive during the cycle, take it out of the game
            State_Set(mpt_Inactivated);
        }
    }


    /*!
     * \brief REFRACTORY_method
     * This method is sensitive to mMembrane->EVENT_MEMBRANE.Refractory
     */
    void scAbstractPatch::
INACTIVATE_method(void)
{
    mNeighborContributions = 0; // Prepare for the next time slot
    mTotalCharge += mFutureCharge;
    mFutureCharge = 0;
    mThisPotential = RESTING_POTENTIAL + mTotalCharge/PATCH_CAPACITY;
}

