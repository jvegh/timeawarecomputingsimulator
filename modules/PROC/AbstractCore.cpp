/** @file AbstractCore.cpp
 *  @brief Function prototypes for the SystemC based EMPA simulator
 *  Provides base for the more concrete cores
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

#include "AbstractCore.h"
#include "AbstractProcessor.h"

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
#include "scGridPoint.h"
#include <vector>
#if 0
NamedEnum_struct meta_table[16+1] =
{
    {"Q_TERM",      Q_TERM},
    {"Q_WAIT",      Q_WAIT},
    {"Q_IWAIT",     Q_IWAIT},
    {"Q_ALLOC",     Q_ALLOC},
    {"Q_CREATE",    Q_CREATE},
    {"Q_CREATER",   Q_CREATER},
    {"Q_CREATET",   Q_CREATET},
    {"Q_CREATEF",   Q_CREATEF},
    {"Q_CALL",      Q_CALL},
    {"Q_MUTEX",     Q_MUTEX},
    {"----",        Q_ERR},
    {"----",        Q_ERR},
    {"----",        Q_ERR},
    {"----",        Q_ERR},
    {"Q_HALT",      Q_HALT},
    {"Q_INT",       Q_INT},
    {"----",        Q_ERR}
};
#endif //0

extern bool OBJECT_FILE_PRINTED;		// If the object file already printed
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];

AbstractCore::AbstractCore(sc_core::sc_module_name nm,
                   AbstractProcessor* Proc
                   , const GridPoint GP
                   , bool StandAlone
                   ):    scGridPoint(nm, Proc, GP, false)
//    ,msVectorMode(cvm_None)
{
/*
    SC_THREAD(EXECUTEMETA_thread)
        sensitive << EVENT_CORE.EXECUTEMETA;
    SC_THREAD(QHALT_thread);
       sensitive << EVENT_CORE.QHALT;
    SC_THREAD(QCREATE_thread);
       sensitive << EVENT_CORE.QCREATE;
    SC_THREAD(QTERM_thread);
        sensitive << EVENT_CORE.QTERM;
    SC_THREAD(QWAIT_thread);
        sensitive << EVENT_CORE.QWAIT;
    SC_THREAD(QWAITMETA_thread);
        sensitive << EVENT_CORE.QWAITMETA;
    SC_THREAD(QCREATEMETA_thread);
        sensitive << EVENT_CORE.QCREATEMETA;
    SC_THREAD(NEXT_thread);
        sensitive << EVENT_CORE.NEXT;
    SC_THREAD(FETCH_thread);
        sensitive << EVENT_CORE.FETCH;
    SC_THREAD(QCALL_thread);
        sensitive << EVENT_CORE.QCALL;
    SC_THREAD(QMUTEX_thread);
        sensitive << EVENT_CORE.QMUTEX;
    SC_THREAD(QALLOC_thread);
        sensitive << EVENT_CORE.QALLOC;
*/
    Reset();    // All core attributes, maybe per thread
// Here scHThread creation remains the same,the scGridPpoint makes it specific
}// of AbstractCore::AbstractCore

    void AbstractCore::
Reset(void)
{
    scGridPoint::Reset();
#ifdef MEASURE_PERFORMANCE
    mInstructionCount = 0;
    mMetaInstructionCount= 0;
    mInstructionTime = sc_time(0,SC_NS);
    mMetaInstructionTime = sc_time(0,SC_NS);
    mMemoryTime = sc_time(0,SC_NS);
    mFetchTime = sc_time(0,SC_NS);
    mWaitTime = sc_time(0,SC_NS);
#endif //MEASURE_PERFORMANCE

}
    AbstractCore::
~AbstractCore()
{
}//AbstractCore::~AbstractCore

    AbstractProcessor* AbstractCore::
Processor_Get(void)
{        return dynamic_cast<AbstractProcessor*>(scGridPoint::Processor_Get());}


/*!
   * \brief AbstractCore::NEXT_thread
   * runs when a next instruction is due to process
   * It handles prefetching, suspending operation,
   * handles execution of meta instructions and conventional instructions
   *
   * The idea of HW thread handling (not yet implemented)
   * The cores have a 'one-hot' mask where the bits identify the HW threads.
   * When a QT gets allocated, the corresponding in HWthread.InUse is set,
   * HWthread.ReadyToFetch is set and HWthread.ReadyToExecute is cleared.
   * When the FETCH_thread or NEXT_thread starts, first they find out which
   * of the threads can be operated: they AND the HWthread.InUse with
   * HWthread.ReadyToFetch and HWthread.ReadyToExecute, respectively.
   * The set bits mean ready to operate HW threads, the simple circular scheduler
   * selects one of them and makes the operation. A limiting resource is the
   * processing capacity, but the memory is much slower, i.e. multiple utilization
   * is possible.
   * The other limiting resource is how much thread can be served by the memory.
   */
void AbstractCore::
NEXT_thread(void)
{
/*!!    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.NEXT");
        wait(EVENT_CORE.NEXT);
        DEBUG_EVENT_OBJECT("RCVD EVENT_CORE.NEXT");

    // The currect instruction  may be fetched or non fetched
    FinishPrefetching(); // Make sure fetching finished
//    bool IsMetaInstruction = IsMetaInstructionFetched();

    // The previous instruction is finished, the current one not yet started
    HandleSuspending(); // Suspend if the simulator asks so; solved at scGridPoint level

  // Now we surely have a fetched instruction: execute it

  // Instruction performance is handled separatedly for conventional and meta instructions
//!!    sc_time ExecDelay = sc_time_stamp();

//!!    if(IsMetaInstructionFetched())
 if(1)//!!
        HandleMetaInstruction();
    else
        HandleConventionalInstruction();
    TerminateExecution();

    //    performance->ExecTimeAdd(sc_time_stamp()-ExecDelay);
    }
    */
}//AbstractCore::   NEXT_thread


void AbstractCore::
FETCH_thread(void)
{
/*!!    while(true)
    {
        DEBUG_FETCH_EVENT("WAIT EVENT_CORE.FETCH");
        wait(EVENT_CORE.FETCH);
        DEBUG_FETCH_EVENT("RECVD EVENT_CORE.FETCH");
//??        DEBUG_PRINT_WITH_SOURCE("Begin fetching line ");
        fetch.ExecutionTime = sc_time_stamp();  // Remember the beginning of prefetching
        fetch.Pending = true; // Prefetching will be started
        fetch.Valid = false;  // No valid prefetched instruction
        LOG_TRACE_FETCH_SOURCE("Fetching instruction");
    //?? The instruction is fetched to avoid timing in the doXXX routine
//!!        if(!doFetchInstruction())
        {
            LOG_CRITICAL("Instruction fetch failed; wrong code");
            break; // Illegal code fetched; fatal
        }
        wait(SCTIME_CLOCKTIME);    // Imitate decoding
        PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);
    // The fetch time is included in readInstrMem
    // Flags are cleared in PrepareNextInstruction
        fetch.Pending = false; // Prefetching finished
        fetch.Valid = true;  // This core has valid prefetched instruction
//!!        Plotter_Get()->PlotQTfetch(ID_Get(),mFetchDelayTime,FetchPC, FetchStage.ExecutionTime.value()); // Plot fetch mark
        EVENT_CORE.FETCHEXECUTED.notify(SC_ZERO_TIME);
        DEBUG_FETCH_EVENT("SENT FETCHEXECUTED_event");
    }
    */
}

bool AbstractCore::
doFetchInstruction(scHThread* H)
{
    DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.MakeFetch") ;
    wait(100,SC_PS);
    DEBUG_EVENT_OBJECT("SENTD EVENT_GRID.FetchReady") ;
    EVENT_GRID.FetchReady.notify();
    return true;
}


bool AbstractCore::
doExecuteInstruction(scHThread* H)
{
    DEBUG_EVENT_OBJECT("RCVD EVENT_GRID.MakeFetch") ;
    wait(100,SC_PS);
    DEBUG_EVENT_OBJECT("SENTD EVENT_GRID.FetchReady") ;
    EVENT_GRID.FetchReady.notify();
    return true;
}


/*!
 * \brief AbstractCore::FinishPrefetching
 * At the beginning of executing NEXT_thread, when this function called,
 * the next instruction the prefetch operation may be
 * 1) already finished,
 * 2) still pending
 * 3) not yet started
 * When this subroutine returns, the prefetch operation is surely finished
 * and the core is ready to execute the finished operation
 */

void AbstractCore::
FinishPrefetching(void)
{
 /*!!   sc_time FetchTime, FetchWaitTime;
    if(mTimedOut)
    {// During execution a timout occurred: must kill the execution of the QT
        doKillQT();
        mTimedOut = false;
    }
    if(fetch.Valid)
    {   // The prefetch was started in the FETCH_thread
        FetchTime = sc_time_stamp()-fetch.ExecutionTime;
        FetchWaitTime = sc_time(SC_ZERO_TIME);
    }
    else
    { //This core has NO valid prefetched instruction
        if(!fetch.Pending)
        { // Prefetching even not started, initiate
            fetch.Pending = true; // Prefetching started
            exec.PC = fetch.PC;
            EVENT_CORE.FETCH.notify();
            DEBUG_EVENT_OBJECT("SENT FETCH_event (emergency)");
        }
        DEBUG_EVENT_OBJECT("WAIT FETCHEXECUTED_event");
        FetchWaitTime = sc_time_stamp();
        wait(EVENT_CORE.FETCHEXECUTED);// Be sure to wait before executing
        DEBUG_EVENT_OBJECT("RECVD  FETCHEXECUTED_event");
        FetchWaitTime = sc_time_stamp() - FetchWaitTime;
        FetchTime = sc_time_stamp()-fetch.ExecutionTime;
        PERFORMANCE_ADDFETCHTIME(FetchTime);
        PERFORMANCE_ADDWAITTIME(FetchWaitTime);
      DEBUG_EVENT_OBJECT("Proceeding after fetching delay " << sc_time_to_nsec_Get(2,8,FetchWaitTime) << " ns");
//          PLOT_FETCH_WAIT_BALL(sc_time_stamp()-FetchWaitTime,FetchWaitTime);
//!!          if(WT!= FetchStage.ExecutionTime.value())
//            Plotter_Get()->PlotQTfetchwait(ID_Get(),mFetchDelayTime,PC_Get(),WT);
    }
//PLOT_FETCH_BALL(fetch.ExecutionTime, FetchTime//-FetchWaitTime
                //);
    PrepareNextInstruction();
    */
}


/*!
* \brief This routine is called when the instruction is fetched (including timing)
*/

void AbstractCore::
PrepareNextInstruction(void)
{
/*!!    fetch.Pending = false; // Prefetching finished
    fetch.Valid = false;  // This core has no valid prefetched instruction
    // Now ready with the conventional instruction fecthing
    // See if a meta-instruction is fetched
//!!    if(IsMetaInstructionFetched())
    //    if(fetch.Stage.hi0 ==  (itype_t) I_QT)
    {
        // It is a morphing instruction for the core, just call it with the subtype
        // This condition is noticed in the prefetch cycle
//        fetch.Stage.CoreStatus = STAT_META;
        msMeta.CA = ClusterAddress_Get();
        msMeta.PC = fetchPC_Get();
        msMeta.Code = fetch.Stage.FunctCode;
        msMeta.Address = fetch.Stage.immediate;
        msMeta.Param = fetch.Stage.byte1;
        msMeta.ExecutionTime = fetch.ExecutionTime;
    }
    exec.Stage = fetch.Stage; // Take over the fetched stage
    exec.PC = fetch.PC;     // The last fetched intruction will be executed
    fetch.PC = fetch.NewPC; // The next instruction will be fetched
    */
}

/*!
 * \brief AbstractCore::doExecuteMetaInstruction
 *
 * Called when the core receives a metacommand from the FIFO,
 * i.e. it is authorized by the processor to execute it.
 * This
 */

    void AbstractCore::
doExecuteMetaInstruction()
{
    // Has a local copy of the meta-instruction; created by the processor
//!!    LOG_TRACE_SOURCE("Executing '" + MetaInstructionName_Get() + "'");
/*    switch(msMeta.Code)
    {
         case Q_ALLOC:
           EVENT_CORE.QALLOC.notify(SC_ZERO_TIME);
           DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QALLOC");
           break;
         case Q_CALL:
           EVENT_CORE.QCALL.notify(SC_ZERO_TIME);
           DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.QCALL");
           break;
         case Q_MUTEX:
           EVENT_CORE.QMUTEX.notify(SC_ZERO_TIME);
           DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QMUTEX");
         break;
        case Q_CREATE:
          case Q_CREATET:
          case Q_CREATEF:
          case Q_CREATER:
            EVENT_CORE.QCREATEMETA.notify(SC_ZERO_TIME);
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QCREATEMETA '" << MetaInstructionName_Get() << "'" );
            break;
        case Q_WAIT:
            EVENT_CORE.QWAITMETA.notify(SC_ZERO_TIME);
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QWAITMETA" );
            break;
        case Q_HALT:
            // Check if it is legal to HALT
            if(!Parent_Get())
            {
                EVENT_CORE.QHALT.notify(SC_ZERO_TIME);
                DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.QHALT" );
                break;
            }
            // We have a parent, i.e. we cannot halt the processor
            // Rather, convert it to Q_TERM, with a warning
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QTERM for '" << MetaInstructionName_Get() << "'");
            LOG_TRACE_SOURCE("Q_HALT converted to Q_TERM");
            // Continue as if it were Q_TERM
        case Q_TERM:
            EVENT_CORE.QTERM.notify(SC_ZERO_TIME);
            DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.QTERM" );
            break;
        default:
          {
            LOG_CRITICAL("Unknown metaevent ");// <<  MetaInstructionName_Get() << " found");
          }
    }
    */
}

    /*!
     * \brief AbstractCore::QALLOC_thread
     */
    void AbstractCore::
QALLOC_thread(void)
{
/*    while(true)
    {
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QALLOC");
        wait(EVENT_CORE.QALLOC);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QALLOC");
 //!!       Processor_Get()->doQALLOC(this, (core_cooperationmode_t) exec.Stage.cval, (reg_id_t)exec.Stage.lo1);
        EVENT_CORE.METAEXECUTED.notify(SC_ZERO_TIME);   // The QT terminated, but the simulator waits for the event
        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.METAEXECUTED '" << MetaInstructionName_Get() << "'");
    }
    */
}

    /*!
     * \brief AbstractCore::QCALL_thread
     */
    void AbstractCore::
QCALL_thread(void)
{
//    DEBUG_PRINT("Core::QCALL_thread started");
    while(true)
    {
//!!        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QCALL");
        wait(EVENT_CORE.QCALL);
//!!        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QCALL");
        // Fetch will overwrite the meta parameters
/*        MetaEvent_Transfer_Type OldMeta = msMeta;
        SC_ADDRESS_TYPE ChildAddress = msMeta.Address;

        // Save the address for the continuation
  //      SC_ADDRESS_TYPE ContinuePC = fetch.PC;    // We will continue here, just creating a new thread 'en passant'
  //      DEBUG_EVENT_OBJECT("Program counter saved: 0x" << hex << ContinuePC);
        // Fetching the next instruction in the parent may be pending, wait it
        if(fetch.Pending)
        {
          DEBUG_EVENT_OBJECT("WAIT  FETCHEXECUTED_event in QCALL");
          wait(FETCHEXECUTED_event);
          DEBUG_EVENT_OBJECT("RECVD FETCHEXECUTED_event in QCALL");
        }
        // Now the actaully fetched instruction stage is finalizeded
        MetaEvent_Transfer_Type SavedMeta = msMeta;   // This holds the new fetched command
        VirtualCoreFetch_t SavedFetch = fetch;    // Save the fetched state for later use
        // Now convert the thread to a QCreate
        string FakePrompt = PrologText_Get(exec.PC-QCreate_Size);
        exec.PC = msMeta.Address; // Imitate that the call was from there: The address of QT is given as the 1st argument
        LOG_TRACE("Execute a fake 'QCREATE' for '" + string(meta_table[msMeta.Code].name) + "'@" +FakePrompt);
        msMeta.Code = Q_CREATE;
  //      msMeta.Param = msMeta.Address;    //
        msMeta.Address = ChildAddress;  // from the previous address
        fetch.Pending = false;
        fetch.Valid = false;
        fetch.PC = ChildAddress;
        sc_time FetchDelay = sc_time_stamp();
        FETCH_event.notify();     // Get the QCreate meta-instruction
        DEBUG_EVENT_OBJECT("WAIT  FETCHEXECUTED_event in QCALL for the fake QCREATE");
        wait(FETCHEXECUTED_event);
        DEBUG_EVENT_OBJECT("RECVD FETCHEXECUTED_event in QCALL for the fake QCREATE");
        PLOT_FETCH_BALL(FetchDelay,SCTIME_SV);
        // Where fetched from, must be a QCreate, with the same backlink parameter
        if(msMeta.Code != Q_CREATE)
        {
             DEBUG_PRINT_WITH_SOURCE("Called '" + string(meta_table[msMeta.Code].name) + "' rather than 'QCreate'");
        }
        if(OldMeta.Param != msMeta.Param)
        {
            char buffer1 [20], buffer2[20];
            sprintf (buffer1, "%x", msMeta.Param);
            sprintf (buffer2, "%x", OldMeta.Param);
            msMeta.Param = OldMeta.Param;
                DEBUG_PRINT_WITH_SOURCE("The callee 'QCreate' parameter 0x" + string(buffer1) +
                                        " differs from that of the caller's 0x" + string(buffer2));
        }
        DEBUG_EVENT_OBJECT("SENT  QCREATE_event in 'QCALL'");
        msMeta.Address = fetch.Stage.cval;
        msMeta.PC = SavedMeta.Address;
        QCREATE_event.notify(SC_ZERO_TIME);  // Issue a 'QCreate' event
        DEBUG_EVENT_OBJECT("WAIT  QCREATE_event in 'QCALL'"); ///??
        wait(QCREATEEXECUTED_event); // Wait until executed
        DEBUG_EVENT_OBJECT("RECVD QCREATE_event in 'QCALL'");
        PLOT_FETCH_BALL(sc_time_stamp(),SCTIME_SV);
        METAEXECUTED_event.notify();   // This metainstruction terminated
        DEBUG_EVENT_OBJECT("SENT  METAEXECUTED_event '" << meta_table[msMeta.Code].name << "'");
        // Prepare for continuation
        exec.PC = ChildAddress;
        PLOT_META_BALL(sc_time_stamp(),SCTIME_SV);
  //      PLOT_QT_REGISTER_MASK_OUT(this, 0x001F);

        // Now the called QT is running, restore fetch and execute PCs
        fetch = SavedFetch;
        fetch.Valid = false; fetch.Pending = false;
        msMeta = SavedMeta;
        NEXT_event.notify(SC_ZERO_TIME); // Continue with the instruction following the QCALL
        DEBUG_EVENT_OBJECT("SENT  NEXT_event");
  //      QCALLEXECUTED_event.notify();
  //      LOG_INFO(PrologText_Get().c_str()  << "Returned after 'QCall' " );
    */}
}

    /*!
     * \brief AbstractCore::QMUTEX_thread
     * The QMutex is essentially a conditional QCall:
     * It waits if there exists a QT with the requested offset,
     * then makes a QCall.
     * If no such QT exists, the QWait returns immediately
     */
    void AbstractCore::
QMUTEX_thread(void)
{
//    DEBUG_PRINT_WITH_SOURCE("QMUTEX_thread started");
    while(true)
    {
//!!        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QMUTEX");
        wait(EVENT_CORE.QMUTEX);
//!!        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QMUTEX_event");
    /*    if(doWaitMask_Get(msMeta.Address))
        { // We do have running QT at that offset; retry later
          // First, if the fetch is not yet finished, wait until it terminates
          // in order to have well-defined fetch stage
            msWaitBegin = sc_time_stamp();
          if(fetch.Pending)
            { DEBUG_EVENT_OBJECT("WAIT  FETCHEXECUTED_event in QCALL");
              wait(FETCHEXECUTED_event);
              DEBUG_EVENT_OBJECT("RECVD FETCHEXECUTED_event in QCALL");
            }
            // For this goal, save the meta-instruction and the fetch stage
            MetaEvent_Transfer_Type SavedMeta = msMeta;
            VirtualCoreFetch_t SavedFetch = fetch;
            // Second, enable to process events, if any, from other cores
            if(msProcessor->MetaEvents->num_available())
            {
              msProcessor->MetaEvents->write_event.notify(SCTIME_SV);
              DEBUG_EVENT_OBJECT("SENT  MetaEvents->write_event before retry");
            }
            DEBUG_EVENT_OBJECT("WAIT QTERM_event from the processor");
            wait(Processor_Get()->QTERM_event); // Wait until some core terminates
            DEBUG_EVENT_OBJECT("RECVD QTERM_event from the processor") ;
            msMeta = SavedMeta;
            fetch = SavedFetch;
            HandleEndOfWaiting();
        }
        // The mutex is free, just continue
        string FakePrompt = PrologText_Get(exec.PC-QCreate_Size);
        LOG_TRACE("Execute a fake 'QCALL' for '" + string(meta_table[msMeta.Code].name) + "'@" +FakePrompt);
        DEBUG_EVENT_OBJECT("SENT  QCALL_event in QMUTEX");
        QCALL_event.notify(SC_ZERO_TIME);
        PLOT_FETCH_BALL(sc_time_stamp(),SCTIME_SV);
    */
    }
}



/*!
 * \brief AbstractCore::EXECUTEMETA_thread
 *
 * This thread is called when a meta-instruction found.
 * The execution takes place in two steps. In the first step, the core writes
 * the meta-instruction in the MetaEvent_fifo AbstractProcessor::MetaEvents,
 * then the operating mode changes to 'Meta' mode. The FIFO is priority-ordered,
 * and the contents are processed by the AbstractProcessor one-by-one.
 * The event of lower priority may be executed at later time is some higher
 * priority events are waiting in the FIFO.
 * The AbstractCore waits until the AbstractProcessor sends and event
 * EVENT_CORE#EXECUTEMETA.
 *
 *
 */
void AbstractCore::
EXECUTEMETA_thread(void)
{
    while(true)
    {
/*        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.EXECUTEMETA");
        wait(EVENT_CORE.EXECUTEMETA);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.EXECUTEMETA '"+ MetaInstructionName_Get() + "'");
        //    PLOT_META_BALL(sc_time_stamp(),SCTIME_SV);
        wait(SCTIME_CLOCKTIME);
        PERFORMANCE_ADDWAITTIME(SCTIME_CLOCKTIME);
        doExecuteMetaInstruction();
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.METAEXECUTED after '"+ MetaInstructionName_Get() + "'");
        wait(EVENT_CORE.METAEXECUTED);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.METAEXECUTED after '"+ MetaInstructionName_Get() + "'");

        // Now we are after the execution of the meta-instruction
        // The processor may use global resources, so it executes one metaevent at a time
        // For a busy processor it may happen that more events are still in the queue
        // Tell the processor this metainstruction terminated, may take the next waiting one

        if(Processor_Get()->MetaEvents->num_available())
        {   // More metaevents are waiting, just activate processing
            // The events are in the FIFO of the processor for another core!
            Processor_Get()->MetaEvents->write_event.notify(SCTIME_CLOCKTIME);
            DEBUG_EVENT_OBJECT("SENT  METATRIGGER_event");
       }
*/     }
}//AbstractCore::ExecuteMeta


/*!
  * \brief scCore::QCREATE_thread
  *
  * Called when a new QT is to be created, in one of the cases
  * 1) booting: no more core left, but no HALT follows
  * 2) A kind of QCreateX metacommand issued
  * 3) in another metacommand an inplicite QCreateX is used
  */

    void AbstractCore::
QCREATE_thread(void)
{
    while(true)
    {
#if 0 //!!
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QCREATE");
        wait(EVENT_CORE.QCREATE);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QCREATE");
        CorePreference_t CPT = cpt_Member;
/*        if(!Parent_Get() || !Parent_Get()->Parent_Get()
                         || (Q_CREATET == msMeta.Code) )
            CPT = cpt_Head; // Prefer cluster heads if allocated by the processor or with higher computing demand
        else
            CPT = cpt_Member;
        if(msMeta.Code != Q_CREATER)
            msMeta.Param |= 0x100;  // Mark that is is a mask rather than register code
*/        SC_CORE_MASK_TYPE CloneMask = 0x7;
        SC_CORE_MASK_TYPE BackCloneMask = 0x01;
        AbstractCore* MyCore1 = Processor_Get()->doQCREATE(this, (AbstractCore*)NULL,
                      CloneMask, BackCloneMask,          CPT); // Now it is the child core

        if(MyCore1)
        {   // We have a new core and use it for processing
            // The parent is fetched and can proceed
//!!         LOG_TRACE_SOURCE(string("QT created with ").append(string(RegisterContentStrings_Get(msMask.CloneRegisters, true))));
//!!         PLOT_QT_REGISTER_MASK_IN(MyCore1, CLONE_REGISTER_MASK);
            EVENT_CORE.NEXT.notify(SC_ZERO_TIME); // Let this (parent) QT know that meta-instruction is terminated
            MyCore1->EVENT_CORE.NEXT.notify(SC_ZERO_TIME); // Start processing in the child
            EVENT_CORE.QCREATEEXECUTED.notify(SC_ZERO_TIME); // Let this (parent) QT know that meta-instruction is terminated
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QCREATEEXECUTED" );
 /*        if(MyCore1)
         { //We have a new core and use it for processing
                 // Now both the child and parent have new PC, no prefetch is walid
                 // Trigger executing in  both cores
                 msPrefetchValid = false; msPrefetchPending = false;
                 NEXT_event.notify(SC_ZERO_TIME); // Submit new ones
                 DEBUG_EVENT_OBJECT("SENT  NEXT_event @" << PrologText_Get().c_str());
                 if(this != MyCore1)
                 { // This is NOT the rebooting case
                   MyCore1->NEXT_event.notify(SC_ZERO_TIME); // Start its processing
                   DEBUG_EVENT_OBJECT("SENT  NEXT_event @" << MyCore1->PrologText_Get().c_str());
                 }
             QCREATEEXECUTED_event.notify(SCTIME_SV); // Let this QT know that waiting terminated
             DEBUG_EVENT_OBJECT("SENT QCREATEEXECUTED_event '" << meta_table[msMeta.Code].name << "' @" << PrologText_Get().c_str());
           }*/
      }
       else
       { // We did not find an available core, wait until the processor can provide a "reprocessed" core
           // Messages about QTERM are sent to the processor, we watch them and retry acquiering a core
         DEBUG_EVENT_OBJECT("WAIT EVENT_PROCESSOR.QTERM");
         wait(Processor_Get()->EVENT_PROCESSOR.QTERM); // Wait until some core terminates
         DEBUG_EVENT_OBJECT("RECVD EVENT_PROCESSOR.QTERM") ;
         EVENT_CORE.QCREATEMETA.notify(SC_ZERO_TIME);   // Retry
         DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QCREATE retry");
       }
 #endif //0
   }
}//AbstractCore::QCREATE_thread(void)

 void AbstractCore::
QCREATEMETA_thread(void)
{
    while(true)
    {
#if 0 //!!
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QCREATEMETA");
        wait(EVENT_CORE.QCREATEMETA);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QCREATEMETA '" + MetaInstructionName_Get() + "'");
        bool CreateAQT;
        // Make processing needed before creating the new core in vector mode
 /*       switch((qt_t)exec.Stage.FunctCode)
        {
            case Q_CREATE:
            case Q_CREATER:
            // By default a QT shall be created unconditionally
                CreateAQT = true;
                break;
            case Q_CREATET:
       // Create QT only if CAlloc DID succeded
                CreateAQT = msCooperationMode>0;
                break;
            case Q_CREATEF:
       // Create QT only if CAlloc DID NOT succed
                CreateAQT = msCooperationMode<=0;
                break;
            default:
            break;
        }
        // Now either create the new QT or skip its body
        if(CreateAQT)
        {
//      Plotter_Get()->PlotQTmeta(ID_Get(),sc_time_stamp().value(),ExecuteStage.NewPC-QCreate_Size, 1000);
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QCREATE");
            EVENT_CORE.QCREATE.notify(SC_ZERO_TIME);  // Issue an event
            DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QCREATEEXECUTED");
            wait(EVENT_CORE.QCREATEEXECUTED); // Wait until executed
            DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QCREATEEXECUTED");
            switch(msMeta.Code)
            {
                case Q_CREATE:
                case Q_CREATER:
                case Q_CREATET:
                case Q_CREATEF:
                default:
                    break;
            }
        }
        else
        { // No, no QT is to be created, but we must skip the code
//        Plotter_Get()->PlotQTmeta(ID_Get(),sc_time_stamp().value(),mSavedPC, 1000);
            doSkipQTCode();
        }
 */       // Anyhow, we are after the metainstruction terminated:
        // And continue with the new PC
        EVENT_CORE.METAEXECUTED.notify(SC_ZERO_TIME);   // This metainstruction terminated
        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.METAEXECUTED '" << MetaInstructionName_Get() << "'");
#endif //0 !!
    }
}// AbstractCore:: QCREATEMETA_thread(void)

#ifdef threads
 /*!
  * \fn AbstractCore::CreateQT
  * \brief This function is called when a "QT" message received
  */
 void AbstractCore::
doCreateQT(scIGPMessage* MyMessage)
{
     assert(!IsDenied());  // A denied core must not be used
     assert(IsAllocated());  // The core must already be allocated
     // Maybe here the low-power mode shall be switched off; wait included
     assert(!IsWaiting());
     // Now copy the message
     // During allocation, the parent-child relationship is already established,
     // Just copy register contents, PC and flags
/*!!     int32_t Mask = MyMessage->BufferContent_Get(3);
     int index = 0; int Reg = 0;
     while(Mask)
     {
         if(Mask & 1)
         {
             REGS[Reg] = MyMessage->BufferContent_Get(4+index++);
         }
         Mask /=2; Reg++;
     }
     PC_Set(MyMessage->BufferContent_Get(4+index++));
     if(FlagWordLength_Get()) ConditionCode_Set(MyMessage->BufferContent_Get(4+index++));
//        DEBUG_CONTRIBUTE_STRING(DebugRoute,"CREATED QT ",StringOfMessage_Get(MyMessage).append("\n"));

     DEBUG_EVENT_OBJECT("SENT EVENT_CORE.NEXT");
     EVENT_CORE.NEXT.notify(SCTIME_CLOCKTIME);   // Now everything OK, can start processing
*/
 }//AbstractCore::doCreateQT

#endif //#ifdef threads
/*!
 * \brief AbstractCore::QHALT_thread
 * Called when a 'Q_HALT' (or equivalently, a 'halt' received
 */
   void AbstractCore::
QHALT_thread(void)
{
     while(true)
     {
#if 0 //!!
         DEBUG_EVENT_OBJECT("WAIT  EVENT_CORE.QHALT");
        wait(EVENT_CORE.QHALT);

        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QHALT");
        EVENT_CORE.QTERM.notify();  //First terminate the own QT
        DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.QTERM" );
//        EVENT_CORE.METAEXECUTED.notify();  //The 'QHALT' logically executed
//        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.METAEXECUTED");
        // Now we must wait until the QT can terminate
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QWAITEXECUTED in QHALT" );
        // From this point on, the core may be again in the pool (i.e. not any more allocated)
        // However, the core physically exists and so do events and event handlers
        wait(EVENT_CORE.QTERMEXECUTED);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.METAEXECUTED in QHALT");
        wait(SCTIME_CLOCKTIME); // The final wait
        PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);
/*        PLOT_META_BALL(sc_time_stamp(),SCTIME_SV);
        PLOT_QT_REGISTER_MASK_OUT(this, 0x001F);
*/
/*        exec.Stage.CoreStatus = STAT_HLT;
        Processor_Get()->EVENT_PROCESSOR.HALT.notify(SCTIME_CLOCKTIME);
        DEBUG_EVENT_OBJECT("SENT EVENT_PROCESSOR.HALT");
*/        EVENT_CORE.METAEXECUTED.notify(SC_ZERO_TIME);   // The QT terminated, but the simulator waits for the event
        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.METAEXECUTED");
        // The control comes here only if we are the last core
        // AND we found a Q_HALT metainstruction
        assert(!Processor_Get()->AllocatedMask_Get());
        Processor_Get()->Halted_Set(true);  // Mark the processor as halted
#endif //
     }
} //void AbstractCore::QHALT_thread


   /*!
    * \brief AbstractCore::QTERM_thread
    * Called when a QTerm metainstruction is executed
    *
    * All code fragment must run in a QT environment
    */
   void AbstractCore::
QTERM_thread(void)
{
    while(true)
     {
#if 0 //!!
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QTERM");
        wait(EVENT_CORE.QTERM);
        DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QTERM");
        msWaitOffset = MEMORY_DEFAULT_ADDRESS;
  //       msWaitTimeout = 0;
         EVENT_CORE.QWAIT.notify();
        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QWAIT");
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QWAITEXECUTED in QTERM");
        wait(EVENT_CORE.QWAITEXECUTED); // This is an implied wait
        DEBUG_EVENT_OBJECT("RCVD QEVENT_CORE.QWAITEXECUTED in QTERM" );
 //       scCore* MyCore1 = Parent_Get(); // Remember who was the parent of the terminating core
 //!!       Plotter_Get()->PlotQTmeta(ID_Get(),sc_time_stamp().value(),PC_Get(), 1000);
        wait(SCTIME_CLOCKTIME);
        PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);

        LOG_TRACE_SOURCE(string("QT terminated with \n      ") +
                         string(RegisterContentStrings_Get(0xFF,false))
                         );
//!!        PLOT_QT_REGISTER_MASK_OUT(this, BacklinkRegisterMask_Get());
        doQTERM();   // Terminate the thread
        EVENT_CORE.QTERMEXECUTED.notify(  SCTIME_CLOCKTIME);   // The QT terminated, but the simulator waits for the event
        DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QTERMEXECUTED in 'QTERM_thread'");
//!!        PLOT_QT_FIGURE();
 //!!       Plotter_Get()->PlotQTfigure(QTID_Get(),sc_time_stamp().value()-AllocationBegin_Get() , ID_Get(), AllocationBegin_Get(), 0);
 //       ExecTime_Add((sc_time_stamp().value()-AllocationBegin_Get())/1000);

       if(!Processor_Get()->AllocatedMask_Get())
        {    // This was the last core of the processor, remember the PC
            Processor_Get()->PC_Set(PC_Get()+1 //!!
                                    //QHalt_Size
                                    );
        }
       Processor_Get()->EVENT_PROCESSOR.QTERM.notify(); // The number of core resources changed
#endif //
    }
}//void AbstractCore::QTERM_thread

   bool AbstractCore::
doQTERM() {
         ReleasePreAllocatedCores(); // Maybe some cores remained preallocated
//!!         AbstractCore* Parent = Parent_Get();
//         if(Parent)
//         Parent->CloneRegisterFileFrom(this,msMask.BackLinkRegisters,false);
   /*       BL_FromSV_Transfer_Type T;
       QT_TerminateCooperation();
       doRegisterCloneBack_read(-1,T); // Before terminating, read the output from our children
       doRegisterCloneBack_write(); // After this, send out output to our parent
       LOG_INFO(PrologText_Get().c_str() << "Terminated");
       // Now everything cleared, return the core to the pool
   */    if(1 == OnesInMask_Get(Processor_Get()->AllocatedMask_Get()))
       { // This was the last core, we are about to return it
   //        Processor_Get()->AllocationBegin_ReSet(AllocationBegin_Get());
       }
       return Deallocate();
}//AbstractCore:: doQTERM()

   /*!
    * \brief AbstractCore::ReleasePreAllocatedCores
    * Upon terminating a QT, release pre-allocated cores, if any
    */
   void AbstractCore::
ReleasePreAllocatedCores(void)
{
   SC_CORE_MASK_TYPE M = PreAllocatedMask_Get();
   if(M)
   { // The core wants to be terminated, but some others are preallocated for it
       int i = 0;
       while(M)
       {
           if(M & 1)
           {
               AbstractCore *C = Processor_Get()->ByID_Get(i);
               C->PreAllocatedBit_Set(false);
           }
           ++i; M = M >> 1;
       }
   }
}


/*!
 * \brief AbstractCore::QWAIT_thread
 *
 * Runs when a EVENT_CORE.QWAIT found (by QWAIT, QTERM, QMUTEX or QHALT)
 * (i.e. the event is implied in the execution of the mentioned meta-instructions)
 * The routine checks if this QT must wait. If so, waits until some
 * other QT terminates and then retries
 *
 * The event handling routine handles two parameters
 * i) a timeout value (multiples of SCTIME_CLOCKTIME; zero means no timing.
 * msWaitOffset must be set up in the thread issueing EVENT_CORE.QWAIT;
 * the other metaevents do not use timing)
 * ii) the address offset of the QT we are waiting for; MEMORY_DEFAULT_ADDRESS
 * means all children
 *
 */
   void AbstractCore::
QWAIT_thread(void)
{
    while(true)
    {
#if 0 //!!
        DEBUG_EVENT_OBJECT("WAIT  EVENT_CORE.QWAIT");
        wait(EVENT_CORE.QWAIT);
        DEBUG_EVENT_OBJECT("RECVD   EVENT_CORE.QWAIT");
         // A QWAIT event arrived, see if the QT can be continued

        if(doCanTerminate(msWaitOffset))
        {// We are waiting for nobody, can terminate immediately
            if(msStatus.Wait)
            { // we were waiting, calculate waiting time
                HandleEndOfWaiting();
                msStatus.Wait = false;  // We are not waiting any more
                EVENT_CORE.QWAITEXECUTED.notify(SCTIME_CLOCKTIME); // Let this QT know that waiting terminated
            }
            else
            { // No, we were not waiting, proceed immediately
                LOG_TRACE_SOURCE("Proceed without waiting");
                DEBUG_EVENT_OBJECT("Proceed without waiting");
                EVENT_CORE.QWAITEXECUTED.notify(SC_ZERO_TIME); // Let this QT know that waiting terminated
            }
            DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QWAITEXECUTED in QWAIT");
            // Anyhow: waiting terminated
//!!          // Anyhow: read latched link register contents, if any
//!!          BL_FromSV_Transfer_Type T;
//          wait(SC_ZERO_TIME); // Be sure to receive the backlinked contents
 //!!         doRegisterCloneBack_read(ExecuteStage.cval,T);
        }
        else
         {// We need to wait
            if(msStatus.Wait)
            { // We were also waiting before, nothing to do
            }
            else
            {    // We were not waiting before, so waiting begins right now
                msWaitBegin = sc_time_stamp();
                LOG_TRACE_SOURCE("Begin waiting");
                DEBUG_EVENT_OBJECT("Begin waiting");
                msStatus.Wait = true;  // We are in waiting state
            }
            wait(Processor_Get()->EVENT_PROCESSOR.QTERM); // Wait until some QT finishes and retry
            EVENT_CORE.QWAIT.notify(SCTIME_CLOCKTIME);
         }
#endif //!!
     }
}//void AbstractCore:: QWAIT_thread(void)


   /*!
   * \brief AbstractCore::QWAITMETA_thread
   * Runs when a QWait metaevent found
   */

    void AbstractCore::
QWAITMETA_thread(void)
{
    while(true)
    {
#if 0 //!!
        DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QWAITMETA");
          wait(EVENT_CORE.QWAITMETA);
          DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QWAITMETA");
          // OK, now wait until the QT in this core is also executed, but maybe with time limitation
          // The metacommand has two parameters:
          // The address of the QT (or MEMORY_DEFAULT_ADDRESS) in msMeta.Address
          // and the timeout parameter msMeta.Param (in units of SCTIME_SYSTEMTIME, defined in Config.h)
          msWaitTimeout = msMeta.Param;    //
          msWaitOffset = msMeta.Address; // The offset of the QT we are waiting for
          EVENT_CORE.QWAIT.notify();
          DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QWAIT");
          AbstractCore *C = doFindHostToProcess(msWaitOffset,Processor_Get()->AllocatedMask_Get());
          if(msWaitTimeout && C)
          {   // We have a non-zero timeout value and did find an active QT with the Offset
              // prepare for having timeout
              C->EVENT_GRID.TIMEOUT.notify(msWaitTimeout*SCTIME_CLOCKTIME);
              DEBUG_EVENT_OBJECT("WAIT  QWAITEXECUTED_event OR SYSTEM_TIMEOUT in QWAITMETA");
              wait(EVENT_CORE.QWAITEXECUTED | C->EVENT_GRID.TIMEOUT); // This is an implied wait
              wait(SC_ZERO_TIME); // Just be sure that possible system timeout executed
              if(C->mTimedOut)
              {// A timeout occurred
               // Just to be sure, cancel the event that failed to arrive
                  EVENT_CORE.QWAITEXECUTED.cancel();
                  DEBUG_EVENT_OBJECT("RECVD NO EVENT_CORE.QWAITEXECUTED in QWAITMETA_thread, TIMEOUT occurred");
              }
              else
              {// No system time changed, i.e. the QWAITEXECUTED_event arrived
                  C->EVENT_GRID.TIMEOUT.cancel();
                 //nothing special to do:
                  DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QWAITEXECUTED in QWAITMETA_thread, no timeout");
              }
              DEBUG_EVENT_OBJECT("FINSHED  QWAITMETA_thread" );
          }
          else
          {// No, it is just the immediate wait

              DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.QWAITEXECUTED in QWAITMETA_thread");
              wait(EVENT_CORE.QWAITEXECUTED); // This is an implied wait
              DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.QWAITEXECUTED in QWAITMETA_thread" );
          }
          // This core will wait, but:
          // This meta-instruction is executed, the processor can take the next metacommand;
          EVENT_CORE.QWAITEXECUTED.notify();   // The QT terminated, but the simulator waits for the event
          DEBUG_EVENT_OBJECT("SENT EVENT_CORE.QWAITEXECUTED '" << MetaInstructionName_Get() << "'");
//          PLOT_QT_FIGURE();
#endif //0
    }
}//void scCoreAbstractCore:: QWAITMETA_thread(void)

/*!
 * \brief AbstractCore::doCanTerminate
 * \param Address the offset of the QT we need to wait for or a 'wild card'
 *
 * \return true if the QT can terminate (should not wait)
 */
   bool AbstractCore::
doCanTerminate( SC_CORE_MASK_TYPE Address)
{
    // The two procedures return a mask of the cores we are waiting for
    return !doWaitMask_Get(Address);
}

/*!
   * \brief AbstractCore::doKillQT
   *
   * To kill a QT means to advance its fetch pointers
   * to the corresponding QTerm
   */
void AbstractCore::
doKillQT(void)
{
/*    fetch.NewPC = QTOffset_Get() + QTLength_Get();
    fetch.PC = fetch.NewPC; exec.PC =  fetch.PC;
    fetch.Pending = false; fetch.Valid = false;
*/}

/*!
 * \brief AbstractCore::HandleMetaInstruction
 * Called when the next instruction is a metainstruction
 * The metainstruction is written into a priority-ordered FIFO
 * Per core only one meta-instruction is possible
 */
void AbstractCore::
HandleMetaInstruction(void)
{
 /*
   // Write the new metainstruction into the priority-ordered FIFO
//!!    mProgramCounter = exec.PC;
//!!    LOG_TRACE_SOURCE("Queued '" + MetaInstructionName_Get() + "'");
//!!      ControlTime_Add(msMeta.ExecutionTime.value());
//      msMeta.ExecutionTime = SCTIME_CLOCKTIME; // Simulate execution by SV delay
    MetaBit_Set(true);
    // From now on, the core is in meta-state
    sc_time MetaBegin = sc_time_stamp();
    Processor_Get()->MetaEvents->write(msMeta); // Write the intruction into the queue
    PERFORMANCE_COUNTMETAINSTRUCTION;
    DEBUG_EVENT_OBJECT("RCVD META_event '" << MetaInstructionName_Get() << "'");
    wait(SCTIME_CLOCKTIME);    // Simulating the FIFO
    PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);
// The metainstruction is now in the FIFO, can start fetching a new one, if needed
#ifdef USE_PARALLEL_PREFETCH
    if(!CreatingOrTerminatingQT())
    { // Start prefetching the next
//           fetch.Pending = true; fetch.Valid = false; // Will be set in FETCH_thread
      FETCH_event.notify(SC_ZERO_TIME); // Start to fetch instruction in parallel
      DEBUG_EVENT_OBJECT("SENT  FETCH_event after meta instruction");
    }
    else
#endif
    { // Prefetching will be triggered by the metainstruction
        //fetch.Pending = false; fetch.Valid = false;
    }
    // Now the metainstruction is in the FIFO, we must wait until executed
    DEBUG_EVENT_OBJECT("WAIT EVENT_CORE.METAEXECUTED '" << MetaInstructionName_Get() << "'");
    wait(EVENT_CORE.METAEXECUTED); // The core must stop until supervisor completes this metainstruction
    // The event passed the priority control of the core
    DEBUG_EVENT_OBJECT("RECVD EVENT_CORE.METAEXECUTED '" << MetaInstructionName_Get() << "' after delay"
         << sc_time_to_nsec_Get(2,8,sc_time_stamp()-MetaBegin) << " ns");
    PERFORMANCE_ADDMETAINSTRUCTIONTIME(sc_time_stamp()-MetaBegin);
    // Now the meta-instruction is executed, we can go back into normal mode
    MetaBit_Set(false);
    PERFORMANCE_ADDMETAINSTRUCTIONTIME(sc_time_stamp()-MetaBegin);
    */
}

#ifdef threads
/*!
 * \brief AbstractCore::HandleConventionalInstruction
 * Called when the next instruction is a conventional instruction
 * The intructions can one-by-one, so picking them up is almost independent
 * from each other
 */


void AbstractCore::
HandleConventionalInstruction(void)
{ // It is a normal instruction, just execute it
    /*!!
//    MEASURE_EXECUTION_TIME(string MyExec =  sc_time_to_nsec_Get(exec.ExecutionTime,1));
//    MAKE_PERFORMANCE_PLOT(sc_time ExecBegin = sc_time_stamp());
        // Prefetching is implemented in two ways
        // Fetching next instruction is implemented; if this option is on, it is made parallel
#ifdef USE_PARALLEL_PREFETCH
    if(exec.Stage.okFetch)
    { // Yes, we may safely fetch next instruction in parallel with the execution of the previous instruction
      // No branch, end of QT, etc. follows, the control remain at the predicted PC
        FETCH_event.notify(SC_ZERO_TIME); // Start to fetch instruction in parallel
        DEBUG_EVENT_OBJECT("SENT  FETCH_event IN PARALLEL with a conventional instruction");
      // Now the instruction fetch started, may execute in parallel with processing the previus one
    }
#endif // USE_PARALLEL_PREFETCH
    //exec.Stage.CoreStatus =
            assert(doExecuteInstruction()); // The execution is available, do it immediately
            PERFORMANCE_COUNTINSTRUCTION;
            PERFORMANCE_ADDINSTRUCTIONTIME(exec.ExecutionTime);
    // So, the executable instruction  is performed already, attempt to fetch the next one
//!!    wait(exec.ExecutionTime);
    //PLOT_EXEC_BALL(ExecBegin, sc_time_stamp()-ExecBegin);
#ifdef USE_PARALLEL_PREFETCH
    if(!exec.Stage.okFetch)
    {
        // No, prefetching was NOT started in parallel with the loading of the instruction, make it now
        FETCH_event.notify(SCTIME_SV); // Start to fetch instruction only when this finished
        DEBUG_EVENT_OBJECT("SENT  FETCH_event AFTER finishing a PC-influencing conventional instruction");
    }
#else
    // No, parallel prefetching is not used at all, make prefetching after execution
    EVENT_CORE.FETCH.notify(SC_ZERO_TIME); // Start to fetch instruction only when this finished
    DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.FETCH AFTER a conventional instruction");
#endif // USE_PARALLEL_PREFETCH
*/
}
#endif //#ifdef threads


/*!
 * \brief AbstractCore::TerminateExecution
 * Terminate execution of both kinds of instruction:
 *   1) Decide if fetch the next one
 *   2) Decide if to reboot
 *   3) Decide if to halt
 */
void AbstractCore::
TerminateExecution(void)
{
#if 0 //!!
    if( !Processor_Get()->AllocatedMask_Get())
    { // No cores left; reboot
        wait(SCTIME_CLOCKTIME);
        PERFORMANCE_ADDMETAINSTRUCTIONTIME(SCTIME_CLOCKTIME);
        AbstractCore* ActiveCore = Processor_Get()->doReboot(Processor_Get()->PC_Get());
        if(ActiveCore)
        { // A real reboot case
//!!              LOG_INFO(PrologText_Get().c_str()  << "Must reboot at line " << LN  << ":\n  " << S.c_str() );
//!!              ActiveCore->AllocationBegin_ReSet(Processor_Get()->AllocationBegin_Get());
//!!          PLOT_ALLOCATON_BEGIN_SET(ActiveCore);
          ActiveCore->EVENT_CORE.NEXT.notify(SCTIME_CLOCKTIME);
          DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.NEXT");
        }
        else
        { // The final stop
                    // This must NOT be done with the macro PLOT_META_BALL()
//!!            Plot->PlotQTmeta(-1,sc_time_stamp().value(),PC_Get(), 1);
//!!              LN = Memory_Get()->findLinenoToAddress(PC_Get()); string S = MainMemory->getSourceLine(LN);
//!!              LOG_INFO(PrologText_Get().c_str()  << "Exiting at line " << LN  << ":\n  " << S.c_str() );
//!!          exec.Stage.CoreStatus = STAT_HLT;
          Processor_Get()->EVENT_PROCESSOR.HALT.notify(SCTIME_CLOCKTIME);
//         METAEXECUTED_event.notify(SC_ZERO_TIME);   // The QT terminated, but the simulator waits for the event
         DEBUG_EVENT_OBJECT("SENT  HALT_event" << Processor_Get()->PrologText_Get().c_str());
//             LOG_INFO(Processor_Get()->PrologText_Get(Processor_Get()->PC_Get()).c_str() << "Exiting last allocated core ");
         LOG_INFO("Exiting last allocated core ");
         wait(EVENT_CORE.NEXT);
        }
    }

//    if(!(//(exec.Stage.CoreStatus == STAT_META)
//         (MetaBit_Get() && (msMeta.Code == Q_TERM)) // The QT finished
//          || Processor_Get()->IsHalted())) // or the processor halted
    if(IsAllocated())
      { // the next events follows
        EVENT_CORE.NEXT.notify(SCTIME_CLOCKTIME);    // We are ready to fire again
        DEBUG_EVENT_OBJECT("SENT  EVENT_CORE.NEXT");
      }
#endif
}//AbstractCore:: TerminateExecution


/*
// Now surely either the meta or the executable instruction is terminated
#ifndef PREFETCH
if(!((ExecuteStage.CoreStatus == STAT_HLT) || // It is already halted
             ((ExecuteStage.CoreStatus == STAT_META)
              // or it is the last instruction of a QT
             &&  ((msMeta.Code == Q_TERM) || (msMeta.Code == Q_HALT)
                  // the QT was just created
                  || (msMeta.Code == Q_CREATE) || (msMeta.Code == Q_CREATE) //
                  || (msMeta.Code == Q_CREATET) || (msMeta.Code == Q_CREATEF) //
                 )
             )
     ))
{
  FETCH_event.notify(SC_ZERO_TIME); // Start to fetch instruction in parallel
  DEBUG_EVENT_OBJECT("SENT  FETCH_event after normal instruction");
}
#endif
*/
/**
 * Access data memory to get data for LOAD OPs
 * @param  addr address to access to
 * @return data value read
 */
uint64_t AbstractCore::
readInstrMem(SC_WORD_TYPE addr)
{   //sc_time MemDelay = sc_time_stamp();
//     performance->codeMemoryRead();
     // Just temporary, to read out memory data directly
     SC_WORD_TYPE W0 = MainMemory->DWord_Get(addr&~3);
     SC_WORD_TYPE W1 = MainMemory->DWord_Get((addr+4)&~3);
     wait(DMEMORY_READ_TIME);   // Just imitate waiting
     PERFORMANCE_ADDMEMORYTIME(DMEMORY_READ_TIME);
     uint64_t U = W1;
     U = (U << 32) | W0;
      int Shifts = addr & 3; // Find out which bytes to drop
       U = U >> Shifts*8;
//     SC_WORD_TYPE W = Processor_Get()->readInstrMem(addr, size);
//      performance->MemoryTimeAdd(sc_time_stamp()-MemDelay);
    return  U;
}

void AbstractCore::
writeInstrMem(SC_WORD_TYPE addr, uint32_t C)
{ //!!  sc_time MemDelay = sc_time_stamp();
//     performance->codeMemoryWrite();
    int Shifts = addr & 3; // Find out which bytes to drop

    uint64_t U1 = readInstrMem(addr);
    uint64_t UMask = 0x00000000FFffFFff;
    UMask = UMask << Shifts*8;
     // Just temporary, to read out memory data directly
/*     SC_WORD_TYPE W0 = MainMemory->DWord_Get(addr&~3);
     SC_WORD_TYPE W1 = MainMemory->DWord_Get(addr+4);
     uint64_t U = W1;
     U = (U << 32) | W0;
     U = U & ~UMask;    // Mask out the old content
     U = U | U1;
*/
     uint64_t  U = C;
     U = U << Shifts*8 ;  // Now the new value is in position
     U1 = U1 & ~UMask;    // Mask out the old content
     U = U | U1;    // Mask in the new content
     SC_WORD_TYPE W1 = U >> 32;
     SC_WORD_TYPE W0 = U & 0xFFffFFff;
     MainMemory->DWord_Set(addr&~3, W0);
     MainMemory->DWord_Set((addr+4)&~3, W1);
//      performance->MemoryTimeAdd(sc_time_stamp()-MemDelay);
}

/*uint64_t AbstractCore::
readInstrMem(SC_WORD_TYPE addr)
   { sc_time MemDelay = sc_time_stamp();
//     performance->codeMemoryRead();
     // Justt temporary, to read out memory data directly
     SC_WORD_TYPE W0 = MainMemory->DWord_Get(addr);
     SC_WORD_TYPE W1 = MainMemory->DWord_Get(addr+4);
     uint64_t U = W1;
     U = (U << 32) + W0;
      int Shifts = addr & 3; // Find out which bytes to drop
       U = U >> Shifts*8;
//     SC_WORD_TYPE W = Processor_Get()->readInstrMem(addr, size);
//      performance->MemoryTimeAdd(sc_time_stamp()-MemDelay);
    return  U;
}*/


  /*!
      * \brief AbstractGridPoint::AllocateFor
      *
      * Allocate this core for Parent if possible
      * \param Parent the parent AbstractCore
      * \return allocated gridpoint or NULL
      */
     AbstractCore* AbstractCore::
AllocateFor(AbstractCore* Parent)
{
    return dynamic_cast<AbstractCore*>(scGridPoint::AllocateFor(Parent));
//     msBLfifo->reset();
}

     AbstractCore* AbstractCore::
PreAllocateFor(AbstractCore* Parent)
{
    return dynamic_cast<AbstractCore*>(scGridPoint::PreAllocateFor(Parent));
}

     void AbstractCore::
HandleEndOfWaiting(void)
{
#if 0
              sc_time WT = sc_time_stamp()-msWaitBegin;
    //!!          performance->WaitTimeAdd(WT);
    //!!          PLOT_QT_WAIT(WT);
 /*             Plotter_Get()->PlotQTwait(ID_Get(),msWaitBegin.value(),PC_Get(),(sc_time_stamp()-msWaitBegin).value()+2000);
              if(WT>3)
              {
                  wait(2*SCTIME_SV); WT +=2;
                  Plotter_Get()->PlotQTmeta(ID_Get(),sc_time_stamp().value(),PC_Get(), 2000);
                  LOG_INFO(PrologText_Get().c_str() << "Reviving core after waiting " << WT << " ns");
               }
 */
    LOG_TRACE_SOURCE("Proceeding after waiting " + sc_time_to_nsec_Get(2, 6, WT) + " ns");
 //             DEBUG_EVENT_OBJECT("Proceeding after waiting " << sc_time_to_nsec(WT) << " ns");
 /*             if(WT>5)
              { // Repeat the meta symbol for waiting
                  Plotter_Get()->PlotQTmeta(ID_Get(),sc_time_stamp().value(),mSavedPC, 1000);
                  WaitTime_Add(5);
                  DeepWaitTime_Add(WT-5);
              }
              else
              {
                  WaitTime_Add(WT);
              }*/
#endif
}


 #ifdef other

  /**
   * @brief Assemble core info for the heading text for 'core' messages
   *
   * @return string, describing core internals
   */
      // Assemble core info for the heading text for 'core' messages
    string scVirtualCore::
  CoreText_Get(void)
      {
        ostringstream oss;
        oss << hex << setw(2) << setfill('0') << dec << ID_Get();// << "C";
//        if(msNestingLevel) oss << "/" << msNestingLevel;
  //      if(mEmbeddedLevel) oss << "/" << (char) ('a' + mEmbeddedLevel -1);
        return oss.str();
      }

#endif //other

    /* !
     * \brief AbstractCore::doInspect
     *
     * Make the actual inspection; overwrite in scGridPoint
     * \param in    input data vector
     * \param out   output data vector
     */
/*    void
doInspect(vector<int32_t>& in, vector<int32_t>& out)
{
        std::cerr << "AbstractCore Inspecting, received " << in.size();
}*/


    void AbstractCore::
ResetForParent(scGridPoint* Parent)
{
    scGridPoint::ResetForParent(Parent);
/**
          msICB->msFromSV_FIFO->reset();
          msICB->msPseudoRegisters->ForChild = 0;
          msICB->msPseudoRegisters->ForParent = 0;
          msICB->msPseudoRegisters->FromChild = 0;
          msICB->msPseudoRegisters->FromParent = 0;
**/
//          msWaitCount = 0; // Not waiting when allocated
#if 0 //!!
    fetch.Valid = false;fetch.Pending = false;
    fetch.PC = exec.PC;
#endif // 0
// **          msICB->msFromSV_FIFO->reset(); //**msBLfifo->reset();
}



      /*!
       * \brief Get value of register R
       * \param R code of the register to be read
       * \param Performance If to measure performance
       * \return Content of register R
       */
    SC_WORD_TYPE AbstractCore::
RegisterValue_Get(uint8_t R, bool Performance)
{
 //       if(Performance) performance->registerRead();
    return 0; //!!!REGS[R];
#if 0
        if(R<=R_EDI)
          return registers->getValue(R, Performance);
                  //(SC_WORD_TYPE) get_reg_val((mem_t)Registers_Get(), (reg_id_t) R);
          else if(R==R_ESV)
          {
   /*         if(Cloning)
            {// Read the data to send for the parent
                return msICB->msPseudoRegisters->ForParent;
            }
            else
                switch((int)ESVState_Get())
                {
                case cm_Child:
                    return msICB->msPseudoRegisters->FromParent;
                    break;
                case cm_ParentPRE:
                    return  msICB->msPseudoRegisters->FromParent;
                    break;
                case cm_ParentPOST:
                    return msICB->msPseudoRegisters->FromChild;
                    break;
                case cm_Normal:
                    return msICB->msPseudoRegisters->FromChild;
                    break;
                }*/
          }
         return 0;
#endif
      }
                   /*!
                      * \brief AbstractCore::RegisterValue_Set Set value of a register
                      * \param R Code of register
                      * \param V New content of register
                    * \param Performance If to measure performance
                      */
    void AbstractCore::
RegisterValue_Set(uint8_t R, SC_WORD_TYPE V, bool Performance)
{
//!!        REGS[R] = V;
#if 0
                         if(Performance) performance->registerWrite();

                   if(R<=R_EDI)
                       registers->setValue(R, V);
                       //**     set_reg_val((mem_t)Registers_Get(), (reg_id_t) R, (word_t)V);
                   else if(R==R_ESV)
                     { 	// Want to set %esv
   /*                    if(Cloning)
                       { // Store the data received from the child
                       msICB->msPseudoRegisters->FromChild = V;
                       }
                       else
                           switch((int)ESVState_Get())
                           {
                           case cm_Child:
                               msICB->msPseudoRegisters->ForParent = V;
                               break;
                           case cm_ParentPRE:
                               msICB->msPseudoRegisters->ForChild = V;
                               break;
                           case cm_ParentPOST:
                               msICB->msPseudoRegisters->ForParent = V;
                               break;
                           case cm_Normal:
                               msICB->msPseudoRegisters->ForParent = V;
                               break;
                           }*/
                     }
#endif // 0
                     }  ///< Set value of the private register

