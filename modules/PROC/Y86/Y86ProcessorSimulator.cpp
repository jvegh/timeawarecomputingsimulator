/** @file Y86ProcessorSimulator.cpp
 *  @brief Function prototypes for the  scProcessor simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "ProcConfig.h"
#include "Utils.h"
#include "Y86Processor.h"

#include "Y86ProcessorSimulator.h"
//#include "Plot.h"
//#include "Y86Core.h"
//#include "RISCVCore.h"

//#include "manage.h"
#include "Stuff.h"
#include "QFile"
#include "QString"

#include "scIGPMessage.h"   //  Debug only


// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
//#include "manage.h"
#define MAKE_LOG_TRACING
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "scClusterBusMemorySlow.h"

extern bool UNIT_TESTING;	// Whether in course of unit testing
extern scClusterBusMemorySlow *MainMemory;
#ifdef PREPARE_PERFORMANCE_DIAGRAM
//extern EMPAPlotter *Plot;
ofstream plotfile;		// Not related to simulation, just makes 'gpf/tikz' plots
#endif // PREPARE_PERFORMANCE_DIAGRAM

//!!extern SystemDirectories SDirectories;
//!!extern SystemDirectories* SystemDirectories_Get(void);
/*!
 * @brief	Creates a @ref Y86ProcessorSimulator, with CoreTopology
 */

Y86ProcessorSimulator::Y86ProcessorSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone)
//    : ProcessorSimulator( "Y86Sim", filename, noofgridpoints, false)
    : ProcessorSimulator( "Y86Sim", argc, argv, false)
{
    if(StandAlone)
    {   // use your own setup method
        readSettings();     /// Read the setting files at different levels
//        HandleSpecials();   /// Populate mSpecials with the found special AbstractCore modules (from mSpecial)
        // Create the main memory, belonging to the systems (presently only one processor)
        MainMemory = new scClusterBusMemorySlow("MainMemory", 0x00, FMAX_MEMORY_SIZE-1, 2);
//        MainMemory->clock(C1);    // The far memory uses clock; wait states measured in bus clock periods
        msProcessor = new Y86Processor("Y86", mSpecials,true);
//        msProcessor->clock(C1);
 #ifdef newdef
        if(filename && MainMemory->Y86loadHexFile(string(filename)))
        {   // Reading the binary (hexa) file was successful
            Processor_Get()->PC_Set( MainMemory->ProgramCounter_Get());
            SC_THREAD(SInitialize_method);
            // This part adds the performance plot handling
    #ifdef PREPARE_PERFORMANCE_DIAGRAM
            char buffer[50];
            sprintf (buffer, "%d",noofgridpoints);
            string FileNameRoot = GetFileNameRoot( filename)+"_"+string(buffer);
//            Plot = new EMPAPlotter(Processor_Get(), FileNameRoot.c_str());
    #endif // PREPARE_PERFORMANCE_DIAGRAM

        }
        else
            Processor_Get()->PC_Set( MEMORY_DEFAULT_ADDRESS);
#endif //newdef
    }
}


Y86ProcessorSimulator::~Y86ProcessorSimulator()
{
//    writeSettings();    // Save the settings in the project directory
    PrintFinalReport();
#ifdef PREPARE_PERFORMANCE_DIAGRAM
 //   if(Plot){ delete Plot; Plot = 0;}
#endif // PREPARE_PERFORMANCE_DIAGRAM
}

Y86Processor* Y86ProcessorSimulator::
Processor_Get(void)
{return  dynamic_cast<Y86Processor*>(ProcessorSimulator::Processor_Get());}

//#include "SimulatorSettings.include"
/*!
 *  The main settings reading routine:
 *  Reads setting from levels System, User, project
 */
    void Y86ProcessorSimulator::
readSettings()
{
#ifdef other
    QSettings Ssettings(QSettings::IniFormat , QSettings::SystemScope, QString(mSettingsFileName.c_str()));
    // First read the application-wide settings
    readSettings(&Ssettings,"System");
    QSettings Usettings(QSettings::IniFormat , QSettings::UserScope, QString(mSettingsFileName.c_str()));
    // Second read the user-wide settings
    readSettings(&Usettings,"User");
//    string S =  SystemDirectories_Get()->UserData + "/MyProj" +string(".ini");
    string S =  SystemDirectories_Get()->UserData + "/" + mSettingsFileName +string(".ini");
    QSettings Psettings( QString(S.c_str() ), QSettings::IniFormat);
    // Second read the project-wide settings
    readSettings(&Psettings,"Project");
//    Ssettings.setValue("Work", 2234);
    // Read Specials.ini -> Generate the special cores
    //
    // Read Alias.ini -> Generate the normal neurers

    S = SystemDirectories_Get()->UserData + "/Alias" +string(".ini");
    if( QFile::exists(QString(S.c_str())))
    {
        QSettings Asettings( QString(S.c_str() ), QSettings::IniFormat);
        readAliases(&Psettings,"Project");
    }
    else
    {
        std::cerr << "File " << " is required for the simulation\n";
/*        //qCritical(
        QMessageLogger(
                    //QString("File ") +
                   QString(S.c_str())
//                   + QString(" is required for the simulation\n")
                   );*/
    }

#endif //other
}

// This makes the real reading, either system, user or project-wide settings
    void Y86ProcessorSimulator::
readSettings(QSettings *settings, string Level)
{
#ifdef other
//    std::cerr << "Read '" << settings->fileName().toUtf8().constData() << "' settings\n";
/*    qInfo() << "Reading 'MainWindow' settings";
    if (settings->allKeys().isEmpty())
        return; // A virgin startup
    settings->beginGroup("MainWindow");
  int Work =settings->value("Works",5).toInt();
    settings->endGroup();*/
    HandleNeurerStructure(settings, Level, false);
    HandleAliases(settings, Level, false);
#endif //
}


     /*!
     * \brief Y86ProcessorSimulator::writeSettings
     * This routine saves current setting in the 'Work' directory
     */
    void Y86ProcessorSimulator::
writeSettings(void)
{
    // This is the only settings file we write
//        string S =  SystemDirectories_Get()->UserData + "/MyProj" +string(".ini");
       string S =  SystemDirectories_Get()->UserData + "/" + mSettingsFileName +string(".ini");
//        std::cerr << S << endl;
        QSettings Psettings( QString(S.c_str() ), QSettings::IniFormat);
//    QSettings Psettings( QString(SDirectories.UserData.c_str() ), QSettings::IniFormat);
//    QSettings Psettings(QString(string(scSimulator::Directories.UserData + mSettingsFileName.c_str() +string(".ini").c_str()), QSettings::IniFormat);
}
/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
void Y86ProcessorSimulator::
SInitialize_method(void)
{
  DEBUG_PRINT("Initialize_method" );
  
//  Processor_Get()->EVENT_PROCESSOR.START.notify();
}
/* These are just for eventual testing
  wait(4,SC_NS);
  START_event.notify();
  wait(5,SC_NS);
  SUSPEND_event.notify();
  wait(5,SC_NS);
  RESUME_event.notify();
  wait(5,SC_NS);
  STOP_event.notify();*/

#if 0
/*!
 * \brief scSimulator::SUSPEND_thread
 * Since the simulator is the outmost object of the system, it must be suspended for the time of the work
 * 
 */
void Y86ProcessorSimulator::
SSUSPEND_thread(void)
{
   DEBUG_PRINT("SSUSPEND_thread started");
   while(true)
    {
        wait(EVENT_SIMULATOR.SUSPEND);
        DEBUG_EVENT_SC("RCVD EVENT_SIMULATOR.SUSPEND");
        if(Processor_Get()->ProcessorHalted_Get())
        {
//            LOG_INFO_SC("attempted to suspend a halted processor");
        }
        else
        {
          Processor_Get()->EVENT_SCPROCESSOR.SUSPEND.notify();
          msSuspended = true;
        }
    }
}

    void Y86ProcessorSimulator::
SRESUME_thread(void)
{
   DEBUG_PRINT("SRESUME_thread started");
   while(true)
    {
        wait(EVENT_SIMULATOR.RESUME);
        DEBUG_EVENT_SC("RCVD EVENT_SIMULATOR.RESUME");
        if(Processor_Get()->ProcessorHalted_Get())
        {
//          LOG_INFO_SC("attempted to suspend a halted processor");
        }
        else
        {
          Processor_Get()->EVENT_SCPROCESSOR.RESUME.notify();
          msSuspended = false;
        }
    }
}

/*!
 * \brief scSimulator::SSTART_thread
 * This thread is waiting for a START_event and processes it
 */
    void Y86ProcessorSimulator::
SSTART_thread(void)
{
     while(true)
    {
       DEBUG_EVENT("WAIT EVENT_SIMULATOR.START");
        wait(EVENT_SIMULATOR.START);
        DEBUG_EVENT("RCVD EVENT_SIMULATOR.START_event");
        if(!Processor_Get()->ProcessorHalted_Get())
        {
//          LOG_INFO_SC("attempted to start a running processor");
        }
        else
        {
          Processor_Get()->EVENT_SCPROCESSOR.START.notify();
        }
    }
}

    /*!
     * \brief scSimulator::SSTOP_thread
     * This thread is waiting for a STOP_event and processes it
     */
    void Y86ProcessorSimulator::
SSTOP_thread(void)
{
    DEBUG_PRINT("SSTOP_thread started");
    while(true)
    {
        wait(EVENT_SIMULATOR.STOP);
        DEBUG_EVENT_SC("RCVD EVENT_SIMULATOR.STOP");
        if(Processor_Get()->ProcessorHalted_Get())
        {
//              LOG_INFO_SC("attempted to stop at a running processor");
        }
        else
        {
            Processor_Get()->EVENT_SCPROCESSOR.STOP.notify();
        }
    }
}
/*!
 * \brief scSimulator::HALT_thread
 * This thread is waiting for a HALT_event and processes it
 */
    void Y86ProcessorSimulator::
SHALT_thread(void)
{
   DEBUG_PRINT("HALT_thread started");
    while(true)
    {
        wait(EVENT_SIMULATOR.HALT);
        DEBUG_EVENT_SC("RCVD EVENT_SIMULATOR.HALT");
        if(Processor_Get()->ProcessorHalted_Get())
        {
//          LOG_INFO_SC( "attempted to halt a halted processor");
        }
        else
        {
            Processor_Get()->EVENT_SCPROCESSOR.HALT.notify();
        }
    }
}
#endif // 0

    /**
     * @brief Print a summary report at the end of processing
     *
     * Print a summary table, which contains statistics of operation
     * @return void
     */
    void Y86ProcessorSimulator::PrintFinalReport(void)
    {
 //       ProcessorSimulator::PrintFinalReport();
      if(!UNIT_TESTING)
      {
#ifdef other
    //  Processor* Proc = Processor_Get();
    //  unsigned int tmemorycycles = 0;
      qInfo() << "\nSummary table";
      qInfo().noquote().nospace() << "Core(no)    " << "Instr " << "MemR  "  << "MemW  "  << " Tmemo " << "  Tftch " << "  Texec " << "  Twait  "  << " TTime " << "Payl% " << "Util%" ;

      // These will be used for summing
      uint64_t  tinstructions=0, // Executed instructions
              tmemoryreads, // Memory accesses
              tmemoryrites, // Memory accesses
              tdeepwaittime, // Waiting at low power
              tpayloadtime=0, // Payload % (tfecthtime+texextime)/ttotaltime)
              tutilizationtime=0; // utilization % (ttotaltime/simulationtime);
    sc_time tfetchtime = SC_ZERO_TIME; // Intruction fetch time
    sc_time texectime = SC_ZERO_TIME; // The total time to calculate
    sc_time twaittime = SC_ZERO_TIME; // Just waiting
    sc_time tmemtime = SC_ZERO_TIME; // Waiting for memory
    sc_time ttotaltime = SC_ZERO_TIME; // Total time being used

      int NoOfCores = Proc->NoOfCores_Get();
      int SimTime = sc_core::sc_time_stamp().value(); if(!SimTime) ++SimTime;
      int CoreUtilization[NUMBER_OF_CORES]; int counter = 0;
      for(int i = 0; i < NoOfCores; i++)
      {
        scCore* C = Proc->CoreByID_Get(i);
        if(C->InstructionsExecuted_Get() )//|| C->MetaIntructionsExecuted_Get())
//        if(C->FetchTime_Get()+C->ExecTime_Get())
        { // The core was active during this run
          counter++;
          tinstructions += C->InstructionsExecuted_Get();
          tmemoryreads += C->DataMemoryRead_Get(); // Memory accesses
          tmemoryrites += C->DataMemoryWrite_Get(); // Memory accesses
          tmemtime += C->MemoryTime_Get();
          tfetchtime += C->FetchTime_Get();
          texectime += C->ExecTime_Get();
          twaittime += C->WaitTime_Get();
          sc_time TotalTime = C->MemoryTime_Get() + C->FetchTime_Get() + C->ExecTime_Get() + C->WaitTime_Get();
 /*         tmemoryaccesses += C->MemoryCycles_Get();
          tdeepwaittime += C->DeepWaitTime_Get();
          int TotalTime = C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get();
          ttotaltime += TotalTime;
          int PayloadUtilization = (C->FetchTime_Get()+C->ExecTime_Get())*100/TotalTime + 0.49;
          tpayloadtime+= (C->FetchTime_Get()+C->ExecTime_Get());
     //     int LowEnergyUtilization = (C->DeepWaitTime_Get())*100/TotalTime + 0.49;
          int ThisUtilization = (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get())*100/(SimTime/1000) + 0.49;
          tutilizationtime += (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get());
          CoreUtilization[i] = ThisUtilization;
          */
          ostringstream LogInfo;
            LogInfo << "Core(" << std::setw(2) << std::setfill('0') << i << ")  "
          << std::setw(6) << setfill(' ')
          << std::setw(6) << C->InstructionsExecuted_Get() // No of instruction executed by this core
          << std::setw(6) << C->DataMemoryRead_Get() // No of times the memory accessed
          << std::setw(6) << C->DataMemoryWrite_Get() // No of times the memory accessed
          << std::setw(6) << sc_time_to_nsec(C->MemoryTime_Get(),2) // Time of memory access
          << std::setw(6) << sc_time_to_nsec(C->FetchTime_Get(),2) // Time of fetching
          << std::setw(6) << sc_time_to_nsec(C->ExecTime_Get(),2) // Time of executing
          << std::setw(6) << sc_time_to_nsec(C->WaitTime_Get(),2) // Time of waiting
          << std::setw(6) << sc_time_to_nsec(TotalTime,2) // Time of waiting
/*          << std::setw(6) << C->FetchTime_Get()
          << std::setw(6) << C->ExecTime_Get()
          << std::setw(6) << C->ControlTime_Get()
          << std::setw(6) << C->WaitTime_Get()
          << std::setw(6) << C->DeepWaitTime_Get()
          << std::setw(6) << TotalTime // Total time being used by this core
          << std::setw(6)  << PayloadUtilization // Utilization% (tfecthtime+texextime+tcontroltime)/tutilizationtime
          << std::setw(6)  << ThisUtilization // Payload % (tfecthtime+texextime)/ttotaltime)
*/              ;
          qInfo() << LogInfo.str().c_str();
    //      tutilizationtime += ThisUtilization;
    //      tpayloadtime += PayloadUtilization;
         }
      }
/*
 *       ostringstream LogInfo;
      if(counter)
      {
        LogInfo << "Total     "  << setfill(' ')
        << std::setw(6) << tinstructions
        << std::setw(6) << tmemoryaccesses
        << std::setw(6) << tfetchtime
        << std::setw(6) << texectime
        << std::setw(6) << tcontroltime
        << std::setw(6) << twaittime
        << std::setw(6) << tdeepwaittime
        << std::setw(6) << ttotaltime
        << std::setw(6) << (int)(tpayloadtime*100/ttotaltime+ 0.49)
        << std::setw(6) << (int)(tutilizationtime*100/(SimTime/1000)/counter + 0.49);
           ;

      LogInfo  << "\nSimTime   " << std::setw(6) << //QString("%1").arg
                                     (sc_core::sc_time_stamp().value()/1000);
      qInfo() << LogInfo.str().c_str() ;
      int Max = 0; int MyVal; double Avg = 0; double Weights = 0;
      for(int i=0; i<counter; i++)
      {
          MyVal = CoreUtilization[i];
          if(MyVal>Max) Max = MyVal;
          Avg += i*MyVal;
          Weights += MyVal;
      }
      Avg = Avg/Weights;
      int AvgInt = Avg * 100;
      double Norm = 50./Max; QString a("%1");
      qInfo().noquote() << "\nSpeed gain =" << QString::number(double(texectime+tfetchtime)/(SimTime/1000),'f',2);
      qInfo().noquote() << QString("Average core number = %1").arg(AvgInt/100.,5);
      qInfo().noquote() << "Core utilization frequency distribution\n";
      for(int i=0; i< counter; i++)
      {
          int j = CoreUtilization[i]*Norm;
          qInfo().noquote() << i << a.arg(CoreUtilization[i],j+1,10,QChar('>'));
      }
      qInfo().noquote() << "\n";
    */
#endif //other

//      plotfile << "$};\n";
//      plotfile << "\\node[right,above] at ( 0.9 cm,.5cm) {";
//      plotfile << "$U=" << tutilizationtime/counter << "\\%,~P=" << tpayloadtime/counter;
//      plotfile << "\\% $};\n";
    //  cerr << LogInfo.str();
      } // UNIT_TESTING
    }
