/** @file Y86Processor.cpp
 *  @brief Function prototypes for the topology of processor cores, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "Y86Processor.h"
//#include "Y86Core.h"
//#include "scIGPCB.h"
//#include "scIGPMessage.h"
#include "Utils.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING

#include "Macros.h"

extern bool OBJECT_FILE_PRINTED;		// If the object file already printed
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
extern reg_struct meta_table[R_ERR+1];
extern scClusterBusMemorySlow *MainMemory;
//* Return name of register given its ID */

/*!
 * \brief Y86Processor::Y86Processor
 * \param nm SystemC name
 * \param Specials the list of special neurons
 * \param StandAlone true if some part shall be executed here rather than in the subclass
 */
     Y86Processor::
Y86Processor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone)
             : AbstractProcessor(nm, Specials, false)
{
    if(StandAlone)
    {
      SC_THREAD(Initialize_method);
      SC_THREAD(START_thread);
        sensitive << EVENT_PROCESSOR.START;
      Populate(Specials); // Populate with the 60 cores
      CreateClusters();      // Clusterize cores as gridpoints
      ConnectIGPCBs();       // Connect their communication lines
      ConnectClusterHeadsToBus(mClusterBus);  // Connect ONLY cluster heads to inter-cluster bus
      Reset();
    }
}

    Y86Processor::
~Y86Processor(void)
{}
    void Y86Processor::
Reset(void)
{
    AbstractProcessor::Reset();
}

    // The text editor already contains the file, now read it to the memory
    // if used in GUI, not just make lineno to address mapping
     bool
     Y86Processor::
 loadHexFile(string fileName)
 {
    //  if(fileName.empty())
    //      fileName = msObjectFileName;
         lastFile = fileName;
      ifstream infile(fileName, ifstream::in);
      string line;
      if(!infile.good())
        return false;
      if(!OBJECT_FILE_PRINTED)
          LOG_TRACE("Loading executable from file '" + fileName + "'");
    //    LOG_INFO_SC();
       // OK, initialize global memory variables
//      mTotalBytes = 0;
      int lineno = 0;
      mInstructionAddress = (uint32_t)-1;

    //  int bytepos = 0;
     // First read the complete file into a string:
      string CodeStream;
      while(getline(infile, line))
      {
          CodeStream += line + '\n';
          if(!OBJECT_FILE_PRINTED)
          {
              ostringstream oss;
              oss << "  Line" << setw(3) << setfill('0') << ++lineno << ":" << line.c_str();
     //         LOG_INFO_SC( oss.str().c_str());
              LOG_TRACE(oss.str().c_str());
          }
      }
      // Now the file is read completely, interpret its content and store it in memory
      loadStream(CodeStream);
      lastFile = fileName.substr(fileName.find_last_of('/')+1);
      lastFile = lastFile.substr(0,lastFile.find_last_of('.'));
      if(!OBJECT_FILE_PRINTED)
      {
        LOG_TRACE("reading from file '" + fileName + " succeeded ");

      }
      OBJECT_FILE_PRINTED = true;
      return (mTotalBytes>0);
 }// of loadHexFile


     // The text editor already contains the file, now read it to the memory
     // if used in GUI, not just make lineno to address mapping
     /*!
        * \brief Load an 'object file' from a stream, mainly for testing
        * \param fileString
        * \return
        */
       bool Y86Processor::
 loadStream(string fileString)
     {
       char split_char = '\n';
       mTotalBytes = 0;
       mTotalLines = 0;
       int lineno = 0;
       program_counter = CORE_DEFAULT_ADDRESS; PC_seen = false;

 //      unsigned int bytepos = 0;
       AddressToLineMap.clear(); // Make the map empty
       istringstream split(fileString);
       vector<string> tokens;
       for (string each; getline(split, each, split_char); tokens.push_back(each), mTotalLines++);
       for (vector<string>::iterator it = tokens.begin(); it != tokens.end(); )
         {
           string line = *it++;
 //          unsigned int old_bytepos = bytepos;
           unsigned int TheseBytes = interpretLine(line, lineno);
         // if(!OBJECT_FILE_PRINTED)
     //       LOG(INFO) << setw(3) << setfill('0') << lineno << ":"<< line;
           mTotalBytes += TheseBytes;
           AddressToLineRecord* A = new AddressToLineRecord;
           A->Address = TheseBytes ? mInstructionAddress : -1;	// Use invalid address if no code generated
     //      A->Lineno = TheseBytes ? lineno : -1;	// use invalid lineno if no code generated
           A->Lineno = lineno;
           A->Line = line;				// Anyhow, add the source line to the map
           AddressToLineMap.push_back(A);
           DEBUG_PRINT( hex << A->Address << " " << A->Lineno << " " << A->Line);
         }
 //      LOG_TRACE(lineno << " lines, " << TotalBytes << " bytes read ");
 //      LOG_TRACE( "Starting PC is: 0x" <<  hex << program_counter << dec );
       PC_Set(program_counter);
       return true;
     }


       int hex2dig(char c)
       {
           if (isdigit((int)c))
           return c - '0';
           if (isupper((int)c))
           return c - 'A' + 10;
           else
           return c - 'a' + 10;
       }

       // This routine interprets a line of 'object code'
         int Y86Processor::
    interpretLine(string S, int &lineno)
    {
         char buf[4096];
         char c, ch, cl;
         strcpy(buf,S.c_str());
         int this_byte_cnt = 0;
         int cpos = 0;
       //  int index = 0;
         lineno++;
               // Skip white space
           while (isspace((int)buf[cpos]))
               cpos++;
       // Check if line contains executable code
           if (buf[cpos] != '0' ||
               (buf[cpos+1] != 'x' && buf[cpos+1] != 'X'))
                   return this_byte_cnt; // Skip this line
           cpos+=2;

           // It looks like it contains code:
               // Get address
           int bytepos = 0;
           while (isxdigit((int)(c=buf[cpos]))) {
               cpos++;
               bytepos = bytepos*16 + hex2dig(c);
           }
           if(bytepos >= 0)
           { // No content was loaded before
               mInstructionAddress = bytepos;
               if((S.find(".pos") != std::string::npos) && !PC_seen )
               {
                   program_counter = bytepos; // Assume it is the starting address
                   PC_seen = true;
               }
           }
           else
               mInstructionAddress = (uint32_t)-1; // No address found

           while (isspace((int)buf[cpos]))
               cpos++;

           // Syntax check: colon must follow: needs GUI check
           if (buf[cpos++] != ':') {
       //	    if (report_error) {
       //		fprintf(stderr, "Error reading file. Expected colon\n");
       //		fprintf(stderr, "Line %d:%s\n", lineno, buf);
       //		fprintf(stderr,
       //			"Reading '%c' at position %d\n", buf[cpos], cpos);
       //            }
               return 0;
           }

           //	addr = bytepos;

           while (isspace((int)buf[cpos]))
               cpos++;

       //    index = 0;

               // Get code
           while (isxdigit((int)(ch=buf[cpos++])) &&
                  isxdigit((int)(cl=buf[cpos++]))) {
               unsigned char byte = 0;
       //	    if (bytepos >= Length) {
       //		if (report_error) {
       //		    fprintf(stderr,
       //			    "Error reading file. Invalid address. 0x%x\n",
       //			    bytepos);
       //		    fprintf(stderr, "Line %d:%s\n", lineno, buf);
       //		}
       //		return 0;
       //	    }
               byte = hex2dig(ch)*16+hex2dig(cl);
               MainMemory->Byte_Set(bytepos++,byte);
       //        ms_storage->contents[bytepos++] = byte;
               this_byte_cnt++;
       //        hexcode[index++] = ch;
       //        hexcode[index++] = cl;
           }
       /*        // Fill rest of hexcode with blanks
           for (; index < 12; index++)
               hexcode[index] = ' ';
           hexcode[index] = '\0';*/
           return this_byte_cnt;

    }

#ifdef OldY86Read

    // This routine interprets a line of 'object code'
      int Y86Processor::
    interpretLine(string S, int &lineno)
    {
      char buf[4096];
      char c, ch, cl;
      strcpy(buf,S.c_str());
      int this_byte_cnt = 0;
      int cpos = 0;
    //  int index = 0;
      lineno++;
            // Skip white space
        while (isspace((int)buf[cpos]))
            cpos++;
    // Check if line contains executable code
        if (buf[cpos] != '0' ||
            (buf[cpos+1] != 'x' && buf[cpos+1] != 'X'))
                return this_byte_cnt; // Skip this line
        cpos+=2;

        // It looks like it contains code:
            // Get address
        unsigned int bytepos = 0;
        while (isxdigit((int)(c=buf[cpos]))) {
            cpos++;
            bytepos = bytepos*16 + hex2dig(c);
        }

        while (isspace((int)buf[cpos]))
            cpos++;

        // Syntax check: colon must follow: needs GUI check
        if (buf[cpos++] != ':') {
    //	    if (report_error) {
    //		fprintf(stderr, "Error reading file. Expected colon\n");
    //		fprintf(stderr, "Line %d:%s\n", lineno, buf);
    //		fprintf(stderr,
    //			"Reading '%c' at position %d\n", buf[cpos], cpos);
    //            }
            return 0;
        }

        //	addr = bytepos;

        while (isspace((int)buf[cpos]))
            cpos++;

    //    index = 0;

            // Get code
        while (isxdigit((int)(ch=buf[cpos++])) &&
               isxdigit((int)(cl=buf[cpos++]))) {
            int8_t byte = 0;
    //	    if (bytepos >= Length) {
    //		if (report_error) {
    //		    fprintf(stderr,
    //			    "Error reading file. Invalid address. 0x%x\n",
    //			    bytepos);
    //		    fprintf(stderr, "Line %d:%s\n", lineno, buf);
    //		}
    //		return 0;
    //	    }
            byte = hex2dig(ch)*16+hex2dig(cl);
//!!            ms_storage->contents[bytepos++] = byte;
            this_byte_cnt++;
    //        hexcode[index++] = ch;
    //        hexcode[index++] = cl;
        }
    /*        // Fill rest of hexcode with blanks
        for (; index < 12; index++)
            hexcode[index] = ' ';
        hexcode[index] = '\0';*/
        return this_byte_cnt;
    }
#endif //OldY86Read

#ifdef Y86Good
    bool Y86Processor::
loadHexFile(string filename)
{
 //   if(fileName.empty())
   //         fileName = msObjectFileName;
    ifstream infile(filename, ifstream::in);
    string line;
    if(!infile.good())
        return false;
    //    if(!OBJECT_FILE_PRINTED)
      //    LOG_INFO("Starting to load object data from file '" << fileName.c_str() << "'");
//        lastFile = fileName.substr(fileName.find_last_of('/')+1);
//        lastFile = lastFile.substr(0,lastFile.find_last_of('.'));
        // OK, initialize global memory variables
        mTotalBytes = 0;
        int lineno = 0;
        int bytepos = 0;

         while(getline(infile, line))
         {
           unsigned int old_bytepos = bytepos;
           unsigned int TheseBytes = interpretLine(line, lineno);
/*           if(!OBJECT_FILE_PRINTED)
           {
               ostringstream oss;
               oss << "Line" << setw(3) << setfill('0') << lineno << ":" << line.c_str();
    //           LOG_INFO( oss.str().c_str());
           }*/
             mTotalBytes += TheseBytes;
             AddressToLineRecord* A = new AddressToLineRecord;
             A->Address = TheseBytes ? old_bytepos : -1;	// Use invalid address if no code generated
             A->Lineno = TheseBytes ? lineno : -1;	// use invalid lineno if no code generated
             A->Line = line;				// Anyhow, add the source line to the map
             AddressToLineMap.push_back(A);
         }
 //       if(!OBJECT_FILE_PRINTED)
        {
     //     qInfo().nospace() << ":::" << lineno << " lines, " << TotalBytes << " bytes read from file '" << fileName.c_str() << "'";
        }
        return (mTotalBytes>0);
         return true;
}
#endif //Y86Good
        /*!
         * \brief Y86Processor::doReboot
         * \return the pointer to the newly allocated core
         *
         */
    scHThread* Y86Processor::
doReboot(SC_ADDRESS_TYPE LoadAddress, HThreadPreference_t HPT)
{
        if(Halted_Get()) return nullptr;
    // To reboot, we must have at least one core; allocate it
    LOG_TRACE("Rebooting needed");
    if(LoadAddress == (SC_ADDRESS_TYPE) MEMORY_DEFAULT_ADDRESS) LoadAddress = PC_Get();
    scHThread* MyThread = HThreadAllocateFor((scHThread*) nullptr,HPT);
    // In principle, this call can be repeated any times, enabling multiprocessing,
    // starting OS, allocating resources, etc. The address can be the same (say, for neurons)
    // or different.

//!!    sc_time RebootTime = sc_time_stamp();
//!
    if(!MyThread)
    {
        LOG_CRITICAL( "Processor could not be REBOOTed, no scHThread");
        return (scHThread*) NULL; // Could not allocate a core; fatal
    }
//    MyCore->fetch.Stage.CoreStatus = STAT_AOK; // Assume success
    // Now we surely have a HThread; start processing
    // If more Hthreads are started, all they must set the PC and the fetch flags correspondingly
    MyThread->OperatingBit_Set(tob_FetchValid,false);
    MyThread->OperatingBit_Set(tob_FetchPending,false);
    MyThread->fetchPC_Set(LoadAddress);
//    MyCore->fetchNewPC_Set(LoadAddress);
    MyThread->PC_Set(LoadAddress);
    LOG_TRACE(string(MyThread->PrologString_Get() + " Rebooted"));
    MyThread->EVENT_HTHREAD.NEXT.notify(SC_ZERO_TIME); // Must be 0 time!

#ifdef NotNormal
    // The instruction must be read to core to avoid waiting
    // Now everything looks like after a normal prefetching
    //    PLOT_FETCH_BALL(sc_time_stamp(),MyCore->fetch.ExecutionTime);
//!!    sc_time FetchBegin = sc_time_stamp();
    Y86Core* GP = (Y86Core*)MyThread->Owner_Get();
    bool FetchWasOK = GP->doFetchInstruction(MyThread);
    if(!FetchWasOK) // Must imitate a prefetch to find out the first instruction
    {
        LOG_CRITICAL("Instruction fetch failed; wrong code");
        return (scHThread*) NULL; // Could not allocate a core; fatal
    }
//          Plot->PlotQTfetch(MyCore->ID_Get(),atoi(sc_time_to_nsec(FetchBegin,0).c_str()),MyCore->fetch.PC, atoi(sc_time_to_nsec(sc_time_stamp()-FetchBegin,0).c_str()));
    MyThread->PrepareNextInstruction();
    // Now we have a core, fetched the instruction, prepared for execution
    // Check what we got
    if(GP->IsMorphingInstructionFetched(MyThread))
    { // A QCreate follows, but the core is created already
        if(Q_HALT == (qt_t) MyThread->fetchFunctionCode_Get() )
        { // A 'QHalt' follows: no QT, so proceed directly
            LOG_TRACE(string("Reboot withdrawn; final halting ")+MyThread->PrologString_Get());
            MyThread->Deallocate();  // We do not need the allocated HThread
            MyThread = (scHThread*) NULL;
            msHalted = true;
        }
        else
        { // A normal Q instruction follows
            LOG_TRACE("Continuing after rebooting");
            MyThread->QTOffset_Set(LoadAddress);
            MyThread->QTLength_Set(MyThread->fetchImmediate_Get()-LoadAddress - QCreate_Size);
            MyThread->PC_Set(LoadAddress + MyThread->fetchImmediate_Get() + QTerm_Size); // Advance to address after the QTerm
 //??           MyThread->PC_Set(MyThread->PC_Get()+QCreate_Size);
            MyThread->QTRegime_Set(true); // A 'real' QT
 //!!           scIGPMessage* MyMessage = GP->CreateQtCreateMessageTo(MyThread, LoadAddress, 0,0); // An intentional self-addressing
//!!            GP->RouteMessage(MyMessage);
        }
    }
    else
    {   // A conventional instruction follows,
        // We need to create implicitly a 'R'egular mode QT: setup a fake 'QCreate' metainstruction
        if(!FetchWasOK)
        {
            LOG_CRITICAL( "Processor could not be REBOOTed, wrong code");
            return (scHThread*) NULL; // Could not allocate a core; fatal
        }
        // It looks like a conventional instruction is successfully fetched; a hidden QT must be prepared
#ifdef newdef
        // Prepare a 'fake' QT
        MyCore->fetch.ExecutionTime = SCTIME_CLOCKTIME;
        MyCore->msMeta.Address = MyCore->msMeta.PC = LoadAddress; // Self-addressing by intention
        MyCore->msMeta.Code = Q_CREATER;    // Register QCreate
        MyCore->msMeta.Param = R_NONE;    // No register to return in the case of fake event
        MyCore->msMeta.CA = MyCore->ClusterAddress_Get();
        MyCore->msMeta.ExecutionTime = SCTIME_CLOCKTIME;
        MyCore->QTRegime_Set(false); // An 'old style' QT
        MyCore->fetch.Valid = true; // Imitate a fetched meta-instruction
//        MyCore->fetch.Stage.CoreStatus = STAT_META; // Tell it was a meta-instruction
        MyCore->fetch.ExecutionTime = sc_time_stamp();
        // Normally, the msMeta in cores is set from msMeta in processor
/*        msMetaEvent.Address = MyCore->msMeta.Address;
        msMetaEvent.Code =  MyCore->msMeta.Code;
        msMetaEvent.CA = MyCore->msMeta.CA;
        msMetaEvent.PC = MyCore->msMeta.PC;
        msMetaEvent.Param = MyCore->msMeta.Param;*/
        msMetaEvent =  MyCore->msMeta; // In 'ReBoot' case make a reverse copy
#ifdef MAKE_PERFORMANCE_DIAGRAM
        MyCore-> msMetaBegin = sc_time_stamp();
#endif // MAKE_PERFORMANCE_DIAGRAM
        LOG_TRACE("Prepared fake '" + string(meta_table[MyCore->msMeta.Code].name) + "' for reboot");
#endif // newdef
        // Now everything looks like after fetching a 'Q_CREATER', execute the 'fake' instruction
        MyThread->OperatingBit_Set(tob_Morphing,false);
/*         doQCREATE((Y86Core*)NULL,MyThread,
                  (SC_CORE_MASK_TYPE) 0,
                  (SC_CORE_MASK_TYPE) 0); // Now the child core is allocated in advance
*/
//         MyThread->OperatingBit_Set(tob_Morphing,false);
//        MyThread->MetaBit_Set(false); Is it needed???
        MyThread->PC_Set(LoadAddress);  // doQCREATE thinks QT Create code should be skipped, restore PC
//??        PC_Set(LoadAddress);          // Also in the processor
        MyThread->fetchPC_Set(LoadAddress); // No need to fetch it again
//              Plot->PlotQTmeta(MyCore->ID_Get(),atoi(sc_time_to_nsec(RebootTime-SCTIME_SV,0).c_str()),LoadAddress, atoi(sc_time_to_nsec(SCTIME_SV,0).c_str()));
//        MyCore->fetch.Stage.CoreStatus = STAT_AOK;   // Tell it was the originally fetched instruction
//!!        LOG_TRACE("Executed fake '" + string(meta_table[MyThread->msMeta.Code].name) + "' for reboot");
//??        MyThread->QTID_Set(MyThread->QT_AssembleID((Y86Core*)NULL));
        // OK, now everything prepared, send the start message
//??        RefreshGUI(*MyCoreThread);
        MyThread->EVENT_HTHREAD.NEXT.notify(SC_ZERO_TIME);
//!!        DEBUG_EVENT("EVENT_HTHREAD.NEXT");
//        scIGPMessage* MyMessage = MyCore->CreateQtCreateMessageTo(MyCore, LoadAddress, 0,0); // An intentional self-addressing
//        MyCore->RouteMessage(MyMessage);
      }
#endif //NotNormal
        return MyThread;
    }

void Y86Processor::Initialize_method(void)
{
 //   wait(SCTIME_SV); // For setup
    DEBUG_PRINT("Y86 Initialize_method started" );
 //   mscoreStatus = STAT_AOK; // Assume everything all-right
    // We surely have no processing unit and also the first instruction must be fetched
}

  void Y86Processor::
Populate(vector<scGridPoint*>& Specials)
{
    // Create communication grid point objects
    Y86Core* C;
    for(int i = 0; i < GRID_SIZE_X; i++, i++) // This needed because of the hexagonal grid
    {
        for(int j = 0; j < GRID_SIZE_Y; j++)
        {
            int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
            C = new Y86Core(sc_core::sc_module_name(string("_").append(IDtoString(LinearAddress1,GRID_BUS_WIDTH)).c_str()),
                    this, //LinearAddress1,
                    GridPoint(i,j)//, true//StandAlone
            );

            mGrid.at(i).at(j) = C; // Store the pointer in the grid
//            mVector[LinearAddress1] = C;  // Store in the pointer in the vector, too
            int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
            C = new Y86Core(sc_core::sc_module_name(string("_").append(IDtoString(LinearAddress3,GRID_BUS_WIDTH)).c_str()),
                    this, //LinearAddress3,
                    GridPoint(i+1,j)//, true//StandAlone
            );
           mGrid.at(i+1).at(j) = C; // Store the pointer in the shadow-column
//           mVector.at(LinearAddress3) = C;  // as well as in the vector
        }
    }
}

    string Y86Processor::
StringOfMessage_Get(scIGPMessage* Message)
{
    ostringstream oss;
    assert(Message);
    oss << '<' << StringOfSender_Get(Message) << " => ";
    oss << StringOfReceiver_Get(Message);
    if(msg_Mem == Message->Type_Get())
         {} // oss << StringOfMemory_Get(Message);
    else
          oss << " Reg%" << StringOfRegisters_Get(Message);
    oss << ">";
    return oss.str();
}

#ifdef newdef
/*!
 * \brief Y86Processor::START_thread
 * This thread is waiting for a EVENT_PROCESSOR.START and processes it
 * It is reimplemented here because speciafically Y86processors must be handled
 */
    void
Y86Processor::START_thread(void)
{
 while(true)
 {
      DEBUG_EVENT("WAIT EVENT_PROCESSOR.START");
      wait(EVENT_PROCESSOR.START);
      DEBUG_EVENT("RCVD EVENT_PROCESSOR.START");
      msHalted = false;
      wait(SCTIME_CLOCKTIME);
      Y86Core* RebootCore = doReboot(MEMORY_DEFAULT_ADDRESS);    // Find first an available core
      DEBUG_EVENT("SENT EVENT_SCPROCESSOR.NEXT");
//!!      RebootCore->EVENT_GRID.NEXT.notify(SCTIME_CLOCKTIME);  // and starts its processing
      RebootCore->EVENT_CORE.NEXT.notify(SCTIME_CLOCKTIME);  // and starts its processing
 }
}
    /*!
       * \brief scProcessor::ChildFindFor
       * \param Parent the potential parent
       * \param CPT the preferred core (clustered) type
       * \return the requested core that can be allocated or NULL if not found
       *
       * The core preferences can be: cpt_Head, cpt_Member, cpt_Neighbor, cpt_AnyCore
       * 1./ cluster head (the most limited resource)
       * 2./ cluster member (a core from the same cluster, ordinary members first)
       * 3./ immediate neighbor (in the same cluster first)
       * 4./ any available core
       *
       * In the simplest form, get the next available core,
       * with the lowest sequence number. Can be replaced by considering temperature,
       * utilization, etc
       *
       */
/*      Y86Core* Y86Processor::
ChildFindFor(Y86Core* Parent, CorePreference_t CPT=cpt_AnyCore)
{
       Y86Core *GP;
       // For the processor,  and always take a cluster head if cpt_AnyCore asked
       if(!Parent && CPT==cpt_AnyCore) CPT = cpt_Head;
       // Try first if we have available cluster head
       //!! Consider threads here
       if(cpt_Head == CPT )
       {
            for(unsigned int i = 0; i<mClusters.size(); i++)
            {
                GP = ClusterHead_Get(i);
                if(GP->IsAvailable()) return GP;   // OK, a free cluster head found
            }

            if(Parent)
                CPT = cpt_Member;   // No available head found, attempt a member
            else
                CPT = cpt_AnyCore;  // // No available head found; for the processor, take any core
       }

       if(cpt_Member == CPT)
       { // Find a member helper for 'Parent'
           ClusterAddress_t CA = Parent->ClusterAddress_Get();
    //       scCore* Head = CoreByClusterAddress_Get(CA.ClusterNumber, cm_Head);
           for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
           {
                Y86Core* GP = ByClusterAddress_Get(CA.Cluster, N);
                if(GP->IsAvailable()) return GP;   // OK, a free cluster member found
           }
           // No "ordinary" member found, attempt a "corresponding" member
           for(ClusterNeighbor N = cm_North; N <= cm_NW; N = ClusterNeighbor((int)N + 1))
           {
                Y86Core* OCore = ByClusterAddress_Get(CA.Cluster, N); // This is an ordinary member
                ClusterAddress_t CB = OCore->ClusterAddress_Get();
                for(ClusterNeighbor NN = cm_North; NN <= cm_NW; N = ClusterNeighbor((int)NN + 1))
                {
                    Y86Core* GP = ByClusterAddress_Get(CB.Cluster, NN);
                    if(GP->IsAvailable() // OK, a free corresponding member found
                       && ( cm_Head  != GP->ClusterAddress_Get().Member)  // And, it not a free cluster head
                            ) return GP;
                }
                // Also, no " corresponding member" found
           }
           CPT = cpt_AnyCore;  // // The last chance: take any core
       }
       // Any core would do
       SC_GRIDPOINT_MASK_TYPE MyMask = 1;
       for (int i=0; i<MAX_GRIDPOINTS; i++)
       {
          Y86Core *GP = ByIDMask_Get(MyMask);
          if(GP->IsAvailable()) return GP;
          MyMask += MyMask;   // Get next core
       }
      return (Y86Core*)NULL ; // No available core found
}
*/

/*
    // Allocate a child core for the Parent core
    Y86Core* Y86Processor::
ChildAllocateFor(Y86Core* Parent, CorePreference_t CPT)
{
    Y86Core *GP =  ChildFindFor(Parent, CPT); ///< find a core for this parent
    if(GP)
        GP = GP->AllocateFor(Parent);
    return GP;
}

    // PreAllocate a child core for the Parent scGridPoint
    // param CPT the preferred core (clustered) type

    Y86Core* Y86Processor::
ChildPreAllocateFor(Y86Core* Parent, CorePreference_t CPT)
{
    Y86Core *GP =  ChildFindFor(Parent, CPT); ///< find a core for this parent
    if(GP)
        GP = GP->PreAllocateFor(Parent);
    return GP;
}
*/
    #endif //newdef
