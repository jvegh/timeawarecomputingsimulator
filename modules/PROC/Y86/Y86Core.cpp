/** @file Y86Core.cpp
 *  @brief Function prototypes for the SystemC based EMPA simulator
 *  Privides the Y86 functionality
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
//#include "Utils.h"

//#include "scTypes.h"
#include "Y86Core.h"
#include "AbstractProcessor.h"
#include "Y86Processor.h"
//#include "Plot.h"

/// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#define MAKE_LOG_TRACING    // We want to have trace messages
#define MAKE_SIGNAL_TRACING // Trace the external electronic signals
#include "Macros.h"
#include "scGridPoint.h"
#include <vector>
extern reg_struct meta_table[];
/*
 * Basic SystemC functionality of grid modules + grid topologic position handling
 */
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];
/*!
 * \brief Y86Core::Y86Core
 *
 */
//* \param StandAlone standalone event handling shall be implemented

Y86Core::Y86Core(sc_core::sc_module_name nm,
                   Y86Processor* Proc
                   , const GridPoint GP):    AbstractCore(nm, Proc, //ID, 
                                                          GP, false)
{
    Reset();
/*    string S = string(reg_table[0].name);
    vector <int32_t> in,out;
    Inspect(in,out);
    InspectSwitch(true);
    Inspect(in,out);*/
}// of Y86Core::Y86Core

void Y86Core::
Reset(void)
{
    AbstractCore::Reset();
    ConditionCode_Set(DEFAULT_CC);
}
Y86Core::~Y86Core()
{
}//Y86Core::~Y86Core

Y86Processor*
Y86Core::
Processor_Get(void)
{    return dynamic_cast<Y86Processor*>(AbstractCore::Processor_Get());}

#if 0
/*!
 * \fn Y86Core::CREATE_method
 * \brief   This method is called when a new QT is to be created.
 * At the time of calling, an QT-type scIGPMessage must be already available
 *
 */
void Y86Core::
CREATE_method(void)
{
    assert();
}
#endif

#ifdef newdef
    string Y86Core::
MetaInstructionName_Get(void)
{   return meta_table[this->msMeta.Code].name;}

    string Y86Core::
RegisterContentString_Get(reg_id_t R)
{
    ostringstream oss;
    int32_t W =  RegisterValue_Get((reg_id_t)R);
    oss << " ["  << string(reg_table[R].name) << "]=" << W;
    if((W<0) || (W>9)) oss << hex << "(0x" << RegisterValue_Get((reg_id_t)R ) <<  dec << ")";
    return oss.str();
}
    string Y86Core::
RegisterContentStrings_Get(SC_REGISTER_MASK_TYPE M, bool AlsoCC)
{
    ostringstream oss;
    int i = (reg_id_t)R_EAX;
            while(M)
              {
                  if(M & 1)
                  {
                      oss << RegisterContentString_Get((reg_id_t)i);
                  }
                  ++i; M /=2;
              }
            if(AlsoCC)
            {
                int32_t W =  RegCC_Get();
                oss << " [%ecc]=" << W;
                if(W) oss << hex << "(0x" << W <<  ")" << dec;
            }
    return oss.str();
}

/*    void Y86Core::
doQTERM(void)
{

}*/

/*    bool Y86Core::
doProcessQMessage(scIGPMessage* Message)
{
    return true;
}
*/

#endif

    /*!
 * \brief Y86Core::doFetchInstruction
 * This is the first stage of the two-stage execution processing:
 * fetch the instruction pointed out by fetchPC
 *
 * As the memory takes only MOD(4) addresses and provides 4-byte data,
 * the processor must think about the rest
 *
 * Called from FETCH_thread (maybe also from Reboot)
 * \return true if succeded
 */
    bool Y86Core::
doFetchInstruction(scHThread* H)
{
    // Prepare locale variables
    H->fetch.Stage.byte1 = 0;
    byte_t byte0;
    //   word_t dval;
    bool need_imm;  // If to read immediate
    bool need_regids; // If to read register IDs
//    fetch.Stage.ok1 = TRUE;
    H->fetch.Stage.immediate = 0;
    H->fetch.Stage.okc = TRUE;
   stat_t CoreStatus = STAT_AOK;
   uint32_t ftpc = H->fetchPC_Get();    // This is a fall-through PC for fetching
    uint64_t Instr = readInstrMem(ftpc);  // This will store at least 5, at max 8 bytes
    H->fetch.ExecutionTime = DMEMORY_READ_TIME;

    // Read 32 bits from the instruction memory to argument Instr, although not all bytes surely  used up
        byte0 = Instr & 0xFF; Instr = Instr >> 8;
    if(! byte0)
    {  // Transform the original 'halt' to 'Q_HALT'
        byte0 = HPACK(I_QT, Q_HALT);
    }
    H->fetch.Stage.OpCode = (itype_t) HI4(byte0);
    H->fetch.Stage.FunctCode = (alu_t) LO4(byte0);
    ftpc++;   // Now instruction code fetched
    need_regids =
    ( ( // The conventional intructions have their own high nibble
        (   (itype_t)H->fetch.Stage.OpCode == I_RRMOVL || (itype_t)H->fetch.Stage.OpCode == I_ALU
         || (itype_t)H->fetch.Stage.OpCode == I_PUSHL  || (itype_t)H->fetch.Stage.OpCode == I_POPL
         || (itype_t)H->fetch.Stage.OpCode == I_IRMOVL || (itype_t)H->fetch.Stage.OpCode == I_RMMOVL
         || (itype_t)H->fetch.Stage.OpCode == I_MRMOVL || (itype_t)H->fetch.Stage.OpCode == I_IADDL))
        || // Q-intructions uses this field as subcode
      (( IsMetaInstructionFetched(H))
        &&
        ((qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATER  ||
        (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_ALLOC
        )
      )
    );

    if (need_regids) {
//        fetch.Stage.ok1 = true;
        H->fetch.Stage.byte1 = Instr & 0xFF; Instr = Instr >> 8;   // Get next byte from instr
        ftpc++;
        H->fetch.Stage.RSource1 = (reg_id_t) HI4(H->fetch.Stage.byte1);
        H->fetch.Stage.RDest = (reg_id_t) LO4(H->fetch.Stage.byte1);
    }
    if(// This is 1-byte immediate, in position of registers
      ( IsMetaInstructionFetched(H))
              &&
               (   (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATE
                || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATEF || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATET
                || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CALL || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_WAIT
                || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_MUTEX
               )
      )
        {
//         fetch.Stage.ok1 = true;
         H->fetch.Stage.byte1 = Instr & 0xFF; Instr = Instr >> 8;  // Get next byte from instr
         H->fetch.Stage.immediate = H->fetch.Stage.byte1;  // Maybe overwritten by 4-byte immediate
             ftpc++;
        };
    // Check if we need the 1-byte immediate argument
    if((IsMetaInstructionFetched(H))
            && (((qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_ALLOC) || ((qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_WAIT)))
    { // This is exceptionally 1-byte operand
//        fetch.Stage.ok1 = true;
        H->fetch.Stage.byte1 = Instr & 0xFF; Instr = Instr >> 8;
        H->fetch.Stage.immediate = H->fetch.Stage.byte1;
    }
    need_imm =
    ( ( // The conventional intructions have their own high nibble
        (H->fetch.Stage.OpCode == I_IRMOVL || H->fetch.Stage.OpCode == I_RMMOVL || H->fetch.Stage.OpCode == I_MRMOVL ||
        H->fetch.Stage.OpCode == I_JMP || H->fetch.Stage.OpCode == I_CALL || H->fetch.Stage.OpCode == I_IADDL)
        )
    ||
    (( IsMetaInstructionFetched(H)) &&
    (   (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATER || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATE
     || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATEF || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CREATET
     || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_WAIT || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_MUTEX
     || (qt_t)H->fetch.Stage.FunctCode == (qt_t)Q_CALL )
    )
    );

    if (need_imm) {
        // We need at least 4 bytes, maybe we have it
        if((ftpc&~3) != (H->fetchPC_Get()&~3)) // In unfortunate case, another reading is needed
            Instr = readInstrMem(ftpc) ;  // This will store at least 5, at max 8 bytes
        H->fetch.ExecutionTime += DMEMORY_READ_TIME;
        H->fetch.Stage.immediate = Instr & 0xFFffFFff;
        H->fetch.Stage.okc = true;
//!!           fetch.Stage.immediate = readData32(ftpc);
//        FetchStage.okc = get_word_val(msCoreState->m, ftpc, &FetchStage.immediate);
        ftpc += 4;
    }
    H->fetch.NewPC = ftpc;
    // Find out if parallel fetch shall be issued
    H->fetch.Stage.okFetch = !(PC_MaybeAffected(H) // NOT potentially PC-changing intruction
              || ((I_QT == (itype_t)H->fetch.Stage.OpCode) // and not a QHALT
                   && ((Q_HALT == (qt_t)H->fetch.Stage.FunctCode)
                      || (Q_TERM == (qt_t)H->fetch.Stage.FunctCode))
                    ));
    return STAT_AOK == CoreStatus;
}//Y86Core::doFetchInstruction()

    /** @brief The basic ISA functionality is taken from Y86
     *
     * Added state store/restore
     * Error handling: strings are returned, rather than direct printing
     *
     */

     bool
     Y86Core::
 doExecuteInstruction(scHThread* H)
 {
     SC_WORD_TYPE //!!OldMem, OldVal,
             argA, argB;
     word_t val//, dval
             ;
//!!     LOG_TRACE_SOURCE("Executing instruction");
     //performance->instructionsInc();
         H->exec.ExecutionTime = EXECUTION_TIME;  // This is a base time
         H->PC_Affected_Set(false);
         msCoreStatus = STAT_AOK;   // Assume success
         ostringstream oss;  // For the instruction in the switch
         // Now branch on instruction
 //??        assert(!H->fetch.Pending);
 //??        assert(!H->fetch.Valid);
     switch (H->exec.Stage.OpCode) {
       case I_NOP: break;
          H->PC_Set( H->exec.PC );
      case I_RRMOVL:  // Both unconditional and conditional moves
//         if (!exec.Stage.ok1)
  //       { H->exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!reg_valid((reg_id_t)H->exec.Stage.RSource1))
         { msCoreStatus = STAT_REG; break;}
         if (!reg_valid((reg_id_t)H->exec.Stage.RDest))
         { msCoreStatus = STAT_REG; break;}
         // exec.Stage.
         val = RegisterValue_Get((reg_id_t)H->exec.Stage.RSource1);
 //         OldMem = RegisterValue_Get(ExecuteStage.lo1);
 //        if (cond_holds(RegCC_Get(), (cond_t)ExecuteStage.lo0))
                  RegisterValue_Set((reg_id_t)H->exec.Stage.RDest, //exec.Stage.
                                    val);
//!!             LOG_TRACE_HEXA("Reg[" +string(reg_table[H->exec.Stage.RDest].name) + "]<=", val );
//!!             PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
  //           LOG_TRACE_VALUE("Reg[" +string(reg_table[exec.Stage.lo1].name) + "]<=", //exec.Stage.
  //                   val );
          break;
       case I_IRMOVL:
//          if (!exec.Stage.ok1)
  //        {exec.Stage.CoreStatus = STAT_ADR; break;}
          if (!H->exec.Stage.okc)
          { msCoreStatus = STAT_INS; break;}
          if (!reg_valid((reg_id_t)H->exec.Stage.RDest))
          { msCoreStatus = STAT_REG; break;}
          //exec.Stage.
//!!                  OldReg = RegisterValue_Get(exec.Stage.lo1, false);
          RegisterValue_Set((reg_id_t)H->exec.Stage.RDest, H->exec.Stage.immediate);
//!!          LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.Stage.immediate );
//!!          PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
 //       //    mLastAddr = immediate; // Do no scroll to this address
          break;
       case I_RMMOVL:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
         if (!reg_valid((reg_id_t)H->exec.Stage.RSource1))
         { msCoreStatus = STAT_REG; break;}
          if (reg_valid((reg_id_t)H->exec.Stage.RDest) //|| (exec.Stage.hi1==R_ESV)
                  )
            H->exec.Stage.immediate += RegisterValue_Get((reg_id_t)H->exec.Stage.RDest,false);
 //         get_word_val(msCoreState->m, ExecuteStage.immediate, &OldMem);
 //!!         OldMem = readData32(exec.Stage.immediate);
 //         OldMem = MainMemory->DirectGet(exec.Stage.immediate);
            H->exec.val = RegisterValue_Get((reg_id_t)H->exec.Stage.RSource1,false);
 //         if (!set_word_val(msCoreState->m, ExecuteStage.immediate, ExecuteStage.val))
 //           return STAT_ADR;
            writeInstrMem(H->exec.Stage.immediate, H->exec.val);
 //!!         writeData32(exec.Stage.immediate, exec.val);
          oss << "0x" << hex << H->exec.Stage.immediate << dec;
//!!          LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", exec.val );

 //!!        PLOT_QT_MEMORY_DEBUG(this,exec.Stage.immediate,exec.val,true);
         //         PC_Set( ExecuteStage.PC);
          break;
       case I_MRMOVL:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
          if (!reg_valid((reg_id_t)H->exec.Stage.RSource1))
          { msCoreStatus = STAT_REG; break;}
           //!!
//!!          OldVal = RegisterValue_Get(exec.Stage.hi1,false);
          if (reg_valid((reg_id_t)H->exec.Stage.RDest) //|| (exec.Stage.lo1==R_ESV)
                  )
            H->exec.Stage.immediate += RegisterValue_Get((reg_id_t)H->exec.Stage.RDest,false);
          val = readInstrMem(H->exec.Stage.immediate);
//!!                  val = readData32(exec.Stage.immediate);
//!!                  PLOT_QT_MEMORY_DEBUG(this,exec.Stage.immediate,val,false);
 //         if (!get_word_val(msCoreState->m, exec.Stage.immediate, &exec.Stage.val))
 //!!            return STAT_ADR;
//!!           LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RSource1].name) + "]<=",val );
          RegisterValue_Set((reg_id_t)H->exec.Stage.RSource1, val,false);
//!!          PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.hi1, true);
          break;
       case I_ALU:
//          if (!exec.Stage.ok1)
  //        { exec.Stage.CoreStatus = STAT_ADR; break;}

          argA = RegisterValue_Get( (reg_id_t)H->exec.Stage.RSource1);
  /*        if((exec.Stage.lo1 == R_ESV) && (ParentMode_Get()==cvm_SUMUP) && (ExecuteStage.lo0==A_ADD))
            { // It is an exception: just latch the value for the parent and trigger addition
              OldMem = Parent_Get()->msICB->msPseudoRegisters->FromChild;
              Parent_Get()->msICB->msPseudoRegisters->FromChild = OldMem + ExecuteStage.argA;
            }
          else // It is just a normal operation
 */            if (reg_valid((reg_id_t)H->exec.Stage.RDest) //|| (exec.Stage.lo1==R_ESV)
                   )
               {
                  argB = RegisterValue_Get((reg_id_t)H->exec.Stage.RDest,false);
                  H->exec.val = compute_alu((alu_t)H->exec.Stage.FunctCode, argA, argB);
                  //!!OldVal = RegisterValue_Get(exec.Stage.lo1);
                  RegisterValue_Set((reg_id_t)H->exec.Stage.RDest, H->exec.val,false);
//!!                  LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.val );
                        //                 OldCC = RegCC_Get();
//                  PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
                  RegCC_Set(H, compute_cc((alu_t)H->exec.Stage.FunctCode, argA, argB));
               }
           break;
       case I_JMP:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
         // The fetch must recognize that no prefetch should happen
//??         assert(!H->fetch.Pending);
//??         assert(!H->fetch.Valid);
         H->PC_Affected_Set(true);
         if (cond_holds(RegCC_Get(H), (cond_t)H->exec.Stage.FunctCode))
         {
  //           PLOT_EXEC_BALL(sc_time_stamp(), exec.ExecutionTime);
             H->exec.PC = H->exec.Stage.immediate; // Will continue there
             H->fetch.PC = H->exec.PC; // but pick it up first
 //            fetch.NewPC =  exec.Stage.immediate;
 //            exec.PC = fetch.PC;
         }
           break;
       case I_CALL:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
         // The fetch must recognize that no prefetch should happen
//??         assert(!H->fetch.Pending);
//??         assert(!H->fetch.Valid);

             H->PC_Affected_Set(true);
             H->exec.val = RegisterValue_Get(R_ESP) -4;   // The memory for the return address
//!!             OldMem = MainMemory->DirectGet(exec.val);
             RegisterValue_Set(R_ESP,  H->exec.val);
//!!             LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.val );
             oss << "0x" << hex << H->exec.val << dec;
//!!             LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", fetch.PC );
//             PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
//             writeData32(exec.val, fetch.PC);
  //           PLOT_QT_MEMORY_DEBUG(this,exec.val,fetch.PC,true);
 //            MainMemory->DirectPut(exec.val, fetch.PC);
             writeInstrMem(H->exec.val, H->fetch.PC);
             H->exec.PC = H->exec.Stage.immediate; // Will continue there
             H->fetch.PC = H->exec.PC; // but pick it up first
            break;
       case I_RET:
           // Return Instruction.  Pop address from stack
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
         // The fetch must recognize that no prefetch should happen
           H->PC_Affected_Set(true);
           H->exec.dval = RegisterValue_Get(R_ESP);
           H->exec.val = readInstrMem(H->exec.dval);
//!!           exec.val = readData32(exec.dval);
            H->exec.dval += 4;
           RegisterValue_Set(R_ESP, H->exec.dval);

//!!           LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval );
    //       PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
      //     PLOT_QT_MEMORY_DEBUG(this,exec.dval,exec.val,false);
           H->exec.PC = H->exec.val; // Will continue there
           H->fetch.PC = H->exec.PC; // but pick it up first
         break;
        case I_PUSHL:
//            if (!exec.Stage.ok1)
  //              {exec.Stage.CoreStatus = STAT_ADR; break;}
            if (!reg_valid((reg_id_t)H->exec.Stage.RSource1))
                { msCoreStatus = STAT_REG; break;}
            H->exec.val = RegisterValue_Get((reg_id_t)H->exec.Stage.RSource1);   // The register content to push
            H->exec.dval = RegisterValue_Get(R_ESP) - 4;
//!!            OldMem = readInstrMem(exec.dval);
//!!           OldMem = MainMemory->DirectGet(exec.dval);
            RegisterValue_Set(R_ESP,H->exec.dval);
 //!!           LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval );
            oss << "0x" << hex << H->exec.dval << dec;
 //!!           LOG_TRACE_HEXA("Mem[" + oss.str() + "]<=", exec.val );
//!!           PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
//!!           PLOT_QT_MEMORY_DEBUG(this,exec.dval,exec.val,true);
//!!           writeData32(exec.dval, exec.val );
            writeInstrMem(H->exec.dval, H->exec.val);
            break;
     case I_POPL:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!reg_valid((reg_id_t)H->exec.Stage.RSource1))
         { msCoreStatus = STAT_REG; break;}
//!!            OldMem = RegisterValue_Get(H->exec.Stage.hi1);  // The old register content
            H->exec.dval = RegisterValue_Get(R_ESP) ;        // Where ESP points to
            H->exec.val = readInstrMem(H->exec.dval);
            H->exec.dval += 4;
            RegisterValue_Set(R_ESP, H->exec.dval);       // Update ESP
//!!            LOG_TRACE_HEXA("Reg[" +string(reg_table[R_ESP].name) + "]<=",exec.dval);
//!!            exec.val = readData32(exec.dval);
            RegisterValue_Set((reg_id_t)H->exec.Stage.RSource1, H->exec.val);
//!!            LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RSource1].name) + "]<=",exec.val );
//!!            PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << R_ESP, true);
         break;
 /*      case I_LEAVE:
         if (!exec.Stage.okc)
         {exec.Stage.CoreStatus = STAT_INS; break;}
           // The fetch must recognize that no prefetch should happen
         assert(!fetch.Pending);
         assert(!fetch.Valid);
           ExecuteStage.dval = get_reg_val(Registers_Get(), R_EBP);
           set_reg_val(Registers_Get(), R_ESP, ExecuteStage.dval+4);
            if (!get_word_val(msNewCoreState->m, ExecuteStage.dval, &ExecuteStage.val))
              return STAT_ADR;
           set_reg_val(Registers_Get(), R_EBP, ExecuteStage.val);
           PC_Set( ExecuteStage.PC );
           break;
 */      case I_IADDL:
//         if (!exec.Stage.ok1)
  //       {exec.Stage.CoreStatus = STAT_ADR; break;}
         if (!H->exec.Stage.okc)
         { msCoreStatus = STAT_INS; break;}
         if (!reg_valid((reg_id_t)H->exec.Stage.RDest))
         { msCoreStatus = STAT_REG; break;}
           argB = RegisterValue_Get((reg_id_t)H->exec.Stage.RDest);
           H->exec.val = argB + H->exec.Stage.immediate;
           RegisterValue_Set((reg_id_t)H->exec.Stage.RDest, H->exec.val);
//!!           LOG_TRACE_HEXA("Reg[" +string(reg_table[exec.Stage.RDest].name) + "]<=",exec.val );
//           PLOT_QT_REGISTER_MASK_DEBUG(this, 1 << exec.Stage.lo1, true);
           RegCC_Set(H, compute_cc(A_ADD, H->exec.Stage.immediate, argB));
           break;
       default: // Some unrecognized intruction received
           msCoreStatus = STAT_INS;
       }
     return STAT_AOK == msCoreStatus;
}

      bool Y86Core::
IsMorphingInstructionFetched(scHThread* H)
{
    return I_QT == H->fetch.Stage.OpCode;
}

#ifdef newdef
     /*!
     * \brief Y86Core::doSetQTAddresses
     * \param Parent core of the QT to be created
     *
     * Set the fetch and excute PCs after creating a QT:
     * and the parent skips the outsourced code (sets the PC correspondingly)
     */
    void  Y86Core::
doSetQTAddresses(scGridPoint* Parent)
{
    if(Parent)
    {
        Y86Core* Y86Parent = dynamic_cast<Y86Core*>(Parent);
        assert(Y86Parent);  // Maybe it is not a proper ancestor
        mQT.Offset = Y86Parent->PC_Get();
     // Calculate the total length of the code fragment
        mQT.Length = Y86Parent->msMeta.Address // Address of the QTERM
                    //Parent->exec.Stage.immediate
                    - mQT.Offset;  // Address of the CREATE
//        PC_Set(mQT.Offset + QCreate_Size);
        fetchPC_Set(mQT.Offset + QCreate_Size);
        exec.PC = fetch.PC;
        fetch.NewPC = fetch.PC; // Fetch the new instruction from here
        Y86Parent->exec.PC = Y86Parent->exec.Stage.immediate + QTerm_Size;
        Y86Parent->fetch.PC = Y86Parent->exec.PC ;
//        Y86Parent->PC_Set(Y86Parent->exec.Stage.immediate + QTerm_Size);   // The parent must skip the ode
        //??Parent->fetchPC_Set(Parent->PC_Get());
        Y86Parent->fetch.NewPC = Y86Parent->fetch.PC;
    }
    else
    { // The parent is the processor itself
        mQT.Offset = Processor_Get()->PC_Get(); // The offset will be where processor PS points to
        PC_Set(mQT.Offset + QCreate_Size); // The PC is set after the QCreate
        Processor_Get()->PC_Set(Processor_Get()->PC_Get() + QCreate_Size);
        mQT.Length = FMAX_MEMORY_SIZE - mQT.Offset- QCreate_Size;
    }
}

    /*!
     * \brief Y86Core::doSkipQTCode
     *
     * In the case of conditional code, on false condition just skip the code
     */
    void Y86Core::
doSkipQTCode(void)
{
   fetch.NewPC = msMeta.Address + QTerm_Size;// Just skip the code, as were NOP
   fetch.PC = fetch.NewPC;
   fetch.Pending = false; // Maybe these two not needed
   fetch.Valid = false;
}

    Y86Core* Y86Core::
AllocateFor(Y86Core* Parent)
{
   return dynamic_cast<Y86Core*>(AbstractCore::AllocateFor(Parent));
}
    Y86Core* Y86Core::
PreAllocateFor(Y86Core* Parent)
{
   return dynamic_cast<Y86Core*>(AbstractCore::PreAllocateFor(Parent));
}

    /*!
     * \brief AbstractCore::RegisterValue_Get
     * \param R code of the register to be read
     * \param Performance If measure performance
     * \return Content of register R
     */
  SC_WORD_TYPE Y86Core::
RegisterValue_Get(reg_id_t R, bool Performance)
{
//       if(Performance) performance->registerRead();
//  return REGS.at(R);
      return AbstractCore::RegisterValue_Get((uint8_t) R, Performance);
#if 0
      if(R<=R_EDI)
        return registers->getValue(R, Performance);
                //(SC_WORD_TYPE) get_reg_val((mem_t)Registers_Get(), (reg_id_t) R);
        else if(R==R_ESV)
        {
 /*         if(Cloning)
          {// Read the data to send for the parent
              return msICB->msPseudoRegisters->ForParent;
          }
          else
              switch((int)ESVState_Get())
              {
              case cm_Child:
                  return msICB->msPseudoRegisters->FromParent;
                  break;
              case cm_ParentPRE:
                  return  msICB->msPseudoRegisters->FromParent;
                  break;
              case cm_ParentPOST:
                  return msICB->msPseudoRegisters->FromChild;
                  break;
              case cm_Normal:
                  return msICB->msPseudoRegisters->FromChild;
                  break;
              }*/
        }
       return 0;
#endif
    }
                 /*!
                    * \brief AbstractCore::RegisterValue_Set Set value of a register
                    * \param R Code of register
                    * \param V New content of register
                    */
  void Y86Core::
RegisterValue_Set(reg_id_t R, SC_WORD_TYPE V, bool Performance)
{
      AbstractCore::RegisterValue_Set((uint8_t) R, V, Performance);
//      REGS[R] = V;
#if 0
                       if(Performance) performance->registerWrite();

                 if(R<=R_EDI)
                     registers->setValue(R, V);
                     //**     set_reg_val((mem_t)Registers_Get(), (reg_id_t) R, (word_t)V);
                 else if(R==R_ESV)
                   { 	// Want to set %esv
 /*                    if(Cloning)
                     { // Store the data received from the child
                     msICB->msPseudoRegisters->FromChild = V;
                     }
                     else
                         switch((int)ESVState_Get())
                         {
                         case cm_Child:
                             msICB->msPseudoRegisters->ForParent = V;
                             break;
                         case cm_ParentPRE:
                             msICB->msPseudoRegisters->ForChild = V;
                             break;
                         case cm_ParentPOST:
                             msICB->msPseudoRegisters->ForParent = V;
                             break;
                         case cm_Normal:
                             msICB->msPseudoRegisters->ForParent = V;
                             break;
                         }*/
                   }
#endif // 0
}  ///< Set value of the private register


    /* !
     * \brief Y86Core::doInspect
     *
     * Make the actual inspection; overwrite in scGridPoint
     * \param in    input data vector
     * \param out   output data vector
     */
/*    void Y86Core::
doInspect(vector<int32_t>& in, vector<int32_t>& out)
{
        std::cerr << "Y86Core Inspecting, received " << in.size();
}*/
  #endif //newdef
