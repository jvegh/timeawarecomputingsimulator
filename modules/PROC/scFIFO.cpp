/** @file scFIFO.cpp
 *  @brief Function prototypes for the EMPA simulator, scFIFO
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scFIFO.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
//#define DEBUG_PRINTS    // Print general debug messages
//#define DEBUG_PENDING_OPERATIONS // Print pending, queued, executing, executed operations
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

// This file defines the FIFOs used in EMPA data communication

extern bool UNIT_TESTING;
/*
#ifdef __cplusplus
extern "C" {
#endif

#include "isaE.h"
#ifdef __cplusplus
}
#endif
*/
//!!extern reg_struct meta_table[]; // Just to print by name
using namespace std;

  // This is the FIFO for the metaevents
  // This FIFO is priority ordered; the new events are inserted in the queue
  // according to their priority
  /*!
   * \brief MetaEvent_fifo::MetaEvent_fifo
   *
   * When a metainstruction is found by an scCore during prefetching, it (by using its 'Meta' signal)
   * the control is transferred to the scProcessor, and the processing in that scCore is suspended
   * until scProcessor processes the metainstruction and notifies scCore about it
   *
   * The scProcessor processes metainstructions sequentially, while the scCores can send
   * servicing requests in a 'random' way. To assure sequential processing and provide handling
   * of priorities of the different metainstructions, the input FIFO contains the metainstructions
   * ordered by their priority. I.e. inserting new metainstructions involves priority checking,
   * processing means immediate execution. After executing the metainstruction, the scProcessor
   * resumes the sending scCore.
   *
   * \param name the name of the FIFO
   */
  MetaEvent_fifo::MetaEvent_fifo(const sc_module_name& name)
      : sc_core::sc_channel(name), num_elements(0), first(0)
{
   DEBUG_PRINT(sc_time_stamp() << '|' << name << " Created ");
//!!   for(int i = 0; i<num_elements; i++) data[i].Code = Q_ERR;
}

  /// write a morphing command to the FIFO
    void MetaEvent_fifo::
  write(MetaEvent_Transfer_Type T) {
    if (num_elements == max)
             wait(read_event);
    dowrite(T);
    write_event.notify(SC_ZERO_TIME);
  }

    /// write a morphing command to the FIFO
    void MetaEvent_fifo::
  dowrite(MetaEvent_Transfer_Type T) {
    int i=0; int index;
    while(i<num_elements)
    { // Check all elements
        index = (first+i) % max;  // This is the next position in the FIFO
        if(NewPriorityHigher(T,data[index]))
        break;// Insert here
         i++;
    }
    index = (first+i) % max;  // This is the position of inserting in the FIFO
    // Anyhow, we must insert new item here
    DEBUG_PRINT("Inserting event " << meta_table[T.Code].name << " @" << index);
  // Maybe we must shift the queue elements
    if(i<num_elements)
    {// We ned to shift the elements above this position
      int j = num_elements;
      while( j>0)
        {
          data[(first + j) % max] = data[(first + j-1) % max];
          DEBUG_PRINT("Shifted (" << ((first + j) % max) << ") <= (" << ((first + j-1) % max) << ')');
          j--;
        }
    }
    data[index] = T; // Anyhow, insert the new item here
    ++ num_elements;
/*    DEBUG_PRINT_SC(" Metainstruction " << T.Code << " (" <<  meta_table[T.Code].name << ") written @" << (index)  << " from core#" << T.CoreID);
    DEBUG_PRINT_SC(" WQueue is: [" << first << " of " << num_elements << "] "
                   << data[0].Code << ',' << data[1].Code << ',' << data[2].Code << ',' << data[3].Code << ','<< data[4].Code << ',' << data[5].Code << ','
                                      << data[6].Code << ',' << data[7].Code << ',' << data[8].Code << ','<< data[9].Code );
*/    }

    /*!
     * This function defines in which order metainstructions are executed
     * \param New the instruction to be inserted
     * \param Old the old instruction to compare its priority to
     * \return true if New is to be inserted before Old
     */
    bool MetaEvent_fifo::
  NewPriorityHigher(MetaEvent_Transfer_Type New, MetaEvent_Transfer_Type Old)
  {
        return New.Code < Old.Code;
  }
/// Read a metainstruction from the FIFO (blocking)
    void MetaEvent_fifo::
  read(MetaEvent_Transfer_Type &T){
    if (num_elements == 0)
       wait(write_event);
    doread(T);
    read_event.notify(SC_ZERO_TIME);
  }

    void MetaEvent_fifo::
  doread(MetaEvent_Transfer_Type &T){
    T = data[first];
    -- num_elements;
/*    DEBUG_PRINT_SC(" Metainstruction " << T.Code << " (" <<  meta_table[T.Code].name << ") read @" << first  << " from core#" << T.CoreID);
    DEBUG_PRINT_SC(" RQueue is: [" << first << " of " << num_elements << "] "
                   << data[0].Code << ',' << data[1].Code << ',' << data[2].Code << ',' << data[3].Code << ','<< data[4].Code << ',' << data[5].Code << ','
                                      << data[6].Code << ',' << data[7].Code << ',' << data[8].Code << ','<< data[9].Code );
*/    first = (first + 1) % max;
  }
