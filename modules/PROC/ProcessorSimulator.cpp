/** @file ProcessorSimulator.cpp
 *  @brief Function prototypes for the  scProcessor simulator
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */
#include "scProcessor.h"

#include "Stuff.h"
#include "Project.h"
#include "BasicConfig.h"
#include "scGridPoint.h"
//#include "QStuff.h"
#include "scHThread.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
//#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
#define MAKE_LOG_TRACING
//#define MAKE_TIME_BENCHMARKING
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"
#include "ProcessorSimulator.h"
#include "AbstractProcessor.h"
#include "scClusterBusMemorySlow.h"
//#include "scqTreeModel.h"

//#include "scGridPoint.h"    // Debug
//#include "scIGPMessage.h"   //  only


extern bool UNIT_TESTING;	// Whether in course of unit testing
extern scClusterBusMemorySlow *MainMemory;
//EMPAPlotter *Plot;
//extern reg_struct reg_table[R_ERR+1];
//qQDir dir;
// This is the logging facility provided by Qt.
//
// http://www.qtcentre.org/threads/19534-redirect-qDebug()-to-file
//
// qDebug is disabled in release mode
// http://beyondrelational.com/modules/2/blogs/79/Posts/19245/how-to-put-logging-in-c-application-using-qt50.aspx

/*!
 * @class ProcessorSimulator
 * @brief The equivalent of the electronically implemented simulator
 * The simulator manipulates \see scProcessor, scCore, etc
 */

/*!
 * @brief	Creates an EMPA-aware @ref ProcessorSimulator, with AbstractProcessor
 * 
 *  * @verbatim
 *  |scSimulator
 *  |../ProcessorSimulator
 *  |..../XXProcessorSimulator
 * @endverbatim
 */

ProcessorSimulator::ProcessorSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone)
    : scSimulator( nm, argc, argv, false)
    ,msStepwiseMode(false)  // Initially, do not step
//    ,msSuspended(false) // Initially not suspended
{
#ifdef newdef
     // This file will be sought in the system, user and project directories, in this order

    if(StandAlone)
    {
        // Create the main memory, belonging to the systems (presently only one processor)
        MainMemory = new scClusterBusMemorySlow("MainMemory", 0x00, FMAX_MEMORY_SIZE-1, 2);
 //       MainMemory->clock(C1);    // The far memory uses clock; wait states measured in bus clock periods
        // Now specials can be loaded with special points; will be considered by the topology
        HandleSpecials();   /// Populate mSpecials with the found special scNeurers (from mSpecial)
 //!!  handle optional input file     MainMemory = new scMemory("Main_Memory", filename);
        msProcessor = new AbstractProcessor("Proc", mSpecials, true);
 //       msProcessor->clock(C1);
        /// Read the setting files at different levels; most not when we have a processor
        readSettings();
        SetupHierarchies();

//        MainMemory->Y86loadHexFile(filename);
    }
    // As long as nothing more than sending out event in the derived simulator class
    SC_THREAD(SSUSPEND_thread);
        sensitive << EVENT_SIMULATOR.SUSPEND;
    SC_THREAD(SRESUME_thread);
        sensitive << EVENT_SIMULATOR.RESUME;
    SC_THREAD(SSTART_thread);
        sensitive << EVENT_SIMULATOR.START;
    SC_THREAD(SSTOP_thread);
        sensitive << EVENT_SIMULATOR.STOP;
    SC_THREAD(SHALT_thread);
        sensitive << EVENT_SIMULATOR.HALT;
    SC_METHOD(SInitialize_method);
/*    SC_METHOD(clock_method);
        sensitive << C1;  */

//    Plot = new EMPAPlotter(msProcessor, FileNameRoot.c_str());
#endif //newdef
 }


ProcessorSimulator::~ProcessorSimulator()
{
//    PrintFinalReport(msProcessor);

//    if(Plot){ delete Plot; Plot = 0;}
    std::cerr << simulation_name << " scSimulator total time "
              << m_sum_time.count()/1000/1000 << " msecs" << std::endl;

}

    AbstractProcessor* ProcessorSimulator::
Processor_Get()
{ return dynamic_cast<AbstractProcessor*> (scqSimulator::Processor_Get());}
 #ifdef newdef
void ProcessorSimulator::
SetupHierarchies(void)
{
//??    mModuleTree = new scqTreeModel(msProcessor);
//    for(int i = 0; i < GRID_SIZE_X; i++)
//       for(int j = 0; i < GRID_SIZE_X; i++)
//            if()

    SC_GRIDPOINT_MASK_TYPE ActivePoints = Processor_Get()->AllocatedMask_Get();
    unsigned int index = 0;
    while(ActivePoints)
    {
     if(ActivePoints&1)
     {
         scGridPoint* GP = Processor_Get()->ByID_Get(index);
         assert(GP);
         cerr << "Active " << GP->Name_Get() << "==" << Processor_Get()->StringOfClusterAddress_Get(GP) << "\n";
     }
     ActivePoints /= 2; index++;
    }

//    scqTreeModel* mClusterTree;

}
 
/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
void ProcessorSimulator::
SInitialize_method(void)
{
  DEBUG_PRINT_OBJECT("Initialize_method started" );
  Processor_Get()->EVENT_PROCESSOR.START.notify(SC_ZERO_TIME);
  //Processor_Get()->EVENT_PROCESSOR.HALT.notify();
//  EVENT_SIMULATOR.HALT.notify(SC_ZERO_TIME);
}

 
 /*               vector <int32_t> in,out;
                GP->Inspect(in,out);
                GP->InspectSwitch(true);
                GP->Inspect(in,out);*/

 /*!
 * \brief ProcessorSimulator::SUSPEND_thread
 * Since the simulator is the outmost object of the system, it must be suspended for the time of the work
 * 
 */
void ProcessorSimulator::
SSUSPEND_thread(void)
{
//   DEBUG_PRINT_OBJECT("SSUSPEND_thread started");
  while(true)
    {
        wait(EVENT_SIMULATOR.SUSPEND);
        DEBUG_EVENT("RCVD EVENT_SIMULATOR.SUSPEND");
        if(Processor_Get()->Halted_Get())
        {
            LOG_INFO("Attempted to suspend a halted processor");
        }
        else
        {
         Processor_Get()->EVENT_PROCESSOR.SUSPEND.notify();
          msSuspended = true;
        }
    }
}

    void ProcessorSimulator::
SRESUME_thread(void)
{
 //  DEBUG_PRINT_OBJECT("SRESUME_thread started");
   while(true)
    {
        wait(EVENT_SIMULATOR.RESUME);
        DEBUG_EVENT("RCVD EVENT_SIMULATOR.RESUME");
        if(Processor_Get()->Halted_Get())
        {
            LOG_INFO("Attempted to suspend a halted processor");
        }
        else
        {
            Processor_Get()->EVENT_PROCESSOR.RESUME.notify();
            msSuspended = false;
        }
    }
}

/*!
 * \brief ProcessorSimulator::SSTART_thread
 * This thread is waiting for a START_event and processes it
 */
    void ProcessorSimulator::
SSTART_thread(void)
{
    DEBUG_PRINT_OBJECT("SSTART_thread started");
    while(true)
    {
        DEBUG_EVENT("WAIT EVENT_SIMULATOR.START");
        wait(EVENT_SIMULATOR.START);
        DEBUG_EVENT("EVENT_SIMULATOR.START");
        if(!Processor_Get()->Halted_Get())
        {
         LOG_INFO("Attempted to start a running processor");
        }
        else
        {
            Processor_Get()->EVENT_PROCESSOR.START.notify();
        }
    }

}

    /*!
     * \fn ProcessorSimulator::SSTOP_thread
     * This thread is waiting for a STOP_event and processes it
     */
        void ProcessorSimulator::
    SSTOP_thread(void)
    {
 //      DEBUG_PRINT_OBJECT("SSTOP_thread started");
        while(true)
        {
            wait(EVENT_SIMULATOR.STOP);
            DEBUG_EVENT("RCVD EVENT_SIMULATOR.STOP");
            if(Processor_Get()->Halted_Get())
            {
              LOG_INFO("Attempted to stop at a running processor");
            }
            else
            {
              Processor_Get()->EVENT_PROCESSOR.STOP.notify();
            }
        }

    }
/*!
 * \brief ProcessorSimulator::HALT_thread
 * This thread is waiting for a HALT_event and processes it
 */
    void ProcessorSimulator::
SHALT_thread(void)
{
        while(true)
        {
            wait(EVENT_SIMULATOR.HALT);
            DEBUG_EVENT("RCVD EVENT_SIMULATOR.HALT");
        }
/*   DEBUG_PRINT("SHALT_thread started");
    while(true)
    {
        wait(EVENT_SIMULATOR.HALT);
        DEBUG_EVENT("RCVD EVENT_SIMULATOR.HALT");
        if(Processor_Get()->Halted_Get())
        {
            LOG_INFO( "Attempted to halt a halted processor");
        }
        else
        {
            if(!Processor_Get()->Halted_Get())
            {
                Processor_Get()->EVENT_PROCESSOR.HALT.notify();
                LOG_INFO( "Processor halted by the simulator");
            }
            LOG_INFO( "Simulation halted");
        }
    }
    */
}

/*!
 * \brief ProcessorSimulator::HandleSpecials
 *
 * Different special-purpose scGridPoints may have to be added to the grid,
 * they are handled here
 */
    void ProcessorSimulator::
HandleSpecials(void) // Belongs to the constructor
{
// Handle scGridPoints in mSpecial that need special handling
    size_t size = mSpecials.size();
    if(!size)   return; // No special points
}

/*

    Plot = new EMPAPlotter(msProcessor, FileNameRoot.c_str());
    */



    void ProcessorSimulator::
Setup(void)  // Belongs to the constructor
{
}

string ProcessorSimulator::
PrologText_Get(void)
{
ostringstream oss;
string SG = "Simulator";
oss  << '@' << StringOfTime_Get()  <<": " << SG.c_str();
 oss << '%';
//    while(oss.str().size()<24) oss << ' ';
return oss.str();
}


#endif //newdef

void ProcessorSimulator::
    writeSettings(void)
    {}
#if 0
/*
 * When the simulator is running, the only way to access it it sending a message and
 * the SystemC suspends its processing
 */
    /**
     * @brief Print a summary report at the end of processing
     *
     * Print a summary table, which contains statistics of operation
     * @return void
     */
    void ProcessorSimulator::PrintFinalReport(scProcessor *Proc)
    {
      if(!UNIT_TESTING)
      {
    //  Processor* Proc = Processor_Get();
    //  unsigned int tmemorycycles = 0;
      qInfo() << "\nSummary table";
      qInfo().noquote().nospace() << "Core(no)    " << "Instr " << "MemR  "  << "MemW  "  << " Tmemo " << "  Tftch " << "  Texec " << "  Twait  "  << " TTime " << "Payl% " << "Util%" ;

      // These will be used for summing
      uint64_t  tinstructions=0, // Executed instructions
              tmemoryreads, // Memory accesses
              tmemoryrites, // Memory accesses
              tdeepwaittime, // Waiting at low power
              tpayloadtime=0, // Payload % (tfecthtime+texextime)/ttotaltime)
              tutilizationtime=0; // utilization % (ttotaltime/simulationtime);
    sc_time tfetchtime = SC_ZERO_TIME; // Intruction fetch time
    sc_time texectime = SC_ZERO_TIME; // The total time to calculate
    sc_time twaittime = SC_ZERO_TIME; // Just waiting
    sc_time tmemtime = SC_ZERO_TIME; // Waiting for memory
    sc_time ttotaltime = SC_ZERO_TIME; // Total time being used

      int NoOfCores = Proc->NoOfCores_Get();
      int SimTime = sc_core::sc_time_stamp().value(); if(!SimTime) ++SimTime;
      int CoreUtilization[NUMBER_OF_CORES]; int counter = 0;
      for(int i = 0; i < NoOfCores; i++)
      {
        scCore* C = Proc->CoreByID_Get(i);
        if(C->InstructionsExecuted_Get() )//|| C->MetaIntructionsExecuted_Get())
//        if(C->FetchTime_Get()+C->ExecTime_Get())
        { // The core was active during this run
          counter++;
          tinstructions += C->InstructionsExecuted_Get();
          tmemoryreads += C->DataMemoryRead_Get(); // Memory accesses
          tmemoryrites += C->DataMemoryWrite_Get(); // Memory accesses
          tmemtime += C->MemoryTime_Get();
          tfetchtime += C->FetchTime_Get();
          texectime += C->ExecTime_Get();
          twaittime += C->WaitTime_Get();
          sc_time TotalTime = C->MemoryTime_Get() + C->FetchTime_Get() + C->ExecTime_Get() + C->WaitTime_Get();
 /*         tmemoryaccesses += C->MemoryCycles_Get();
          tdeepwaittime += C->DeepWaitTime_Get();
          int TotalTime = C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get();
          ttotaltime += TotalTime;
          int PayloadUtilization = (C->FetchTime_Get()+C->ExecTime_Get())*100/TotalTime + 0.49;
          tpayloadtime+= (C->FetchTime_Get()+C->ExecTime_Get());
     //     int LowEnergyUtilization = (C->DeepWaitTime_Get())*100/TotalTime + 0.49;
          int ThisUtilization = (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get())*100/(SimTime/1000) + 0.49;
          tutilizationtime += (C->FetchTime_Get()+C->ExecTime_Get()+C->ControlTime_Get()+C->WaitTime_Get()+C->DeepWaitTime_Get());
          CoreUtilization[i] = ThisUtilization;
          */
          ostringstream LogInfo;
            LogInfo << "Core(" << std::setw(2) << std::setfill('0') << i << ")  "
          << std::setw(6) << setfill(' ')
          << std::setw(6) << C->InstructionsExecuted_Get() // No of instruction executed by this core
          << std::setw(6) << C->DataMemoryRead_Get() // No of times the memory accessed
          << std::setw(6) << C->DataMemoryWrite_Get() // No of times the memory accessed
          << std::setw(6) << sc_time_to_nsec(C->MemoryTime_Get(),2) // Time of memory access
          << std::setw(6) << sc_time_to_nsec(C->FetchTime_Get(),2) // Time of fetching
          << std::setw(6) << sc_time_to_nsec(C->ExecTime_Get(),2) // Time of executing
          << std::setw(6) << sc_time_to_nsec(C->WaitTime_Get(),2) // Time of waiting
          << std::setw(6) << sc_time_to_nsec(TotalTime,2) // Time of waiting
/*          << std::setw(6) << C->FetchTime_Get()
          << std::setw(6) << C->ExecTime_Get()
          << std::setw(6) << C->ControlTime_Get()
          << std::setw(6) << C->WaitTime_Get()
          << std::setw(6) << C->DeepWaitTime_Get()
          << std::setw(6) << TotalTime // Total time being used by this core
          << std::setw(6)  << PayloadUtilization // Utilization% (tfecthtime+texextime+tcontroltime)/tutilizationtime
          << std::setw(6)  << ThisUtilization // Payload % (tfecthtime+texextime)/ttotaltime)
*/              ;
          qInfo() << LogInfo.str().c_str();
    //      tutilizationtime += ThisUtilization;
    //      tpayloadtime += PayloadUtilization;
         }
      }
/*
 *       ostringstream LogInfo;
      if(counter)
      {
        LogInfo << "Total     "  << setfill(' ')
        << std::setw(6) << tinstructions
        << std::setw(6) << tmemoryaccesses
        << std::setw(6) << tfetchtime
        << std::setw(6) << texectime
        << std::setw(6) << tcontroltime
        << std::setw(6) << twaittime
        << std::setw(6) << tdeepwaittime
        << std::setw(6) << ttotaltime
        << std::setw(6) << (int)(tpayloadtime*100/ttotaltime+ 0.49)
        << std::setw(6) << (int)(tutilizationtime*100/(SimTime/1000)/counter + 0.49);
           ;

      LogInfo  << "\nSimTime   " << std::setw(6) << //QString("%1").arg
                                     (sc_core::sc_time_stamp().value()/1000);
      qInfo() << LogInfo.str().c_str() ;
      int Max = 0; int MyVal; double Avg = 0; double Weights = 0;
      for(int i=0; i<counter; i++)
      {
          MyVal = CoreUtilization[i];
          if(MyVal>Max) Max = MyVal;
          Avg += i*MyVal;
          Weights += MyVal;
      }
      Avg = Avg/Weights;
      int AvgInt = Avg * 100;
      double Norm = 50./Max; QString a("%1");
      qInfo().noquote() << "\nSpeed gain =" << QString::number(double(texectime+tfetchtime)/(SimTime/1000),'f',2);
      qInfo().noquote() << QString("Average core number = %1").arg(AvgInt/100.,5);
      qInfo().noquote() << "Core utilization frequency distribution\n";
      for(int i=0; i< counter; i++)
      {
          int j = CoreUtilization[i]*Norm;
          qInfo().noquote() << i << a.arg(CoreUtilization[i],j+1,10,QChar('>'));
      }
      qInfo().noquote() << "\n";

      plotfile << "$};\n";
      plotfile << "\\node[right,above] at ( 0.9 cm,.5cm) {";
      plotfile << "$U=" << tutilizationtime/counter << "\\%,~P=" << tpayloadtime/counter;
      plotfile << "\\% $};\n";
    }
    //  cerr << LogInfo.str();
    */
      } // UNIT_TESTING
    }
 #endif //other

