// The configured options and settings for processor-related SystemC developments
// Will be used to configure modules/Proc/include/ProcConfig.h
/** @file ProcConfig.h
 *  @brief Configuration information for the EMPA processor
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

// Take over basic configuration
#include "BasicConfig.h"
// Define settings (.ini) files as a string list
#define INI_FILES "Simulator"

// List keywords legal in 'Simulator.ini'
#undef  INI_SIMULATOR_KEYWORDS
#define INI_SIMULATOR_KEYWORDS "Version"


// Core-specific operational characteristics
#define  CORE_GRID_SIZE_X  GRID_SIZE_X
#define  CORE_GRID_SIZE_Y  GRID_SIZE_Y
#define  SC_CORE_ID_TYPE   SC_GRID_ID_TYPE
#define  CORE_BUS_WIDTH    GRID_BUS_WIDTH
#define  MAX_NUMBER_OF_CORES CORE_GRID_SIZE_X*CORE_GRID_SIZE_Y

// Choose if to make detailed textual annotation of operations (slows down)
#define USE_PERFORMANCE_ANNOTATIONS
// If to measure performance (handles counters; slows down)
#define MEASURE_PERFORMANCE
// Choose if to prepare performance diagram plot (slows down)
#define PREPARE_PERFORMANCE_DIAGRAM
// Length of one "instruction time"
#define PLOT_TIME_TICK 50
