/** @file AbstractProcessor.h
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *  It assumes 'AbstractCore' objects in the gridpoints
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef ABSTRACTPROCESSOR_H
#define ABSTRACTPROCESSOR_H
//#include "AbstractCore.h"
//#include "scProcessor.h"
#include "scFIFO.h"

/*! \brief The AbstractProcessor class assumes that in the AbstractProcessor gridpoints
 * AbstractCore objects are sitting
  */
using namespace std;
// The modules can be the head of the cluster, a member of a cluster, or neither (stand-alone)
// These are the standard offsets of the neighbors in the order of
// the hexagonal Cluster Grid:  Head, N, NE, SE, S, SW, NW

// Here a two-dimensional topology is assumed, and a multi-layer one implied

/*!
 * \brief The AbstractProcessor class
 *
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |----/AbstractProcessor
 * @endverbatim
 */

class AbstractProcessor : public scProcessor
{
  public:
    //Channel/Submodule* definitions
      MetaEvent_fifo* MetaEvents;
    AbstractProcessor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone);
    SC_HAS_PROCESS(AbstractProcessor); // Will be defined in separate file
    ~AbstractProcessor(void);
        void
    Reset(void);
    // Directly HW-related functionality
    void
  Execute_thread(void); ///< Execute metacode
    void Initialize_method(void);
        virtual void
    Populate(vector<scGridPoint*>& Specials);
        AbstractCore*
    Core_Get(int i, int j){ return dynamic_cast<AbstractCore*>(scProcessor::ByIndex_Get(i,j));}
        AbstractCore*
    ClusterHead_Get(int i)
        { return dynamic_cast<AbstractCore*>(AbstractTopology::ClusterHead_Get(i)); }
    AbstractCore *GridPoint_Get(const int i, const int j)
        {return dynamic_cast<AbstractCore *>(mGrid.at(i).at(j));}
    AbstractCore *GridPoint_Get(scGridPoint *GP) // A kind of self-check: dynamic_cast will return NULL if wrong
        {return dynamic_cast<AbstractCore *>(GP);}
 
    AbstractCore* ByIndex_Get(const int X, const int Y)
        { return dynamic_cast<AbstractCore*>(AbstractTopology::ByIndex_Get(X, Y));}
    AbstractCore* ByPosition_Get(const int X, const int Y)
        { return dynamic_cast<AbstractCore*>(AbstractTopology::ByPosition_Get(X, Y));}
    AbstractCore* ByClusterAddress_Get(ClusterAddress_t CA)
    { return dynamic_cast<AbstractCore*>(AbstractTopology::ByClusterAddress_Get(CA));}
    AbstractCore* ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM= cm_Head)
    { return dynamic_cast<AbstractCore*>(AbstractTopology::ByClusterMember_Get(CN,CM));}

    AbstractCore* ByID_Get(int N)
    { return dynamic_cast<AbstractCore*>(scProcessor::ByID_Get(N));}
    string StringOfCore_Get(int N)
    {   return StringOfClusterAddress_Get(ByID_Get(N));}
    AbstractCore* ByIDMask_Get(SC_CORE_MASK_TYPE Mask) /// Return a pointer to gridpoint of the core given by its mask
        { return dynamic_cast<AbstractCore*>(scProcessor::ByIDMask_Get(Mask)); }

/*        AbstractCore*
    ByClusterMember_Get(scGridPoint* GP, ClusterNeighbor N)
    { return dynamic_cast<AbstractCore*>(scProcessor::ByClusterMember_Get(GP,N));}
*/        AbstractCore*
    ChildAllocateFor(scGridPoint* Parent, CorePreference_t CPT = cpt_AnyCore) override
    { return dynamic_cast<AbstractCore*>(scProcessor::ChildAllocateFor(Parent,CPT));}
        AbstractCore*
    ChildPreAllocateFor(AbstractCore* Parent, CorePreference_t CPT = cpt_AnyCore)
    { return dynamic_cast<AbstractCore*>(scProcessor::ChildPreAllocateFor(Parent,CPT));}
        AbstractCore*
    ChildFindCFor(AbstractCore* Parent, CorePreference_t CPT = cpt_AnyCore)
    { return dynamic_cast<AbstractCore*>(scProcessor::ChildFindFor(Parent,CPT));}
    AbstractProcessor* Processor_Get(void)
    {    return dynamic_cast<AbstractProcessor*>(scProcessor::Processor_Get());}
        void
    Reboot(void);
        void
    START_thread(void);
        void
    NEXT_thread(void);
 /*       void
    ChildrenMaskBit_Set(AbstractCore* C);
        void 
    ChildrenMaskBit_Clear(AbstractCore* C);*/
 //!!
        virtual scHThread*
//   doReboot(SC_ADDRESS_TYPE A) = 0;
   doReboot(SC_ADDRESS_TYPE A, HThreadPreference_t hpt_Any){ return (scHThread*)NULL;}
        AbstractCore*  // Allocate a new core for Parent and create a new QT
    doQCREATE(AbstractCore* TheParent, AbstractCore* TheChild,
          SC_CORE_MASK_TYPE CloneMask,
          SC_CORE_MASK_TYPE BackLinkMask,
          CorePreference_t CPT= cpt_Member);
        string
        FileName_Get(void){return lastFile;}
protected:
//        SC_GRIDPOINT_MASK_TYPE
//    msChildrenMask;	///< Account for allocated cores by bit masks
        MetaEvent_Transfer_Type
    msMetaEvent;
        uint32_t
    mTotalBytes;    ///< No of bytes loaded
        uint32_t
    mTotalLines;    ///< No of lines read
        string
    lastFile;     ///< Remember name of the last object file

        vector <AddressToLineRecord*> // Map the memory address to source line
    AddressToLineMap;
        SC_ADDRESS_TYPE
    mInstructionAddress;    // Used in loading object files
};// of class AbstractProcessor

#endif // ABSTRACTPROCESSOR_H
