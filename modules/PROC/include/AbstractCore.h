/** @file AbstractCore.h
 *  @brief Function prototypes for the EMPA simulator, Core.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef AbstractCore_h
#define AbstractCore_h
/** @addtogroup TAC_MODULE_PROC
 *  The neural-related classes and functionality
 *  @{
 */

//#include "Config.h"
//#include "scTypes.h"

#include "scProcessor.h"
//#include "scGridPoint.h"
#include "scClusterBusMemorySlow.h"
using namespace sc_core;
using namespace sc_dt;

typedef SC_GRIDPOINT_ID_TYPE SC_CORE_ID_TYPE;
typedef SC_GRIDPOINT_MASK_TYPE  SC_CORE_MASK_TYPE;

#ifdef MEASURE_PERFORMANCE
    #define PERFORMANCE_COUNTINSTRUCTION CountInstruction();
    #define PERFORMANCE_COUNTMETAINSTRUCTION CountMetaInstruction();
    #define PERFORMANCE_ADDINSTRUCTIONTIME(t) mInstructionTime +=t;
    #define PERFORMANCE_ADDMETAINSTRUCTIONTIME(t) mMetaInstructionTime +=t;
    #define PERFORMANCE_ADDMEMORYTIME(t) mMemoryTime +=t;
    #define PERFORMANCE_ADDFETCHTIME(t) mFetchTime +=t;
    #define PERFORMANCE_ADDWAITTIME(t) mWaitTime +=t;
#else
    #define PERFORMANCE_COUNTINSTRUCTION
    #define PERFORMANCE_COUNTMETAINSTRUCTION
    #define PERFORMANCE_ADDINSTRUCTIONTIME(t)
    #define PERFORMANCE_ADDMETAINSTRUCTIONTIME(t)
    #define PERFORMANCE_ADDMEMORYTIME(t)
    #define PERFORMANCE_ADDFETCHTIME(t)
    #define PERFORMANCE_ADDWAITTIME(t)
#endif // MEASURE_PERFORMANCE



#define  USE_DEBUG_DATA_TYPES

#ifdef USE_DEBUG_DATA_TYPES
typedef int32_t SC_REGISTER_TYPE;
typedef int SC_COOPERATION_MODE_TYPE;
typedef int32_t SC_REGISTER_MASK_TYPE;
#else
typedef sc_dt::sc_uint<CORE_REGISTER_BITS> SC_REGISTER_TYPE;
typedef sc_dt::sc_uint<COOPERATION_MODE_WIDTH> SC_COOPERATION_MODE_TYPE;
typedef sc_dt::sc_uint<MAX_NUMBER_OF_REGFteregicISTERS> SC_REGISTER_MASK_TYPE;
#endif //USE_DEBUG_DATA_TYPES

/*
// These includes and structure fields are needed until made processor-independent
#ifdef __cplusplus
extern "C" {
#endif

#include "isaE.h"
#ifdef __cplusplus
}
#endif
*/

#if 0
/* Different QT operations */
/*!
  \var typedef qt_t
  \brief  The quasi-thread related meta-instructions
 */
typedef enum {
                Q_TERM, //!< Terminate QT
                Q_WAIT, Q_IWAIT, //!< Wait for condition
                Q_ALLOC,    //!< Allocate cores for later use
                Q_CREATE, Q_CREATER, Q_CREATET, Q_CREATEF, //!< Create QT, (un)conditionally
                Q_CALL, Q_MUTEX, //!< QT call, (un)conditionally

                Q_HALT=0xE, //!< Halt processing
                Q_INT, Q_ERR } qt_t; //!< Make an interrupt (for simulation)
#endif//0

// Type for the one-hot bitmasks
//typedef int SC_CORE_MASK_TYPE;
typedef SC_GRIDPOINT_MASK_TYPE SC_CORE_MASK_TYPE;
typedef int SC_REGISTER_TYPE;
/*// Type for the core IS type
// The word type used by the SystemC model
//typedef int SC_ADDRESS_TYPE;
#else
// Type for the one-hot bitmasks
typedef sc_dt::sc_uint<CORE_MASK_WIDTH>  SC_CORE_MASK_TYPE;
typedef sc_dt::sc_uint<CORE_REGISTER_BITS> SC_REGISTER_TYPE;
// Type for the core IS type
// The word type used by the SystemC model
typedef sc_dt::sc_uint<COOPERATION_MODE_WIDTH> SC_COOPERATION_MODE_TYPE;
typedef sc_dt::sc_uint<MEMORY_ADDRESS_WIDTH> SC_ADDRESS_TYPE;
typedef int SC_COOPERATION_MODE_TYPE;
#endif
*/
//#include "scIGPCB.h"
//#include <queue>
//#include <vector>
//#include <iostream>

using namespace sc_core; using namespace std;

class AbstractProcessor;
/*!
 * \class AbstractCore
 * \brief The abstract base class (i.e. must not be instantiated) of concrete processors,
 * implementing their general behavior.

 * The AbstractProcessor is actually built on top of scGridPoint (this is its bottom layer)
 * and uses the autonomous communication facilities implemented there.
 * It has inter-core communication facilities
 * and communicates autonomously with its immediate neighbors and cluster members.
 * The generic processor functionality is implemented in the present top layer,
 * both the computing-related and the morphing functionalities.
 * All general processor-like functionalities may be redefined in subclasses;
 * the processor-specific functionalities must be defined in the subclasses.
 * The event handling is implemented is such a way, that the threads related to
 * processor-like operation are implemented in a non processor-specific way,
 * where the event handling threads use processor-specific virtual functions.
 *
 *
 * This class provides base for specific cores, such as the EMPA-aware cores Y86Core and RISCVCore
 * in this package, but also a neuron-like behavior is subclassed from it.
 * @verbatim
 *  |GridPoint
 *  |--/scGridPoint
 *  |----/AbstractCore
 *  |....../XXCore
 * @endverbatim
 */

class AbstractCore:  public scGridPoint
{
    friend AbstractProcessor;
  public:
    /*! Construct an abstract core. Implements Reset() and event handling.
     * \param nm    The SystemC module name
     * \param Proc  The processor the core belongs to
     * \param[in] GP the topological location of the module
     * \param[in] StandAlone If the core implements some stand-alone functionalities
     *
     * The module implements all general-purpose functionality of an EMPA-aware core.
     * The general-purpose functionality is implemented in the form of event-handling threads,
     * and the core-specific functionality in virtual functions
     */

    AbstractCore(sc_core::sc_module_name nm
             ,AbstractProcessor* Proc
             ,const GridPoint GP
             ,bool StandAlone
            );

//    friend class TestY86Core;
    /*! Destruct an abstract core, including its data structures */
    ~AbstractCore(void);
        SC_HAS_PROCESS(AbstractCore);  // We have the constructor in the .cpp file
        AbstractProcessor*
    Processor_Get(void);
      //! Directly HW-related functionality
        void
    Reset(void);
        virtual bool
        IsMorphingInstructionFetched(scHThread* H){ return true;} ///< During pre-fetch
/*!!        virtual bool
    IsMetaInstructionReceived(void) = 0; ///< During execution, from MetaFIFO
*/        bool
    doExecuteInstruction(scHThread* H);
        bool
    doFetchInstruction(scHThread* H);
        void
    ResetForParent(scGridPoint* Parent);// override;
        AbstractCore* 
    AllocateFor(AbstractCore* Parent);
        AbstractCore*
    PreAllocateFor(AbstractCore* Parent);
       string /// Return the string form of the core address
    CoreText_Get(void) //! Return the string describig the core only
        { return scGridPoint::StringOfClusterAddress_Get();}
        string  /// Return the time of the message
    StringOfTime_Get(void){ return sc_time_to_nsec_Get(1, 6, sc_time_stamp());} 

        AbstractCore*   //!< Return the parent of the core
    Parent_Get(void) { return  dynamic_cast<AbstractCore*>(scGridPoint::Parent_Get());}	/// Return the parent scGridPoint of the scGridPoint

        SC_WORD_TYPE    //!< Return value of register R
    RegisterValue_Get(uint8_t R, bool Performance=true);
        void    /// Set value of register R to V
    RegisterValue_Set(uint8_t R, SC_WORD_TYPE V, bool Performance=true);
        struct{
    sc_core::sc_event
        EXECUTEMETA,
        FETCH,
        FETCHEXECUTED,
        METAEXECUTED,
        NEXT,
        QALLOC,
        QCALL,
        QCALLEXECUTED,
        QCREATE,
        QCREATEEXECUTED,
        QCREATEMETA,
        QHALT,
        QMUTEX,
        QWAIT,
        QWAITMETA,
        QWAITEXECUTED,
        QTERMEXECUTED,
        QTERM;
        }EVENT_CORE;  //!< These events are handled at abstract processor level

        virtual string
    MetaInstructionName_Get(void){ return "";}
        virtual string
    RegisterContentStrings_Get(SC_REGISTER_MASK_TYPE M, bool AlsoCC){return "";}
        void //! Handle if the core was in waiting state
    HandleEndOfWaiting(void);
        bool //! Return true if the QT is ready to terminate
    doCanTerminate( SC_CORE_MASK_TYPE Address);
        void    /// Prepare the execution of the next instruction, after fetching
     PrepareNextInstruction(void);
#ifdef MEASURE_PERFORMANCE
        uint32_t    /// Count conventional instructions
    InstructionCount_Get(){return mInstructionCount;}
        uint32_t    /// Count metainstructions
    MetaInstructionCount_Get(){return mMetaInstructionCount;}
        void CountInstruction(){mInstructionCount++;}
        void CountMetaInstruction(){mMetaInstructionCount++;}
        sc_core::sc_time
    InstructionTime_Get() {return mInstructionTime;}
        sc_core::sc_time
    MetaInstructionTime_Get(){ return    mMetaInstructionTime;}
        sc_core::sc_time
    MemoryTime_Get(){ return mMemoryTime;}
        sc_core::sc_time
    FetchTime_Get(){ return    mFetchTime;}
        sc_core::sc_time
    WaitTime_Get()  {return  mWaitTime;}
#endif //MEASURE_PERFORMANCE

protected:
//        void
//    doCreateQT(scIGPMessage* MyMessage);
        void
    doSkipQTCode(void){}
        void
    NEXT_thread(void);
        void
    EXECUTEMETA_thread(void);
        void
    FETCH_thread(void);

        void
    QHALT_thread(void);
        void
    QTERM_thread(void);
        void
    QWAIT_thread(void);
        void
    QCREATE_thread(void);
        void
    QCREATEMETA_thread(void);
        void
    QWAITMETA_thread(void);
        void
    QALLOC_thread(void);
        void
    QCALL_thread(void);
        void
    QMUTEX_thread(void);
        virtual AbstractCore*
    doFindHostToProcess(SC_ADDRESS_TYPE Offs, SC_GRIDPOINT_MASK_TYPE Mask)
        { return  dynamic_cast<AbstractCore*>(scGridPoint::doFindHostToProcess(Offs, Mask));}

        void
    doExecuteMetaInstruction();
        void
    HandleMetaInstruction(void);
        void ///< A helper function for NEXT_thread
    HandleConventionalInstruction(void);
        void ///< A helper function for NEXT_thread
    TerminateExecution(void);

        void
    FinishPrefetching(void);
/*!!        virtual bool
    doFetchInstruction() = 0;
    */
        bool
    doQTERM(void);
        void
    ReleasePreAllocatedCores(void);

        void
    doKillQT(void);
        uint64_t
    readInstrMem(SC_WORD_TYPE addr);
        void
    writeInstrMem(SC_WORD_TYPE addr, uint32_t C);
#ifdef MAKE_PERFORMANCE_DIAGRAM
        sc_time
    msAllocationBegin;    ///< The beginning of the allocation
        sc_time
    msMetaBegin;    ///< The beginning of a meta instruction

        sc_time
    AllocationBegin_Get(void) { return msAllocationBegin;} // Just to record the beginning of being allocated
         void
    AllocationBegin_Set(sc_time T) {msAllocationBegin = T;} // Just to record the beginning of being allocated
#endif //MAKE_PERFORMANCE_DIAGRAM
        MetaEvent_Transfer_Type
    msMeta; // The metainstruction fetched by the core

//    msVectorMode
/*        void    /// Make the actual inspection; overwritten in subclasses
    doInspect(vector<int32_t>& in, vector<int32_t>& out);*/
/*#ifdef MEASURE_PERFORMANCE
public:
        sc_time
    FetchTime_Get(){ return mFetchTime;}
        sc_time
    InstructionTime_Get(){ return mInstructionTime;}
        sc_time
    MetaInstructionTime_Get(){ return mMetaInstructionTime;}
        sc_time
    WaitTime_Get(){ return mFetchTime;}
        void
    AddFetchTime(sc_time T){mFetchTime += T;}
        void
    AddInstructionTime(sc_time T){mInstructionTime += T;}
        void
    AddMetaInstructionTime(sc_time T){mMetaInstructionTime += T;}
        void
    AddWaitTime(sc_time T){mFetchTime += T;}
        uint32_t    /// Return number of conventional instructions
    InstructionCount_Get(){return mInstructionCount;}
        uint32_t    /// Returm number of  metainstructions
    MetaInstructionCount_Get(){return mMetaInstructionCount;}
#endif //MEASURE_PERFORMANCE
    */
#ifdef MEASURE_PERFORMANCE

         uint32_t
    mInstructionCount,
    mMetaInstructionCount;
        sc_time
    mFetchTime,
    mInstructionTime,
    mMemoryTime,
    mMetaInstructionTime,
    mWaitTime;
#endif //MEASURE_PERFORMANCE
}; // of AbstractCore
/** @}*/

#endif // AbstractCore_h

