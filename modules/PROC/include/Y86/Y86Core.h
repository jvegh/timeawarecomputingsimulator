/** @file Y86Core.h
 *  @brief Function prototypes for the EMPA simulator, Y86Core.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef Y86Core_h
#define Y86Core_h

//#include "Config.h"
//#include "scTypes.h"
//#include "AbstractCore.h"
//#include "isaE.h"
//#include "AbstractProcessor.h"
//#include "scIGPCB.h"
//#include <queue>
//#include <vector>
//#include <iostream>
#ifdef __cplusplus
extern "C" {
#endif

#include "isaE.h"
#ifdef __cplusplus
}
#endif

#include "AbstractProcessor.h"

using namespace sc_core; using namespace std;

extern reg_struct reg_table[];  // Just Y86 register names
/*!
 * \brief The Y86Core class
 * The base class is the AbstractCore, i.e. a general-purpose core.
 * It has its associated inter-gridpoint communication facilities
 * and communicates autonomously with it immediate neighbors and cluster members.
 *
 * This class implements the Y86 specific functionality
 * \param nm the name of the abstract core
 * \param[in] Proc the processor we belong to
 * \param[in] GP the topological location of the new communicating grid point
 *
 * @verbatim
 *  |GridPoint
 *  |--/scGridPoint
 *  |----/AbstractCore
 *  |------/Y86Core
 *  |....../NeurerCore
 * @endverbatim
 */

class Y86Processor;
class Y86Core:  public AbstractCore
{
    friend class Y86Processor;
    friend class scHThread;
  public:
    // Constructor declaration:
    Y86Core(sc_core::sc_module_name nm, // Just the systemC name
             Y86Processor* Proc, // The present system is prepared for one topology only, but ...
             const GridPoint GP // The topographic position
            );
    ~Y86Core(void);
        SC_HAS_PROCESS(Y86Core);  // We have the constructor in the .cpp file
      // Directly HW-related functionality
        void
    Reset(void);
         Y86Processor*
    Processor_Get(void);
         bool
     ConditionHolds(scHThread* H, cond_t StageStage){ return cond_holds(RegCC_Get(H), StageStage);}
         cc_t
     RegCC_Get(scHThread* H) { return (cc_t)H->mCSR; }	// return the condition codes byte
         void
     RegCC_Set(scHThread* H, cc_t CC) {H->mCSR = (short int)CC & 7;}
                         bool
                    IsMetaInstructionFetched(scHThread* H)
                         {return H->fetch.Stage.OpCode ==  (itype_t) I_QT;}
                         /*!
                          * \brief PC_MaybeAffected
                          * \return true if the program counter imay potentially be affected
                          */
                         bool PC_MaybeAffected(scHThread* H)
                     {   itype_t I = (itype_t)H->fetch.Stage.OpCode;  // Check what was fetched
                         return ( I== I_JMP) || (I == I_CALL) || (I == I_RET) || (I == I_LEAVE);
                     }
                         bool
                     PC_IsAffected(scHThread* H, cond_t C)
                     {   return PC_MaybeAffected(H) && ConditionHolds(H, C);
                                        }	// Set the condition codes byte
 #ifdef newdef
        void
    doSetQTAddresses(scGridPoint* Parent) override;


        scIGPMessage*
    CreateRegisterMessageTo(Y86Core* To, int32_t Mask)
    {return  dynamic_cast<scIGPMessage*>(AbstractCore::CreateRegisterMessageTo(To, Mask));}
        Y86Core*
    Parent_Get(void) { return  dynamic_cast<Y86Core*>(AbstractCore::Parent_Get());}	///< Return the parent scGridPoint of the scGridPoint
        string
    RegisterName_Get(reg_id_t R){ return string(reg_table[R].name);}
         Y86Core*
    AllocateFor(Y86Core* Parent);
        Y86Core*
    PreAllocateFor(Y86Core* Parent);
        int
    FlagWordLength_Get(void) {return 1;}
        int32_t
    ConditionCode_Get(void) {return  RegCC_Get();}
        void
    ConditionCode_Set(int32_t C){RegCC_Set((cc_t) C);}
//    void CREATE_method(void);
        SC_WORD_TYPE
      RegisterValue_Get(reg_id_t R, bool Performance=true);
        void
      RegisterValue_Set(reg_id_t R, SC_WORD_TYPE V, bool Performance=true);
#endif

        bool
    doFetchInstruction(scHThread* H//SC_WORD_TYPE Instr
                         ); // The actual fetching; does not generate event
        bool
    doExecuteInstruction(scHThread* H);
        bool
    IsMorphingInstructionFetched(scHThread* H);

#ifdef newdef
        void
    doSkipQTCode(void);
/*        bool
    doProcessQMessage(scIGPMessage* Message);*/
         virtual string
    MetaInstructionName_Get(void);

 protected:
//        void
//    doQTERM(void);
//        void
        void ///< A helper function for NEXT_thread
    TerminateExecution(void);
        bool
    IsMetaInstructionReceived(void)
    {   return (itype_t)msMeta.Code == I_QT;}
        void
    FinishPrefetching(void);
        bool
    FindCA(ClusterAddress_t CA) ;

        string
        RegisterContentString_Get(reg_id_t R);
        virtual string
    RegisterContentStrings_Get(SC_REGISTER_MASK_TYPE M, bool AlsoCC);
#endif //newdef
        stat_t
    msCoreStatus;
}; // of Y86Core

#endif // Y86Core_h

