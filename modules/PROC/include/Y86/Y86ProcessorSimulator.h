/** @file Y86ProcessorSimulator.h
 *  @brief Function prototypes for the EMPA simulator, simulator main file.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef Y86ProcessorSimulator_h
#define Y86ProcessorSimulator_h
//#include <systemc>
#include "ProcConfig.h"
#include "ProcessorSimulator.h"

using namespace sc_core; using namespace std;
//Submodule forward class declarations
class Y86Processor;


class Y86ProcessorSimulator : public ProcessorSimulator {
  public:
    // Constructor declaration: Every SystemC aspects are handled in the anchestor
    /*!
     * @brief The equivalent of the electronically implemented simulator
     *
         * \param nm SystemC name
         * \param argc Number of arguments
         * \param argv  The argument strings
         * \param StandAlone If standalone operation requested
     */
Y86ProcessorSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone);
    SC_HAS_PROCESS(Y86ProcessorSimulator); // Contructor implemented in .cpp
        virtual
    ~Y86ProcessorSimulator();
      Y86Processor*
    Processor_Get(void);
  protected:
//        void HandleSpecials(void); //
        void
    Setup(void);
   void readSettings(void);
   // This makes the real reading, either application- or project-wide settings
   void readSettings(QSettings *settings, string Level);
   void writeSettings();
   void readAliases(QSettings *Psettings, string Level);
   void readConnections(QSettings *settings, string Level);
    void 
    SInitialize_method(void);
/*      void
    SSUSPEND_thread(void);
      void
    SRESUME_thread(void);
      void
    SSTART_thread(void);
      void
    SSTOP_thread(void);
      void
    SHALT_thread(void);
    */
    /*
   protected:
    // These members are initialized in the constructor
 */     void
    PrintFinalReport(void);

}; // of Y86ProcessorSimulator

#endif //  Y86ProcessorSimulator_h
