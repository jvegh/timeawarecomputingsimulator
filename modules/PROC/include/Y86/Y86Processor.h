/** @file Y86Processor.h
 *  @brief Function prototypes for the topology of electronic modules, placed on a die.
 *  It assumes 'Y86Core' objects in the gridpoints
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef Y86PROCESSOR_H
#define Y86PROCESSOR_H
#include "Y86Core.h"
//#include "AbstractProcessor.h"
/*#ifdef __cplusplus
extern "C" {
#endif

#include "isaE.h"
#ifdef __cplusplus
}
#endif
*/
/*! \brief The CoreTopology class assumes that in the AbstractProcessor gridpoints
 * AbstractCore objects are sitting
  */
using namespace std;
/*!
 * \brief The Y86Processor class
 *
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |----/AbstractProcessor
 *  |------/Y86Processor
 * @endverbatim
 */
class Y86Processor : public AbstractProcessor
{
  public:
    Y86Processor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone);
    SC_HAS_PROCESS(Y86Processor); // Will be defined in separate file
    ~Y86Processor(void);
        void
    Reset(void);
        void
    Initialize_method(void);
        int
    interpretLine(string S, int &lineno);
        bool
    loadHexFile(string fileName);
        bool
    loadStream(string fileString);
        scHThread*
    doReboot(SC_ADDRESS_TYPE A, HThreadPreference_t hpt_Any);
        void
    Populate(vector<scGridPoint*>& Specials);
        string
    StringOfMessage_Get(scIGPMessage* Message);
        Y86Core*
    ChildAllocateFor(scGridPoint* Parent, CorePreference_t CPT = cpt_AnyCore) override
        { return dynamic_cast<Y86Core*>(AbstractProcessor::ChildAllocateFor(Parent,CPT));}
        Y86Core*
    ByIndex_Get(int i, int j){ return dynamic_cast<Y86Core*>(AbstractProcessor::GridPoint_Get(i,j));}
        string ///< An Y86 specific register name
    RegisterName_Get(reg_id_t Index);
        Y86Core* ByPosition_Get(const int X, const int Y)
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByPosition_Get(X, Y));}
        Y86Core* ByID_Get(const int ID)
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByID_Get(ID));}
        Y86Core* ByIDMask_Get(SC_CORE_MASK_TYPE Mask) /// Return a pointer to gridpoint of the core given by its mask
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByIDMask_Get(Mask)); }
        Y86Core* ByClusterAddress_Get(ClusterAddress_t CA)
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByClusterAddress_Get(CA));}
#ifdef newdef
        Y86Core* ByClusterMember_Get(unsigned short int CN, ClusterNeighbor CM= cm_Head)
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByClusterAddress_Get(CN,CM));}
        Y86Core* ByClusterMember_Get(scGridPoint* GP, ClusterNeighbor N)
    { return dynamic_cast<Y86Core*>(AbstractProcessor::ByClusterMember_Get(GP,N));}
        Y86Core*
    ChildPreAllocateFor(Y86Core* Parent, CorePreference_t CPT = cpt_AnyCore)
        { return dynamic_cast<Y86Core*>(AbstractProcessor::ChildPreAllocateFor(Parent,CPT));}
        Y86Core*
    ChildFindFor(Y86Core* Parent, CorePreference_t CPT = cpt_AnyCore)
        { return dynamic_cast<Y86Core*>(AbstractProcessor::ChildFindFor(Parent,CPT));}
        Y86Processor*
    Processor_Get(void)
        {    return dynamic_cast<Y86Processor*>(AbstractProcessor::Processor_Get());}

        Y86Core*
    ClusterHead_Get(int i)
        { return dynamic_cast<Y86Core*>(AbstractProcessor::ClusterHead_Get(i));}
 //       Y86Core*
 //   ChildFindFor(Y86Core* Parent, CorePreference_t CPT); ///< find a core for this parent
        void
    START_thread(void);
 /*    scGridPoint *GridPoint_Get(scGridPoint *GP) // A kind of self-check: dynamic_cast will return NULL if wrong
        {return dynamic_cast<scGridPoint *>(GP);}

    scGridPoint* ByIndex_Get(const int X, const int Y)
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ByIndex_Get(X, Y));}
    scGridPoint* GridPointByPosition_Get(const int X, const int Y)
        { return dynamic_cast<scGridPoint*>(AbstractTopology::ByPosition_Get(X, Y));}
    scGridPoint* ByClusterAddress_Get(ClusterAddress_t CA)
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ByClusterAddress_Get(CA.Cluster,(ClusterNeighbor)CA.Member));}
    scGridPoint* ByClusterAddress_Get(unsigned short int CN, ClusterNeighbor CM= cm_Head)
    { return dynamic_cast<scGridPoint*>(AbstractTopology::ByClusterAddress_Get(CN,CM));}
*/
protected:
//        MetaEvent_Transfer_Type
//      msMetaEvent;
#endif //newdef
protected:
//        uint32_t
//    mTheseBytes;     ///< Remember name of the last object file
        SC_ADDRESS_TYPE
    program_counter;
        bool
    PC_seen;
};       // of class Y86Processor

#endif // Y86PROCESSOR_H
