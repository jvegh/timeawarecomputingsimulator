/** @file scFIFO.h
 *  @brief Function prototypes for the scEMPA simulator, FIFOs.
 *
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

// Header for the scEMPA simulator, Inter-core block

#ifndef scFIFO_h
#define scFIFO_h
//#include "Utils.h"
#include "AbstractCore.h"

using namespace sc_core;
//Submodule forward class declarations

// This is a FIFO for the prioritized metaevents

class MetaEvent_write_if : virtual public sc_interface
{
   public:
     virtual void write(MetaEvent_Transfer_Type) = 0;
     virtual void reset() = 0;
};

class MetaEvent_read_if : virtual public sc_interface
{
   public:
     virtual void read(MetaEvent_Transfer_Type &) = 0;
     virtual int num_available() = 0;
};

// This is a FIFO for the prioritized metaevents
class MetaEvent_fifo: public sc_core::sc_channel, MetaEvent_write_if, MetaEvent_read_if
{
   public:
     MetaEvent_fifo(const sc_module_name& name) ;
     virtual
       ~MetaEvent_fifo(void){}
     void write(MetaEvent_Transfer_Type T);
     void read(MetaEvent_Transfer_Type &T);
     void dowrite(MetaEvent_Transfer_Type T);
     void doread(MetaEvent_Transfer_Type &T);
     void reset() { num_elements = first = 0;}
     int num_available() { return num_elements;}
     bool NewPriorityHigher(MetaEvent_Transfer_Type New, MetaEvent_Transfer_Type Old);
     int Length_Get(void){ return max;}
     sc_event write_event, read_event;
   private:
//   enum e { max = MAX_NUMBER_OF_CORES };
     enum e { max = 10 }; // Only for testing!
     MetaEvent_Transfer_Type data[max];
     int num_elements, first;
};


#endif // scFIFO
