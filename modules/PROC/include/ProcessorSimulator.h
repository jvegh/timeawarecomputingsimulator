/** @file ProcessorSimulator.h
 *  @brief Function prototypes for the scEMPA simulator, simulator main file.
 */
 /*  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#ifndef ProcessorSimulator_h
#define ProcessorSimulator_h
//#include <systemc>
#include <map> // For the aliases
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>
#include <QDate>
#include <QTime>
#include <QSettings>
#include <QCoreApplication>
//#include "scGridPoint.h"
#include "scSimulator.h"
class scClusterBus;
class scClusterBusArbiter;
class AbstractProcessor;
class AbstractCore;
class scqTreeModel;
using namespace sc_core; using namespace std;


//Submodule forward class declarations
class ProcessorSimulator : public scSimulator {
    //Port declarations
    // channels
  protected:
//    sc_clock C1 {"clk", 100, SC_PS};
    //
    //Channel/Submodule* definitions
/*  simple_bus_master_blocking     *master_b;
  simple_bus_master_non_blocking *master_nb;
  simple_bus_master_direct       *master_d;
  simple_bus_slow_mem            *mem_slow;
*/
     scClusterBus                *msClusterBus;
//  simple_bus_fast_mem            *mem_fast;
  scClusterBusArbiter             *msClusterBusArbiter;
  public:
    // Constructor declaration:
  /*!
     * \brief The generic ProcessorSimulator
     * \param nm SystemC name
     * \param argc Number of arguments
     * \param argv  The argument strings
     * \param StandAlone If standalone operation requested
     */
    ProcessorSimulator(sc_core::sc_module_name nm, int argc, char* argv[], bool StandAlone=true);
      SC_HAS_PROCESS(ProcessorSimulator); // Contructor implemented in .cpp
      virtual
    ~ProcessorSimulator();
      string
      PrologText_Get(void);

    void Setup(void);  // Belongs to the constructor
    void writeSettings(void); // Write settings to the project directory
/*        void
    SetupSystemDirectories(QWidget* parent);
        void
    SetupSettings(void);*/
      void
    SInitialize_method(void); ///< This initializes the simulator
      bool
    StepwiseMode_Get(void){ return msStepwiseMode;}
      void
    StepwiseMode_Set(bool b){msStepwiseMode = b;}
      void
    SSUSPEND_thread(void);
      void
    SRESUME_thread(void);
      void
    SSTART_thread(void);
      void
    SSTOP_thread(void);
      void
    SHALT_thread(void);
        struct{
      sc_core::sc_event
    START,
    STOP,
    HALT,
    SUSPEND,
    RESUME;
        }EVENT_SIMULATOR;
        //virtual
        AbstractProcessor*
    Processor_Get();
        void ClockFlag_Set(bool B){m_clockFlag = B;}
        bool ClockFlag_Get(void){return m_clockFlag;}

   scqTreeModel* mModuleTree;
   scqTreeModel* mClusterTree;
//   scqTreeItem *rootItem;

  protected:
        void clock_method(void)
        { ClockFlag_Set(true);}
        void SetupHierarchies(void);
    /*
       void
    PrintFinalReport(scProcessor *Proc);
    */
        void
    HandleSpecials(void); /// Handle the exceptional points
        vector<scGridPoint*>
    mSpecials;   /// Stores the special points
        std::map<string,int32_t>
    mAliases;      /// Stores alias names of the gridpoints
      bool
    msStepwiseMode,   ///< If to process stepwise
    msSuspended;      ///< If running simulation is suspended
      bool
    m_clockFlag;
    string mSettingsFileName;   /// The filename of the config files
    QScopedPointer<QFile>   m_logFile;
}; // of ProcessorSimulator

#endif // ProcessorSimulator_h
