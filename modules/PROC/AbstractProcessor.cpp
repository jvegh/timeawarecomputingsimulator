/** @file AbstractProcessor.cpp
 *  @brief Function prototypes for the topology of processor cores, placed on a die.
 *
 * Source file for electronic simulators, for handling points with electronic functionality  (modules)
 * arranged logically as a rectangular grid physically as a hexagonal grid
 * The physical proximity provides for sharing facilities, to communicate directly, etc.
 * Here only the topology handled, the  communication details can be found in Communication.cpp
 * and Buses.cpp
 *  @author János Végh (jvegh)
 *  @bug No known bugs.
 */

#include "AbstractProcessor.h"
//#include "AbstractCore.h"
//#include "scIGPCB.h"
//#include "scIGPMessage.h"
#include "Utils.h"

// This section configures debug and log printing
//#define SUPPRESS_LOGGING // Suppress all log messages
#define DEBUG_EVENTS    // Print event trace debug messages
#define DEBUG_PRINTS    // Print general debug messages
// Those defines must be located before 'Macros.h", and are undefined in that file
#include "Macros.h"

bool OBJECT_FILE_PRINTED;
extern bool UNIT_TESTING;	// Whether in course of unit testing
extern NamedEnum_struct MessageTypes[];

/*!
 * \brief AbstractProcessor::AbstractProcessor
 * \param nm SystemC name
 * \param Specials the list of special neurons
 * \param StandAlone true if some part shall be executed here rather than in the subclass
 * 
 * @verbatim
 *  |AbstractTopology
 *  |--/scProcessor
 *  |..../AbstractProcessor
 *  |....  /XXProcessor
 * @endverbatim
 * 
 */
     AbstractProcessor::
AbstractProcessor(sc_core::sc_module_name nm, vector<scGridPoint*> Specials, bool StandAlone)
             : scProcessor(nm, Specials, false)
{
    if(StandAlone)
    {
        SC_THREAD(Initialize_method);
        SC_THREAD(START_thread);
             sensitive << EVENT_PROCESSOR.START;
        Populate(Specials); // Populate with abstract cores
        CreateClusters();      // Clusterize cores as gridpoints
        ConnectIGPCBs();       // Connect their communication lines
        ConnectClusterHeadsToBus(mClusterBus);  // Connect ONLY cluster heads to inter-cluster bus
        Reset();
    }

    MetaEvents = new  MetaEvent_fifo("SV_MetaFIFO");
    // Triggered by writing into the MetaEvent_fifo
    SC_THREAD(Execute_thread);
      sensitive << MetaEvents->write_event;
}

   AbstractProcessor::
~AbstractProcessor(void)
{
}

/*!
 * This method starts up when everything is initialized
 */
void AbstractProcessor::Initialize_method(void)
{
//    wait(SCTIME_SV); // For setup
    DEBUG_PRINT_OBJECT("Initialize_method started" );
//    mscoreStatus = STAT_AOK; // Assume everything all-right
    // We surely have no processing unit and also the first instruction must be fetched
//    Processor_Get()->EVENT_PROCESSOR.START.notify(SC_ZERO_TIME);
}

    void AbstractProcessor::
Reset(void)
{
      scProcessor::Reset();
      for(int i = 0; i < GRID_SIZE_X; i++)
        for(int j = 0; j < GRID_SIZE_Y; j++)
            ByIndex_Get(i,j)->Reset();
}
/*!
 * \brief AbstractProcessor::START_thread
 * This thread is waiting for a START_event and processes it
 * It should be reimplemented in the derived classes
 */
void
AbstractProcessor::START_thread(void)
{
    while(true)
    {
         DEBUG_EVENT_OBJECT("WAIT  EVENT_PROCESSOR.START");
         wait(EVENT_PROCESSOR.START);
         DEBUG_EVENT_OBJECT("RCVD  EVENT_PROCESSOR.START");
         msHalted = false;
         //    wait(1,SC_NS);
         // Temporary, used to test FETCH events
/*         scProcessor *Proc = Processor_Get();
         scGridPoint *GP10 = Proc->ByID_Get(10);
         scGridPoint *GP11 = Proc->ByID_Get(11);
         scHThread *HT103 = GP10->HThread_Get(3);
         scHThread *HT105 = GP10->HThread_Get(5);
         scHThread *HT115 = GP11->HThread_Get(5);
         //        wait(1,SC_NS);  // Enable initialization happen
         // These HThreads ask for a gridpoint at the same time.
         // Only one of HT103 or HT105 will get through
         HT103->EVENT_HTHREAD.FETCH.notify();
         wait(10,SC_PS);
         HT105->EVENT_HTHREAD.FETCH.notify();
         HT115->EVENT_HTHREAD.FETCH.notify();
         wait(10,SC_PS);
         bool HT103Busy = HT103->OperatingBit_Get(tob_FetchPending);
         bool HT105Busy = HT105->OperatingBit_Get(tob_FetchPending);
         bool HT115Busy = HT115->OperatingBit_Get(tob_FetchPending);
         */
    }
}


/*!
 * \brief scscProcessor::Execute
 *
 * Metaevents (sent by the cores) are waiting in the queue. Upon receiving a clock pulse,
 * scSupervisor executes the next one. Since the events are ordered by priority, simply takes the first
 */
void AbstractProcessor::
Execute_thread(void)
{
while(true)
  {
    DEBUG_EVENT_OBJECT("WAIT META_event from FIFO");
    wait(MetaEvents->write_event);
//    DEBUG_EVENT_OBJECT("RCVD META_event from FIFO");
    wait(SCTIME_CLOCKTIME);   // Imitate processing delay
    MetaEvents->read(msMetaEvent); // Maybe blocking read
    // OK, now we have the metaevent, can play with its parameters
    AbstractCore* TheCore = ByClusterAddress_Get(msMetaEvent.CA);
    DEBUG_EVENT_OBJECT("Dequeued '"  << TheCore->MetaInstructionName_Get() << "'");
    TheCore->msMeta = msMetaEvent;  // Give a copy of the present meta-instruction to the core
    TheCore->EVENT_CORE.EXECUTEMETA.notify(SC_ZERO_TIME);
//!!    DEBUG_EVENT_OBJECT("SENT EVENT_CORE.EXECUTEMETA '"  << TheCore->MetaInstructionName_Get() << "' to " << TheCore->PrologText_Get());
  }
}


/* ***
 * For rebooting, it is necessary that
 * 1/ the processor is not yet halted
 * 2/ no cores are actually used
 * 3/ the core fragment begins with a conventional instruction
 */
void AbstractProcessor::
Reboot(void){
    doReboot(-1 , hpt_Any);
};

/*!
 * \brief AbstractProcessor::Populate
 * \param Specials pre-created, subclassed from scGridPoint, points
 */
void AbstractProcessor::
Populate(vector<scGridPoint*>& Specials)
{
    // Create communication grid point objects
   AbstractCore* C;
   for(int i = 0; i < GRID_SIZE_X; i++, i++) // Twice because of the hexagonal grid
   {
       for(int j = 0; j < GRID_SIZE_Y; j++)
       {
           int LinearAddress1 = LinearAddressFromCoordinates_Get(i,j);
               if(!mGrid.at(i).at(j))
               {// This is pre-populated by a special
                   C = new AbstractCore(sc_core::sc_module_name(string("GP_").append(IDtoString(LinearAddress1,GRID_BUS_WIDTH)).c_str()),
                       this,  GridPoint(i,j), true//StandAlone
                   );
                   mGrid.at(i).at(j)= C; // Store the pointer in the grid
               }
               int LinearAddress3 =  LinearAddressFromCoordinates_Get(i+1,j);
               if(!mGrid.at(i+1).at(j))
               {// This is pre-populated by a special
                   C = new AbstractCore(sc_core::sc_module_name(string("GP_").append(IDtoString(LinearAddress3,GRID_BUS_WIDTH)).c_str()),
                       this, GridPoint(i+1,j), true//StandAlone
                   );
                   mGrid.at(i+1).at(j) = C; // Store the pointer in the shadow-column
               }
       }
   }
}

  /*!
   * \brief AbstractProcessor::doQCREATE
   * Create a new QT for TheParent, allocate a core if TheChild is NULL
   *
   * \param TheParent that needs a new child
   * \param TheChild if NULL will be sought, else the core is already allocated
   * \param CloneMask
   * \param BackLinkMask
   * \param CPT Which type of core is preferred
   * \return TheChild if successful
   */

  AbstractCore* AbstractProcessor::
doQCREATE(AbstractCore* TheParent, AbstractCore* TheChild,
          SC_CORE_MASK_TYPE CloneMask,
          SC_CORE_MASK_TYPE BackLinkMask,
          CorePreference_t CPT)
{
    AbstractCore* TheCore;
    if(TheChild)
        TheCore = TheChild; // The core is pre-allocated
    else
        TheCore = ChildAllocateFor(TheParent, CPT);
    if(!TheCore) return nullptr; // No available cores found, help yourself with NULL
    // Everything OK, do the work
//!!    SetCloningMasks(TheCore);
//!!    TheCore->CloneRegisterFileFrom(TheParent, CLONE_REGISTER_MASK, true);
    return TheCore;
}

/*
    void AbstractProcessor::
ChildrenMaskBit_Set(AbstractCore* C)
  { msChildrenMask |= C->IDMask_Get();}	///< Set the mask bit of the allocated cores
    void AbstractProcessor::
ChildrenMaskBit_Clear(AbstractCore* C)
  { msChildrenMask &= ~C->IDMask_Get();}  ///< Clear the mask bit of the allocated cores
*/
