# This is the main CMakeLists file of the Modules/PROC library
#
# @author János Végh

# Generate the processor module library

message(HIGHLIGHTED "                      PROC library")

file(GLOB_RECURSE MY_SRCS
    *.cpp
    Y86/*.cpp
    Y86/*.c
)

file(GLOB_RECURSE MY_HEADERS
    include/*.h
)

include_directories( include
    ../include
    include/Y86
    ../BASIC/include
       ${Qt5Widgets_INCLUDE_DIRS}
       ${SystemC_INCLUDE_DIRS}
       ../3rdParty/QSystemC/include
)

link_directories(
        ${SystemC_LIBRARY_DIRS}
        )

ADD_LIBRARY(ProcModules  STATIC
    ${MY_SRCS}
)

target_link_libraries( ProcModules
                       BasicModules
                      Qt5::Widgets
                      ${SystemC_LIBRARIES}
                      ${Pthread}
 )

set_target_properties(ProcModules
                      PROPERTIES OUTPUT_NAME ProcModules
                      )
target_include_directories(ProcModules PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)


INSTALL(FILES  ${MY_SRCS} CMakeLists.txt
        DESTINATION modules/PROC
        COMPONENT srcs)

INSTALL(FILES  ${MY_HEADERS}
        DESTINATION modules/PROC/include
        COMPONENT srcs)

